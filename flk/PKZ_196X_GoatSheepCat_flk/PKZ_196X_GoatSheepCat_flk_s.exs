<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID59AFB449-555A-B00B-179C-AD90A26527B9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_GoatSheepCat_flk.wav" />
         <referenced-file url="PKZ_196X_GoatSheepCat_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_GoatSheepCat_flk\PKZ_196X_GoatSheepCat_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">374</ud-information>
            <ud-information attribute-name="# HIAT:w">256</ud-information>
            <ud-information attribute-name="# e">256</ud-information>
            <ud-information attribute-name="# HIAT:u">60</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.034" type="appl" />
         <tli id="T1" time="1.234" type="appl" />
         <tli id="T2" time="2.433" type="appl" />
         <tli id="T3" time="3.632" type="appl" />
         <tli id="T4" time="4.832" type="appl" />
         <tli id="T5" time="6.032" type="appl" />
         <tli id="T6" time="7.231" type="appl" />
         <tli id="T7" time="8.43" type="appl" />
         <tli id="T8" time="9.63" type="appl" />
         <tli id="T9" time="10.83" type="appl" />
         <tli id="T10" time="12.029" type="appl" />
         <tli id="T11" time="12.774" type="appl" />
         <tli id="T12" time="13.426" type="appl" />
         <tli id="T13" time="14.077" type="appl" />
         <tli id="T14" time="14.729" type="appl" />
         <tli id="T15" time="15.588" type="appl" />
         <tli id="T16" time="16.349" type="appl" />
         <tli id="T17" time="17.324" type="appl" />
         <tli id="T18" time="19.366403111669033" />
         <tli id="T19" time="20.205" type="appl" />
         <tli id="T20" time="21.115" type="appl" />
         <tli id="T21" time="22.026" type="appl" />
         <tli id="T22" time="22.936" type="appl" />
         <tli id="T23" time="23.846" type="appl" />
         <tli id="T24" time="24.756" type="appl" />
         <tli id="T25" time="25.666" type="appl" />
         <tli id="T26" time="26.577" type="appl" />
         <tli id="T27" time="27.487" type="appl" />
         <tli id="T28" time="28.397" type="appl" />
         <tli id="T29" time="28.956" type="appl" />
         <tli id="T30" time="29.516" type="appl" />
         <tli id="T31" time="30.075" type="appl" />
         <tli id="T32" time="30.634" type="appl" />
         <tli id="T33" time="31.794" type="appl" />
         <tli id="T34" time="32.917" type="appl" />
         <tli id="T35" time="34.04" type="appl" />
         <tli id="T36" time="35.164" type="appl" />
         <tli id="T37" time="36.287" type="appl" />
         <tli id="T38" time="37.41" type="appl" />
         <tli id="T39" time="38.007" type="appl" />
         <tli id="T40" time="38.605" type="appl" />
         <tli id="T41" time="39.172" type="appl" />
         <tli id="T42" time="39.739" type="appl" />
         <tli id="T43" time="40.306" type="appl" />
         <tli id="T44" time="40.873" type="appl" />
         <tli id="T45" time="41.63943333407395" />
         <tli id="T46" time="42.175" type="appl" />
         <tli id="T47" time="42.91" type="appl" />
         <tli id="T48" time="43.646" type="appl" />
         <tli id="T49" time="44.381" type="appl" />
         <tli id="T50" time="45.157" type="appl" />
         <tli id="T51" time="45.851" type="appl" />
         <tli id="T52" time="47.11269218250089" />
         <tli id="T53" time="48.155" type="appl" />
         <tli id="T54" time="49.051" type="appl" />
         <tli id="T55" time="49.948" type="appl" />
         <tli id="T56" time="50.632" type="appl" />
         <tli id="T57" time="51.317" type="appl" />
         <tli id="T58" time="52.001" type="appl" />
         <tli id="T59" time="53.78593470049769" />
         <tli id="T60" time="54.653" type="appl" />
         <tli id="T61" time="55.429" type="appl" />
         <tli id="T62" time="56.205" type="appl" />
         <tli id="T63" time="57.51255065210629" />
         <tli id="T64" time="58.609" type="appl" />
         <tli id="T65" time="59.798" type="appl" />
         <tli id="T66" time="60.986" type="appl" />
         <tli id="T67" time="61.923" type="appl" />
         <tli id="T68" time="62.859" type="appl" />
         <tli id="T69" time="63.932463284305015" />
         <tli id="T70" time="64.476" type="appl" />
         <tli id="T71" time="65.157" type="appl" />
         <tli id="T72" time="65.837" type="appl" />
         <tli id="T73" time="66.664" type="appl" />
         <tli id="T74" time="67.492" type="appl" />
         <tli id="T75" time="69.727" type="appl" />
         <tli id="T76" time="70.887" type="appl" />
         <tli id="T77" time="72.047" type="appl" />
         <tli id="T78" time="73.207" type="appl" />
         <tli id="T79" time="74.367" type="appl" />
         <tli id="T80" time="75.282" type="appl" />
         <tli id="T81" time="76.197" type="appl" />
         <tli id="T82" time="77.112" type="appl" />
         <tli id="T83" time="78.002" type="appl" />
         <tli id="T84" time="79.55225071653928" />
         <tli id="T85" time="80.411" type="appl" />
         <tli id="T86" time="81.17" type="appl" />
         <tli id="T87" time="81.928" type="appl" />
         <tli id="T88" time="82.687" type="appl" />
         <tli id="T89" time="84.341" type="appl" />
         <tli id="T90" time="85.874" type="appl" />
         <tli id="T91" time="87.406" type="appl" />
         <tli id="T92" time="88.939" type="appl" />
         <tli id="T93" time="90.472" type="appl" />
         <tli id="T94" time="91.662" type="appl" />
         <tli id="T95" time="92.848" type="appl" />
         <tli id="T96" time="94.014" type="appl" />
         <tli id="T97" time="95.181" type="appl" />
         <tli id="T98" time="97.46534027284038" />
         <tli id="T99" time="98.391" type="appl" />
         <tli id="T100" time="99.264" type="appl" />
         <tli id="T101" time="100.138" type="appl" />
         <tli id="T102" time="101.012" type="appl" />
         <tli id="T103" time="101.886" type="appl" />
         <tli id="T104" time="102.76" type="appl" />
         <tli id="T105" time="103.633" type="appl" />
         <tli id="T106" time="104.507" type="appl" />
         <tli id="T107" time="105.236" type="appl" />
         <tli id="T108" time="105.923" type="appl" />
         <tli id="T109" time="106.616" type="appl" />
         <tli id="T110" time="107.85853216650374" />
         <tli id="T111" time="108.377" type="appl" />
         <tli id="T112" time="109.444" type="appl" />
         <tli id="T113" time="110.511" type="appl" />
         <tli id="T114" time="111.578" type="appl" />
         <tli id="T115" time="113.31179128710451" />
         <tli id="T116" time="114.282" type="appl" />
         <tli id="T117" time="115.477" type="appl" />
         <tli id="T118" time="116.673" type="appl" />
         <tli id="T119" time="117.869" type="appl" />
         <tli id="T120" time="118.79" type="appl" />
         <tli id="T121" time="119.71" type="appl" />
         <tli id="T122" time="120.631" type="appl" />
         <tli id="T123" time="121.99833973960185" />
         <tli id="T124" time="122.934" type="appl" />
         <tli id="T125" time="123.767" type="appl" />
         <tli id="T126" time="124.599" type="appl" />
         <tli id="T127" time="126.59827713961963" />
         <tli id="T128" time="127.547" type="appl" />
         <tli id="T129" time="128.219" type="appl" />
         <tli id="T130" time="128.892" type="appl" />
         <tli id="T131" time="129.565" type="appl" />
         <tli id="T132" time="130.469" type="appl" />
         <tli id="T133" time="131.373" type="appl" />
         <tli id="T134" time="132.277" type="appl" />
         <tli id="T135" time="133.181" type="appl" />
         <tli id="T136" time="136.37147747067186" />
         <tli id="T137" time="137.588" type="appl" />
         <tli id="T138" time="138.524" type="appl" />
         <tli id="T139" time="139.221" type="appl" />
         <tli id="T140" time="139.918" type="appl" />
         <tli id="T141" time="141.20474502866156" />
         <tli id="T142" time="141.939" type="appl" />
         <tli id="T143" time="142.74" type="appl" />
         <tli id="T144" time="143.542" type="appl" />
         <tli id="T145" time="144.129" type="appl" />
         <tli id="T146" time="144.716" type="appl" />
         <tli id="T147" time="145.304" type="appl" />
         <tli id="T148" time="145.891" type="appl" />
         <tli id="T149" time="147.11133131332207" />
         <tli id="T150" time="148.532" type="appl" />
         <tli id="T151" time="149.954" type="appl" />
         <tli id="T152" time="151.377" type="appl" />
         <tli id="T153" time="152.799" type="appl" />
         <tli id="T154" time="153.686" type="appl" />
         <tli id="T155" time="154.346" type="appl" />
         <tli id="T156" time="155.007" type="appl" />
         <tli id="T157" time="155.668" type="appl" />
         <tli id="T158" time="156.328" type="appl" />
         <tli id="T159" time="156.989" type="appl" />
         <tli id="T160" time="157.649" type="appl" />
         <tli id="T161" time="158.31" type="appl" />
         <tli id="T162" time="159.303" type="appl" />
         <tli id="T163" time="160.296" type="appl" />
         <tli id="T164" time="161.289" type="appl" />
         <tli id="T165" time="162.327" type="appl" />
         <tli id="T166" time="163.366" type="appl" />
         <tli id="T167" time="164.404" type="appl" />
         <tli id="T168" time="165.441" type="appl" />
         <tli id="T169" time="166.477" type="appl" />
         <tli id="T170" time="167.89104852470672" />
         <tli id="T171" time="168.597" type="appl" />
         <tli id="T172" time="169.336" type="appl" />
         <tli id="T173" time="170.076" type="appl" />
         <tli id="T174" time="170.815" type="appl" />
         <tli id="T175" time="171.554" type="appl" />
         <tli id="T176" time="172.324" type="appl" />
         <tli id="T177" time="173.25097558211877" />
         <tli id="T178" time="174.096" type="appl" />
         <tli id="T179" time="174.631" type="appl" />
         <tli id="T180" time="175.165" type="appl" />
         <tli id="T181" time="175.7" type="appl" />
         <tli id="T182" time="176.234" type="appl" />
         <tli id="T183" time="176.769" type="appl" />
         <tli id="T184" time="177.303" type="appl" />
         <tli id="T185" time="178.184" type="appl" />
         <tli id="T186" time="179.3975586006932" />
         <tli id="T187" time="180.0" type="appl" />
         <tli id="T188" time="180.503" type="appl" />
         <tli id="T189" time="181.006" type="appl" />
         <tli id="T190" time="181.509" type="appl" />
         <tli id="T191" time="182.012" type="appl" />
         <tli id="T192" time="182.97750988157662" />
         <tli id="T193" time="183.624" type="appl" />
         <tli id="T194" time="184.347" type="appl" />
         <tli id="T195" time="185.071" type="appl" />
         <tli id="T196" time="185.795" type="appl" />
         <tli id="T197" time="186.519" type="appl" />
         <tli id="T198" time="187.242" type="appl" />
         <tli id="T199" time="188.5907668247867" />
         <tli id="T200" time="190.133" type="appl" />
         <tli id="T201" time="191.533" type="appl" />
         <tli id="T202" time="192.933" type="appl" />
         <tli id="T203" time="194.333" type="appl" />
         <tli id="T204" time="195.733" type="appl" />
         <tli id="T205" time="197.133" type="appl" />
         <tli id="T206" time="198.533" type="appl" />
         <tli id="T207" time="199.933" type="appl" />
         <tli id="T208" time="200.592" type="appl" />
         <tli id="T209" time="201.251" type="appl" />
         <tli id="T210" time="201.91" type="appl" />
         <tli id="T211" time="202.569" type="appl" />
         <tli id="T212" time="204.087" type="appl" />
         <tli id="T213" time="205.593" type="appl" />
         <tli id="T214" time="207.099" type="appl" />
         <tli id="T215" time="208.605" type="appl" />
         <tli id="T216" time="209.356" type="appl" />
         <tli id="T217" time="210.01" type="appl" />
         <tli id="T218" time="210.663" type="appl" />
         <tli id="T219" time="211.59" type="appl" />
         <tli id="T220" time="212.378" type="appl" />
         <tli id="T221" time="213.165" type="appl" />
         <tli id="T222" time="213.759" type="appl" />
         <tli id="T223" time="214.353" type="appl" />
         <tli id="T224" time="214.916" type="appl" />
         <tli id="T225" time="215.42" type="appl" />
         <tli id="T226" time="215.923" type="appl" />
         <tli id="T227" time="216.426" type="appl" />
         <tli id="T228" time="216.93" type="appl" />
         <tli id="T229" time="217.433" type="appl" />
         <tli id="T230" time="217.937" type="appl" />
         <tli id="T231" time="218.44" type="appl" />
         <tli id="T232" time="219.798" type="appl" />
         <tli id="T233" time="221.045" type="appl" />
         <tli id="T234" time="222.291" type="appl" />
         <tli id="T235" time="223.537" type="appl" />
         <tli id="T236" time="224.783" type="appl" />
         <tli id="T237" time="226.03" type="appl" />
         <tli id="T238" time="227.276" type="appl" />
         <tli id="T239" time="227.888" type="appl" />
         <tli id="T240" time="228.5" type="appl" />
         <tli id="T241" time="229.112" type="appl" />
         <tli id="T242" time="229.724" type="appl" />
         <tli id="T243" time="230.618" type="appl" />
         <tli id="T244" time="231.512" type="appl" />
         <tli id="T245" time="232.407" type="appl" />
         <tli id="T246" time="233.70348622467117" />
         <tli id="T247" time="236.2967842661305" />
         <tli id="T248" time="237.123" type="appl" />
         <tli id="T249" time="237.91" type="appl" />
         <tli id="T250" time="238.697" type="appl" />
         <tli id="T251" time="239.484" type="appl" />
         <tli id="T252" time="240.27" type="appl" />
         <tli id="T253" time="241.057" type="appl" />
         <tli id="T254" time="241.844" type="appl" />
         <tli id="T255" time="242.631" type="appl" />
         <tli id="T256" time="243.729" type="appl" />
         <tli id="T257" time="243.79" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T256" id="Seg_0" n="sc" s="T0">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nagurgöʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">poʔto</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">tibi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">i</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ular</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">tibi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">i</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">tospak</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">tibi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_35" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Tospak</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_39" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">ug-</ts>
                  <nts id="Seg_42" n="HIAT:ip">)</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">uge</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">tojirbi</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">Uja</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">amnaʔbə</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_61" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">Oroma</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">amnaʔbə</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_70" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">Dĭn</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">net</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">bar</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">münörlaʔbə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_83" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">ujubə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">s-</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">ujubə</ts>
                  <nts id="Seg_92" n="HIAT:ip">)</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">münörbi</ts>
                  <nts id="Seg_96" n="HIAT:ip">,</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">ujut</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">ĭzembi</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_106" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">Dĭ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">šonəga</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">i</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">dʼorlaʔbə</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_121" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">A</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_126" n="HIAT:w" s="T33">šide</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">ular</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">da</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">poʔto</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">iʔbəlaʔbə</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_144" n="HIAT:u" s="T38">
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">Ĭmbi</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">dʼorlaʔbəl</ts>
                  <nts id="Seg_151" n="HIAT:ip">?</nts>
                  <nts id="Seg_152" n="HIAT:ip">"</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_155" n="HIAT:u" s="T40">
                  <nts id="Seg_156" n="HIAT:ip">"</nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">Da</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">măna</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">münörbi</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">dĭ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">ne</ts>
                  <nts id="Seg_171" n="HIAT:ip">"</nts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_175" n="HIAT:u" s="T45">
                  <nts id="Seg_176" n="HIAT:ip">"</nts>
                  <nts id="Seg_177" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">Ambiz-</ts>
                  <nts id="Seg_180" n="HIAT:ip">)</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">Ĭmbi</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">tăn</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">abial</ts>
                  <nts id="Seg_190" n="HIAT:ip">?</nts>
                  <nts id="Seg_191" n="HIAT:ip">"</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_194" n="HIAT:u" s="T49">
                  <nts id="Seg_195" n="HIAT:ip">"</nts>
                  <ts e="T50" id="Seg_197" n="HIAT:w" s="T49">Da</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_200" n="HIAT:w" s="T50">örömem</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">ambiam</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_208" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_210" n="HIAT:w" s="T52">Dĭgəttə</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_213" n="HIAT:w" s="T53">dĭ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_216" n="HIAT:w" s="T54">mănlia:</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_218" n="HIAT:ip">"</nts>
                  <ts e="T56" id="Seg_220" n="HIAT:w" s="T55">Tuj</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_223" n="HIAT:w" s="T56">bar</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">oromam</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">ambial</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_233" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_235" n="HIAT:w" s="T59">Nada</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_238" n="HIAT:w" s="T60">măna</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">poʔto</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">băʔsittə</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_248" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_250" n="HIAT:w" s="T63">Măn</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_253" n="HIAT:w" s="T64">tuganbə</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_256" n="HIAT:w" s="T65">šoluʔjəʔ</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_260" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_262" n="HIAT:w" s="T66">Nada</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_265" n="HIAT:w" s="T67">amorzittə</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">mĭzittə</ts>
                  <nts id="Seg_269" n="HIAT:ip">"</nts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_273" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_275" n="HIAT:w" s="T69">Dĭgəttə</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_278" n="HIAT:w" s="T70">dĭzeŋ</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_281" n="HIAT:w" s="T71">măliaʔi:</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_283" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_285" n="HIAT:w" s="T72">Kanžəbaʔ</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_288" n="HIAT:w" s="T73">nagurgöʔ</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_292" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_294" n="HIAT:w" s="T74">Šaʔlaːmbaʔ</ts>
                  <nts id="Seg_295" n="HIAT:ip">"</nts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_299" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_301" n="HIAT:w" s="T75">Dĭgəttə</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_304" n="HIAT:w" s="T76">onʼiʔ</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_307" n="HIAT:w" s="T77">măndə</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_310" n="HIAT:w" s="T78">ulardə:</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_312" n="HIAT:ip">"</nts>
                  <ts e="T80" id="Seg_314" n="HIAT:w" s="T79">Toʔnardə</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_317" n="HIAT:w" s="T80">uluziʔ</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_320" n="HIAT:w" s="T81">varotaʔi</ts>
                  <nts id="Seg_321" n="HIAT:ip">"</nts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_325" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_327" n="HIAT:w" s="T82">Dĭ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_330" n="HIAT:w" s="T83">toʔnarluʔpi</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_334" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_336" n="HIAT:w" s="T84">Ej</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_338" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_340" n="HIAT:w" s="T85">mo=</ts>
                  <nts id="Seg_341" n="HIAT:ip">)</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_344" n="HIAT:w" s="T86">ej</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_347" n="HIAT:w" s="T87">karluʔpiʔi</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_351" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_353" n="HIAT:w" s="T88">Dĭgəttə</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_356" n="HIAT:w" s="T89">poʔto</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_359" n="HIAT:w" s="T90">toʔnarluʔpi</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_362" n="HIAT:w" s="T91">bostə</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_365" n="HIAT:w" s="T92">uluziʔ</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_369" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_371" n="HIAT:w" s="T93">Varotaʔi</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_374" n="HIAT:w" s="T94">karluʔpiʔi</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_378" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_380" n="HIAT:w" s="T95">Dĭgəttə</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_383" n="HIAT:w" s="T96">dĭzeŋ</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_386" n="HIAT:w" s="T97">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_390" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_392" n="HIAT:w" s="T98">Nuʔməluʔpiʔi</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_396" n="HIAT:w" s="T99">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_400" n="HIAT:w" s="T100">dĭgəttə</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_403" n="HIAT:w" s="T101">šobiʔi</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_407" n="HIAT:w" s="T102">dĭn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_410" n="HIAT:w" s="T103">noʔ</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_413" n="HIAT:w" s="T104">iʔgö</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_416" n="HIAT:w" s="T105">nugaʔi</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_420" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_422" n="HIAT:w" s="T106">Nudʼi</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_425" n="HIAT:w" s="T107">molaːmbi</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_429" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_431" n="HIAT:w" s="T108">Nada</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_434" n="HIAT:w" s="T109">šaːsʼtə</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_438" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_440" n="HIAT:w" s="T110">Dĭgəttə</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_443" n="HIAT:w" s="T111">kĭrbi</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_446" n="HIAT:w" s="T112">kuba</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_449" n="HIAT:w" s="T113">sĭri</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_452" n="HIAT:w" s="T114">pagən</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_456" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_458" n="HIAT:w" s="T115">I</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_461" n="HIAT:w" s="T116">dĭʔnə</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_464" n="HIAT:w" s="T117">amnuʔtə</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_467" n="HIAT:w" s="T118">šerbi</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_471" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_473" n="HIAT:w" s="T119">Dĭgəttə</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_476" n="HIAT:w" s="T120">măndə:</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_478" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_480" n="HIAT:w" s="T121">toʔnar=</ts>
                  <nts id="Seg_481" n="HIAT:ip">)</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_484" n="HIAT:w" s="T122">toʔnarlaʔ</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_488" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_490" n="HIAT:w" s="T123">Onʼiʔ</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_493" n="HIAT:w" s="T124">onʼiʔtə</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_496" n="HIAT:w" s="T125">dĭzeŋ</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_499" n="HIAT:w" s="T126">toʔnarluʔpiʔi</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_503" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_505" n="HIAT:w" s="T127">I</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_507" n="HIAT:ip">(</nts>
                  <ts e="T129" id="Seg_509" n="HIAT:w" s="T128">šud-</ts>
                  <nts id="Seg_510" n="HIAT:ip">)</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_513" n="HIAT:w" s="T129">šü</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_516" n="HIAT:w" s="T130">nanʼiluʔpi</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_520" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_522" n="HIAT:w" s="T131">Dĭgəttə</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_525" n="HIAT:w" s="T132">amnəlbiʔi</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_528" n="HIAT:w" s="T133">i</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_531" n="HIAT:w" s="T134">ejümneʔpiʔi</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_535" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_537" n="HIAT:w" s="T135">Kabarləj</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_541" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_543" n="HIAT:w" s="T136">Dĭzeŋ</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_546" n="HIAT:w" s="T137">ejümnubiʔi</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_550" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_552" n="HIAT:w" s="T138">Dĭgəttə</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_555" n="HIAT:w" s="T139">urgaːba</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_558" n="HIAT:w" s="T140">šobi</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_562" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_564" n="HIAT:w" s="T141">Amnəlbi</ts>
                  <nts id="Seg_565" n="HIAT:ip">,</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_568" n="HIAT:w" s="T142">tože</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_571" n="HIAT:w" s="T143">ejümnubi</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_575" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_577" n="HIAT:w" s="T144">Dĭgəttə</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_580" n="HIAT:w" s="T145">kambi</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_583" n="HIAT:w" s="T146">noʔdə</ts>
                  <nts id="Seg_584" n="HIAT:ip">,</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_587" n="HIAT:w" s="T147">dĭn</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_590" n="HIAT:w" s="T148">iʔbəbi</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_594" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_596" n="HIAT:w" s="T149">A</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_599" n="HIAT:w" s="T150">tospak</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_602" n="HIAT:w" s="T151">nʼuʔnən</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_605" n="HIAT:w" s="T152">amnəbi</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_609" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_611" n="HIAT:w" s="T153">A</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_613" n="HIAT:ip">(</nts>
                  <ts e="T155" id="Seg_615" n="HIAT:w" s="T154">š-</ts>
                  <nts id="Seg_616" n="HIAT:ip">)</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_619" n="HIAT:w" s="T155">dĭ</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_622" n="HIAT:w" s="T156">ular</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_625" n="HIAT:w" s="T157">i</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_628" n="HIAT:w" s="T158">poʔto</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_631" n="HIAT:w" s="T159">šügən</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_634" n="HIAT:w" s="T160">iʔbəlaʔbi</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_638" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_640" n="HIAT:w" s="T161">Dĭgəttə</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_643" n="HIAT:w" s="T162">šobiʔi</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_646" n="HIAT:w" s="T163">volkəʔi</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_650" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_652" n="HIAT:w" s="T164">Onʼiʔ</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_655" n="HIAT:w" s="T165">sĭre</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_658" n="HIAT:w" s="T166">volk</ts>
                  <nts id="Seg_659" n="HIAT:ip">.</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_662" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_664" n="HIAT:w" s="T167">Dĭgəttə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_667" n="HIAT:w" s="T168">tospak</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_670" n="HIAT:w" s="T169">măndə</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_674" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_676" n="HIAT:w" s="T170">Miʔ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_679" n="HIAT:w" s="T171">kagam</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_682" n="HIAT:w" s="T172">iʔbəlaʔbə</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_684" n="HIAT:ip">(</nts>
                  <ts e="T174" id="Seg_686" n="HIAT:w" s="T173">dĭn</ts>
                  <nts id="Seg_687" n="HIAT:ip">)</nts>
                  <nts id="Seg_688" n="HIAT:ip">,</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_690" n="HIAT:ip">(</nts>
                  <ts e="T175" id="Seg_692" n="HIAT:w" s="T174">stogugən</ts>
                  <nts id="Seg_693" n="HIAT:ip">)</nts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_697" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_699" n="HIAT:w" s="T175">Dĭm</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_701" n="HIAT:ip">(</nts>
                  <ts e="T177" id="Seg_703" n="HIAT:w" s="T176">ejümneʔbəʔ</ts>
                  <nts id="Seg_704" n="HIAT:ip">)</nts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_708" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_710" n="HIAT:w" s="T177">Dĭgəttə</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_713" n="HIAT:w" s="T178">dĭ</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_716" n="HIAT:w" s="T179">urgaːba</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_719" n="HIAT:w" s="T180">onʼiʔ</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_722" n="HIAT:w" s="T181">ibi</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_725" n="HIAT:w" s="T182">volkdə</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_727" n="HIAT:ip">(</nts>
                  <ts e="T184" id="Seg_729" n="HIAT:w" s="T183">i</ts>
                  <nts id="Seg_730" n="HIAT:ip">)</nts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_734" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_736" n="HIAT:w" s="T184">Onʼiʔ</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_739" n="HIAT:w" s="T185">udandə</ts>
                  <nts id="Seg_740" n="HIAT:ip">.</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_743" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_745" n="HIAT:w" s="T186">I</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_748" n="HIAT:w" s="T187">onʼiʔ</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_751" n="HIAT:w" s="T188">ibi</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_754" n="HIAT:w" s="T189">baška</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_757" n="HIAT:w" s="T190">udandə</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_760" n="HIAT:w" s="T191">dĭ</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_764" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_766" n="HIAT:w" s="T192">Dĭzeŋ</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_769" n="HIAT:w" s="T193">bar</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_772" n="HIAT:w" s="T194">kak</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_775" n="HIAT:w" s="T195">neröjluʔpiʔi</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_778" n="HIAT:w" s="T196">i</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_780" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_782" n="HIAT:w" s="T197">ujmə-</ts>
                  <nts id="Seg_783" n="HIAT:ip">)</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_786" n="HIAT:w" s="T198">üʔməluʔpiʔi</ts>
                  <nts id="Seg_787" n="HIAT:ip">.</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_790" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_792" n="HIAT:w" s="T199">Dĭgəttə</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_795" n="HIAT:w" s="T200">ular</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_798" n="HIAT:w" s="T201">i</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_801" n="HIAT:w" s="T202">poʔto</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_804" n="HIAT:w" s="T203">i</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_807" n="HIAT:w" s="T204">tospak</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_810" n="HIAT:w" s="T205">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_813" n="HIAT:w" s="T206">bazoʔ</ts>
                  <nts id="Seg_814" n="HIAT:ip">.</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_817" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_819" n="HIAT:w" s="T207">Dĭgəttə</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_822" n="HIAT:w" s="T208">dĭn</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_825" n="HIAT:w" s="T209">bazoʔ</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_828" n="HIAT:w" s="T210">volkəʔjə</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_832" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_834" n="HIAT:w" s="T211">Dĭ</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_837" n="HIAT:w" s="T212">tospak</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_840" n="HIAT:w" s="T213">sʼabi</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_843" n="HIAT:w" s="T214">panə</ts>
                  <nts id="Seg_844" n="HIAT:ip">.</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_847" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_849" n="HIAT:w" s="T215">A</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_852" n="HIAT:w" s="T216">dĭzeŋ</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_855" n="HIAT:w" s="T217">edöbiʔi</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_859" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_861" n="HIAT:w" s="T218">Dĭgəttə</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_864" n="HIAT:w" s="T219">dĭ</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_867" n="HIAT:w" s="T220">măndə:</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_869" n="HIAT:ip">"</nts>
                  <ts e="T222" id="Seg_871" n="HIAT:w" s="T221">Măn</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_874" n="HIAT:w" s="T222">kagam</ts>
                  <nts id="Seg_875" n="HIAT:ip">.</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_878" n="HIAT:u" s="T223">
                  <nts id="Seg_879" n="HIAT:ip">(</nts>
                  <ts e="T224" id="Seg_881" n="HIAT:w" s="T223">Ši-</ts>
                  <nts id="Seg_882" n="HIAT:ip">)</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_885" n="HIAT:w" s="T224">Măn</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_888" n="HIAT:w" s="T225">šidem</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_891" n="HIAT:w" s="T226">ilim</ts>
                  <nts id="Seg_892" n="HIAT:ip">,</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_895" n="HIAT:w" s="T227">a</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_898" n="HIAT:w" s="T228">tănan</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_901" n="HIAT:w" s="T229">kumən</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_904" n="HIAT:w" s="T230">kereʔ</ts>
                  <nts id="Seg_905" n="HIAT:ip">?</nts>
                  <nts id="Seg_906" n="HIAT:ip">"</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_909" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_911" n="HIAT:w" s="T231">Dĭgəttə</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_914" n="HIAT:w" s="T232">poʔto</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_917" n="HIAT:w" s="T233">saʔməluʔpi</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_920" n="HIAT:w" s="T234">da</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_923" n="HIAT:w" s="T235">volkəndə</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_925" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_927" n="HIAT:w" s="T236">naʔamzum</ts>
                  <nts id="Seg_928" n="HIAT:ip">)</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_931" n="HIAT:w" s="T237">amnosʼtə</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_935" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_937" n="HIAT:w" s="T238">Dĭgəttə</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_940" n="HIAT:w" s="T239">dĭ</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_942" n="HIAT:ip">(</nts>
                  <ts e="T241" id="Seg_944" n="HIAT:w" s="T240">nen-</ts>
                  <nts id="Seg_945" n="HIAT:ip">)</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_948" n="HIAT:w" s="T241">nereʔluʔpi</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_952" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_954" n="HIAT:w" s="T242">Dĭzeŋ</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_957" n="HIAT:w" s="T243">nereʔluʔpiʔi</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_960" n="HIAT:w" s="T244">i</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_963" n="HIAT:w" s="T245">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_967" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_969" n="HIAT:w" s="T246">Kabarləj</ts>
                  <nts id="Seg_970" n="HIAT:ip">.</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_973" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_975" n="HIAT:w" s="T247">Dĭzeŋ</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_978" n="HIAT:w" s="T248">dĭgəttə</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_981" n="HIAT:w" s="T249">kambiʔi</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_984" n="HIAT:w" s="T250">bostə</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_986" n="HIAT:ip">(</nts>
                  <ts e="T252" id="Seg_988" n="HIAT:w" s="T251">a-</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_991" n="HIAT:w" s="T252">aktʼit-</ts>
                  <nts id="Seg_992" n="HIAT:ip">)</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_995" n="HIAT:w" s="T253">aktʼizaŋdə</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_998" n="HIAT:w" s="T254">nagurgöʔ</ts>
                  <nts id="Seg_999" n="HIAT:ip">.</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1002" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_1004" n="HIAT:w" s="T255">Kabarləj</ts>
                  <nts id="Seg_1005" n="HIAT:ip">.</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T256" id="Seg_1007" n="sc" s="T0">
               <ts e="T1" id="Seg_1009" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_1011" n="e" s="T1">nagurgöʔ </ts>
               <ts e="T3" id="Seg_1013" n="e" s="T2">poʔto </ts>
               <ts e="T4" id="Seg_1015" n="e" s="T3">tibi </ts>
               <ts e="T5" id="Seg_1017" n="e" s="T4">i </ts>
               <ts e="T6" id="Seg_1019" n="e" s="T5">ular </ts>
               <ts e="T7" id="Seg_1021" n="e" s="T6">tibi </ts>
               <ts e="T8" id="Seg_1023" n="e" s="T7">i </ts>
               <ts e="T9" id="Seg_1025" n="e" s="T8">tospak </ts>
               <ts e="T10" id="Seg_1027" n="e" s="T9">tibi. </ts>
               <ts e="T11" id="Seg_1029" n="e" s="T10">Tospak </ts>
               <ts e="T12" id="Seg_1031" n="e" s="T11">(ug-) </ts>
               <ts e="T13" id="Seg_1033" n="e" s="T12">uge </ts>
               <ts e="T14" id="Seg_1035" n="e" s="T13">tojirbi. </ts>
               <ts e="T15" id="Seg_1037" n="e" s="T14">Uja </ts>
               <ts e="T16" id="Seg_1039" n="e" s="T15">amnaʔbə. </ts>
               <ts e="T17" id="Seg_1041" n="e" s="T16">Oroma </ts>
               <ts e="T18" id="Seg_1043" n="e" s="T17">amnaʔbə. </ts>
               <ts e="T19" id="Seg_1045" n="e" s="T18">Dĭn </ts>
               <ts e="T20" id="Seg_1047" n="e" s="T19">net </ts>
               <ts e="T21" id="Seg_1049" n="e" s="T20">bar </ts>
               <ts e="T22" id="Seg_1051" n="e" s="T21">münörlaʔbə </ts>
               <ts e="T23" id="Seg_1053" n="e" s="T22">(ujubə </ts>
               <ts e="T24" id="Seg_1055" n="e" s="T23">s- </ts>
               <ts e="T25" id="Seg_1057" n="e" s="T24">ujubə) </ts>
               <ts e="T26" id="Seg_1059" n="e" s="T25">münörbi, </ts>
               <ts e="T27" id="Seg_1061" n="e" s="T26">ujut </ts>
               <ts e="T28" id="Seg_1063" n="e" s="T27">ĭzembi. </ts>
               <ts e="T29" id="Seg_1065" n="e" s="T28">Dĭ </ts>
               <ts e="T30" id="Seg_1067" n="e" s="T29">šonəga </ts>
               <ts e="T31" id="Seg_1069" n="e" s="T30">i </ts>
               <ts e="T32" id="Seg_1071" n="e" s="T31">dʼorlaʔbə. </ts>
               <ts e="T33" id="Seg_1073" n="e" s="T32">A </ts>
               <ts e="T34" id="Seg_1075" n="e" s="T33">šide, </ts>
               <ts e="T35" id="Seg_1077" n="e" s="T34">ular </ts>
               <ts e="T36" id="Seg_1079" n="e" s="T35">da </ts>
               <ts e="T37" id="Seg_1081" n="e" s="T36">poʔto, </ts>
               <ts e="T38" id="Seg_1083" n="e" s="T37">iʔbəlaʔbə. </ts>
               <ts e="T39" id="Seg_1085" n="e" s="T38">"Ĭmbi </ts>
               <ts e="T40" id="Seg_1087" n="e" s="T39">dʼorlaʔbəl?" </ts>
               <ts e="T41" id="Seg_1089" n="e" s="T40">"Da </ts>
               <ts e="T42" id="Seg_1091" n="e" s="T41">măna </ts>
               <ts e="T43" id="Seg_1093" n="e" s="T42">münörbi </ts>
               <ts e="T44" id="Seg_1095" n="e" s="T43">dĭ </ts>
               <ts e="T45" id="Seg_1097" n="e" s="T44">ne". </ts>
               <ts e="T46" id="Seg_1099" n="e" s="T45">"(Ambiz-) </ts>
               <ts e="T47" id="Seg_1101" n="e" s="T46">Ĭmbi </ts>
               <ts e="T48" id="Seg_1103" n="e" s="T47">tăn </ts>
               <ts e="T49" id="Seg_1105" n="e" s="T48">abial?" </ts>
               <ts e="T50" id="Seg_1107" n="e" s="T49">"Da </ts>
               <ts e="T51" id="Seg_1109" n="e" s="T50">örömem </ts>
               <ts e="T52" id="Seg_1111" n="e" s="T51">ambiam." </ts>
               <ts e="T53" id="Seg_1113" n="e" s="T52">Dĭgəttə </ts>
               <ts e="T54" id="Seg_1115" n="e" s="T53">dĭ </ts>
               <ts e="T55" id="Seg_1117" n="e" s="T54">mănlia: </ts>
               <ts e="T56" id="Seg_1119" n="e" s="T55">"Tuj </ts>
               <ts e="T57" id="Seg_1121" n="e" s="T56">bar </ts>
               <ts e="T58" id="Seg_1123" n="e" s="T57">oromam </ts>
               <ts e="T59" id="Seg_1125" n="e" s="T58">ambial. </ts>
               <ts e="T60" id="Seg_1127" n="e" s="T59">Nada </ts>
               <ts e="T61" id="Seg_1129" n="e" s="T60">măna </ts>
               <ts e="T62" id="Seg_1131" n="e" s="T61">poʔto </ts>
               <ts e="T63" id="Seg_1133" n="e" s="T62">băʔsittə. </ts>
               <ts e="T64" id="Seg_1135" n="e" s="T63">Măn </ts>
               <ts e="T65" id="Seg_1137" n="e" s="T64">tuganbə </ts>
               <ts e="T66" id="Seg_1139" n="e" s="T65">šoluʔjəʔ. </ts>
               <ts e="T67" id="Seg_1141" n="e" s="T66">Nada </ts>
               <ts e="T68" id="Seg_1143" n="e" s="T67">amorzittə </ts>
               <ts e="T69" id="Seg_1145" n="e" s="T68">mĭzittə". </ts>
               <ts e="T70" id="Seg_1147" n="e" s="T69">Dĭgəttə </ts>
               <ts e="T71" id="Seg_1149" n="e" s="T70">dĭzeŋ </ts>
               <ts e="T72" id="Seg_1151" n="e" s="T71">măliaʔi: </ts>
               <ts e="T73" id="Seg_1153" n="e" s="T72">"Kanžəbaʔ </ts>
               <ts e="T74" id="Seg_1155" n="e" s="T73">nagurgöʔ. </ts>
               <ts e="T75" id="Seg_1157" n="e" s="T74">Šaʔlaːmbaʔ". </ts>
               <ts e="T76" id="Seg_1159" n="e" s="T75">Dĭgəttə </ts>
               <ts e="T77" id="Seg_1161" n="e" s="T76">onʼiʔ </ts>
               <ts e="T78" id="Seg_1163" n="e" s="T77">măndə </ts>
               <ts e="T79" id="Seg_1165" n="e" s="T78">ulardə: </ts>
               <ts e="T80" id="Seg_1167" n="e" s="T79">"Toʔnardə </ts>
               <ts e="T81" id="Seg_1169" n="e" s="T80">uluziʔ </ts>
               <ts e="T82" id="Seg_1171" n="e" s="T81">varotaʔi". </ts>
               <ts e="T83" id="Seg_1173" n="e" s="T82">Dĭ </ts>
               <ts e="T84" id="Seg_1175" n="e" s="T83">toʔnarluʔpi. </ts>
               <ts e="T85" id="Seg_1177" n="e" s="T84">Ej </ts>
               <ts e="T86" id="Seg_1179" n="e" s="T85">(mo=) </ts>
               <ts e="T87" id="Seg_1181" n="e" s="T86">ej </ts>
               <ts e="T88" id="Seg_1183" n="e" s="T87">karluʔpiʔi. </ts>
               <ts e="T89" id="Seg_1185" n="e" s="T88">Dĭgəttə </ts>
               <ts e="T90" id="Seg_1187" n="e" s="T89">poʔto </ts>
               <ts e="T91" id="Seg_1189" n="e" s="T90">toʔnarluʔpi </ts>
               <ts e="T92" id="Seg_1191" n="e" s="T91">bostə </ts>
               <ts e="T93" id="Seg_1193" n="e" s="T92">uluziʔ. </ts>
               <ts e="T94" id="Seg_1195" n="e" s="T93">Varotaʔi </ts>
               <ts e="T95" id="Seg_1197" n="e" s="T94">karluʔpiʔi. </ts>
               <ts e="T96" id="Seg_1199" n="e" s="T95">Dĭgəttə </ts>
               <ts e="T97" id="Seg_1201" n="e" s="T96">dĭzeŋ </ts>
               <ts e="T98" id="Seg_1203" n="e" s="T97">nuʔməluʔpiʔi. </ts>
               <ts e="T99" id="Seg_1205" n="e" s="T98">Nuʔməluʔpiʔi, </ts>
               <ts e="T100" id="Seg_1207" n="e" s="T99">nuʔməluʔpiʔi, </ts>
               <ts e="T101" id="Seg_1209" n="e" s="T100">dĭgəttə </ts>
               <ts e="T102" id="Seg_1211" n="e" s="T101">šobiʔi, </ts>
               <ts e="T103" id="Seg_1213" n="e" s="T102">dĭn </ts>
               <ts e="T104" id="Seg_1215" n="e" s="T103">noʔ </ts>
               <ts e="T105" id="Seg_1217" n="e" s="T104">iʔgö </ts>
               <ts e="T106" id="Seg_1219" n="e" s="T105">nugaʔi. </ts>
               <ts e="T107" id="Seg_1221" n="e" s="T106">Nudʼi </ts>
               <ts e="T108" id="Seg_1223" n="e" s="T107">molaːmbi. </ts>
               <ts e="T109" id="Seg_1225" n="e" s="T108">Nada </ts>
               <ts e="T110" id="Seg_1227" n="e" s="T109">šaːsʼtə. </ts>
               <ts e="T111" id="Seg_1229" n="e" s="T110">Dĭgəttə </ts>
               <ts e="T112" id="Seg_1231" n="e" s="T111">kĭrbi </ts>
               <ts e="T113" id="Seg_1233" n="e" s="T112">kuba </ts>
               <ts e="T114" id="Seg_1235" n="e" s="T113">sĭri </ts>
               <ts e="T115" id="Seg_1237" n="e" s="T114">pagən. </ts>
               <ts e="T116" id="Seg_1239" n="e" s="T115">I </ts>
               <ts e="T117" id="Seg_1241" n="e" s="T116">dĭʔnə </ts>
               <ts e="T118" id="Seg_1243" n="e" s="T117">amnuʔtə </ts>
               <ts e="T119" id="Seg_1245" n="e" s="T118">šerbi. </ts>
               <ts e="T120" id="Seg_1247" n="e" s="T119">Dĭgəttə </ts>
               <ts e="T121" id="Seg_1249" n="e" s="T120">măndə: </ts>
               <ts e="T122" id="Seg_1251" n="e" s="T121">(toʔnar=) </ts>
               <ts e="T123" id="Seg_1253" n="e" s="T122">toʔnarlaʔ. </ts>
               <ts e="T124" id="Seg_1255" n="e" s="T123">Onʼiʔ </ts>
               <ts e="T125" id="Seg_1257" n="e" s="T124">onʼiʔtə </ts>
               <ts e="T126" id="Seg_1259" n="e" s="T125">dĭzeŋ </ts>
               <ts e="T127" id="Seg_1261" n="e" s="T126">toʔnarluʔpiʔi. </ts>
               <ts e="T128" id="Seg_1263" n="e" s="T127">I </ts>
               <ts e="T129" id="Seg_1265" n="e" s="T128">(šud-) </ts>
               <ts e="T130" id="Seg_1267" n="e" s="T129">šü </ts>
               <ts e="T131" id="Seg_1269" n="e" s="T130">nanʼiluʔpi. </ts>
               <ts e="T132" id="Seg_1271" n="e" s="T131">Dĭgəttə </ts>
               <ts e="T133" id="Seg_1273" n="e" s="T132">amnəlbiʔi </ts>
               <ts e="T134" id="Seg_1275" n="e" s="T133">i </ts>
               <ts e="T135" id="Seg_1277" n="e" s="T134">ejümneʔpiʔi. </ts>
               <ts e="T136" id="Seg_1279" n="e" s="T135">Kabarləj. </ts>
               <ts e="T137" id="Seg_1281" n="e" s="T136">Dĭzeŋ </ts>
               <ts e="T138" id="Seg_1283" n="e" s="T137">ejümnubiʔi. </ts>
               <ts e="T139" id="Seg_1285" n="e" s="T138">Dĭgəttə </ts>
               <ts e="T140" id="Seg_1287" n="e" s="T139">urgaːba </ts>
               <ts e="T141" id="Seg_1289" n="e" s="T140">šobi. </ts>
               <ts e="T142" id="Seg_1291" n="e" s="T141">Amnəlbi, </ts>
               <ts e="T143" id="Seg_1293" n="e" s="T142">tože </ts>
               <ts e="T144" id="Seg_1295" n="e" s="T143">ejümnubi. </ts>
               <ts e="T145" id="Seg_1297" n="e" s="T144">Dĭgəttə </ts>
               <ts e="T146" id="Seg_1299" n="e" s="T145">kambi </ts>
               <ts e="T147" id="Seg_1301" n="e" s="T146">noʔdə, </ts>
               <ts e="T148" id="Seg_1303" n="e" s="T147">dĭn </ts>
               <ts e="T149" id="Seg_1305" n="e" s="T148">iʔbəbi. </ts>
               <ts e="T150" id="Seg_1307" n="e" s="T149">A </ts>
               <ts e="T151" id="Seg_1309" n="e" s="T150">tospak </ts>
               <ts e="T152" id="Seg_1311" n="e" s="T151">nʼuʔnən </ts>
               <ts e="T153" id="Seg_1313" n="e" s="T152">amnəbi. </ts>
               <ts e="T154" id="Seg_1315" n="e" s="T153">A </ts>
               <ts e="T155" id="Seg_1317" n="e" s="T154">(š-) </ts>
               <ts e="T156" id="Seg_1319" n="e" s="T155">dĭ </ts>
               <ts e="T157" id="Seg_1321" n="e" s="T156">ular </ts>
               <ts e="T158" id="Seg_1323" n="e" s="T157">i </ts>
               <ts e="T159" id="Seg_1325" n="e" s="T158">poʔto </ts>
               <ts e="T160" id="Seg_1327" n="e" s="T159">šügən </ts>
               <ts e="T161" id="Seg_1329" n="e" s="T160">iʔbəlaʔbi. </ts>
               <ts e="T162" id="Seg_1331" n="e" s="T161">Dĭgəttə </ts>
               <ts e="T163" id="Seg_1333" n="e" s="T162">šobiʔi </ts>
               <ts e="T164" id="Seg_1335" n="e" s="T163">volkəʔi. </ts>
               <ts e="T165" id="Seg_1337" n="e" s="T164">Onʼiʔ </ts>
               <ts e="T166" id="Seg_1339" n="e" s="T165">sĭre </ts>
               <ts e="T167" id="Seg_1341" n="e" s="T166">volk. </ts>
               <ts e="T168" id="Seg_1343" n="e" s="T167">Dĭgəttə </ts>
               <ts e="T169" id="Seg_1345" n="e" s="T168">tospak </ts>
               <ts e="T170" id="Seg_1347" n="e" s="T169">măndə. </ts>
               <ts e="T171" id="Seg_1349" n="e" s="T170">Miʔ </ts>
               <ts e="T172" id="Seg_1351" n="e" s="T171">kagam </ts>
               <ts e="T173" id="Seg_1353" n="e" s="T172">iʔbəlaʔbə </ts>
               <ts e="T174" id="Seg_1355" n="e" s="T173">(dĭn), </ts>
               <ts e="T175" id="Seg_1357" n="e" s="T174">(stogugən). </ts>
               <ts e="T176" id="Seg_1359" n="e" s="T175">Dĭm </ts>
               <ts e="T177" id="Seg_1361" n="e" s="T176">(ejümneʔbəʔ). </ts>
               <ts e="T178" id="Seg_1363" n="e" s="T177">Dĭgəttə </ts>
               <ts e="T179" id="Seg_1365" n="e" s="T178">dĭ </ts>
               <ts e="T180" id="Seg_1367" n="e" s="T179">urgaːba </ts>
               <ts e="T181" id="Seg_1369" n="e" s="T180">onʼiʔ </ts>
               <ts e="T182" id="Seg_1371" n="e" s="T181">ibi </ts>
               <ts e="T183" id="Seg_1373" n="e" s="T182">volkdə </ts>
               <ts e="T184" id="Seg_1375" n="e" s="T183">(i). </ts>
               <ts e="T185" id="Seg_1377" n="e" s="T184">Onʼiʔ </ts>
               <ts e="T186" id="Seg_1379" n="e" s="T185">udandə. </ts>
               <ts e="T187" id="Seg_1381" n="e" s="T186">I </ts>
               <ts e="T188" id="Seg_1383" n="e" s="T187">onʼiʔ </ts>
               <ts e="T189" id="Seg_1385" n="e" s="T188">ibi </ts>
               <ts e="T190" id="Seg_1387" n="e" s="T189">baška </ts>
               <ts e="T191" id="Seg_1389" n="e" s="T190">udandə </ts>
               <ts e="T192" id="Seg_1391" n="e" s="T191">dĭ. </ts>
               <ts e="T193" id="Seg_1393" n="e" s="T192">Dĭzeŋ </ts>
               <ts e="T194" id="Seg_1395" n="e" s="T193">bar </ts>
               <ts e="T195" id="Seg_1397" n="e" s="T194">kak </ts>
               <ts e="T196" id="Seg_1399" n="e" s="T195">neröjluʔpiʔi </ts>
               <ts e="T197" id="Seg_1401" n="e" s="T196">i </ts>
               <ts e="T198" id="Seg_1403" n="e" s="T197">(ujmə-) </ts>
               <ts e="T199" id="Seg_1405" n="e" s="T198">üʔməluʔpiʔi. </ts>
               <ts e="T200" id="Seg_1407" n="e" s="T199">Dĭgəttə </ts>
               <ts e="T201" id="Seg_1409" n="e" s="T200">ular </ts>
               <ts e="T202" id="Seg_1411" n="e" s="T201">i </ts>
               <ts e="T203" id="Seg_1413" n="e" s="T202">poʔto </ts>
               <ts e="T204" id="Seg_1415" n="e" s="T203">i </ts>
               <ts e="T205" id="Seg_1417" n="e" s="T204">tospak </ts>
               <ts e="T206" id="Seg_1419" n="e" s="T205">nuʔməluʔpiʔi </ts>
               <ts e="T207" id="Seg_1421" n="e" s="T206">bazoʔ. </ts>
               <ts e="T208" id="Seg_1423" n="e" s="T207">Dĭgəttə </ts>
               <ts e="T209" id="Seg_1425" n="e" s="T208">dĭn </ts>
               <ts e="T210" id="Seg_1427" n="e" s="T209">bazoʔ </ts>
               <ts e="T211" id="Seg_1429" n="e" s="T210">volkəʔjə. </ts>
               <ts e="T212" id="Seg_1431" n="e" s="T211">Dĭ </ts>
               <ts e="T213" id="Seg_1433" n="e" s="T212">tospak </ts>
               <ts e="T214" id="Seg_1435" n="e" s="T213">sʼabi </ts>
               <ts e="T215" id="Seg_1437" n="e" s="T214">panə. </ts>
               <ts e="T216" id="Seg_1439" n="e" s="T215">A </ts>
               <ts e="T217" id="Seg_1441" n="e" s="T216">dĭzeŋ </ts>
               <ts e="T218" id="Seg_1443" n="e" s="T217">edöbiʔi. </ts>
               <ts e="T219" id="Seg_1445" n="e" s="T218">Dĭgəttə </ts>
               <ts e="T220" id="Seg_1447" n="e" s="T219">dĭ </ts>
               <ts e="T221" id="Seg_1449" n="e" s="T220">măndə: </ts>
               <ts e="T222" id="Seg_1451" n="e" s="T221">"Măn </ts>
               <ts e="T223" id="Seg_1453" n="e" s="T222">kagam. </ts>
               <ts e="T224" id="Seg_1455" n="e" s="T223">(Ši-) </ts>
               <ts e="T225" id="Seg_1457" n="e" s="T224">Măn </ts>
               <ts e="T226" id="Seg_1459" n="e" s="T225">šidem </ts>
               <ts e="T227" id="Seg_1461" n="e" s="T226">ilim, </ts>
               <ts e="T228" id="Seg_1463" n="e" s="T227">a </ts>
               <ts e="T229" id="Seg_1465" n="e" s="T228">tănan </ts>
               <ts e="T230" id="Seg_1467" n="e" s="T229">kumən </ts>
               <ts e="T231" id="Seg_1469" n="e" s="T230">kereʔ?" </ts>
               <ts e="T232" id="Seg_1471" n="e" s="T231">Dĭgəttə </ts>
               <ts e="T233" id="Seg_1473" n="e" s="T232">poʔto </ts>
               <ts e="T234" id="Seg_1475" n="e" s="T233">saʔməluʔpi </ts>
               <ts e="T235" id="Seg_1477" n="e" s="T234">da </ts>
               <ts e="T236" id="Seg_1479" n="e" s="T235">volkəndə </ts>
               <ts e="T237" id="Seg_1481" n="e" s="T236">(naʔamzum) </ts>
               <ts e="T238" id="Seg_1483" n="e" s="T237">amnosʼtə. </ts>
               <ts e="T239" id="Seg_1485" n="e" s="T238">Dĭgəttə </ts>
               <ts e="T240" id="Seg_1487" n="e" s="T239">dĭ </ts>
               <ts e="T241" id="Seg_1489" n="e" s="T240">(nen-) </ts>
               <ts e="T242" id="Seg_1491" n="e" s="T241">nereʔluʔpi. </ts>
               <ts e="T243" id="Seg_1493" n="e" s="T242">Dĭzeŋ </ts>
               <ts e="T244" id="Seg_1495" n="e" s="T243">nereʔluʔpiʔi </ts>
               <ts e="T245" id="Seg_1497" n="e" s="T244">i </ts>
               <ts e="T246" id="Seg_1499" n="e" s="T245">nuʔməluʔpiʔi. </ts>
               <ts e="T247" id="Seg_1501" n="e" s="T246">Kabarləj. </ts>
               <ts e="T248" id="Seg_1503" n="e" s="T247">Dĭzeŋ </ts>
               <ts e="T249" id="Seg_1505" n="e" s="T248">dĭgəttə </ts>
               <ts e="T250" id="Seg_1507" n="e" s="T249">kambiʔi </ts>
               <ts e="T251" id="Seg_1509" n="e" s="T250">bostə </ts>
               <ts e="T252" id="Seg_1511" n="e" s="T251">(a- </ts>
               <ts e="T253" id="Seg_1513" n="e" s="T252">aktʼit-) </ts>
               <ts e="T254" id="Seg_1515" n="e" s="T253">aktʼizaŋdə </ts>
               <ts e="T255" id="Seg_1517" n="e" s="T254">nagurgöʔ. </ts>
               <ts e="T256" id="Seg_1519" n="e" s="T255">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_1520" s="T0">PKZ_196X_GoatSheepCat_flk.001 (001)</ta>
            <ta e="T14" id="Seg_1521" s="T10">PKZ_196X_GoatSheepCat_flk.002 (002)</ta>
            <ta e="T16" id="Seg_1522" s="T14">PKZ_196X_GoatSheepCat_flk.003 (003)</ta>
            <ta e="T18" id="Seg_1523" s="T16">PKZ_196X_GoatSheepCat_flk.004 (004)</ta>
            <ta e="T28" id="Seg_1524" s="T18">PKZ_196X_GoatSheepCat_flk.005 (005)</ta>
            <ta e="T32" id="Seg_1525" s="T28">PKZ_196X_GoatSheepCat_flk.006 (006)</ta>
            <ta e="T38" id="Seg_1526" s="T32">PKZ_196X_GoatSheepCat_flk.007 (007)</ta>
            <ta e="T40" id="Seg_1527" s="T38">PKZ_196X_GoatSheepCat_flk.008 (008)</ta>
            <ta e="T45" id="Seg_1528" s="T40">PKZ_196X_GoatSheepCat_flk.009 (009)</ta>
            <ta e="T49" id="Seg_1529" s="T45">PKZ_196X_GoatSheepCat_flk.010 (010)</ta>
            <ta e="T52" id="Seg_1530" s="T49">PKZ_196X_GoatSheepCat_flk.011 (011)</ta>
            <ta e="T59" id="Seg_1531" s="T52">PKZ_196X_GoatSheepCat_flk.012 (012)</ta>
            <ta e="T63" id="Seg_1532" s="T59">PKZ_196X_GoatSheepCat_flk.013 (014)</ta>
            <ta e="T66" id="Seg_1533" s="T63">PKZ_196X_GoatSheepCat_flk.014 (015)</ta>
            <ta e="T69" id="Seg_1534" s="T66">PKZ_196X_GoatSheepCat_flk.015 (016)</ta>
            <ta e="T74" id="Seg_1535" s="T69">PKZ_196X_GoatSheepCat_flk.016 (017)</ta>
            <ta e="T75" id="Seg_1536" s="T74">PKZ_196X_GoatSheepCat_flk.017 (019)</ta>
            <ta e="T82" id="Seg_1537" s="T75">PKZ_196X_GoatSheepCat_flk.018 (020)</ta>
            <ta e="T84" id="Seg_1538" s="T82">PKZ_196X_GoatSheepCat_flk.019 (022)</ta>
            <ta e="T88" id="Seg_1539" s="T84">PKZ_196X_GoatSheepCat_flk.020 (023)</ta>
            <ta e="T93" id="Seg_1540" s="T88">PKZ_196X_GoatSheepCat_flk.021 (024)</ta>
            <ta e="T95" id="Seg_1541" s="T93">PKZ_196X_GoatSheepCat_flk.022 (025)</ta>
            <ta e="T98" id="Seg_1542" s="T95">PKZ_196X_GoatSheepCat_flk.023 (026)</ta>
            <ta e="T106" id="Seg_1543" s="T98">PKZ_196X_GoatSheepCat_flk.024 (027)</ta>
            <ta e="T108" id="Seg_1544" s="T106">PKZ_196X_GoatSheepCat_flk.025 (028)</ta>
            <ta e="T110" id="Seg_1545" s="T108">PKZ_196X_GoatSheepCat_flk.026 (029)</ta>
            <ta e="T115" id="Seg_1546" s="T110">PKZ_196X_GoatSheepCat_flk.027 (030)</ta>
            <ta e="T119" id="Seg_1547" s="T115">PKZ_196X_GoatSheepCat_flk.028 (031)</ta>
            <ta e="T123" id="Seg_1548" s="T119">PKZ_196X_GoatSheepCat_flk.029 (032)</ta>
            <ta e="T127" id="Seg_1549" s="T123">PKZ_196X_GoatSheepCat_flk.030 (033)</ta>
            <ta e="T131" id="Seg_1550" s="T127">PKZ_196X_GoatSheepCat_flk.031 (034)</ta>
            <ta e="T135" id="Seg_1551" s="T131">PKZ_196X_GoatSheepCat_flk.032 (035)</ta>
            <ta e="T136" id="Seg_1552" s="T135">PKZ_196X_GoatSheepCat_flk.033 (036)</ta>
            <ta e="T138" id="Seg_1553" s="T136">PKZ_196X_GoatSheepCat_flk.034 (037)</ta>
            <ta e="T141" id="Seg_1554" s="T138">PKZ_196X_GoatSheepCat_flk.035 (038)</ta>
            <ta e="T144" id="Seg_1555" s="T141">PKZ_196X_GoatSheepCat_flk.036 (039)</ta>
            <ta e="T149" id="Seg_1556" s="T144">PKZ_196X_GoatSheepCat_flk.037 (040)</ta>
            <ta e="T153" id="Seg_1557" s="T149">PKZ_196X_GoatSheepCat_flk.038 (041)</ta>
            <ta e="T161" id="Seg_1558" s="T153">PKZ_196X_GoatSheepCat_flk.039 (042)</ta>
            <ta e="T164" id="Seg_1559" s="T161">PKZ_196X_GoatSheepCat_flk.040 (043)</ta>
            <ta e="T167" id="Seg_1560" s="T164">PKZ_196X_GoatSheepCat_flk.041 (044)</ta>
            <ta e="T170" id="Seg_1561" s="T167">PKZ_196X_GoatSheepCat_flk.042 (045)</ta>
            <ta e="T175" id="Seg_1562" s="T170">PKZ_196X_GoatSheepCat_flk.043 (046)</ta>
            <ta e="T177" id="Seg_1563" s="T175">PKZ_196X_GoatSheepCat_flk.044 (047)</ta>
            <ta e="T184" id="Seg_1564" s="T177">PKZ_196X_GoatSheepCat_flk.045 (048)</ta>
            <ta e="T186" id="Seg_1565" s="T184">PKZ_196X_GoatSheepCat_flk.046 (049)</ta>
            <ta e="T192" id="Seg_1566" s="T186">PKZ_196X_GoatSheepCat_flk.047 (050)</ta>
            <ta e="T199" id="Seg_1567" s="T192">PKZ_196X_GoatSheepCat_flk.048 (051)</ta>
            <ta e="T207" id="Seg_1568" s="T199">PKZ_196X_GoatSheepCat_flk.049 (052)</ta>
            <ta e="T211" id="Seg_1569" s="T207">PKZ_196X_GoatSheepCat_flk.050 (053)</ta>
            <ta e="T215" id="Seg_1570" s="T211">PKZ_196X_GoatSheepCat_flk.051 (054)</ta>
            <ta e="T218" id="Seg_1571" s="T215">PKZ_196X_GoatSheepCat_flk.052 (055)</ta>
            <ta e="T223" id="Seg_1572" s="T218">PKZ_196X_GoatSheepCat_flk.053 (056)</ta>
            <ta e="T231" id="Seg_1573" s="T223">PKZ_196X_GoatSheepCat_flk.054 (058)</ta>
            <ta e="T238" id="Seg_1574" s="T231">PKZ_196X_GoatSheepCat_flk.055 (059)</ta>
            <ta e="T242" id="Seg_1575" s="T238">PKZ_196X_GoatSheepCat_flk.056 (060)</ta>
            <ta e="T246" id="Seg_1576" s="T242">PKZ_196X_GoatSheepCat_flk.057 (061)</ta>
            <ta e="T247" id="Seg_1577" s="T246">PKZ_196X_GoatSheepCat_flk.058 (062)</ta>
            <ta e="T255" id="Seg_1578" s="T247">PKZ_196X_GoatSheepCat_flk.059 (063)</ta>
            <ta e="T256" id="Seg_1579" s="T255">PKZ_196X_GoatSheepCat_flk.060 (064)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_1580" s="T0">Amnobiʔi nagurgöʔ poʔto tibi i ular tibi i tospak tibi. </ta>
            <ta e="T14" id="Seg_1581" s="T10">Tospak (ug-) uge tojirbi. </ta>
            <ta e="T16" id="Seg_1582" s="T14">Uja amnaʔbə. </ta>
            <ta e="T18" id="Seg_1583" s="T16">Oroma amnaʔbə. </ta>
            <ta e="T28" id="Seg_1584" s="T18">Dĭn net bar münörlaʔbə (ujubə s- ujubə) münörbi, ujut ĭzembi. </ta>
            <ta e="T32" id="Seg_1585" s="T28">Dĭ šonəga i dʼorlaʔbə. </ta>
            <ta e="T38" id="Seg_1586" s="T32">A šide, ular da poʔto, iʔbəlaʔbə. </ta>
            <ta e="T40" id="Seg_1587" s="T38">"Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T45" id="Seg_1588" s="T40">"Da măna münörbi dĭ ne". </ta>
            <ta e="T49" id="Seg_1589" s="T45">"(Ambiz-) Ĭmbi tăn abial?" </ta>
            <ta e="T52" id="Seg_1590" s="T49">"Da örömem ambiam." </ta>
            <ta e="T59" id="Seg_1591" s="T52">Dĭgəttə dĭ mănlia: "Tuj bar oromam ambial. </ta>
            <ta e="T63" id="Seg_1592" s="T59">Nada măna poʔto băʔsittə. </ta>
            <ta e="T66" id="Seg_1593" s="T63">Măn tuganbə šoluʔjəʔ. </ta>
            <ta e="T69" id="Seg_1594" s="T66">Nada amorzittə mĭzittə". </ta>
            <ta e="T74" id="Seg_1595" s="T69">Dĭgəttə dĭzeŋ măliaʔi: "Kanžəbaʔ nagurgöʔ. </ta>
            <ta e="T75" id="Seg_1596" s="T74">Šaʔlaːmbaʔ". </ta>
            <ta e="T82" id="Seg_1597" s="T75">Dĭgəttə onʼiʔ măndə ulardə: "Toʔnardə uluziʔ varotaʔi". </ta>
            <ta e="T84" id="Seg_1598" s="T82">Dĭ toʔnarluʔpi. </ta>
            <ta e="T88" id="Seg_1599" s="T84">Ej (mo=) ej karluʔpiʔi. </ta>
            <ta e="T93" id="Seg_1600" s="T88">Dĭgəttə poʔto toʔnarluʔpi bostə uluziʔ. </ta>
            <ta e="T95" id="Seg_1601" s="T93">Varotaʔi karluʔpiʔi. </ta>
            <ta e="T98" id="Seg_1602" s="T95">Dĭgəttə dĭzeŋ nuʔməluʔpiʔi. </ta>
            <ta e="T106" id="Seg_1603" s="T98">Nuʔməluʔpiʔi, nuʔməluʔpiʔi, dĭgəttə šobiʔi, dĭn noʔ iʔgö nugaʔi. </ta>
            <ta e="T108" id="Seg_1604" s="T106">Nudʼi molaːmbi. </ta>
            <ta e="T110" id="Seg_1605" s="T108">Nada šaːsʼtə. </ta>
            <ta e="T115" id="Seg_1606" s="T110">Dĭgəttə kĭrbi kuba sĭri pagən. </ta>
            <ta e="T119" id="Seg_1607" s="T115">I dĭʔnə amnuʔtə šerbi. </ta>
            <ta e="T123" id="Seg_1608" s="T119">Dĭgəttə măndə: (toʔnar=) toʔnarlaʔ. </ta>
            <ta e="T127" id="Seg_1609" s="T123">Onʼiʔ onʼiʔtə dĭzeŋ toʔnarluʔpiʔi. </ta>
            <ta e="T131" id="Seg_1610" s="T127">I (šud-) šü nanʼiluʔpi. </ta>
            <ta e="T135" id="Seg_1611" s="T131">Dĭgəttə amnəlbiʔi i ejümneʔpiʔi. </ta>
            <ta e="T136" id="Seg_1612" s="T135">Kabarləj. </ta>
            <ta e="T138" id="Seg_1613" s="T136">Dĭzeŋ ejümnubiʔi. </ta>
            <ta e="T141" id="Seg_1614" s="T138">Dĭgəttə urgaːba šobi. </ta>
            <ta e="T144" id="Seg_1615" s="T141">Amnəlbi, tože ejümnubi. </ta>
            <ta e="T149" id="Seg_1616" s="T144">Dĭgəttə kambi noʔdə, dĭn iʔbəbi. </ta>
            <ta e="T153" id="Seg_1617" s="T149">A tospak nʼuʔnən amnəbi. </ta>
            <ta e="T161" id="Seg_1618" s="T153">A (š-) dĭ ular i poʔto šügən iʔbəlaʔbi. </ta>
            <ta e="T164" id="Seg_1619" s="T161">Dĭgəttə šobiʔi volkəʔi. </ta>
            <ta e="T167" id="Seg_1620" s="T164">Onʼiʔ sĭre volk. </ta>
            <ta e="T170" id="Seg_1621" s="T167">Dĭgəttə tospak măndə. </ta>
            <ta e="T175" id="Seg_1622" s="T170">Miʔ kagam iʔbəlaʔbə (dĭn), (stogugən). </ta>
            <ta e="T177" id="Seg_1623" s="T175">Dĭm (ejümneʔbəʔ). </ta>
            <ta e="T184" id="Seg_1624" s="T177">Dĭgəttə dĭ urgaːba onʼiʔ ibi volkdə (i). </ta>
            <ta e="T186" id="Seg_1625" s="T184">Onʼiʔ udandə. </ta>
            <ta e="T192" id="Seg_1626" s="T186">I onʼiʔ ibi baška udandə dĭ. </ta>
            <ta e="T199" id="Seg_1627" s="T192">Dĭzeŋ bar kak neröjluʔpiʔi i (ujmə-) üʔməluʔpiʔi. </ta>
            <ta e="T207" id="Seg_1628" s="T199">Dĭgəttə ular i poʔto i tospak nuʔməluʔpiʔi bazoʔ. </ta>
            <ta e="T211" id="Seg_1629" s="T207">Dĭgəttə dĭn bazoʔ volkəʔjə. </ta>
            <ta e="T215" id="Seg_1630" s="T211">Dĭ tospak sʼabi panə. </ta>
            <ta e="T218" id="Seg_1631" s="T215">A dĭzeŋ edöbiʔi. </ta>
            <ta e="T223" id="Seg_1632" s="T218">Dĭgəttə dĭ măndə: "Măn kagam. </ta>
            <ta e="T231" id="Seg_1633" s="T223">(Ši-) Măn šidem ilim, a tănan kumən kereʔ?" </ta>
            <ta e="T238" id="Seg_1634" s="T231">Dĭgəttə poʔto saʔməluʔpi da volkəndə (naʔamzum) amnosʼtə. </ta>
            <ta e="T242" id="Seg_1635" s="T238">Dĭgəttə dĭ (nen-) nereʔluʔpi. </ta>
            <ta e="T246" id="Seg_1636" s="T242">Dĭzeŋ nereʔluʔpiʔi i nuʔməluʔpiʔi. </ta>
            <ta e="T247" id="Seg_1637" s="T246">Kabarləj. </ta>
            <ta e="T255" id="Seg_1638" s="T247">Dĭzeŋ dĭgəttə kambiʔi bostə (a- aktʼit-) aktʼizaŋdə nagurgöʔ. </ta>
            <ta e="T256" id="Seg_1639" s="T255">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1640" s="T0">amno-bi-ʔi</ta>
            <ta e="T2" id="Seg_1641" s="T1">nagur-göʔ</ta>
            <ta e="T3" id="Seg_1642" s="T2">poʔto</ta>
            <ta e="T4" id="Seg_1643" s="T3">tibi</ta>
            <ta e="T5" id="Seg_1644" s="T4">i</ta>
            <ta e="T6" id="Seg_1645" s="T5">ular</ta>
            <ta e="T7" id="Seg_1646" s="T6">tibi</ta>
            <ta e="T8" id="Seg_1647" s="T7">i</ta>
            <ta e="T9" id="Seg_1648" s="T8">tospak</ta>
            <ta e="T10" id="Seg_1649" s="T9">tibi</ta>
            <ta e="T11" id="Seg_1650" s="T10">tospak</ta>
            <ta e="T13" id="Seg_1651" s="T12">uge</ta>
            <ta e="T14" id="Seg_1652" s="T13">tojir-bi</ta>
            <ta e="T15" id="Seg_1653" s="T14">uja</ta>
            <ta e="T16" id="Seg_1654" s="T15">am-naʔbə</ta>
            <ta e="T17" id="Seg_1655" s="T16">oroma</ta>
            <ta e="T18" id="Seg_1656" s="T17">am-naʔbə</ta>
            <ta e="T19" id="Seg_1657" s="T18">dĭn</ta>
            <ta e="T20" id="Seg_1658" s="T19">ne-t</ta>
            <ta e="T21" id="Seg_1659" s="T20">bar</ta>
            <ta e="T22" id="Seg_1660" s="T21">münör-laʔbə</ta>
            <ta e="T23" id="Seg_1661" s="T22">uju-bə</ta>
            <ta e="T25" id="Seg_1662" s="T24">uju-bə</ta>
            <ta e="T26" id="Seg_1663" s="T25">münör-bi</ta>
            <ta e="T27" id="Seg_1664" s="T26">uju-t</ta>
            <ta e="T28" id="Seg_1665" s="T27">ĭzem-bi</ta>
            <ta e="T29" id="Seg_1666" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_1667" s="T29">šonə-ga</ta>
            <ta e="T31" id="Seg_1668" s="T30">i</ta>
            <ta e="T32" id="Seg_1669" s="T31">dʼor-laʔbə</ta>
            <ta e="T33" id="Seg_1670" s="T32">a</ta>
            <ta e="T34" id="Seg_1671" s="T33">šide</ta>
            <ta e="T35" id="Seg_1672" s="T34">ular</ta>
            <ta e="T36" id="Seg_1673" s="T35">da</ta>
            <ta e="T37" id="Seg_1674" s="T36">poʔto</ta>
            <ta e="T38" id="Seg_1675" s="T37">iʔbə-laʔbə</ta>
            <ta e="T39" id="Seg_1676" s="T38">ĭmbi</ta>
            <ta e="T40" id="Seg_1677" s="T39">dʼor-laʔbə-l</ta>
            <ta e="T41" id="Seg_1678" s="T40">da</ta>
            <ta e="T42" id="Seg_1679" s="T41">măna</ta>
            <ta e="T43" id="Seg_1680" s="T42">münör-bi</ta>
            <ta e="T44" id="Seg_1681" s="T43">dĭ</ta>
            <ta e="T45" id="Seg_1682" s="T44">ne</ta>
            <ta e="T47" id="Seg_1683" s="T46">ĭmbi</ta>
            <ta e="T48" id="Seg_1684" s="T47">tăn</ta>
            <ta e="T49" id="Seg_1685" s="T48">a-bia-l</ta>
            <ta e="T50" id="Seg_1686" s="T49">da</ta>
            <ta e="T51" id="Seg_1687" s="T50">öröme-m</ta>
            <ta e="T52" id="Seg_1688" s="T51">am-bia-m</ta>
            <ta e="T53" id="Seg_1689" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_1690" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1691" s="T54">măn-lia</ta>
            <ta e="T56" id="Seg_1692" s="T55">tuj</ta>
            <ta e="T57" id="Seg_1693" s="T56">bar</ta>
            <ta e="T58" id="Seg_1694" s="T57">oroma-m</ta>
            <ta e="T59" id="Seg_1695" s="T58">am-bia-l</ta>
            <ta e="T60" id="Seg_1696" s="T59">nada</ta>
            <ta e="T61" id="Seg_1697" s="T60">măna</ta>
            <ta e="T62" id="Seg_1698" s="T61">poʔto</ta>
            <ta e="T63" id="Seg_1699" s="T62">băʔ-sittə</ta>
            <ta e="T64" id="Seg_1700" s="T63">măn</ta>
            <ta e="T65" id="Seg_1701" s="T64">tugan-bə</ta>
            <ta e="T66" id="Seg_1702" s="T65">šo-luʔ-jəʔ</ta>
            <ta e="T67" id="Seg_1703" s="T66">nada</ta>
            <ta e="T68" id="Seg_1704" s="T67">amor-zittə</ta>
            <ta e="T69" id="Seg_1705" s="T68">mĭ-zittə</ta>
            <ta e="T70" id="Seg_1706" s="T69">dĭgəttə</ta>
            <ta e="T71" id="Seg_1707" s="T70">dĭ-zeŋ</ta>
            <ta e="T72" id="Seg_1708" s="T71">mă-lia-ʔi</ta>
            <ta e="T73" id="Seg_1709" s="T72">kan-žə-baʔ</ta>
            <ta e="T74" id="Seg_1710" s="T73">nagur-göʔ</ta>
            <ta e="T75" id="Seg_1711" s="T74">šaʔ-laːm-baʔ</ta>
            <ta e="T76" id="Seg_1712" s="T75">dĭgəttə</ta>
            <ta e="T77" id="Seg_1713" s="T76">onʼiʔ</ta>
            <ta e="T78" id="Seg_1714" s="T77">măn-də</ta>
            <ta e="T79" id="Seg_1715" s="T78">ular-də</ta>
            <ta e="T80" id="Seg_1716" s="T79">toʔ-nar-də</ta>
            <ta e="T81" id="Seg_1717" s="T80">ulu-ziʔ</ta>
            <ta e="T82" id="Seg_1718" s="T81">varota-ʔi</ta>
            <ta e="T83" id="Seg_1719" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1720" s="T83">toʔ-nar-luʔ-pi</ta>
            <ta e="T85" id="Seg_1721" s="T84">ej</ta>
            <ta e="T86" id="Seg_1722" s="T85">mo</ta>
            <ta e="T87" id="Seg_1723" s="T86">ej</ta>
            <ta e="T88" id="Seg_1724" s="T87">kar-luʔ-pi-ʔi</ta>
            <ta e="T89" id="Seg_1725" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_1726" s="T89">poʔto</ta>
            <ta e="T91" id="Seg_1727" s="T90">toʔ-nar-luʔ-pi</ta>
            <ta e="T92" id="Seg_1728" s="T91">bos-tə</ta>
            <ta e="T93" id="Seg_1729" s="T92">ulu-ziʔ</ta>
            <ta e="T94" id="Seg_1730" s="T93">varota-ʔi</ta>
            <ta e="T95" id="Seg_1731" s="T94">kar-luʔ-pi-ʔi</ta>
            <ta e="T96" id="Seg_1732" s="T95">dĭgəttə</ta>
            <ta e="T97" id="Seg_1733" s="T96">dĭ-zeŋ</ta>
            <ta e="T98" id="Seg_1734" s="T97">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T99" id="Seg_1735" s="T98">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T100" id="Seg_1736" s="T99">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T101" id="Seg_1737" s="T100">dĭgəttə</ta>
            <ta e="T102" id="Seg_1738" s="T101">šo-bi-ʔi</ta>
            <ta e="T103" id="Seg_1739" s="T102">dĭn</ta>
            <ta e="T104" id="Seg_1740" s="T103">noʔ</ta>
            <ta e="T105" id="Seg_1741" s="T104">iʔgö</ta>
            <ta e="T106" id="Seg_1742" s="T105">nu-ga-ʔi</ta>
            <ta e="T107" id="Seg_1743" s="T106">nudʼi</ta>
            <ta e="T108" id="Seg_1744" s="T107">mo-laːm-bi</ta>
            <ta e="T109" id="Seg_1745" s="T108">nada</ta>
            <ta e="T110" id="Seg_1746" s="T109">šaː-sʼtə</ta>
            <ta e="T111" id="Seg_1747" s="T110">dĭgəttə</ta>
            <ta e="T112" id="Seg_1748" s="T111">kĭr-bi</ta>
            <ta e="T113" id="Seg_1749" s="T112">kuba</ta>
            <ta e="T114" id="Seg_1750" s="T113">sĭri</ta>
            <ta e="T115" id="Seg_1751" s="T114">pa-gən</ta>
            <ta e="T116" id="Seg_1752" s="T115">i</ta>
            <ta e="T117" id="Seg_1753" s="T116">dĭʔ-nə</ta>
            <ta e="T118" id="Seg_1754" s="T117">amnu-ʔ-tə</ta>
            <ta e="T119" id="Seg_1755" s="T118">šer-bi</ta>
            <ta e="T120" id="Seg_1756" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_1757" s="T120">măn-də</ta>
            <ta e="T122" id="Seg_1758" s="T121">toʔ-nar</ta>
            <ta e="T123" id="Seg_1759" s="T122">toʔ-nar-laʔ</ta>
            <ta e="T124" id="Seg_1760" s="T123">onʼiʔ</ta>
            <ta e="T125" id="Seg_1761" s="T124">onʼiʔ-tə</ta>
            <ta e="T126" id="Seg_1762" s="T125">dĭ-zeŋ</ta>
            <ta e="T127" id="Seg_1763" s="T126">toʔ-nar-luʔ-pi-ʔi</ta>
            <ta e="T128" id="Seg_1764" s="T127">i</ta>
            <ta e="T130" id="Seg_1765" s="T129">šü</ta>
            <ta e="T131" id="Seg_1766" s="T130">nanʼi-luʔ-pi</ta>
            <ta e="T132" id="Seg_1767" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_1768" s="T132">amnəl-bi-ʔi</ta>
            <ta e="T134" id="Seg_1769" s="T133">i</ta>
            <ta e="T135" id="Seg_1770" s="T134">ejüm-neʔ-pi-ʔi</ta>
            <ta e="T136" id="Seg_1771" s="T135">kabarləj</ta>
            <ta e="T137" id="Seg_1772" s="T136">dĭ-zeŋ</ta>
            <ta e="T138" id="Seg_1773" s="T137">ejüm-nu-bi-ʔi</ta>
            <ta e="T139" id="Seg_1774" s="T138">dĭgəttə</ta>
            <ta e="T140" id="Seg_1775" s="T139">urgaːba</ta>
            <ta e="T141" id="Seg_1776" s="T140">šo-bi</ta>
            <ta e="T142" id="Seg_1777" s="T141">amnəl-bi</ta>
            <ta e="T143" id="Seg_1778" s="T142">tože</ta>
            <ta e="T144" id="Seg_1779" s="T143">ejüm-nu-bi</ta>
            <ta e="T145" id="Seg_1780" s="T144">dĭgəttə</ta>
            <ta e="T146" id="Seg_1781" s="T145">kam-bi</ta>
            <ta e="T147" id="Seg_1782" s="T146">noʔ-də</ta>
            <ta e="T148" id="Seg_1783" s="T147">dĭn</ta>
            <ta e="T149" id="Seg_1784" s="T148">iʔbə-bi</ta>
            <ta e="T150" id="Seg_1785" s="T149">a</ta>
            <ta e="T151" id="Seg_1786" s="T150">tospak</ta>
            <ta e="T152" id="Seg_1787" s="T151">nʼuʔnən</ta>
            <ta e="T153" id="Seg_1788" s="T152">amnə-bi</ta>
            <ta e="T154" id="Seg_1789" s="T153">a</ta>
            <ta e="T156" id="Seg_1790" s="T155">dĭ</ta>
            <ta e="T157" id="Seg_1791" s="T156">ular</ta>
            <ta e="T158" id="Seg_1792" s="T157">i</ta>
            <ta e="T159" id="Seg_1793" s="T158">poʔto</ta>
            <ta e="T160" id="Seg_1794" s="T159">šü-gən</ta>
            <ta e="T161" id="Seg_1795" s="T160">iʔbə-laʔ-bi</ta>
            <ta e="T162" id="Seg_1796" s="T161">dĭgəttə</ta>
            <ta e="T163" id="Seg_1797" s="T162">šo-bi-ʔi</ta>
            <ta e="T164" id="Seg_1798" s="T163">volkə-ʔi</ta>
            <ta e="T165" id="Seg_1799" s="T164">onʼiʔ</ta>
            <ta e="T166" id="Seg_1800" s="T165">sĭre</ta>
            <ta e="T167" id="Seg_1801" s="T166">volk</ta>
            <ta e="T168" id="Seg_1802" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_1803" s="T168">tospak</ta>
            <ta e="T170" id="Seg_1804" s="T169">măn-də</ta>
            <ta e="T171" id="Seg_1805" s="T170">miʔ</ta>
            <ta e="T172" id="Seg_1806" s="T171">kaga-m</ta>
            <ta e="T173" id="Seg_1807" s="T172">iʔbə-laʔbə</ta>
            <ta e="T174" id="Seg_1808" s="T173">dĭn</ta>
            <ta e="T175" id="Seg_1809" s="T174">stogu-gən</ta>
            <ta e="T176" id="Seg_1810" s="T175">dĭ-m</ta>
            <ta e="T177" id="Seg_1811" s="T176">ejüm-neʔbə-ʔ</ta>
            <ta e="T178" id="Seg_1812" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_1813" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_1814" s="T179">urgaːba</ta>
            <ta e="T181" id="Seg_1815" s="T180">onʼiʔ</ta>
            <ta e="T182" id="Seg_1816" s="T181">i-bi</ta>
            <ta e="T183" id="Seg_1817" s="T182">volk-də</ta>
            <ta e="T184" id="Seg_1818" s="T183">i</ta>
            <ta e="T185" id="Seg_1819" s="T184">onʼiʔ</ta>
            <ta e="T186" id="Seg_1820" s="T185">uda-ndə</ta>
            <ta e="T187" id="Seg_1821" s="T186">i</ta>
            <ta e="T188" id="Seg_1822" s="T187">onʼiʔ</ta>
            <ta e="T189" id="Seg_1823" s="T188">i-bi</ta>
            <ta e="T190" id="Seg_1824" s="T189">baška</ta>
            <ta e="T191" id="Seg_1825" s="T190">uda-ndə</ta>
            <ta e="T192" id="Seg_1826" s="T191">dĭ</ta>
            <ta e="T193" id="Seg_1827" s="T192">dĭ-zeŋ</ta>
            <ta e="T194" id="Seg_1828" s="T193">bar</ta>
            <ta e="T195" id="Seg_1829" s="T194">kak</ta>
            <ta e="T196" id="Seg_1830" s="T195">neröj-luʔ-pi-ʔi</ta>
            <ta e="T197" id="Seg_1831" s="T196">i</ta>
            <ta e="T199" id="Seg_1832" s="T198">üʔmə-luʔ-pi-ʔi</ta>
            <ta e="T200" id="Seg_1833" s="T199">dĭgəttə</ta>
            <ta e="T201" id="Seg_1834" s="T200">ular</ta>
            <ta e="T202" id="Seg_1835" s="T201">i</ta>
            <ta e="T203" id="Seg_1836" s="T202">poʔto</ta>
            <ta e="T204" id="Seg_1837" s="T203">i</ta>
            <ta e="T205" id="Seg_1838" s="T204">tospak</ta>
            <ta e="T206" id="Seg_1839" s="T205">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T207" id="Seg_1840" s="T206">bazoʔ</ta>
            <ta e="T208" id="Seg_1841" s="T207">dĭgəttə</ta>
            <ta e="T209" id="Seg_1842" s="T208">dĭn</ta>
            <ta e="T210" id="Seg_1843" s="T209">bazoʔ</ta>
            <ta e="T211" id="Seg_1844" s="T210">volkə-ʔjə</ta>
            <ta e="T212" id="Seg_1845" s="T211">dĭ</ta>
            <ta e="T213" id="Seg_1846" s="T212">tospak</ta>
            <ta e="T214" id="Seg_1847" s="T213">sʼa-bi</ta>
            <ta e="T215" id="Seg_1848" s="T214">pa-nə</ta>
            <ta e="T216" id="Seg_1849" s="T215">a</ta>
            <ta e="T217" id="Seg_1850" s="T216">dĭ-zeŋ</ta>
            <ta e="T218" id="Seg_1851" s="T217">edö-bi-ʔi</ta>
            <ta e="T219" id="Seg_1852" s="T218">dĭgəttə</ta>
            <ta e="T220" id="Seg_1853" s="T219">dĭ</ta>
            <ta e="T221" id="Seg_1854" s="T220">măn-də</ta>
            <ta e="T222" id="Seg_1855" s="T221">măn</ta>
            <ta e="T223" id="Seg_1856" s="T222">kaga-m</ta>
            <ta e="T225" id="Seg_1857" s="T224">măn</ta>
            <ta e="T226" id="Seg_1858" s="T225">šide-m</ta>
            <ta e="T227" id="Seg_1859" s="T226">i-li-m</ta>
            <ta e="T228" id="Seg_1860" s="T227">a</ta>
            <ta e="T229" id="Seg_1861" s="T228">tănan</ta>
            <ta e="T230" id="Seg_1862" s="T229">kumən</ta>
            <ta e="T231" id="Seg_1863" s="T230">kereʔ</ta>
            <ta e="T232" id="Seg_1864" s="T231">dĭgəttə</ta>
            <ta e="T233" id="Seg_1865" s="T232">poʔto</ta>
            <ta e="T234" id="Seg_1866" s="T233">saʔmə-luʔ-pi</ta>
            <ta e="T235" id="Seg_1867" s="T234">da</ta>
            <ta e="T236" id="Seg_1868" s="T235">volkə-ndə</ta>
            <ta e="T237" id="Seg_1869" s="T236">naʔamzum</ta>
            <ta e="T238" id="Seg_1870" s="T237">amno-sʼtə</ta>
            <ta e="T239" id="Seg_1871" s="T238">dĭgəttə</ta>
            <ta e="T240" id="Seg_1872" s="T239">dĭ</ta>
            <ta e="T242" id="Seg_1873" s="T241">nereʔ-luʔ-pi</ta>
            <ta e="T243" id="Seg_1874" s="T242">dĭ-zeŋ</ta>
            <ta e="T244" id="Seg_1875" s="T243">nereʔ-luʔ-pi-ʔi</ta>
            <ta e="T245" id="Seg_1876" s="T244">i</ta>
            <ta e="T246" id="Seg_1877" s="T245">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T247" id="Seg_1878" s="T246">kabarləj</ta>
            <ta e="T248" id="Seg_1879" s="T247">dĭ-zeŋ</ta>
            <ta e="T249" id="Seg_1880" s="T248">dĭgəttə</ta>
            <ta e="T250" id="Seg_1881" s="T249">kam-bi-ʔi</ta>
            <ta e="T251" id="Seg_1882" s="T250">bos-tə</ta>
            <ta e="T254" id="Seg_1883" s="T253">aktʼi-zaŋ-də</ta>
            <ta e="T255" id="Seg_1884" s="T254">nagur-göʔ</ta>
            <ta e="T256" id="Seg_1885" s="T255">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1886" s="T0">amnə-bi-jəʔ</ta>
            <ta e="T2" id="Seg_1887" s="T1">nagur-göʔ</ta>
            <ta e="T3" id="Seg_1888" s="T2">poʔto</ta>
            <ta e="T4" id="Seg_1889" s="T3">tibi</ta>
            <ta e="T5" id="Seg_1890" s="T4">i</ta>
            <ta e="T6" id="Seg_1891" s="T5">ular</ta>
            <ta e="T7" id="Seg_1892" s="T6">tibi</ta>
            <ta e="T8" id="Seg_1893" s="T7">i</ta>
            <ta e="T9" id="Seg_1894" s="T8">tospak</ta>
            <ta e="T10" id="Seg_1895" s="T9">tibi</ta>
            <ta e="T11" id="Seg_1896" s="T10">tospak</ta>
            <ta e="T13" id="Seg_1897" s="T12">üge</ta>
            <ta e="T14" id="Seg_1898" s="T13">tojar-bi</ta>
            <ta e="T15" id="Seg_1899" s="T14">uja</ta>
            <ta e="T16" id="Seg_1900" s="T15">am-laʔbə</ta>
            <ta e="T17" id="Seg_1901" s="T16">oroma</ta>
            <ta e="T18" id="Seg_1902" s="T17">am-laʔbə</ta>
            <ta e="T19" id="Seg_1903" s="T18">dĭn</ta>
            <ta e="T20" id="Seg_1904" s="T19">ne-t</ta>
            <ta e="T21" id="Seg_1905" s="T20">bar</ta>
            <ta e="T22" id="Seg_1906" s="T21">münör-laʔbə</ta>
            <ta e="T23" id="Seg_1907" s="T22">üjü-bə</ta>
            <ta e="T25" id="Seg_1908" s="T24">üjü-bə</ta>
            <ta e="T26" id="Seg_1909" s="T25">münör-bi</ta>
            <ta e="T27" id="Seg_1910" s="T26">üjü-t</ta>
            <ta e="T28" id="Seg_1911" s="T27">ĭzem-bi</ta>
            <ta e="T29" id="Seg_1912" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_1913" s="T29">šonə-gA</ta>
            <ta e="T31" id="Seg_1914" s="T30">i</ta>
            <ta e="T32" id="Seg_1915" s="T31">tʼor-laʔbə</ta>
            <ta e="T33" id="Seg_1916" s="T32">a</ta>
            <ta e="T34" id="Seg_1917" s="T33">šide</ta>
            <ta e="T35" id="Seg_1918" s="T34">ular</ta>
            <ta e="T36" id="Seg_1919" s="T35">da</ta>
            <ta e="T37" id="Seg_1920" s="T36">poʔto</ta>
            <ta e="T38" id="Seg_1921" s="T37">iʔbə-laʔbə</ta>
            <ta e="T39" id="Seg_1922" s="T38">ĭmbi</ta>
            <ta e="T40" id="Seg_1923" s="T39">tʼor-laʔbə-l</ta>
            <ta e="T41" id="Seg_1924" s="T40">da</ta>
            <ta e="T42" id="Seg_1925" s="T41">măna</ta>
            <ta e="T43" id="Seg_1926" s="T42">münör-bi</ta>
            <ta e="T44" id="Seg_1927" s="T43">dĭ</ta>
            <ta e="T45" id="Seg_1928" s="T44">ne</ta>
            <ta e="T47" id="Seg_1929" s="T46">ĭmbi</ta>
            <ta e="T48" id="Seg_1930" s="T47">tăn</ta>
            <ta e="T49" id="Seg_1931" s="T48">a-bi-l</ta>
            <ta e="T50" id="Seg_1932" s="T49">da</ta>
            <ta e="T51" id="Seg_1933" s="T50">oroma-m</ta>
            <ta e="T52" id="Seg_1934" s="T51">am-bi-m</ta>
            <ta e="T53" id="Seg_1935" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_1936" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1937" s="T54">măn-liA</ta>
            <ta e="T56" id="Seg_1938" s="T55">tüj</ta>
            <ta e="T57" id="Seg_1939" s="T56">bar</ta>
            <ta e="T58" id="Seg_1940" s="T57">oroma-m</ta>
            <ta e="T59" id="Seg_1941" s="T58">am-bi-l</ta>
            <ta e="T60" id="Seg_1942" s="T59">nadə</ta>
            <ta e="T61" id="Seg_1943" s="T60">măna</ta>
            <ta e="T62" id="Seg_1944" s="T61">poʔto</ta>
            <ta e="T63" id="Seg_1945" s="T62">băt-zittə</ta>
            <ta e="T64" id="Seg_1946" s="T63">măn</ta>
            <ta e="T65" id="Seg_1947" s="T64">tugan-m</ta>
            <ta e="T66" id="Seg_1948" s="T65">šo-luʔbdə-jəʔ</ta>
            <ta e="T67" id="Seg_1949" s="T66">nadə</ta>
            <ta e="T68" id="Seg_1950" s="T67">amor-zittə</ta>
            <ta e="T69" id="Seg_1951" s="T68">mĭ-zittə</ta>
            <ta e="T70" id="Seg_1952" s="T69">dĭgəttə</ta>
            <ta e="T71" id="Seg_1953" s="T70">dĭ-zAŋ</ta>
            <ta e="T72" id="Seg_1954" s="T71">măn-liA-jəʔ</ta>
            <ta e="T73" id="Seg_1955" s="T72">kan-žə-bAʔ</ta>
            <ta e="T74" id="Seg_1956" s="T73">nagur-göʔ</ta>
            <ta e="T75" id="Seg_1957" s="T74">šaʔ-laːm-bAʔ</ta>
            <ta e="T76" id="Seg_1958" s="T75">dĭgəttə</ta>
            <ta e="T77" id="Seg_1959" s="T76">onʼiʔ</ta>
            <ta e="T78" id="Seg_1960" s="T77">măn-ntə</ta>
            <ta e="T79" id="Seg_1961" s="T78">ular-də</ta>
            <ta e="T80" id="Seg_1962" s="T79">toʔbdə-nar-t</ta>
            <ta e="T81" id="Seg_1963" s="T80">ulu-ziʔ</ta>
            <ta e="T82" id="Seg_1964" s="T81">varota-jəʔ</ta>
            <ta e="T83" id="Seg_1965" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1966" s="T83">toʔbdə-nar-luʔbdə-bi</ta>
            <ta e="T85" id="Seg_1967" s="T84">ej</ta>
            <ta e="T86" id="Seg_1968" s="T85">mo</ta>
            <ta e="T87" id="Seg_1969" s="T86">ej</ta>
            <ta e="T88" id="Seg_1970" s="T87">kar-luʔbdə-bi-jəʔ</ta>
            <ta e="T89" id="Seg_1971" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_1972" s="T89">poʔto</ta>
            <ta e="T91" id="Seg_1973" s="T90">toʔbdə-nar-luʔbdə-bi</ta>
            <ta e="T92" id="Seg_1974" s="T91">bos-də</ta>
            <ta e="T93" id="Seg_1975" s="T92">ulu-ziʔ</ta>
            <ta e="T94" id="Seg_1976" s="T93">varota-jəʔ</ta>
            <ta e="T95" id="Seg_1977" s="T94">kar-luʔbdə-bi-jəʔ</ta>
            <ta e="T96" id="Seg_1978" s="T95">dĭgəttə</ta>
            <ta e="T97" id="Seg_1979" s="T96">dĭ-zAŋ</ta>
            <ta e="T98" id="Seg_1980" s="T97">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T99" id="Seg_1981" s="T98">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T100" id="Seg_1982" s="T99">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T101" id="Seg_1983" s="T100">dĭgəttə</ta>
            <ta e="T102" id="Seg_1984" s="T101">šo-bi-jəʔ</ta>
            <ta e="T103" id="Seg_1985" s="T102">dĭn</ta>
            <ta e="T104" id="Seg_1986" s="T103">noʔ</ta>
            <ta e="T105" id="Seg_1987" s="T104">iʔgö</ta>
            <ta e="T106" id="Seg_1988" s="T105">nu-gA-jəʔ</ta>
            <ta e="T107" id="Seg_1989" s="T106">nüdʼi</ta>
            <ta e="T108" id="Seg_1990" s="T107">mo-laːm-bi</ta>
            <ta e="T109" id="Seg_1991" s="T108">nadə</ta>
            <ta e="T110" id="Seg_1992" s="T109">šaː-zittə</ta>
            <ta e="T111" id="Seg_1993" s="T110">dĭgəttə</ta>
            <ta e="T112" id="Seg_1994" s="T111">kĭr-bi</ta>
            <ta e="T113" id="Seg_1995" s="T112">kuba</ta>
            <ta e="T114" id="Seg_1996" s="T113">sĭri</ta>
            <ta e="T115" id="Seg_1997" s="T114">pa-Kən</ta>
            <ta e="T116" id="Seg_1998" s="T115">i</ta>
            <ta e="T117" id="Seg_1999" s="T116">dĭ-Tə</ta>
            <ta e="T118" id="Seg_2000" s="T117">amnu-jəʔ-Tə</ta>
            <ta e="T119" id="Seg_2001" s="T118">šer-bi</ta>
            <ta e="T120" id="Seg_2002" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_2003" s="T120">măn-ntə</ta>
            <ta e="T122" id="Seg_2004" s="T121">toʔbdə-nar</ta>
            <ta e="T123" id="Seg_2005" s="T122">toʔbdə-nar-lAʔ</ta>
            <ta e="T124" id="Seg_2006" s="T123">onʼiʔ</ta>
            <ta e="T125" id="Seg_2007" s="T124">onʼiʔ-Tə</ta>
            <ta e="T126" id="Seg_2008" s="T125">dĭ-zAŋ</ta>
            <ta e="T127" id="Seg_2009" s="T126">toʔbdə-nar-luʔbdə-bi-jəʔ</ta>
            <ta e="T128" id="Seg_2010" s="T127">i</ta>
            <ta e="T130" id="Seg_2011" s="T129">šü</ta>
            <ta e="T131" id="Seg_2012" s="T130">nanʼi-luʔbdə-bi</ta>
            <ta e="T132" id="Seg_2013" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_2014" s="T132">amnəl-bi-jəʔ</ta>
            <ta e="T134" id="Seg_2015" s="T133">i</ta>
            <ta e="T135" id="Seg_2016" s="T134">ejüm-luʔbdə-bi-jəʔ</ta>
            <ta e="T136" id="Seg_2017" s="T135">kabarləj</ta>
            <ta e="T137" id="Seg_2018" s="T136">dĭ-zAŋ</ta>
            <ta e="T138" id="Seg_2019" s="T137">ejüm-luʔbdə-bi-jəʔ</ta>
            <ta e="T139" id="Seg_2020" s="T138">dĭgəttə</ta>
            <ta e="T140" id="Seg_2021" s="T139">urgaːba</ta>
            <ta e="T141" id="Seg_2022" s="T140">šo-bi</ta>
            <ta e="T142" id="Seg_2023" s="T141">amnəl-bi</ta>
            <ta e="T143" id="Seg_2024" s="T142">tože</ta>
            <ta e="T144" id="Seg_2025" s="T143">ejüm-luʔbdə-bi</ta>
            <ta e="T145" id="Seg_2026" s="T144">dĭgəttə</ta>
            <ta e="T146" id="Seg_2027" s="T145">kan-bi</ta>
            <ta e="T147" id="Seg_2028" s="T146">noʔ-Tə</ta>
            <ta e="T148" id="Seg_2029" s="T147">dĭn</ta>
            <ta e="T149" id="Seg_2030" s="T148">iʔbə-bi</ta>
            <ta e="T150" id="Seg_2031" s="T149">a</ta>
            <ta e="T151" id="Seg_2032" s="T150">tospak</ta>
            <ta e="T152" id="Seg_2033" s="T151">nʼuʔnun</ta>
            <ta e="T153" id="Seg_2034" s="T152">amnə-bi</ta>
            <ta e="T154" id="Seg_2035" s="T153">a</ta>
            <ta e="T156" id="Seg_2036" s="T155">dĭ</ta>
            <ta e="T157" id="Seg_2037" s="T156">ular</ta>
            <ta e="T158" id="Seg_2038" s="T157">i</ta>
            <ta e="T159" id="Seg_2039" s="T158">poʔto</ta>
            <ta e="T160" id="Seg_2040" s="T159">šü-Kən</ta>
            <ta e="T161" id="Seg_2041" s="T160">iʔbə-laʔbə-bi</ta>
            <ta e="T162" id="Seg_2042" s="T161">dĭgəttə</ta>
            <ta e="T163" id="Seg_2043" s="T162">šo-bi-jəʔ</ta>
            <ta e="T164" id="Seg_2044" s="T163">volk-jəʔ</ta>
            <ta e="T165" id="Seg_2045" s="T164">onʼiʔ</ta>
            <ta e="T166" id="Seg_2046" s="T165">sĭri</ta>
            <ta e="T167" id="Seg_2047" s="T166">volk</ta>
            <ta e="T168" id="Seg_2048" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_2049" s="T168">tospak</ta>
            <ta e="T170" id="Seg_2050" s="T169">măn-ntə</ta>
            <ta e="T171" id="Seg_2051" s="T170">miʔ</ta>
            <ta e="T172" id="Seg_2052" s="T171">kaga-m</ta>
            <ta e="T173" id="Seg_2053" s="T172">iʔbə-laʔbə</ta>
            <ta e="T174" id="Seg_2054" s="T173">dĭn</ta>
            <ta e="T175" id="Seg_2055" s="T174">stog-Kən</ta>
            <ta e="T176" id="Seg_2056" s="T175">dĭ-m</ta>
            <ta e="T177" id="Seg_2057" s="T176">ejüm-laʔbə-ʔ</ta>
            <ta e="T178" id="Seg_2058" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_2059" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_2060" s="T179">urgaːba</ta>
            <ta e="T181" id="Seg_2061" s="T180">onʼiʔ</ta>
            <ta e="T182" id="Seg_2062" s="T181">i-bi</ta>
            <ta e="T183" id="Seg_2063" s="T182">volk-də</ta>
            <ta e="T184" id="Seg_2064" s="T183">i</ta>
            <ta e="T185" id="Seg_2065" s="T184">onʼiʔ</ta>
            <ta e="T186" id="Seg_2066" s="T185">uda-gəndə</ta>
            <ta e="T187" id="Seg_2067" s="T186">i</ta>
            <ta e="T188" id="Seg_2068" s="T187">onʼiʔ</ta>
            <ta e="T189" id="Seg_2069" s="T188">i-bi</ta>
            <ta e="T190" id="Seg_2070" s="T189">baška</ta>
            <ta e="T191" id="Seg_2071" s="T190">uda-gəndə</ta>
            <ta e="T192" id="Seg_2072" s="T191">dĭ</ta>
            <ta e="T193" id="Seg_2073" s="T192">dĭ-zAŋ</ta>
            <ta e="T194" id="Seg_2074" s="T193">bar</ta>
            <ta e="T195" id="Seg_2075" s="T194">kak</ta>
            <ta e="T196" id="Seg_2076" s="T195">nerö-luʔbdə-bi-jəʔ</ta>
            <ta e="T197" id="Seg_2077" s="T196">i</ta>
            <ta e="T199" id="Seg_2078" s="T198">üʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T200" id="Seg_2079" s="T199">dĭgəttə</ta>
            <ta e="T201" id="Seg_2080" s="T200">ular</ta>
            <ta e="T202" id="Seg_2081" s="T201">i</ta>
            <ta e="T203" id="Seg_2082" s="T202">poʔto</ta>
            <ta e="T204" id="Seg_2083" s="T203">i</ta>
            <ta e="T205" id="Seg_2084" s="T204">tospak</ta>
            <ta e="T206" id="Seg_2085" s="T205">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T207" id="Seg_2086" s="T206">bazoʔ</ta>
            <ta e="T208" id="Seg_2087" s="T207">dĭgəttə</ta>
            <ta e="T209" id="Seg_2088" s="T208">dĭn</ta>
            <ta e="T210" id="Seg_2089" s="T209">bazoʔ</ta>
            <ta e="T211" id="Seg_2090" s="T210">volk-jəʔ</ta>
            <ta e="T212" id="Seg_2091" s="T211">dĭ</ta>
            <ta e="T213" id="Seg_2092" s="T212">tospak</ta>
            <ta e="T214" id="Seg_2093" s="T213">sʼa-bi</ta>
            <ta e="T215" id="Seg_2094" s="T214">pa-Tə</ta>
            <ta e="T216" id="Seg_2095" s="T215">a</ta>
            <ta e="T217" id="Seg_2096" s="T216">dĭ-zAŋ</ta>
            <ta e="T218" id="Seg_2097" s="T217">edö-bi-jəʔ</ta>
            <ta e="T219" id="Seg_2098" s="T218">dĭgəttə</ta>
            <ta e="T220" id="Seg_2099" s="T219">dĭ</ta>
            <ta e="T221" id="Seg_2100" s="T220">măn-ntə</ta>
            <ta e="T222" id="Seg_2101" s="T221">măn</ta>
            <ta e="T223" id="Seg_2102" s="T222">kaga-m</ta>
            <ta e="T225" id="Seg_2103" s="T224">măn</ta>
            <ta e="T226" id="Seg_2104" s="T225">šide-m</ta>
            <ta e="T227" id="Seg_2105" s="T226">i-lV-m</ta>
            <ta e="T228" id="Seg_2106" s="T227">a</ta>
            <ta e="T229" id="Seg_2107" s="T228">tănan</ta>
            <ta e="T230" id="Seg_2108" s="T229">kumən</ta>
            <ta e="T231" id="Seg_2109" s="T230">kereʔ</ta>
            <ta e="T232" id="Seg_2110" s="T231">dĭgəttə</ta>
            <ta e="T233" id="Seg_2111" s="T232">poʔto</ta>
            <ta e="T234" id="Seg_2112" s="T233">saʔmə-luʔbdə-bi</ta>
            <ta e="T235" id="Seg_2113" s="T234">da</ta>
            <ta e="T236" id="Seg_2114" s="T235">volk-gəndə</ta>
            <ta e="T237" id="Seg_2115" s="T236">naʔamzum</ta>
            <ta e="T238" id="Seg_2116" s="T237">amnə-zittə</ta>
            <ta e="T239" id="Seg_2117" s="T238">dĭgəttə</ta>
            <ta e="T240" id="Seg_2118" s="T239">dĭ</ta>
            <ta e="T242" id="Seg_2119" s="T241">nereʔ-luʔbdə-bi</ta>
            <ta e="T243" id="Seg_2120" s="T242">dĭ-zAŋ</ta>
            <ta e="T244" id="Seg_2121" s="T243">nereʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T245" id="Seg_2122" s="T244">i</ta>
            <ta e="T246" id="Seg_2123" s="T245">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T247" id="Seg_2124" s="T246">kabarləj</ta>
            <ta e="T248" id="Seg_2125" s="T247">dĭ-zAŋ</ta>
            <ta e="T249" id="Seg_2126" s="T248">dĭgəttə</ta>
            <ta e="T250" id="Seg_2127" s="T249">kan-bi-jəʔ</ta>
            <ta e="T251" id="Seg_2128" s="T250">bos-də</ta>
            <ta e="T254" id="Seg_2129" s="T253">aʔtʼi-zAŋ-Tə</ta>
            <ta e="T255" id="Seg_2130" s="T254">nagur-göʔ</ta>
            <ta e="T256" id="Seg_2131" s="T255">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2132" s="T0">live-PST-3PL</ta>
            <ta e="T2" id="Seg_2133" s="T1">three-COLL</ta>
            <ta e="T3" id="Seg_2134" s="T2">goat.[NOM.SG]</ta>
            <ta e="T4" id="Seg_2135" s="T3">man.[NOM.SG]</ta>
            <ta e="T5" id="Seg_2136" s="T4">and</ta>
            <ta e="T6" id="Seg_2137" s="T5">sheep.[NOM.SG]</ta>
            <ta e="T7" id="Seg_2138" s="T6">man.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2139" s="T7">and</ta>
            <ta e="T9" id="Seg_2140" s="T8">cat.[NOM.SG]</ta>
            <ta e="T10" id="Seg_2141" s="T9">man.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2142" s="T10">cat.[NOM.SG]</ta>
            <ta e="T13" id="Seg_2143" s="T12">always</ta>
            <ta e="T14" id="Seg_2144" s="T13">steal-PST.[3SG]</ta>
            <ta e="T15" id="Seg_2145" s="T14">meat.[NOM.SG]</ta>
            <ta e="T16" id="Seg_2146" s="T15">eat-DUR.[3SG]</ta>
            <ta e="T17" id="Seg_2147" s="T16">cream.[NOM.SG]</ta>
            <ta e="T18" id="Seg_2148" s="T17">eat-DUR.[3SG]</ta>
            <ta e="T19" id="Seg_2149" s="T18">there</ta>
            <ta e="T20" id="Seg_2150" s="T19">woman-NOM/GEN.3SG</ta>
            <ta e="T21" id="Seg_2151" s="T20">PTCL</ta>
            <ta e="T22" id="Seg_2152" s="T21">beat-DUR.[3SG]</ta>
            <ta e="T23" id="Seg_2153" s="T22">foot-ACC.3SG</ta>
            <ta e="T25" id="Seg_2154" s="T24">foot-ACC.3SG</ta>
            <ta e="T26" id="Seg_2155" s="T25">beat-PST.[3SG]</ta>
            <ta e="T27" id="Seg_2156" s="T26">foot-NOM/GEN.3SG</ta>
            <ta e="T28" id="Seg_2157" s="T27">hurt-PST.[3SG]</ta>
            <ta e="T29" id="Seg_2158" s="T28">this.[NOM.SG]</ta>
            <ta e="T30" id="Seg_2159" s="T29">come-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_2160" s="T30">and</ta>
            <ta e="T32" id="Seg_2161" s="T31">cry-DUR.[3SG]</ta>
            <ta e="T33" id="Seg_2162" s="T32">and</ta>
            <ta e="T34" id="Seg_2163" s="T33">two.[NOM.SG]</ta>
            <ta e="T35" id="Seg_2164" s="T34">sheep.[NOM.SG]</ta>
            <ta e="T36" id="Seg_2165" s="T35">and</ta>
            <ta e="T37" id="Seg_2166" s="T36">goat.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2167" s="T37">lie.down-DUR.[3SG]</ta>
            <ta e="T39" id="Seg_2168" s="T38">what.[NOM.SG]</ta>
            <ta e="T40" id="Seg_2169" s="T39">cry-DUR-2SG</ta>
            <ta e="T41" id="Seg_2170" s="T40">and</ta>
            <ta e="T42" id="Seg_2171" s="T41">I.ACC</ta>
            <ta e="T43" id="Seg_2172" s="T42">beat-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2173" s="T43">this.[NOM.SG]</ta>
            <ta e="T45" id="Seg_2174" s="T44">woman.[NOM.SG]</ta>
            <ta e="T47" id="Seg_2175" s="T46">what.[NOM.SG]</ta>
            <ta e="T48" id="Seg_2176" s="T47">you.NOM</ta>
            <ta e="T49" id="Seg_2177" s="T48">make-PST-2SG</ta>
            <ta e="T50" id="Seg_2178" s="T49">and</ta>
            <ta e="T51" id="Seg_2179" s="T50">cream-ACC</ta>
            <ta e="T52" id="Seg_2180" s="T51">eat-PST-1SG</ta>
            <ta e="T53" id="Seg_2181" s="T52">then</ta>
            <ta e="T54" id="Seg_2182" s="T53">this.[NOM.SG]</ta>
            <ta e="T55" id="Seg_2183" s="T54">say-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_2184" s="T55">now</ta>
            <ta e="T57" id="Seg_2185" s="T56">all</ta>
            <ta e="T58" id="Seg_2186" s="T57">cream-ACC</ta>
            <ta e="T59" id="Seg_2187" s="T58">eat-PST-2SG</ta>
            <ta e="T60" id="Seg_2188" s="T59">one.should</ta>
            <ta e="T61" id="Seg_2189" s="T60">I.LAT</ta>
            <ta e="T62" id="Seg_2190" s="T61">goat.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2191" s="T62">cut-INF.LAT</ta>
            <ta e="T64" id="Seg_2192" s="T63">I.GEN</ta>
            <ta e="T65" id="Seg_2193" s="T64">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T66" id="Seg_2194" s="T65">come-MOM-3PL</ta>
            <ta e="T67" id="Seg_2195" s="T66">one.should</ta>
            <ta e="T68" id="Seg_2196" s="T67">eat-INF.LAT</ta>
            <ta e="T69" id="Seg_2197" s="T68">give-INF.LAT</ta>
            <ta e="T70" id="Seg_2198" s="T69">then</ta>
            <ta e="T71" id="Seg_2199" s="T70">this-PL</ta>
            <ta e="T72" id="Seg_2200" s="T71">say-PRS-3PL</ta>
            <ta e="T73" id="Seg_2201" s="T72">go-OPT.DU/PL-1PL</ta>
            <ta e="T74" id="Seg_2202" s="T73">three-COLL</ta>
            <ta e="T75" id="Seg_2203" s="T74">hide-RES-1PL</ta>
            <ta e="T76" id="Seg_2204" s="T75">then</ta>
            <ta e="T77" id="Seg_2205" s="T76">one.[NOM.SG]</ta>
            <ta e="T78" id="Seg_2206" s="T77">say-IPFVZ.[3SG]</ta>
            <ta e="T79" id="Seg_2207" s="T78">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T80" id="Seg_2208" s="T79">hit-MULT-IMP.2SG.O</ta>
            <ta e="T81" id="Seg_2209" s="T80">head-INS</ta>
            <ta e="T82" id="Seg_2210" s="T81">gate-NOM/GEN/ACC.3PL</ta>
            <ta e="T83" id="Seg_2211" s="T82">this.[NOM.SG]</ta>
            <ta e="T84" id="Seg_2212" s="T83">hit-MULT-MOM-PST.[3SG]</ta>
            <ta e="T85" id="Seg_2213" s="T84">NEG</ta>
            <ta e="T86" id="Seg_2214" s="T85">can</ta>
            <ta e="T87" id="Seg_2215" s="T86">NEG</ta>
            <ta e="T88" id="Seg_2216" s="T87">open-MOM-PST-3PL</ta>
            <ta e="T89" id="Seg_2217" s="T88">then</ta>
            <ta e="T90" id="Seg_2218" s="T89">goat.[NOM.SG]</ta>
            <ta e="T91" id="Seg_2219" s="T90">hit-MULT-MOM-PST.[3SG]</ta>
            <ta e="T92" id="Seg_2220" s="T91">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T93" id="Seg_2221" s="T92">head-INS</ta>
            <ta e="T94" id="Seg_2222" s="T93">gate-NOM/GEN/ACC.3PL</ta>
            <ta e="T95" id="Seg_2223" s="T94">open-MOM-PST-3PL</ta>
            <ta e="T96" id="Seg_2224" s="T95">then</ta>
            <ta e="T97" id="Seg_2225" s="T96">this-PL</ta>
            <ta e="T98" id="Seg_2226" s="T97">run-MOM-PST-3PL</ta>
            <ta e="T99" id="Seg_2227" s="T98">run-MOM-PST-3PL</ta>
            <ta e="T100" id="Seg_2228" s="T99">run-MOM-PST-3PL</ta>
            <ta e="T101" id="Seg_2229" s="T100">then</ta>
            <ta e="T102" id="Seg_2230" s="T101">come-PST-3PL</ta>
            <ta e="T103" id="Seg_2231" s="T102">there</ta>
            <ta e="T104" id="Seg_2232" s="T103">grass.[NOM.SG]</ta>
            <ta e="T105" id="Seg_2233" s="T104">many</ta>
            <ta e="T106" id="Seg_2234" s="T105">stand-PRS-3PL</ta>
            <ta e="T107" id="Seg_2235" s="T106">evening.[NOM.SG]</ta>
            <ta e="T108" id="Seg_2236" s="T107">become-RES-PST.[3SG]</ta>
            <ta e="T109" id="Seg_2237" s="T108">one.should</ta>
            <ta e="T110" id="Seg_2238" s="T109">spend.night-INF.LAT</ta>
            <ta e="T111" id="Seg_2239" s="T110">then</ta>
            <ta e="T112" id="Seg_2240" s="T111">skin-PST.[3SG]</ta>
            <ta e="T113" id="Seg_2241" s="T112">skin.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2242" s="T113">white.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2243" s="T114">tree-LOC</ta>
            <ta e="T116" id="Seg_2244" s="T115">and</ta>
            <ta e="T117" id="Seg_2245" s="T116">this-LAT</ta>
            <ta e="T118" id="Seg_2246" s="T117">horn-PL-LAT</ta>
            <ta e="T119" id="Seg_2247" s="T118">dress-PST.[3SG]</ta>
            <ta e="T120" id="Seg_2248" s="T119">then</ta>
            <ta e="T121" id="Seg_2249" s="T120">say-IPFVZ.[3SG]</ta>
            <ta e="T122" id="Seg_2250" s="T121">hit-MULT</ta>
            <ta e="T123" id="Seg_2251" s="T122">hit-MULT-2PL</ta>
            <ta e="T124" id="Seg_2252" s="T123">one.[NOM.SG]</ta>
            <ta e="T125" id="Seg_2253" s="T124">one-LAT</ta>
            <ta e="T126" id="Seg_2254" s="T125">this-PL</ta>
            <ta e="T127" id="Seg_2255" s="T126">hit-MULT-MOM-PST-3PL</ta>
            <ta e="T128" id="Seg_2256" s="T127">and</ta>
            <ta e="T130" id="Seg_2257" s="T129">fire.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2258" s="T130">burn-MOM-PST.[3SG]</ta>
            <ta e="T132" id="Seg_2259" s="T131">then</ta>
            <ta e="T133" id="Seg_2260" s="T132">sit.down-PST-3PL</ta>
            <ta e="T134" id="Seg_2261" s="T133">and</ta>
            <ta e="T135" id="Seg_2262" s="T134">become.warm-MOM-PST-3PL</ta>
            <ta e="T136" id="Seg_2263" s="T135">enough</ta>
            <ta e="T137" id="Seg_2264" s="T136">this-PL</ta>
            <ta e="T138" id="Seg_2265" s="T137">become.warm-MOM-PST-3PL</ta>
            <ta e="T139" id="Seg_2266" s="T138">then</ta>
            <ta e="T140" id="Seg_2267" s="T139">bear.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2268" s="T140">come-PST.[3SG]</ta>
            <ta e="T142" id="Seg_2269" s="T141">sit.down-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2270" s="T142">also</ta>
            <ta e="T144" id="Seg_2271" s="T143">become.warm-MOM-PST.[3SG]</ta>
            <ta e="T145" id="Seg_2272" s="T144">then</ta>
            <ta e="T146" id="Seg_2273" s="T145">go-PST.[3SG]</ta>
            <ta e="T147" id="Seg_2274" s="T146">grass-LAT</ta>
            <ta e="T148" id="Seg_2275" s="T147">there</ta>
            <ta e="T149" id="Seg_2276" s="T148">lie.down-PST.[3SG]</ta>
            <ta e="T150" id="Seg_2277" s="T149">and</ta>
            <ta e="T151" id="Seg_2278" s="T150">cat.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2279" s="T151">%above</ta>
            <ta e="T153" id="Seg_2280" s="T152">sit.down-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2281" s="T153">and</ta>
            <ta e="T156" id="Seg_2282" s="T155">this.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2283" s="T156">sheep.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2284" s="T157">and</ta>
            <ta e="T159" id="Seg_2285" s="T158">goat.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2286" s="T159">fire-LOC</ta>
            <ta e="T161" id="Seg_2287" s="T160">lie.down-DUR-PST.[3SG]</ta>
            <ta e="T162" id="Seg_2288" s="T161">then</ta>
            <ta e="T163" id="Seg_2289" s="T162">come-PST-3PL</ta>
            <ta e="T164" id="Seg_2290" s="T163">wolf-PL</ta>
            <ta e="T165" id="Seg_2291" s="T164">single.[NOM.SG]</ta>
            <ta e="T166" id="Seg_2292" s="T165">white.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2293" s="T166">wolf.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2294" s="T167">then</ta>
            <ta e="T169" id="Seg_2295" s="T168">cat.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2296" s="T169">say-IPFVZ.[3SG]</ta>
            <ta e="T171" id="Seg_2297" s="T170">we.NOM</ta>
            <ta e="T172" id="Seg_2298" s="T171">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T173" id="Seg_2299" s="T172">lie.down-DUR.[3SG]</ta>
            <ta e="T174" id="Seg_2300" s="T173">there</ta>
            <ta e="T175" id="Seg_2301" s="T174">haystack-LOC</ta>
            <ta e="T176" id="Seg_2302" s="T175">this-ACC</ta>
            <ta e="T177" id="Seg_2303" s="T176">become.warm-DUR-IMP.2SG</ta>
            <ta e="T178" id="Seg_2304" s="T177">then</ta>
            <ta e="T179" id="Seg_2305" s="T178">this.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2306" s="T179">bear.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2307" s="T180">single.[NOM.SG]</ta>
            <ta e="T182" id="Seg_2308" s="T181">take-PST.[3SG]</ta>
            <ta e="T183" id="Seg_2309" s="T182">wolf-NOM/GEN/ACC.3SG</ta>
            <ta e="T184" id="Seg_2310" s="T183">and</ta>
            <ta e="T185" id="Seg_2311" s="T184">single.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2312" s="T185">hand-LAT/LOC.3SG</ta>
            <ta e="T187" id="Seg_2313" s="T186">and</ta>
            <ta e="T188" id="Seg_2314" s="T187">single.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2315" s="T188">take-PST.[3SG]</ta>
            <ta e="T190" id="Seg_2316" s="T189">another.[NOM.SG]</ta>
            <ta e="T191" id="Seg_2317" s="T190">hand-LAT/LOC.3SG</ta>
            <ta e="T192" id="Seg_2318" s="T191">this.[NOM.SG]</ta>
            <ta e="T193" id="Seg_2319" s="T192">this-PL</ta>
            <ta e="T194" id="Seg_2320" s="T193">all</ta>
            <ta e="T195" id="Seg_2321" s="T194">like</ta>
            <ta e="T196" id="Seg_2322" s="T195">get.frightened-MOM-PST-3PL</ta>
            <ta e="T197" id="Seg_2323" s="T196">and</ta>
            <ta e="T199" id="Seg_2324" s="T198">run-MOM-PST-3PL</ta>
            <ta e="T200" id="Seg_2325" s="T199">then</ta>
            <ta e="T201" id="Seg_2326" s="T200">sheep.[NOM.SG]</ta>
            <ta e="T202" id="Seg_2327" s="T201">and</ta>
            <ta e="T203" id="Seg_2328" s="T202">goat.[NOM.SG]</ta>
            <ta e="T204" id="Seg_2329" s="T203">and</ta>
            <ta e="T205" id="Seg_2330" s="T204">cat.[NOM.SG]</ta>
            <ta e="T206" id="Seg_2331" s="T205">run-MOM-PST-3PL</ta>
            <ta e="T207" id="Seg_2332" s="T206">again</ta>
            <ta e="T208" id="Seg_2333" s="T207">then</ta>
            <ta e="T209" id="Seg_2334" s="T208">there</ta>
            <ta e="T210" id="Seg_2335" s="T209">again</ta>
            <ta e="T211" id="Seg_2336" s="T210">wolf-PL</ta>
            <ta e="T212" id="Seg_2337" s="T211">this.[NOM.SG]</ta>
            <ta e="T213" id="Seg_2338" s="T212">cat.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2339" s="T213">climb-PST.[3SG]</ta>
            <ta e="T215" id="Seg_2340" s="T214">tree-LAT</ta>
            <ta e="T216" id="Seg_2341" s="T215">and</ta>
            <ta e="T217" id="Seg_2342" s="T216">this-PL</ta>
            <ta e="T218" id="Seg_2343" s="T217">hang-PST-3PL</ta>
            <ta e="T219" id="Seg_2344" s="T218">then</ta>
            <ta e="T220" id="Seg_2345" s="T219">this.[NOM.SG]</ta>
            <ta e="T221" id="Seg_2346" s="T220">say-IPFVZ.[3SG]</ta>
            <ta e="T222" id="Seg_2347" s="T221">I.NOM</ta>
            <ta e="T223" id="Seg_2348" s="T222">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T225" id="Seg_2349" s="T224">I.NOM</ta>
            <ta e="T226" id="Seg_2350" s="T225">two-NOM/GEN/ACC.1SG</ta>
            <ta e="T227" id="Seg_2351" s="T226">take-FUT-1SG</ta>
            <ta e="T228" id="Seg_2352" s="T227">and</ta>
            <ta e="T229" id="Seg_2353" s="T228">you.DAT</ta>
            <ta e="T230" id="Seg_2354" s="T229">how.much</ta>
            <ta e="T231" id="Seg_2355" s="T230">one.needs</ta>
            <ta e="T232" id="Seg_2356" s="T231">then</ta>
            <ta e="T233" id="Seg_2357" s="T232">goat.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2358" s="T233">fall-MOM-PST.[3SG]</ta>
            <ta e="T235" id="Seg_2359" s="T234">and</ta>
            <ta e="T236" id="Seg_2360" s="T235">wolf-LAT/LOC.3SG</ta>
            <ta e="T237" id="Seg_2361" s="T236">%%</ta>
            <ta e="T238" id="Seg_2362" s="T237">sit.down-INF.LAT</ta>
            <ta e="T239" id="Seg_2363" s="T238">then</ta>
            <ta e="T240" id="Seg_2364" s="T239">this.[NOM.SG]</ta>
            <ta e="T242" id="Seg_2365" s="T241">frighten-MOM-PST.[3SG]</ta>
            <ta e="T243" id="Seg_2366" s="T242">this-PL</ta>
            <ta e="T244" id="Seg_2367" s="T243">frighten-MOM-PST-3PL</ta>
            <ta e="T245" id="Seg_2368" s="T244">and</ta>
            <ta e="T246" id="Seg_2369" s="T245">run-MOM-PST-3PL</ta>
            <ta e="T247" id="Seg_2370" s="T246">enough</ta>
            <ta e="T248" id="Seg_2371" s="T247">this-PL</ta>
            <ta e="T249" id="Seg_2372" s="T248">then</ta>
            <ta e="T250" id="Seg_2373" s="T249">go-PST-3PL</ta>
            <ta e="T251" id="Seg_2374" s="T250">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T254" id="Seg_2375" s="T253">road-PL-LAT</ta>
            <ta e="T255" id="Seg_2376" s="T254">three-COLL</ta>
            <ta e="T256" id="Seg_2377" s="T255">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2378" s="T0">жить-PST-3PL</ta>
            <ta e="T2" id="Seg_2379" s="T1">три-COLL</ta>
            <ta e="T3" id="Seg_2380" s="T2">коза.[NOM.SG]</ta>
            <ta e="T4" id="Seg_2381" s="T3">мужчина.[NOM.SG]</ta>
            <ta e="T5" id="Seg_2382" s="T4">и</ta>
            <ta e="T6" id="Seg_2383" s="T5">овца.[NOM.SG]</ta>
            <ta e="T7" id="Seg_2384" s="T6">мужчина.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2385" s="T7">и</ta>
            <ta e="T9" id="Seg_2386" s="T8">кот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_2387" s="T9">мужчина.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2388" s="T10">кот.[NOM.SG]</ta>
            <ta e="T13" id="Seg_2389" s="T12">всегда</ta>
            <ta e="T14" id="Seg_2390" s="T13">украсть-PST.[3SG]</ta>
            <ta e="T15" id="Seg_2391" s="T14">мясо.[NOM.SG]</ta>
            <ta e="T16" id="Seg_2392" s="T15">съесть-DUR.[3SG]</ta>
            <ta e="T17" id="Seg_2393" s="T16">сметана.[NOM.SG]</ta>
            <ta e="T18" id="Seg_2394" s="T17">съесть-DUR.[3SG]</ta>
            <ta e="T19" id="Seg_2395" s="T18">там</ta>
            <ta e="T20" id="Seg_2396" s="T19">женщина-NOM/GEN.3SG</ta>
            <ta e="T21" id="Seg_2397" s="T20">PTCL</ta>
            <ta e="T22" id="Seg_2398" s="T21">бить-DUR.[3SG]</ta>
            <ta e="T23" id="Seg_2399" s="T22">нога-ACC.3SG</ta>
            <ta e="T25" id="Seg_2400" s="T24">нога-ACC.3SG</ta>
            <ta e="T26" id="Seg_2401" s="T25">бить-PST.[3SG]</ta>
            <ta e="T27" id="Seg_2402" s="T26">нога-NOM/GEN.3SG</ta>
            <ta e="T28" id="Seg_2403" s="T27">болеть-PST.[3SG]</ta>
            <ta e="T29" id="Seg_2404" s="T28">этот.[NOM.SG]</ta>
            <ta e="T30" id="Seg_2405" s="T29">прийти-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_2406" s="T30">и</ta>
            <ta e="T32" id="Seg_2407" s="T31">плакать-DUR.[3SG]</ta>
            <ta e="T33" id="Seg_2408" s="T32">а</ta>
            <ta e="T34" id="Seg_2409" s="T33">два.[NOM.SG]</ta>
            <ta e="T35" id="Seg_2410" s="T34">овца.[NOM.SG]</ta>
            <ta e="T36" id="Seg_2411" s="T35">и</ta>
            <ta e="T37" id="Seg_2412" s="T36">коза.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2413" s="T37">ложиться-DUR.[3SG]</ta>
            <ta e="T39" id="Seg_2414" s="T38">что.[NOM.SG]</ta>
            <ta e="T40" id="Seg_2415" s="T39">плакать-DUR-2SG</ta>
            <ta e="T41" id="Seg_2416" s="T40">и</ta>
            <ta e="T42" id="Seg_2417" s="T41">я.ACC</ta>
            <ta e="T43" id="Seg_2418" s="T42">бить-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2419" s="T43">этот.[NOM.SG]</ta>
            <ta e="T45" id="Seg_2420" s="T44">женщина.[NOM.SG]</ta>
            <ta e="T47" id="Seg_2421" s="T46">что.[NOM.SG]</ta>
            <ta e="T48" id="Seg_2422" s="T47">ты.NOM</ta>
            <ta e="T49" id="Seg_2423" s="T48">делать-PST-2SG</ta>
            <ta e="T50" id="Seg_2424" s="T49">и</ta>
            <ta e="T51" id="Seg_2425" s="T50">сметана-ACC</ta>
            <ta e="T52" id="Seg_2426" s="T51">съесть-PST-1SG</ta>
            <ta e="T53" id="Seg_2427" s="T52">тогда</ta>
            <ta e="T54" id="Seg_2428" s="T53">этот.[NOM.SG]</ta>
            <ta e="T55" id="Seg_2429" s="T54">сказать-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_2430" s="T55">сейчас</ta>
            <ta e="T57" id="Seg_2431" s="T56">весь</ta>
            <ta e="T58" id="Seg_2432" s="T57">сметана-ACC</ta>
            <ta e="T59" id="Seg_2433" s="T58">съесть-PST-2SG</ta>
            <ta e="T60" id="Seg_2434" s="T59">надо</ta>
            <ta e="T61" id="Seg_2435" s="T60">я.LAT</ta>
            <ta e="T62" id="Seg_2436" s="T61">коза.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2437" s="T62">резать-INF.LAT</ta>
            <ta e="T64" id="Seg_2438" s="T63">я.GEN</ta>
            <ta e="T65" id="Seg_2439" s="T64">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T66" id="Seg_2440" s="T65">прийти-MOM-3PL</ta>
            <ta e="T67" id="Seg_2441" s="T66">надо</ta>
            <ta e="T68" id="Seg_2442" s="T67">есть-INF.LAT</ta>
            <ta e="T69" id="Seg_2443" s="T68">дать-INF.LAT</ta>
            <ta e="T70" id="Seg_2444" s="T69">тогда</ta>
            <ta e="T71" id="Seg_2445" s="T70">этот-PL</ta>
            <ta e="T72" id="Seg_2446" s="T71">сказать-PRS-3PL</ta>
            <ta e="T73" id="Seg_2447" s="T72">пойти-OPT.DU/PL-1PL</ta>
            <ta e="T74" id="Seg_2448" s="T73">три-COLL</ta>
            <ta e="T75" id="Seg_2449" s="T74">спрятаться-RES-1PL</ta>
            <ta e="T76" id="Seg_2450" s="T75">тогда</ta>
            <ta e="T77" id="Seg_2451" s="T76">один.[NOM.SG]</ta>
            <ta e="T78" id="Seg_2452" s="T77">сказать-IPFVZ.[3SG]</ta>
            <ta e="T79" id="Seg_2453" s="T78">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T80" id="Seg_2454" s="T79">ударить-MULT-IMP.2SG.O</ta>
            <ta e="T81" id="Seg_2455" s="T80">голова-INS</ta>
            <ta e="T82" id="Seg_2456" s="T81">ворота-NOM/GEN/ACC.3PL</ta>
            <ta e="T83" id="Seg_2457" s="T82">этот.[NOM.SG]</ta>
            <ta e="T84" id="Seg_2458" s="T83">ударить-MULT-MOM-PST.[3SG]</ta>
            <ta e="T85" id="Seg_2459" s="T84">NEG</ta>
            <ta e="T86" id="Seg_2460" s="T85">мочь</ta>
            <ta e="T87" id="Seg_2461" s="T86">NEG</ta>
            <ta e="T88" id="Seg_2462" s="T87">открыть-MOM-PST-3PL</ta>
            <ta e="T89" id="Seg_2463" s="T88">тогда</ta>
            <ta e="T90" id="Seg_2464" s="T89">коза.[NOM.SG]</ta>
            <ta e="T91" id="Seg_2465" s="T90">ударить-MULT-MOM-PST.[3SG]</ta>
            <ta e="T92" id="Seg_2466" s="T91">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T93" id="Seg_2467" s="T92">голова-INS</ta>
            <ta e="T94" id="Seg_2468" s="T93">ворота-NOM/GEN/ACC.3PL</ta>
            <ta e="T95" id="Seg_2469" s="T94">открыть-MOM-PST-3PL</ta>
            <ta e="T96" id="Seg_2470" s="T95">тогда</ta>
            <ta e="T97" id="Seg_2471" s="T96">этот-PL</ta>
            <ta e="T98" id="Seg_2472" s="T97">бежать-MOM-PST-3PL</ta>
            <ta e="T99" id="Seg_2473" s="T98">бежать-MOM-PST-3PL</ta>
            <ta e="T100" id="Seg_2474" s="T99">бежать-MOM-PST-3PL</ta>
            <ta e="T101" id="Seg_2475" s="T100">тогда</ta>
            <ta e="T102" id="Seg_2476" s="T101">прийти-PST-3PL</ta>
            <ta e="T103" id="Seg_2477" s="T102">там</ta>
            <ta e="T104" id="Seg_2478" s="T103">трава.[NOM.SG]</ta>
            <ta e="T105" id="Seg_2479" s="T104">много</ta>
            <ta e="T106" id="Seg_2480" s="T105">стоять-PRS-3PL</ta>
            <ta e="T107" id="Seg_2481" s="T106">вечер.[NOM.SG]</ta>
            <ta e="T108" id="Seg_2482" s="T107">стать-RES-PST.[3SG]</ta>
            <ta e="T109" id="Seg_2483" s="T108">надо</ta>
            <ta e="T110" id="Seg_2484" s="T109">ночевать-INF.LAT</ta>
            <ta e="T111" id="Seg_2485" s="T110">тогда</ta>
            <ta e="T112" id="Seg_2486" s="T111">ободрать-PST.[3SG]</ta>
            <ta e="T113" id="Seg_2487" s="T112">кожа.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2488" s="T113">белый.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2489" s="T114">дерево-LOC</ta>
            <ta e="T116" id="Seg_2490" s="T115">и</ta>
            <ta e="T117" id="Seg_2491" s="T116">этот-LAT</ta>
            <ta e="T118" id="Seg_2492" s="T117">рог-PL-LAT</ta>
            <ta e="T119" id="Seg_2493" s="T118">надеть-PST.[3SG]</ta>
            <ta e="T120" id="Seg_2494" s="T119">тогда</ta>
            <ta e="T121" id="Seg_2495" s="T120">сказать-IPFVZ.[3SG]</ta>
            <ta e="T122" id="Seg_2496" s="T121">ударить-MULT</ta>
            <ta e="T123" id="Seg_2497" s="T122">ударить-MULT-2PL</ta>
            <ta e="T124" id="Seg_2498" s="T123">один.[NOM.SG]</ta>
            <ta e="T125" id="Seg_2499" s="T124">один-LAT</ta>
            <ta e="T126" id="Seg_2500" s="T125">этот-PL</ta>
            <ta e="T127" id="Seg_2501" s="T126">ударить-MULT-MOM-PST-3PL</ta>
            <ta e="T128" id="Seg_2502" s="T127">и</ta>
            <ta e="T130" id="Seg_2503" s="T129">огонь.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2504" s="T130">гореть-MOM-PST.[3SG]</ta>
            <ta e="T132" id="Seg_2505" s="T131">тогда</ta>
            <ta e="T133" id="Seg_2506" s="T132">сесть-PST-3PL</ta>
            <ta e="T134" id="Seg_2507" s="T133">и</ta>
            <ta e="T135" id="Seg_2508" s="T134">нагреться-MOM-PST-3PL</ta>
            <ta e="T136" id="Seg_2509" s="T135">хватит</ta>
            <ta e="T137" id="Seg_2510" s="T136">этот-PL</ta>
            <ta e="T138" id="Seg_2511" s="T137">нагреться-MOM-PST-3PL</ta>
            <ta e="T139" id="Seg_2512" s="T138">тогда</ta>
            <ta e="T140" id="Seg_2513" s="T139">медведь.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2514" s="T140">прийти-PST.[3SG]</ta>
            <ta e="T142" id="Seg_2515" s="T141">сесть-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2516" s="T142">тоже</ta>
            <ta e="T144" id="Seg_2517" s="T143">нагреться-MOM-PST.[3SG]</ta>
            <ta e="T145" id="Seg_2518" s="T144">тогда</ta>
            <ta e="T146" id="Seg_2519" s="T145">пойти-PST.[3SG]</ta>
            <ta e="T147" id="Seg_2520" s="T146">трава-LAT</ta>
            <ta e="T148" id="Seg_2521" s="T147">там</ta>
            <ta e="T149" id="Seg_2522" s="T148">ложиться-PST.[3SG]</ta>
            <ta e="T150" id="Seg_2523" s="T149">а</ta>
            <ta e="T151" id="Seg_2524" s="T150">кот.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2525" s="T151">%наверху</ta>
            <ta e="T153" id="Seg_2526" s="T152">сесть-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2527" s="T153">а</ta>
            <ta e="T156" id="Seg_2528" s="T155">этот.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2529" s="T156">овца.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2530" s="T157">и</ta>
            <ta e="T159" id="Seg_2531" s="T158">коза.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2532" s="T159">огонь-LOC</ta>
            <ta e="T161" id="Seg_2533" s="T160">ложиться-DUR-PST.[3SG]</ta>
            <ta e="T162" id="Seg_2534" s="T161">тогда</ta>
            <ta e="T163" id="Seg_2535" s="T162">прийти-PST-3PL</ta>
            <ta e="T164" id="Seg_2536" s="T163">волк-PL</ta>
            <ta e="T165" id="Seg_2537" s="T164">один.[NOM.SG]</ta>
            <ta e="T166" id="Seg_2538" s="T165">белый.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2539" s="T166">волк.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2540" s="T167">тогда</ta>
            <ta e="T169" id="Seg_2541" s="T168">кот.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2542" s="T169">сказать-IPFVZ.[3SG]</ta>
            <ta e="T171" id="Seg_2543" s="T170">мы.NOM</ta>
            <ta e="T172" id="Seg_2544" s="T171">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T173" id="Seg_2545" s="T172">ложиться-DUR.[3SG]</ta>
            <ta e="T174" id="Seg_2546" s="T173">там</ta>
            <ta e="T175" id="Seg_2547" s="T174">стог-LOC</ta>
            <ta e="T176" id="Seg_2548" s="T175">этот-ACC</ta>
            <ta e="T177" id="Seg_2549" s="T176">нагреться-DUR-IMP.2SG</ta>
            <ta e="T178" id="Seg_2550" s="T177">тогда</ta>
            <ta e="T179" id="Seg_2551" s="T178">этот.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2552" s="T179">медведь.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2553" s="T180">один.[NOM.SG]</ta>
            <ta e="T182" id="Seg_2554" s="T181">взять-PST.[3SG]</ta>
            <ta e="T183" id="Seg_2555" s="T182">волк-NOM/GEN/ACC.3SG</ta>
            <ta e="T184" id="Seg_2556" s="T183">и</ta>
            <ta e="T185" id="Seg_2557" s="T184">один.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2558" s="T185">рука-LAT/LOC.3SG</ta>
            <ta e="T187" id="Seg_2559" s="T186">и</ta>
            <ta e="T188" id="Seg_2560" s="T187">один.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2561" s="T188">взять-PST.[3SG]</ta>
            <ta e="T190" id="Seg_2562" s="T189">другой.[NOM.SG]</ta>
            <ta e="T191" id="Seg_2563" s="T190">рука-LAT/LOC.3SG</ta>
            <ta e="T192" id="Seg_2564" s="T191">этот.[NOM.SG]</ta>
            <ta e="T193" id="Seg_2565" s="T192">этот-PL</ta>
            <ta e="T194" id="Seg_2566" s="T193">весь</ta>
            <ta e="T195" id="Seg_2567" s="T194">как</ta>
            <ta e="T196" id="Seg_2568" s="T195">пугаться-MOM-PST-3PL</ta>
            <ta e="T197" id="Seg_2569" s="T196">и</ta>
            <ta e="T199" id="Seg_2570" s="T198">бежать-MOM-PST-3PL</ta>
            <ta e="T200" id="Seg_2571" s="T199">тогда</ta>
            <ta e="T201" id="Seg_2572" s="T200">овца.[NOM.SG]</ta>
            <ta e="T202" id="Seg_2573" s="T201">и</ta>
            <ta e="T203" id="Seg_2574" s="T202">коза.[NOM.SG]</ta>
            <ta e="T204" id="Seg_2575" s="T203">и</ta>
            <ta e="T205" id="Seg_2576" s="T204">кот.[NOM.SG]</ta>
            <ta e="T206" id="Seg_2577" s="T205">бежать-MOM-PST-3PL</ta>
            <ta e="T207" id="Seg_2578" s="T206">опять</ta>
            <ta e="T208" id="Seg_2579" s="T207">тогда</ta>
            <ta e="T209" id="Seg_2580" s="T208">там</ta>
            <ta e="T210" id="Seg_2581" s="T209">опять</ta>
            <ta e="T211" id="Seg_2582" s="T210">волк-PL</ta>
            <ta e="T212" id="Seg_2583" s="T211">этот.[NOM.SG]</ta>
            <ta e="T213" id="Seg_2584" s="T212">кот.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2585" s="T213">влезать-PST.[3SG]</ta>
            <ta e="T215" id="Seg_2586" s="T214">дерево-LAT</ta>
            <ta e="T216" id="Seg_2587" s="T215">а</ta>
            <ta e="T217" id="Seg_2588" s="T216">этот-PL</ta>
            <ta e="T218" id="Seg_2589" s="T217">висеть-PST-3PL</ta>
            <ta e="T219" id="Seg_2590" s="T218">тогда</ta>
            <ta e="T220" id="Seg_2591" s="T219">этот.[NOM.SG]</ta>
            <ta e="T221" id="Seg_2592" s="T220">сказать-IPFVZ.[3SG]</ta>
            <ta e="T222" id="Seg_2593" s="T221">я.NOM</ta>
            <ta e="T223" id="Seg_2594" s="T222">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T225" id="Seg_2595" s="T224">я.NOM</ta>
            <ta e="T226" id="Seg_2596" s="T225">два-NOM/GEN/ACC.1SG</ta>
            <ta e="T227" id="Seg_2597" s="T226">взять-FUT-1SG</ta>
            <ta e="T228" id="Seg_2598" s="T227">а</ta>
            <ta e="T229" id="Seg_2599" s="T228">ты.DAT</ta>
            <ta e="T230" id="Seg_2600" s="T229">сколько</ta>
            <ta e="T231" id="Seg_2601" s="T230">нужно</ta>
            <ta e="T232" id="Seg_2602" s="T231">тогда</ta>
            <ta e="T233" id="Seg_2603" s="T232">коза.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2604" s="T233">упасть-MOM-PST.[3SG]</ta>
            <ta e="T235" id="Seg_2605" s="T234">и</ta>
            <ta e="T236" id="Seg_2606" s="T235">волк-LAT/LOC.3SG</ta>
            <ta e="T237" id="Seg_2607" s="T236">%%</ta>
            <ta e="T238" id="Seg_2608" s="T237">сесть-INF.LAT</ta>
            <ta e="T239" id="Seg_2609" s="T238">тогда</ta>
            <ta e="T240" id="Seg_2610" s="T239">этот.[NOM.SG]</ta>
            <ta e="T242" id="Seg_2611" s="T241">пугать-MOM-PST.[3SG]</ta>
            <ta e="T243" id="Seg_2612" s="T242">этот-PL</ta>
            <ta e="T244" id="Seg_2613" s="T243">пугать-MOM-PST-3PL</ta>
            <ta e="T245" id="Seg_2614" s="T244">и</ta>
            <ta e="T246" id="Seg_2615" s="T245">бежать-MOM-PST-3PL</ta>
            <ta e="T247" id="Seg_2616" s="T246">хватит</ta>
            <ta e="T248" id="Seg_2617" s="T247">этот-PL</ta>
            <ta e="T249" id="Seg_2618" s="T248">тогда</ta>
            <ta e="T250" id="Seg_2619" s="T249">пойти-PST-3PL</ta>
            <ta e="T251" id="Seg_2620" s="T250">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T254" id="Seg_2621" s="T253">дорога-PL-LAT</ta>
            <ta e="T255" id="Seg_2622" s="T254">три-COLL</ta>
            <ta e="T256" id="Seg_2623" s="T255">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2624" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_2625" s="T1">num-num&gt;num</ta>
            <ta e="T3" id="Seg_2626" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2627" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_2628" s="T4">conj</ta>
            <ta e="T6" id="Seg_2629" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_2630" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_2631" s="T7">conj</ta>
            <ta e="T9" id="Seg_2632" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_2633" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_2634" s="T10">n-n:case</ta>
            <ta e="T13" id="Seg_2635" s="T12">adv</ta>
            <ta e="T14" id="Seg_2636" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_2637" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_2638" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_2639" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_2640" s="T17">v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_2641" s="T18">adv</ta>
            <ta e="T20" id="Seg_2642" s="T19">n-n:case.poss</ta>
            <ta e="T21" id="Seg_2643" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_2644" s="T21">v-v&gt;v-v:pn</ta>
            <ta e="T23" id="Seg_2645" s="T22">n-n:case.poss</ta>
            <ta e="T25" id="Seg_2646" s="T24">n-n:case.poss</ta>
            <ta e="T26" id="Seg_2647" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_2648" s="T26">n-n:case.poss</ta>
            <ta e="T28" id="Seg_2649" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_2650" s="T28">dempro-n:case</ta>
            <ta e="T30" id="Seg_2651" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_2652" s="T30">conj</ta>
            <ta e="T32" id="Seg_2653" s="T31">v-v&gt;v-v:pn</ta>
            <ta e="T33" id="Seg_2654" s="T32">conj</ta>
            <ta e="T34" id="Seg_2655" s="T33">num-n:case</ta>
            <ta e="T35" id="Seg_2656" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_2657" s="T35">conj</ta>
            <ta e="T37" id="Seg_2658" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_2659" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_2660" s="T38">que-n:case</ta>
            <ta e="T40" id="Seg_2661" s="T39">v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_2662" s="T40">conj</ta>
            <ta e="T42" id="Seg_2663" s="T41">pers</ta>
            <ta e="T43" id="Seg_2664" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_2665" s="T43">dempro-n:case</ta>
            <ta e="T45" id="Seg_2666" s="T44">n-n:case</ta>
            <ta e="T47" id="Seg_2667" s="T46">que-n:case</ta>
            <ta e="T48" id="Seg_2668" s="T47">pers</ta>
            <ta e="T49" id="Seg_2669" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_2670" s="T49">conj</ta>
            <ta e="T51" id="Seg_2671" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2672" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_2673" s="T52">adv</ta>
            <ta e="T54" id="Seg_2674" s="T53">dempro-n:case</ta>
            <ta e="T55" id="Seg_2675" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_2676" s="T55">adv</ta>
            <ta e="T57" id="Seg_2677" s="T56">quant</ta>
            <ta e="T58" id="Seg_2678" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_2679" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_2680" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_2681" s="T60">pers</ta>
            <ta e="T62" id="Seg_2682" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_2683" s="T62">v-v:n.fin</ta>
            <ta e="T64" id="Seg_2684" s="T63">pers</ta>
            <ta e="T65" id="Seg_2685" s="T64">n-n:case.poss</ta>
            <ta e="T66" id="Seg_2686" s="T65">v-v&gt;v-v:pn</ta>
            <ta e="T67" id="Seg_2687" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2688" s="T67">v-v:n.fin</ta>
            <ta e="T69" id="Seg_2689" s="T68">v-v:n.fin</ta>
            <ta e="T70" id="Seg_2690" s="T69">adv</ta>
            <ta e="T71" id="Seg_2691" s="T70">dempro-n:num</ta>
            <ta e="T72" id="Seg_2692" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_2693" s="T72">v-v:mood-v:pn</ta>
            <ta e="T74" id="Seg_2694" s="T73">num-num&gt;num</ta>
            <ta e="T75" id="Seg_2695" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_2696" s="T75">adv</ta>
            <ta e="T77" id="Seg_2697" s="T76">num-n:case</ta>
            <ta e="T78" id="Seg_2698" s="T77">v-v&gt;v-v:pn</ta>
            <ta e="T79" id="Seg_2699" s="T78">n-n:case.poss</ta>
            <ta e="T80" id="Seg_2700" s="T79">v-v&gt;v-v:mood.pn</ta>
            <ta e="T81" id="Seg_2701" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_2702" s="T81">n-n:case.poss</ta>
            <ta e="T83" id="Seg_2703" s="T82">dempro-n:case</ta>
            <ta e="T84" id="Seg_2704" s="T83">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_2705" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_2706" s="T85">v</ta>
            <ta e="T87" id="Seg_2707" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_2708" s="T87">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_2709" s="T88">adv</ta>
            <ta e="T90" id="Seg_2710" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2711" s="T90">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_2712" s="T91">refl-n:case.poss</ta>
            <ta e="T93" id="Seg_2713" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_2714" s="T93">n-n:case.poss</ta>
            <ta e="T95" id="Seg_2715" s="T94">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_2716" s="T95">adv</ta>
            <ta e="T97" id="Seg_2717" s="T96">dempro-n:num</ta>
            <ta e="T98" id="Seg_2718" s="T97">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2719" s="T98">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_2720" s="T99">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_2721" s="T100">adv</ta>
            <ta e="T102" id="Seg_2722" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_2723" s="T102">adv</ta>
            <ta e="T104" id="Seg_2724" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2725" s="T104">quant</ta>
            <ta e="T106" id="Seg_2726" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_2727" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_2728" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_2729" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_2730" s="T109">v-v:n.fin</ta>
            <ta e="T111" id="Seg_2731" s="T110">adv</ta>
            <ta e="T112" id="Seg_2732" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_2733" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2734" s="T113">adj-n:case</ta>
            <ta e="T115" id="Seg_2735" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_2736" s="T115">conj</ta>
            <ta e="T117" id="Seg_2737" s="T116">dempro-n:case</ta>
            <ta e="T118" id="Seg_2738" s="T117">n-n:num-n:case</ta>
            <ta e="T119" id="Seg_2739" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_2740" s="T119">adv</ta>
            <ta e="T121" id="Seg_2741" s="T120">v-v&gt;v-v:pn</ta>
            <ta e="T122" id="Seg_2742" s="T121">v-v&gt;v</ta>
            <ta e="T123" id="Seg_2743" s="T122">v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_2744" s="T123">num-n:case</ta>
            <ta e="T125" id="Seg_2745" s="T124">num-n:case</ta>
            <ta e="T126" id="Seg_2746" s="T125">dempro-n:num</ta>
            <ta e="T127" id="Seg_2747" s="T126">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_2748" s="T127">conj</ta>
            <ta e="T130" id="Seg_2749" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_2750" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2751" s="T131">adv</ta>
            <ta e="T133" id="Seg_2752" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2753" s="T133">conj</ta>
            <ta e="T135" id="Seg_2754" s="T134">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_2755" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_2756" s="T136">dempro-n:num</ta>
            <ta e="T138" id="Seg_2757" s="T137">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_2758" s="T138">adv</ta>
            <ta e="T140" id="Seg_2759" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_2760" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2761" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_2762" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_2763" s="T143">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_2764" s="T144">adv</ta>
            <ta e="T146" id="Seg_2765" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_2766" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2767" s="T147">adv</ta>
            <ta e="T149" id="Seg_2768" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_2769" s="T149">conj</ta>
            <ta e="T151" id="Seg_2770" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2771" s="T151">adv</ta>
            <ta e="T153" id="Seg_2772" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_2773" s="T153">conj</ta>
            <ta e="T156" id="Seg_2774" s="T155">dempro-n:case</ta>
            <ta e="T157" id="Seg_2775" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_2776" s="T157">conj</ta>
            <ta e="T159" id="Seg_2777" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_2778" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2779" s="T160">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2780" s="T161">adv</ta>
            <ta e="T163" id="Seg_2781" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2782" s="T163">n-n:num</ta>
            <ta e="T165" id="Seg_2783" s="T164">adj-n:case</ta>
            <ta e="T166" id="Seg_2784" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_2785" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_2786" s="T167">adv</ta>
            <ta e="T169" id="Seg_2787" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2788" s="T169">v-v&gt;v-v:pn</ta>
            <ta e="T171" id="Seg_2789" s="T170">pers</ta>
            <ta e="T172" id="Seg_2790" s="T171">n-n:case.poss</ta>
            <ta e="T173" id="Seg_2791" s="T172">v-v&gt;v-v:pn</ta>
            <ta e="T174" id="Seg_2792" s="T173">adv</ta>
            <ta e="T175" id="Seg_2793" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_2794" s="T175">dempro-n:case</ta>
            <ta e="T177" id="Seg_2795" s="T176">v-v&gt;v-v:mood.pn</ta>
            <ta e="T178" id="Seg_2796" s="T177">adv</ta>
            <ta e="T179" id="Seg_2797" s="T178">dempro-n:case</ta>
            <ta e="T180" id="Seg_2798" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2799" s="T180">adj-n:case</ta>
            <ta e="T182" id="Seg_2800" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_2801" s="T182">n-n:case.poss</ta>
            <ta e="T184" id="Seg_2802" s="T183">conj</ta>
            <ta e="T185" id="Seg_2803" s="T184">adj-n:case</ta>
            <ta e="T186" id="Seg_2804" s="T185">n-n:case.poss</ta>
            <ta e="T187" id="Seg_2805" s="T186">conj</ta>
            <ta e="T188" id="Seg_2806" s="T187">adj-n:case</ta>
            <ta e="T189" id="Seg_2807" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_2808" s="T189">adj-n:case</ta>
            <ta e="T191" id="Seg_2809" s="T190">n-n:case.poss</ta>
            <ta e="T192" id="Seg_2810" s="T191">dempro-n:case</ta>
            <ta e="T193" id="Seg_2811" s="T192">dempro-n:num</ta>
            <ta e="T194" id="Seg_2812" s="T193">quant</ta>
            <ta e="T195" id="Seg_2813" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_2814" s="T195">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2815" s="T196">conj</ta>
            <ta e="T199" id="Seg_2816" s="T198">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_2817" s="T199">adv</ta>
            <ta e="T201" id="Seg_2818" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_2819" s="T201">conj</ta>
            <ta e="T203" id="Seg_2820" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_2821" s="T203">conj</ta>
            <ta e="T205" id="Seg_2822" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2823" s="T205">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_2824" s="T206">adv</ta>
            <ta e="T208" id="Seg_2825" s="T207">adv</ta>
            <ta e="T209" id="Seg_2826" s="T208">adv</ta>
            <ta e="T210" id="Seg_2827" s="T209">adv</ta>
            <ta e="T211" id="Seg_2828" s="T210">n-n:num</ta>
            <ta e="T212" id="Seg_2829" s="T211">dempro-n:case</ta>
            <ta e="T213" id="Seg_2830" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_2831" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_2832" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_2833" s="T215">conj</ta>
            <ta e="T217" id="Seg_2834" s="T216">dempro-n:num</ta>
            <ta e="T218" id="Seg_2835" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_2836" s="T218">adv</ta>
            <ta e="T220" id="Seg_2837" s="T219">dempro-n:case</ta>
            <ta e="T221" id="Seg_2838" s="T220">v-v&gt;v-v:pn</ta>
            <ta e="T222" id="Seg_2839" s="T221">pers</ta>
            <ta e="T223" id="Seg_2840" s="T222">n-n:case.poss</ta>
            <ta e="T225" id="Seg_2841" s="T224">pers</ta>
            <ta e="T226" id="Seg_2842" s="T225">num-n:case.poss</ta>
            <ta e="T227" id="Seg_2843" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_2844" s="T227">conj</ta>
            <ta e="T229" id="Seg_2845" s="T228">pers</ta>
            <ta e="T230" id="Seg_2846" s="T229">adv</ta>
            <ta e="T231" id="Seg_2847" s="T230">adv</ta>
            <ta e="T232" id="Seg_2848" s="T231">adv</ta>
            <ta e="T233" id="Seg_2849" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_2850" s="T233">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_2851" s="T234">conj</ta>
            <ta e="T236" id="Seg_2852" s="T235">n-n:case.poss</ta>
            <ta e="T237" id="Seg_2853" s="T236">%%</ta>
            <ta e="T238" id="Seg_2854" s="T237">v-v:n.fin</ta>
            <ta e="T239" id="Seg_2855" s="T238">adv</ta>
            <ta e="T240" id="Seg_2856" s="T239">dempro-n:case</ta>
            <ta e="T242" id="Seg_2857" s="T241">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_2858" s="T242">dempro-n:num</ta>
            <ta e="T244" id="Seg_2859" s="T243">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_2860" s="T244">conj</ta>
            <ta e="T246" id="Seg_2861" s="T245">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T247" id="Seg_2862" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_2863" s="T247">dempro-n:num</ta>
            <ta e="T249" id="Seg_2864" s="T248">adv</ta>
            <ta e="T250" id="Seg_2865" s="T249">v-v:tense-v:pn</ta>
            <ta e="T251" id="Seg_2866" s="T250">refl-n:case.poss</ta>
            <ta e="T254" id="Seg_2867" s="T253">n-n:num-n:case</ta>
            <ta e="T255" id="Seg_2868" s="T254">num-num&gt;num</ta>
            <ta e="T256" id="Seg_2869" s="T255">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2870" s="T0">v</ta>
            <ta e="T2" id="Seg_2871" s="T1">num</ta>
            <ta e="T3" id="Seg_2872" s="T2">n</ta>
            <ta e="T4" id="Seg_2873" s="T3">n</ta>
            <ta e="T5" id="Seg_2874" s="T4">conj</ta>
            <ta e="T6" id="Seg_2875" s="T5">n</ta>
            <ta e="T7" id="Seg_2876" s="T6">n</ta>
            <ta e="T8" id="Seg_2877" s="T7">conj</ta>
            <ta e="T9" id="Seg_2878" s="T8">n</ta>
            <ta e="T10" id="Seg_2879" s="T9">n</ta>
            <ta e="T11" id="Seg_2880" s="T10">n</ta>
            <ta e="T13" id="Seg_2881" s="T12">adv</ta>
            <ta e="T14" id="Seg_2882" s="T13">v</ta>
            <ta e="T15" id="Seg_2883" s="T14">n</ta>
            <ta e="T16" id="Seg_2884" s="T15">v</ta>
            <ta e="T17" id="Seg_2885" s="T16">n</ta>
            <ta e="T18" id="Seg_2886" s="T17">v</ta>
            <ta e="T19" id="Seg_2887" s="T18">adv</ta>
            <ta e="T20" id="Seg_2888" s="T19">n</ta>
            <ta e="T21" id="Seg_2889" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_2890" s="T21">v</ta>
            <ta e="T23" id="Seg_2891" s="T22">n</ta>
            <ta e="T25" id="Seg_2892" s="T24">n</ta>
            <ta e="T26" id="Seg_2893" s="T25">v</ta>
            <ta e="T27" id="Seg_2894" s="T26">n</ta>
            <ta e="T28" id="Seg_2895" s="T27">v</ta>
            <ta e="T29" id="Seg_2896" s="T28">dempro</ta>
            <ta e="T30" id="Seg_2897" s="T29">v</ta>
            <ta e="T31" id="Seg_2898" s="T30">conj</ta>
            <ta e="T32" id="Seg_2899" s="T31">v</ta>
            <ta e="T33" id="Seg_2900" s="T32">conj</ta>
            <ta e="T34" id="Seg_2901" s="T33">num</ta>
            <ta e="T35" id="Seg_2902" s="T34">n</ta>
            <ta e="T36" id="Seg_2903" s="T35">conj</ta>
            <ta e="T37" id="Seg_2904" s="T36">n</ta>
            <ta e="T38" id="Seg_2905" s="T37">v</ta>
            <ta e="T39" id="Seg_2906" s="T38">que</ta>
            <ta e="T40" id="Seg_2907" s="T39">v</ta>
            <ta e="T41" id="Seg_2908" s="T40">conj</ta>
            <ta e="T42" id="Seg_2909" s="T41">pers</ta>
            <ta e="T43" id="Seg_2910" s="T42">v</ta>
            <ta e="T44" id="Seg_2911" s="T43">dempro</ta>
            <ta e="T45" id="Seg_2912" s="T44">n</ta>
            <ta e="T47" id="Seg_2913" s="T46">que</ta>
            <ta e="T48" id="Seg_2914" s="T47">pers</ta>
            <ta e="T49" id="Seg_2915" s="T48">v</ta>
            <ta e="T50" id="Seg_2916" s="T49">conj</ta>
            <ta e="T51" id="Seg_2917" s="T50">n</ta>
            <ta e="T52" id="Seg_2918" s="T51">v</ta>
            <ta e="T53" id="Seg_2919" s="T52">adv</ta>
            <ta e="T54" id="Seg_2920" s="T53">dempro</ta>
            <ta e="T55" id="Seg_2921" s="T54">v</ta>
            <ta e="T56" id="Seg_2922" s="T55">adv</ta>
            <ta e="T57" id="Seg_2923" s="T56">quant</ta>
            <ta e="T58" id="Seg_2924" s="T57">n</ta>
            <ta e="T59" id="Seg_2925" s="T58">v</ta>
            <ta e="T60" id="Seg_2926" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_2927" s="T60">pers</ta>
            <ta e="T62" id="Seg_2928" s="T61">n</ta>
            <ta e="T63" id="Seg_2929" s="T62">v</ta>
            <ta e="T64" id="Seg_2930" s="T63">pers</ta>
            <ta e="T65" id="Seg_2931" s="T64">n</ta>
            <ta e="T66" id="Seg_2932" s="T65">v</ta>
            <ta e="T67" id="Seg_2933" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2934" s="T67">v</ta>
            <ta e="T69" id="Seg_2935" s="T68">v</ta>
            <ta e="T70" id="Seg_2936" s="T69">adv</ta>
            <ta e="T71" id="Seg_2937" s="T70">dempro</ta>
            <ta e="T72" id="Seg_2938" s="T71">v</ta>
            <ta e="T73" id="Seg_2939" s="T72">v</ta>
            <ta e="T74" id="Seg_2940" s="T73">num</ta>
            <ta e="T75" id="Seg_2941" s="T74">v</ta>
            <ta e="T76" id="Seg_2942" s="T75">adv</ta>
            <ta e="T77" id="Seg_2943" s="T76">num</ta>
            <ta e="T78" id="Seg_2944" s="T77">v</ta>
            <ta e="T79" id="Seg_2945" s="T78">n</ta>
            <ta e="T80" id="Seg_2946" s="T79">v</ta>
            <ta e="T81" id="Seg_2947" s="T80">n</ta>
            <ta e="T82" id="Seg_2948" s="T81">n</ta>
            <ta e="T83" id="Seg_2949" s="T82">dempro</ta>
            <ta e="T84" id="Seg_2950" s="T83">v</ta>
            <ta e="T85" id="Seg_2951" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_2952" s="T85">v</ta>
            <ta e="T87" id="Seg_2953" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_2954" s="T87">v</ta>
            <ta e="T89" id="Seg_2955" s="T88">adv</ta>
            <ta e="T90" id="Seg_2956" s="T89">n</ta>
            <ta e="T91" id="Seg_2957" s="T90">v</ta>
            <ta e="T92" id="Seg_2958" s="T91">refl</ta>
            <ta e="T93" id="Seg_2959" s="T92">n</ta>
            <ta e="T94" id="Seg_2960" s="T93">n</ta>
            <ta e="T95" id="Seg_2961" s="T94">v</ta>
            <ta e="T96" id="Seg_2962" s="T95">adv</ta>
            <ta e="T97" id="Seg_2963" s="T96">dempro</ta>
            <ta e="T98" id="Seg_2964" s="T97">v</ta>
            <ta e="T99" id="Seg_2965" s="T98">v</ta>
            <ta e="T100" id="Seg_2966" s="T99">v</ta>
            <ta e="T101" id="Seg_2967" s="T100">adv</ta>
            <ta e="T102" id="Seg_2968" s="T101">v</ta>
            <ta e="T103" id="Seg_2969" s="T102">adv</ta>
            <ta e="T104" id="Seg_2970" s="T103">n</ta>
            <ta e="T105" id="Seg_2971" s="T104">quant</ta>
            <ta e="T106" id="Seg_2972" s="T105">v</ta>
            <ta e="T107" id="Seg_2973" s="T106">n</ta>
            <ta e="T108" id="Seg_2974" s="T107">v</ta>
            <ta e="T109" id="Seg_2975" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_2976" s="T109">v</ta>
            <ta e="T111" id="Seg_2977" s="T110">adv</ta>
            <ta e="T112" id="Seg_2978" s="T111">v</ta>
            <ta e="T113" id="Seg_2979" s="T112">n</ta>
            <ta e="T114" id="Seg_2980" s="T113">adj</ta>
            <ta e="T115" id="Seg_2981" s="T114">n</ta>
            <ta e="T116" id="Seg_2982" s="T115">conj</ta>
            <ta e="T117" id="Seg_2983" s="T116">dempro</ta>
            <ta e="T118" id="Seg_2984" s="T117">n</ta>
            <ta e="T119" id="Seg_2985" s="T118">v</ta>
            <ta e="T120" id="Seg_2986" s="T119">adv</ta>
            <ta e="T121" id="Seg_2987" s="T120">v</ta>
            <ta e="T122" id="Seg_2988" s="T121">v</ta>
            <ta e="T123" id="Seg_2989" s="T122">v</ta>
            <ta e="T124" id="Seg_2990" s="T123">num</ta>
            <ta e="T125" id="Seg_2991" s="T124">num</ta>
            <ta e="T126" id="Seg_2992" s="T125">dempro</ta>
            <ta e="T127" id="Seg_2993" s="T126">v</ta>
            <ta e="T128" id="Seg_2994" s="T127">conj</ta>
            <ta e="T130" id="Seg_2995" s="T129">n</ta>
            <ta e="T131" id="Seg_2996" s="T130">v</ta>
            <ta e="T132" id="Seg_2997" s="T131">adv</ta>
            <ta e="T133" id="Seg_2998" s="T132">v</ta>
            <ta e="T134" id="Seg_2999" s="T133">conj</ta>
            <ta e="T135" id="Seg_3000" s="T134">v</ta>
            <ta e="T136" id="Seg_3001" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_3002" s="T136">dempro</ta>
            <ta e="T138" id="Seg_3003" s="T137">v</ta>
            <ta e="T139" id="Seg_3004" s="T138">adv</ta>
            <ta e="T140" id="Seg_3005" s="T139">n</ta>
            <ta e="T141" id="Seg_3006" s="T140">v</ta>
            <ta e="T142" id="Seg_3007" s="T141">v</ta>
            <ta e="T143" id="Seg_3008" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_3009" s="T143">v</ta>
            <ta e="T145" id="Seg_3010" s="T144">adv</ta>
            <ta e="T146" id="Seg_3011" s="T145">v</ta>
            <ta e="T147" id="Seg_3012" s="T146">n</ta>
            <ta e="T148" id="Seg_3013" s="T147">adv</ta>
            <ta e="T149" id="Seg_3014" s="T148">v</ta>
            <ta e="T150" id="Seg_3015" s="T149">conj</ta>
            <ta e="T151" id="Seg_3016" s="T150">n</ta>
            <ta e="T152" id="Seg_3017" s="T151">adv</ta>
            <ta e="T153" id="Seg_3018" s="T152">v</ta>
            <ta e="T154" id="Seg_3019" s="T153">conj</ta>
            <ta e="T156" id="Seg_3020" s="T155">dempro</ta>
            <ta e="T157" id="Seg_3021" s="T156">n</ta>
            <ta e="T158" id="Seg_3022" s="T157">conj</ta>
            <ta e="T159" id="Seg_3023" s="T158">n</ta>
            <ta e="T160" id="Seg_3024" s="T159">n</ta>
            <ta e="T161" id="Seg_3025" s="T160">v</ta>
            <ta e="T162" id="Seg_3026" s="T161">adv</ta>
            <ta e="T163" id="Seg_3027" s="T162">v</ta>
            <ta e="T164" id="Seg_3028" s="T163">n</ta>
            <ta e="T165" id="Seg_3029" s="T164">adj</ta>
            <ta e="T166" id="Seg_3030" s="T165">adj</ta>
            <ta e="T167" id="Seg_3031" s="T166">n</ta>
            <ta e="T168" id="Seg_3032" s="T167">adv</ta>
            <ta e="T169" id="Seg_3033" s="T168">n</ta>
            <ta e="T170" id="Seg_3034" s="T169">v</ta>
            <ta e="T171" id="Seg_3035" s="T170">pers</ta>
            <ta e="T172" id="Seg_3036" s="T171">n</ta>
            <ta e="T173" id="Seg_3037" s="T172">v</ta>
            <ta e="T174" id="Seg_3038" s="T173">adv</ta>
            <ta e="T175" id="Seg_3039" s="T174">n</ta>
            <ta e="T176" id="Seg_3040" s="T175">dempro</ta>
            <ta e="T177" id="Seg_3041" s="T176">v</ta>
            <ta e="T178" id="Seg_3042" s="T177">adv</ta>
            <ta e="T179" id="Seg_3043" s="T178">dempro</ta>
            <ta e="T180" id="Seg_3044" s="T179">n</ta>
            <ta e="T181" id="Seg_3045" s="T180">adj</ta>
            <ta e="T182" id="Seg_3046" s="T181">v</ta>
            <ta e="T183" id="Seg_3047" s="T182">n</ta>
            <ta e="T184" id="Seg_3048" s="T183">conj</ta>
            <ta e="T185" id="Seg_3049" s="T184">adj</ta>
            <ta e="T186" id="Seg_3050" s="T185">n</ta>
            <ta e="T187" id="Seg_3051" s="T186">conj</ta>
            <ta e="T188" id="Seg_3052" s="T187">adj</ta>
            <ta e="T189" id="Seg_3053" s="T188">v</ta>
            <ta e="T190" id="Seg_3054" s="T189">adj</ta>
            <ta e="T191" id="Seg_3055" s="T190">n</ta>
            <ta e="T192" id="Seg_3056" s="T191">dempro</ta>
            <ta e="T193" id="Seg_3057" s="T192">dempro</ta>
            <ta e="T194" id="Seg_3058" s="T193">quant</ta>
            <ta e="T195" id="Seg_3059" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_3060" s="T195">v</ta>
            <ta e="T197" id="Seg_3061" s="T196">conj</ta>
            <ta e="T199" id="Seg_3062" s="T198">v</ta>
            <ta e="T200" id="Seg_3063" s="T199">adv</ta>
            <ta e="T201" id="Seg_3064" s="T200">n</ta>
            <ta e="T202" id="Seg_3065" s="T201">conj</ta>
            <ta e="T203" id="Seg_3066" s="T202">n</ta>
            <ta e="T204" id="Seg_3067" s="T203">conj</ta>
            <ta e="T205" id="Seg_3068" s="T204">n</ta>
            <ta e="T206" id="Seg_3069" s="T205">v</ta>
            <ta e="T207" id="Seg_3070" s="T206">adv</ta>
            <ta e="T208" id="Seg_3071" s="T207">adv</ta>
            <ta e="T209" id="Seg_3072" s="T208">adv</ta>
            <ta e="T210" id="Seg_3073" s="T209">adv</ta>
            <ta e="T211" id="Seg_3074" s="T210">n</ta>
            <ta e="T212" id="Seg_3075" s="T211">dempro</ta>
            <ta e="T213" id="Seg_3076" s="T212">n</ta>
            <ta e="T214" id="Seg_3077" s="T213">v</ta>
            <ta e="T215" id="Seg_3078" s="T214">n</ta>
            <ta e="T216" id="Seg_3079" s="T215">conj</ta>
            <ta e="T217" id="Seg_3080" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3081" s="T217">v</ta>
            <ta e="T219" id="Seg_3082" s="T218">adv</ta>
            <ta e="T220" id="Seg_3083" s="T219">dempro</ta>
            <ta e="T221" id="Seg_3084" s="T220">v</ta>
            <ta e="T222" id="Seg_3085" s="T221">pers</ta>
            <ta e="T223" id="Seg_3086" s="T222">n</ta>
            <ta e="T225" id="Seg_3087" s="T224">pers</ta>
            <ta e="T226" id="Seg_3088" s="T225">num</ta>
            <ta e="T227" id="Seg_3089" s="T226">v</ta>
            <ta e="T228" id="Seg_3090" s="T227">conj</ta>
            <ta e="T229" id="Seg_3091" s="T228">pers</ta>
            <ta e="T230" id="Seg_3092" s="T229">adv</ta>
            <ta e="T231" id="Seg_3093" s="T230">adv</ta>
            <ta e="T232" id="Seg_3094" s="T231">adv</ta>
            <ta e="T233" id="Seg_3095" s="T232">n</ta>
            <ta e="T234" id="Seg_3096" s="T233">v</ta>
            <ta e="T235" id="Seg_3097" s="T234">conj</ta>
            <ta e="T236" id="Seg_3098" s="T235">n</ta>
            <ta e="T238" id="Seg_3099" s="T237">v</ta>
            <ta e="T239" id="Seg_3100" s="T238">adv</ta>
            <ta e="T240" id="Seg_3101" s="T239">dempro</ta>
            <ta e="T242" id="Seg_3102" s="T241">v</ta>
            <ta e="T243" id="Seg_3103" s="T242">dempro</ta>
            <ta e="T244" id="Seg_3104" s="T243">v</ta>
            <ta e="T245" id="Seg_3105" s="T244">conj</ta>
            <ta e="T246" id="Seg_3106" s="T245">v</ta>
            <ta e="T247" id="Seg_3107" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_3108" s="T247">dempro</ta>
            <ta e="T249" id="Seg_3109" s="T248">adv</ta>
            <ta e="T250" id="Seg_3110" s="T249">v</ta>
            <ta e="T251" id="Seg_3111" s="T250">refl</ta>
            <ta e="T254" id="Seg_3112" s="T253">n</ta>
            <ta e="T255" id="Seg_3113" s="T254">num</ta>
            <ta e="T256" id="Seg_3114" s="T255">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_3115" s="T3">np.h:E</ta>
            <ta e="T7" id="Seg_3116" s="T6">np.h:E</ta>
            <ta e="T10" id="Seg_3117" s="T9">np.h:E</ta>
            <ta e="T11" id="Seg_3118" s="T10">np.h:A</ta>
            <ta e="T15" id="Seg_3119" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_3120" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_3121" s="T16">np:Th</ta>
            <ta e="T18" id="Seg_3122" s="T17">0.3.h:A</ta>
            <ta e="T20" id="Seg_3123" s="T19">np.h:A 0.3.h:Poss</ta>
            <ta e="T23" id="Seg_3124" s="T22">np:E</ta>
            <ta e="T25" id="Seg_3125" s="T24">np:E</ta>
            <ta e="T26" id="Seg_3126" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_3127" s="T26">np:E</ta>
            <ta e="T28" id="Seg_3128" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_3129" s="T28">pro.h:A</ta>
            <ta e="T32" id="Seg_3130" s="T31">0.3.h:E</ta>
            <ta e="T35" id="Seg_3131" s="T34">np.h:E</ta>
            <ta e="T37" id="Seg_3132" s="T36">np.h:E</ta>
            <ta e="T40" id="Seg_3133" s="T39">0.2.h:E</ta>
            <ta e="T42" id="Seg_3134" s="T41">pro.h:E</ta>
            <ta e="T45" id="Seg_3135" s="T44">np.h:A</ta>
            <ta e="T48" id="Seg_3136" s="T47">pro.h:A</ta>
            <ta e="T51" id="Seg_3137" s="T50">np:P</ta>
            <ta e="T52" id="Seg_3138" s="T51">0.1.h:A</ta>
            <ta e="T53" id="Seg_3139" s="T52">adv:Time</ta>
            <ta e="T54" id="Seg_3140" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_3141" s="T55">adv:Time</ta>
            <ta e="T58" id="Seg_3142" s="T57">np:P</ta>
            <ta e="T59" id="Seg_3143" s="T58">0.2.h:A</ta>
            <ta e="T61" id="Seg_3144" s="T60">pro.h:A</ta>
            <ta e="T62" id="Seg_3145" s="T61">np.h:P</ta>
            <ta e="T64" id="Seg_3146" s="T63">pro.h:Poss</ta>
            <ta e="T65" id="Seg_3147" s="T64">np.h:A</ta>
            <ta e="T70" id="Seg_3148" s="T69">adv:Time</ta>
            <ta e="T71" id="Seg_3149" s="T70">pro.h:A</ta>
            <ta e="T73" id="Seg_3150" s="T72">0.1.h:A</ta>
            <ta e="T75" id="Seg_3151" s="T74">0.1.h:A</ta>
            <ta e="T76" id="Seg_3152" s="T75">adv:Time</ta>
            <ta e="T77" id="Seg_3153" s="T76">np.h:A</ta>
            <ta e="T79" id="Seg_3154" s="T78">np.h:R</ta>
            <ta e="T80" id="Seg_3155" s="T79">0.2.h:A</ta>
            <ta e="T81" id="Seg_3156" s="T80">np:Ins</ta>
            <ta e="T82" id="Seg_3157" s="T81">np:Th</ta>
            <ta e="T83" id="Seg_3158" s="T82">pro.h:A</ta>
            <ta e="T89" id="Seg_3159" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_3160" s="T89">np.h:A</ta>
            <ta e="T92" id="Seg_3161" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_3162" s="T92">np:Ins</ta>
            <ta e="T94" id="Seg_3163" s="T93">np:Th</ta>
            <ta e="T96" id="Seg_3164" s="T95">adv:Time</ta>
            <ta e="T97" id="Seg_3165" s="T96">pro.h:A</ta>
            <ta e="T99" id="Seg_3166" s="T98">0.3.h:A</ta>
            <ta e="T100" id="Seg_3167" s="T99">0.3.h:A</ta>
            <ta e="T102" id="Seg_3168" s="T101">0.3.h:A</ta>
            <ta e="T104" id="Seg_3169" s="T103">np:Th</ta>
            <ta e="T107" id="Seg_3170" s="T106">n:Time</ta>
            <ta e="T111" id="Seg_3171" s="T110">adv:Time</ta>
            <ta e="T112" id="Seg_3172" s="T111">0.3.h:A</ta>
            <ta e="T115" id="Seg_3173" s="T114">np:P</ta>
            <ta e="T117" id="Seg_3174" s="T116">pro.h:Poss</ta>
            <ta e="T118" id="Seg_3175" s="T117">np:G</ta>
            <ta e="T119" id="Seg_3176" s="T118">0.3.h:A</ta>
            <ta e="T120" id="Seg_3177" s="T119">adv:Time</ta>
            <ta e="T121" id="Seg_3178" s="T120">0.3.h:A</ta>
            <ta e="T123" id="Seg_3179" s="T122">0.2.h:A</ta>
            <ta e="T124" id="Seg_3180" s="T123">np.h:A</ta>
            <ta e="T130" id="Seg_3181" s="T129">np.h:A</ta>
            <ta e="T132" id="Seg_3182" s="T131">adv:Time</ta>
            <ta e="T133" id="Seg_3183" s="T132">0.3.h:A</ta>
            <ta e="T135" id="Seg_3184" s="T134">0.3.h:E</ta>
            <ta e="T137" id="Seg_3185" s="T136">pro.h:E</ta>
            <ta e="T139" id="Seg_3186" s="T138">adv:Time</ta>
            <ta e="T140" id="Seg_3187" s="T139">np.h:A</ta>
            <ta e="T142" id="Seg_3188" s="T141">0.3.h:A</ta>
            <ta e="T144" id="Seg_3189" s="T143">0.3.h:E</ta>
            <ta e="T145" id="Seg_3190" s="T144">adv:Time</ta>
            <ta e="T146" id="Seg_3191" s="T145">0.3.h:A</ta>
            <ta e="T147" id="Seg_3192" s="T146">np:G</ta>
            <ta e="T148" id="Seg_3193" s="T147">adv:L</ta>
            <ta e="T149" id="Seg_3194" s="T148">0.3.h:A</ta>
            <ta e="T151" id="Seg_3195" s="T150">np.h:Th</ta>
            <ta e="T152" id="Seg_3196" s="T151">adv:L</ta>
            <ta e="T157" id="Seg_3197" s="T156">np.h:Th</ta>
            <ta e="T159" id="Seg_3198" s="T158">np.h:Th</ta>
            <ta e="T160" id="Seg_3199" s="T159">np:L</ta>
            <ta e="T162" id="Seg_3200" s="T161">adv:Time</ta>
            <ta e="T164" id="Seg_3201" s="T163">np.h:A</ta>
            <ta e="T168" id="Seg_3202" s="T167">adv:Time</ta>
            <ta e="T169" id="Seg_3203" s="T168">np.h:A</ta>
            <ta e="T171" id="Seg_3204" s="T170">pro.h:Poss</ta>
            <ta e="T172" id="Seg_3205" s="T171">np.h:Th</ta>
            <ta e="T174" id="Seg_3206" s="T173">adv:L</ta>
            <ta e="T175" id="Seg_3207" s="T174">np:L</ta>
            <ta e="T176" id="Seg_3208" s="T175">pro.h:E</ta>
            <ta e="T177" id="Seg_3209" s="T176">0.2.h:A</ta>
            <ta e="T178" id="Seg_3210" s="T177">adv:Time</ta>
            <ta e="T180" id="Seg_3211" s="T179">np.h:A</ta>
            <ta e="T183" id="Seg_3212" s="T182">np.h:P</ta>
            <ta e="T189" id="Seg_3213" s="T188">0.3.h:A</ta>
            <ta e="T191" id="Seg_3214" s="T190">np:L</ta>
            <ta e="T192" id="Seg_3215" s="T191">np.h:Th</ta>
            <ta e="T193" id="Seg_3216" s="T192">pro.h:E</ta>
            <ta e="T199" id="Seg_3217" s="T198">0.3.h:A</ta>
            <ta e="T200" id="Seg_3218" s="T199">adv:Time</ta>
            <ta e="T201" id="Seg_3219" s="T200">np.h:A</ta>
            <ta e="T203" id="Seg_3220" s="T202">np.h:A</ta>
            <ta e="T205" id="Seg_3221" s="T204">np.h:A</ta>
            <ta e="T208" id="Seg_3222" s="T207">adv:Time</ta>
            <ta e="T209" id="Seg_3223" s="T208">adv:L</ta>
            <ta e="T211" id="Seg_3224" s="T210">np.h:A</ta>
            <ta e="T213" id="Seg_3225" s="T212">np.h:A</ta>
            <ta e="T215" id="Seg_3226" s="T214">np:G</ta>
            <ta e="T217" id="Seg_3227" s="T216">pro.h:A</ta>
            <ta e="T219" id="Seg_3228" s="T218">adv:Time</ta>
            <ta e="T220" id="Seg_3229" s="T219">pro.h:A</ta>
            <ta e="T222" id="Seg_3230" s="T221">pro.h:Poss</ta>
            <ta e="T225" id="Seg_3231" s="T224">pro.h:A</ta>
            <ta e="T226" id="Seg_3232" s="T225">np.h:P</ta>
            <ta e="T229" id="Seg_3233" s="T228">pro.h:B</ta>
            <ta e="T232" id="Seg_3234" s="T231">adv:Time</ta>
            <ta e="T233" id="Seg_3235" s="T232">np.h:E</ta>
            <ta e="T239" id="Seg_3236" s="T238">adv:Time</ta>
            <ta e="T240" id="Seg_3237" s="T239">pro.h:E</ta>
            <ta e="T243" id="Seg_3238" s="T242">pro.h:E</ta>
            <ta e="T248" id="Seg_3239" s="T247">pro.h:A</ta>
            <ta e="T249" id="Seg_3240" s="T248">adv:Time</ta>
            <ta e="T251" id="Seg_3241" s="T250">pro.h:Poss</ta>
            <ta e="T254" id="Seg_3242" s="T253">np:Pth</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3243" s="T0">v:pred</ta>
            <ta e="T4" id="Seg_3244" s="T3">np.h:S</ta>
            <ta e="T7" id="Seg_3245" s="T6">np.h:S</ta>
            <ta e="T10" id="Seg_3246" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_3247" s="T10">np.h:S</ta>
            <ta e="T14" id="Seg_3248" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_3249" s="T14">np:O</ta>
            <ta e="T16" id="Seg_3250" s="T15">v:pred 0.3.h:S</ta>
            <ta e="T17" id="Seg_3251" s="T16">np:O</ta>
            <ta e="T18" id="Seg_3252" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T20" id="Seg_3253" s="T19">np.h:S</ta>
            <ta e="T22" id="Seg_3254" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_3255" s="T22">np:O</ta>
            <ta e="T25" id="Seg_3256" s="T24">np:O</ta>
            <ta e="T26" id="Seg_3257" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T27" id="Seg_3258" s="T26">np:O</ta>
            <ta e="T28" id="Seg_3259" s="T27">v:pred 0.3.h:S</ta>
            <ta e="T29" id="Seg_3260" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_3261" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_3262" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T35" id="Seg_3263" s="T34">np.h:S</ta>
            <ta e="T37" id="Seg_3264" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_3265" s="T37">v:pred</ta>
            <ta e="T40" id="Seg_3266" s="T39">v:pred 0.2.h:S</ta>
            <ta e="T42" id="Seg_3267" s="T41">pro.h:O</ta>
            <ta e="T43" id="Seg_3268" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_3269" s="T44">np.h:S</ta>
            <ta e="T48" id="Seg_3270" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_3271" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_3272" s="T50">np:O</ta>
            <ta e="T52" id="Seg_3273" s="T51">v:pred 0.1.h:S</ta>
            <ta e="T54" id="Seg_3274" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_3275" s="T54">v:pred</ta>
            <ta e="T58" id="Seg_3276" s="T57">np:O</ta>
            <ta e="T59" id="Seg_3277" s="T58">v:pred 0.2.h:S</ta>
            <ta e="T60" id="Seg_3278" s="T59">ptcl:pred</ta>
            <ta e="T62" id="Seg_3279" s="T61">np.h:O</ta>
            <ta e="T65" id="Seg_3280" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_3281" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_3282" s="T66">ptcl:pred</ta>
            <ta e="T71" id="Seg_3283" s="T70">pro.h:S</ta>
            <ta e="T72" id="Seg_3284" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_3285" s="T72">v:pred 0.1.h:S</ta>
            <ta e="T75" id="Seg_3286" s="T74">v:pred 0.1.h:S</ta>
            <ta e="T77" id="Seg_3287" s="T76">np.h:S</ta>
            <ta e="T78" id="Seg_3288" s="T77">v:pred</ta>
            <ta e="T80" id="Seg_3289" s="T79">v:pred 0.2.h:S</ta>
            <ta e="T82" id="Seg_3290" s="T81">np:O</ta>
            <ta e="T83" id="Seg_3291" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_3292" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_3293" s="T84">ptcl.neg</ta>
            <ta e="T87" id="Seg_3294" s="T86">ptcl.neg</ta>
            <ta e="T88" id="Seg_3295" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_3296" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_3297" s="T90">v:pred</ta>
            <ta e="T94" id="Seg_3298" s="T93">np:S</ta>
            <ta e="T95" id="Seg_3299" s="T94">v:pred</ta>
            <ta e="T97" id="Seg_3300" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_3301" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_3302" s="T98">v:pred 0.3.h:S</ta>
            <ta e="T100" id="Seg_3303" s="T99">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_3304" s="T101">v:pred 0.3.h:S</ta>
            <ta e="T104" id="Seg_3305" s="T103">np:S</ta>
            <ta e="T106" id="Seg_3306" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_3307" s="T106">np:S</ta>
            <ta e="T108" id="Seg_3308" s="T107">v:pred</ta>
            <ta e="T109" id="Seg_3309" s="T108">ptcl:pred</ta>
            <ta e="T112" id="Seg_3310" s="T111">v:pred 0.3.h:S</ta>
            <ta e="T115" id="Seg_3311" s="T114">np:O</ta>
            <ta e="T119" id="Seg_3312" s="T118">v:pred 0.3.h:S</ta>
            <ta e="T121" id="Seg_3313" s="T120">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_3314" s="T122">v:pred 0.2.h:S</ta>
            <ta e="T124" id="Seg_3315" s="T123">np.h:S</ta>
            <ta e="T127" id="Seg_3316" s="T126">v:pred</ta>
            <ta e="T130" id="Seg_3317" s="T129">np:S</ta>
            <ta e="T131" id="Seg_3318" s="T130">v:pred</ta>
            <ta e="T133" id="Seg_3319" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T135" id="Seg_3320" s="T134">v:pred 0.3.h:S</ta>
            <ta e="T137" id="Seg_3321" s="T136">pro.h:S</ta>
            <ta e="T138" id="Seg_3322" s="T137">v:pred</ta>
            <ta e="T140" id="Seg_3323" s="T139">np.h:S</ta>
            <ta e="T141" id="Seg_3324" s="T140">v:pred</ta>
            <ta e="T142" id="Seg_3325" s="T141">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_3326" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T146" id="Seg_3327" s="T145">v:pred 0.3.h:S</ta>
            <ta e="T149" id="Seg_3328" s="T148">v:pred 0.3.h:S</ta>
            <ta e="T151" id="Seg_3329" s="T150">np.h:S</ta>
            <ta e="T153" id="Seg_3330" s="T152">v:pred</ta>
            <ta e="T157" id="Seg_3331" s="T156">np.h:S</ta>
            <ta e="T159" id="Seg_3332" s="T158">np.h:S</ta>
            <ta e="T161" id="Seg_3333" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_3334" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_3335" s="T163">np.h:S</ta>
            <ta e="T169" id="Seg_3336" s="T168">np.h:S</ta>
            <ta e="T170" id="Seg_3337" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_3338" s="T171">np.h:S</ta>
            <ta e="T173" id="Seg_3339" s="T172">v:pred</ta>
            <ta e="T176" id="Seg_3340" s="T175">pro.h:O</ta>
            <ta e="T177" id="Seg_3341" s="T176">v:pred 0.2.h:S</ta>
            <ta e="T180" id="Seg_3342" s="T179">np.h:S</ta>
            <ta e="T182" id="Seg_3343" s="T181">v:pred</ta>
            <ta e="T183" id="Seg_3344" s="T182">np.h:O</ta>
            <ta e="T186" id="Seg_3345" s="T185">np:O</ta>
            <ta e="T189" id="Seg_3346" s="T188">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_3347" s="T190">np:O</ta>
            <ta e="T193" id="Seg_3348" s="T192">pro.h:S</ta>
            <ta e="T196" id="Seg_3349" s="T195">v:pred</ta>
            <ta e="T199" id="Seg_3350" s="T198">v:pred 0.3.h:S</ta>
            <ta e="T201" id="Seg_3351" s="T200">np.h:S</ta>
            <ta e="T203" id="Seg_3352" s="T202">np.h:S</ta>
            <ta e="T205" id="Seg_3353" s="T204">np.h:S</ta>
            <ta e="T206" id="Seg_3354" s="T205">v:pred</ta>
            <ta e="T211" id="Seg_3355" s="T210">np.h:S</ta>
            <ta e="T213" id="Seg_3356" s="T212">np.h:S</ta>
            <ta e="T214" id="Seg_3357" s="T213">v:pred</ta>
            <ta e="T217" id="Seg_3358" s="T216">pro.h:S</ta>
            <ta e="T218" id="Seg_3359" s="T217">v:pred</ta>
            <ta e="T220" id="Seg_3360" s="T219">pro.h:S</ta>
            <ta e="T221" id="Seg_3361" s="T220">v:pred</ta>
            <ta e="T225" id="Seg_3362" s="T224">pro.h:S</ta>
            <ta e="T227" id="Seg_3363" s="T226">v:pred</ta>
            <ta e="T231" id="Seg_3364" s="T230">adj:pred</ta>
            <ta e="T233" id="Seg_3365" s="T232">np.h:S</ta>
            <ta e="T234" id="Seg_3366" s="T233">v:pred</ta>
            <ta e="T240" id="Seg_3367" s="T239">pro.h:S</ta>
            <ta e="T242" id="Seg_3368" s="T241">v:pred</ta>
            <ta e="T243" id="Seg_3369" s="T242">pro.h:S</ta>
            <ta e="T244" id="Seg_3370" s="T243">v:pred</ta>
            <ta e="T246" id="Seg_3371" s="T245">v:pred</ta>
            <ta e="T248" id="Seg_3372" s="T247">pro.h:S</ta>
            <ta e="T250" id="Seg_3373" s="T249">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_3374" s="T4">RUS:gram</ta>
            <ta e="T8" id="Seg_3375" s="T7">RUS:gram</ta>
            <ta e="T17" id="Seg_3376" s="T16">TURK:cult</ta>
            <ta e="T21" id="Seg_3377" s="T20">TURK:disc</ta>
            <ta e="T31" id="Seg_3378" s="T30">RUS:gram</ta>
            <ta e="T33" id="Seg_3379" s="T32">RUS:gram</ta>
            <ta e="T36" id="Seg_3380" s="T35">RUS:gram</ta>
            <ta e="T41" id="Seg_3381" s="T40">RUS:gram</ta>
            <ta e="T50" id="Seg_3382" s="T49">RUS:gram</ta>
            <ta e="T51" id="Seg_3383" s="T50">TURK:cult</ta>
            <ta e="T57" id="Seg_3384" s="T56">TURK:disc</ta>
            <ta e="T58" id="Seg_3385" s="T57">TURK:cult</ta>
            <ta e="T60" id="Seg_3386" s="T59">RUS:mod</ta>
            <ta e="T65" id="Seg_3387" s="T64">TURK:core</ta>
            <ta e="T67" id="Seg_3388" s="T66">RUS:mod</ta>
            <ta e="T82" id="Seg_3389" s="T81">RUS:cult</ta>
            <ta e="T94" id="Seg_3390" s="T93">RUS:cult</ta>
            <ta e="T109" id="Seg_3391" s="T108">RUS:mod</ta>
            <ta e="T116" id="Seg_3392" s="T115">RUS:gram</ta>
            <ta e="T128" id="Seg_3393" s="T127">RUS:gram</ta>
            <ta e="T134" id="Seg_3394" s="T133">RUS:gram</ta>
            <ta e="T143" id="Seg_3395" s="T142">RUS:mod</ta>
            <ta e="T150" id="Seg_3396" s="T149">RUS:gram</ta>
            <ta e="T154" id="Seg_3397" s="T153">RUS:gram</ta>
            <ta e="T158" id="Seg_3398" s="T157">RUS:gram</ta>
            <ta e="T164" id="Seg_3399" s="T163">RUS:core</ta>
            <ta e="T167" id="Seg_3400" s="T166">RUS:core</ta>
            <ta e="T183" id="Seg_3401" s="T182">RUS:core</ta>
            <ta e="T184" id="Seg_3402" s="T183">RUS:gram</ta>
            <ta e="T187" id="Seg_3403" s="T186">RUS:gram</ta>
            <ta e="T190" id="Seg_3404" s="T189">TURK:core</ta>
            <ta e="T194" id="Seg_3405" s="T193">TURK:disc</ta>
            <ta e="T195" id="Seg_3406" s="T194">RUS:gram</ta>
            <ta e="T197" id="Seg_3407" s="T196">RUS:gram</ta>
            <ta e="T202" id="Seg_3408" s="T201">RUS:gram</ta>
            <ta e="T204" id="Seg_3409" s="T203">RUS:gram</ta>
            <ta e="T211" id="Seg_3410" s="T210">RUS:core</ta>
            <ta e="T216" id="Seg_3411" s="T215">RUS:gram</ta>
            <ta e="T228" id="Seg_3412" s="T227">RUS:gram</ta>
            <ta e="T235" id="Seg_3413" s="T234">RUS:gram</ta>
            <ta e="T236" id="Seg_3414" s="T235">RUS:core</ta>
            <ta e="T245" id="Seg_3415" s="T244">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T10" id="Seg_3416" s="T0">Жили трое, козёл и баран и кот.</ta>
            <ta e="T14" id="Seg_3417" s="T10">Кот всё время [что-нибудь] воровал.</ta>
            <ta e="T16" id="Seg_3418" s="T14">Мясо [стащит и] ест.</ta>
            <ta e="T18" id="Seg_3419" s="T16">Сметану [стащит и] ест.</ta>
            <ta e="T28" id="Seg_3420" s="T18">Потом [хозяина] жена по ноге его ударила, нога у него заболела.</ta>
            <ta e="T32" id="Seg_3421" s="T28">Он идёт и плачет.</ta>
            <ta e="T38" id="Seg_3422" s="T32">А [те] два, баран и козёл, лежат.</ta>
            <ta e="T40" id="Seg_3423" s="T38">«Почему ты плачешь?»</ta>
            <ta e="T45" id="Seg_3424" s="T40">«Да меня побила эта женщина».</ta>
            <ta e="T49" id="Seg_3425" s="T45">«Что ты наделал?»</ta>
            <ta e="T52" id="Seg_3426" s="T49">«Да сметану съел.</ta>
            <ta e="T55" id="Seg_3427" s="T52">Потом она сказала:</ta>
            <ta e="T59" id="Seg_3428" s="T55">“Ты теперь всю сметану съел.</ta>
            <ta e="T63" id="Seg_3429" s="T59">Мне придётся козла зарезать.</ta>
            <ta e="T66" id="Seg_3430" s="T63">Мои родственники приедут.</ta>
            <ta e="T69" id="Seg_3431" s="T66">Надо их кормить”».</ta>
            <ta e="T72" id="Seg_3432" s="T69">Потом они говорят:</ta>
            <ta e="T74" id="Seg_3433" s="T72">«Пойдёмте втроём.</ta>
            <ta e="T75" id="Seg_3434" s="T74">Мы спрячемся».</ta>
            <ta e="T79" id="Seg_3435" s="T75">Потом один [из них] говорит барану:</ta>
            <ta e="T82" id="Seg_3436" s="T79">«Ударь головой в ворота».</ta>
            <ta e="T84" id="Seg_3437" s="T82">Он ударил.</ta>
            <ta e="T88" id="Seg_3438" s="T84">[Ворота] не открылись.</ta>
            <ta e="T93" id="Seg_3439" s="T88">Тогда козёл удаил своей головой.</ta>
            <ta e="T95" id="Seg_3440" s="T93">Ворота открылись.</ta>
            <ta e="T98" id="Seg_3441" s="T95">Потом они убежали.</ta>
            <ta e="T106" id="Seg_3442" s="T98">Бежали, бежали, потом пришли, там травы много стояло.</ta>
            <ta e="T108" id="Seg_3443" s="T106">Наступил вечер.</ta>
            <ta e="T110" id="Seg_3444" s="T108">Надо ночевать.</ta>
            <ta e="T115" id="Seg_3445" s="T110">Потом [кот?] содрал кору с берёзы.</ta>
            <ta e="T119" id="Seg_3446" s="T115">Надел на рога [козлу].</ta>
            <ta e="T123" id="Seg_3447" s="T119">Потом сказал: «Ударьтесь [лбами]!»</ta>
            <ta e="T127" id="Seg_3448" s="T123">Они ударились [друг с другом].</ta>
            <ta e="T131" id="Seg_3449" s="T127">И огонь загорелся.</ta>
            <ta e="T135" id="Seg_3450" s="T131">Тогда они сели и согрелись.</ta>
            <ta e="T136" id="Seg_3451" s="T135">Останови [плёнку].</ta>
            <ta e="T138" id="Seg_3452" s="T136">Они согрелись.</ta>
            <ta e="T141" id="Seg_3453" s="T138">Потом пришёл медведь.</ta>
            <ta e="T144" id="Seg_3454" s="T141">Сел, тоже согрелся.</ta>
            <ta e="T149" id="Seg_3455" s="T144">Потом пошёл к [стогу] сена, там лёг.</ta>
            <ta e="T153" id="Seg_3456" s="T149">А кот сел сверху [стога].</ta>
            <ta e="T161" id="Seg_3457" s="T153">А баран и козёл у огня легли.</ta>
            <ta e="T164" id="Seg_3458" s="T161">Потом пришли волки.</ta>
            <ta e="T167" id="Seg_3459" s="T164">Один [был] белый волк.</ta>
            <ta e="T170" id="Seg_3460" s="T167">Тогда кот говорит:</ta>
            <ta e="T175" id="Seg_3461" s="T170">«Там у стога наш брат лежит.</ta>
            <ta e="T177" id="Seg_3462" s="T175">Согрейте(?) его».</ta>
            <ta e="T184" id="Seg_3463" s="T177">Тогда этот медведь взял одного волка.</ta>
            <ta e="T186" id="Seg_3464" s="T184">В одну лапу.</ta>
            <ta e="T192" id="Seg_3465" s="T186">А другого взял в другую лапу.</ta>
            <ta e="T199" id="Seg_3466" s="T192">Они все [как] испугались и убежали.</ta>
            <ta e="T207" id="Seg_3467" s="T199">Потом баран и козёл и кот снова убежали.</ta>
            <ta e="T211" id="Seg_3468" s="T207">Потом там опять волки.</ta>
            <ta e="T215" id="Seg_3469" s="T211">Кот залез на дерево.</ta>
            <ta e="T218" id="Seg_3470" s="T215">А они [козёл и баран] повисли [на ветке, зацепившись ногами].</ta>
            <ta e="T221" id="Seg_3471" s="T218">Потом [кот] говорит [козлу]:</ta>
            <ta e="T223" id="Seg_3472" s="T221">«Брат!</ta>
            <ta e="T231" id="Seg_3473" s="T223">Я двоих [волков] возьму, а тебе сколько нужно?»</ta>
            <ta e="T238" id="Seg_3474" s="T231">Потом козёл упал и волку (?).</ta>
            <ta e="T242" id="Seg_3475" s="T238">Потом он испугался.</ta>
            <ta e="T246" id="Seg_3476" s="T242">Они испугались и убежали.</ta>
            <ta e="T247" id="Seg_3477" s="T246">Хватит!</ta>
            <ta e="T255" id="Seg_3478" s="T247">Потом они трое пошли [каждый?] своей дорогой.</ta>
            <ta e="T256" id="Seg_3479" s="T255">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_3480" s="T0">[Once there] lived a male goat, a male sheep and a male (cat?).</ta>
            <ta e="T14" id="Seg_3481" s="T10">The cat would always steal [something].</ta>
            <ta e="T16" id="Seg_3482" s="T14">He [steals and] eats meat.</ta>
            <ta e="T18" id="Seg_3483" s="T16">He [steals and] eats cream.</ta>
            <ta e="T28" id="Seg_3484" s="T18">Then the [master's] wife hit his foot, hit his foot, hurt his foot.</ta>
            <ta e="T32" id="Seg_3485" s="T28">He comes and cries.</ta>
            <ta e="T38" id="Seg_3486" s="T32">And the two, the sheep and the goat, are lying.</ta>
            <ta e="T40" id="Seg_3487" s="T38">"Why are you crying?"</ta>
            <ta e="T45" id="Seg_3488" s="T40">"This woman hit me".</ta>
            <ta e="T49" id="Seg_3489" s="T45">"What did you do?"</ta>
            <ta e="T52" id="Seg_3490" s="T49">"I ate the cream.</ta>
            <ta e="T55" id="Seg_3491" s="T52">Then she [the mistress] says:</ta>
            <ta e="T59" id="Seg_3492" s="T55">"Now you ate all of the cream</ta>
            <ta e="T63" id="Seg_3493" s="T59">I need to slaughter the goat.</ta>
            <ta e="T66" id="Seg_3494" s="T63">My relatives are coming.</ta>
            <ta e="T69" id="Seg_3495" s="T66">I need to give them [something] to eat."</ta>
            <ta e="T72" id="Seg_3496" s="T69">Then they say:</ta>
            <ta e="T74" id="Seg_3497" s="T72">"Let's go, the three of us.</ta>
            <ta e="T75" id="Seg_3498" s="T74">We’ll hide.</ta>
            <ta e="T79" id="Seg_3499" s="T75">Then one [of them] says to the sheep:</ta>
            <ta e="T82" id="Seg_3500" s="T79">"Ram the gate with your head!"</ta>
            <ta e="T84" id="Seg_3501" s="T82">He rammed [it].</ta>
            <ta e="T88" id="Seg_3502" s="T84">(He couldn't) It [the gate] didn't open.</ta>
            <ta e="T93" id="Seg_3503" s="T88">Then the goat rammed [it] with his own head.</ta>
            <ta e="T95" id="Seg_3504" s="T93">The gate opened.</ta>
            <ta e="T98" id="Seg_3505" s="T95">Then they ran.</ta>
            <ta e="T106" id="Seg_3506" s="T98">They ran, they ran, then they arrived, there is a lot of grass standing.</ta>
            <ta e="T108" id="Seg_3507" s="T106">The evening came.</ta>
            <ta e="T110" id="Seg_3508" s="T108">[They] have to spend the night [somewhere].</ta>
            <ta e="T115" id="Seg_3509" s="T110">Then he [the cat?] skinned a birch tree.</ta>
            <ta e="T119" id="Seg_3510" s="T115">And put it on the [goat’s] horns.</ta>
            <ta e="T123" id="Seg_3511" s="T119">Then he says: “Ram!”</ta>
            <ta e="T127" id="Seg_3512" s="T123">They rammed one another.</ta>
            <ta e="T131" id="Seg_3513" s="T127">And fire started burning.</ta>
            <ta e="T135" id="Seg_3514" s="T131">Then they sat down and got warm.</ta>
            <ta e="T136" id="Seg_3515" s="T135">Stop [the tape].</ta>
            <ta e="T138" id="Seg_3516" s="T136">They became warm.</ta>
            <ta e="T141" id="Seg_3517" s="T138">Then a bear came.</ta>
            <ta e="T144" id="Seg_3518" s="T141">He sat down, also became warm.</ta>
            <ta e="T149" id="Seg_3519" s="T144">Then he went to the hay[stack], lied there.</ta>
            <ta e="T153" id="Seg_3520" s="T149">But the tospak sat on top [of the haystack].</ta>
            <ta e="T161" id="Seg_3521" s="T153">And the sheep and the goat lie by the fire.</ta>
            <ta e="T164" id="Seg_3522" s="T161">Then wolves came.</ta>
            <ta e="T167" id="Seg_3523" s="T164">One (was) white wolf.</ta>
            <ta e="T170" id="Seg_3524" s="T167">Then the tospak says:</ta>
            <ta e="T175" id="Seg_3525" s="T170">“Our brother lies there by the haystack.</ta>
            <ta e="T177" id="Seg_3526" s="T175">Warm(?) him up.”</ta>
            <ta e="T184" id="Seg_3527" s="T177">Then the bear took one wolf .</ta>
            <ta e="T186" id="Seg_3528" s="T184">One into his one paw.</ta>
            <ta e="T192" id="Seg_3529" s="T186">And he took one [other] into his other paw.</ta>
            <ta e="T199" id="Seg_3530" s="T192">They got scared and ran off.</ta>
            <ta e="T207" id="Seg_3531" s="T199">Then the sheep and the goat and the tospak ran again.</ta>
            <ta e="T211" id="Seg_3532" s="T207">Then there [are] wolves again.</ta>
            <ta e="T215" id="Seg_3533" s="T211">The cat climbed onto a tree.</ta>
            <ta e="T218" id="Seg_3534" s="T215">And they [the goat and the sheep] hang [on the tree, holding on to a branch with their forelegs].</ta>
            <ta e="T221" id="Seg_3535" s="T218">Then he says [to the goat]:</ta>
            <ta e="T223" id="Seg_3536" s="T221">"My brother.</ta>
            <ta e="T231" id="Seg_3537" s="T223">I will take two [wolves], and you, how many do you need?"</ta>
            <ta e="T238" id="Seg_3538" s="T231">Then the goat fell down and (?) on the wolf.</ta>
            <ta e="T242" id="Seg_3539" s="T238">Then he got scared.</ta>
            <ta e="T246" id="Seg_3540" s="T242">They got scared and ran away.</ta>
            <ta e="T247" id="Seg_3541" s="T246">Enough!</ta>
            <ta e="T255" id="Seg_3542" s="T247">Then they went their own ways, the three of them.</ta>
            <ta e="T256" id="Seg_3543" s="T255">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_3544" s="T0">Es lebte einmal ein Bock, ein Widder und ein Kater. </ta>
            <ta e="T14" id="Seg_3545" s="T10">Der Kater stahl immer [etwas].</ta>
            <ta e="T16" id="Seg_3546" s="T14">Er [stiehlt und] isst Fleisch.</ta>
            <ta e="T18" id="Seg_3547" s="T16">Er [stiehlt und] isst Sahne</ta>
            <ta e="T28" id="Seg_3548" s="T18">Dann schlug die Frau [des Meisters] seinen Fuß, schlug seinen Fuß, verletzte seinen Fuß.</ta>
            <ta e="T32" id="Seg_3549" s="T28">Er kommt und weint.</ta>
            <ta e="T38" id="Seg_3550" s="T32">Und die zwei, der Widder und der Bock, liegen.</ta>
            <ta e="T40" id="Seg_3551" s="T38">"Warum weinst du?"</ta>
            <ta e="T45" id="Seg_3552" s="T40">"Diese Frau hat mich geschlagen."</ta>
            <ta e="T49" id="Seg_3553" s="T45">"Was hast du gemacht?"</ta>
            <ta e="T52" id="Seg_3554" s="T49">"Ich habe die Sahne gegessen."</ta>
            <ta e="T55" id="Seg_3555" s="T52">Dann sagte sie [die Frau]:</ta>
            <ta e="T59" id="Seg_3556" s="T55">"Jetzt hast du alle Sahne gegessen.</ta>
            <ta e="T63" id="Seg_3557" s="T59">Ich muss die Ziege schlachten.</ta>
            <ta e="T66" id="Seg_3558" s="T63">Meine Verwandten kommen.</ta>
            <ta e="T69" id="Seg_3559" s="T66">Ich muss ihnen [etwas] zu essen geben."</ta>
            <ta e="T72" id="Seg_3560" s="T69">Dann sagten sie: </ta>
            <ta e="T74" id="Seg_3561" s="T72">"Lass uns gehen, wir drei zusammen.</ta>
            <ta e="T75" id="Seg_3562" s="T74">Wir verstecken uns.</ta>
            <ta e="T79" id="Seg_3563" s="T75">Dann sagte einer [von ihnen] zum Widder: </ta>
            <ta e="T82" id="Seg_3564" s="T79">"Ramme das Tor mit deinem Kopf."</ta>
            <ta e="T84" id="Seg_3565" s="T82">Er rammte [es].</ta>
            <ta e="T88" id="Seg_3566" s="T84">(Er konnte nicht). Es [das Tor] öffnete sich nicht. </ta>
            <ta e="T93" id="Seg_3567" s="T88">Der Bock rammte [es] mit seinem eigenen Kopf.</ta>
            <ta e="T95" id="Seg_3568" s="T93">Das Tor öffnete sich.</ta>
            <ta e="T98" id="Seg_3569" s="T95">Dann rannten sie.</ta>
            <ta e="T106" id="Seg_3570" s="T98">Sie rannten, sie rannten, dann kammen sie an, dort war viel Gras. </ta>
            <ta e="T108" id="Seg_3571" s="T106">Es wurde abend.</ta>
            <ta e="T110" id="Seg_3572" s="T108">[Sie] mussten die Nacht [irgendwo] verbringen.</ta>
            <ta e="T115" id="Seg_3573" s="T110">Dann häutete er [der Kater?] eine Birke.</ta>
            <ta e="T119" id="Seg_3574" s="T115">Und tat es auf die Hörner [des Bocks].</ta>
            <ta e="T123" id="Seg_3575" s="T119">Dann sagte er: "Ramme!"</ta>
            <ta e="T127" id="Seg_3576" s="T123">Sie rammten ineinander.</ta>
            <ta e="T131" id="Seg_3577" s="T127">Und das Feuer begann zu brennen.</ta>
            <ta e="T135" id="Seg_3578" s="T131">Dann setzten sie sich hin und wurden warm.</ta>
            <ta e="T136" id="Seg_3579" s="T135">Stoppe [das Band].</ta>
            <ta e="T138" id="Seg_3580" s="T136">Ihnen wurde warm.</ta>
            <ta e="T141" id="Seg_3581" s="T138">Dann kam ein Bär.</ta>
            <ta e="T144" id="Seg_3582" s="T141">Er setzte sich, ihm wurde auch warm.</ta>
            <ta e="T149" id="Seg_3583" s="T144">Dann ging er zum Heu[haufen], legte sich dort hin.</ta>
            <ta e="T153" id="Seg_3584" s="T149">Aber der Kater saß oben [auf dem Hauhaufen].</ta>
            <ta e="T161" id="Seg_3585" s="T153">Und der Widder und der Bock lagen am Feuer.</ta>
            <ta e="T164" id="Seg_3586" s="T161">Dann kamen Wölfe.</ta>
            <ta e="T167" id="Seg_3587" s="T164">Einer [war] ein weißer Wolf.</ta>
            <ta e="T170" id="Seg_3588" s="T167">Dann sagte der Kater: </ta>
            <ta e="T175" id="Seg_3589" s="T170">"Unser Bruder liegt dort beim Heuhaufen.</ta>
            <ta e="T177" id="Seg_3590" s="T175">Wärme(?) ihn."</ta>
            <ta e="T184" id="Seg_3591" s="T177">Dann nahm der Bär einen Wolf.</ta>
            <ta e="T186" id="Seg_3592" s="T184">Einen in seine Tatze.</ta>
            <ta e="T192" id="Seg_3593" s="T186">Und er nahm einen [anderen] in seine andere Tatze.</ta>
            <ta e="T199" id="Seg_3594" s="T192">Sie bekamen Angst und rannten weg.</ta>
            <ta e="T207" id="Seg_3595" s="T199">Dann der Widder und der Bock und der Kater rannten wieder.</ta>
            <ta e="T211" id="Seg_3596" s="T207">Dann waren da wieder Wölfe.</ta>
            <ta e="T215" id="Seg_3597" s="T211">Der Kater kletterte auf einen Baum.</ta>
            <ta e="T218" id="Seg_3598" s="T215">Und sie [der Bock und der Widder] hängen [am Baum, sie halten einen Ast mit ihren Vorderläufen].</ta>
            <ta e="T221" id="Seg_3599" s="T218">Dann sagte er [zum Bock]:</ta>
            <ta e="T223" id="Seg_3600" s="T221">"Mein Bruder.</ta>
            <ta e="T231" id="Seg_3601" s="T223">Ich werde zwei [Wölfe] nehmen, und du, wie viele brauchst du?"</ta>
            <ta e="T238" id="Seg_3602" s="T231">Dann fiel der Bock runter und (?) auf den Wolf.</ta>
            <ta e="T242" id="Seg_3603" s="T238">Dann bekam er Angst.</ta>
            <ta e="T246" id="Seg_3604" s="T242">Sie bekamen Angst und rannten weg.</ta>
            <ta e="T247" id="Seg_3605" s="T246">Genug!</ta>
            <ta e="T255" id="Seg_3606" s="T247">Dann gingen sie ihre eigenen Wege, die drei von ihnen.</ta>
            <ta e="T256" id="Seg_3607" s="T255">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_3608" s="T0">[GVY:] A tale about a Goat, a Sheep and a Cat (see e.g. http://narodstory.net/russkie-skazki.php?id=33). Cf. Khakas tospax, tošpax 'fat, thick'? [AAV]: Tape SU0192</ta>
            <ta e="T38" id="Seg_3609" s="T32">3SG instead of 3PL.</ta>
            <ta e="T88" id="Seg_3610" s="T84">[GVY:] Here pronounced as kajluʔpiʔi, but cf. the same form below. The plural comes from the Russan vorota (pl.t.).</ta>
            <ta e="T131" id="Seg_3611" s="T127">[KlT:] (They hit their horns together while wearing the birchbark on their horns to get sparks and start the fire).</ta>
            <ta e="T135" id="Seg_3612" s="T131">[KlT:] Ejüm- to become warm; amnol instead of amnə-. [GVY:] The verbal stem is pronounced here in different ways: ejumneʔ-piʔi, ejumnuʔ-piʔi, ejumnu-bi. Donner (17a, 18a) has ejumne-</ta>
            <ta e="T138" id="Seg_3613" s="T136">[GVY:] Pronounced ejümnuʔbiʔi.</ta>
            <ta e="T144" id="Seg_3614" s="T141">[AAV] amnəl- instead of amnə-?</ta>
            <ta e="T161" id="Seg_3615" s="T153">3SG instead of 3PL.</ta>
            <ta e="T175" id="Seg_3616" s="T170">[AAV:] Referring to the bear.</ta>
            <ta e="T177" id="Seg_3617" s="T175">[GVY:] Unclear.</ta>
            <ta e="T211" id="Seg_3618" s="T207">[AAV:] They met wolves again in the woods. </ta>
            <ta e="T238" id="Seg_3619" s="T231">[GVY:] naʔamzum and the whole sentence unclear. Cf. naʔma 'branch; arm'; namzəm- 'hurt' (Donner 43a,b); amnu 'horn'.</ta>
            <ta e="T242" id="Seg_3620" s="T238">[GVY:] This whole sentence may be a slip of the tongue.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
