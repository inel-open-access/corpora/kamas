<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6D6F31C3-28DF-82A2-1DFF-76A3F77CEDA2">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FairyPot_flk.wav" />
         <referenced-file url="PKZ_196X_FairyPot_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FairyPot_flk\PKZ_196X_FairyPot_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">193</ud-information>
            <ud-information attribute-name="# HIAT:w">130</ud-information>
            <ud-information attribute-name="# e">130</ud-information>
            <ud-information attribute-name="# HIAT:u">24</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T606" time="0.148" type="appl" />
         <tli id="T607" time="1.113" type="appl" />
         <tli id="T608" time="2.078" type="appl" />
         <tli id="T609" time="3.043" type="appl" />
         <tli id="T610" time="4.007" type="appl" />
         <tli id="T611" time="4.972" type="appl" />
         <tli id="T612" time="5.937" type="appl" />
         <tli id="T613" time="6.722" type="appl" />
         <tli id="T614" time="7.493" type="appl" />
         <tli id="T615" time="8.264" type="appl" />
         <tli id="T616" time="9.54654369755474" />
         <tli id="T617" time="10.178" type="appl" />
         <tli id="T618" time="10.802" type="appl" />
         <tli id="T619" time="11.939846202737806" />
         <tli id="T620" time="12.765" type="appl" />
         <tli id="T621" time="13.993153087407402" />
         <tli id="T622" time="14.774" type="appl" />
         <tli id="T623" time="15.451" type="appl" />
         <tli id="T624" time="16.128" type="appl" />
         <tli id="T625" time="16.806" type="appl" />
         <tli id="T626" time="17.483" type="appl" />
         <tli id="T627" time="18.49309512361512" />
         <tli id="T628" time="18.925" type="appl" />
         <tli id="T629" time="19.424" type="appl" />
         <tli id="T630" time="19.924" type="appl" />
         <tli id="T631" time="20.424" type="appl" />
         <tli id="T632" time="20.923" type="appl" />
         <tli id="T633" time="21.423" type="appl" />
         <tli id="T634" time="22.251" type="appl" />
         <tli id="T635" time="23.079" type="appl" />
         <tli id="T636" time="23.907" type="appl" />
         <tli id="T637" time="24.89967926701603" />
         <tli id="T638" time="25.61" type="appl" />
         <tli id="T639" time="26.294" type="appl" />
         <tli id="T640" time="26.807" type="appl" />
         <tli id="T641" time="27.319" type="appl" />
         <tli id="T642" time="28.91296090523387" />
         <tli id="T643" time="29.681" type="appl" />
         <tli id="T644" time="30.419" type="appl" />
         <tli id="T645" time="31.158" type="appl" />
         <tli id="T646" time="31.896" type="appl" />
         <tli id="T647" time="32.82624383153599" />
         <tli id="T648" time="33.531" type="appl" />
         <tli id="T649" time="34.157" type="appl" />
         <tli id="T650" time="34.782" type="appl" />
         <tli id="T651" time="35.407" type="appl" />
         <tli id="T652" time="36.033" type="appl" />
         <tli id="T653" time="36.658" type="appl" />
         <tli id="T654" time="37.284" type="appl" />
         <tli id="T655" time="37.909" type="appl" />
         <tli id="T656" time="38.534" type="appl" />
         <tli id="T657" time="39.16" type="appl" />
         <tli id="T658" time="39.785" type="appl" />
         <tli id="T659" time="40.41" type="appl" />
         <tli id="T660" time="41.036" type="appl" />
         <tli id="T661" time="42.35278778670759" />
         <tli id="T662" time="43.646104460817654" />
         <tli id="T663" time="44.219" type="appl" />
         <tli id="T664" time="44.724" type="appl" />
         <tli id="T665" time="46.06607328917825" />
         <tli id="T666" time="46.835" type="appl" />
         <tli id="T667" time="47.569" type="appl" />
         <tli id="T668" time="48.304" type="appl" />
         <tli id="T669" time="49.038" type="appl" />
         <tli id="T670" time="49.773" type="appl" />
         <tli id="T671" time="50.507" type="appl" />
         <tli id="T672" time="51.242" type="appl" />
         <tli id="T673" time="51.976" type="appl" />
         <tli id="T674" time="53.32597977426003" />
         <tli id="T675" time="53.763" type="appl" />
         <tli id="T676" time="54.517" type="appl" />
         <tli id="T677" time="55.272" type="appl" />
         <tli id="T678" time="56.026" type="appl" />
         <tli id="T679" time="56.781" type="appl" />
         <tli id="T680" time="57.536" type="appl" />
         <tli id="T681" time="58.29" type="appl" />
         <tli id="T682" time="60.12589218452947" />
         <tli id="T683" time="60.754" type="appl" />
         <tli id="T684" time="61.297" type="appl" />
         <tli id="T685" time="61.84" type="appl" />
         <tli id="T686" time="62.384" type="appl" />
         <tli id="T687" time="62.927" type="appl" />
         <tli id="T688" time="63.57918103601924" />
         <tli id="T689" time="64.795" type="appl" />
         <tli id="T690" time="65.86" type="appl" />
         <tli id="T691" time="66.723" type="appl" />
         <tli id="T692" time="67.453" type="appl" />
         <tli id="T693" time="68.184" type="appl" />
         <tli id="T694" time="68.914" type="appl" />
         <tli id="T695" time="69.88576646750442" />
         <tli id="T696" time="70.998" type="appl" />
         <tli id="T697" time="72.034" type="appl" />
         <tli id="T698" time="73.35238848058296" />
         <tli id="T699" time="74.038" type="appl" />
         <tli id="T700" time="74.868" type="appl" />
         <tli id="T701" time="75.698" type="appl" />
         <tli id="T702" time="76.527" type="appl" />
         <tli id="T703" time="77.357" type="appl" />
         <tli id="T704" time="78.35232407636931" />
         <tli id="T705" time="78.902" type="appl" />
         <tli id="T706" time="79.464" type="appl" />
         <tli id="T707" time="80.027" type="appl" />
         <tli id="T708" time="80.59" type="appl" />
         <tli id="T709" time="81.153" type="appl" />
         <tli id="T710" time="81.715" type="appl" />
         <tli id="T711" time="82.278" type="appl" />
         <tli id="T712" time="82.99226430925904" />
         <tli id="T713" time="83.764" type="appl" />
         <tli id="T714" time="84.409" type="appl" />
         <tli id="T715" time="85.055" type="appl" />
         <tli id="T716" time="85.7" type="appl" />
         <tli id="T717" time="86.80554852364544" />
         <tli id="T718" time="87.569" type="appl" />
         <tli id="T719" time="88.32" type="appl" />
         <tli id="T720" time="89.072" type="appl" />
         <tli id="T721" time="89.823" type="appl" />
         <tli id="T722" time="90.575" type="appl" />
         <tli id="T723" time="91.326" type="appl" />
         <tli id="T724" time="92.078" type="appl" />
         <tli id="T725" time="92.697" type="appl" />
         <tli id="T726" time="93.88545732727891" />
         <tli id="T727" time="94.456" type="appl" />
         <tli id="T728" time="95.084" type="appl" />
         <tli id="T729" time="95.711" type="appl" />
         <tli id="T730" time="96.338" type="appl" />
         <tli id="T731" time="96.965" type="appl" />
         <tli id="T732" time="97.593" type="appl" />
         <tli id="T733" time="98.22" type="appl" />
         <tli id="T734" time="98.847" type="appl" />
         <tli id="T735" time="99.474" type="appl" />
         <tli id="T736" time="100.102" type="appl" />
         <tli id="T737" time="100.729" type="appl" />
         <tli id="T738" time="101.692" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T738" id="Seg_0" n="sc" s="T606">
               <ts e="T612" id="Seg_2" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_4" n="HIAT:w" s="T606">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_7" n="HIAT:w" s="T607">onʼiʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_10" n="HIAT:w" s="T608">turagən</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_13" n="HIAT:w" s="T609">koʔbdo</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_16" n="HIAT:w" s="T610">i</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_19" n="HIAT:w" s="T611">nʼi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_23" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_25" n="HIAT:w" s="T612">Dĭzeŋdə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_28" n="HIAT:w" s="T613">amzittə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_31" n="HIAT:w" s="T614">nagabi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_34" n="HIAT:w" s="T615">ĭmbidə</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_38" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_40" n="HIAT:w" s="T616">Amzittə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_43" n="HIAT:w" s="T617">naga</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_46" n="HIAT:w" s="T618">dĭzeŋ</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_50" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_52" n="HIAT:w" s="T619">Koʔbdo</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_55" n="HIAT:w" s="T620">kambi</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T627" id="Seg_59" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_61" n="HIAT:w" s="T621">Šonəga</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_65" n="HIAT:w" s="T622">šonəga</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_68" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_70" n="HIAT:w" s="T623">nuke-</ts>
                  <nts id="Seg_71" n="HIAT:ip">)</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_74" n="HIAT:w" s="T624">nüke</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_77" n="HIAT:w" s="T625">bü</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_80" n="HIAT:w" s="T626">detleʔbə</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T633" id="Seg_84" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_86" n="HIAT:w" s="T627">Dĭ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_89" n="HIAT:w" s="T628">măndə:</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_91" n="HIAT:ip">"</nts>
                  <ts e="T630" id="Seg_93" n="HIAT:w" s="T629">Deʔ</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_96" n="HIAT:w" s="T630">măn</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_99" n="HIAT:w" s="T631">tănan</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_102" n="HIAT:w" s="T632">kabažərlam</ts>
                  <nts id="Seg_103" n="HIAT:ip">"</nts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_107" n="HIAT:u" s="T633">
                  <ts e="T634" id="Seg_109" n="HIAT:w" s="T633">Dĭgəttə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_112" n="HIAT:w" s="T634">šidegöʔ</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_115" n="HIAT:w" s="T635">kumbiʔi</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_119" n="HIAT:w" s="T636">kumbiʔi</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_123" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_125" n="HIAT:w" s="T637">Deʔpiʔi</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_128" n="HIAT:w" s="T638">maːndə</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_132" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_134" n="HIAT:w" s="T639">Dĭgəttə</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_137" n="HIAT:w" s="T640">dĭ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_140" n="HIAT:w" s="T641">măndə:</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_142" n="HIAT:ip">"</nts>
                  <ts e="T643" id="Seg_144" n="HIAT:w" s="T642">Măn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_147" n="HIAT:w" s="T643">tănan</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_150" n="HIAT:w" s="T644">mĭlem</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_153" n="HIAT:w" s="T645">šojdʼo</ts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_157" n="HIAT:w" s="T646">kanaʔ</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_161" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_163" n="HIAT:w" s="T647">Da</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_166" n="HIAT:w" s="T648">dĭ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_169" n="HIAT:w" s="T649">bostə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_172" n="HIAT:w" s="T650">mĭnzərləj</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_175" n="HIAT:w" s="T651">tănan</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_178" n="HIAT:w" s="T652">kaša</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_181" n="HIAT:ip">(</nts>
                  <ts e="T654" id="Seg_183" n="HIAT:w" s="T653">am-</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_186" n="HIAT:w" s="T654">a</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_189" n="HIAT:w" s="T655">manaʔ-</ts>
                  <nts id="Seg_190" n="HIAT:ip">)</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_193" n="HIAT:w" s="T656">a</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_196" n="HIAT:w" s="T657">dĭgəttə</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_198" n="HIAT:ip">(</nts>
                  <ts e="T659" id="Seg_200" n="HIAT:w" s="T658">măn-</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_203" n="HIAT:w" s="T659">măna-</ts>
                  <nts id="Seg_204" n="HIAT:ip">)</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_207" n="HIAT:w" s="T660">măndə:</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_209" n="HIAT:ip">"</nts>
                  <ts e="T662" id="Seg_211" n="HIAT:w" s="T661">Pasiba</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T665" id="Seg_215" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_217" n="HIAT:w" s="T662">Ej</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_220" n="HIAT:w" s="T663">kereʔ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_223" n="HIAT:w" s="T664">daška</ts>
                  <nts id="Seg_224" n="HIAT:ip">"</nts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_228" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_230" n="HIAT:w" s="T665">Dĭgəttə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_233" n="HIAT:w" s="T666">dĭ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_236" n="HIAT:w" s="T667">šobi</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ts e="T669" id="Seg_240" n="HIAT:w" s="T668">i</ts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_244" n="HIAT:w" s="T669">maːndə</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_247" n="HIAT:ip">(</nts>
                  <ts e="T672" id="Seg_249" n="HIAT:w" s="T670">davaj=</ts>
                  <nts id="Seg_250" n="HIAT:ip">)</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_253" n="HIAT:w" s="T672">šüm</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_256" n="HIAT:w" s="T673">embi</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_260" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_262" n="HIAT:w" s="T674">Dĭ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_265" n="HIAT:w" s="T675">šojdʼom</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_269" n="HIAT:w" s="T677">dĭ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_272" n="HIAT:w" s="T678">bar</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_275" n="HIAT:w" s="T679">mĭnzərleʔbə</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_279" n="HIAT:w" s="T680">iʔgö</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_282" n="HIAT:w" s="T681">mĭnzərbi</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_286" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_288" n="HIAT:w" s="T682">Măndə:</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_290" n="HIAT:ip">"</nts>
                  <ts e="T684" id="Seg_292" n="HIAT:w" s="T683">Spasiba</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_295" n="HIAT:w" s="T684">tănan</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_297" n="HIAT:ip">(</nts>
                  <ts e="T686" id="Seg_299" n="HIAT:w" s="T685">s-</ts>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_303" n="HIAT:w" s="T686">šo</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_306" n="HIAT:w" s="T687">šojdʼo</ts>
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_311" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_313" n="HIAT:w" s="T688">Dĭgəttə</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_316" n="HIAT:w" s="T689">ambiʔi</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_320" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_322" n="HIAT:w" s="T690">I</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_325" n="HIAT:w" s="T691">dăreʔ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_328" n="HIAT:w" s="T692">uge</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_331" n="HIAT:w" s="T693">amnobiʔi</ts>
                  <nts id="Seg_332" n="HIAT:ip">,</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_335" n="HIAT:w" s="T694">amorbiʔi</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_339" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_341" n="HIAT:w" s="T695">Onʼiʔ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_344" n="HIAT:w" s="T696">kalla</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_347" n="HIAT:w" s="T697">dʼürbi</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_351" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_353" n="HIAT:w" s="T698">A</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_356" n="HIAT:w" s="T699">nʼit:</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_358" n="HIAT:ip">"</nts>
                  <ts e="T701" id="Seg_360" n="HIAT:w" s="T700">Davaj</ts>
                  <nts id="Seg_361" n="HIAT:ip">,</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_364" n="HIAT:w" s="T701">mĭnzərəʔ</ts>
                  <nts id="Seg_365" n="HIAT:ip">,</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_368" n="HIAT:w" s="T702">măndə</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_372" n="HIAT:w" s="T703">măna</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_376" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_378" n="HIAT:w" s="T704">Dĭ</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_381" n="HIAT:w" s="T705">mĭnzərbi</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_385" n="HIAT:w" s="T706">a</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_388" n="HIAT:w" s="T707">dĭ</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_391" n="HIAT:w" s="T708">ej</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_394" n="HIAT:w" s="T709">tĭmniet</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_397" n="HIAT:w" s="T710">ĭmbi</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_400" n="HIAT:w" s="T711">nörbəsʼtə</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_404" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_406" n="HIAT:w" s="T712">Mʼaŋnaʔbə</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_410" n="HIAT:w" s="T713">vʼezʼdʼe</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_413" n="HIAT:w" s="T714">bar</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_416" n="HIAT:w" s="T715">dĭ</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_419" n="HIAT:w" s="T716">kaša</ts>
                  <nts id="Seg_420" n="HIAT:ip">.</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_423" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_425" n="HIAT:w" s="T717">Dĭgəttə</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_428" n="HIAT:w" s="T718">sʼestrat</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_431" n="HIAT:w" s="T719">šonəga</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_434" n="HIAT:w" s="T720">da</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_437" n="HIAT:w" s="T721">kirgarlaʔbə:</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_439" n="HIAT:ip">"</nts>
                  <ts e="T723" id="Seg_441" n="HIAT:w" s="T722">Spasiba</ts>
                  <nts id="Seg_442" n="HIAT:ip">,</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_445" n="HIAT:w" s="T723">goršok</ts>
                  <nts id="Seg_446" n="HIAT:ip">!</nts>
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T726" id="Seg_450" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_452" n="HIAT:w" s="T724">Dĭgəttə</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_455" n="HIAT:w" s="T725">šobi</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_459" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_461" n="HIAT:w" s="T726">I</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_463" n="HIAT:ip">(</nts>
                  <ts e="T728" id="Seg_465" n="HIAT:w" s="T727">dĭ=</ts>
                  <nts id="Seg_466" n="HIAT:ip">)</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_469" n="HIAT:w" s="T728">dĭ</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_472" n="HIAT:w" s="T729">nʼi</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_475" n="HIAT:w" s="T730">kaməndə</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_478" n="HIAT:w" s="T731">daška</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_481" n="HIAT:w" s="T732">ej</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_484" n="HIAT:w" s="T733">mĭnzərbi</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_487" n="HIAT:w" s="T734">dĭ</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_490" n="HIAT:w" s="T735">goršokgən</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_493" n="HIAT:w" s="T736">kaša</ts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_497" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_499" n="HIAT:w" s="T737">Kabarləj</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T738" id="Seg_502" n="sc" s="T606">
               <ts e="T607" id="Seg_504" n="e" s="T606">Amnobiʔi </ts>
               <ts e="T608" id="Seg_506" n="e" s="T607">onʼiʔ </ts>
               <ts e="T609" id="Seg_508" n="e" s="T608">turagən </ts>
               <ts e="T610" id="Seg_510" n="e" s="T609">koʔbdo </ts>
               <ts e="T611" id="Seg_512" n="e" s="T610">i </ts>
               <ts e="T612" id="Seg_514" n="e" s="T611">nʼi. </ts>
               <ts e="T613" id="Seg_516" n="e" s="T612">Dĭzeŋdə </ts>
               <ts e="T614" id="Seg_518" n="e" s="T613">amzittə </ts>
               <ts e="T615" id="Seg_520" n="e" s="T614">nagabi </ts>
               <ts e="T616" id="Seg_522" n="e" s="T615">ĭmbidə. </ts>
               <ts e="T617" id="Seg_524" n="e" s="T616">Amzittə </ts>
               <ts e="T618" id="Seg_526" n="e" s="T617">naga </ts>
               <ts e="T619" id="Seg_528" n="e" s="T618">dĭzeŋ. </ts>
               <ts e="T620" id="Seg_530" n="e" s="T619">Koʔbdo </ts>
               <ts e="T621" id="Seg_532" n="e" s="T620">kambi. </ts>
               <ts e="T622" id="Seg_534" n="e" s="T621">Šonəga, </ts>
               <ts e="T623" id="Seg_536" n="e" s="T622">šonəga, </ts>
               <ts e="T624" id="Seg_538" n="e" s="T623">(nuke-) </ts>
               <ts e="T625" id="Seg_540" n="e" s="T624">nüke </ts>
               <ts e="T626" id="Seg_542" n="e" s="T625">bü </ts>
               <ts e="T627" id="Seg_544" n="e" s="T626">detleʔbə. </ts>
               <ts e="T628" id="Seg_546" n="e" s="T627">Dĭ </ts>
               <ts e="T629" id="Seg_548" n="e" s="T628">măndə: </ts>
               <ts e="T630" id="Seg_550" n="e" s="T629">"Deʔ </ts>
               <ts e="T631" id="Seg_552" n="e" s="T630">măn </ts>
               <ts e="T632" id="Seg_554" n="e" s="T631">tănan </ts>
               <ts e="T633" id="Seg_556" n="e" s="T632">kabažərlam". </ts>
               <ts e="T634" id="Seg_558" n="e" s="T633">Dĭgəttə </ts>
               <ts e="T635" id="Seg_560" n="e" s="T634">šidegöʔ </ts>
               <ts e="T636" id="Seg_562" n="e" s="T635">kumbiʔi, </ts>
               <ts e="T637" id="Seg_564" n="e" s="T636">kumbiʔi. </ts>
               <ts e="T638" id="Seg_566" n="e" s="T637">Deʔpiʔi </ts>
               <ts e="T639" id="Seg_568" n="e" s="T638">maːndə. </ts>
               <ts e="T640" id="Seg_570" n="e" s="T639">Dĭgəttə </ts>
               <ts e="T641" id="Seg_572" n="e" s="T640">dĭ </ts>
               <ts e="T642" id="Seg_574" n="e" s="T641">măndə: </ts>
               <ts e="T643" id="Seg_576" n="e" s="T642">"Măn </ts>
               <ts e="T644" id="Seg_578" n="e" s="T643">tănan </ts>
               <ts e="T645" id="Seg_580" n="e" s="T644">mĭlem </ts>
               <ts e="T646" id="Seg_582" n="e" s="T645">šojdʼo, </ts>
               <ts e="T647" id="Seg_584" n="e" s="T646">kanaʔ. </ts>
               <ts e="T648" id="Seg_586" n="e" s="T647">Da </ts>
               <ts e="T649" id="Seg_588" n="e" s="T648">dĭ </ts>
               <ts e="T650" id="Seg_590" n="e" s="T649">bostə </ts>
               <ts e="T651" id="Seg_592" n="e" s="T650">mĭnzərləj </ts>
               <ts e="T652" id="Seg_594" n="e" s="T651">tănan </ts>
               <ts e="T653" id="Seg_596" n="e" s="T652">kaša, </ts>
               <ts e="T654" id="Seg_598" n="e" s="T653">(am- </ts>
               <ts e="T655" id="Seg_600" n="e" s="T654">a </ts>
               <ts e="T656" id="Seg_602" n="e" s="T655">manaʔ-) </ts>
               <ts e="T657" id="Seg_604" n="e" s="T656">a </ts>
               <ts e="T658" id="Seg_606" n="e" s="T657">dĭgəttə </ts>
               <ts e="T659" id="Seg_608" n="e" s="T658">(măn- </ts>
               <ts e="T660" id="Seg_610" n="e" s="T659">măna-) </ts>
               <ts e="T661" id="Seg_612" n="e" s="T660">măndə: </ts>
               <ts e="T662" id="Seg_614" n="e" s="T661">"Pasiba. </ts>
               <ts e="T663" id="Seg_616" n="e" s="T662">Ej </ts>
               <ts e="T664" id="Seg_618" n="e" s="T663">kereʔ </ts>
               <ts e="T665" id="Seg_620" n="e" s="T664">daška". </ts>
               <ts e="T666" id="Seg_622" n="e" s="T665">Dĭgəttə </ts>
               <ts e="T667" id="Seg_624" n="e" s="T666">dĭ </ts>
               <ts e="T668" id="Seg_626" n="e" s="T667">šobi </ts>
               <ts e="T669" id="Seg_628" n="e" s="T668">(i) </ts>
               <ts e="T670" id="Seg_630" n="e" s="T669">maːndə, </ts>
               <ts e="T672" id="Seg_632" n="e" s="T670">(davaj=) </ts>
               <ts e="T673" id="Seg_634" n="e" s="T672">šüm </ts>
               <ts e="T674" id="Seg_636" n="e" s="T673">embi. </ts>
               <ts e="T675" id="Seg_638" n="e" s="T674">Dĭ </ts>
               <ts e="T677" id="Seg_640" n="e" s="T675">šojdʼom, </ts>
               <ts e="T678" id="Seg_642" n="e" s="T677">dĭ </ts>
               <ts e="T679" id="Seg_644" n="e" s="T678">bar </ts>
               <ts e="T680" id="Seg_646" n="e" s="T679">mĭnzərleʔbə, </ts>
               <ts e="T681" id="Seg_648" n="e" s="T680">iʔgö </ts>
               <ts e="T682" id="Seg_650" n="e" s="T681">mĭnzərbi. </ts>
               <ts e="T683" id="Seg_652" n="e" s="T682">Măndə: </ts>
               <ts e="T684" id="Seg_654" n="e" s="T683">"Spasiba </ts>
               <ts e="T685" id="Seg_656" n="e" s="T684">tănan </ts>
               <ts e="T686" id="Seg_658" n="e" s="T685">(s-) </ts>
               <ts e="T687" id="Seg_660" n="e" s="T686">šo </ts>
               <ts e="T688" id="Seg_662" n="e" s="T687">šojdʼo". </ts>
               <ts e="T689" id="Seg_664" n="e" s="T688">Dĭgəttə </ts>
               <ts e="T690" id="Seg_666" n="e" s="T689">ambiʔi. </ts>
               <ts e="T691" id="Seg_668" n="e" s="T690">I </ts>
               <ts e="T692" id="Seg_670" n="e" s="T691">dăreʔ </ts>
               <ts e="T693" id="Seg_672" n="e" s="T692">uge </ts>
               <ts e="T694" id="Seg_674" n="e" s="T693">amnobiʔi, </ts>
               <ts e="T695" id="Seg_676" n="e" s="T694">amorbiʔi. </ts>
               <ts e="T696" id="Seg_678" n="e" s="T695">Onʼiʔ </ts>
               <ts e="T697" id="Seg_680" n="e" s="T696">kalla </ts>
               <ts e="T698" id="Seg_682" n="e" s="T697">dʼürbi. </ts>
               <ts e="T699" id="Seg_684" n="e" s="T698">A </ts>
               <ts e="T700" id="Seg_686" n="e" s="T699">nʼit: </ts>
               <ts e="T701" id="Seg_688" n="e" s="T700">"Davaj, </ts>
               <ts e="T702" id="Seg_690" n="e" s="T701">mĭnzərəʔ, </ts>
               <ts e="T703" id="Seg_692" n="e" s="T702">măndə, </ts>
               <ts e="T704" id="Seg_694" n="e" s="T703">măna. </ts>
               <ts e="T705" id="Seg_696" n="e" s="T704">Dĭ </ts>
               <ts e="T706" id="Seg_698" n="e" s="T705">mĭnzərbi, </ts>
               <ts e="T707" id="Seg_700" n="e" s="T706">a </ts>
               <ts e="T708" id="Seg_702" n="e" s="T707">dĭ </ts>
               <ts e="T709" id="Seg_704" n="e" s="T708">ej </ts>
               <ts e="T710" id="Seg_706" n="e" s="T709">tĭmniet </ts>
               <ts e="T711" id="Seg_708" n="e" s="T710">ĭmbi </ts>
               <ts e="T712" id="Seg_710" n="e" s="T711">nörbəsʼtə. </ts>
               <ts e="T713" id="Seg_712" n="e" s="T712">Mʼaŋnaʔbə, </ts>
               <ts e="T714" id="Seg_714" n="e" s="T713">vʼezʼdʼe </ts>
               <ts e="T715" id="Seg_716" n="e" s="T714">bar </ts>
               <ts e="T716" id="Seg_718" n="e" s="T715">dĭ </ts>
               <ts e="T717" id="Seg_720" n="e" s="T716">kaša. </ts>
               <ts e="T718" id="Seg_722" n="e" s="T717">Dĭgəttə </ts>
               <ts e="T719" id="Seg_724" n="e" s="T718">sʼestrat </ts>
               <ts e="T720" id="Seg_726" n="e" s="T719">šonəga </ts>
               <ts e="T721" id="Seg_728" n="e" s="T720">da </ts>
               <ts e="T722" id="Seg_730" n="e" s="T721">kirgarlaʔbə: </ts>
               <ts e="T723" id="Seg_732" n="e" s="T722">"Spasiba, </ts>
               <ts e="T724" id="Seg_734" n="e" s="T723">goršok!" </ts>
               <ts e="T725" id="Seg_736" n="e" s="T724">Dĭgəttə </ts>
               <ts e="T726" id="Seg_738" n="e" s="T725">šobi. </ts>
               <ts e="T727" id="Seg_740" n="e" s="T726">I </ts>
               <ts e="T728" id="Seg_742" n="e" s="T727">(dĭ=) </ts>
               <ts e="T729" id="Seg_744" n="e" s="T728">dĭ </ts>
               <ts e="T730" id="Seg_746" n="e" s="T729">nʼi </ts>
               <ts e="T731" id="Seg_748" n="e" s="T730">kaməndə </ts>
               <ts e="T732" id="Seg_750" n="e" s="T731">daška </ts>
               <ts e="T733" id="Seg_752" n="e" s="T732">ej </ts>
               <ts e="T734" id="Seg_754" n="e" s="T733">mĭnzərbi </ts>
               <ts e="T735" id="Seg_756" n="e" s="T734">dĭ </ts>
               <ts e="T736" id="Seg_758" n="e" s="T735">goršokgən </ts>
               <ts e="T737" id="Seg_760" n="e" s="T736">kaša. </ts>
               <ts e="T738" id="Seg_762" n="e" s="T737">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T612" id="Seg_763" s="T606">PKZ_196X_FairyPot_flk.001 (001)</ta>
            <ta e="T616" id="Seg_764" s="T612">PKZ_196X_FairyPot_flk.002 (002)</ta>
            <ta e="T619" id="Seg_765" s="T616">PKZ_196X_FairyPot_flk.003 (003)</ta>
            <ta e="T621" id="Seg_766" s="T619">PKZ_196X_FairyPot_flk.004 (004)</ta>
            <ta e="T627" id="Seg_767" s="T621">PKZ_196X_FairyPot_flk.005 (005)</ta>
            <ta e="T633" id="Seg_768" s="T627">PKZ_196X_FairyPot_flk.006 (006)</ta>
            <ta e="T637" id="Seg_769" s="T633">PKZ_196X_FairyPot_flk.007 (007)</ta>
            <ta e="T639" id="Seg_770" s="T637">PKZ_196X_FairyPot_flk.008 (008)</ta>
            <ta e="T647" id="Seg_771" s="T639">PKZ_196X_FairyPot_flk.009 (009) </ta>
            <ta e="T662" id="Seg_772" s="T647">PKZ_196X_FairyPot_flk.010 (011) </ta>
            <ta e="T665" id="Seg_773" s="T662">PKZ_196X_FairyPot_flk.011 (013)</ta>
            <ta e="T674" id="Seg_774" s="T665">PKZ_196X_FairyPot_flk.012 (014)</ta>
            <ta e="T682" id="Seg_775" s="T674">PKZ_196X_FairyPot_flk.013 (015)</ta>
            <ta e="T688" id="Seg_776" s="T682">PKZ_196X_FairyPot_flk.014 (016)</ta>
            <ta e="T690" id="Seg_777" s="T688">PKZ_196X_FairyPot_flk.015 (017)</ta>
            <ta e="T695" id="Seg_778" s="T690">PKZ_196X_FairyPot_flk.016 (018)</ta>
            <ta e="T698" id="Seg_779" s="T695">PKZ_196X_FairyPot_flk.017 (019)</ta>
            <ta e="T704" id="Seg_780" s="T698">PKZ_196X_FairyPot_flk.018 (020)</ta>
            <ta e="T712" id="Seg_781" s="T704">PKZ_196X_FairyPot_flk.019 (021)</ta>
            <ta e="T717" id="Seg_782" s="T712">PKZ_196X_FairyPot_flk.020 (022)</ta>
            <ta e="T724" id="Seg_783" s="T717">PKZ_196X_FairyPot_flk.021 (023)</ta>
            <ta e="T726" id="Seg_784" s="T724">PKZ_196X_FairyPot_flk.022 (024)</ta>
            <ta e="T737" id="Seg_785" s="T726">PKZ_196X_FairyPot_flk.023 (025)</ta>
            <ta e="T738" id="Seg_786" s="T737">PKZ_196X_FairyPot_flk.024 (026)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T612" id="Seg_787" s="T606">Amnobiʔi onʼiʔ turagən koʔbdo i nʼi. </ta>
            <ta e="T616" id="Seg_788" s="T612">Dĭzeŋdə amzittə nagabi ĭmbidə. </ta>
            <ta e="T619" id="Seg_789" s="T616">Amzittə naga dĭzeŋ. </ta>
            <ta e="T621" id="Seg_790" s="T619">Koʔbdo kambi. </ta>
            <ta e="T627" id="Seg_791" s="T621">Šonəga, šonəga, (nuke-) nüke bü detleʔbə. </ta>
            <ta e="T633" id="Seg_792" s="T627">Dĭ măndə: "Deʔ măn tănan kabažərlam". </ta>
            <ta e="T637" id="Seg_793" s="T633">Dĭgəttə šidegöʔ kumbiʔi, kumbiʔi. </ta>
            <ta e="T639" id="Seg_794" s="T637">Deʔpiʔi maːndə. </ta>
            <ta e="T647" id="Seg_795" s="T639">Dĭgəttə dĭ măndə: "Măn tănan mĭlem šojdʼo, kanaʔ. </ta>
            <ta e="T662" id="Seg_796" s="T647">Da dĭ bostə mĭnzərləj tănan kaša, (am- a manaʔ-) a dĭgəttə (măn- măna-) măndə: "Pasiba. </ta>
            <ta e="T665" id="Seg_797" s="T662">Ej kereʔ daška". </ta>
            <ta e="T674" id="Seg_798" s="T665">Dĭgəttə dĭ šobi (i) maːndə, (davaj=) šüm embi. </ta>
            <ta e="T682" id="Seg_799" s="T674">Dĭ šojdʼom, dĭ bar mĭnzərleʔbə, iʔgö mĭnzərbi. </ta>
            <ta e="T688" id="Seg_800" s="T682">Măndə: "Spasiba tănan (s-) šo šojdʼo". </ta>
            <ta e="T690" id="Seg_801" s="T688">Dĭgəttə ambiʔi. </ta>
            <ta e="T695" id="Seg_802" s="T690">I dăreʔ uge amnobiʔi, amorbiʔi. </ta>
            <ta e="T698" id="Seg_803" s="T695">Onʼiʔ kalla dʼürbi. </ta>
            <ta e="T704" id="Seg_804" s="T698">A nʼit: "Davaj, mĭnzərəʔ, măndə, mănaː. </ta>
            <ta e="T712" id="Seg_805" s="T704">Dĭ mĭnzərbi, a dĭ ej tĭmniet ĭmbi nörbəsʼtə. </ta>
            <ta e="T717" id="Seg_806" s="T712">Mʼaŋnaʔbə, vʼezʼdʼe bar dĭ kaša. </ta>
            <ta e="T724" id="Seg_807" s="T717">Dĭgəttə sʼestrat šonəga da kirgarlaʔbə:" Spasiba, goršok!" </ta>
            <ta e="T726" id="Seg_808" s="T724">Dĭgəttə šobi. </ta>
            <ta e="T737" id="Seg_809" s="T726">I (dĭ=) dĭ nʼi kaməndə daška ej mĭnzərbi dĭ goršokgən kaša. </ta>
            <ta e="T738" id="Seg_810" s="T737">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T607" id="Seg_811" s="T606">amno-bi-ʔi</ta>
            <ta e="T608" id="Seg_812" s="T607">onʼiʔ</ta>
            <ta e="T609" id="Seg_813" s="T608">tura-gən</ta>
            <ta e="T610" id="Seg_814" s="T609">koʔbdo</ta>
            <ta e="T611" id="Seg_815" s="T610">i</ta>
            <ta e="T612" id="Seg_816" s="T611">nʼi</ta>
            <ta e="T613" id="Seg_817" s="T612">dĭ-zeŋ-də</ta>
            <ta e="T614" id="Seg_818" s="T613">am-zittə</ta>
            <ta e="T615" id="Seg_819" s="T614">naga-bi</ta>
            <ta e="T616" id="Seg_820" s="T615">ĭmbi=də</ta>
            <ta e="T617" id="Seg_821" s="T616">am-zittə</ta>
            <ta e="T618" id="Seg_822" s="T617">naga</ta>
            <ta e="T619" id="Seg_823" s="T618">dĭ-zeŋ</ta>
            <ta e="T620" id="Seg_824" s="T619">koʔbdo</ta>
            <ta e="T621" id="Seg_825" s="T620">kam-bi</ta>
            <ta e="T622" id="Seg_826" s="T621">šonə-ga</ta>
            <ta e="T623" id="Seg_827" s="T622">šonə-ga</ta>
            <ta e="T625" id="Seg_828" s="T624">nüke</ta>
            <ta e="T626" id="Seg_829" s="T625">bü</ta>
            <ta e="T627" id="Seg_830" s="T626">det-leʔbə</ta>
            <ta e="T628" id="Seg_831" s="T627">dĭ</ta>
            <ta e="T629" id="Seg_832" s="T628">măn-də</ta>
            <ta e="T630" id="Seg_833" s="T629">de-ʔ</ta>
            <ta e="T631" id="Seg_834" s="T630">măn</ta>
            <ta e="T632" id="Seg_835" s="T631">tănan</ta>
            <ta e="T633" id="Seg_836" s="T632">kabažər-la-m</ta>
            <ta e="T634" id="Seg_837" s="T633">dĭgəttə</ta>
            <ta e="T635" id="Seg_838" s="T634">šide-göʔ</ta>
            <ta e="T636" id="Seg_839" s="T635">kum-bi-ʔi</ta>
            <ta e="T637" id="Seg_840" s="T636">kum-bi-ʔi</ta>
            <ta e="T638" id="Seg_841" s="T637">deʔ-pi-ʔi</ta>
            <ta e="T639" id="Seg_842" s="T638">ma-ndə</ta>
            <ta e="T640" id="Seg_843" s="T639">dĭgəttə</ta>
            <ta e="T641" id="Seg_844" s="T640">dĭ</ta>
            <ta e="T642" id="Seg_845" s="T641">măn-də</ta>
            <ta e="T643" id="Seg_846" s="T642">măn</ta>
            <ta e="T644" id="Seg_847" s="T643">tănan</ta>
            <ta e="T645" id="Seg_848" s="T644">mĭ-le-m</ta>
            <ta e="T646" id="Seg_849" s="T645">šojdʼo</ta>
            <ta e="T647" id="Seg_850" s="T646">kan-a-ʔ</ta>
            <ta e="T648" id="Seg_851" s="T647">da</ta>
            <ta e="T649" id="Seg_852" s="T648">dĭ</ta>
            <ta e="T650" id="Seg_853" s="T649">bos-tə</ta>
            <ta e="T651" id="Seg_854" s="T650">mĭnzər-lə-j</ta>
            <ta e="T652" id="Seg_855" s="T651">tănan</ta>
            <ta e="T653" id="Seg_856" s="T652">kaša</ta>
            <ta e="T657" id="Seg_857" s="T656">a</ta>
            <ta e="T658" id="Seg_858" s="T657">dĭgəttə</ta>
            <ta e="T661" id="Seg_859" s="T660">măn-də</ta>
            <ta e="T662" id="Seg_860" s="T661">pasiba</ta>
            <ta e="T663" id="Seg_861" s="T662">ej</ta>
            <ta e="T664" id="Seg_862" s="T663">kereʔ</ta>
            <ta e="T665" id="Seg_863" s="T664">daška</ta>
            <ta e="T666" id="Seg_864" s="T665">dĭgəttə</ta>
            <ta e="T667" id="Seg_865" s="T666">dĭ</ta>
            <ta e="T668" id="Seg_866" s="T667">šo-bi</ta>
            <ta e="T669" id="Seg_867" s="T668">i</ta>
            <ta e="T670" id="Seg_868" s="T669">ma-ndə</ta>
            <ta e="T672" id="Seg_869" s="T670">davaj</ta>
            <ta e="T673" id="Seg_870" s="T672">šü-m</ta>
            <ta e="T674" id="Seg_871" s="T673">em-bi</ta>
            <ta e="T675" id="Seg_872" s="T674">dĭ</ta>
            <ta e="T677" id="Seg_873" s="T675">šojdʼo-m</ta>
            <ta e="T678" id="Seg_874" s="T677">dĭ</ta>
            <ta e="T679" id="Seg_875" s="T678">bar</ta>
            <ta e="T680" id="Seg_876" s="T679">mĭnzər-leʔbə</ta>
            <ta e="T681" id="Seg_877" s="T680">iʔgö</ta>
            <ta e="T682" id="Seg_878" s="T681">mĭnzər-bi</ta>
            <ta e="T683" id="Seg_879" s="T682">măn-də</ta>
            <ta e="T684" id="Seg_880" s="T683">spasiba</ta>
            <ta e="T685" id="Seg_881" s="T684">tănan</ta>
            <ta e="T687" id="Seg_882" s="T686">šo</ta>
            <ta e="T688" id="Seg_883" s="T687">šojdʼo</ta>
            <ta e="T689" id="Seg_884" s="T688">dĭgəttə</ta>
            <ta e="T690" id="Seg_885" s="T689">am-bi-ʔi</ta>
            <ta e="T691" id="Seg_886" s="T690">i</ta>
            <ta e="T692" id="Seg_887" s="T691">dăreʔ</ta>
            <ta e="T693" id="Seg_888" s="T692">uge</ta>
            <ta e="T694" id="Seg_889" s="T693">amno-bi-ʔi</ta>
            <ta e="T695" id="Seg_890" s="T694">amor-bi-ʔi</ta>
            <ta e="T696" id="Seg_891" s="T695">onʼiʔ</ta>
            <ta e="T697" id="Seg_892" s="T696">kal-la</ta>
            <ta e="T698" id="Seg_893" s="T697">dʼür-bi</ta>
            <ta e="T699" id="Seg_894" s="T698">a</ta>
            <ta e="T700" id="Seg_895" s="T699">nʼi-t</ta>
            <ta e="T701" id="Seg_896" s="T700">davaj</ta>
            <ta e="T702" id="Seg_897" s="T701">mĭnzər-ə-ʔ</ta>
            <ta e="T703" id="Seg_898" s="T702">măn-də</ta>
            <ta e="T704" id="Seg_899" s="T703">măna</ta>
            <ta e="T705" id="Seg_900" s="T704">dĭ</ta>
            <ta e="T706" id="Seg_901" s="T705">mĭnzər-bi</ta>
            <ta e="T707" id="Seg_902" s="T706">a</ta>
            <ta e="T708" id="Seg_903" s="T707">dĭ</ta>
            <ta e="T709" id="Seg_904" s="T708">ej</ta>
            <ta e="T710" id="Seg_905" s="T709">tĭm-nie-t</ta>
            <ta e="T711" id="Seg_906" s="T710">ĭmbi</ta>
            <ta e="T712" id="Seg_907" s="T711">nörbə-sʼtə</ta>
            <ta e="T713" id="Seg_908" s="T712">mʼaŋ-naʔbə</ta>
            <ta e="T714" id="Seg_909" s="T713">vʼezʼdʼe</ta>
            <ta e="T715" id="Seg_910" s="T714">bar</ta>
            <ta e="T716" id="Seg_911" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_912" s="T716">kaša</ta>
            <ta e="T718" id="Seg_913" s="T717">dĭgəttə</ta>
            <ta e="T719" id="Seg_914" s="T718">sʼestra-t</ta>
            <ta e="T720" id="Seg_915" s="T719">šonə-ga</ta>
            <ta e="T721" id="Seg_916" s="T720">da</ta>
            <ta e="T722" id="Seg_917" s="T721">kirgar-laʔbə</ta>
            <ta e="T723" id="Seg_918" s="T722">spasiba</ta>
            <ta e="T724" id="Seg_919" s="T723">goršok</ta>
            <ta e="T725" id="Seg_920" s="T724">dĭgəttə</ta>
            <ta e="T726" id="Seg_921" s="T725">šo-bi</ta>
            <ta e="T727" id="Seg_922" s="T726">i</ta>
            <ta e="T728" id="Seg_923" s="T727">dĭ</ta>
            <ta e="T729" id="Seg_924" s="T728">dĭ</ta>
            <ta e="T730" id="Seg_925" s="T729">nʼi</ta>
            <ta e="T731" id="Seg_926" s="T730">kamən=də</ta>
            <ta e="T732" id="Seg_927" s="T731">daška</ta>
            <ta e="T733" id="Seg_928" s="T732">ej</ta>
            <ta e="T734" id="Seg_929" s="T733">mĭnzər-bi</ta>
            <ta e="T735" id="Seg_930" s="T734">dĭ</ta>
            <ta e="T736" id="Seg_931" s="T735">goršok-gən</ta>
            <ta e="T737" id="Seg_932" s="T736">kaša</ta>
            <ta e="T738" id="Seg_933" s="T737">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T607" id="Seg_934" s="T606">amno-bi-jəʔ</ta>
            <ta e="T608" id="Seg_935" s="T607">onʼiʔ</ta>
            <ta e="T609" id="Seg_936" s="T608">tura-Kən</ta>
            <ta e="T610" id="Seg_937" s="T609">koʔbdo</ta>
            <ta e="T611" id="Seg_938" s="T610">i</ta>
            <ta e="T612" id="Seg_939" s="T611">nʼi</ta>
            <ta e="T613" id="Seg_940" s="T612">dĭ-zAŋ-Tə</ta>
            <ta e="T614" id="Seg_941" s="T613">am-zittə</ta>
            <ta e="T615" id="Seg_942" s="T614">naga-bi</ta>
            <ta e="T616" id="Seg_943" s="T615">ĭmbi=də</ta>
            <ta e="T617" id="Seg_944" s="T616">am-zittə</ta>
            <ta e="T618" id="Seg_945" s="T617">naga</ta>
            <ta e="T619" id="Seg_946" s="T618">dĭ-zAŋ</ta>
            <ta e="T620" id="Seg_947" s="T619">koʔbdo</ta>
            <ta e="T621" id="Seg_948" s="T620">kan-bi</ta>
            <ta e="T622" id="Seg_949" s="T621">šonə-gA</ta>
            <ta e="T623" id="Seg_950" s="T622">šonə-gA</ta>
            <ta e="T625" id="Seg_951" s="T624">nüke</ta>
            <ta e="T626" id="Seg_952" s="T625">bü</ta>
            <ta e="T627" id="Seg_953" s="T626">det-laʔbə</ta>
            <ta e="T628" id="Seg_954" s="T627">dĭ</ta>
            <ta e="T629" id="Seg_955" s="T628">măn-ntə</ta>
            <ta e="T630" id="Seg_956" s="T629">det-ʔ</ta>
            <ta e="T631" id="Seg_957" s="T630">măn</ta>
            <ta e="T632" id="Seg_958" s="T631">tănan</ta>
            <ta e="T633" id="Seg_959" s="T632">kabažar-lV-m</ta>
            <ta e="T634" id="Seg_960" s="T633">dĭgəttə</ta>
            <ta e="T635" id="Seg_961" s="T634">šide-göʔ</ta>
            <ta e="T636" id="Seg_962" s="T635">kun-bi-jəʔ</ta>
            <ta e="T637" id="Seg_963" s="T636">kun-bi-jəʔ</ta>
            <ta e="T638" id="Seg_964" s="T637">det-bi-jəʔ</ta>
            <ta e="T639" id="Seg_965" s="T638">maʔ-gəndə</ta>
            <ta e="T640" id="Seg_966" s="T639">dĭgəttə</ta>
            <ta e="T641" id="Seg_967" s="T640">dĭ</ta>
            <ta e="T642" id="Seg_968" s="T641">măn-ntə</ta>
            <ta e="T643" id="Seg_969" s="T642">măn</ta>
            <ta e="T644" id="Seg_970" s="T643">tănan</ta>
            <ta e="T645" id="Seg_971" s="T644">mĭ-lV-m</ta>
            <ta e="T646" id="Seg_972" s="T645">šojdʼo</ta>
            <ta e="T647" id="Seg_973" s="T646">kan-ə-ʔ</ta>
            <ta e="T648" id="Seg_974" s="T647">da</ta>
            <ta e="T649" id="Seg_975" s="T648">dĭ</ta>
            <ta e="T650" id="Seg_976" s="T649">bos-də</ta>
            <ta e="T651" id="Seg_977" s="T650">mĭnzər-lV-j</ta>
            <ta e="T652" id="Seg_978" s="T651">tănan</ta>
            <ta e="T653" id="Seg_979" s="T652">kaša</ta>
            <ta e="T657" id="Seg_980" s="T656">a</ta>
            <ta e="T658" id="Seg_981" s="T657">dĭgəttə</ta>
            <ta e="T661" id="Seg_982" s="T660">măn-ntə</ta>
            <ta e="T662" id="Seg_983" s="T661">pasiba</ta>
            <ta e="T663" id="Seg_984" s="T662">ej</ta>
            <ta e="T664" id="Seg_985" s="T663">kereʔ</ta>
            <ta e="T665" id="Seg_986" s="T664">daška</ta>
            <ta e="T666" id="Seg_987" s="T665">dĭgəttə</ta>
            <ta e="T667" id="Seg_988" s="T666">dĭ</ta>
            <ta e="T668" id="Seg_989" s="T667">šo-bi</ta>
            <ta e="T669" id="Seg_990" s="T668">i</ta>
            <ta e="T670" id="Seg_991" s="T669">maʔ-gəndə</ta>
            <ta e="T672" id="Seg_992" s="T670">davaj</ta>
            <ta e="T673" id="Seg_993" s="T672">šü-m</ta>
            <ta e="T674" id="Seg_994" s="T673">hen-bi</ta>
            <ta e="T675" id="Seg_995" s="T674">dĭ</ta>
            <ta e="T677" id="Seg_996" s="T675">šojdʼo-m</ta>
            <ta e="T678" id="Seg_997" s="T677">dĭ</ta>
            <ta e="T679" id="Seg_998" s="T678">bar</ta>
            <ta e="T680" id="Seg_999" s="T679">mĭnzər-laʔbə</ta>
            <ta e="T681" id="Seg_1000" s="T680">iʔgö</ta>
            <ta e="T682" id="Seg_1001" s="T681">mĭnzər-bi</ta>
            <ta e="T683" id="Seg_1002" s="T682">măn-ntə</ta>
            <ta e="T684" id="Seg_1003" s="T683">pasiba</ta>
            <ta e="T685" id="Seg_1004" s="T684">tănan</ta>
            <ta e="T687" id="Seg_1005" s="T686">šo</ta>
            <ta e="T688" id="Seg_1006" s="T687">šojdʼo</ta>
            <ta e="T689" id="Seg_1007" s="T688">dĭgəttə</ta>
            <ta e="T690" id="Seg_1008" s="T689">am-bi-jəʔ</ta>
            <ta e="T691" id="Seg_1009" s="T690">i</ta>
            <ta e="T692" id="Seg_1010" s="T691">dărəʔ</ta>
            <ta e="T693" id="Seg_1011" s="T692">üge</ta>
            <ta e="T694" id="Seg_1012" s="T693">amno-bi-jəʔ</ta>
            <ta e="T695" id="Seg_1013" s="T694">amor-bi-jəʔ</ta>
            <ta e="T696" id="Seg_1014" s="T695">onʼiʔ</ta>
            <ta e="T697" id="Seg_1015" s="T696">kan-lAʔ</ta>
            <ta e="T698" id="Seg_1016" s="T697">tʼür-bi</ta>
            <ta e="T699" id="Seg_1017" s="T698">a</ta>
            <ta e="T700" id="Seg_1018" s="T699">nʼi-t</ta>
            <ta e="T701" id="Seg_1019" s="T700">davaj</ta>
            <ta e="T702" id="Seg_1020" s="T701">mĭnzər-ə-ʔ</ta>
            <ta e="T703" id="Seg_1021" s="T702">măn-ntə</ta>
            <ta e="T704" id="Seg_1022" s="T703">măna</ta>
            <ta e="T705" id="Seg_1023" s="T704">dĭ</ta>
            <ta e="T706" id="Seg_1024" s="T705">mĭnzər-bi</ta>
            <ta e="T707" id="Seg_1025" s="T706">a</ta>
            <ta e="T708" id="Seg_1026" s="T707">dĭ</ta>
            <ta e="T709" id="Seg_1027" s="T708">ej</ta>
            <ta e="T710" id="Seg_1028" s="T709">tĭm-liA-t</ta>
            <ta e="T711" id="Seg_1029" s="T710">ĭmbi</ta>
            <ta e="T712" id="Seg_1030" s="T711">nörbə-zittə</ta>
            <ta e="T713" id="Seg_1031" s="T712">mʼaŋ-laʔbə</ta>
            <ta e="T714" id="Seg_1032" s="T713">vʼezʼdʼe</ta>
            <ta e="T715" id="Seg_1033" s="T714">bar</ta>
            <ta e="T716" id="Seg_1034" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_1035" s="T716">kaša</ta>
            <ta e="T718" id="Seg_1036" s="T717">dĭgəttə</ta>
            <ta e="T719" id="Seg_1037" s="T718">sʼestra-t</ta>
            <ta e="T720" id="Seg_1038" s="T719">šonə-gA</ta>
            <ta e="T721" id="Seg_1039" s="T720">da</ta>
            <ta e="T722" id="Seg_1040" s="T721">kirgaːr-laʔbə</ta>
            <ta e="T723" id="Seg_1041" s="T722">pasiba</ta>
            <ta e="T724" id="Seg_1042" s="T723">goršok</ta>
            <ta e="T725" id="Seg_1043" s="T724">dĭgəttə</ta>
            <ta e="T726" id="Seg_1044" s="T725">šo-bi</ta>
            <ta e="T727" id="Seg_1045" s="T726">i</ta>
            <ta e="T728" id="Seg_1046" s="T727">dĭ</ta>
            <ta e="T729" id="Seg_1047" s="T728">dĭ</ta>
            <ta e="T730" id="Seg_1048" s="T729">nʼi</ta>
            <ta e="T731" id="Seg_1049" s="T730">kamən=də</ta>
            <ta e="T732" id="Seg_1050" s="T731">daška</ta>
            <ta e="T733" id="Seg_1051" s="T732">ej</ta>
            <ta e="T734" id="Seg_1052" s="T733">mĭnzər-bi</ta>
            <ta e="T735" id="Seg_1053" s="T734">dĭ</ta>
            <ta e="T736" id="Seg_1054" s="T735">goršok-Kən</ta>
            <ta e="T737" id="Seg_1055" s="T736">kaša</ta>
            <ta e="T738" id="Seg_1056" s="T737">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T607" id="Seg_1057" s="T606">live-PST-3PL</ta>
            <ta e="T608" id="Seg_1058" s="T607">one.[NOM.SG]</ta>
            <ta e="T609" id="Seg_1059" s="T608">settlement-LOC</ta>
            <ta e="T610" id="Seg_1060" s="T609">girl.[NOM.SG]</ta>
            <ta e="T611" id="Seg_1061" s="T610">and</ta>
            <ta e="T612" id="Seg_1062" s="T611">boy.[NOM.SG]</ta>
            <ta e="T613" id="Seg_1063" s="T612">this-PL-LAT</ta>
            <ta e="T614" id="Seg_1064" s="T613">eat-INF.LAT</ta>
            <ta e="T615" id="Seg_1065" s="T614">NEG.EX-PST.[3SG]</ta>
            <ta e="T616" id="Seg_1066" s="T615">what.[NOM.SG]=INDEF</ta>
            <ta e="T617" id="Seg_1067" s="T616">eat-INF.LAT</ta>
            <ta e="T618" id="Seg_1068" s="T617">NEG.EX</ta>
            <ta e="T619" id="Seg_1069" s="T618">this-PL</ta>
            <ta e="T620" id="Seg_1070" s="T619">girl.[NOM.SG]</ta>
            <ta e="T621" id="Seg_1071" s="T620">go-PST.[3SG]</ta>
            <ta e="T622" id="Seg_1072" s="T621">come-PRS.[3SG]</ta>
            <ta e="T623" id="Seg_1073" s="T622">come-PRS.[3SG]</ta>
            <ta e="T625" id="Seg_1074" s="T624">woman.[NOM.SG]</ta>
            <ta e="T626" id="Seg_1075" s="T625">water.[NOM.SG]</ta>
            <ta e="T627" id="Seg_1076" s="T626">bring-DUR.[3SG]</ta>
            <ta e="T628" id="Seg_1077" s="T627">this.[NOM.SG]</ta>
            <ta e="T629" id="Seg_1078" s="T628">say-IPFVZ.[3SG]</ta>
            <ta e="T630" id="Seg_1079" s="T629">bring-IMP.2SG</ta>
            <ta e="T631" id="Seg_1080" s="T630">I.NOM</ta>
            <ta e="T632" id="Seg_1081" s="T631">you.DAT</ta>
            <ta e="T633" id="Seg_1082" s="T632">help-FUT-1SG</ta>
            <ta e="T634" id="Seg_1083" s="T633">then</ta>
            <ta e="T635" id="Seg_1084" s="T634">two-COLL</ta>
            <ta e="T636" id="Seg_1085" s="T635">bring-PST-3PL</ta>
            <ta e="T637" id="Seg_1086" s="T636">bring-PST-3PL</ta>
            <ta e="T638" id="Seg_1087" s="T637">bring-PST-3PL</ta>
            <ta e="T639" id="Seg_1088" s="T638">tent-LAT/LOC.3SG</ta>
            <ta e="T640" id="Seg_1089" s="T639">then</ta>
            <ta e="T641" id="Seg_1090" s="T640">this.[NOM.SG]</ta>
            <ta e="T642" id="Seg_1091" s="T641">say-IPFVZ.[3SG]</ta>
            <ta e="T643" id="Seg_1092" s="T642">I.NOM</ta>
            <ta e="T644" id="Seg_1093" s="T643">you.DAT</ta>
            <ta e="T645" id="Seg_1094" s="T644">give-FUT-1SG</ta>
            <ta e="T646" id="Seg_1095" s="T645">birchbark.vessel.[NOM.SG]</ta>
            <ta e="T647" id="Seg_1096" s="T646">go-EP-IMP.2SG</ta>
            <ta e="T648" id="Seg_1097" s="T647">and</ta>
            <ta e="T649" id="Seg_1098" s="T648">this.[NOM.SG]</ta>
            <ta e="T650" id="Seg_1099" s="T649">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T651" id="Seg_1100" s="T650">boil-FUT-3SG</ta>
            <ta e="T652" id="Seg_1101" s="T651">you.DAT</ta>
            <ta e="T653" id="Seg_1102" s="T652">porridge.[NOM.SG]</ta>
            <ta e="T657" id="Seg_1103" s="T656">and</ta>
            <ta e="T658" id="Seg_1104" s="T657">then</ta>
            <ta e="T661" id="Seg_1105" s="T660">say-IPFVZ.[3SG]</ta>
            <ta e="T662" id="Seg_1106" s="T661">thank.you</ta>
            <ta e="T663" id="Seg_1107" s="T662">NEG</ta>
            <ta e="T664" id="Seg_1108" s="T663">one.needs</ta>
            <ta e="T665" id="Seg_1109" s="T664">else</ta>
            <ta e="T666" id="Seg_1110" s="T665">then</ta>
            <ta e="T667" id="Seg_1111" s="T666">this.[NOM.SG]</ta>
            <ta e="T668" id="Seg_1112" s="T667">come-PST.[3SG]</ta>
            <ta e="T669" id="Seg_1113" s="T668">and</ta>
            <ta e="T670" id="Seg_1114" s="T669">tent-LAT/LOC.3SG</ta>
            <ta e="T672" id="Seg_1115" s="T670">INCH</ta>
            <ta e="T673" id="Seg_1116" s="T672">fire-ACC</ta>
            <ta e="T674" id="Seg_1117" s="T673">put-PST.[3SG]</ta>
            <ta e="T675" id="Seg_1118" s="T674">this.[NOM.SG]</ta>
            <ta e="T677" id="Seg_1119" s="T675">birchbark.vessel-ACC</ta>
            <ta e="T678" id="Seg_1120" s="T677">this.[NOM.SG]</ta>
            <ta e="T679" id="Seg_1121" s="T678">PTCL</ta>
            <ta e="T680" id="Seg_1122" s="T679">boil-DUR.[3SG]</ta>
            <ta e="T681" id="Seg_1123" s="T680">many</ta>
            <ta e="T682" id="Seg_1124" s="T681">boil-PST.[3SG]</ta>
            <ta e="T683" id="Seg_1125" s="T682">say-IPFVZ.[3SG]</ta>
            <ta e="T684" id="Seg_1126" s="T683">thank.you</ta>
            <ta e="T685" id="Seg_1127" s="T684">you.DAT</ta>
            <ta e="T687" id="Seg_1128" s="T686">birchbark.[NOM.SG]</ta>
            <ta e="T688" id="Seg_1129" s="T687">birchbark.vessel.[NOM.SG]</ta>
            <ta e="T689" id="Seg_1130" s="T688">then</ta>
            <ta e="T690" id="Seg_1131" s="T689">eat-PST-3PL</ta>
            <ta e="T691" id="Seg_1132" s="T690">and</ta>
            <ta e="T692" id="Seg_1133" s="T691">so</ta>
            <ta e="T693" id="Seg_1134" s="T692">always</ta>
            <ta e="T694" id="Seg_1135" s="T693">live-PST-3PL</ta>
            <ta e="T695" id="Seg_1136" s="T694">eat-PST-3PL</ta>
            <ta e="T696" id="Seg_1137" s="T695">one.[NOM.SG]</ta>
            <ta e="T697" id="Seg_1138" s="T696">go-CVB</ta>
            <ta e="T698" id="Seg_1139" s="T697">disappear-PST.[3SG]</ta>
            <ta e="T699" id="Seg_1140" s="T698">and</ta>
            <ta e="T700" id="Seg_1141" s="T699">son-NOM/GEN.3SG</ta>
            <ta e="T701" id="Seg_1142" s="T700">INCH</ta>
            <ta e="T702" id="Seg_1143" s="T701">boil-EP-IMP.2SG</ta>
            <ta e="T703" id="Seg_1144" s="T702">say-IPFVZ.[3SG]</ta>
            <ta e="T704" id="Seg_1145" s="T703">I.LAT</ta>
            <ta e="T705" id="Seg_1146" s="T704">this.[NOM.SG]</ta>
            <ta e="T706" id="Seg_1147" s="T705">boil-PST.[3SG]</ta>
            <ta e="T707" id="Seg_1148" s="T706">and</ta>
            <ta e="T708" id="Seg_1149" s="T707">this.[NOM.SG]</ta>
            <ta e="T709" id="Seg_1150" s="T708">NEG</ta>
            <ta e="T710" id="Seg_1151" s="T709">know-PRS-3SG.O</ta>
            <ta e="T711" id="Seg_1152" s="T710">what.[NOM.SG]</ta>
            <ta e="T712" id="Seg_1153" s="T711">tell-INF.LAT</ta>
            <ta e="T713" id="Seg_1154" s="T712">flow-DUR.[3SG]</ta>
            <ta e="T714" id="Seg_1155" s="T713">everywhere</ta>
            <ta e="T715" id="Seg_1156" s="T714">all</ta>
            <ta e="T716" id="Seg_1157" s="T715">this.[NOM.SG]</ta>
            <ta e="T717" id="Seg_1158" s="T716">porridge.[NOM.SG]</ta>
            <ta e="T718" id="Seg_1159" s="T717">then</ta>
            <ta e="T719" id="Seg_1160" s="T718">sister-NOM/GEN.3SG</ta>
            <ta e="T720" id="Seg_1161" s="T719">come-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_1162" s="T720">and</ta>
            <ta e="T722" id="Seg_1163" s="T721">shout-DUR.[3SG]</ta>
            <ta e="T723" id="Seg_1164" s="T722">thank.you</ta>
            <ta e="T724" id="Seg_1165" s="T723">pot.[NOM.SG]</ta>
            <ta e="T725" id="Seg_1166" s="T724">then</ta>
            <ta e="T726" id="Seg_1167" s="T725">come-PST.[3SG]</ta>
            <ta e="T727" id="Seg_1168" s="T726">and</ta>
            <ta e="T728" id="Seg_1169" s="T727">this.[NOM.SG]</ta>
            <ta e="T729" id="Seg_1170" s="T728">this.[NOM.SG]</ta>
            <ta e="T730" id="Seg_1171" s="T729">boy.[NOM.SG]</ta>
            <ta e="T731" id="Seg_1172" s="T730">when=INDEF</ta>
            <ta e="T732" id="Seg_1173" s="T731">else</ta>
            <ta e="T733" id="Seg_1174" s="T732">NEG</ta>
            <ta e="T734" id="Seg_1175" s="T733">boil-PST.[3SG]</ta>
            <ta e="T735" id="Seg_1176" s="T734">this.[NOM.SG]</ta>
            <ta e="T736" id="Seg_1177" s="T735">pot-LOC</ta>
            <ta e="T737" id="Seg_1178" s="T736">porridge.[NOM.SG]</ta>
            <ta e="T738" id="Seg_1179" s="T737">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T607" id="Seg_1180" s="T606">жить-PST-3PL</ta>
            <ta e="T608" id="Seg_1181" s="T607">один.[NOM.SG]</ta>
            <ta e="T609" id="Seg_1182" s="T608">поселение-LOC</ta>
            <ta e="T610" id="Seg_1183" s="T609">девушка.[NOM.SG]</ta>
            <ta e="T611" id="Seg_1184" s="T610">и</ta>
            <ta e="T612" id="Seg_1185" s="T611">мальчик.[NOM.SG]</ta>
            <ta e="T613" id="Seg_1186" s="T612">этот-PL-LAT</ta>
            <ta e="T614" id="Seg_1187" s="T613">съесть-INF.LAT</ta>
            <ta e="T615" id="Seg_1188" s="T614">NEG.EX-PST.[3SG]</ta>
            <ta e="T616" id="Seg_1189" s="T615">что.[NOM.SG]=INDEF</ta>
            <ta e="T617" id="Seg_1190" s="T616">съесть-INF.LAT</ta>
            <ta e="T618" id="Seg_1191" s="T617">NEG.EX</ta>
            <ta e="T619" id="Seg_1192" s="T618">этот-PL</ta>
            <ta e="T620" id="Seg_1193" s="T619">девушка.[NOM.SG]</ta>
            <ta e="T621" id="Seg_1194" s="T620">пойти-PST.[3SG]</ta>
            <ta e="T622" id="Seg_1195" s="T621">прийти-PRS.[3SG]</ta>
            <ta e="T623" id="Seg_1196" s="T622">прийти-PRS.[3SG]</ta>
            <ta e="T625" id="Seg_1197" s="T624">женщина.[NOM.SG]</ta>
            <ta e="T626" id="Seg_1198" s="T625">вода.[NOM.SG]</ta>
            <ta e="T627" id="Seg_1199" s="T626">принести-DUR.[3SG]</ta>
            <ta e="T628" id="Seg_1200" s="T627">этот.[NOM.SG]</ta>
            <ta e="T629" id="Seg_1201" s="T628">сказать-IPFVZ.[3SG]</ta>
            <ta e="T630" id="Seg_1202" s="T629">принести-IMP.2SG</ta>
            <ta e="T631" id="Seg_1203" s="T630">я.NOM</ta>
            <ta e="T632" id="Seg_1204" s="T631">ты.DAT</ta>
            <ta e="T633" id="Seg_1205" s="T632">помогать-FUT-1SG</ta>
            <ta e="T634" id="Seg_1206" s="T633">тогда</ta>
            <ta e="T635" id="Seg_1207" s="T634">два-COLL</ta>
            <ta e="T636" id="Seg_1208" s="T635">нести-PST-3PL</ta>
            <ta e="T637" id="Seg_1209" s="T636">нести-PST-3PL</ta>
            <ta e="T638" id="Seg_1210" s="T637">принести-PST-3PL</ta>
            <ta e="T639" id="Seg_1211" s="T638">чум-LAT/LOC.3SG</ta>
            <ta e="T640" id="Seg_1212" s="T639">тогда</ta>
            <ta e="T641" id="Seg_1213" s="T640">этот.[NOM.SG]</ta>
            <ta e="T642" id="Seg_1214" s="T641">сказать-IPFVZ.[3SG]</ta>
            <ta e="T643" id="Seg_1215" s="T642">я.NOM</ta>
            <ta e="T644" id="Seg_1216" s="T643">ты.DAT</ta>
            <ta e="T645" id="Seg_1217" s="T644">дать-FUT-1SG</ta>
            <ta e="T646" id="Seg_1218" s="T645">туес.[NOM.SG]</ta>
            <ta e="T647" id="Seg_1219" s="T646">пойти-EP-IMP.2SG</ta>
            <ta e="T648" id="Seg_1220" s="T647">и</ta>
            <ta e="T649" id="Seg_1221" s="T648">этот.[NOM.SG]</ta>
            <ta e="T650" id="Seg_1222" s="T649">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T651" id="Seg_1223" s="T650">кипятить-FUT-3SG</ta>
            <ta e="T652" id="Seg_1224" s="T651">ты.DAT</ta>
            <ta e="T653" id="Seg_1225" s="T652">каша.[NOM.SG]</ta>
            <ta e="T657" id="Seg_1226" s="T656">а</ta>
            <ta e="T658" id="Seg_1227" s="T657">тогда</ta>
            <ta e="T661" id="Seg_1228" s="T660">сказать-IPFVZ.[3SG]</ta>
            <ta e="T662" id="Seg_1229" s="T661">спасибо</ta>
            <ta e="T663" id="Seg_1230" s="T662">NEG</ta>
            <ta e="T664" id="Seg_1231" s="T663">нужно</ta>
            <ta e="T665" id="Seg_1232" s="T664">еще</ta>
            <ta e="T666" id="Seg_1233" s="T665">тогда</ta>
            <ta e="T667" id="Seg_1234" s="T666">этот.[NOM.SG]</ta>
            <ta e="T668" id="Seg_1235" s="T667">прийти-PST.[3SG]</ta>
            <ta e="T669" id="Seg_1236" s="T668">и</ta>
            <ta e="T670" id="Seg_1237" s="T669">чум-LAT/LOC.3SG</ta>
            <ta e="T672" id="Seg_1238" s="T670">INCH</ta>
            <ta e="T673" id="Seg_1239" s="T672">огонь-ACC</ta>
            <ta e="T674" id="Seg_1240" s="T673">класть-PST.[3SG]</ta>
            <ta e="T675" id="Seg_1241" s="T674">этот.[NOM.SG]</ta>
            <ta e="T677" id="Seg_1242" s="T675">туес-ACC</ta>
            <ta e="T678" id="Seg_1243" s="T677">этот.[NOM.SG]</ta>
            <ta e="T679" id="Seg_1244" s="T678">PTCL</ta>
            <ta e="T680" id="Seg_1245" s="T679">кипятить-DUR.[3SG]</ta>
            <ta e="T681" id="Seg_1246" s="T680">много</ta>
            <ta e="T682" id="Seg_1247" s="T681">кипятить-PST.[3SG]</ta>
            <ta e="T683" id="Seg_1248" s="T682">сказать-IPFVZ.[3SG]</ta>
            <ta e="T684" id="Seg_1249" s="T683">спасибо</ta>
            <ta e="T685" id="Seg_1250" s="T684">ты.DAT</ta>
            <ta e="T687" id="Seg_1251" s="T686">береста.[NOM.SG]</ta>
            <ta e="T688" id="Seg_1252" s="T687">туес.[NOM.SG]</ta>
            <ta e="T689" id="Seg_1253" s="T688">тогда</ta>
            <ta e="T690" id="Seg_1254" s="T689">съесть-PST-3PL</ta>
            <ta e="T691" id="Seg_1255" s="T690">и</ta>
            <ta e="T692" id="Seg_1256" s="T691">так</ta>
            <ta e="T693" id="Seg_1257" s="T692">всегда</ta>
            <ta e="T694" id="Seg_1258" s="T693">жить-PST-3PL</ta>
            <ta e="T695" id="Seg_1259" s="T694">есть-PST-3PL</ta>
            <ta e="T696" id="Seg_1260" s="T695">один.[NOM.SG]</ta>
            <ta e="T697" id="Seg_1261" s="T696">пойти-CVB</ta>
            <ta e="T698" id="Seg_1262" s="T697">исчезнуть-PST.[3SG]</ta>
            <ta e="T699" id="Seg_1263" s="T698">а</ta>
            <ta e="T700" id="Seg_1264" s="T699">сын-NOM/GEN.3SG</ta>
            <ta e="T701" id="Seg_1265" s="T700">INCH</ta>
            <ta e="T702" id="Seg_1266" s="T701">кипятить-EP-IMP.2SG</ta>
            <ta e="T703" id="Seg_1267" s="T702">сказать-IPFVZ.[3SG]</ta>
            <ta e="T704" id="Seg_1268" s="T703">я.LAT</ta>
            <ta e="T705" id="Seg_1269" s="T704">этот.[NOM.SG]</ta>
            <ta e="T706" id="Seg_1270" s="T705">кипятить-PST.[3SG]</ta>
            <ta e="T707" id="Seg_1271" s="T706">а</ta>
            <ta e="T708" id="Seg_1272" s="T707">этот.[NOM.SG]</ta>
            <ta e="T709" id="Seg_1273" s="T708">NEG</ta>
            <ta e="T710" id="Seg_1274" s="T709">знать-PRS-3SG.O</ta>
            <ta e="T711" id="Seg_1275" s="T710">что.[NOM.SG]</ta>
            <ta e="T712" id="Seg_1276" s="T711">сказать-INF.LAT</ta>
            <ta e="T713" id="Seg_1277" s="T712">течь-DUR.[3SG]</ta>
            <ta e="T714" id="Seg_1278" s="T713">везде</ta>
            <ta e="T715" id="Seg_1279" s="T714">весь</ta>
            <ta e="T716" id="Seg_1280" s="T715">этот.[NOM.SG]</ta>
            <ta e="T717" id="Seg_1281" s="T716">каша.[NOM.SG]</ta>
            <ta e="T718" id="Seg_1282" s="T717">тогда</ta>
            <ta e="T719" id="Seg_1283" s="T718">сестра-NOM/GEN.3SG</ta>
            <ta e="T720" id="Seg_1284" s="T719">прийти-PRS.[3SG]</ta>
            <ta e="T721" id="Seg_1285" s="T720">и</ta>
            <ta e="T722" id="Seg_1286" s="T721">кричать-DUR.[3SG]</ta>
            <ta e="T723" id="Seg_1287" s="T722">спасибо</ta>
            <ta e="T724" id="Seg_1288" s="T723">горшок.[NOM.SG]</ta>
            <ta e="T725" id="Seg_1289" s="T724">тогда</ta>
            <ta e="T726" id="Seg_1290" s="T725">прийти-PST.[3SG]</ta>
            <ta e="T727" id="Seg_1291" s="T726">и</ta>
            <ta e="T728" id="Seg_1292" s="T727">этот.[NOM.SG]</ta>
            <ta e="T729" id="Seg_1293" s="T728">этот.[NOM.SG]</ta>
            <ta e="T730" id="Seg_1294" s="T729">мальчик.[NOM.SG]</ta>
            <ta e="T731" id="Seg_1295" s="T730">когда=INDEF</ta>
            <ta e="T732" id="Seg_1296" s="T731">еще</ta>
            <ta e="T733" id="Seg_1297" s="T732">NEG</ta>
            <ta e="T734" id="Seg_1298" s="T733">кипятить-PST.[3SG]</ta>
            <ta e="T735" id="Seg_1299" s="T734">этот.[NOM.SG]</ta>
            <ta e="T736" id="Seg_1300" s="T735">горшок-LOC</ta>
            <ta e="T737" id="Seg_1301" s="T736">каша.[NOM.SG]</ta>
            <ta e="T738" id="Seg_1302" s="T737">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T607" id="Seg_1303" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_1304" s="T607">num-n:case</ta>
            <ta e="T609" id="Seg_1305" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_1306" s="T609">n-n:case</ta>
            <ta e="T611" id="Seg_1307" s="T610">conj</ta>
            <ta e="T612" id="Seg_1308" s="T611">n-n:case</ta>
            <ta e="T613" id="Seg_1309" s="T612">dempro-n:num-n:case</ta>
            <ta e="T614" id="Seg_1310" s="T613">v-v:n.fin</ta>
            <ta e="T615" id="Seg_1311" s="T614">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_1312" s="T615">que-n:case=ptcl</ta>
            <ta e="T617" id="Seg_1313" s="T616">v-v:n.fin</ta>
            <ta e="T618" id="Seg_1314" s="T617">v</ta>
            <ta e="T619" id="Seg_1315" s="T618">dempro-n:num</ta>
            <ta e="T620" id="Seg_1316" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_1317" s="T620">v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_1318" s="T621">v-v:tense-v:pn</ta>
            <ta e="T623" id="Seg_1319" s="T622">v-v:tense-v:pn</ta>
            <ta e="T625" id="Seg_1320" s="T624">n-n:case</ta>
            <ta e="T626" id="Seg_1321" s="T625">n-n:case</ta>
            <ta e="T627" id="Seg_1322" s="T626">v-v&gt;v-v:pn</ta>
            <ta e="T628" id="Seg_1323" s="T627">dempro-n:case</ta>
            <ta e="T629" id="Seg_1324" s="T628">v-v&gt;v-v:pn</ta>
            <ta e="T630" id="Seg_1325" s="T629">v-v:mood.pn</ta>
            <ta e="T631" id="Seg_1326" s="T630">pers</ta>
            <ta e="T632" id="Seg_1327" s="T631">pers</ta>
            <ta e="T633" id="Seg_1328" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_1329" s="T633">adv</ta>
            <ta e="T635" id="Seg_1330" s="T634">num-num&gt;num</ta>
            <ta e="T636" id="Seg_1331" s="T635">v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_1332" s="T636">v-v:tense-v:pn</ta>
            <ta e="T638" id="Seg_1333" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_1334" s="T638">n-n:case.poss</ta>
            <ta e="T640" id="Seg_1335" s="T639">adv</ta>
            <ta e="T641" id="Seg_1336" s="T640">dempro-n:case</ta>
            <ta e="T642" id="Seg_1337" s="T641">v-v&gt;v-v:pn</ta>
            <ta e="T643" id="Seg_1338" s="T642">pers</ta>
            <ta e="T644" id="Seg_1339" s="T643">pers</ta>
            <ta e="T645" id="Seg_1340" s="T644">v-v:tense-v:pn</ta>
            <ta e="T646" id="Seg_1341" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_1342" s="T646">v-v:ins-v:mood.pn</ta>
            <ta e="T648" id="Seg_1343" s="T647">conj</ta>
            <ta e="T649" id="Seg_1344" s="T648">dempro-n:case</ta>
            <ta e="T650" id="Seg_1345" s="T649">refl-n:case.poss</ta>
            <ta e="T651" id="Seg_1346" s="T650">v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_1347" s="T651">pers</ta>
            <ta e="T653" id="Seg_1348" s="T652">n-n:case</ta>
            <ta e="T657" id="Seg_1349" s="T656">conj</ta>
            <ta e="T658" id="Seg_1350" s="T657">adv</ta>
            <ta e="T661" id="Seg_1351" s="T660">v-v&gt;v-v:pn</ta>
            <ta e="T662" id="Seg_1352" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_1353" s="T662">ptcl</ta>
            <ta e="T664" id="Seg_1354" s="T663">adv</ta>
            <ta e="T665" id="Seg_1355" s="T664">adv</ta>
            <ta e="T666" id="Seg_1356" s="T665">adv</ta>
            <ta e="T667" id="Seg_1357" s="T666">dempro-n:case</ta>
            <ta e="T668" id="Seg_1358" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_1359" s="T668">conj</ta>
            <ta e="T670" id="Seg_1360" s="T669">n-n:case.poss</ta>
            <ta e="T672" id="Seg_1361" s="T670">ptcl</ta>
            <ta e="T673" id="Seg_1362" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_1363" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_1364" s="T674">dempro-n:case</ta>
            <ta e="T677" id="Seg_1365" s="T675">n-n:case</ta>
            <ta e="T678" id="Seg_1366" s="T677">dempro-n:case</ta>
            <ta e="T679" id="Seg_1367" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_1368" s="T679">v-v&gt;v-v:pn</ta>
            <ta e="T681" id="Seg_1369" s="T680">quant</ta>
            <ta e="T682" id="Seg_1370" s="T681">v-v:tense-v:pn</ta>
            <ta e="T683" id="Seg_1371" s="T682">v-v&gt;v-v:pn</ta>
            <ta e="T684" id="Seg_1372" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_1373" s="T684">pers</ta>
            <ta e="T687" id="Seg_1374" s="T686">n-n:case</ta>
            <ta e="T688" id="Seg_1375" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_1376" s="T688">adv</ta>
            <ta e="T690" id="Seg_1377" s="T689">v-v:tense-v:pn</ta>
            <ta e="T691" id="Seg_1378" s="T690">conj</ta>
            <ta e="T692" id="Seg_1379" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_1380" s="T692">adv</ta>
            <ta e="T694" id="Seg_1381" s="T693">v-v:tense-v:pn</ta>
            <ta e="T695" id="Seg_1382" s="T694">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_1383" s="T695">num-n:case</ta>
            <ta e="T697" id="Seg_1384" s="T696">v-v:n.fin</ta>
            <ta e="T698" id="Seg_1385" s="T697">v-v:tense-v:pn</ta>
            <ta e="T699" id="Seg_1386" s="T698">conj</ta>
            <ta e="T700" id="Seg_1387" s="T699">n-n:case.poss</ta>
            <ta e="T701" id="Seg_1388" s="T700">ptcl</ta>
            <ta e="T702" id="Seg_1389" s="T701">v-v:ins-v:mood.pn</ta>
            <ta e="T703" id="Seg_1390" s="T702">v-v&gt;v-v:pn</ta>
            <ta e="T704" id="Seg_1391" s="T703">pers</ta>
            <ta e="T705" id="Seg_1392" s="T704">dempro-n:case</ta>
            <ta e="T706" id="Seg_1393" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_1394" s="T706">conj</ta>
            <ta e="T708" id="Seg_1395" s="T707">dempro-n:case</ta>
            <ta e="T709" id="Seg_1396" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_1397" s="T709">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_1398" s="T710">que-n:case</ta>
            <ta e="T712" id="Seg_1399" s="T711">v-v:n.fin</ta>
            <ta e="T713" id="Seg_1400" s="T712">v-v&gt;v-v:pn</ta>
            <ta e="T714" id="Seg_1401" s="T713">adv</ta>
            <ta e="T715" id="Seg_1402" s="T714">quant</ta>
            <ta e="T716" id="Seg_1403" s="T715">dempro-n:case</ta>
            <ta e="T717" id="Seg_1404" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_1405" s="T717">adv</ta>
            <ta e="T719" id="Seg_1406" s="T718">n-n:case.poss</ta>
            <ta e="T720" id="Seg_1407" s="T719">v-v:tense-v:pn</ta>
            <ta e="T721" id="Seg_1408" s="T720">conj</ta>
            <ta e="T722" id="Seg_1409" s="T721">v-v&gt;v-v:pn</ta>
            <ta e="T723" id="Seg_1410" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_1411" s="T723">n-n:case</ta>
            <ta e="T725" id="Seg_1412" s="T724">adv</ta>
            <ta e="T726" id="Seg_1413" s="T725">v-v:tense-v:pn</ta>
            <ta e="T727" id="Seg_1414" s="T726">conj</ta>
            <ta e="T728" id="Seg_1415" s="T727">dempro-n:case</ta>
            <ta e="T729" id="Seg_1416" s="T728">dempro-n:case</ta>
            <ta e="T730" id="Seg_1417" s="T729">n-n:case</ta>
            <ta e="T731" id="Seg_1418" s="T730">que=ptcl</ta>
            <ta e="T732" id="Seg_1419" s="T731">adv</ta>
            <ta e="T733" id="Seg_1420" s="T732">ptcl</ta>
            <ta e="T734" id="Seg_1421" s="T733">v-v:tense-v:pn</ta>
            <ta e="T735" id="Seg_1422" s="T734">dempro-n:case</ta>
            <ta e="T736" id="Seg_1423" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_1424" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_1425" s="T737">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T607" id="Seg_1426" s="T606">v</ta>
            <ta e="T608" id="Seg_1427" s="T607">num</ta>
            <ta e="T609" id="Seg_1428" s="T608">n</ta>
            <ta e="T610" id="Seg_1429" s="T609">n</ta>
            <ta e="T611" id="Seg_1430" s="T610">conj</ta>
            <ta e="T612" id="Seg_1431" s="T611">n</ta>
            <ta e="T613" id="Seg_1432" s="T612">dempro</ta>
            <ta e="T614" id="Seg_1433" s="T613">v</ta>
            <ta e="T615" id="Seg_1434" s="T614">v</ta>
            <ta e="T616" id="Seg_1435" s="T615">que</ta>
            <ta e="T617" id="Seg_1436" s="T616">v</ta>
            <ta e="T618" id="Seg_1437" s="T617">v</ta>
            <ta e="T619" id="Seg_1438" s="T618">dempro</ta>
            <ta e="T620" id="Seg_1439" s="T619">n</ta>
            <ta e="T621" id="Seg_1440" s="T620">v</ta>
            <ta e="T622" id="Seg_1441" s="T621">v</ta>
            <ta e="T623" id="Seg_1442" s="T622">v</ta>
            <ta e="T625" id="Seg_1443" s="T624">n</ta>
            <ta e="T626" id="Seg_1444" s="T625">n</ta>
            <ta e="T627" id="Seg_1445" s="T626">v</ta>
            <ta e="T628" id="Seg_1446" s="T627">dempro</ta>
            <ta e="T629" id="Seg_1447" s="T628">v</ta>
            <ta e="T630" id="Seg_1448" s="T629">v</ta>
            <ta e="T631" id="Seg_1449" s="T630">pers</ta>
            <ta e="T632" id="Seg_1450" s="T631">pers</ta>
            <ta e="T633" id="Seg_1451" s="T632">v</ta>
            <ta e="T634" id="Seg_1452" s="T633">adv</ta>
            <ta e="T635" id="Seg_1453" s="T634">num</ta>
            <ta e="T636" id="Seg_1454" s="T635">v</ta>
            <ta e="T637" id="Seg_1455" s="T636">v</ta>
            <ta e="T638" id="Seg_1456" s="T637">v</ta>
            <ta e="T639" id="Seg_1457" s="T638">n</ta>
            <ta e="T640" id="Seg_1458" s="T639">adv</ta>
            <ta e="T641" id="Seg_1459" s="T640">dempro</ta>
            <ta e="T642" id="Seg_1460" s="T641">v</ta>
            <ta e="T643" id="Seg_1461" s="T642">pers</ta>
            <ta e="T644" id="Seg_1462" s="T643">pers</ta>
            <ta e="T645" id="Seg_1463" s="T644">v</ta>
            <ta e="T646" id="Seg_1464" s="T645">n</ta>
            <ta e="T647" id="Seg_1465" s="T646">v</ta>
            <ta e="T648" id="Seg_1466" s="T647">conj</ta>
            <ta e="T649" id="Seg_1467" s="T648">dempro</ta>
            <ta e="T650" id="Seg_1468" s="T649">refl</ta>
            <ta e="T651" id="Seg_1469" s="T650">v</ta>
            <ta e="T652" id="Seg_1470" s="T651">pers</ta>
            <ta e="T653" id="Seg_1471" s="T652">n</ta>
            <ta e="T657" id="Seg_1472" s="T656">conj</ta>
            <ta e="T658" id="Seg_1473" s="T657">adv</ta>
            <ta e="T661" id="Seg_1474" s="T660">v</ta>
            <ta e="T662" id="Seg_1475" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_1476" s="T662">ptcl</ta>
            <ta e="T664" id="Seg_1477" s="T663">adv</ta>
            <ta e="T665" id="Seg_1478" s="T664">adv</ta>
            <ta e="T666" id="Seg_1479" s="T665">adv</ta>
            <ta e="T667" id="Seg_1480" s="T666">dempro</ta>
            <ta e="T668" id="Seg_1481" s="T667">v</ta>
            <ta e="T669" id="Seg_1482" s="T668">conj</ta>
            <ta e="T670" id="Seg_1483" s="T669">n</ta>
            <ta e="T672" id="Seg_1484" s="T670">ptcl</ta>
            <ta e="T673" id="Seg_1485" s="T672">n</ta>
            <ta e="T674" id="Seg_1486" s="T673">v</ta>
            <ta e="T675" id="Seg_1487" s="T674">dempro</ta>
            <ta e="T677" id="Seg_1488" s="T675">n</ta>
            <ta e="T678" id="Seg_1489" s="T677">dempro</ta>
            <ta e="T679" id="Seg_1490" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_1491" s="T679">v</ta>
            <ta e="T681" id="Seg_1492" s="T680">quant</ta>
            <ta e="T682" id="Seg_1493" s="T681">v</ta>
            <ta e="T683" id="Seg_1494" s="T682">v</ta>
            <ta e="T684" id="Seg_1495" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_1496" s="T684">pers</ta>
            <ta e="T687" id="Seg_1497" s="T686">n</ta>
            <ta e="T688" id="Seg_1498" s="T687">n</ta>
            <ta e="T689" id="Seg_1499" s="T688">adv</ta>
            <ta e="T690" id="Seg_1500" s="T689">v</ta>
            <ta e="T691" id="Seg_1501" s="T690">conj</ta>
            <ta e="T692" id="Seg_1502" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_1503" s="T692">adv</ta>
            <ta e="T694" id="Seg_1504" s="T693">v</ta>
            <ta e="T695" id="Seg_1505" s="T694">v</ta>
            <ta e="T696" id="Seg_1506" s="T695">num</ta>
            <ta e="T697" id="Seg_1507" s="T696">v</ta>
            <ta e="T698" id="Seg_1508" s="T697">v</ta>
            <ta e="T699" id="Seg_1509" s="T698">conj</ta>
            <ta e="T700" id="Seg_1510" s="T699">n</ta>
            <ta e="T701" id="Seg_1511" s="T700">ptcl</ta>
            <ta e="T702" id="Seg_1512" s="T701">v</ta>
            <ta e="T703" id="Seg_1513" s="T702">v</ta>
            <ta e="T704" id="Seg_1514" s="T703">pers</ta>
            <ta e="T705" id="Seg_1515" s="T704">dempro</ta>
            <ta e="T706" id="Seg_1516" s="T705">v</ta>
            <ta e="T707" id="Seg_1517" s="T706">conj</ta>
            <ta e="T708" id="Seg_1518" s="T707">dempro</ta>
            <ta e="T709" id="Seg_1519" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_1520" s="T709">v</ta>
            <ta e="T711" id="Seg_1521" s="T710">que</ta>
            <ta e="T712" id="Seg_1522" s="T711">v</ta>
            <ta e="T713" id="Seg_1523" s="T712">v</ta>
            <ta e="T714" id="Seg_1524" s="T713">adv</ta>
            <ta e="T715" id="Seg_1525" s="T714">quant</ta>
            <ta e="T716" id="Seg_1526" s="T715">dempro</ta>
            <ta e="T717" id="Seg_1527" s="T716">n</ta>
            <ta e="T718" id="Seg_1528" s="T717">adv</ta>
            <ta e="T719" id="Seg_1529" s="T718">n</ta>
            <ta e="T720" id="Seg_1530" s="T719">v</ta>
            <ta e="T721" id="Seg_1531" s="T720">conj</ta>
            <ta e="T722" id="Seg_1532" s="T721">v</ta>
            <ta e="T723" id="Seg_1533" s="T722">ptcl</ta>
            <ta e="T724" id="Seg_1534" s="T723">n</ta>
            <ta e="T725" id="Seg_1535" s="T724">adv</ta>
            <ta e="T726" id="Seg_1536" s="T725">v</ta>
            <ta e="T727" id="Seg_1537" s="T726">conj</ta>
            <ta e="T728" id="Seg_1538" s="T727">dempro</ta>
            <ta e="T729" id="Seg_1539" s="T728">dempro</ta>
            <ta e="T730" id="Seg_1540" s="T729">n</ta>
            <ta e="T731" id="Seg_1541" s="T730">que</ta>
            <ta e="T732" id="Seg_1542" s="T731">adv</ta>
            <ta e="T733" id="Seg_1543" s="T732">ptcl</ta>
            <ta e="T734" id="Seg_1544" s="T733">v</ta>
            <ta e="T735" id="Seg_1545" s="T734">dempro</ta>
            <ta e="T736" id="Seg_1546" s="T735">n</ta>
            <ta e="T737" id="Seg_1547" s="T736">n</ta>
            <ta e="T738" id="Seg_1548" s="T737">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T609" id="Seg_1549" s="T608">np:L</ta>
            <ta e="T610" id="Seg_1550" s="T609">np.h:E</ta>
            <ta e="T612" id="Seg_1551" s="T611">np.h:E</ta>
            <ta e="T613" id="Seg_1552" s="T612">pro.h:Poss</ta>
            <ta e="T616" id="Seg_1553" s="T615">pro:Th</ta>
            <ta e="T619" id="Seg_1554" s="T618">pro.h:Poss</ta>
            <ta e="T620" id="Seg_1555" s="T619">np.h:A</ta>
            <ta e="T622" id="Seg_1556" s="T621">0.3.h:A</ta>
            <ta e="T623" id="Seg_1557" s="T622">0.3.h:A</ta>
            <ta e="T625" id="Seg_1558" s="T624">np.h:A</ta>
            <ta e="T626" id="Seg_1559" s="T625">np:Th</ta>
            <ta e="T628" id="Seg_1560" s="T627">pro.h:A</ta>
            <ta e="T630" id="Seg_1561" s="T629">0.2.h:A</ta>
            <ta e="T631" id="Seg_1562" s="T630">pro.h:A</ta>
            <ta e="T632" id="Seg_1563" s="T631">pro.h:B</ta>
            <ta e="T634" id="Seg_1564" s="T633">adv:Time</ta>
            <ta e="T635" id="Seg_1565" s="T634">np.h:A</ta>
            <ta e="T638" id="Seg_1566" s="T637">0.3.h:A</ta>
            <ta e="T639" id="Seg_1567" s="T638">np:G</ta>
            <ta e="T640" id="Seg_1568" s="T639">adv:Time</ta>
            <ta e="T641" id="Seg_1569" s="T640">pro.h:A</ta>
            <ta e="T643" id="Seg_1570" s="T642">pro.h:A</ta>
            <ta e="T644" id="Seg_1571" s="T643">pro.h:R</ta>
            <ta e="T646" id="Seg_1572" s="T645">np:Th</ta>
            <ta e="T647" id="Seg_1573" s="T646">0.2.h:A</ta>
            <ta e="T649" id="Seg_1574" s="T648">pro:A</ta>
            <ta e="T652" id="Seg_1575" s="T651">pro.h:B</ta>
            <ta e="T653" id="Seg_1576" s="T652">np:P</ta>
            <ta e="T658" id="Seg_1577" s="T657">adv:Time</ta>
            <ta e="T661" id="Seg_1578" s="T660">0.2.h:A</ta>
            <ta e="T666" id="Seg_1579" s="T665">adv:Time</ta>
            <ta e="T667" id="Seg_1580" s="T666">pro.h:A</ta>
            <ta e="T670" id="Seg_1581" s="T669">np:L</ta>
            <ta e="T673" id="Seg_1582" s="T672">np:P</ta>
            <ta e="T674" id="Seg_1583" s="T673">0.3.h:A</ta>
            <ta e="T678" id="Seg_1584" s="T677">pro:A</ta>
            <ta e="T681" id="Seg_1585" s="T680">np:P</ta>
            <ta e="T683" id="Seg_1586" s="T682">0.3.h:A</ta>
            <ta e="T685" id="Seg_1587" s="T684">pro:B</ta>
            <ta e="T689" id="Seg_1588" s="T688">adv:Time</ta>
            <ta e="T690" id="Seg_1589" s="T689">0.3.h:A</ta>
            <ta e="T693" id="Seg_1590" s="T692">adv:Time</ta>
            <ta e="T694" id="Seg_1591" s="T693">0.3.h:E</ta>
            <ta e="T695" id="Seg_1592" s="T694">0.3.h:A</ta>
            <ta e="T696" id="Seg_1593" s="T695">np.h:A</ta>
            <ta e="T700" id="Seg_1594" s="T699">np.h:A</ta>
            <ta e="T702" id="Seg_1595" s="T701">0.2.h:A</ta>
            <ta e="T704" id="Seg_1596" s="T703">pro.h:B</ta>
            <ta e="T705" id="Seg_1597" s="T704">pro:A</ta>
            <ta e="T708" id="Seg_1598" s="T707">pro.h:E</ta>
            <ta e="T714" id="Seg_1599" s="T713">adv:L</ta>
            <ta e="T717" id="Seg_1600" s="T716">np:Th</ta>
            <ta e="T718" id="Seg_1601" s="T717">adv:Time</ta>
            <ta e="T719" id="Seg_1602" s="T718">np.h:A</ta>
            <ta e="T722" id="Seg_1603" s="T721">0.3.h:A</ta>
            <ta e="T725" id="Seg_1604" s="T724">adv:Time</ta>
            <ta e="T726" id="Seg_1605" s="T725">0.3.h:A</ta>
            <ta e="T730" id="Seg_1606" s="T729">np.h:A</ta>
            <ta e="T736" id="Seg_1607" s="T735">np:L</ta>
            <ta e="T737" id="Seg_1608" s="T736">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T607" id="Seg_1609" s="T606">v:pred</ta>
            <ta e="T610" id="Seg_1610" s="T609">np.h:S</ta>
            <ta e="T612" id="Seg_1611" s="T611">np.h:S</ta>
            <ta e="T613" id="Seg_1612" s="T612">pro.h:S</ta>
            <ta e="T614" id="Seg_1613" s="T613">s:purp</ta>
            <ta e="T615" id="Seg_1614" s="T614">v:pred</ta>
            <ta e="T616" id="Seg_1615" s="T615">pro:O</ta>
            <ta e="T617" id="Seg_1616" s="T616">s:purp</ta>
            <ta e="T618" id="Seg_1617" s="T617">v:pred</ta>
            <ta e="T619" id="Seg_1618" s="T618">pro.h:S</ta>
            <ta e="T620" id="Seg_1619" s="T619">np.h:S</ta>
            <ta e="T621" id="Seg_1620" s="T620">v:pred</ta>
            <ta e="T622" id="Seg_1621" s="T621">v:pred 0.3.h:S</ta>
            <ta e="T623" id="Seg_1622" s="T622">v:pred 0.3.h:S</ta>
            <ta e="T625" id="Seg_1623" s="T624">np.h:S</ta>
            <ta e="T626" id="Seg_1624" s="T625">np:O</ta>
            <ta e="T627" id="Seg_1625" s="T626">v:pred</ta>
            <ta e="T628" id="Seg_1626" s="T627">pro.h:S</ta>
            <ta e="T629" id="Seg_1627" s="T628">v:pred</ta>
            <ta e="T630" id="Seg_1628" s="T629">v:pred 0.2.h:S</ta>
            <ta e="T631" id="Seg_1629" s="T630">pro.h:S</ta>
            <ta e="T633" id="Seg_1630" s="T632">v:pred</ta>
            <ta e="T635" id="Seg_1631" s="T634">np.h:S</ta>
            <ta e="T636" id="Seg_1632" s="T635">v:pred</ta>
            <ta e="T637" id="Seg_1633" s="T636">v:pred</ta>
            <ta e="T638" id="Seg_1634" s="T637">v:pred 0.3.h:S</ta>
            <ta e="T641" id="Seg_1635" s="T640">pro.h:S</ta>
            <ta e="T642" id="Seg_1636" s="T641">v:pred</ta>
            <ta e="T643" id="Seg_1637" s="T642">pro.h:S</ta>
            <ta e="T645" id="Seg_1638" s="T644">v:pred</ta>
            <ta e="T646" id="Seg_1639" s="T645">np:O</ta>
            <ta e="T647" id="Seg_1640" s="T646">v:pred 0.2.h:S</ta>
            <ta e="T649" id="Seg_1641" s="T648">pro:S</ta>
            <ta e="T651" id="Seg_1642" s="T650">v:pred</ta>
            <ta e="T661" id="Seg_1643" s="T660">v:pred 0.2.h:S</ta>
            <ta e="T663" id="Seg_1644" s="T662">ptcl.neg</ta>
            <ta e="T667" id="Seg_1645" s="T666">pro.h:S</ta>
            <ta e="T668" id="Seg_1646" s="T667">v:pred</ta>
            <ta e="T672" id="Seg_1647" s="T670">ptcl:pred</ta>
            <ta e="T673" id="Seg_1648" s="T672">np:O</ta>
            <ta e="T678" id="Seg_1649" s="T677">pro:S</ta>
            <ta e="T680" id="Seg_1650" s="T679">v:pred</ta>
            <ta e="T681" id="Seg_1651" s="T680">np:O</ta>
            <ta e="T682" id="Seg_1652" s="T681">v:pred</ta>
            <ta e="T683" id="Seg_1653" s="T682">v:pred 0.3.h:S</ta>
            <ta e="T690" id="Seg_1654" s="T689">v:pred 0.3.h:S</ta>
            <ta e="T694" id="Seg_1655" s="T693">v:pred 0.3.h:S</ta>
            <ta e="T695" id="Seg_1656" s="T694">v:pred 0.3.h:S</ta>
            <ta e="T696" id="Seg_1657" s="T695">np.h:S</ta>
            <ta e="T697" id="Seg_1658" s="T696">conv:pred</ta>
            <ta e="T698" id="Seg_1659" s="T697">v:pred</ta>
            <ta e="T700" id="Seg_1660" s="T699">np.h:S</ta>
            <ta e="T701" id="Seg_1661" s="T700">ptcl:pred</ta>
            <ta e="T702" id="Seg_1662" s="T701">v:pred 0.2:S</ta>
            <ta e="T703" id="Seg_1663" s="T702">v:pred</ta>
            <ta e="T705" id="Seg_1664" s="T704">pro:S</ta>
            <ta e="T706" id="Seg_1665" s="T705">v:pred</ta>
            <ta e="T708" id="Seg_1666" s="T707">pro.h:S</ta>
            <ta e="T709" id="Seg_1667" s="T708">ptcl.neg</ta>
            <ta e="T710" id="Seg_1668" s="T709">v:pred</ta>
            <ta e="T713" id="Seg_1669" s="T712">v:pred</ta>
            <ta e="T717" id="Seg_1670" s="T716">np:S</ta>
            <ta e="T719" id="Seg_1671" s="T718">np.h:S</ta>
            <ta e="T720" id="Seg_1672" s="T719">v:pred</ta>
            <ta e="T722" id="Seg_1673" s="T721">v:pred 0.3.h:S</ta>
            <ta e="T726" id="Seg_1674" s="T725">v:pred 0.3.h:S</ta>
            <ta e="T730" id="Seg_1675" s="T729">np.h:S</ta>
            <ta e="T733" id="Seg_1676" s="T732">ptcl.neg</ta>
            <ta e="T734" id="Seg_1677" s="T733">v:pred</ta>
            <ta e="T737" id="Seg_1678" s="T736">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T609" id="Seg_1679" s="T608">TAT:cult</ta>
            <ta e="T611" id="Seg_1680" s="T610">RUS:gram</ta>
            <ta e="T616" id="Seg_1681" s="T615">TURK:gram(INDEF)</ta>
            <ta e="T648" id="Seg_1682" s="T647">RUS:gram</ta>
            <ta e="T653" id="Seg_1683" s="T652">RUS:cult</ta>
            <ta e="T657" id="Seg_1684" s="T656">RUS:gram</ta>
            <ta e="T662" id="Seg_1685" s="T661">RUS:cult</ta>
            <ta e="T669" id="Seg_1686" s="T668">RUS:gram</ta>
            <ta e="T672" id="Seg_1687" s="T670">RUS:gram</ta>
            <ta e="T679" id="Seg_1688" s="T678">TURK:disc</ta>
            <ta e="T684" id="Seg_1689" s="T683">RUS:cult</ta>
            <ta e="T691" id="Seg_1690" s="T690">RUS:gram</ta>
            <ta e="T699" id="Seg_1691" s="T698">RUS:gram</ta>
            <ta e="T701" id="Seg_1692" s="T700">RUS:gram</ta>
            <ta e="T707" id="Seg_1693" s="T706">RUS:gram</ta>
            <ta e="T714" id="Seg_1694" s="T713">RUS:core</ta>
            <ta e="T715" id="Seg_1695" s="T714">TURK:disc</ta>
            <ta e="T717" id="Seg_1696" s="T716">RUS:cult</ta>
            <ta e="T719" id="Seg_1697" s="T718">RUS:core</ta>
            <ta e="T721" id="Seg_1698" s="T720">RUS:gram</ta>
            <ta e="T723" id="Seg_1699" s="T722">RUS:cult</ta>
            <ta e="T724" id="Seg_1700" s="T723">RUS:cult</ta>
            <ta e="T727" id="Seg_1701" s="T726">RUS:gram</ta>
            <ta e="T731" id="Seg_1702" s="T730">TURK:gram(INDEF)</ta>
            <ta e="T736" id="Seg_1703" s="T735">RUS:cult</ta>
            <ta e="T737" id="Seg_1704" s="T736">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T633" id="Seg_1705" s="T629">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T612" id="Seg_1706" s="T606">Жили в одной деревне девочка и мальчик.</ta>
            <ta e="T616" id="Seg_1707" s="T612">Есть у них было нечего.</ta>
            <ta e="T619" id="Seg_1708" s="T616">Есть нечего им.</ta>
            <ta e="T621" id="Seg_1709" s="T619">Девочка пошла.</ta>
            <ta e="T627" id="Seg_1710" s="T621">Идёт, идёт, [видит,] женщина воду несёт.</ta>
            <ta e="T633" id="Seg_1711" s="T627">Она говорит: «Дай я тебе помогу».</ta>
            <ta e="T637" id="Seg_1712" s="T633">Потом вдвоём несли, несли [воду].</ta>
            <ta e="T639" id="Seg_1713" s="T637">Принесли домой.</ta>
            <ta e="T642" id="Seg_1714" s="T639">Потом она говорит:</ta>
            <ta e="T647" id="Seg_1715" s="T642">«Я дам тебе туес, иди.</ta>
            <ta e="T661" id="Seg_1716" s="T647">И он сам тебе кашу будет варить, а потом говори:</ta>
            <ta e="T662" id="Seg_1717" s="T661">“Спасибо.</ta>
            <ta e="T665" id="Seg_1718" s="T662">Больше не нужно”».</ta>
            <ta e="T674" id="Seg_1719" s="T665">Потом она пришла домой, (стала…) огонь развела.</ta>
            <ta e="T682" id="Seg_1720" s="T674">Этот туес, он варит, много наварил.</ta>
            <ta e="T688" id="Seg_1721" s="T682">Она говорит: «Спасибо, туес».</ta>
            <ta e="T690" id="Seg_1722" s="T688">Потом они поели.</ta>
            <ta e="T695" id="Seg_1723" s="T690">И так они долго жили, ели.</ta>
            <ta e="T698" id="Seg_1724" s="T695">Однажды [девочка] ушла.</ta>
            <ta e="T704" id="Seg_1725" s="T698">А мальчик [сказал]: «Давай, вари, говорит, для меня».</ta>
            <ta e="T712" id="Seg_1726" s="T704">Он [туес] варит, а он не знает, что сказать.</ta>
            <ta e="T717" id="Seg_1727" s="T712">Течёт, уже везде эта каша.</ta>
            <ta e="T724" id="Seg_1728" s="T717">Потом приходит сестра и кричит: «Спасибо, горшок!»</ta>
            <ta e="T726" id="Seg_1729" s="T724">Потом пришла.</ta>
            <ta e="T737" id="Seg_1730" s="T726">И этот мальчик больше никогда не варил в этом горшке кашу.</ta>
            <ta e="T738" id="Seg_1731" s="T737">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T612" id="Seg_1732" s="T606">There lived a girl and a boy in a village.</ta>
            <ta e="T616" id="Seg_1733" s="T612">They had nothing to eat.</ta>
            <ta e="T619" id="Seg_1734" s="T616">They had nothing to eat.</ta>
            <ta e="T621" id="Seg_1735" s="T619">The girl went.</ta>
            <ta e="T627" id="Seg_1736" s="T621">She comes, she comes, a woman is bringing water.</ta>
            <ta e="T633" id="Seg_1737" s="T627">She says: "Let me help you."</ta>
            <ta e="T637" id="Seg_1738" s="T633">Then the two of them carried and carried [it].</ta>
            <ta e="T639" id="Seg_1739" s="T637">Brought it home.</ta>
            <ta e="T642" id="Seg_1740" s="T639">Then she says:</ta>
            <ta e="T647" id="Seg_1741" s="T642">"I will give you a birchbark vessel, go.</ta>
            <ta e="T661" id="Seg_1742" s="T647">And it will cook porridge for you by itself, and then say:</ta>
            <ta e="T662" id="Seg_1743" s="T661">"Thank you.</ta>
            <ta e="T665" id="Seg_1744" s="T662">I don't need [it] anymore."</ta>
            <ta e="T674" id="Seg_1745" s="T665">Then she came home, (started to) made a fire.</ta>
            <ta e="T682" id="Seg_1746" s="T674">The birchbark vessel, it is cooking, it cooked a lot.</ta>
            <ta e="T688" id="Seg_1747" s="T682">She says: "Thank you, birchbark vessel."</ta>
            <ta e="T690" id="Seg_1748" s="T688">Then they ate.</ta>
            <ta e="T695" id="Seg_1749" s="T690">And so they lived [and] ate for a long time.</ta>
            <ta e="T698" id="Seg_1750" s="T695">Once [the girl] went away.</ta>
            <ta e="T704" id="Seg_1751" s="T698">And the boy [says]: "Go on, cook, he said, for me."</ta>
            <ta e="T712" id="Seg_1752" s="T704">It cooked, but he does not know what to say.</ta>
            <ta e="T717" id="Seg_1753" s="T712">It flows, everywhere is this porridge.</ta>
            <ta e="T724" id="Seg_1754" s="T717">Then his sister comes and shouts: "Thank you, pot!"</ta>
            <ta e="T726" id="Seg_1755" s="T724">Then she came.</ta>
            <ta e="T737" id="Seg_1756" s="T726">And the boy never again cooked porridge in this pot.</ta>
            <ta e="T738" id="Seg_1757" s="T737">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T612" id="Seg_1758" s="T606">Es lebten ein Mädchen und ein Jungen in einem Dorf.</ta>
            <ta e="T616" id="Seg_1759" s="T612">Sie hatten nichts zu essen.</ta>
            <ta e="T619" id="Seg_1760" s="T616">Sie hatten nichts zu essen.</ta>
            <ta e="T621" id="Seg_1761" s="T619">Das Mädchen ging.</ta>
            <ta e="T627" id="Seg_1762" s="T621">Sie kommt, sie kommt, eine Frau holt Wasser.</ta>
            <ta e="T633" id="Seg_1763" s="T627">Sie sagt: „Lass mich dir helfen.“</ta>
            <ta e="T637" id="Seg_1764" s="T633">Dann trugen und trugen die zwei [es].</ta>
            <ta e="T639" id="Seg_1765" s="T637">Brachten es nach Hause.</ta>
            <ta e="T642" id="Seg_1766" s="T639">Dann sagt sie:</ta>
            <ta e="T647" id="Seg_1767" s="T642">„Ich schenke dir ein Birkenrindengefäß, gehe.</ta>
            <ta e="T661" id="Seg_1768" s="T647">Und es wird dir von alleine Brei kochen, und sage dann:</ta>
            <ta e="T662" id="Seg_1769" s="T661">„Danke.</ta>
            <ta e="T665" id="Seg_1770" s="T662">Ich brauche [ihn] nicht mehr.“</ta>
            <ta e="T674" id="Seg_1771" s="T665">Dann kam sie nach Hause, (fing an) Feuer zu machen.</ta>
            <ta e="T682" id="Seg_1772" s="T674">Das Birkenrindengefäß, es kocht, es kocht viel.</ta>
            <ta e="T688" id="Seg_1773" s="T682">Sie sagt: „Danke, Birkenrindengefäß.“</ta>
            <ta e="T690" id="Seg_1774" s="T688">Dann aßen sie.</ta>
            <ta e="T695" id="Seg_1775" s="T690">Und so lebten sie [und] aßen sie langer Zeir.</ta>
            <ta e="T698" id="Seg_1776" s="T695">Einmal ging [das Mädchen] weg.</ta>
            <ta e="T704" id="Seg_1777" s="T698">Und der Junge [sagt]: „Mach schon, koch, sagte er, für mich.“ </ta>
            <ta e="T712" id="Seg_1778" s="T704">Es kochte, aber er weiß nicht, was zu sagen.</ta>
            <ta e="T717" id="Seg_1779" s="T712">Es fließt, überall ist dieser Brei.</ta>
            <ta e="T724" id="Seg_1780" s="T717">Dann kommt seine Schwester und ruft. „Danke, Topf!“</ta>
            <ta e="T726" id="Seg_1781" s="T724">Dann kam sie.</ta>
            <ta e="T737" id="Seg_1782" s="T726">Und der Junge kochte nie wieder in diesem Topf Brei.</ta>
            <ta e="T738" id="Seg_1783" s="T737">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T612" id="Seg_1784" s="T606">[GVY:] Another well-known story about a pot that cooked porridge by itself.</ta>
            <ta e="T633" id="Seg_1785" s="T627">[GVY:] Pronounced kabazərlam. A calque from Russian "дай я тебе помогу".</ta>
            <ta e="T637" id="Seg_1786" s="T633">[GVY:] carried the water?</ta>
            <ta e="T674" id="Seg_1787" s="T665">[AAV] šumuj ?</ta>
            <ta e="T688" id="Seg_1788" s="T682">[GVY:] The first šo may be just a slip of the tongue</ta>
            <ta e="T698" id="Seg_1789" s="T695">[GVY:] Here onʼiʔ probably means "once".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
