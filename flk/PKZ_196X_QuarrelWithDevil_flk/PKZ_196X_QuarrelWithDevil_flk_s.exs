<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID31E19371-474C-41E4-7BE1-0CDA53CB97E9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_QuarrelWithDevil_flk.wav" />
         <referenced-file url="PKZ_196X_QuarrelWithDevil_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_QuarrelWithDevil_flk\PKZ_196X_QuarrelWithDevil_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">205</ud-information>
            <ud-information attribute-name="# HIAT:w">131</ud-information>
            <ud-information attribute-name="# e">129</ud-information>
            <ud-information attribute-name="# HIAT:u">28</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T334" time="0.0" type="appl" />
         <tli id="T335" time="2.047" type="appl" />
         <tli id="T336" time="4.094" type="appl" />
         <tli id="T337" time="6.141" type="appl" />
         <tli id="T338" time="8.188" type="appl" />
         <tli id="T339" time="8.608" type="appl" />
         <tli id="T340" time="9.786601741265315" />
         <tli id="T341" time="10.606" type="appl" />
         <tli id="T342" time="11.427" type="appl" />
         <tli id="T343" time="12.248" type="appl" />
         <tli id="T344" time="12.856" type="appl" />
         <tli id="T345" time="13.465" type="appl" />
         <tli id="T346" time="14.073" type="appl" />
         <tli id="T347" time="14.833" type="appl" />
         <tli id="T348" time="15.592" type="appl" />
         <tli id="T349" time="16.352" type="appl" />
         <tli id="T350" time="17.111" type="appl" />
         <tli id="T351" time="17.871" type="appl" />
         <tli id="T352" time="18.507" type="appl" />
         <tli id="T353" time="19.142" type="appl" />
         <tli id="T354" time="21.59985670415505" />
         <tli id="T355" time="22.578" type="appl" />
         <tli id="T356" time="23.535" type="appl" />
         <tli id="T357" time="24.491" type="appl" />
         <tli id="T358" time="25.448" type="appl" />
         <tli id="T359" time="26.404" type="appl" />
         <tli id="T360" time="26.989" type="appl" />
         <tli id="T361" time="27.446" type="appl" />
         <tli id="T362" time="28.413144837379274" />
         <tli id="T363" time="29.119" type="appl" />
         <tli id="T364" time="29.85" type="appl" />
         <tli id="T365" time="30.581" type="appl" />
         <tli id="T366" time="31.312" type="appl" />
         <tli id="T367" time="32.043" type="appl" />
         <tli id="T368" time="32.868" type="appl" />
         <tli id="T369" time="33.531" type="appl" />
         <tli id="T370" time="35.03976754229597" />
         <tli id="T371" time="35.745" type="appl" />
         <tli id="T372" time="36.317" type="appl" />
         <tli id="T373" time="36.89" type="appl" />
         <tli id="T374" time="37.462" type="appl" />
         <tli id="T375" time="38.035" type="appl" />
         <tli id="T376" time="38.607" type="appl" />
         <tli id="T377" time="41.07972747253193" />
         <tli id="T378" time="41.72" type="appl" />
         <tli id="T379" time="42.178" type="appl" />
         <tli id="T380" time="42.636" type="appl" />
         <tli id="T381" time="43.093" type="appl" />
         <tli id="T382" time="43.576" type="appl" />
         <tli id="T383" time="44.06" type="appl" />
         <tli id="T384" time="44.544" type="appl" />
         <tli id="T385" time="45.33303258896739" />
         <tli id="T386" time="45.819" type="appl" />
         <tli id="T387" time="46.213" type="appl" />
         <tli id="T388" time="46.608" type="appl" />
         <tli id="T389" time="47.002" type="appl" />
         <tli id="T390" time="48.8996755941288" />
         <tli id="T391" time="49.916" type="appl" />
         <tli id="T392" time="50.669" type="appl" />
         <tli id="T393" time="51.422" type="appl" />
         <tli id="T394" time="52.175" type="appl" />
         <tli id="T395" time="52.928" type="appl" />
         <tli id="T396" time="53.681" type="appl" />
         <tli id="T397" time="54.433" type="appl" />
         <tli id="T398" time="55.186" type="appl" />
         <tli id="T399" time="55.939" type="appl" />
         <tli id="T400" time="56.692" type="appl" />
         <tli id="T401" time="57.445" type="appl" />
         <tli id="T402" time="58.198" type="appl" />
         <tli id="T403" time="58.83766956020797" />
         <tli id="T404" time="59.328" type="appl" />
         <tli id="T405" time="59.704" type="appl" />
         <tli id="T406" time="60.08" type="appl" />
         <tli id="T407" time="61.67959081075388" />
         <tli id="T408" time="63.178" type="appl" />
         <tli id="T409" time="64.545" type="appl" />
         <tli id="T410" time="65.913" type="appl" />
         <tli id="T411" time="67.28" type="appl" />
         <tli id="T412" time="68.77287708643937" />
         <tli id="T413" time="69.454" type="appl" />
         <tli id="T414" time="70.06" type="appl" />
         <tli id="T415" time="70.666" type="appl" />
         <tli id="T416" time="71.584" type="appl" />
         <tli id="T417" time="72.483" type="appl" />
         <tli id="T418" time="73.383" type="appl" />
         <tli id="T419" time="74.283" type="appl" />
         <tli id="T420" time="75.055" type="appl" />
         <tli id="T421" time="75.793" type="appl" />
         <tli id="T422" time="76.532" type="appl" />
         <tli id="T423" time="77.27" type="appl" />
         <tli id="T424" time="78.009" type="appl" />
         <tli id="T425" time="78.747" type="appl" />
         <tli id="T426" time="79.486" type="appl" />
         <tli id="T427" time="80.224" type="appl" />
         <tli id="T428" time="80.963" type="appl" />
         <tli id="T429" time="81.701" type="appl" />
         <tli id="T430" time="82.44" type="appl" />
         <tli id="T431" time="83.589" type="appl" />
         <tli id="T432" time="84.738" type="appl" />
         <tli id="T433" time="85.454" type="appl" />
         <tli id="T434" time="86.063" type="appl" />
         <tli id="T435" time="86.672" type="appl" />
         <tli id="T436" time="87.506" type="appl" />
         <tli id="T437" time="88.089" type="appl" />
         <tli id="T438" time="88.672" type="appl" />
         <tli id="T439" time="89.254" type="appl" />
         <tli id="T440" time="89.837" type="appl" />
         <tli id="T441" time="90.42" type="appl" />
         <tli id="T442" time="90.893" type="appl" />
         <tli id="T443" time="91.357" type="appl" />
         <tli id="T444" time="91.82" type="appl" />
         <tli id="T445" time="92.46605323661437" />
         <tli id="T446" time="93.269" type="appl" />
         <tli id="T447" time="93.924" type="appl" />
         <tli id="T448" time="94.579" type="appl" />
         <tli id="T449" time="95.033" type="appl" />
         <tli id="T450" time="95.486" type="appl" />
         <tli id="T451" time="95.94" type="appl" />
         <tli id="T452" time="96.394" type="appl" />
         <tli id="T453" time="96.848" type="appl" />
         <tli id="T454" time="97.301" type="appl" />
         <tli id="T455" time="97.755" type="appl" />
         <tli id="T456" time="98.428" type="appl" />
         <tli id="T457" time="99.081" type="appl" />
         <tli id="T458" time="99.82600440988202" />
         <tli id="T459" time="100.376" type="appl" />
         <tli id="T460" time="101.007" type="appl" />
         <tli id="T461" time="101.62" type="appl" />
         <tli id="T462" time="102.233" type="appl" />
         <tli id="T463" time="102.846" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T363" start="T362">
            <tli id="T362.tx.1" />
         </timeline-fork>
         <timeline-fork end="T450" start="T449">
            <tli id="T449.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T463" id="Seg_0" n="sc" s="T334">
               <ts e="T338" id="Seg_2" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_4" n="HIAT:w" s="T334">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_7" n="HIAT:w" s="T335">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_10" n="HIAT:w" s="T336">eneidəneziʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_13" n="HIAT:w" s="T337">kudonzəbi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_17" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_19" n="HIAT:w" s="T338">Dĭ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_22" n="HIAT:w" s="T339">măndə:</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_24" n="HIAT:ip">"</nts>
                  <ts e="T341" id="Seg_26" n="HIAT:w" s="T340">Šobiam</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_29" n="HIAT:w" s="T341">tăn</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_32" n="HIAT:w" s="T342">mʼegəsʼtə</ts>
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_37" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_39" n="HIAT:w" s="T343">A</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_42" n="HIAT:w" s="T344">dĭ</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_45" n="HIAT:w" s="T345">măndə:</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_47" n="HIAT:ip">"</nts>
                  <ts e="T347" id="Seg_49" n="HIAT:w" s="T346">Nu</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_53" n="HIAT:w" s="T347">măn</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_55" n="HIAT:ip">(</nts>
                  <ts e="T349" id="Seg_57" n="HIAT:w" s="T348">e-</ts>
                  <nts id="Seg_58" n="HIAT:ip">)</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_61" n="HIAT:w" s="T349">ej</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_64" n="HIAT:w" s="T350">moliam</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_68" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_70" n="HIAT:w" s="T351">Kallam</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_73" n="HIAT:w" s="T352">băra</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_76" n="HIAT:w" s="T353">detləm</ts>
                  <nts id="Seg_77" n="HIAT:ip">"</nts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_81" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_83" n="HIAT:w" s="T354">A</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_86" n="HIAT:w" s="T355">bostə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_89" n="HIAT:w" s="T356">urgo</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_92" n="HIAT:w" s="T357">măja</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_95" n="HIAT:w" s="T358">dʼabolaʔbə</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_99" n="HIAT:u" s="T359">
                  <nts id="Seg_100" n="HIAT:ip">"</nts>
                  <ts e="T360" id="Seg_102" n="HIAT:w" s="T359">Dʼabot</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_105" n="HIAT:w" s="T360">dĭ</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_108" n="HIAT:w" s="T361">măjam</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_112" n="HIAT:u" s="T362">
                  <ts e="T362.tx.1" id="Seg_114" n="HIAT:w" s="T362">A</ts>
                  <nts id="Seg_115" n="HIAT:ip">_</nts>
                  <ts e="T363" id="Seg_117" n="HIAT:w" s="T362.tx.1">to</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_120" n="HIAT:w" s="T363">dĭ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_123" n="HIAT:w" s="T364">saʔməluʔləj</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_127" n="HIAT:w" s="T365">turam</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_130" n="HIAT:w" s="T366">pʼaŋluʔləj</ts>
                  <nts id="Seg_131" n="HIAT:ip">"</nts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_135" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_137" n="HIAT:w" s="T367">Bostə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_140" n="HIAT:w" s="T368">kanbi</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_143" n="HIAT:w" s="T369">turandə</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_147" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_149" n="HIAT:w" s="T370">A</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_152" n="HIAT:w" s="T371">dĭ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_155" n="HIAT:w" s="T372">eneidəne</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_158" n="HIAT:w" s="T373">măndə:</ts>
                  <nts id="Seg_159" n="HIAT:ip">"</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_162" n="HIAT:w" s="T374">Ĭmbi</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_165" n="HIAT:w" s="T375">dĭm</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_168" n="HIAT:w" s="T376">dʼabosʼtə</ts>
                  <nts id="Seg_169" n="HIAT:ip">?</nts>
                  <nts id="Seg_170" n="HIAT:ip">"</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_173" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_175" n="HIAT:w" s="T377">Ibi</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_178" n="HIAT:w" s="T378">da</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_181" n="HIAT:w" s="T379">kalla</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_184" n="HIAT:w" s="T380">dʼürbi</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_188" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_190" n="HIAT:w" s="T381">Dĭgəttə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_193" n="HIAT:w" s="T382">kuza</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_197" n="HIAT:w" s="T383">dĭ</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_200" n="HIAT:w" s="T384">šobi</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_204" n="HIAT:u" s="T385">
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <ts e="T386" id="Seg_207" n="HIAT:w" s="T385">No</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_210" n="HIAT:w" s="T386">ĭmbi</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_212" n="HIAT:ip">(</nts>
                  <ts e="T388" id="Seg_214" n="HIAT:w" s="T387">tăn=</ts>
                  <nts id="Seg_215" n="HIAT:ip">)</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_218" n="HIAT:w" s="T388">tănan</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_221" n="HIAT:w" s="T389">kereʔ</ts>
                  <nts id="Seg_222" n="HIAT:ip">?</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_225" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_227" n="HIAT:w" s="T390">Tăn</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_230" n="HIAT:w" s="T391">mănziʔ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_232" n="HIAT:ip">(</nts>
                  <ts e="T393" id="Seg_234" n="HIAT:w" s="T392">x-</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_237" n="HIAT:w" s="T393">x-</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_240" n="HIAT:w" s="T394">x-</ts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_243" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_245" n="HIAT:w" s="T395">mʼegəsiʔlujtstə</ts>
                  <nts id="Seg_246" n="HIAT:ip">)</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_249" n="HIAT:w" s="T396">măna</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_252" n="HIAT:w" s="T397">xatʼel</ts>
                  <nts id="Seg_253" n="HIAT:ip">,</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_256" n="HIAT:w" s="T398">a</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_259" n="HIAT:w" s="T399">tuj</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_262" n="HIAT:w" s="T400">măn</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_265" n="HIAT:w" s="T401">tănan</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_268" n="HIAT:w" s="T402">mʼegəluʔpim</ts>
                  <nts id="Seg_269" n="HIAT:ip">"</nts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_273" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_275" n="HIAT:w" s="T403">Dĭgəttə</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_278" n="HIAT:w" s="T404">dĭ</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_281" n="HIAT:w" s="T405">kalla</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_284" n="HIAT:w" s="T406">dʼürbi</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_288" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_290" n="HIAT:w" s="T407">Dĭgəttə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_293" n="HIAT:w" s="T408">šonəgaʔi</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_295" n="HIAT:ip">(</nts>
                  <ts e="T410" id="Seg_297" n="HIAT:w" s="T409">tu-</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_300" n="HIAT:w" s="T410">tura-</ts>
                  <nts id="Seg_301" n="HIAT:ip">)</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_304" n="HIAT:w" s="T411">turazaŋdə</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_308" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_310" n="HIAT:w" s="T412">Dĭgəttə</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_313" n="HIAT:w" s="T413">dĭ</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_316" n="HIAT:w" s="T414">măndə:</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_318" n="HIAT:ip">"</nts>
                  <nts id="Seg_319" n="HIAT:ip">(</nts>
                  <ts e="T416" id="Seg_321" n="HIAT:w" s="T415">Kan-</ts>
                  <nts id="Seg_322" n="HIAT:ip">)</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_325" n="HIAT:w" s="T416">Kanžəbəj</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_327" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_329" n="HIAT:w" s="T417">bügən=</ts>
                  <nts id="Seg_330" n="HIAT:ip">)</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_333" n="HIAT:w" s="T418">bünə</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_337" n="HIAT:u" s="T419">
                  <nts id="Seg_338" n="HIAT:ip">(</nts>
                  <ts e="T420" id="Seg_340" n="HIAT:w" s="T419">Măn=</ts>
                  <nts id="Seg_341" n="HIAT:ip">)</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_344" n="HIAT:w" s="T420">Miʔ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_346" n="HIAT:ip">(</nts>
                  <ts e="T422" id="Seg_348" n="HIAT:w" s="T421">šobil-</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_351" n="HIAT:w" s="T422">šolal-</ts>
                  <nts id="Seg_352" n="HIAT:ip">)</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_355" n="HIAT:w" s="T423">šobibaʔ</ts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_359" n="HIAT:w" s="T424">dĭgəttə</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_362" n="HIAT:w" s="T425">dĭn</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_365" n="HIAT:w" s="T426">tăn</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_368" n="HIAT:w" s="T427">bü</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_371" n="HIAT:w" s="T428">ej</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_374" n="HIAT:w" s="T429">bĭtliel</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_378" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_380" n="HIAT:w" s="T430">Parlal</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_383" n="HIAT:w" s="T431">bazoʔ</ts>
                  <nts id="Seg_384" n="HIAT:ip">"</nts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_388" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_390" n="HIAT:w" s="T432">Dĭgəttə</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_393" n="HIAT:w" s="T433">šobiʔi</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_396" n="HIAT:w" s="T434">bünə</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_400" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_402" n="HIAT:w" s="T435">Măllaʔbəjəʔ</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_406" n="HIAT:u" s="T436">
                  <nts id="Seg_407" n="HIAT:ip">"</nts>
                  <ts e="T437" id="Seg_409" n="HIAT:w" s="T436">Nada</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_412" n="HIAT:w" s="T437">kuza</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_415" n="HIAT:w" s="T438">štobɨ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_417" n="HIAT:ip">(</nts>
                  <ts e="T440" id="Seg_419" n="HIAT:w" s="T439">măm-</ts>
                  <nts id="Seg_420" n="HIAT:ip">)</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_423" n="HIAT:w" s="T440">mămbi</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_427" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_429" n="HIAT:w" s="T441">Kădə</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_432" n="HIAT:w" s="T442">tăn</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_435" n="HIAT:w" s="T443">bü</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_438" n="HIAT:w" s="T444">bĭtləl</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_442" n="HIAT:u" s="T445">
                  <nts id="Seg_443" n="HIAT:ip">(</nts>
                  <ts e="T446" id="Seg_445" n="HIAT:w" s="T445">Dĭ-</ts>
                  <nts id="Seg_446" n="HIAT:ip">)</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_449" n="HIAT:w" s="T446">Dĭgəttə</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_452" n="HIAT:w" s="T447">kambiʔi</ts>
                  <nts id="Seg_453" n="HIAT:ip">"</nts>
                  <nts id="Seg_454" n="HIAT:ip">.</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_457" n="HIAT:u" s="T448">
                  <nts id="Seg_458" n="HIAT:ip">"</nts>
                  <ts e="T449" id="Seg_460" n="HIAT:w" s="T448">A</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449.tx.1" id="Seg_463" n="HIAT:w" s="T449">na</ts>
                  <nts id="Seg_464" n="HIAT:ip">_</nts>
                  <ts e="T450" id="Seg_466" n="HIAT:w" s="T449.tx.1">što</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_469" n="HIAT:w" s="T450">kuza</ts>
                  <nts id="Seg_470" n="HIAT:ip">,</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_473" n="HIAT:w" s="T451">tăn</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_476" n="HIAT:w" s="T452">bügən</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_478" n="HIAT:ip">(</nts>
                  <ts e="T454" id="Seg_480" n="HIAT:w" s="T453">bɨl-</ts>
                  <nts id="Seg_481" n="HIAT:ip">)</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_484" n="HIAT:w" s="T454">ibiel</ts>
                  <nts id="Seg_485" n="HIAT:ip">?</nts>
                  <nts id="Seg_486" n="HIAT:ip">"</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_489" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_491" n="HIAT:w" s="T455">Ibiem</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_495" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_497" n="HIAT:w" s="T456">Bü</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_500" n="HIAT:w" s="T457">bĭʔpiel</ts>
                  <nts id="Seg_501" n="HIAT:ip">?</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_504" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_506" n="HIAT:w" s="T458">Ej</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_509" n="HIAT:w" s="T459">bĭʔpiem</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_513" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_515" n="HIAT:w" s="T460">Nu</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_518" n="HIAT:w" s="T461">i</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_521" n="HIAT:w" s="T462">kabarləj</ts>
                  <nts id="Seg_522" n="HIAT:ip">.</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T463" id="Seg_524" n="sc" s="T334">
               <ts e="T335" id="Seg_526" n="e" s="T334">Onʼiʔ </ts>
               <ts e="T336" id="Seg_528" n="e" s="T335">kuza </ts>
               <ts e="T337" id="Seg_530" n="e" s="T336">eneidəneziʔ </ts>
               <ts e="T338" id="Seg_532" n="e" s="T337">kudonzəbi. </ts>
               <ts e="T339" id="Seg_534" n="e" s="T338">Dĭ </ts>
               <ts e="T340" id="Seg_536" n="e" s="T339">măndə: </ts>
               <ts e="T341" id="Seg_538" n="e" s="T340">"Šobiam </ts>
               <ts e="T342" id="Seg_540" n="e" s="T341">tăn </ts>
               <ts e="T343" id="Seg_542" n="e" s="T342">mʼegəsʼtə". </ts>
               <ts e="T344" id="Seg_544" n="e" s="T343">A </ts>
               <ts e="T345" id="Seg_546" n="e" s="T344">dĭ </ts>
               <ts e="T346" id="Seg_548" n="e" s="T345">măndə: </ts>
               <ts e="T347" id="Seg_550" n="e" s="T346">"Nu, </ts>
               <ts e="T348" id="Seg_552" n="e" s="T347">măn </ts>
               <ts e="T349" id="Seg_554" n="e" s="T348">(e-) </ts>
               <ts e="T350" id="Seg_556" n="e" s="T349">ej </ts>
               <ts e="T351" id="Seg_558" n="e" s="T350">moliam. </ts>
               <ts e="T352" id="Seg_560" n="e" s="T351">Kallam </ts>
               <ts e="T353" id="Seg_562" n="e" s="T352">băra </ts>
               <ts e="T354" id="Seg_564" n="e" s="T353">detləm". </ts>
               <ts e="T355" id="Seg_566" n="e" s="T354">A </ts>
               <ts e="T356" id="Seg_568" n="e" s="T355">bostə </ts>
               <ts e="T357" id="Seg_570" n="e" s="T356">urgo </ts>
               <ts e="T358" id="Seg_572" n="e" s="T357">măja </ts>
               <ts e="T359" id="Seg_574" n="e" s="T358">dʼabolaʔbə. </ts>
               <ts e="T360" id="Seg_576" n="e" s="T359">"Dʼabot </ts>
               <ts e="T361" id="Seg_578" n="e" s="T360">dĭ </ts>
               <ts e="T362" id="Seg_580" n="e" s="T361">măjam. </ts>
               <ts e="T363" id="Seg_582" n="e" s="T362">A_to </ts>
               <ts e="T364" id="Seg_584" n="e" s="T363">dĭ </ts>
               <ts e="T365" id="Seg_586" n="e" s="T364">saʔməluʔləj, </ts>
               <ts e="T366" id="Seg_588" n="e" s="T365">turam </ts>
               <ts e="T367" id="Seg_590" n="e" s="T366">pʼaŋluʔləj". </ts>
               <ts e="T368" id="Seg_592" n="e" s="T367">Bostə </ts>
               <ts e="T369" id="Seg_594" n="e" s="T368">kanbi </ts>
               <ts e="T370" id="Seg_596" n="e" s="T369">turandə. </ts>
               <ts e="T371" id="Seg_598" n="e" s="T370">A </ts>
               <ts e="T372" id="Seg_600" n="e" s="T371">dĭ </ts>
               <ts e="T373" id="Seg_602" n="e" s="T372">eneidəne </ts>
               <ts e="T374" id="Seg_604" n="e" s="T373">măndə:" </ts>
               <ts e="T375" id="Seg_606" n="e" s="T374">Ĭmbi </ts>
               <ts e="T376" id="Seg_608" n="e" s="T375">dĭm </ts>
               <ts e="T377" id="Seg_610" n="e" s="T376">dʼabosʼtə?" </ts>
               <ts e="T378" id="Seg_612" n="e" s="T377">Ibi </ts>
               <ts e="T379" id="Seg_614" n="e" s="T378">da </ts>
               <ts e="T380" id="Seg_616" n="e" s="T379">kalla </ts>
               <ts e="T381" id="Seg_618" n="e" s="T380">dʼürbi. </ts>
               <ts e="T382" id="Seg_620" n="e" s="T381">Dĭgəttə </ts>
               <ts e="T383" id="Seg_622" n="e" s="T382">kuza, </ts>
               <ts e="T384" id="Seg_624" n="e" s="T383">dĭ </ts>
               <ts e="T385" id="Seg_626" n="e" s="T384">šobi. </ts>
               <ts e="T386" id="Seg_628" n="e" s="T385">"No </ts>
               <ts e="T387" id="Seg_630" n="e" s="T386">ĭmbi </ts>
               <ts e="T388" id="Seg_632" n="e" s="T387">(tăn=) </ts>
               <ts e="T389" id="Seg_634" n="e" s="T388">tănan </ts>
               <ts e="T390" id="Seg_636" n="e" s="T389">kereʔ? </ts>
               <ts e="T391" id="Seg_638" n="e" s="T390">Tăn </ts>
               <ts e="T392" id="Seg_640" n="e" s="T391">mănziʔ </ts>
               <ts e="T393" id="Seg_642" n="e" s="T392">(x- </ts>
               <ts e="T394" id="Seg_644" n="e" s="T393">x- </ts>
               <ts e="T395" id="Seg_646" n="e" s="T394">x-) </ts>
               <ts e="T396" id="Seg_648" n="e" s="T395">(mʼegəsiʔlujtstə) </ts>
               <ts e="T397" id="Seg_650" n="e" s="T396">măna </ts>
               <ts e="T398" id="Seg_652" n="e" s="T397">xatʼel, </ts>
               <ts e="T399" id="Seg_654" n="e" s="T398">a </ts>
               <ts e="T400" id="Seg_656" n="e" s="T399">tuj </ts>
               <ts e="T401" id="Seg_658" n="e" s="T400">măn </ts>
               <ts e="T402" id="Seg_660" n="e" s="T401">tănan </ts>
               <ts e="T403" id="Seg_662" n="e" s="T402">mʼegəluʔpim". </ts>
               <ts e="T404" id="Seg_664" n="e" s="T403">Dĭgəttə </ts>
               <ts e="T405" id="Seg_666" n="e" s="T404">dĭ </ts>
               <ts e="T406" id="Seg_668" n="e" s="T405">kalla </ts>
               <ts e="T407" id="Seg_670" n="e" s="T406">dʼürbi. </ts>
               <ts e="T408" id="Seg_672" n="e" s="T407">Dĭgəttə </ts>
               <ts e="T409" id="Seg_674" n="e" s="T408">šonəgaʔi </ts>
               <ts e="T410" id="Seg_676" n="e" s="T409">(tu- </ts>
               <ts e="T411" id="Seg_678" n="e" s="T410">tura-) </ts>
               <ts e="T412" id="Seg_680" n="e" s="T411">turazaŋdə. </ts>
               <ts e="T413" id="Seg_682" n="e" s="T412">Dĭgəttə </ts>
               <ts e="T414" id="Seg_684" n="e" s="T413">dĭ </ts>
               <ts e="T415" id="Seg_686" n="e" s="T414">măndə: </ts>
               <ts e="T416" id="Seg_688" n="e" s="T415">"(Kan-) </ts>
               <ts e="T417" id="Seg_690" n="e" s="T416">Kanžəbəj </ts>
               <ts e="T418" id="Seg_692" n="e" s="T417">(bügən=) </ts>
               <ts e="T419" id="Seg_694" n="e" s="T418">bünə. </ts>
               <ts e="T420" id="Seg_696" n="e" s="T419">(Măn=) </ts>
               <ts e="T421" id="Seg_698" n="e" s="T420">Miʔ </ts>
               <ts e="T422" id="Seg_700" n="e" s="T421">(šobil- </ts>
               <ts e="T423" id="Seg_702" n="e" s="T422">šolal-) </ts>
               <ts e="T424" id="Seg_704" n="e" s="T423">šobibaʔ, </ts>
               <ts e="T425" id="Seg_706" n="e" s="T424">dĭgəttə </ts>
               <ts e="T426" id="Seg_708" n="e" s="T425">dĭn </ts>
               <ts e="T427" id="Seg_710" n="e" s="T426">tăn </ts>
               <ts e="T428" id="Seg_712" n="e" s="T427">bü </ts>
               <ts e="T429" id="Seg_714" n="e" s="T428">ej </ts>
               <ts e="T430" id="Seg_716" n="e" s="T429">bĭtliel. </ts>
               <ts e="T431" id="Seg_718" n="e" s="T430">Parlal </ts>
               <ts e="T432" id="Seg_720" n="e" s="T431">bazoʔ". </ts>
               <ts e="T433" id="Seg_722" n="e" s="T432">Dĭgəttə </ts>
               <ts e="T434" id="Seg_724" n="e" s="T433">šobiʔi </ts>
               <ts e="T435" id="Seg_726" n="e" s="T434">bünə. </ts>
               <ts e="T436" id="Seg_728" n="e" s="T435">Măllaʔbəjəʔ. </ts>
               <ts e="T437" id="Seg_730" n="e" s="T436">"Nada </ts>
               <ts e="T438" id="Seg_732" n="e" s="T437">kuza </ts>
               <ts e="T439" id="Seg_734" n="e" s="T438">štobɨ </ts>
               <ts e="T440" id="Seg_736" n="e" s="T439">(măm-) </ts>
               <ts e="T441" id="Seg_738" n="e" s="T440">mămbi. </ts>
               <ts e="T442" id="Seg_740" n="e" s="T441">Kădə </ts>
               <ts e="T443" id="Seg_742" n="e" s="T442">tăn </ts>
               <ts e="T444" id="Seg_744" n="e" s="T443">bü </ts>
               <ts e="T445" id="Seg_746" n="e" s="T444">bĭtləl. </ts>
               <ts e="T446" id="Seg_748" n="e" s="T445">(Dĭ-) </ts>
               <ts e="T447" id="Seg_750" n="e" s="T446">Dĭgəttə </ts>
               <ts e="T448" id="Seg_752" n="e" s="T447">kambiʔi". </ts>
               <ts e="T449" id="Seg_754" n="e" s="T448">"A </ts>
               <ts e="T450" id="Seg_756" n="e" s="T449">na_što </ts>
               <ts e="T451" id="Seg_758" n="e" s="T450">kuza, </ts>
               <ts e="T452" id="Seg_760" n="e" s="T451">tăn </ts>
               <ts e="T453" id="Seg_762" n="e" s="T452">bügən </ts>
               <ts e="T454" id="Seg_764" n="e" s="T453">(bɨl-) </ts>
               <ts e="T455" id="Seg_766" n="e" s="T454">ibiel?" </ts>
               <ts e="T456" id="Seg_768" n="e" s="T455">Ibiem. </ts>
               <ts e="T457" id="Seg_770" n="e" s="T456">Bü </ts>
               <ts e="T458" id="Seg_772" n="e" s="T457">bĭʔpiel? </ts>
               <ts e="T459" id="Seg_774" n="e" s="T458">Ej </ts>
               <ts e="T460" id="Seg_776" n="e" s="T459">bĭʔpiem. </ts>
               <ts e="T461" id="Seg_778" n="e" s="T460">Nu </ts>
               <ts e="T462" id="Seg_780" n="e" s="T461">i </ts>
               <ts e="T463" id="Seg_782" n="e" s="T462">kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T338" id="Seg_783" s="T334">PKZ_196X_QuarrelWithDevil_flk.001 (001)</ta>
            <ta e="T343" id="Seg_784" s="T338">PKZ_196X_QuarrelWithDevil_flk.002 (002) </ta>
            <ta e="T351" id="Seg_785" s="T343">PKZ_196X_QuarrelWithDevil_flk.003 (004) </ta>
            <ta e="T354" id="Seg_786" s="T351">PKZ_196X_QuarrelWithDevil_flk.004 (006)</ta>
            <ta e="T359" id="Seg_787" s="T354">PKZ_196X_QuarrelWithDevil_flk.005 (007)</ta>
            <ta e="T362" id="Seg_788" s="T359">PKZ_196X_QuarrelWithDevil_flk.006 (008)</ta>
            <ta e="T367" id="Seg_789" s="T362">PKZ_196X_QuarrelWithDevil_flk.007 (009)</ta>
            <ta e="T370" id="Seg_790" s="T367">PKZ_196X_QuarrelWithDevil_flk.008 (010)</ta>
            <ta e="T377" id="Seg_791" s="T370">PKZ_196X_QuarrelWithDevil_flk.009 (011)</ta>
            <ta e="T381" id="Seg_792" s="T377">PKZ_196X_QuarrelWithDevil_flk.010 (012)</ta>
            <ta e="T385" id="Seg_793" s="T381">PKZ_196X_QuarrelWithDevil_flk.011 (013)</ta>
            <ta e="T390" id="Seg_794" s="T385">PKZ_196X_QuarrelWithDevil_flk.012 (014)</ta>
            <ta e="T403" id="Seg_795" s="T390">PKZ_196X_QuarrelWithDevil_flk.013 (015)</ta>
            <ta e="T407" id="Seg_796" s="T403">PKZ_196X_QuarrelWithDevil_flk.014 (016)</ta>
            <ta e="T412" id="Seg_797" s="T407">PKZ_196X_QuarrelWithDevil_flk.015 (017)</ta>
            <ta e="T419" id="Seg_798" s="T412">PKZ_196X_QuarrelWithDevil_flk.016 (018) </ta>
            <ta e="T430" id="Seg_799" s="T419">PKZ_196X_QuarrelWithDevil_flk.017 (020)</ta>
            <ta e="T432" id="Seg_800" s="T430">PKZ_196X_QuarrelWithDevil_flk.018 (021)</ta>
            <ta e="T435" id="Seg_801" s="T432">PKZ_196X_QuarrelWithDevil_flk.019 (022)</ta>
            <ta e="T436" id="Seg_802" s="T435">PKZ_196X_QuarrelWithDevil_flk.020 (023)</ta>
            <ta e="T441" id="Seg_803" s="T436">PKZ_196X_QuarrelWithDevil_flk.021 (024)</ta>
            <ta e="T445" id="Seg_804" s="T441">PKZ_196X_QuarrelWithDevil_flk.022 (025)</ta>
            <ta e="T448" id="Seg_805" s="T445">PKZ_196X_QuarrelWithDevil_flk.023 (026)</ta>
            <ta e="T455" id="Seg_806" s="T448">PKZ_196X_QuarrelWithDevil_flk.024 (027)</ta>
            <ta e="T456" id="Seg_807" s="T455">PKZ_196X_QuarrelWithDevil_flk.025 (028)</ta>
            <ta e="T458" id="Seg_808" s="T456">PKZ_196X_QuarrelWithDevil_flk.026 (029)</ta>
            <ta e="T460" id="Seg_809" s="T458">PKZ_196X_QuarrelWithDevil_flk.027 (030)</ta>
            <ta e="T463" id="Seg_810" s="T460">PKZ_196X_QuarrelWithDevil_flk.028 (031)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T338" id="Seg_811" s="T334">Onʼiʔ kuza eneidəneziʔ kudonzəbi. </ta>
            <ta e="T343" id="Seg_812" s="T338">Dĭ măndə: "Šobiam tăn mʼegəsʼtə". </ta>
            <ta e="T351" id="Seg_813" s="T343">A dĭ măndə: "Nu, măn (e-) ej moliam. </ta>
            <ta e="T354" id="Seg_814" s="T351">Kallam băra detləm". </ta>
            <ta e="T359" id="Seg_815" s="T354">A bostə urgo măja dʼabolaʔbə. </ta>
            <ta e="T362" id="Seg_816" s="T359">"Dʼabot dĭ măjam. </ta>
            <ta e="T367" id="Seg_817" s="T362">A to dĭ saʔməluʔləj, turam pʼaŋluʔləj". </ta>
            <ta e="T370" id="Seg_818" s="T367">Bostə kanbi turandə. </ta>
            <ta e="T377" id="Seg_819" s="T370">A dĭ eneidəne măndə:" Ĭmbi dĭm dʼabosʼtə?" </ta>
            <ta e="T381" id="Seg_820" s="T377">Ibi da kalla dʼürbi. </ta>
            <ta e="T385" id="Seg_821" s="T381">Dĭgəttə kuza, dĭ šobi. </ta>
            <ta e="T390" id="Seg_822" s="T385">"No ĭmbi (tăn=) tănan kereʔ? </ta>
            <ta e="T403" id="Seg_823" s="T390">Tăn mănziʔ (x- x- x-) (mʼegəsiʔlujtstə) măna xatʼel, a tuj măn tănan mʼegəluʔpim". </ta>
            <ta e="T407" id="Seg_824" s="T403">Dĭgəttə dĭ kalla dʼürbi. </ta>
            <ta e="T412" id="Seg_825" s="T407">Dĭgəttə šonəgaʔi (tu- tura-) turazaŋdə. </ta>
            <ta e="T419" id="Seg_826" s="T412">Dĭgəttə dĭ măndə: "(Kan-) Kanžəbəj (bügən=) bünə. </ta>
            <ta e="T430" id="Seg_827" s="T419">(Măn=) Miʔ (šobil- šolal-) šobibaʔ, dĭgəttə dĭn tăn bü ej bĭtliel. </ta>
            <ta e="T432" id="Seg_828" s="T430">Parlal bazoʔ". </ta>
            <ta e="T435" id="Seg_829" s="T432">Dĭgəttə šobiʔi bünə. </ta>
            <ta e="T436" id="Seg_830" s="T435">Măllaʔbəjəʔ. </ta>
            <ta e="T441" id="Seg_831" s="T436">"Nada kuza štobɨ (măm-) mămbi. </ta>
            <ta e="T445" id="Seg_832" s="T441">Kădə tăn bü bĭtləl." </ta>
            <ta e="T448" id="Seg_833" s="T445">(Dĭ-) Dĭgəttə kambiʔi. </ta>
            <ta e="T455" id="Seg_834" s="T448">"A na što kuza, tăn bügən (bɨl-) ibiel?" </ta>
            <ta e="T456" id="Seg_835" s="T455">Ibiem. </ta>
            <ta e="T458" id="Seg_836" s="T456">Bü bĭʔpiel? </ta>
            <ta e="T460" id="Seg_837" s="T458">Ej bĭʔpiem. </ta>
            <ta e="T463" id="Seg_838" s="T460">Nu i kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T335" id="Seg_839" s="T334">onʼiʔ</ta>
            <ta e="T336" id="Seg_840" s="T335">kuza</ta>
            <ta e="T337" id="Seg_841" s="T336">eneidəne-ziʔ</ta>
            <ta e="T338" id="Seg_842" s="T337">kudo-nzə-bi</ta>
            <ta e="T339" id="Seg_843" s="T338">dĭ</ta>
            <ta e="T340" id="Seg_844" s="T339">măn-də</ta>
            <ta e="T341" id="Seg_845" s="T340">šo-bia-m</ta>
            <ta e="T342" id="Seg_846" s="T341">tăn</ta>
            <ta e="T343" id="Seg_847" s="T342">mʼegə-sʼtə</ta>
            <ta e="T344" id="Seg_848" s="T343">a</ta>
            <ta e="T345" id="Seg_849" s="T344">dĭ</ta>
            <ta e="T346" id="Seg_850" s="T345">măn-də</ta>
            <ta e="T347" id="Seg_851" s="T346">nu</ta>
            <ta e="T348" id="Seg_852" s="T347">măn</ta>
            <ta e="T350" id="Seg_853" s="T349">ej</ta>
            <ta e="T351" id="Seg_854" s="T350">mo-lia-m</ta>
            <ta e="T352" id="Seg_855" s="T351">kal-la-m</ta>
            <ta e="T353" id="Seg_856" s="T352">băra</ta>
            <ta e="T354" id="Seg_857" s="T353">det-lə-m</ta>
            <ta e="T355" id="Seg_858" s="T354">a</ta>
            <ta e="T356" id="Seg_859" s="T355">bos-tə</ta>
            <ta e="T357" id="Seg_860" s="T356">urgo</ta>
            <ta e="T358" id="Seg_861" s="T357">măja</ta>
            <ta e="T359" id="Seg_862" s="T358">dʼabo-laʔbə</ta>
            <ta e="T360" id="Seg_863" s="T359">dʼabo-t</ta>
            <ta e="T361" id="Seg_864" s="T360">dĭ</ta>
            <ta e="T362" id="Seg_865" s="T361">măja-m</ta>
            <ta e="T363" id="Seg_866" s="T362">ato</ta>
            <ta e="T364" id="Seg_867" s="T363">dĭ</ta>
            <ta e="T365" id="Seg_868" s="T364">saʔmə-luʔ-lə-j</ta>
            <ta e="T366" id="Seg_869" s="T365">tura-m</ta>
            <ta e="T367" id="Seg_870" s="T366">pʼaŋ-luʔ-lə-j</ta>
            <ta e="T368" id="Seg_871" s="T367">bos-tə</ta>
            <ta e="T369" id="Seg_872" s="T368">kan-bi</ta>
            <ta e="T370" id="Seg_873" s="T369">tura-ndə</ta>
            <ta e="T371" id="Seg_874" s="T370">a</ta>
            <ta e="T372" id="Seg_875" s="T371">dĭ</ta>
            <ta e="T373" id="Seg_876" s="T372">eneidəne</ta>
            <ta e="T374" id="Seg_877" s="T373">măn-də</ta>
            <ta e="T375" id="Seg_878" s="T374">ĭmbi</ta>
            <ta e="T376" id="Seg_879" s="T375">dĭ-m</ta>
            <ta e="T377" id="Seg_880" s="T376">dʼabo-sʼtə</ta>
            <ta e="T378" id="Seg_881" s="T377">i-bi</ta>
            <ta e="T379" id="Seg_882" s="T378">da</ta>
            <ta e="T380" id="Seg_883" s="T379">kal-la</ta>
            <ta e="T381" id="Seg_884" s="T380">dʼür-bi</ta>
            <ta e="T382" id="Seg_885" s="T381">dĭgəttə</ta>
            <ta e="T383" id="Seg_886" s="T382">kuza</ta>
            <ta e="T384" id="Seg_887" s="T383">dĭ</ta>
            <ta e="T385" id="Seg_888" s="T384">šo-bi</ta>
            <ta e="T386" id="Seg_889" s="T385">no</ta>
            <ta e="T387" id="Seg_890" s="T386">ĭmbi</ta>
            <ta e="T388" id="Seg_891" s="T387">tăn</ta>
            <ta e="T389" id="Seg_892" s="T388">tănan</ta>
            <ta e="T390" id="Seg_893" s="T389">kereʔ</ta>
            <ta e="T391" id="Seg_894" s="T390">tăn</ta>
            <ta e="T392" id="Seg_895" s="T391">măn-ziʔ</ta>
            <ta e="T396" id="Seg_896" s="T395">mʼegə-siʔlujtstə</ta>
            <ta e="T397" id="Seg_897" s="T396">măna</ta>
            <ta e="T398" id="Seg_898" s="T397">xatʼel</ta>
            <ta e="T399" id="Seg_899" s="T398">a</ta>
            <ta e="T400" id="Seg_900" s="T399">tuj</ta>
            <ta e="T401" id="Seg_901" s="T400">măn</ta>
            <ta e="T402" id="Seg_902" s="T401">tănan</ta>
            <ta e="T403" id="Seg_903" s="T402">mʼegə-luʔ-pi-m</ta>
            <ta e="T404" id="Seg_904" s="T403">dĭgəttə</ta>
            <ta e="T405" id="Seg_905" s="T404">dĭ</ta>
            <ta e="T406" id="Seg_906" s="T405">kal-la</ta>
            <ta e="T407" id="Seg_907" s="T406">dʼür-bi</ta>
            <ta e="T408" id="Seg_908" s="T407">dĭgəttə</ta>
            <ta e="T409" id="Seg_909" s="T408">šonə-ga-ʔi</ta>
            <ta e="T412" id="Seg_910" s="T411">tura-zaŋ-də</ta>
            <ta e="T413" id="Seg_911" s="T412">dĭgəttə</ta>
            <ta e="T414" id="Seg_912" s="T413">dĭ</ta>
            <ta e="T415" id="Seg_913" s="T414">măn-də</ta>
            <ta e="T417" id="Seg_914" s="T416">kan-žə-bəj</ta>
            <ta e="T418" id="Seg_915" s="T417">bü-gən</ta>
            <ta e="T419" id="Seg_916" s="T418">bü-nə</ta>
            <ta e="T420" id="Seg_917" s="T419">măn</ta>
            <ta e="T421" id="Seg_918" s="T420">miʔ</ta>
            <ta e="T424" id="Seg_919" s="T423">šo-bi-baʔ</ta>
            <ta e="T425" id="Seg_920" s="T424">dĭgəttə</ta>
            <ta e="T426" id="Seg_921" s="T425">dĭn</ta>
            <ta e="T427" id="Seg_922" s="T426">tăn</ta>
            <ta e="T428" id="Seg_923" s="T427">bü</ta>
            <ta e="T429" id="Seg_924" s="T428">ej</ta>
            <ta e="T430" id="Seg_925" s="T429">bĭt-lie-l</ta>
            <ta e="T431" id="Seg_926" s="T430">par-la-l</ta>
            <ta e="T432" id="Seg_927" s="T431">bazoʔ</ta>
            <ta e="T433" id="Seg_928" s="T432">dĭgəttə</ta>
            <ta e="T434" id="Seg_929" s="T433">šo-bi-ʔi</ta>
            <ta e="T435" id="Seg_930" s="T434">bü-nə</ta>
            <ta e="T436" id="Seg_931" s="T435">măl-laʔbə-jəʔ</ta>
            <ta e="T437" id="Seg_932" s="T436">nada</ta>
            <ta e="T438" id="Seg_933" s="T437">kuza</ta>
            <ta e="T439" id="Seg_934" s="T438">štobɨ</ta>
            <ta e="T441" id="Seg_935" s="T440">măm-bi</ta>
            <ta e="T442" id="Seg_936" s="T441">kădə</ta>
            <ta e="T443" id="Seg_937" s="T442">tăn</ta>
            <ta e="T444" id="Seg_938" s="T443">bü</ta>
            <ta e="T445" id="Seg_939" s="T444">bĭt-lə-l</ta>
            <ta e="T447" id="Seg_940" s="T446">dĭgəttə</ta>
            <ta e="T448" id="Seg_941" s="T447">kam-bi-ʔi</ta>
            <ta e="T449" id="Seg_942" s="T448">a</ta>
            <ta e="T450" id="Seg_943" s="T449">našto</ta>
            <ta e="T451" id="Seg_944" s="T450">kuza</ta>
            <ta e="T452" id="Seg_945" s="T451">tăn</ta>
            <ta e="T453" id="Seg_946" s="T452">bü-gən</ta>
            <ta e="T455" id="Seg_947" s="T454">i-bie-l</ta>
            <ta e="T456" id="Seg_948" s="T455">i-bie-m</ta>
            <ta e="T457" id="Seg_949" s="T456">bü</ta>
            <ta e="T458" id="Seg_950" s="T457">bĭʔ-pie-l</ta>
            <ta e="T459" id="Seg_951" s="T458">ej</ta>
            <ta e="T460" id="Seg_952" s="T459">bĭʔ-pie-m</ta>
            <ta e="T461" id="Seg_953" s="T460">nu</ta>
            <ta e="T462" id="Seg_954" s="T461">i</ta>
            <ta e="T463" id="Seg_955" s="T462">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T335" id="Seg_956" s="T334">onʼiʔ</ta>
            <ta e="T336" id="Seg_957" s="T335">kuza</ta>
            <ta e="T337" id="Seg_958" s="T336">eneidəne-ziʔ</ta>
            <ta e="T338" id="Seg_959" s="T337">kudo-nzə-bi</ta>
            <ta e="T339" id="Seg_960" s="T338">dĭ</ta>
            <ta e="T340" id="Seg_961" s="T339">măn-ntə</ta>
            <ta e="T341" id="Seg_962" s="T340">šo-bi-m</ta>
            <ta e="T342" id="Seg_963" s="T341">tăn</ta>
            <ta e="T343" id="Seg_964" s="T342">mʼegə-zittə</ta>
            <ta e="T344" id="Seg_965" s="T343">a</ta>
            <ta e="T345" id="Seg_966" s="T344">dĭ</ta>
            <ta e="T346" id="Seg_967" s="T345">măn-ntə</ta>
            <ta e="T347" id="Seg_968" s="T346">nu</ta>
            <ta e="T348" id="Seg_969" s="T347">măn</ta>
            <ta e="T350" id="Seg_970" s="T349">ej</ta>
            <ta e="T351" id="Seg_971" s="T350">mo-liA-m</ta>
            <ta e="T352" id="Seg_972" s="T351">kan-lV-m</ta>
            <ta e="T353" id="Seg_973" s="T352">băra</ta>
            <ta e="T354" id="Seg_974" s="T353">det-lV-m</ta>
            <ta e="T355" id="Seg_975" s="T354">a</ta>
            <ta e="T356" id="Seg_976" s="T355">bos-də</ta>
            <ta e="T357" id="Seg_977" s="T356">urgo</ta>
            <ta e="T358" id="Seg_978" s="T357">măja</ta>
            <ta e="T359" id="Seg_979" s="T358">dʼabə-laʔbə</ta>
            <ta e="T360" id="Seg_980" s="T359">dʼabə-t</ta>
            <ta e="T361" id="Seg_981" s="T360">dĭ</ta>
            <ta e="T362" id="Seg_982" s="T361">măja-m</ta>
            <ta e="T363" id="Seg_983" s="T362">ato</ta>
            <ta e="T364" id="Seg_984" s="T363">dĭ</ta>
            <ta e="T365" id="Seg_985" s="T364">saʔmə-luʔbdə-lV-j</ta>
            <ta e="T366" id="Seg_986" s="T365">tura-m</ta>
            <ta e="T367" id="Seg_987" s="T366">pʼaŋdə-luʔbdə-lV-j</ta>
            <ta e="T368" id="Seg_988" s="T367">bos-də</ta>
            <ta e="T369" id="Seg_989" s="T368">kan-bi</ta>
            <ta e="T370" id="Seg_990" s="T369">tura-gəndə</ta>
            <ta e="T371" id="Seg_991" s="T370">a</ta>
            <ta e="T372" id="Seg_992" s="T371">dĭ</ta>
            <ta e="T373" id="Seg_993" s="T372">eneidəne</ta>
            <ta e="T374" id="Seg_994" s="T373">măn-ntə</ta>
            <ta e="T375" id="Seg_995" s="T374">ĭmbi</ta>
            <ta e="T376" id="Seg_996" s="T375">dĭ-m</ta>
            <ta e="T377" id="Seg_997" s="T376">dʼabə-zittə</ta>
            <ta e="T378" id="Seg_998" s="T377">i-bi</ta>
            <ta e="T379" id="Seg_999" s="T378">da</ta>
            <ta e="T380" id="Seg_1000" s="T379">kan-lAʔ</ta>
            <ta e="T381" id="Seg_1001" s="T380">tʼür-bi</ta>
            <ta e="T382" id="Seg_1002" s="T381">dĭgəttə</ta>
            <ta e="T383" id="Seg_1003" s="T382">kuza</ta>
            <ta e="T384" id="Seg_1004" s="T383">dĭ</ta>
            <ta e="T385" id="Seg_1005" s="T384">šo-bi</ta>
            <ta e="T386" id="Seg_1006" s="T385">no</ta>
            <ta e="T387" id="Seg_1007" s="T386">ĭmbi</ta>
            <ta e="T388" id="Seg_1008" s="T387">tăn</ta>
            <ta e="T389" id="Seg_1009" s="T388">tănan</ta>
            <ta e="T390" id="Seg_1010" s="T389">kereʔ</ta>
            <ta e="T391" id="Seg_1011" s="T390">tăn</ta>
            <ta e="T392" id="Seg_1012" s="T391">măn-ziʔ</ta>
            <ta e="T396" id="Seg_1013" s="T395">mʼegə-siʔlujtstə</ta>
            <ta e="T397" id="Seg_1014" s="T396">măna</ta>
            <ta e="T398" id="Seg_1015" s="T397">xatʼel</ta>
            <ta e="T399" id="Seg_1016" s="T398">a</ta>
            <ta e="T400" id="Seg_1017" s="T399">tüj</ta>
            <ta e="T401" id="Seg_1018" s="T400">măn</ta>
            <ta e="T402" id="Seg_1019" s="T401">tănan</ta>
            <ta e="T403" id="Seg_1020" s="T402">mʼegə-luʔbdə-bi-m</ta>
            <ta e="T404" id="Seg_1021" s="T403">dĭgəttə</ta>
            <ta e="T405" id="Seg_1022" s="T404">dĭ</ta>
            <ta e="T406" id="Seg_1023" s="T405">kan-lAʔ</ta>
            <ta e="T407" id="Seg_1024" s="T406">tʼür-bi</ta>
            <ta e="T408" id="Seg_1025" s="T407">dĭgəttə</ta>
            <ta e="T409" id="Seg_1026" s="T408">šonə-gA-jəʔ</ta>
            <ta e="T412" id="Seg_1027" s="T411">tura-zAŋ-Tə</ta>
            <ta e="T413" id="Seg_1028" s="T412">dĭgəttə</ta>
            <ta e="T414" id="Seg_1029" s="T413">dĭ</ta>
            <ta e="T415" id="Seg_1030" s="T414">măn-ntə</ta>
            <ta e="T417" id="Seg_1031" s="T416">kan-žə-bəj</ta>
            <ta e="T418" id="Seg_1032" s="T417">bü-Kən</ta>
            <ta e="T419" id="Seg_1033" s="T418">bü-Tə</ta>
            <ta e="T420" id="Seg_1034" s="T419">măn</ta>
            <ta e="T421" id="Seg_1035" s="T420">miʔ</ta>
            <ta e="T424" id="Seg_1036" s="T423">šo-bi-bAʔ</ta>
            <ta e="T425" id="Seg_1037" s="T424">dĭgəttə</ta>
            <ta e="T426" id="Seg_1038" s="T425">dĭn</ta>
            <ta e="T427" id="Seg_1039" s="T426">tăn</ta>
            <ta e="T428" id="Seg_1040" s="T427">bü</ta>
            <ta e="T429" id="Seg_1041" s="T428">ej</ta>
            <ta e="T430" id="Seg_1042" s="T429">bĭs-liA-l</ta>
            <ta e="T431" id="Seg_1043" s="T430">par-lV-l</ta>
            <ta e="T432" id="Seg_1044" s="T431">bazoʔ</ta>
            <ta e="T433" id="Seg_1045" s="T432">dĭgəttə</ta>
            <ta e="T434" id="Seg_1046" s="T433">šo-bi-jəʔ</ta>
            <ta e="T435" id="Seg_1047" s="T434">bü-Tə</ta>
            <ta e="T436" id="Seg_1048" s="T435">măndo-laʔbə-jəʔ</ta>
            <ta e="T437" id="Seg_1049" s="T436">nadə</ta>
            <ta e="T438" id="Seg_1050" s="T437">kuza</ta>
            <ta e="T439" id="Seg_1051" s="T438">štobɨ</ta>
            <ta e="T441" id="Seg_1052" s="T440">măn-bi</ta>
            <ta e="T442" id="Seg_1053" s="T441">kădaʔ</ta>
            <ta e="T443" id="Seg_1054" s="T442">tăn</ta>
            <ta e="T444" id="Seg_1055" s="T443">bü</ta>
            <ta e="T445" id="Seg_1056" s="T444">bĭs-lV-l</ta>
            <ta e="T447" id="Seg_1057" s="T446">dĭgəttə</ta>
            <ta e="T448" id="Seg_1058" s="T447">kan-bi-jəʔ</ta>
            <ta e="T449" id="Seg_1059" s="T448">a</ta>
            <ta e="T450" id="Seg_1060" s="T449">našto</ta>
            <ta e="T451" id="Seg_1061" s="T450">kuza</ta>
            <ta e="T452" id="Seg_1062" s="T451">tăn</ta>
            <ta e="T453" id="Seg_1063" s="T452">bü-Kən</ta>
            <ta e="T455" id="Seg_1064" s="T454">i-bi-l</ta>
            <ta e="T456" id="Seg_1065" s="T455">i-bi-m</ta>
            <ta e="T457" id="Seg_1066" s="T456">bü</ta>
            <ta e="T458" id="Seg_1067" s="T457">bĭs-bi-l</ta>
            <ta e="T459" id="Seg_1068" s="T458">ej</ta>
            <ta e="T460" id="Seg_1069" s="T459">bĭs-bi-m</ta>
            <ta e="T461" id="Seg_1070" s="T460">nu</ta>
            <ta e="T462" id="Seg_1071" s="T461">i</ta>
            <ta e="T463" id="Seg_1072" s="T462">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T335" id="Seg_1073" s="T334">one.[NOM.SG]</ta>
            <ta e="T336" id="Seg_1074" s="T335">man.[NOM.SG]</ta>
            <ta e="T337" id="Seg_1075" s="T336">devil-INS</ta>
            <ta e="T338" id="Seg_1076" s="T337">scold-DES-PST.[3SG]</ta>
            <ta e="T339" id="Seg_1077" s="T338">this.[NOM.SG]</ta>
            <ta e="T340" id="Seg_1078" s="T339">say-IPFVZ.[3SG]</ta>
            <ta e="T341" id="Seg_1079" s="T340">come-PST-1SG</ta>
            <ta e="T342" id="Seg_1080" s="T341">you.NOM</ta>
            <ta e="T343" id="Seg_1081" s="T342">try-INF.LAT</ta>
            <ta e="T344" id="Seg_1082" s="T343">and</ta>
            <ta e="T345" id="Seg_1083" s="T344">this.[NOM.SG]</ta>
            <ta e="T346" id="Seg_1084" s="T345">say-IPFVZ.[3SG]</ta>
            <ta e="T347" id="Seg_1085" s="T346">well</ta>
            <ta e="T348" id="Seg_1086" s="T347">I.NOM</ta>
            <ta e="T350" id="Seg_1087" s="T349">NEG</ta>
            <ta e="T351" id="Seg_1088" s="T350">can-PRS-1SG</ta>
            <ta e="T352" id="Seg_1089" s="T351">go-FUT-1SG</ta>
            <ta e="T353" id="Seg_1090" s="T352">sack.[NOM.SG]</ta>
            <ta e="T354" id="Seg_1091" s="T353">bring-FUT-1SG</ta>
            <ta e="T355" id="Seg_1092" s="T354">and</ta>
            <ta e="T356" id="Seg_1093" s="T355">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T357" id="Seg_1094" s="T356">big.[NOM.SG]</ta>
            <ta e="T358" id="Seg_1095" s="T357">mountain.[NOM.SG]</ta>
            <ta e="T359" id="Seg_1096" s="T358">capture-DUR.[3SG]</ta>
            <ta e="T360" id="Seg_1097" s="T359">capture-IMP.2SG.O</ta>
            <ta e="T361" id="Seg_1098" s="T360">this.[NOM.SG]</ta>
            <ta e="T362" id="Seg_1099" s="T361">mountain-ACC</ta>
            <ta e="T363" id="Seg_1100" s="T362">otherwise</ta>
            <ta e="T364" id="Seg_1101" s="T363">this.[NOM.SG]</ta>
            <ta e="T365" id="Seg_1102" s="T364">fall-MOM-FUT-3SG</ta>
            <ta e="T366" id="Seg_1103" s="T365">house-NOM/GEN/ACC.1SG</ta>
            <ta e="T367" id="Seg_1104" s="T366">press-MOM-FUT-3SG</ta>
            <ta e="T368" id="Seg_1105" s="T367">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T369" id="Seg_1106" s="T368">go-PST.[3SG]</ta>
            <ta e="T370" id="Seg_1107" s="T369">house-LAT/LOC.3SG</ta>
            <ta e="T371" id="Seg_1108" s="T370">and</ta>
            <ta e="T372" id="Seg_1109" s="T371">this.[NOM.SG]</ta>
            <ta e="T373" id="Seg_1110" s="T372">devil.[NOM.SG]</ta>
            <ta e="T374" id="Seg_1111" s="T373">say-IPFVZ.[3SG]</ta>
            <ta e="T375" id="Seg_1112" s="T374">what.[NOM.SG]</ta>
            <ta e="T376" id="Seg_1113" s="T375">this-ACC</ta>
            <ta e="T377" id="Seg_1114" s="T376">capture-INF.LAT</ta>
            <ta e="T378" id="Seg_1115" s="T377">take-PST.[3SG]</ta>
            <ta e="T379" id="Seg_1116" s="T378">and</ta>
            <ta e="T380" id="Seg_1117" s="T379">go-CVB</ta>
            <ta e="T381" id="Seg_1118" s="T380">disappear-PST.[3SG]</ta>
            <ta e="T382" id="Seg_1119" s="T381">then</ta>
            <ta e="T383" id="Seg_1120" s="T382">man.[NOM.SG]</ta>
            <ta e="T384" id="Seg_1121" s="T383">this.[NOM.SG]</ta>
            <ta e="T385" id="Seg_1122" s="T384">come-PST.[3SG]</ta>
            <ta e="T386" id="Seg_1123" s="T385">well</ta>
            <ta e="T387" id="Seg_1124" s="T386">what.[NOM.SG]</ta>
            <ta e="T388" id="Seg_1125" s="T387">you.NOM</ta>
            <ta e="T389" id="Seg_1126" s="T388">you.DAT</ta>
            <ta e="T390" id="Seg_1127" s="T389">one.needs</ta>
            <ta e="T391" id="Seg_1128" s="T390">you.NOM</ta>
            <ta e="T392" id="Seg_1129" s="T391">I-COM</ta>
            <ta e="T396" id="Seg_1130" s="T395">try-%%</ta>
            <ta e="T397" id="Seg_1131" s="T396">I.ACC</ta>
            <ta e="T398" id="Seg_1132" s="T397">want.PST.M.SG</ta>
            <ta e="T399" id="Seg_1133" s="T398">and</ta>
            <ta e="T400" id="Seg_1134" s="T399">now</ta>
            <ta e="T401" id="Seg_1135" s="T400">I.NOM</ta>
            <ta e="T402" id="Seg_1136" s="T401">you.ACC</ta>
            <ta e="T403" id="Seg_1137" s="T402">try-MOM-PST-1SG</ta>
            <ta e="T404" id="Seg_1138" s="T403">then</ta>
            <ta e="T405" id="Seg_1139" s="T404">this.[NOM.SG]</ta>
            <ta e="T406" id="Seg_1140" s="T405">go-CVB</ta>
            <ta e="T407" id="Seg_1141" s="T406">disappear-PST.[3SG]</ta>
            <ta e="T408" id="Seg_1142" s="T407">then</ta>
            <ta e="T409" id="Seg_1143" s="T408">come-PRS-3PL</ta>
            <ta e="T412" id="Seg_1144" s="T411">house-PL-LAT</ta>
            <ta e="T413" id="Seg_1145" s="T412">then</ta>
            <ta e="T414" id="Seg_1146" s="T413">this.[NOM.SG]</ta>
            <ta e="T415" id="Seg_1147" s="T414">say-IPFVZ.[3SG]</ta>
            <ta e="T417" id="Seg_1148" s="T416">go-OPT.DU/PL-1DU</ta>
            <ta e="T418" id="Seg_1149" s="T417">water-LOC</ta>
            <ta e="T419" id="Seg_1150" s="T418">water-LAT</ta>
            <ta e="T420" id="Seg_1151" s="T419">I.NOM</ta>
            <ta e="T421" id="Seg_1152" s="T420">we.NOM</ta>
            <ta e="T424" id="Seg_1153" s="T423">come-PST-1PL</ta>
            <ta e="T425" id="Seg_1154" s="T424">then</ta>
            <ta e="T426" id="Seg_1155" s="T425">there</ta>
            <ta e="T427" id="Seg_1156" s="T426">you.NOM</ta>
            <ta e="T428" id="Seg_1157" s="T427">water.[NOM.SG]</ta>
            <ta e="T429" id="Seg_1158" s="T428">NEG</ta>
            <ta e="T430" id="Seg_1159" s="T429">drink-PRS-2SG</ta>
            <ta e="T431" id="Seg_1160" s="T430">return-FUT-2SG</ta>
            <ta e="T432" id="Seg_1161" s="T431">again</ta>
            <ta e="T433" id="Seg_1162" s="T432">then</ta>
            <ta e="T434" id="Seg_1163" s="T433">come-PST-3PL</ta>
            <ta e="T435" id="Seg_1164" s="T434">water-LAT</ta>
            <ta e="T436" id="Seg_1165" s="T435">look-DUR-3PL</ta>
            <ta e="T437" id="Seg_1166" s="T436">one.should</ta>
            <ta e="T438" id="Seg_1167" s="T437">man.[NOM.SG]</ta>
            <ta e="T439" id="Seg_1168" s="T438">so.that</ta>
            <ta e="T441" id="Seg_1169" s="T440">say-PST.[3SG]</ta>
            <ta e="T442" id="Seg_1170" s="T441">how</ta>
            <ta e="T443" id="Seg_1171" s="T442">you.NOM</ta>
            <ta e="T444" id="Seg_1172" s="T443">water.[NOM.SG]</ta>
            <ta e="T445" id="Seg_1173" s="T444">drink-FUT-2SG</ta>
            <ta e="T447" id="Seg_1174" s="T446">then</ta>
            <ta e="T448" id="Seg_1175" s="T447">go-PST-3PL</ta>
            <ta e="T449" id="Seg_1176" s="T448">and</ta>
            <ta e="T450" id="Seg_1177" s="T449">what.for</ta>
            <ta e="T451" id="Seg_1178" s="T450">man.[NOM.SG]</ta>
            <ta e="T452" id="Seg_1179" s="T451">you.NOM</ta>
            <ta e="T453" id="Seg_1180" s="T452">water-LOC</ta>
            <ta e="T455" id="Seg_1181" s="T454">be-PST-2SG</ta>
            <ta e="T456" id="Seg_1182" s="T455">be-PST-1SG</ta>
            <ta e="T457" id="Seg_1183" s="T456">water.[NOM.SG]</ta>
            <ta e="T458" id="Seg_1184" s="T457">drink-PST-2SG</ta>
            <ta e="T459" id="Seg_1185" s="T458">NEG</ta>
            <ta e="T460" id="Seg_1186" s="T459">drink-PST-1SG</ta>
            <ta e="T461" id="Seg_1187" s="T460">well</ta>
            <ta e="T462" id="Seg_1188" s="T461">and</ta>
            <ta e="T463" id="Seg_1189" s="T462">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T335" id="Seg_1190" s="T334">один.[NOM.SG]</ta>
            <ta e="T336" id="Seg_1191" s="T335">мужчина.[NOM.SG]</ta>
            <ta e="T337" id="Seg_1192" s="T336">черт-INS</ta>
            <ta e="T338" id="Seg_1193" s="T337">ругать-DES-PST.[3SG]</ta>
            <ta e="T339" id="Seg_1194" s="T338">этот.[NOM.SG]</ta>
            <ta e="T340" id="Seg_1195" s="T339">сказать-IPFVZ.[3SG]</ta>
            <ta e="T341" id="Seg_1196" s="T340">прийти-PST-1SG</ta>
            <ta e="T342" id="Seg_1197" s="T341">ты.NOM</ta>
            <ta e="T343" id="Seg_1198" s="T342">пробовать-INF.LAT</ta>
            <ta e="T344" id="Seg_1199" s="T343">а</ta>
            <ta e="T345" id="Seg_1200" s="T344">этот.[NOM.SG]</ta>
            <ta e="T346" id="Seg_1201" s="T345">сказать-IPFVZ.[3SG]</ta>
            <ta e="T347" id="Seg_1202" s="T346">ну</ta>
            <ta e="T348" id="Seg_1203" s="T347">я.NOM</ta>
            <ta e="T350" id="Seg_1204" s="T349">NEG</ta>
            <ta e="T351" id="Seg_1205" s="T350">мочь-PRS-1SG</ta>
            <ta e="T352" id="Seg_1206" s="T351">пойти-FUT-1SG</ta>
            <ta e="T353" id="Seg_1207" s="T352">мешок.[NOM.SG]</ta>
            <ta e="T354" id="Seg_1208" s="T353">принести-FUT-1SG</ta>
            <ta e="T355" id="Seg_1209" s="T354">а</ta>
            <ta e="T356" id="Seg_1210" s="T355">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T357" id="Seg_1211" s="T356">большой.[NOM.SG]</ta>
            <ta e="T358" id="Seg_1212" s="T357">гора.[NOM.SG]</ta>
            <ta e="T359" id="Seg_1213" s="T358">ловить-DUR.[3SG]</ta>
            <ta e="T360" id="Seg_1214" s="T359">ловить-IMP.2SG.O</ta>
            <ta e="T361" id="Seg_1215" s="T360">этот.[NOM.SG]</ta>
            <ta e="T362" id="Seg_1216" s="T361">гора-ACC</ta>
            <ta e="T363" id="Seg_1217" s="T362">а.то</ta>
            <ta e="T364" id="Seg_1218" s="T363">этот.[NOM.SG]</ta>
            <ta e="T365" id="Seg_1219" s="T364">упасть-MOM-FUT-3SG</ta>
            <ta e="T366" id="Seg_1220" s="T365">дом-NOM/GEN/ACC.1SG</ta>
            <ta e="T367" id="Seg_1221" s="T366">нажимать-MOM-FUT-3SG</ta>
            <ta e="T368" id="Seg_1222" s="T367">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T369" id="Seg_1223" s="T368">пойти-PST.[3SG]</ta>
            <ta e="T370" id="Seg_1224" s="T369">дом-LAT/LOC.3SG</ta>
            <ta e="T371" id="Seg_1225" s="T370">а</ta>
            <ta e="T372" id="Seg_1226" s="T371">этот.[NOM.SG]</ta>
            <ta e="T373" id="Seg_1227" s="T372">черт.[NOM.SG]</ta>
            <ta e="T374" id="Seg_1228" s="T373">сказать-IPFVZ.[3SG]</ta>
            <ta e="T375" id="Seg_1229" s="T374">что.[NOM.SG]</ta>
            <ta e="T376" id="Seg_1230" s="T375">этот-ACC</ta>
            <ta e="T377" id="Seg_1231" s="T376">ловить-INF.LAT</ta>
            <ta e="T378" id="Seg_1232" s="T377">взять-PST.[3SG]</ta>
            <ta e="T379" id="Seg_1233" s="T378">и</ta>
            <ta e="T380" id="Seg_1234" s="T379">пойти-CVB</ta>
            <ta e="T381" id="Seg_1235" s="T380">исчезнуть-PST.[3SG]</ta>
            <ta e="T382" id="Seg_1236" s="T381">тогда</ta>
            <ta e="T383" id="Seg_1237" s="T382">мужчина.[NOM.SG]</ta>
            <ta e="T384" id="Seg_1238" s="T383">этот.[NOM.SG]</ta>
            <ta e="T385" id="Seg_1239" s="T384">прийти-PST.[3SG]</ta>
            <ta e="T386" id="Seg_1240" s="T385">ну</ta>
            <ta e="T387" id="Seg_1241" s="T386">что.[NOM.SG]</ta>
            <ta e="T388" id="Seg_1242" s="T387">ты.NOM</ta>
            <ta e="T389" id="Seg_1243" s="T388">ты.DAT</ta>
            <ta e="T390" id="Seg_1244" s="T389">нужно</ta>
            <ta e="T391" id="Seg_1245" s="T390">ты.NOM</ta>
            <ta e="T392" id="Seg_1246" s="T391">я-COM</ta>
            <ta e="T396" id="Seg_1247" s="T395">пробовать-%%</ta>
            <ta e="T397" id="Seg_1248" s="T396">я.ACC</ta>
            <ta e="T398" id="Seg_1249" s="T397">хотеть.PST.M.SG</ta>
            <ta e="T399" id="Seg_1250" s="T398">а</ta>
            <ta e="T400" id="Seg_1251" s="T399">сейчас</ta>
            <ta e="T401" id="Seg_1252" s="T400">я.NOM</ta>
            <ta e="T402" id="Seg_1253" s="T401">ты.ACC</ta>
            <ta e="T403" id="Seg_1254" s="T402">пробовать-MOM-PST-1SG</ta>
            <ta e="T404" id="Seg_1255" s="T403">тогда</ta>
            <ta e="T405" id="Seg_1256" s="T404">этот.[NOM.SG]</ta>
            <ta e="T406" id="Seg_1257" s="T405">пойти-CVB</ta>
            <ta e="T407" id="Seg_1258" s="T406">исчезнуть-PST.[3SG]</ta>
            <ta e="T408" id="Seg_1259" s="T407">тогда</ta>
            <ta e="T409" id="Seg_1260" s="T408">прийти-PRS-3PL</ta>
            <ta e="T412" id="Seg_1261" s="T411">дом-PL-LAT</ta>
            <ta e="T413" id="Seg_1262" s="T412">тогда</ta>
            <ta e="T414" id="Seg_1263" s="T413">этот.[NOM.SG]</ta>
            <ta e="T415" id="Seg_1264" s="T414">сказать-IPFVZ.[3SG]</ta>
            <ta e="T417" id="Seg_1265" s="T416">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T418" id="Seg_1266" s="T417">вода-LOC</ta>
            <ta e="T419" id="Seg_1267" s="T418">вода-LAT</ta>
            <ta e="T420" id="Seg_1268" s="T419">я.NOM</ta>
            <ta e="T421" id="Seg_1269" s="T420">мы.NOM</ta>
            <ta e="T424" id="Seg_1270" s="T423">прийти-PST-1PL</ta>
            <ta e="T425" id="Seg_1271" s="T424">тогда</ta>
            <ta e="T426" id="Seg_1272" s="T425">там</ta>
            <ta e="T427" id="Seg_1273" s="T426">ты.NOM</ta>
            <ta e="T428" id="Seg_1274" s="T427">вода.[NOM.SG]</ta>
            <ta e="T429" id="Seg_1275" s="T428">NEG</ta>
            <ta e="T430" id="Seg_1276" s="T429">пить-PRS-2SG</ta>
            <ta e="T431" id="Seg_1277" s="T430">вернуться-FUT-2SG</ta>
            <ta e="T432" id="Seg_1278" s="T431">опять</ta>
            <ta e="T433" id="Seg_1279" s="T432">тогда</ta>
            <ta e="T434" id="Seg_1280" s="T433">прийти-PST-3PL</ta>
            <ta e="T435" id="Seg_1281" s="T434">вода-LAT</ta>
            <ta e="T436" id="Seg_1282" s="T435">смотреть-DUR-3PL</ta>
            <ta e="T437" id="Seg_1283" s="T436">надо</ta>
            <ta e="T438" id="Seg_1284" s="T437">мужчина.[NOM.SG]</ta>
            <ta e="T439" id="Seg_1285" s="T438">чтобы</ta>
            <ta e="T441" id="Seg_1286" s="T440">сказать-PST.[3SG]</ta>
            <ta e="T442" id="Seg_1287" s="T441">как</ta>
            <ta e="T443" id="Seg_1288" s="T442">ты.NOM</ta>
            <ta e="T444" id="Seg_1289" s="T443">вода.[NOM.SG]</ta>
            <ta e="T445" id="Seg_1290" s="T444">пить-FUT-2SG</ta>
            <ta e="T447" id="Seg_1291" s="T446">тогда</ta>
            <ta e="T448" id="Seg_1292" s="T447">пойти-PST-3PL</ta>
            <ta e="T449" id="Seg_1293" s="T448">а</ta>
            <ta e="T450" id="Seg_1294" s="T449">зачем</ta>
            <ta e="T451" id="Seg_1295" s="T450">мужчина.[NOM.SG]</ta>
            <ta e="T452" id="Seg_1296" s="T451">ты.NOM</ta>
            <ta e="T453" id="Seg_1297" s="T452">вода-LOC</ta>
            <ta e="T455" id="Seg_1298" s="T454">быть-PST-2SG</ta>
            <ta e="T456" id="Seg_1299" s="T455">быть-PST-1SG</ta>
            <ta e="T457" id="Seg_1300" s="T456">вода.[NOM.SG]</ta>
            <ta e="T458" id="Seg_1301" s="T457">пить-PST-2SG</ta>
            <ta e="T459" id="Seg_1302" s="T458">NEG</ta>
            <ta e="T460" id="Seg_1303" s="T459">пить-PST-1SG</ta>
            <ta e="T461" id="Seg_1304" s="T460">ну</ta>
            <ta e="T462" id="Seg_1305" s="T461">и</ta>
            <ta e="T463" id="Seg_1306" s="T462">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T335" id="Seg_1307" s="T334">num-n:case</ta>
            <ta e="T336" id="Seg_1308" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_1309" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_1310" s="T337">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T339" id="Seg_1311" s="T338">dempro-n:case</ta>
            <ta e="T340" id="Seg_1312" s="T339">v-v&gt;v-v:pn</ta>
            <ta e="T341" id="Seg_1313" s="T340">v-v:tense-v:pn</ta>
            <ta e="T342" id="Seg_1314" s="T341">pers</ta>
            <ta e="T343" id="Seg_1315" s="T342">v-v:n.fin</ta>
            <ta e="T344" id="Seg_1316" s="T343">conj</ta>
            <ta e="T345" id="Seg_1317" s="T344">dempro-n:case</ta>
            <ta e="T346" id="Seg_1318" s="T345">v-v&gt;v-v:pn</ta>
            <ta e="T347" id="Seg_1319" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_1320" s="T347">pers</ta>
            <ta e="T350" id="Seg_1321" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_1322" s="T350">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_1323" s="T351">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_1324" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_1325" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_1326" s="T354">conj</ta>
            <ta e="T356" id="Seg_1327" s="T355">refl-n:case.poss</ta>
            <ta e="T357" id="Seg_1328" s="T356">adj-n:case</ta>
            <ta e="T358" id="Seg_1329" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_1330" s="T358">v-v&gt;v-v:pn</ta>
            <ta e="T360" id="Seg_1331" s="T359">v-v:mood.pn</ta>
            <ta e="T361" id="Seg_1332" s="T360">dempro-n:case</ta>
            <ta e="T362" id="Seg_1333" s="T361">n-n:case</ta>
            <ta e="T363" id="Seg_1334" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_1335" s="T363">dempro-n:case</ta>
            <ta e="T365" id="Seg_1336" s="T364">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T366" id="Seg_1337" s="T365">n-n:case.poss</ta>
            <ta e="T367" id="Seg_1338" s="T366">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_1339" s="T367">refl-n:case.poss</ta>
            <ta e="T369" id="Seg_1340" s="T368">v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_1341" s="T369">n-n:case.poss</ta>
            <ta e="T371" id="Seg_1342" s="T370">conj</ta>
            <ta e="T372" id="Seg_1343" s="T371">dempro-n:case</ta>
            <ta e="T373" id="Seg_1344" s="T372">n-n:case</ta>
            <ta e="T374" id="Seg_1345" s="T373">v-v&gt;v-v:pn</ta>
            <ta e="T375" id="Seg_1346" s="T374">que-n:case</ta>
            <ta e="T376" id="Seg_1347" s="T375">dempro-n:case</ta>
            <ta e="T377" id="Seg_1348" s="T376">v-v:n.fin</ta>
            <ta e="T378" id="Seg_1349" s="T377">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_1350" s="T378">conj</ta>
            <ta e="T380" id="Seg_1351" s="T379">v-v:n.fin</ta>
            <ta e="T381" id="Seg_1352" s="T380">v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_1353" s="T381">adv</ta>
            <ta e="T383" id="Seg_1354" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_1355" s="T383">dempro-n:case</ta>
            <ta e="T385" id="Seg_1356" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_1357" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_1358" s="T386">que-n:case</ta>
            <ta e="T388" id="Seg_1359" s="T387">pers</ta>
            <ta e="T389" id="Seg_1360" s="T388">pers</ta>
            <ta e="T390" id="Seg_1361" s="T389">adv</ta>
            <ta e="T391" id="Seg_1362" s="T390">pers</ta>
            <ta e="T392" id="Seg_1363" s="T391">pers-n:case</ta>
            <ta e="T396" id="Seg_1364" s="T395">v-%%</ta>
            <ta e="T397" id="Seg_1365" s="T396">pers</ta>
            <ta e="T398" id="Seg_1366" s="T397">v</ta>
            <ta e="T399" id="Seg_1367" s="T398">conj</ta>
            <ta e="T400" id="Seg_1368" s="T399">adv</ta>
            <ta e="T401" id="Seg_1369" s="T400">pers</ta>
            <ta e="T402" id="Seg_1370" s="T401">pers</ta>
            <ta e="T403" id="Seg_1371" s="T402">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T404" id="Seg_1372" s="T403">adv</ta>
            <ta e="T405" id="Seg_1373" s="T404">dempro-n:case</ta>
            <ta e="T406" id="Seg_1374" s="T405">v-v:n.fin</ta>
            <ta e="T407" id="Seg_1375" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_1376" s="T407">adv</ta>
            <ta e="T409" id="Seg_1377" s="T408">v-v:tense-v:pn</ta>
            <ta e="T412" id="Seg_1378" s="T411">n-n:num-n:case</ta>
            <ta e="T413" id="Seg_1379" s="T412">adv</ta>
            <ta e="T414" id="Seg_1380" s="T413">dempro-n:case</ta>
            <ta e="T415" id="Seg_1381" s="T414">v-v&gt;v-v:pn</ta>
            <ta e="T417" id="Seg_1382" s="T416">v-v:mood-v:pn</ta>
            <ta e="T418" id="Seg_1383" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_1384" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_1385" s="T419">pers</ta>
            <ta e="T421" id="Seg_1386" s="T420">pers</ta>
            <ta e="T424" id="Seg_1387" s="T423">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_1388" s="T424">adv</ta>
            <ta e="T426" id="Seg_1389" s="T425">adv</ta>
            <ta e="T427" id="Seg_1390" s="T426">pers</ta>
            <ta e="T428" id="Seg_1391" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_1392" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_1393" s="T429">v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_1394" s="T430">v-v:tense-v:pn</ta>
            <ta e="T432" id="Seg_1395" s="T431">adv</ta>
            <ta e="T433" id="Seg_1396" s="T432">adv</ta>
            <ta e="T434" id="Seg_1397" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_1398" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_1399" s="T435">v-v&gt;v-v:pn</ta>
            <ta e="T437" id="Seg_1400" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_1401" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_1402" s="T438">conj</ta>
            <ta e="T441" id="Seg_1403" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_1404" s="T441">que</ta>
            <ta e="T443" id="Seg_1405" s="T442">pers</ta>
            <ta e="T444" id="Seg_1406" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_1407" s="T444">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_1408" s="T446">adv</ta>
            <ta e="T448" id="Seg_1409" s="T447">v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_1410" s="T448">conj</ta>
            <ta e="T450" id="Seg_1411" s="T449">adv</ta>
            <ta e="T451" id="Seg_1412" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_1413" s="T451">pers</ta>
            <ta e="T453" id="Seg_1414" s="T452">n-n:case</ta>
            <ta e="T455" id="Seg_1415" s="T454">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_1416" s="T455">v-v:tense-v:pn</ta>
            <ta e="T457" id="Seg_1417" s="T456">n-n:case</ta>
            <ta e="T458" id="Seg_1418" s="T457">v-v:tense-v:pn</ta>
            <ta e="T459" id="Seg_1419" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_1420" s="T459">v-v:tense-v:pn</ta>
            <ta e="T461" id="Seg_1421" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_1422" s="T461">conj</ta>
            <ta e="T463" id="Seg_1423" s="T462">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T335" id="Seg_1424" s="T334">num</ta>
            <ta e="T336" id="Seg_1425" s="T335">n</ta>
            <ta e="T337" id="Seg_1426" s="T336">n</ta>
            <ta e="T338" id="Seg_1427" s="T337">v</ta>
            <ta e="T339" id="Seg_1428" s="T338">dempro</ta>
            <ta e="T340" id="Seg_1429" s="T339">v</ta>
            <ta e="T341" id="Seg_1430" s="T340">v</ta>
            <ta e="T342" id="Seg_1431" s="T341">pers</ta>
            <ta e="T343" id="Seg_1432" s="T342">v</ta>
            <ta e="T344" id="Seg_1433" s="T343">conj</ta>
            <ta e="T345" id="Seg_1434" s="T344">dempro</ta>
            <ta e="T346" id="Seg_1435" s="T345">v</ta>
            <ta e="T347" id="Seg_1436" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_1437" s="T347">pers</ta>
            <ta e="T350" id="Seg_1438" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_1439" s="T350">v</ta>
            <ta e="T352" id="Seg_1440" s="T351">v</ta>
            <ta e="T353" id="Seg_1441" s="T352">n</ta>
            <ta e="T354" id="Seg_1442" s="T353">v</ta>
            <ta e="T355" id="Seg_1443" s="T354">conj</ta>
            <ta e="T356" id="Seg_1444" s="T355">refl</ta>
            <ta e="T357" id="Seg_1445" s="T356">adj</ta>
            <ta e="T358" id="Seg_1446" s="T357">n</ta>
            <ta e="T359" id="Seg_1447" s="T358">v</ta>
            <ta e="T360" id="Seg_1448" s="T359">v</ta>
            <ta e="T361" id="Seg_1449" s="T360">dempro</ta>
            <ta e="T362" id="Seg_1450" s="T361">n</ta>
            <ta e="T363" id="Seg_1451" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_1452" s="T363">dempro</ta>
            <ta e="T365" id="Seg_1453" s="T364">v</ta>
            <ta e="T366" id="Seg_1454" s="T365">n</ta>
            <ta e="T367" id="Seg_1455" s="T366">v</ta>
            <ta e="T368" id="Seg_1456" s="T367">refl</ta>
            <ta e="T369" id="Seg_1457" s="T368">v</ta>
            <ta e="T370" id="Seg_1458" s="T369">n</ta>
            <ta e="T371" id="Seg_1459" s="T370">conj</ta>
            <ta e="T372" id="Seg_1460" s="T371">dempro</ta>
            <ta e="T373" id="Seg_1461" s="T372">n</ta>
            <ta e="T374" id="Seg_1462" s="T373">v</ta>
            <ta e="T375" id="Seg_1463" s="T374">que</ta>
            <ta e="T376" id="Seg_1464" s="T375">dempro</ta>
            <ta e="T377" id="Seg_1465" s="T376">v</ta>
            <ta e="T378" id="Seg_1466" s="T377">v</ta>
            <ta e="T379" id="Seg_1467" s="T378">conj</ta>
            <ta e="T380" id="Seg_1468" s="T379">v</ta>
            <ta e="T381" id="Seg_1469" s="T380">v</ta>
            <ta e="T382" id="Seg_1470" s="T381">adv</ta>
            <ta e="T383" id="Seg_1471" s="T382">n</ta>
            <ta e="T384" id="Seg_1472" s="T383">dempro</ta>
            <ta e="T385" id="Seg_1473" s="T384">v</ta>
            <ta e="T386" id="Seg_1474" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_1475" s="T386">que</ta>
            <ta e="T388" id="Seg_1476" s="T387">pers</ta>
            <ta e="T389" id="Seg_1477" s="T388">pers</ta>
            <ta e="T390" id="Seg_1478" s="T389">adv</ta>
            <ta e="T391" id="Seg_1479" s="T390">pers</ta>
            <ta e="T392" id="Seg_1480" s="T391">pers</ta>
            <ta e="T396" id="Seg_1481" s="T395">v</ta>
            <ta e="T397" id="Seg_1482" s="T396">pers</ta>
            <ta e="T398" id="Seg_1483" s="T397">v</ta>
            <ta e="T399" id="Seg_1484" s="T398">conj</ta>
            <ta e="T400" id="Seg_1485" s="T399">adv</ta>
            <ta e="T401" id="Seg_1486" s="T400">pers</ta>
            <ta e="T402" id="Seg_1487" s="T401">pers</ta>
            <ta e="T403" id="Seg_1488" s="T402">v</ta>
            <ta e="T404" id="Seg_1489" s="T403">adv</ta>
            <ta e="T405" id="Seg_1490" s="T404">dempro</ta>
            <ta e="T406" id="Seg_1491" s="T405">v</ta>
            <ta e="T407" id="Seg_1492" s="T406">v</ta>
            <ta e="T408" id="Seg_1493" s="T407">adv</ta>
            <ta e="T409" id="Seg_1494" s="T408">v</ta>
            <ta e="T412" id="Seg_1495" s="T411">n</ta>
            <ta e="T413" id="Seg_1496" s="T412">adv</ta>
            <ta e="T414" id="Seg_1497" s="T413">dempro</ta>
            <ta e="T415" id="Seg_1498" s="T414">v</ta>
            <ta e="T417" id="Seg_1499" s="T416">v</ta>
            <ta e="T418" id="Seg_1500" s="T417">n</ta>
            <ta e="T419" id="Seg_1501" s="T418">n</ta>
            <ta e="T420" id="Seg_1502" s="T419">pers</ta>
            <ta e="T421" id="Seg_1503" s="T420">pers</ta>
            <ta e="T424" id="Seg_1504" s="T423">v</ta>
            <ta e="T425" id="Seg_1505" s="T424">adv</ta>
            <ta e="T426" id="Seg_1506" s="T425">adv</ta>
            <ta e="T427" id="Seg_1507" s="T426">pers</ta>
            <ta e="T428" id="Seg_1508" s="T427">n</ta>
            <ta e="T429" id="Seg_1509" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_1510" s="T429">v</ta>
            <ta e="T431" id="Seg_1511" s="T430">v</ta>
            <ta e="T432" id="Seg_1512" s="T431">adv</ta>
            <ta e="T433" id="Seg_1513" s="T432">adv</ta>
            <ta e="T434" id="Seg_1514" s="T433">v</ta>
            <ta e="T435" id="Seg_1515" s="T434">n</ta>
            <ta e="T436" id="Seg_1516" s="T435">v</ta>
            <ta e="T437" id="Seg_1517" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_1518" s="T437">n</ta>
            <ta e="T439" id="Seg_1519" s="T438">conj</ta>
            <ta e="T441" id="Seg_1520" s="T440">v</ta>
            <ta e="T442" id="Seg_1521" s="T441">que</ta>
            <ta e="T443" id="Seg_1522" s="T442">pers</ta>
            <ta e="T444" id="Seg_1523" s="T443">n</ta>
            <ta e="T445" id="Seg_1524" s="T444">v</ta>
            <ta e="T447" id="Seg_1525" s="T446">adv</ta>
            <ta e="T448" id="Seg_1526" s="T447">v</ta>
            <ta e="T449" id="Seg_1527" s="T448">conj</ta>
            <ta e="T450" id="Seg_1528" s="T449">adv</ta>
            <ta e="T451" id="Seg_1529" s="T450">n</ta>
            <ta e="T452" id="Seg_1530" s="T451">pers</ta>
            <ta e="T453" id="Seg_1531" s="T452">n</ta>
            <ta e="T455" id="Seg_1532" s="T454">v</ta>
            <ta e="T456" id="Seg_1533" s="T455">v</ta>
            <ta e="T457" id="Seg_1534" s="T456">n</ta>
            <ta e="T458" id="Seg_1535" s="T457">v</ta>
            <ta e="T459" id="Seg_1536" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_1537" s="T459">v</ta>
            <ta e="T461" id="Seg_1538" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_1539" s="T461">conj</ta>
            <ta e="T463" id="Seg_1540" s="T462">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T336" id="Seg_1541" s="T335">np.h:A</ta>
            <ta e="T337" id="Seg_1542" s="T336">np.h:Com</ta>
            <ta e="T339" id="Seg_1543" s="T338">pro.h:A</ta>
            <ta e="T341" id="Seg_1544" s="T340">0.1.h:A</ta>
            <ta e="T342" id="Seg_1545" s="T341">pro.h:Th</ta>
            <ta e="T345" id="Seg_1546" s="T344">pro.h:A</ta>
            <ta e="T348" id="Seg_1547" s="T347">pro.h:A</ta>
            <ta e="T352" id="Seg_1548" s="T351">0.1.h:A</ta>
            <ta e="T353" id="Seg_1549" s="T352">np:Th</ta>
            <ta e="T354" id="Seg_1550" s="T353">0.1.h:A</ta>
            <ta e="T356" id="Seg_1551" s="T355">pro.h:A</ta>
            <ta e="T358" id="Seg_1552" s="T357">np:Th</ta>
            <ta e="T360" id="Seg_1553" s="T359">0.2.h:A</ta>
            <ta e="T362" id="Seg_1554" s="T361">np:Th</ta>
            <ta e="T364" id="Seg_1555" s="T363">np:Th</ta>
            <ta e="T366" id="Seg_1556" s="T365">np:P</ta>
            <ta e="T367" id="Seg_1557" s="T366">0.3:A</ta>
            <ta e="T368" id="Seg_1558" s="T367">pro.h:A</ta>
            <ta e="T370" id="Seg_1559" s="T369">np:G</ta>
            <ta e="T373" id="Seg_1560" s="T372">np.h:A</ta>
            <ta e="T376" id="Seg_1561" s="T375">pro:Th</ta>
            <ta e="T378" id="Seg_1562" s="T377">0.3.h:A</ta>
            <ta e="T381" id="Seg_1563" s="T380">0.3.h:A</ta>
            <ta e="T382" id="Seg_1564" s="T381">adv:Time</ta>
            <ta e="T384" id="Seg_1565" s="T383">pro.h:A</ta>
            <ta e="T387" id="Seg_1566" s="T386">pro:Th</ta>
            <ta e="T389" id="Seg_1567" s="T388">pro.h:B</ta>
            <ta e="T391" id="Seg_1568" s="T390">pro.h:A</ta>
            <ta e="T392" id="Seg_1569" s="T391">pro.h:Com</ta>
            <ta e="T397" id="Seg_1570" s="T396">pro.h:Th</ta>
            <ta e="T400" id="Seg_1571" s="T399">adv:Time</ta>
            <ta e="T401" id="Seg_1572" s="T400">pro.h:A</ta>
            <ta e="T402" id="Seg_1573" s="T401">pro.h:Th</ta>
            <ta e="T404" id="Seg_1574" s="T403">adv:Time</ta>
            <ta e="T405" id="Seg_1575" s="T404">pro.h:A</ta>
            <ta e="T408" id="Seg_1576" s="T407">adv:Time</ta>
            <ta e="T409" id="Seg_1577" s="T408">0.3.h:A</ta>
            <ta e="T412" id="Seg_1578" s="T411">np:G</ta>
            <ta e="T413" id="Seg_1579" s="T412">adv:Time</ta>
            <ta e="T414" id="Seg_1580" s="T413">pro.h:A</ta>
            <ta e="T417" id="Seg_1581" s="T416">0.1.h:A</ta>
            <ta e="T419" id="Seg_1582" s="T418">np:G</ta>
            <ta e="T421" id="Seg_1583" s="T420">pro.h:A</ta>
            <ta e="T425" id="Seg_1584" s="T424">adv:Time</ta>
            <ta e="T426" id="Seg_1585" s="T425">adv:L</ta>
            <ta e="T427" id="Seg_1586" s="T426">pro.h:A</ta>
            <ta e="T428" id="Seg_1587" s="T427">np:P</ta>
            <ta e="T431" id="Seg_1588" s="T430">0.2.h:A</ta>
            <ta e="T433" id="Seg_1589" s="T432">adv:Time</ta>
            <ta e="T434" id="Seg_1590" s="T433">0.3.h:A</ta>
            <ta e="T435" id="Seg_1591" s="T434">np:G</ta>
            <ta e="T436" id="Seg_1592" s="T435">0.3.h:A</ta>
            <ta e="T438" id="Seg_1593" s="T437">np.h:Th</ta>
            <ta e="T441" id="Seg_1594" s="T440">0.3.h:A</ta>
            <ta e="T443" id="Seg_1595" s="T442">pro.h:A</ta>
            <ta e="T444" id="Seg_1596" s="T443">np:P</ta>
            <ta e="T447" id="Seg_1597" s="T446">adv:Time</ta>
            <ta e="T448" id="Seg_1598" s="T447">0.3.h:A</ta>
            <ta e="T451" id="Seg_1599" s="T450">np:Th</ta>
            <ta e="T452" id="Seg_1600" s="T451">pro.h:Th</ta>
            <ta e="T453" id="Seg_1601" s="T452">np:L</ta>
            <ta e="T456" id="Seg_1602" s="T455">pro.h:Th</ta>
            <ta e="T457" id="Seg_1603" s="T456">np:P</ta>
            <ta e="T458" id="Seg_1604" s="T457">0.2.h:A</ta>
            <ta e="T460" id="Seg_1605" s="T459">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T336" id="Seg_1606" s="T335">np.h:S</ta>
            <ta e="T338" id="Seg_1607" s="T337">v:pred</ta>
            <ta e="T339" id="Seg_1608" s="T338">pro.h:S</ta>
            <ta e="T340" id="Seg_1609" s="T339">v:pred</ta>
            <ta e="T341" id="Seg_1610" s="T340">v:pred 0.1.h:S</ta>
            <ta e="T342" id="Seg_1611" s="T341">pro.h:O</ta>
            <ta e="T343" id="Seg_1612" s="T342">v:pred</ta>
            <ta e="T345" id="Seg_1613" s="T344">pro.h:S</ta>
            <ta e="T346" id="Seg_1614" s="T345">v:pred</ta>
            <ta e="T348" id="Seg_1615" s="T347">pro.h:S</ta>
            <ta e="T350" id="Seg_1616" s="T349">ptcl.neg</ta>
            <ta e="T351" id="Seg_1617" s="T350">v:pred</ta>
            <ta e="T352" id="Seg_1618" s="T351">v:pred 0.1.h:S</ta>
            <ta e="T353" id="Seg_1619" s="T352">np:O</ta>
            <ta e="T354" id="Seg_1620" s="T353">v:pred 0.1.h:S</ta>
            <ta e="T356" id="Seg_1621" s="T355">pro.h:S</ta>
            <ta e="T358" id="Seg_1622" s="T357">np:O</ta>
            <ta e="T359" id="Seg_1623" s="T358">v:pred</ta>
            <ta e="T360" id="Seg_1624" s="T359">v:pred 0.2.h:S</ta>
            <ta e="T362" id="Seg_1625" s="T361">np:O</ta>
            <ta e="T364" id="Seg_1626" s="T363">np:S</ta>
            <ta e="T365" id="Seg_1627" s="T364">v:pred</ta>
            <ta e="T366" id="Seg_1628" s="T365">np:O</ta>
            <ta e="T367" id="Seg_1629" s="T366">v:pred 0.3:S</ta>
            <ta e="T368" id="Seg_1630" s="T367">pro.h:S</ta>
            <ta e="T369" id="Seg_1631" s="T368">v:pred</ta>
            <ta e="T373" id="Seg_1632" s="T372">np.h:S</ta>
            <ta e="T374" id="Seg_1633" s="T373">v:pred</ta>
            <ta e="T376" id="Seg_1634" s="T375">pro:O</ta>
            <ta e="T377" id="Seg_1635" s="T376">v:pred</ta>
            <ta e="T378" id="Seg_1636" s="T377">v:pred 0.3.h:S</ta>
            <ta e="T380" id="Seg_1637" s="T379">conv:pred</ta>
            <ta e="T381" id="Seg_1638" s="T380">v:pred 0.3.h:S</ta>
            <ta e="T384" id="Seg_1639" s="T383">pro.h:S</ta>
            <ta e="T385" id="Seg_1640" s="T384">v:pred</ta>
            <ta e="T387" id="Seg_1641" s="T386">pro:S</ta>
            <ta e="T390" id="Seg_1642" s="T389">v:pred</ta>
            <ta e="T397" id="Seg_1643" s="T396">pro.h:O</ta>
            <ta e="T398" id="Seg_1644" s="T397">ptcl:pred</ta>
            <ta e="T401" id="Seg_1645" s="T400">pro.h:S</ta>
            <ta e="T402" id="Seg_1646" s="T401">pro.h:O</ta>
            <ta e="T403" id="Seg_1647" s="T402">v:pred</ta>
            <ta e="T405" id="Seg_1648" s="T404">pro.h:S</ta>
            <ta e="T406" id="Seg_1649" s="T405">conv:pred</ta>
            <ta e="T407" id="Seg_1650" s="T406">v:pred</ta>
            <ta e="T409" id="Seg_1651" s="T408">v:pred 0.3.h:S</ta>
            <ta e="T414" id="Seg_1652" s="T413">pro.h:S</ta>
            <ta e="T415" id="Seg_1653" s="T414">v:pred</ta>
            <ta e="T417" id="Seg_1654" s="T416">v:pred 0.1.h:S</ta>
            <ta e="T421" id="Seg_1655" s="T420">pro.h:S</ta>
            <ta e="T424" id="Seg_1656" s="T423">v:pred</ta>
            <ta e="T427" id="Seg_1657" s="T426">pro.h:S</ta>
            <ta e="T428" id="Seg_1658" s="T427">np:O</ta>
            <ta e="T429" id="Seg_1659" s="T428">ptcl:pred</ta>
            <ta e="T430" id="Seg_1660" s="T429">v:pred</ta>
            <ta e="T431" id="Seg_1661" s="T430">v:pred 0.2.h:S</ta>
            <ta e="T434" id="Seg_1662" s="T433">v:pred 0.3.h:S</ta>
            <ta e="T436" id="Seg_1663" s="T435">v:pred 0.3.h:S</ta>
            <ta e="T437" id="Seg_1664" s="T436">ptcl:pred</ta>
            <ta e="T438" id="Seg_1665" s="T437">np.h:O</ta>
            <ta e="T441" id="Seg_1666" s="T440">v:pred 0.3.h:S</ta>
            <ta e="T443" id="Seg_1667" s="T442">pro.h:S</ta>
            <ta e="T444" id="Seg_1668" s="T443">np:O</ta>
            <ta e="T445" id="Seg_1669" s="T444">v:pred</ta>
            <ta e="T448" id="Seg_1670" s="T447">v:pred 0.3.h:S</ta>
            <ta e="T451" id="Seg_1671" s="T450">n:pred</ta>
            <ta e="T452" id="Seg_1672" s="T451">pro.h:S</ta>
            <ta e="T455" id="Seg_1673" s="T454">cop</ta>
            <ta e="T456" id="Seg_1674" s="T455">v:pred pro.h:S</ta>
            <ta e="T457" id="Seg_1675" s="T456">np:O</ta>
            <ta e="T458" id="Seg_1676" s="T457">v:pred 0.2.h:S</ta>
            <ta e="T459" id="Seg_1677" s="T458">ptcl.neg</ta>
            <ta e="T460" id="Seg_1678" s="T459">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T344" id="Seg_1679" s="T343">RUS:gram</ta>
            <ta e="T355" id="Seg_1680" s="T354">RUS:gram</ta>
            <ta e="T363" id="Seg_1681" s="T362">RUS:gram</ta>
            <ta e="T366" id="Seg_1682" s="T365">TAT:cult</ta>
            <ta e="T370" id="Seg_1683" s="T369">TAT:cult</ta>
            <ta e="T371" id="Seg_1684" s="T370">RUS:gram</ta>
            <ta e="T379" id="Seg_1685" s="T378">RUS:gram</ta>
            <ta e="T386" id="Seg_1686" s="T385">RUS:disc</ta>
            <ta e="T398" id="Seg_1687" s="T397">RUS:mod</ta>
            <ta e="T399" id="Seg_1688" s="T398">RUS:gram</ta>
            <ta e="T412" id="Seg_1689" s="T411">TAT:cult</ta>
            <ta e="T437" id="Seg_1690" s="T436">RUS:mod</ta>
            <ta e="T439" id="Seg_1691" s="T438">RUS:gram</ta>
            <ta e="T449" id="Seg_1692" s="T448">RUS:gram</ta>
            <ta e="T450" id="Seg_1693" s="T449">RUS:mod</ta>
            <ta e="T462" id="Seg_1694" s="T461">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T381" id="Seg_1695" s="T377">RUS:calq</ta>
            <ta e="T398" id="Seg_1696" s="T397">RUS:int</ta>
            <ta e="T462" id="Seg_1697" s="T460">RUS:int.alt</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T338" id="Seg_1698" s="T334">Один человек поссорился с чёртом.</ta>
            <ta e="T340" id="Seg_1699" s="T338">Он говорит:</ta>
            <ta e="T343" id="Seg_1700" s="T340">«Я пришёл тебя испытать».</ta>
            <ta e="T346" id="Seg_1701" s="T343">А тот говорит:</ta>
            <ta e="T351" id="Seg_1702" s="T346">«Ну, я не могу / не хочу.</ta>
            <ta e="T354" id="Seg_1703" s="T351">Пойду принесу мешок.</ta>
            <ta e="T359" id="Seg_1704" s="T354">А сам большую гору схватил.</ta>
            <ta e="T362" id="Seg_1705" s="T359">«Держи эту гору!</ta>
            <ta e="T367" id="Seg_1706" s="T362">А то она упадёт, дом раздавит».</ta>
            <ta e="T370" id="Seg_1707" s="T367">Он сам пошёл в дом.</ta>
            <ta e="T377" id="Seg_1708" s="T370">А чёрт говорит: «Зачем [мне] её держать?»</ta>
            <ta e="T381" id="Seg_1709" s="T377">Взял и ушёл.</ta>
            <ta e="T385" id="Seg_1710" s="T381">Потом этот человек, он пришёл.</ta>
            <ta e="T390" id="Seg_1711" s="T385">«Ну, что тебе нужно?</ta>
            <ta e="T403" id="Seg_1712" s="T390">Ты испытать меня хотел, а теперь я тебя испытал».</ta>
            <ta e="T407" id="Seg_1713" s="T403">Потом он ушёл.</ta>
            <ta e="T412" id="Seg_1714" s="T407">Потом они пришли в деревню.</ta>
            <ta e="T415" id="Seg_1715" s="T412">Потом он говорит:</ta>
            <ta e="T419" id="Seg_1716" s="T415">«Пойдём на реку.</ta>
            <ta e="T430" id="Seg_1717" s="T419">[Когда] мы придём, тогда ты там воду не будешь пить.</ta>
            <ta e="T432" id="Seg_1718" s="T430">Вернёшься снова».</ta>
            <ta e="T435" id="Seg_1719" s="T432">Потом они пришли на реку.</ta>
            <ta e="T436" id="Seg_1720" s="T435">Смотрят.</ta>
            <ta e="T441" id="Seg_1721" s="T436">«Надо, чтобы кто-нибудь посмотрел (/сказал).</ta>
            <ta e="T445" id="Seg_1722" s="T441">Как ты будешь воду пить?»</ta>
            <ta e="T448" id="Seg_1723" s="T445">Потом они пошли.</ta>
            <ta e="T455" id="Seg_1724" s="T448">«А зачем кто-то, ты на реке был?»</ta>
            <ta e="T456" id="Seg_1725" s="T455">«Был».</ta>
            <ta e="T458" id="Seg_1726" s="T456">«Воду пил?»</ta>
            <ta e="T460" id="Seg_1727" s="T458">«Не пил».</ta>
            <ta e="T463" id="Seg_1728" s="T460">Ну и хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T338" id="Seg_1729" s="T334">One man had a row with a devil.</ta>
            <ta e="T340" id="Seg_1730" s="T338">He says:</ta>
            <ta e="T343" id="Seg_1731" s="T340">"I came to try you."</ta>
            <ta e="T346" id="Seg_1732" s="T343">And the other says:</ta>
            <ta e="T351" id="Seg_1733" s="T346">"Well, I cannot / I will not.</ta>
            <ta e="T354" id="Seg_1734" s="T351">I will go bring a sack.</ta>
            <ta e="T359" id="Seg_1735" s="T354">But he himself picks up a big mountain.</ta>
            <ta e="T362" id="Seg_1736" s="T359">"Hold the mountain!</ta>
            <ta e="T367" id="Seg_1737" s="T362">Or else it will fall down, crush the house."</ta>
            <ta e="T370" id="Seg_1738" s="T367">He himself came to his house.</ta>
            <ta e="T377" id="Seg_1739" s="T370">And the devil says: "What should I hold this for?"</ta>
            <ta e="T381" id="Seg_1740" s="T377">And he left.</ta>
            <ta e="T385" id="Seg_1741" s="T381">Then the man, he came.</ta>
            <ta e="T390" id="Seg_1742" s="T385">"Well, what do you need?</ta>
            <ta e="T403" id="Seg_1743" s="T390">You wanted to try me, but now I tried you."</ta>
            <ta e="T407" id="Seg_1744" s="T403">Then he left.</ta>
            <ta e="T412" id="Seg_1745" s="T407">Then they come to his village.</ta>
            <ta e="T415" id="Seg_1746" s="T412">Then he says:</ta>
            <ta e="T419" id="Seg_1747" s="T415">"Let's go to the river.</ta>
            <ta e="T430" id="Seg_1748" s="T419">[When] we arrive, then there you will not drink water.</ta>
            <ta e="T432" id="Seg_1749" s="T430">You will come back again."</ta>
            <ta e="T435" id="Seg_1750" s="T432">Then they came to the river.</ta>
            <ta e="T436" id="Seg_1751" s="T435">They are looking.</ta>
            <ta e="T441" id="Seg_1752" s="T436">"[We] need someone to look (/to tell).</ta>
            <ta e="T445" id="Seg_1753" s="T441">How will you drink water?"</ta>
            <ta e="T448" id="Seg_1754" s="T445">Then they went.</ta>
            <ta e="T455" id="Seg_1755" s="T448">"But what for a man, were you at the river?"</ta>
            <ta e="T456" id="Seg_1756" s="T455">"I was."</ta>
            <ta e="T458" id="Seg_1757" s="T456">"Did you drink water?"</ta>
            <ta e="T460" id="Seg_1758" s="T458">"I did not drink."</ta>
            <ta e="T463" id="Seg_1759" s="T460">And enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T338" id="Seg_1760" s="T334">Ein Mann hatte einen Streit mit dem Teufel.</ta>
            <ta e="T340" id="Seg_1761" s="T338">Er sagte:</ta>
            <ta e="T343" id="Seg_1762" s="T340">„Ich kam, um dich zu prüfen.“</ta>
            <ta e="T346" id="Seg_1763" s="T343">Und der Andere sagt:</ta>
            <ta e="T351" id="Seg_1764" s="T346">„Also, ich kann nicht / ich will nicht.</ta>
            <ta e="T354" id="Seg_1765" s="T351">Ich werde einen Sack holen gehen.“</ta>
            <ta e="T359" id="Seg_1766" s="T354">Aber er selbst hebt einen großen Berg auf.</ta>
            <ta e="T362" id="Seg_1767" s="T359">„Halte den Berg!</ta>
            <ta e="T367" id="Seg_1768" s="T362">Sonst wird er herunter fallen, das Haus zerdrücken.“</ta>
            <ta e="T370" id="Seg_1769" s="T367">Er selbst kam zu seinem Haus.</ta>
            <ta e="T377" id="Seg_1770" s="T370">Und der Teufel sagt: „Wozu soll ich dieses halten?“</ta>
            <ta e="T381" id="Seg_1771" s="T377">Und er ging.</ta>
            <ta e="T385" id="Seg_1772" s="T381">Dann der Mann, er kam.</ta>
            <ta e="T390" id="Seg_1773" s="T385">„Also, was brauchen wir?</ta>
            <ta e="T403" id="Seg_1774" s="T390">Du wolltest mich prüfen, aber nun habe ich dich geprüft.“</ta>
            <ta e="T407" id="Seg_1775" s="T403">Dann ging er.</ta>
            <ta e="T412" id="Seg_1776" s="T407">Dann kommen sie zu seinem Dorf.</ta>
            <ta e="T415" id="Seg_1777" s="T412">Dann sagt er:</ta>
            <ta e="T419" id="Seg_1778" s="T415">„Lass uns zum Fluss gehen.</ta>
            <ta e="T430" id="Seg_1779" s="T419">[Wenn] wir ankommen, dann dort wirst du kein Wasser trinken.</ta>
            <ta e="T432" id="Seg_1780" s="T430">Du wirst wieder zurück kommen.“</ta>
            <ta e="T435" id="Seg_1781" s="T432">Dann kamen sie zum Fluss.</ta>
            <ta e="T436" id="Seg_1782" s="T435">Sie schauen.</ta>
            <ta e="T441" id="Seg_1783" s="T436">„ [Wir] brauchen jemanden zu schauen (/ zu erzählen).</ta>
            <ta e="T445" id="Seg_1784" s="T441">Wie wirst du Wasser trinken?”</ta>
            <ta e="T448" id="Seg_1785" s="T445">Dann gingen sie.</ta>
            <ta e="T455" id="Seg_1786" s="T448">„Aber was für ein Mann, warst du am Fluss?“</ta>
            <ta e="T456" id="Seg_1787" s="T455">„War ich.“</ta>
            <ta e="T458" id="Seg_1788" s="T456">„Hast du Wasser getrunken?“</ta>
            <ta e="T460" id="Seg_1789" s="T458">„Ich habe nicht getrunken.“</ta>
            <ta e="T463" id="Seg_1790" s="T460">Und genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T338" id="Seg_1791" s="T334">[GVY:] eneidəneʔziʔ? </ta>
            <ta e="T381" id="Seg_1792" s="T377">[GVY:] "took and left" - Russ. "взял и ушел"</ta>
            <ta e="T430" id="Seg_1793" s="T419">[KlT:] Possibly PST form instead of rarer 1PL.FUT.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
