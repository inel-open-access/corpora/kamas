<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID31CD4FC1-B470-FA7C-D7CA-D74931409EC1">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FireBird_flk.wav" />
         <referenced-file url="PKZ_196X_FireBird_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FireBird_flk\PKZ_196X_FireBird_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1258</ud-information>
            <ud-information attribute-name="# HIAT:w">812</ud-information>
            <ud-information attribute-name="# e">805</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">183</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.8199917670261532" />
         <tli id="T1" time="1.57" type="appl" />
         <tli id="T2" time="2.501" type="appl" />
         <tli id="T3" time="3.432" type="appl" />
         <tli id="T4" time="4.282" type="appl" />
         <tli id="T5" time="5.132" type="appl" />
         <tli id="T6" time="5.983" type="appl" />
         <tli id="T7" time="8.226584069189212" />
         <tli id="T8" time="9.23" type="appl" />
         <tli id="T9" time="10.019" type="appl" />
         <tli id="T10" time="10.808" type="appl" />
         <tli id="T11" time="11.596" type="appl" />
         <tli id="T12" time="12.384" type="appl" />
         <tli id="T13" time="13.359865862767569" />
         <tli id="T14" time="14.025" type="appl" />
         <tli id="T15" time="14.788" type="appl" />
         <tli id="T16" time="16.186504149101623" />
         <tli id="T17" time="16.874" type="appl" />
         <tli id="T18" time="17.562" type="appl" />
         <tli id="T19" time="18.251" type="appl" />
         <tli id="T20" time="18.939" type="appl" />
         <tli id="T21" time="19.81980100299799" />
         <tli id="T22" time="21.151" type="appl" />
         <tli id="T23" time="22.413" type="appl" />
         <tli id="T24" time="23.675" type="appl" />
         <tli id="T25" time="24.452" type="appl" />
         <tli id="T26" time="25.228" type="appl" />
         <tli id="T27" time="26.005" type="appl" />
         <tli id="T28" time="26.533" type="appl" />
         <tli id="T29" time="27.058" type="appl" />
         <tli id="T30" time="27.584" type="appl" />
         <tli id="T31" time="28.109" type="appl" />
         <tli id="T32" time="28.693045246183438" />
         <tli id="T33" time="29.239" type="appl" />
         <tli id="T34" time="29.843" type="appl" />
         <tli id="T35" time="30.284" type="appl" />
         <tli id="T36" time="30.94635595557238" />
         <tli id="T37" time="32.156" type="appl" />
         <tli id="T38" time="32.627" type="appl" />
         <tli id="T39" time="33.098" type="appl" />
         <tli id="T40" time="33.646328846999964" />
         <tli id="T41" time="34.306" type="appl" />
         <tli id="T42" time="34.955" type="appl" />
         <tli id="T43" time="35.605" type="appl" />
         <tli id="T44" time="36.254" type="appl" />
         <tli id="T45" time="36.904" type="appl" />
         <tli id="T46" time="37.804" type="appl" />
         <tli id="T47" time="38.683999621376195" />
         <tli id="T48" time="39.116" type="appl" />
         <tli id="T49" time="39.527" type="appl" />
         <tli id="T50" time="39.939" type="appl" />
         <tli id="T51" time="41.193" type="appl" />
         <tli id="T52" time="42.446" type="appl" />
         <tli id="T53" time="43.7" type="appl" />
         <tli id="T54" time="44.454" type="appl" />
         <tli id="T55" time="45.208" type="appl" />
         <tli id="T56" time="45.811" type="appl" />
         <tli id="T57" time="46.414" type="appl" />
         <tli id="T58" time="47.017" type="appl" />
         <tli id="T59" time="47.62" type="appl" />
         <tli id="T60" time="48.224" type="appl" />
         <tli id="T61" time="48.827" type="appl" />
         <tli id="T62" time="49.43" type="appl" />
         <tli id="T63" time="50.033" type="appl" />
         <tli id="T64" time="50.636" type="appl" />
         <tli id="T65" time="51.239" type="appl" />
         <tli id="T66" time="52.549" type="appl" />
         <tli id="T67" time="53.858" type="appl" />
         <tli id="T68" time="55.168" type="appl" />
         <tli id="T69" time="56.478" type="appl" />
         <tli id="T70" time="57.787" type="appl" />
         <tli id="T71" time="59.097" type="appl" />
         <tli id="T72" time="60.067" type="appl" />
         <tli id="T73" time="61.038" type="appl" />
         <tli id="T74" time="62.49937248674947" />
         <tli id="T75" time="63.237" type="appl" />
         <tli id="T76" time="63.923" type="appl" />
         <tli id="T77" time="64.99268078648754" />
         <tli id="T78" time="65.652" type="appl" />
         <tli id="T79" time="66.455" type="appl" />
         <tli id="T80" time="67.22" type="appl" />
         <tli id="T81" time="67.986" type="appl" />
         <tli id="T82" time="70.2259615760447" />
         <tli id="T83" time="71.655" type="appl" />
         <tli id="T84" time="72.916" type="appl" />
         <tli id="T85" time="74.177" type="appl" />
         <tli id="T86" time="75.6059075597041" />
         <tli id="T87" time="76.277" type="appl" />
         <tli id="T88" time="76.964" type="appl" />
         <tli id="T89" time="77.65" type="appl" />
         <tli id="T90" time="78.225" type="appl" />
         <tli id="T91" time="78.795" type="appl" />
         <tli id="T92" time="79.74586599322639" />
         <tli id="T93" time="80.518" type="appl" />
         <tli id="T94" time="81.315" type="appl" />
         <tli id="T95" time="82.111" type="appl" />
         <tli id="T96" time="82.908" type="appl" />
         <tli id="T97" time="83.81915842942946" />
         <tli id="T98" time="84.4" type="appl" />
         <tli id="T99" time="85.096" type="appl" />
         <tli id="T100" time="85.791" type="appl" />
         <tli id="T101" time="87.02579290048295" />
         <tli id="T102" time="87.773" type="appl" />
         <tli id="T103" time="88.453" type="appl" />
         <tli id="T104" time="89.133" type="appl" />
         <tli id="T105" time="89.812" type="appl" />
         <tli id="T106" time="90.492" type="appl" />
         <tli id="T107" time="91.172" type="appl" />
         <tli id="T108" time="91.852" type="appl" />
         <tli id="T109" time="92.652" type="appl" />
         <tli id="T110" time="93.307" type="appl" />
         <tli id="T111" time="93.962" type="appl" />
         <tli id="T112" time="95.03904577825072" />
         <tli id="T113" time="96.332" type="appl" />
         <tli id="T114" time="97.47" type="appl" />
         <tli id="T115" time="98.608" type="appl" />
         <tli id="T116" time="99.745" type="appl" />
         <tli id="T117" time="100.883" type="appl" />
         <tli id="T118" time="102.19230729060084" />
         <tli id="T119" time="102.748" type="appl" />
         <tli id="T120" time="103.476" type="appl" />
         <tli id="T121" time="104.87228038283264" />
         <tli id="T122" time="105.749" type="appl" />
         <tli id="T123" time="106.564" type="appl" />
         <tli id="T124" time="107.38" type="appl" />
         <tli id="T125" time="108.195" type="appl" />
         <tli id="T126" time="109.01" type="appl" />
         <tli id="T127" time="109.825" type="appl" />
         <tli id="T128" time="110.64" type="appl" />
         <tli id="T129" time="111.456" type="appl" />
         <tli id="T130" time="112.271" type="appl" />
         <tli id="T131" time="113.086" type="appl" />
         <tli id="T132" time="113.901" type="appl" />
         <tli id="T133" time="114.716" type="appl" />
         <tli id="T134" time="115.532" type="appl" />
         <tli id="T135" time="116.347" type="appl" />
         <tli id="T136" time="117.79215066329348" />
         <tli id="T137" time="118.678" type="appl" />
         <tli id="T138" time="119.82546358152906" />
         <tli id="T139" time="120.747" type="appl" />
         <tli id="T140" time="121.681" type="appl" />
         <tli id="T141" time="122.615" type="appl" />
         <tli id="T142" time="123.548" type="appl" />
         <tli id="T143" time="124.482" type="appl" />
         <tli id="T144" time="125.416" type="appl" />
         <tli id="T145" time="126.53206291184054" />
         <tli id="T146" time="127.242" type="appl" />
         <tli id="T147" time="127.912" type="appl" />
         <tli id="T148" time="128.95203861452748" />
         <tli id="T149" time="133.10533024751362" />
         <tli id="T150" time="134.128" type="appl" />
         <tli id="T151" time="134.88" type="appl" />
         <tli id="T152" time="135.632" type="appl" />
         <tli id="T153" time="136.78529329953344" />
         <tli id="T154" time="137.577" type="appl" />
         <tli id="T155" time="139.71193058184627" />
         <tli id="T156" time="141.004" type="appl" />
         <tli id="T157" time="142.33190427649083" />
         <tli id="T158" time="143.506" type="appl" />
         <tli id="T159" time="144.624" type="appl" />
         <tli id="T160" time="145.741" type="appl" />
         <tli id="T161" time="147.52518546765646" />
         <tli id="T162" time="148.569" type="appl" />
         <tli id="T163" time="149.46" type="appl" />
         <tli id="T164" time="150.444" type="appl" />
         <tli id="T165" time="152.33180387437073" />
         <tli id="T166" time="153.279" type="appl" />
         <tli id="T167" time="154.077" type="appl" />
         <tli id="T168" time="154.762" type="appl" />
         <tli id="T169" time="155.55177154488806" />
         <tli id="T170" time="156.194" type="appl" />
         <tli id="T171" time="156.823" type="appl" />
         <tli id="T172" time="157.453" type="appl" />
         <tli id="T173" time="158.4" type="appl" />
         <tli id="T174" time="159.53839818457618" />
         <tli id="T175" time="160.504" type="appl" />
         <tli id="T176" time="161.397" type="appl" />
         <tli id="T177" time="162.74503265562967" />
         <tli id="T178" time="163.595" type="appl" />
         <tli id="T179" time="164.27" type="appl" />
         <tli id="T180" time="164.945" type="appl" />
         <tli id="T181" time="165.62" type="appl" />
         <tli id="T182" time="166.295" type="appl" />
         <tli id="T183" time="166.97" type="appl" />
         <tli id="T184" time="167.767" type="appl" />
         <tli id="T185" time="168.564" type="appl" />
         <tli id="T186" time="169.361" type="appl" />
         <tli id="T187" time="170.158" type="appl" />
         <tli id="T188" time="170.954" type="appl" />
         <tli id="T189" time="171.751" type="appl" />
         <tli id="T190" time="172.548" type="appl" />
         <tli id="T191" time="173.62492341812302" />
         <tli id="T192" time="174.704" type="appl" />
         <tli id="T193" time="175.552" type="appl" />
         <tli id="T194" time="176.38666652269168" />
         <tli id="T195" time="177.393" type="appl" />
         <tli id="T196" time="178.386" type="appl" />
         <tli id="T197" time="179.378" type="appl" />
         <tli id="T198" time="180.371" type="appl" />
         <tli id="T199" time="181.218" type="appl" />
         <tli id="T200" time="181.882" type="appl" />
         <tli id="T201" time="182.546" type="appl" />
         <tli id="T202" time="183.209" type="appl" />
         <tli id="T203" time="183.583" type="appl" />
         <tli id="T204" time="183.957" type="appl" />
         <tli id="T205" time="184.331" type="appl" />
         <tli id="T206" time="186.0114657200303" />
         <tli id="T207" time="186.691" type="appl" />
         <tli id="T208" time="187.167" type="appl" />
         <tli id="T209" time="187.643" type="appl" />
         <tli id="T210" time="188.119" type="appl" />
         <tli id="T211" time="188.595" type="appl" />
         <tli id="T212" time="189.071" type="appl" />
         <tli id="T213" time="189.734" type="appl" />
         <tli id="T214" time="190.396" type="appl" />
         <tli id="T215" time="191.058" type="appl" />
         <tli id="T216" time="191.721" type="appl" />
         <tli id="T217" time="192.384" type="appl" />
         <tli id="T218" time="193.046" type="appl" />
         <tli id="T219" time="193.708" type="appl" />
         <tli id="T220" time="194.371" type="appl" />
         <tli id="T221" time="195.034" type="appl" />
         <tli id="T222" time="196.49802709834037" />
         <tli id="T223" time="197.331" type="appl" />
         <tli id="T224" time="197.938" type="appl" />
         <tli id="T225" time="198.536" type="appl" />
         <tli id="T226" time="199.134" type="appl" />
         <tli id="T227" time="199.731" type="appl" />
         <tli id="T228" time="200.329" type="appl" />
         <tli id="T229" time="200.927" type="appl" />
         <tli id="T230" time="201.525" type="appl" />
         <tli id="T231" time="202.123" type="appl" />
         <tli id="T232" time="202.721" type="appl" />
         <tli id="T233" time="203.318" type="appl" />
         <tli id="T234" time="203.916" type="appl" />
         <tli id="T235" time="204.514" type="appl" />
         <tli id="T236" time="205.112" type="appl" />
         <tli id="T237" time="206.192" type="appl" />
         <tli id="T238" time="207.116" type="appl" />
         <tli id="T239" time="208.041" type="appl" />
         <tli id="T240" time="208.674" type="appl" />
         <tli id="T241" time="209.308" type="appl" />
         <tli id="T242" time="209.9410041058127" />
         <tli id="T243" time="210.521" type="appl" />
         <tli id="T244" time="211.102" type="appl" />
         <tli id="T245" time="211.682" type="appl" />
         <tli id="T246" time="212.262" type="appl" />
         <tli id="T247" time="213.373" type="appl" />
         <tli id="T248" time="214.039" type="appl" />
         <tli id="T249" time="214.706" type="appl" />
         <tli id="T250" time="215.372" type="appl" />
         <tli id="T251" time="216.28449510267876" />
         <tli id="T252" time="216.789" type="appl" />
         <tli id="T253" time="217.345" type="appl" />
         <tli id="T254" time="218.29780822171858" />
         <tli id="T255" time="218.914" type="appl" />
         <tli id="T256" time="219.493" type="appl" />
         <tli id="T257" time="220.073" type="appl" />
         <tli id="T258" time="220.653" type="appl" />
         <tli id="T259" time="221.232" type="appl" />
         <tli id="T260" time="221.812" type="appl" />
         <tli id="T261" time="222.619" type="appl" />
         <tli id="T262" time="223.425" type="appl" />
         <tli id="T263" time="224.232" type="appl" />
         <tli id="T264" time="225.0" type="appl" />
         <tli id="T265" time="225.769" type="appl" />
         <tli id="T266" time="227.374" type="appl" />
         <tli id="T267" time="228.271" type="appl" />
         <tli id="T268" time="229.086" type="appl" />
         <tli id="T269" time="229.9" type="appl" />
         <tli id="T270" time="230.714" type="appl" />
         <tli id="T271" time="231.422" type="appl" />
         <tli id="T272" time="232.131" type="appl" />
         <tli id="T273" time="232.839" type="appl" />
         <tli id="T274" time="233.547" type="appl" />
         <tli id="T275" time="234.256" type="appl" />
         <tli id="T276" time="234.964" type="appl" />
         <tli id="T277" time="235.724" type="appl" />
         <tli id="T278" time="236.99762046975403" />
         <tli id="T279" time="238.008" type="appl" />
         <tli id="T280" time="238.948" type="appl" />
         <tli id="T281" time="239.888" type="appl" />
         <tli id="T282" time="240.828" type="appl" />
         <tli id="T283" time="241.768" type="appl" />
         <tli id="T284" time="244.87754135288338" />
         <tli id="T285" time="245.835" type="appl" />
         <tli id="T286" time="246.356" type="appl" />
         <tli id="T287" time="246.876" type="appl" />
         <tli id="T288" time="247.93084403010272" />
         <tli id="T289" time="248.802" type="appl" />
         <tli id="T290" time="249.659" type="appl" />
         <tli id="T291" time="250.517" type="appl" />
         <tli id="T292" time="251.375" type="appl" />
         <tli id="T293" time="252.232" type="appl" />
         <tli id="T294" time="253.09" type="appl" />
         <tli id="T295" time="253.587" type="appl" />
         <tli id="T296" time="254.084" type="appl" />
         <tli id="T297" time="254.79744175398028" />
         <tli id="T298" time="255.706" type="appl" />
         <tli id="T299" time="256.499" type="appl" />
         <tli id="T300" time="257.082" type="appl" />
         <tli id="T301" time="257.666" type="appl" />
         <tli id="T302" time="258.249" type="appl" />
         <tli id="T303" time="259.67072615801374" />
         <tli id="T304" time="260.249" type="appl" />
         <tli id="T305" time="260.756" type="appl" />
         <tli id="T306" time="261.262" type="appl" />
         <tli id="T307" time="261.843" type="appl" />
         <tli id="T308" time="262.425" type="appl" />
         <tli id="T309" time="263.006" type="appl" />
         <tli id="T310" time="263.588" type="appl" />
         <tli id="T311" time="264.169" type="appl" />
         <tli id="T312" time="264.751" type="appl" />
         <tli id="T313" time="265.332" type="appl" />
         <tli id="T314" time="265.735" type="appl" />
         <tli id="T315" time="266.137" type="appl" />
         <tli id="T316" time="266.54" type="appl" />
         <tli id="T317" time="266.943" type="appl" />
         <tli id="T318" time="267.345" type="appl" />
         <tli id="T319" time="267.748" type="appl" />
         <tli id="T320" time="268.151" type="appl" />
         <tli id="T321" time="268.554" type="appl" />
         <tli id="T322" time="268.956" type="appl" />
         <tli id="T323" time="269.359" type="appl" />
         <tli id="T324" time="269.762" type="appl" />
         <tli id="T325" time="270.164" type="appl" />
         <tli id="T326" time="270.9906125028138" />
         <tli id="T327" time="272.007" type="appl" />
         <tli id="T328" time="272.799" type="appl" />
         <tli id="T329" time="273.591" type="appl" />
         <tli id="T330" time="274.383" type="appl" />
         <tli id="T331" time="275.175" type="appl" />
         <tli id="T332" time="275.968" type="appl" />
         <tli id="T333" time="276.76" type="appl" />
         <tli id="T334" time="277.552" type="appl" />
         <tli id="T335" time="278.344" type="appl" />
         <tli id="T336" time="279.136" type="appl" />
         <tli id="T337" time="279.928" type="appl" />
         <tli id="T338" time="280.799" type="appl" />
         <tli id="T339" time="281.669" type="appl" />
         <tli id="T340" time="282.54" type="appl" />
         <tli id="T341" time="283.411" type="appl" />
         <tli id="T342" time="283.966" type="appl" />
         <tli id="T343" time="284.521" type="appl" />
         <tli id="T344" time="285.076" type="appl" />
         <tli id="T345" time="285.63" type="appl" />
         <tli id="T346" time="286.185" type="appl" />
         <tli id="T347" time="286.74" type="appl" />
         <tli id="T348" time="287.295" type="appl" />
         <tli id="T349" time="288.176" type="appl" />
         <tli id="T350" time="288.919" type="appl" />
         <tli id="T351" time="289.663" type="appl" />
         <tli id="T352" time="290.407" type="appl" />
         <tli id="T353" time="291.15" type="appl" />
         <tli id="T354" time="291.894" type="appl" />
         <tli id="T355" time="292.638" type="appl" />
         <tli id="T356" time="293.381" type="appl" />
         <tli id="T357" time="294.125" type="appl" />
         <tli id="T358" time="295.123" type="appl" />
         <tli id="T359" time="296.05" type="appl" />
         <tli id="T360" time="296.854" type="appl" />
         <tli id="T361" time="298.2570054063664" />
         <tli id="T362" time="299.0" type="appl" />
         <tli id="T363" time="299.642" type="appl" />
         <tli id="T364" time="300.8103131036917" />
         <tli id="T365" time="300.995" type="appl" />
         <tli id="T366" time="301.569" type="appl" />
         <tli id="T367" time="302.142" type="appl" />
         <tli id="T368" time="302.715" type="appl" />
         <tli id="T369" time="303.289" type="appl" />
         <tli id="T370" time="303.862" type="appl" />
         <tli id="T371" time="304.435" type="appl" />
         <tli id="T372" time="305.008" type="appl" />
         <tli id="T373" time="305.582" type="appl" />
         <tli id="T374" time="306.155" type="appl" />
         <tli id="T375" time="306.728" type="appl" />
         <tli id="T376" time="307.302" type="appl" />
         <tli id="T377" time="308.116906409876" />
         <tli id="T378" time="308.761" type="appl" />
         <tli id="T379" time="309.266" type="appl" />
         <tli id="T380" time="309.772" type="appl" />
         <tli id="T381" time="310.41021671765645" />
         <tli id="T382" time="311.167" type="appl" />
         <tli id="T383" time="311.909" type="appl" />
         <tli id="T384" time="312.665" type="appl" />
         <tli id="T385" time="314.07684657021247" />
         <tli id="T386" time="314.997" type="appl" />
         <tli id="T387" time="315.628" type="appl" />
         <tli id="T388" time="316.258" type="appl" />
         <tli id="T389" time="316.888" type="appl" />
         <tli id="T390" time="317.842" type="appl" />
         <tli id="T391" time="318.684" type="appl" />
         <tli id="T392" time="319.527" type="appl" />
         <tli id="T393" time="320.369" type="appl" />
         <tli id="T394" time="321.68343686433303" />
         <tli id="T395" time="322.477" type="appl" />
         <tli id="T396" time="323.127" type="appl" />
         <tli id="T397" time="323.757" type="appl" />
         <tli id="T398" time="324.386" type="appl" />
         <tli id="T399" time="325.016" type="appl" />
         <tli id="T400" time="325.725" type="appl" />
         <tli id="T401" time="326.433" type="appl" />
         <tli id="T402" time="327.142" type="appl" />
         <tli id="T403" time="327.85" type="appl" />
         <tli id="T404" time="328.721" type="appl" />
         <tli id="T405" time="329.593" type="appl" />
         <tli id="T406" time="331.31667347695736" />
         <tli id="T407" time="332.202" type="appl" />
         <tli id="T408" time="332.948" type="appl" />
         <tli id="T409" time="333.694" type="appl" />
         <tli id="T410" time="334.439" type="appl" />
         <tli id="T411" time="335.201" type="appl" />
         <tli id="T412" time="335.837" type="appl" />
         <tli id="T413" time="336.631" type="appl" />
         <tli id="T414" time="337.425" type="appl" />
         <tli id="T415" time="338.219" type="appl" />
         <tli id="T416" time="338.736" type="appl" />
         <tli id="T417" time="339.252" type="appl" />
         <tli id="T418" time="339.768" type="appl" />
         <tli id="T419" time="340.285" type="appl" />
         <tli id="T420" time="340.932" type="appl" />
         <tli id="T421" time="341.385" type="appl" />
         <tli id="T422" time="341.839" type="appl" />
         <tli id="T423" time="342.292" type="appl" />
         <tli id="T424" time="342.746" type="appl" />
         <tli id="T425" time="343.199" type="appl" />
         <tli id="T426" time="344.176544359831" />
         <tli id="T427" time="345.082" type="appl" />
         <tli id="T428" time="345.834" type="appl" />
         <tli id="T429" time="346.586" type="appl" />
         <tli id="T430" time="347.338" type="appl" />
         <tli id="T431" time="348.09" type="appl" />
         <tli id="T432" time="348.537" type="appl" />
         <tli id="T433" time="348.985" type="appl" />
         <tli id="T434" time="350.1031515215078" />
         <tli id="T435" time="350.857" type="appl" />
         <tli id="T436" time="351.453" type="appl" />
         <tli id="T437" time="352.05" type="appl" />
         <tli id="T438" time="352.647" type="appl" />
         <tli id="T439" time="353.183" type="appl" />
         <tli id="T440" time="353.719" type="appl" />
         <tli id="T441" time="354.255" type="appl" />
         <tli id="T442" time="354.791" type="appl" />
         <tli id="T443" time="355.328" type="appl" />
         <tli id="T444" time="355.864" type="appl" />
         <tli id="T445" time="356.4" type="appl" />
         <tli id="T446" time="356.936" type="appl" />
         <tli id="T447" time="358.08307140061595" />
         <tli id="T448" time="359.141" type="appl" />
         <tli id="T449" time="359.926" type="appl" />
         <tli id="T450" time="360.712" type="appl" />
         <tli id="T451" time="361.247" type="appl" />
         <tli id="T452" time="361.781" type="appl" />
         <tli id="T453" time="362.316" type="appl" />
         <tli id="T454" time="362.85" type="appl" />
         <tli id="T455" time="363.385" type="appl" />
         <tli id="T456" time="364.579" type="appl" />
         <tli id="T457" time="365.764" type="appl" />
         <tli id="T458" time="367.073" type="appl" />
         <tli id="T459" time="368.166" type="appl" />
         <tli id="T460" time="369.258" type="appl" />
         <tli id="T461" time="370.351" type="appl" />
         <tli id="T462" time="370.902" type="appl" />
         <tli id="T463" time="371.453" type="appl" />
         <tli id="T464" time="372.005" type="appl" />
         <tli id="T465" time="372.556" type="appl" />
         <tli id="T466" time="373.107" type="appl" />
         <tli id="T467" time="373.536" type="appl" />
         <tli id="T468" time="373.965" type="appl" />
         <tli id="T469" time="374.394" type="appl" />
         <tli id="T470" time="375.47623010119514" />
         <tli id="T471" time="376.756" type="appl" />
         <tli id="T472" time="377.929" type="appl" />
         <tli id="T473" time="379.101" type="appl" />
         <tli id="T474" time="380.206" type="appl" />
         <tli id="T475" time="381.31" type="appl" />
         <tli id="T476" time="382.762" type="appl" />
         <tli id="T477" time="384.194" type="appl" />
         <tli id="T478" time="385.626" type="appl" />
         <tli id="T479" time="387.78943980605123" />
         <tli id="T480" time="388.759" type="appl" />
         <tli id="T481" time="389.557" type="appl" />
         <tli id="T482" time="390.356" type="appl" />
         <tli id="T483" time="390.996" type="appl" />
         <tli id="T484" time="391.635" type="appl" />
         <tli id="T485" time="392.275" type="appl" />
         <tli id="T486" time="392.915" type="appl" />
         <tli id="T487" time="393.554" type="appl" />
         <tli id="T488" time="394.194" type="appl" />
         <tli id="T489" time="394.834" type="appl" />
         <tli id="T490" time="395.474" type="appl" />
         <tli id="T491" time="396.113" type="appl" />
         <tli id="T492" time="396.969347636905" />
         <tli id="T493" time="398.133" type="appl" />
         <tli id="T494" time="399.195" type="appl" />
         <tli id="T495" time="399.892" type="appl" />
         <tli id="T496" time="400.589" type="appl" />
         <tli id="T497" time="401.286" type="appl" />
         <tli id="T498" time="401.982" type="appl" />
         <tli id="T499" time="402.679" type="appl" />
         <tli id="T500" time="403.376" type="appl" />
         <tli id="T501" time="406.42925265649944" />
         <tli id="T502" time="407.379" type="appl" />
         <tli id="T503" time="408.173" type="appl" />
         <tli id="T504" time="408.967" type="appl" />
         <tli id="T505" time="409.761" type="appl" />
         <tli id="T506" time="410.556" type="appl" />
         <tli id="T507" time="411.35" type="appl" />
         <tli id="T508" time="412.144" type="appl" />
         <tli id="T509" time="412.938" type="appl" />
         <tli id="T510" time="413.542" type="appl" />
         <tli id="T511" time="414.146" type="appl" />
         <tli id="T512" time="415.55582768949785" />
         <tli id="T513" time="416.362" type="appl" />
         <tli id="T514" time="416.996" type="appl" />
         <tli id="T515" time="417.82247159835055" />
         <tli id="T516" time="418.296" type="appl" />
         <tli id="T517" time="418.81" type="appl" />
         <tli id="T518" time="419.323" type="appl" />
         <tli id="T519" time="420.1691147039864" />
         <tli id="T520" time="420.779" type="appl" />
         <tli id="T521" time="421.35" type="appl" />
         <tli id="T522" time="421.921" type="appl" />
         <tli id="T523" time="422.492" type="appl" />
         <tli id="T524" time="423.212" type="appl" />
         <tli id="T525" time="423.932" type="appl" />
         <tli id="T526" time="424.653" type="appl" />
         <tli id="T527" time="425.373" type="appl" />
         <tli id="T528" time="426.214" type="appl" />
         <tli id="T529" time="427.019" type="appl" />
         <tli id="T530" time="427.825" type="appl" />
         <tli id="T531" time="428.63" type="appl" />
         <tli id="T532" time="429.436" type="appl" />
         <tli id="T533" time="430.241" type="appl" />
         <tli id="T534" time="431.047" type="appl" />
         <tli id="T535" time="431.852" type="appl" />
         <tli id="T536" time="432.658" type="appl" />
         <tli id="T537" time="433.478" type="appl" />
         <tli id="T538" time="434.297" type="appl" />
         <tli id="T539" time="435.116" type="appl" />
         <tli id="T540" time="435.936" type="appl" />
         <tli id="T541" time="436.425" type="appl" />
         <tli id="T542" time="436.914" type="appl" />
         <tli id="T543" time="437.404" type="appl" />
         <tli id="T544" time="437.893" type="appl" />
         <tli id="T545" time="438.53" type="appl" />
         <tli id="T546" time="439.167" type="appl" />
         <tli id="T547" time="439.803" type="appl" />
         <tli id="T548" time="440.44" type="appl" />
         <tli id="T549" time="442.03556182468384" />
         <tli id="T550" time="443.107" type="appl" />
         <tli id="T551" time="443.96" type="appl" />
         <tli id="T552" time="444.814" type="appl" />
         <tli id="T553" time="445.667" type="appl" />
         <tli id="T554" time="446.52" type="appl" />
         <tli id="T555" time="447.373" type="appl" />
         <tli id="T556" time="447.939" type="appl" />
         <tli id="T557" time="448.505" type="appl" />
         <tli id="T558" time="449.3554883303319" />
         <tli id="T559" time="450.112" type="appl" />
         <tli id="T560" time="451.084" type="appl" />
         <tli id="T561" time="452.057" type="appl" />
         <tli id="T562" time="453.03" type="appl" />
         <tli id="T563" time="454.9087659070212" />
         <tli id="T564" time="455.747" type="appl" />
         <tli id="T565" time="456.534" type="appl" />
         <tli id="T566" time="457.322" type="appl" />
         <tli id="T567" time="458.109" type="appl" />
         <tli id="T568" time="460.0220479014039" />
         <tli id="T569" time="460.92" type="appl" />
         <tli id="T570" time="461.601" type="appl" />
         <tli id="T571" time="462.282" type="appl" />
         <tli id="T572" time="462.964" type="appl" />
         <tli id="T573" time="463.645" type="appl" />
         <tli id="T574" time="464.326" type="appl" />
         <tli id="T575" time="465.53532587970165" />
         <tli id="T576" time="466.244" type="appl" />
         <tli id="T577" time="466.78" type="appl" />
         <tli id="T578" time="467.315" type="appl" />
         <tli id="T579" time="467.851" type="appl" />
         <tli id="T580" time="468.3393081435092" />
         <tli id="T581" time="468.848" type="appl" />
         <tli id="T582" time="469.309" type="appl" />
         <tli id="T583" time="470.2286120909733" />
         <tli id="T584" time="471.909" type="appl" />
         <tli id="T585" time="473.224" type="appl" />
         <tli id="T586" time="474.539" type="appl" />
         <tli id="T587" time="476.58188163549295" />
         <tli id="T588" time="477.761" type="appl" />
         <tli id="T589" time="479.036" type="appl" />
         <tli id="T590" time="480.312" type="appl" />
         <tli id="T591" time="481.94849441968853" />
         <tli id="T592" time="482.496" type="appl" />
         <tli id="T593" time="483.24" type="appl" />
         <tli id="T594" time="483.984" type="appl" />
         <tli id="T595" time="484.729" type="appl" />
         <tli id="T596" time="485.473" type="appl" />
         <tli id="T597" time="486.73511302720703" />
         <tli id="T598" time="487.465" type="appl" />
         <tli id="T599" time="488.059" type="appl" />
         <tli id="T600" time="488.653" type="appl" />
         <tli id="T601" time="489.247" type="appl" />
         <tli id="T602" time="489.841" type="appl" />
         <tli id="T603" time="490.435" type="appl" />
         <tli id="T604" time="491.029" type="appl" />
         <tli id="T605" time="491.682" type="appl" />
         <tli id="T606" time="492.334" type="appl" />
         <tli id="T607" time="492.987" type="appl" />
         <tli id="T608" time="493.64" type="appl" />
         <tli id="T609" time="494.293" type="appl" />
         <tli id="T610" time="494.945" type="appl" />
         <tli id="T611" time="496.0083532543077" />
         <tli id="T612" time="496.833" type="appl" />
         <tli id="T613" time="497.534" type="appl" />
         <tli id="T614" time="498.234" type="appl" />
         <tli id="T615" time="498.935" type="appl" />
         <tli id="T616" time="499.636" type="appl" />
         <tli id="T617" time="500.7016394655793" />
         <tli id="T618" time="501.387" type="appl" />
         <tli id="T619" time="502.047" type="appl" />
         <tli id="T620" time="502.707" type="appl" />
         <tli id="T621" time="503.7482755430667" />
         <tli id="T622" time="504.578" type="appl" />
         <tli id="T623" time="505.35" type="appl" />
         <tli id="T624" time="506.123" type="appl" />
         <tli id="T625" time="506.895" type="appl" />
         <tli id="T626" time="507.668" type="appl" />
         <tli id="T627" time="508.44" type="appl" />
         <tli id="T628" time="509.213" type="appl" />
         <tli id="T629" time="509.956" type="appl" />
         <tli id="T630" time="510.699" type="appl" />
         <tli id="T631" time="511.442" type="appl" />
         <tli id="T632" time="512.185" type="appl" />
         <tli id="T633" time="512.928" type="appl" />
         <tli id="T634" time="513.671" type="appl" />
         <tli id="T635" time="514.414" type="appl" />
         <tli id="T636" time="515.05" type="appl" />
         <tli id="T637" time="515.687" type="appl" />
         <tli id="T638" time="516.323" type="appl" />
         <tli id="T639" time="516.96" type="appl" />
         <tli id="T640" time="517.596" type="appl" />
         <tli id="T641" time="518.144" type="appl" />
         <tli id="T642" time="518.692" type="appl" />
         <tli id="T643" time="519.24" type="appl" />
         <tli id="T644" time="519.789" type="appl" />
         <tli id="T645" time="520.337" type="appl" />
         <tli id="T646" time="520.885" type="appl" />
         <tli id="T647" time="521.433" type="appl" />
         <tli id="T648" time="522.349" type="appl" />
         <tli id="T649" time="523.159" type="appl" />
         <tli id="T650" time="524.4880673090697" />
         <tli id="T651" time="525.419" type="appl" />
         <tli id="T652" time="526.056" type="appl" />
         <tli id="T653" time="526.694" type="appl" />
         <tli id="T654" time="527.331" type="appl" />
         <tli id="T655" time="527.968" type="appl" />
         <tli id="T656" time="528.606" type="appl" />
         <tli id="T657" time="529.243" type="appl" />
         <tli id="T658" time="529.88" type="appl" />
         <tli id="T659" time="530.828" type="appl" />
         <tli id="T660" time="531.775" type="appl" />
         <tli id="T661" time="532.723" type="appl" />
         <tli id="T662" time="533.67" type="appl" />
         <tli id="T663" time="534.618" type="appl" />
         <tli id="T664" time="535.565" type="appl" />
         <tli id="T665" time="538.5545927434208" />
         <tli id="T666" time="539.32" type="appl" />
         <tli id="T667" time="539.962" type="appl" />
         <tli id="T668" time="540.604" type="appl" />
         <tli id="T669" time="541.246" type="appl" />
         <tli id="T670" time="541.888" type="appl" />
         <tli id="T671" time="542.53" type="appl" />
         <tli id="T672" time="543.3145449520117" />
         <tli id="T673" time="544.046" type="appl" />
         <tli id="T674" time="544.757" type="appl" />
         <tli id="T675" time="545.468" type="appl" />
         <tli id="T676" time="546.59" type="appl" />
         <tli id="T677" time="547.713" type="appl" />
         <tli id="T678" time="548.499" type="appl" />
         <tli id="T679" time="549.286" type="appl" />
         <tli id="T680" time="550.072" type="appl" />
         <tli id="T681" time="550.9344684455962" />
         <tli id="T682" time="551.922" type="appl" />
         <tli id="T683" time="552.794" type="appl" />
         <tli id="T684" time="554.187769114773" />
         <tli id="T685" time="555.502" type="appl" />
         <tli id="T686" time="556.389" type="appl" />
         <tli id="T687" time="557.12" type="appl" />
         <tli id="T688" time="557.652" type="appl" />
         <tli id="T689" time="558.184" type="appl" />
         <tli id="T690" time="558.715" type="appl" />
         <tli id="T691" time="559.173" type="appl" />
         <tli id="T692" time="559.631" type="appl" />
         <tli id="T693" time="560.089" type="appl" />
         <tli id="T694" time="560.547" type="appl" />
         <tli id="T695" time="561.8076926083576" />
         <tli id="T696" time="563.623" type="appl" />
         <tli id="T697" time="564.226" type="appl" />
         <tli id="T698" time="564.828" type="appl" />
         <tli id="T699" time="565.431" type="appl" />
         <tli id="T700" time="566.034" type="appl" />
         <tli id="T701" time="566.637" type="appl" />
         <tli id="T702" time="567.239" type="appl" />
         <tli id="T703" time="567.842" type="appl" />
         <tli id="T704" time="569.013" type="appl" />
         <tli id="T705" time="569.857" type="appl" />
         <tli id="T706" time="570.7" type="appl" />
         <tli id="T707" time="571.544" type="appl" />
         <tli id="T708" time="572.387" type="appl" />
         <tli id="T709" time="573.195" type="appl" />
         <tli id="T710" time="574.004" type="appl" />
         <tli id="T711" time="574.812" type="appl" />
         <tli id="T712" time="575.62" type="appl" />
         <tli id="T713" time="576.428" type="appl" />
         <tli id="T714" time="577.237" type="appl" />
         <tli id="T715" time="578.045" type="appl" />
         <tli id="T716" time="578.963" type="appl" />
         <tli id="T717" time="579.882" type="appl" />
         <tli id="T718" time="580.8" type="appl" />
         <tli id="T719" time="581.718" type="appl" />
         <tli id="T720" time="582.607" type="appl" />
         <tli id="T721" time="583.495" type="appl" />
         <tli id="T722" time="584.384" type="appl" />
         <tli id="T723" time="585.273" type="appl" />
         <tli id="T724" time="586.161" type="appl" />
         <tli id="T725" time="587.05" type="appl" />
         <tli id="T726" time="587.2541037850959" />
         <tli id="T727" time="588.507424534697" />
         <tli id="T728" time="589.167417908157" />
         <tli id="T729" time="590.3007398625833" />
         <tli id="T730" time="590.6140700499836" />
         <tli id="T731" time="591.6540596081632" />
         <tli id="T732" time="592.2140539856445" />
         <tli id="T733" time="592.5140509735808" />
         <tli id="T734" time="594.0540355116543" />
         <tli id="T735" time="594.7140288851143" />
         <tli id="T736" time="595.580686850264" />
         <tli id="T737" time="596.2073472250644" />
         <tli id="T738" time="596.7140088046904" />
         <tli id="T739" time="596.9606729947714" />
         <tli id="T740" time="597.320669380295" />
         <tli id="T741" time="597.5740001701081" />
         <tli id="T742" time="597.8739971580445" />
         <tli id="T743" time="598.1339945475893" />
         <tli id="T744" time="598.3806587376704" />
         <tli id="T745" time="599.5206472918286" />
         <tli id="T746" time="600.33" type="appl" />
         <tli id="T747" time="601.22" type="appl" />
         <tli id="T748" time="602.109" type="appl" />
         <tli id="T749" time="602.999" type="appl" />
         <tli id="T750" time="603.889" type="appl" />
         <tli id="T751" time="604.435" type="appl" />
         <tli id="T752" time="604.981" type="appl" />
         <tli id="T753" time="605.527" type="appl" />
         <tli id="T754" time="606.074" type="appl" />
         <tli id="T755" time="606.62" type="appl" />
         <tli id="T756" time="607.166" type="appl" />
         <tli id="T757" time="607.712" type="appl" />
         <tli id="T758" time="608.258" type="appl" />
         <tli id="T759" time="608.782" type="appl" />
         <tli id="T760" time="609.301" type="appl" />
         <tli id="T761" time="609.819" type="appl" />
         <tli id="T762" time="610.553" type="appl" />
         <tli id="T763" time="611.572" type="appl" />
         <tli id="T764" time="612.59" type="appl" />
         <tli id="T765" time="614.2938322977633" />
         <tli id="T766" time="615.476" type="appl" />
         <tli id="T767" time="616.553" type="appl" />
         <tli id="T768" time="617.63" type="appl" />
         <tli id="T769" time="619.4537804902693" />
         <tli id="T770" time="620.112" type="appl" />
         <tli id="T771" time="620.672" type="appl" />
         <tli id="T772" time="621.233" type="appl" />
         <tli id="T773" time="621.793" type="appl" />
         <tli id="T774" time="622.354" type="appl" />
         <tli id="T775" time="622.914" type="appl" />
         <tli id="T776" time="623.475" type="appl" />
         <tli id="T777" time="624.035" type="appl" />
         <tli id="T778" time="624.596" type="appl" />
         <tli id="T779" time="625.689" type="appl" />
         <tli id="T780" time="626.782" type="appl" />
         <tli id="T781" time="627.876" type="appl" />
         <tli id="T782" time="628.969" type="appl" />
         <tli id="T783" time="630.062" type="appl" />
         <tli id="T784" time="631.155" type="appl" />
         <tli id="T785" time="632.229" type="appl" />
         <tli id="T786" time="633.303" type="appl" />
         <tli id="T787" time="634.377" type="appl" />
         <tli id="T788" time="634.977" type="appl" />
         <tli id="T789" time="635.577" type="appl" />
         <tli id="T790" time="636.177" type="appl" />
         <tli id="T791" time="636.777" type="appl" />
         <tli id="T792" time="637.7869297530492" />
         <tli id="T793" time="638.594" type="appl" />
         <tli id="T794" time="639.288" type="appl" />
         <tli id="T795" time="639.983" type="appl" />
         <tli id="T796" time="640.7802330326812" />
         <tli id="T797" time="641.618" type="appl" />
         <tli id="T798" time="642.243" type="appl" />
         <tli id="T799" time="642.867" type="appl" />
         <tli id="T800" time="644.3068642908669" />
         <tli id="T801" time="646.372" type="appl" />
         <tli id="T802" time="647.188" type="appl" />
         <tli id="T803" time="648.003" type="appl" />
         <tli id="T804" time="648.594" type="appl" />
         <tli id="T805" time="649.185" type="appl" />
         <tli id="T806" time="649.776" type="appl" />
         <tli id="T807" time="650.367" type="appl" />
         <tli id="T808" time="651.54" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T105" start="T102">
            <tli id="T102.tx.1" />
         </timeline-fork>
         <timeline-fork end="T152" start="T151">
            <tli id="T151.tx.1" />
         </timeline-fork>
         <timeline-fork end="T260" start="T258">
            <tli id="T258.tx.1" />
         </timeline-fork>
         <timeline-fork end="T299" start="T298">
            <tli id="T298.tx.1" />
         </timeline-fork>
         <timeline-fork end="T324" start="T323">
            <tli id="T323.tx.1" />
         </timeline-fork>
         <timeline-fork end="T378" start="T377">
            <tli id="T377.tx.1" />
         </timeline-fork>
         <timeline-fork end="T415" start="T414">
            <tli id="T414.tx.1" />
         </timeline-fork>
         <timeline-fork end="T509" start="T508">
            <tli id="T508.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T808" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">koŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnolaʔpi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Dĭn</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ibiʔi</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nagurgöʔ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">nʼizeŋdə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Dĭn</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">sad</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">ibi</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">jabloki</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">amnolaʔpiʔi</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">kuvazəʔi</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_52" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Šindidə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">bar</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">tojirlaʔbə</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_64" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">Dĭ</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">bar</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">kuliot</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">šindidə</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_77" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">tojirlaʔbə</ts>
                  <nts id="Seg_80" n="HIAT:ip">)</nts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_84" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Nuldəbi</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">iləm</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">măndərzittə</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_96" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">Dĭzeŋ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">măndliaʔi</ts>
                  <nts id="Seg_102" n="HIAT:ip">,</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">naga</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_109" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">Dĭgəttə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">onʼiʔ</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_116" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">nʼi</ts>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_121" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">mən-</ts>
                  <nts id="Seg_124" n="HIAT:ip">)</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">mănlia:</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">Măn</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">kallam</ts>
                  <nts id="Seg_135" n="HIAT:ip">!</nts>
                  <nts id="Seg_136" n="HIAT:ip">"</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_139" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_141" n="HIAT:w" s="T34">Dĭ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_144" n="HIAT:w" s="T35">kambi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_148" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_150" n="HIAT:w" s="T36">Kunolluʔpi</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_154" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_156" n="HIAT:w" s="T37">Ĭmbidə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_159" n="HIAT:w" s="T38">ej</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_162" n="HIAT:w" s="T39">kubi</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_166" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_168" n="HIAT:w" s="T40">Dĭgəttə</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_171" n="HIAT:w" s="T41">baška</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_174" n="HIAT:w" s="T42">nʼi</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T44" id="Seg_178" n="HIAT:w" s="T43">ka-</ts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_182" n="HIAT:w" s="T44">kambi</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_186" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_188" n="HIAT:w" s="T45">Tože</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">kunolluʔpi</ts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_195" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">Ĭmbidə</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_200" n="HIAT:w" s="T48">ej</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_203" n="HIAT:w" s="T49">kubi</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_207" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_209" n="HIAT:w" s="T50">Dĭgəttə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_212" n="HIAT:w" s="T51">Ivanuška</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_215" n="HIAT:w" s="T52">kambi</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_219" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_221" n="HIAT:w" s="T53">Ej</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_224" n="HIAT:w" s="T54">kunolia</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_228" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_230" n="HIAT:w" s="T55">Kunolzittə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">axota</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_237" n="HIAT:w" s="T57">dĭ</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">bar</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">büziʔ</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_246" n="HIAT:w" s="T60">sʼimabə</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_249" n="HIAT:w" s="T61">băzəlia</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_252" n="HIAT:w" s="T62">da</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_255" n="HIAT:w" s="T63">bazoʔ</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_258" n="HIAT:w" s="T64">mĭlleʔbə</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_262" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_264" n="HIAT:w" s="T65">Dĭgəttə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_267" n="HIAT:w" s="T66">kuliot</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_271" n="HIAT:w" s="T67">kulaːmbi</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_274" n="HIAT:w" s="T68">bar:</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_277" n="HIAT:w" s="T69">nendluʔpi</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_280" n="HIAT:w" s="T70">šü</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_284" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_286" n="HIAT:w" s="T71">Dĭ</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_288" n="HIAT:ip">(</nts>
                  <ts e="T73" id="Seg_290" n="HIAT:w" s="T72">ku-</ts>
                  <nts id="Seg_291" n="HIAT:ip">)</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_294" n="HIAT:w" s="T73">kulaːmbi:</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_298" n="HIAT:w" s="T74">Süj-</ts>
                  <nts id="Seg_299" n="HIAT:ip">)</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_302" n="HIAT:w" s="T75">Süjö</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_305" n="HIAT:w" s="T76">amnolaʔbə</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_309" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_311" n="HIAT:w" s="T77">Šü</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_314" n="HIAT:w" s="T78">dĭrgit</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_318" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_320" n="HIAT:w" s="T79">Dĭn</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_322" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_324" n="HIAT:w" s="T80">müʔluʔpi</ts>
                  <nts id="Seg_325" n="HIAT:ip">)</nts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_329" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_331" n="HIAT:w" s="T81">Dʼabəluʔpi</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_335" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_337" n="HIAT:w" s="T82">Onʼiʔ</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">pʼero</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_342" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_344" n="HIAT:w" s="T84">de-</ts>
                  <nts id="Seg_345" n="HIAT:ip">)</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_348" n="HIAT:w" s="T85">deʔpi</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_352" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_354" n="HIAT:w" s="T86">Dĭgəttə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_357" n="HIAT:w" s="T87">šobi</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_360" n="HIAT:w" s="T88">abanə</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_364" n="HIAT:u" s="T89">
                  <nts id="Seg_365" n="HIAT:ip">"</nts>
                  <ts e="T90" id="Seg_367" n="HIAT:w" s="T89">No</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_371" n="HIAT:w" s="T90">ĭmbi</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_374" n="HIAT:w" s="T91">nʼim</ts>
                  <nts id="Seg_375" n="HIAT:ip">?</nts>
                  <nts id="Seg_376" n="HIAT:ip">"</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_379" n="HIAT:u" s="T92">
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <nts id="Seg_381" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_383" n="HIAT:w" s="T92">D-</ts>
                  <nts id="Seg_384" n="HIAT:ip">)</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_387" n="HIAT:w" s="T93">Măn</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_390" n="HIAT:w" s="T94">kubiom</ts>
                  <nts id="Seg_391" n="HIAT:ip">,</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_394" n="HIAT:w" s="T95">šində</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_397" n="HIAT:w" s="T96">tojirlaʔbə</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_401" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_403" n="HIAT:w" s="T97">Dö</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_407" n="HIAT:w" s="T98">tănan</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_410" n="HIAT:w" s="T99">deʔpiem</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_413" n="HIAT:w" s="T100">pʼero</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_417" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_419" n="HIAT:w" s="T101">Dĭ</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102.tx.1" id="Seg_422" n="HIAT:w" s="T102">Žar</ts>
                  <nts id="Seg_423" n="HIAT:ip">_</nts>
                  <ts e="T105" id="Seg_425" n="HIAT:w" s="T102.tx.1">Ptitsa</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_428" n="HIAT:w" s="T105">mĭlleʔbə</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_431" n="HIAT:w" s="T106">i</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_434" n="HIAT:w" s="T107">tojirlaʔbə</ts>
                  <nts id="Seg_435" n="HIAT:ip">"</nts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_439" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_441" n="HIAT:w" s="T108">Dĭgəttə</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_444" n="HIAT:w" s="T109">abat</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_447" n="HIAT:w" s="T110">stal</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_450" n="HIAT:w" s="T111">amzittə</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_454" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_456" n="HIAT:w" s="T112">Dĭgəttə</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_459" n="HIAT:w" s="T113">tenöbi</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_463" n="HIAT:w" s="T114">tenöbi:</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_465" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_467" n="HIAT:w" s="T115">Kangaʔ</ts>
                  <nts id="Seg_468" n="HIAT:ip">,</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_471" n="HIAT:w" s="T116">nʼizeŋ</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_474" n="HIAT:w" s="T117">gibər-nʼibudʼ</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_478" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_480" n="HIAT:w" s="T118">Bar</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_483" n="HIAT:w" s="T119">dʼü</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_486" n="HIAT:w" s="T120">mĭngeʔ</ts>
                  <nts id="Seg_487" n="HIAT:ip">!</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_490" n="HIAT:u" s="T121">
                  <nts id="Seg_491" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_493" n="HIAT:w" s="T121">Il</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_496" n="HIAT:w" s="T122">mə-</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_499" n="HIAT:w" s="T123">mə-</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_502" n="HIAT:w" s="T124">mə-</ts>
                  <nts id="Seg_503" n="HIAT:ip">)</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_506" n="HIAT:w" s="T125">Možet</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_508" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_510" n="HIAT:w" s="T126">gijən=</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_513" n="HIAT:w" s="T127">paim-</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_516" n="HIAT:w" s="T128">ku-</ts>
                  <nts id="Seg_517" n="HIAT:ip">)</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_520" n="HIAT:w" s="T129">gijən-nʼibudʼ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_523" n="HIAT:w" s="T130">dĭ</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_526" n="HIAT:w" s="T131">süjö</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_528" n="HIAT:ip">(</nts>
                  <ts e="T133" id="Seg_530" n="HIAT:w" s="T132">dʼapia</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_533" n="HIAT:w" s="T133">-</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_536" n="HIAT:w" s="T134">dʼabiluj-</ts>
                  <nts id="Seg_537" n="HIAT:ip">)</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_540" n="HIAT:w" s="T135">dʼapləlaʔ</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_544" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_546" n="HIAT:w" s="T136">Girgit</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_549" n="HIAT:w" s="T137">šü</ts>
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_554" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_556" n="HIAT:w" s="T138">Dĭzeŋ</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_558" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_560" n="HIAT:w" s="T139">ka-</ts>
                  <nts id="Seg_561" n="HIAT:ip">)</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_564" n="HIAT:w" s="T140">kambiʔi</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_567" n="HIAT:w" s="T141">onʼiʔ</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_570" n="HIAT:w" s="T142">dibər</ts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_574" n="HIAT:w" s="T143">baška</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_577" n="HIAT:w" s="T144">döbər</ts>
                  <nts id="Seg_578" n="HIAT:ip">.</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_581" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_583" n="HIAT:w" s="T145">Vanʼuška</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_586" n="HIAT:w" s="T146">tože</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_589" n="HIAT:w" s="T147">kambi</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_593" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_595" n="HIAT:w" s="T148">Kabarləj</ts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_599" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_601" n="HIAT:w" s="T149">Dĭgəttə</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_604" n="HIAT:w" s="T150">dĭ</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.1" id="Seg_607" n="HIAT:w" s="T151">Ivan</ts>
                  <nts id="Seg_608" n="HIAT:ip">_</nts>
                  <ts e="T152" id="Seg_610" n="HIAT:w" s="T151.tx.1">Tsarevič</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_613" n="HIAT:w" s="T152">kambi</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_617" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_619" n="HIAT:w" s="T153">Šobi</ts>
                  <nts id="Seg_620" n="HIAT:ip">,</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_623" n="HIAT:w" s="T154">šobi</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_627" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_629" n="HIAT:w" s="T155">Inebə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_632" n="HIAT:w" s="T156">körerluʔpi</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_636" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_638" n="HIAT:w" s="T157">Ambi</ts>
                  <nts id="Seg_639" n="HIAT:ip">,</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_641" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_643" n="HIAT:w" s="T158">amnoʔ-</ts>
                  <nts id="Seg_644" n="HIAT:ip">)</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_647" n="HIAT:w" s="T159">iʔbəbi</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_650" n="HIAT:w" s="T160">kunolzittə</ts>
                  <nts id="Seg_651" n="HIAT:ip">.</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_654" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_656" n="HIAT:w" s="T161">Inet</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_659" n="HIAT:w" s="T162">mĭlleʔbə</ts>
                  <nts id="Seg_660" n="HIAT:ip">.</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_663" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_665" n="HIAT:w" s="T163">Ujubə</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_668" n="HIAT:w" s="T164">sarbi</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_672" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_674" n="HIAT:w" s="T165">Kundʼo</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_677" n="HIAT:w" s="T166">kunolbi</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_681" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_683" n="HIAT:w" s="T167">Dĭgəttə</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_686" n="HIAT:w" s="T168">uʔbdəbi</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_690" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_692" n="HIAT:w" s="T169">Kuliot:</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_695" n="HIAT:w" s="T170">inet</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_698" n="HIAT:w" s="T171">naga</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_702" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_704" n="HIAT:w" s="T172">Măndərbi</ts>
                  <nts id="Seg_705" n="HIAT:ip">,</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_708" n="HIAT:w" s="T173">măndərbi</ts>
                  <nts id="Seg_709" n="HIAT:ip">.</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_712" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_714" n="HIAT:w" s="T174">Tolʼko</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_717" n="HIAT:w" s="T175">leʔi</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_720" n="HIAT:w" s="T176">iʔbolaʔbəʔjə</ts>
                  <nts id="Seg_721" n="HIAT:ip">.</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_724" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_726" n="HIAT:w" s="T177">Amnolaʔbə</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_729" n="HIAT:w" s="T178">da</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_732" n="HIAT:w" s="T179">dĭgəttə</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_735" n="HIAT:w" s="T180">nada</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_738" n="HIAT:w" s="T181">kanzittə</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_741" n="HIAT:w" s="T182">üdʼi</ts>
                  <nts id="Seg_742" n="HIAT:ip">.</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_745" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_747" n="HIAT:w" s="T183">Kandəga</ts>
                  <nts id="Seg_748" n="HIAT:ip">,</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_751" n="HIAT:w" s="T184">kandəga</ts>
                  <nts id="Seg_752" n="HIAT:ip">,</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_755" n="HIAT:w" s="T185">dĭgəttə</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_758" n="HIAT:w" s="T186">tararluʔpi</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_761" n="HIAT:w" s="T187">da</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_764" n="HIAT:w" s="T188">bar</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_767" n="HIAT:w" s="T189">amnəluʔpi</ts>
                  <nts id="Seg_768" n="HIAT:ip">,</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_771" n="HIAT:w" s="T190">amnolaʔbə</ts>
                  <nts id="Seg_772" n="HIAT:ip">.</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_775" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_777" n="HIAT:w" s="T191">Dĭgəttə</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_780" n="HIAT:w" s="T192">volk</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_783" n="HIAT:w" s="T193">nuʔməleʔbə</ts>
                  <nts id="Seg_784" n="HIAT:ip">.</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_787" n="HIAT:u" s="T194">
                  <nts id="Seg_788" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_790" n="HIAT:w" s="T194">Ĭmbi</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_793" n="HIAT:w" s="T195">amnolaʔbəl</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_797" n="HIAT:w" s="T196">ulul</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_800" n="HIAT:w" s="T197">edəbiom</ts>
                  <nts id="Seg_801" n="HIAT:ip">?</nts>
                  <nts id="Seg_802" n="HIAT:ip">"</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_805" n="HIAT:u" s="T198">
                  <nts id="Seg_806" n="HIAT:ip">"</nts>
                  <ts e="T199" id="Seg_808" n="HIAT:w" s="T198">Da</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_811" n="HIAT:w" s="T199">măn</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_814" n="HIAT:w" s="T200">inem</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_817" n="HIAT:w" s="T201">amnuʔpi</ts>
                  <nts id="Seg_818" n="HIAT:ip">"</nts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_822" n="HIAT:u" s="T202">
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <ts e="T203" id="Seg_825" n="HIAT:w" s="T202">A</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_828" n="HIAT:w" s="T203">dĭ</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_831" n="HIAT:w" s="T204">măn</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_834" n="HIAT:w" s="T205">ambiam</ts>
                  <nts id="Seg_835" n="HIAT:ip">.</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_838" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_840" n="HIAT:w" s="T206">Dak</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_842" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_844" n="HIAT:w" s="T207">măʔ-</ts>
                  <nts id="Seg_845" n="HIAT:ip">)</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_848" n="HIAT:w" s="T208">a</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_851" n="HIAT:w" s="T209">gibər</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_854" n="HIAT:w" s="T210">tăn</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_857" n="HIAT:w" s="T211">kandəgal</ts>
                  <nts id="Seg_858" n="HIAT:ip">?</nts>
                  <nts id="Seg_859" n="HIAT:ip">"</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_862" n="HIAT:u" s="T212">
                  <nts id="Seg_863" n="HIAT:ip">"</nts>
                  <ts e="T213" id="Seg_865" n="HIAT:w" s="T212">Da</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_868" n="HIAT:w" s="T213">kandəgam</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_870" n="HIAT:ip">(</nts>
                  <ts e="T215" id="Seg_872" n="HIAT:w" s="T214">süjö=</ts>
                  <nts id="Seg_873" n="HIAT:ip">)</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_876" n="HIAT:w" s="T215">süjö</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_878" n="HIAT:ip">(</nts>
                  <ts e="T217" id="Seg_880" n="HIAT:w" s="T216">măndəzi-</ts>
                  <nts id="Seg_881" n="HIAT:ip">)</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_884" n="HIAT:w" s="T217">măndərzittə</ts>
                  <nts id="Seg_885" n="HIAT:ip">,</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_888" n="HIAT:w" s="T218">abam</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_891" n="HIAT:w" s="T219">măndə:</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_894" n="HIAT:w" s="T220">măndəraʔ</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_897" n="HIAT:w" s="T221">süjö</ts>
                  <nts id="Seg_898" n="HIAT:ip">!</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_901" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_903" n="HIAT:w" s="T222">Šü</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_906" n="HIAT:w" s="T223">dĭrgit</ts>
                  <nts id="Seg_907" n="HIAT:ip">"</nts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_911" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_913" n="HIAT:w" s="T224">Dĭgəttə</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_916" n="HIAT:w" s="T225">dĭ:</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_918" n="HIAT:ip">"</nts>
                  <ts e="T227" id="Seg_920" n="HIAT:w" s="T226">No</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_923" n="HIAT:w" s="T227">tăn</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_926" n="HIAT:w" s="T228">bostə</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_929" n="HIAT:w" s="T229">inelziʔ</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_932" n="HIAT:w" s="T230">dăk</ts>
                  <nts id="Seg_933" n="HIAT:ip">,</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_936" n="HIAT:w" s="T231">nagur</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_939" n="HIAT:w" s="T232">kö</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_942" n="HIAT:w" s="T233">dĭbər</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_945" n="HIAT:w" s="T234">ej</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_948" n="HIAT:w" s="T235">kallal</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_952" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_954" n="HIAT:w" s="T236">Amnaʔ</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_956" n="HIAT:ip">(</nts>
                  <ts e="T238" id="Seg_958" n="HIAT:w" s="T237">mănnə-</ts>
                  <nts id="Seg_959" n="HIAT:ip">)</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_962" n="HIAT:w" s="T238">măna</ts>
                  <nts id="Seg_963" n="HIAT:ip">!</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_966" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_968" n="HIAT:w" s="T239">Măn</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_971" n="HIAT:w" s="T240">tănan</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_974" n="HIAT:w" s="T241">kunnaːllim</ts>
                  <nts id="Seg_975" n="HIAT:ip">"</nts>
                  <nts id="Seg_976" n="HIAT:ip">.</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_979" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_981" n="HIAT:w" s="T242">Amnəbi</ts>
                  <nts id="Seg_982" n="HIAT:ip">,</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_985" n="HIAT:w" s="T243">dĭgəttə</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_988" n="HIAT:w" s="T244">dĭ</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_991" n="HIAT:w" s="T245">deʔpi</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_995" n="HIAT:u" s="T246">
                  <nts id="Seg_996" n="HIAT:ip">"</nts>
                  <ts e="T247" id="Seg_998" n="HIAT:w" s="T246">Kanaʔ</ts>
                  <nts id="Seg_999" n="HIAT:ip">!</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1002" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_1004" n="HIAT:w" s="T247">Tuj</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1007" n="HIAT:w" s="T248">bar</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1010" n="HIAT:w" s="T249">il</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1013" n="HIAT:w" s="T250">kunollaʔbəʔjə</ts>
                  <nts id="Seg_1014" n="HIAT:ip">.</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_1017" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1019" n="HIAT:w" s="T251">Iʔ</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1022" n="HIAT:w" s="T252">dĭ</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1025" n="HIAT:w" s="T253">süjö</ts>
                  <nts id="Seg_1026" n="HIAT:ip">!</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1029" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_1031" n="HIAT:w" s="T254">A</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1034" n="HIAT:w" s="T255">klʼetkabə</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1037" n="HIAT:w" s="T256">iʔ</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1040" n="HIAT:w" s="T257">it</ts>
                  <nts id="Seg_1041" n="HIAT:ip">,</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258.tx.1" id="Seg_1044" n="HIAT:w" s="T258">a</ts>
                  <nts id="Seg_1045" n="HIAT:ip">_</nts>
                  <ts e="T260" id="Seg_1047" n="HIAT:w" s="T258.tx.1">to</ts>
                  <nts id="Seg_1048" n="HIAT:ip">…</nts>
                  <nts id="Seg_1049" n="HIAT:ip">"</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1052" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1054" n="HIAT:w" s="T260">Dĭgəttə</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1057" n="HIAT:w" s="T261">dĭ</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1060" n="HIAT:w" s="T262">kambi</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1064" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1066" n="HIAT:w" s="T263">Ibi</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1069" n="HIAT:w" s="T264">süjö</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1073" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1075" n="HIAT:w" s="T265">Măndərlia</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1079" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1081" n="HIAT:w" s="T266">Klʼetka</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1084" n="HIAT:w" s="T267">kuvas</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1088" n="HIAT:w" s="T268">nada</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1091" n="HIAT:w" s="T269">izittə</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1095" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1097" n="HIAT:w" s="T270">Kak</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1100" n="HIAT:w" s="T271">dĭm</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1103" n="HIAT:w" s="T272">mĭngəluʔpi</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1106" n="HIAT:w" s="T273">bar</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1109" n="HIAT:w" s="T274">kuzurluʔpi</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1112" n="HIAT:w" s="T275">bar</ts>
                  <nts id="Seg_1113" n="HIAT:ip">.</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1116" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1118" n="HIAT:w" s="T276">Dĭm</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1121" n="HIAT:w" s="T277">dʼabəluʔpi</ts>
                  <nts id="Seg_1122" n="HIAT:ip">.</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1125" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1127" n="HIAT:w" s="T278">I</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1129" n="HIAT:ip">(</nts>
                  <ts e="T280" id="Seg_1131" n="HIAT:w" s="T279">kundlaʔ-</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1134" n="HIAT:w" s="T280">kun-</ts>
                  <nts id="Seg_1135" n="HIAT:ip">)</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1138" n="HIAT:w" s="T281">kumbiʔi</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1141" n="HIAT:w" s="T282">koŋdə</ts>
                  <nts id="Seg_1142" n="HIAT:ip">.</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1145" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1147" n="HIAT:w" s="T283">Kabarləj</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1151" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1153" n="HIAT:w" s="T284">Dĭgəttə</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1156" n="HIAT:w" s="T285">dĭ</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1159" n="HIAT:w" s="T286">šobi</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1162" n="HIAT:w" s="T287">koŋdə</ts>
                  <nts id="Seg_1163" n="HIAT:ip">.</nts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1166" n="HIAT:u" s="T288">
                  <nts id="Seg_1167" n="HIAT:ip">"</nts>
                  <ts e="T289" id="Seg_1169" n="HIAT:w" s="T288">Tăn</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1171" n="HIAT:ip">(</nts>
                  <ts e="T290" id="Seg_1173" n="HIAT:w" s="T289">ĭmbi=</ts>
                  <nts id="Seg_1174" n="HIAT:ip">)</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1177" n="HIAT:w" s="T290">ĭmbi</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1180" n="HIAT:w" s="T291">döbər</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1183" n="HIAT:w" s="T292">šobiam</ts>
                  <nts id="Seg_1184" n="HIAT:ip">,</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1187" n="HIAT:w" s="T293">tojirzittə</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1191" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1193" n="HIAT:w" s="T294">Šindin</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1196" n="HIAT:w" s="T295">tăn</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1199" n="HIAT:w" s="T296">nʼi</ts>
                  <nts id="Seg_1200" n="HIAT:ip">?</nts>
                  <nts id="Seg_1201" n="HIAT:ip">"</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1204" n="HIAT:u" s="T297">
                  <nts id="Seg_1205" n="HIAT:ip">"</nts>
                  <ts e="T298" id="Seg_1207" n="HIAT:w" s="T297">Măn</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298.tx.1" id="Seg_1210" n="HIAT:w" s="T298">Ivan</ts>
                  <nts id="Seg_1211" n="HIAT:ip">_</nts>
                  <ts e="T299" id="Seg_1213" n="HIAT:w" s="T298.tx.1">Tsarevič</ts>
                  <nts id="Seg_1214" n="HIAT:ip">"</nts>
                  <nts id="Seg_1215" n="HIAT:ip">.</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1218" n="HIAT:u" s="T299">
                  <nts id="Seg_1219" n="HIAT:ip">"</nts>
                  <ts e="T300" id="Seg_1221" n="HIAT:w" s="T299">Dăk</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1224" n="HIAT:w" s="T300">tăn</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1227" n="HIAT:w" s="T301">tojirzittə</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1230" n="HIAT:w" s="T302">zăxatel</ts>
                  <nts id="Seg_1231" n="HIAT:ip">?</nts>
                  <nts id="Seg_1232" n="HIAT:ip">"</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1235" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1237" n="HIAT:w" s="T303">Dĭgəttə</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1240" n="HIAT:w" s="T304">dĭ</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1243" n="HIAT:w" s="T305">măndə</ts>
                  <nts id="Seg_1244" n="HIAT:ip">.</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1247" n="HIAT:u" s="T306">
                  <nts id="Seg_1248" n="HIAT:ip">"</nts>
                  <ts e="T307" id="Seg_1250" n="HIAT:w" s="T306">A</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1253" n="HIAT:w" s="T307">dĭ</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1256" n="HIAT:w" s="T308">miʔnʼibeʔ</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1259" n="HIAT:w" s="T309">mĭmbi</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1262" n="HIAT:w" s="T310">da</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1265" n="HIAT:w" s="T311">jabloki</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1268" n="HIAT:w" s="T312">ambiʔi</ts>
                  <nts id="Seg_1269" n="HIAT:ip">"</nts>
                  <nts id="Seg_1270" n="HIAT:ip">.</nts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1273" n="HIAT:u" s="T313">
                  <nts id="Seg_1274" n="HIAT:ip">"</nts>
                  <ts e="T314" id="Seg_1276" n="HIAT:w" s="T313">A</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1279" n="HIAT:w" s="T314">tăn</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1282" n="HIAT:w" s="T315">šobial</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1285" n="HIAT:w" s="T316">bɨ</ts>
                  <nts id="Seg_1286" n="HIAT:ip">,</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1289" n="HIAT:w" s="T317">a</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1292" n="HIAT:w" s="T318">măn</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1295" n="HIAT:w" s="T319">dăre</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1298" n="HIAT:w" s="T320">tănan</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1301" n="HIAT:w" s="T321">mĭbiem</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1304" n="HIAT:w" s="T322">bɨ</ts>
                  <nts id="Seg_1305" n="HIAT:ip">,</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323.tx.1" id="Seg_1308" n="HIAT:w" s="T323">a</ts>
                  <nts id="Seg_1309" n="HIAT:ip">_</nts>
                  <ts e="T324" id="Seg_1311" n="HIAT:w" s="T323.tx.1">to</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1314" n="HIAT:w" s="T324">tojirzittə</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1317" n="HIAT:w" s="T325">šobial</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1321" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1323" n="HIAT:w" s="T326">No</ts>
                  <nts id="Seg_1324" n="HIAT:ip">,</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1327" n="HIAT:w" s="T327">măn</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1330" n="HIAT:w" s="T328">tănan</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1333" n="HIAT:w" s="T329">ĭmbidə</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1336" n="HIAT:w" s="T330">ej</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1339" n="HIAT:w" s="T331">alam</ts>
                  <nts id="Seg_1340" n="HIAT:ip">,</nts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1343" n="HIAT:w" s="T332">kanaʔ</ts>
                  <nts id="Seg_1344" n="HIAT:ip">,</nts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1347" n="HIAT:w" s="T333">ine</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1350" n="HIAT:w" s="T334">deʔ</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1353" n="HIAT:w" s="T335">zălatoj</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1356" n="HIAT:w" s="T336">grivazʼiʔ</ts>
                  <nts id="Seg_1357" n="HIAT:ip">"</nts>
                  <nts id="Seg_1358" n="HIAT:ip">.</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1361" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1363" n="HIAT:w" s="T337">Dĭgəttə</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1366" n="HIAT:w" s="T338">dĭ</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1369" n="HIAT:w" s="T339">kambi</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1372" n="HIAT:w" s="T340">volktə</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1376" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1378" n="HIAT:w" s="T341">Volk</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1381" n="HIAT:w" s="T342">măndə:</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1383" n="HIAT:ip">"</nts>
                  <ts e="T344" id="Seg_1385" n="HIAT:w" s="T343">Măn</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1388" n="HIAT:w" s="T344">mămbiam</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1391" n="HIAT:w" s="T345">tănan</ts>
                  <nts id="Seg_1392" n="HIAT:ip">,</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1395" n="HIAT:w" s="T346">iʔ</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1398" n="HIAT:w" s="T347">üžəmeʔ</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1402" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1404" n="HIAT:w" s="T348">A</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1407" n="HIAT:w" s="T349">tăn</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1409" n="HIAT:ip">(</nts>
                  <ts e="T351" id="Seg_1411" n="HIAT:w" s="T350">üžəmbiel</ts>
                  <nts id="Seg_1412" n="HIAT:ip">)</nts>
                  <nts id="Seg_1413" n="HIAT:ip">,</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1416" n="HIAT:w" s="T351">a</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1419" n="HIAT:w" s="T352">tüj</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1421" n="HIAT:ip">(</nts>
                  <nts id="Seg_1422" n="HIAT:ip">(</nts>
                  <ats e="T354" id="Seg_1423" n="HIAT:non-pho" s="T353">PAUSE</ats>
                  <nts id="Seg_1424" n="HIAT:ip">)</nts>
                  <nts id="Seg_1425" n="HIAT:ip">)</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1428" n="HIAT:w" s="T354">amnaʔ</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1431" n="HIAT:w" s="T355">măna</ts>
                  <nts id="Seg_1432" n="HIAT:ip">,</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1435" n="HIAT:w" s="T356">kanžəbəj</ts>
                  <nts id="Seg_1436" n="HIAT:ip">"</nts>
                  <nts id="Seg_1437" n="HIAT:ip">.</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1440" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1442" n="HIAT:w" s="T357">Kambiʔi</ts>
                  <nts id="Seg_1443" n="HIAT:ip">.</nts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1446" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1448" n="HIAT:w" s="T358">No</ts>
                  <nts id="Seg_1449" n="HIAT:ip">,</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1452" n="HIAT:w" s="T359">šobiʔi</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1455" n="HIAT:w" s="T360">dibər</ts>
                  <nts id="Seg_1456" n="HIAT:ip">.</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1459" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1461" n="HIAT:w" s="T361">Dĭgəttə:</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1463" n="HIAT:ip">"</nts>
                  <ts e="T363" id="Seg_1465" n="HIAT:w" s="T362">No</ts>
                  <nts id="Seg_1466" n="HIAT:ip">,</nts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1469" n="HIAT:w" s="T363">kanaʔ</ts>
                  <nts id="Seg_1470" n="HIAT:ip">!</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1473" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1475" n="HIAT:w" s="T364">Da</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1478" n="HIAT:w" s="T365">dʼabit</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1481" n="HIAT:w" s="T366">inem</ts>
                  <nts id="Seg_1482" n="HIAT:ip">,</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1485" n="HIAT:w" s="T367">a</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1488" n="HIAT:w" s="T368">uzdam</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1490" n="HIAT:ip">(</nts>
                  <ts e="T370" id="Seg_1492" n="HIAT:w" s="T369">ej=</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1495" n="HIAT:w" s="T370">uzu-</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1498" n="HIAT:w" s="T371">iʔ=</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1501" n="HIAT:w" s="T372">užum-</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1504" n="HIAT:w" s="T373">iʔ</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1507" n="HIAT:w" s="T374">u-</ts>
                  <nts id="Seg_1508" n="HIAT:ip">)</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1511" n="HIAT:w" s="T375">iʔ</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1514" n="HIAT:w" s="T376">iʔ</ts>
                  <nts id="Seg_1515" n="HIAT:ip">!</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1518" n="HIAT:u" s="T377">
                  <ts e="T377.tx.1" id="Seg_1520" n="HIAT:w" s="T377">A</ts>
                  <nts id="Seg_1521" n="HIAT:ip">_</nts>
                  <ts e="T378" id="Seg_1523" n="HIAT:w" s="T377.tx.1">to</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1526" n="HIAT:w" s="T378">bazoʔ</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1529" n="HIAT:w" s="T379">dăre</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1532" n="HIAT:w" s="T380">molaːlləj</ts>
                  <nts id="Seg_1533" n="HIAT:ip">!</nts>
                  <nts id="Seg_1534" n="HIAT:ip">"</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1537" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1539" n="HIAT:w" s="T381">Dĭ</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1542" n="HIAT:w" s="T382">šobi</ts>
                  <nts id="Seg_1543" n="HIAT:ip">.</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1546" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1548" n="HIAT:w" s="T383">Inem</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1551" n="HIAT:w" s="T384">ibi</ts>
                  <nts id="Seg_1552" n="HIAT:ip">.</nts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1555" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1557" n="HIAT:w" s="T385">A</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1560" n="HIAT:w" s="T386">uzdat</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1563" n="HIAT:w" s="T387">ugaːndə</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1566" n="HIAT:w" s="T388">kuvas</ts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1570" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1572" n="HIAT:w" s="T389">Ĭmbi</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1575" n="HIAT:w" s="T390">inegən</ts>
                  <nts id="Seg_1576" n="HIAT:ip">,</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1579" n="HIAT:w" s="T391">nada</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1582" n="HIAT:w" s="T392">šerzittə</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1585" n="HIAT:w" s="T393">tolʼko</ts>
                  <nts id="Seg_1586" n="HIAT:ip">.</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1589" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1591" n="HIAT:w" s="T394">Ibi</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1594" n="HIAT:w" s="T395">dĭm</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1598" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1600" n="HIAT:w" s="T396">Dĭ</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1603" n="HIAT:w" s="T397">bar</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1606" n="HIAT:w" s="T398">kirgarluʔpi</ts>
                  <nts id="Seg_1607" n="HIAT:ip">.</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1610" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1612" n="HIAT:w" s="T399">Il</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1615" n="HIAT:w" s="T400">suʔmiluʔpi</ts>
                  <nts id="Seg_1616" n="HIAT:ip">,</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1619" n="HIAT:w" s="T401">dĭm</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1622" n="HIAT:w" s="T402">ibiʔi</ts>
                  <nts id="Seg_1623" n="HIAT:ip">.</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1626" n="HIAT:u" s="T403">
                  <nts id="Seg_1627" n="HIAT:ip">(</nts>
                  <ts e="T404" id="Seg_1629" n="HIAT:w" s="T403">Kam-</ts>
                  <nts id="Seg_1630" n="HIAT:ip">)</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1633" n="HIAT:w" s="T404">Kunnaːmbiʔi</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1636" n="HIAT:w" s="T405">koŋdə</ts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1640" n="HIAT:u" s="T406">
                  <nts id="Seg_1641" n="HIAT:ip">"</nts>
                  <ts e="T407" id="Seg_1643" n="HIAT:w" s="T406">Ĭmbi</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1646" n="HIAT:w" s="T407">šobial</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1649" n="HIAT:w" s="T408">ine</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1652" n="HIAT:w" s="T409">tojirzittə</ts>
                  <nts id="Seg_1653" n="HIAT:ip">?</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1656" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1658" n="HIAT:w" s="T410">Šindin</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1661" n="HIAT:w" s="T411">tăn</ts>
                  <nts id="Seg_1662" n="HIAT:ip">?</nts>
                  <nts id="Seg_1663" n="HIAT:ip">"</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1666" n="HIAT:u" s="T412">
                  <nts id="Seg_1667" n="HIAT:ip">"</nts>
                  <nts id="Seg_1668" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_1670" n="HIAT:w" s="T412">Măn=</ts>
                  <nts id="Seg_1671" n="HIAT:ip">)</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1674" n="HIAT:w" s="T413">Măn</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414.tx.1" id="Seg_1677" n="HIAT:w" s="T414">Ivan</ts>
                  <nts id="Seg_1678" n="HIAT:ip">_</nts>
                  <ts e="T415" id="Seg_1680" n="HIAT:w" s="T414.tx.1">Tsarevič</ts>
                  <nts id="Seg_1681" n="HIAT:ip">"</nts>
                  <nts id="Seg_1682" n="HIAT:ip">.</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1685" n="HIAT:u" s="T415">
                  <nts id="Seg_1686" n="HIAT:ip">"</nts>
                  <ts e="T416" id="Seg_1688" n="HIAT:w" s="T415">Dăk</ts>
                  <nts id="Seg_1689" n="HIAT:ip">,</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1692" n="HIAT:w" s="T416">tojirzittə</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1694" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1696" n="HIAT:w" s="T417">s-</ts>
                  <nts id="Seg_1697" n="HIAT:ip">)</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1700" n="HIAT:w" s="T418">šobial</ts>
                  <nts id="Seg_1701" n="HIAT:ip">?</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1704" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1706" n="HIAT:w" s="T419">No</ts>
                  <nts id="Seg_1707" n="HIAT:ip">,</nts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1710" n="HIAT:w" s="T420">kanaʔ</ts>
                  <nts id="Seg_1711" n="HIAT:ip">,</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1714" n="HIAT:w" s="T421">măn</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1717" n="HIAT:w" s="T422">tănan</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1720" n="HIAT:w" s="T423">ĭmbidə</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1723" n="HIAT:w" s="T424">ej</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1726" n="HIAT:w" s="T425">alam</ts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1730" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1732" n="HIAT:w" s="T426">Dĭn</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1735" n="HIAT:w" s="T427">ige</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1738" n="HIAT:w" s="T428">koʔbdo</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1741" n="HIAT:w" s="T429">Ilena</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1744" n="HIAT:w" s="T430">Prĭkrasnaja</ts>
                  <nts id="Seg_1745" n="HIAT:ip">.</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1748" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1750" n="HIAT:w" s="T431">Dettə</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1753" n="HIAT:w" s="T432">măna</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1756" n="HIAT:w" s="T433">dĭm</ts>
                  <nts id="Seg_1757" n="HIAT:ip">!</nts>
                  <nts id="Seg_1758" n="HIAT:ip">"</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1761" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1763" n="HIAT:w" s="T434">Dĭgəttə</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1766" n="HIAT:w" s="T435">dĭ</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1768" n="HIAT:ip">(</nts>
                  <ts e="T437" id="Seg_1770" n="HIAT:w" s="T436">šo-</ts>
                  <nts id="Seg_1771" n="HIAT:ip">)</nts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1774" n="HIAT:w" s="T437">šonəga</ts>
                  <nts id="Seg_1775" n="HIAT:ip">.</nts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1778" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1780" n="HIAT:w" s="T438">A</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1783" n="HIAT:w" s="T439">volktə:</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1785" n="HIAT:ip">"</nts>
                  <ts e="T441" id="Seg_1787" n="HIAT:w" s="T440">Mămbiam:</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1790" n="HIAT:w" s="T441">iʔ</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1793" n="HIAT:w" s="T442">üžəmeʔ</ts>
                  <nts id="Seg_1794" n="HIAT:ip">,</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1797" n="HIAT:w" s="T443">a</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1800" n="HIAT:w" s="T444">tăn</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1803" n="HIAT:w" s="T445">üge</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1806" n="HIAT:w" s="T446">üžəmneʔpiem</ts>
                  <nts id="Seg_1807" n="HIAT:ip">"</nts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1811" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1813" n="HIAT:w" s="T447">No</ts>
                  <nts id="Seg_1814" n="HIAT:ip">,</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1817" n="HIAT:w" s="T448">ĭmbidə</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1819" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1821" n="HIAT:w" s="T449">emnajaʔa</ts>
                  <nts id="Seg_1822" n="HIAT:ip">)</nts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1826" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1828" n="HIAT:w" s="T450">Dĭ</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1831" n="HIAT:w" s="T451">măndə:</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1833" n="HIAT:ip">"</nts>
                  <ts e="T453" id="Seg_1835" n="HIAT:w" s="T452">No</ts>
                  <nts id="Seg_1836" n="HIAT:ip">,</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1839" n="HIAT:w" s="T453">amnaʔ</ts>
                  <nts id="Seg_1840" n="HIAT:ip">,</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1843" n="HIAT:w" s="T454">kanžəbəj</ts>
                  <nts id="Seg_1844" n="HIAT:ip">!</nts>
                  <nts id="Seg_1845" n="HIAT:ip">"</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1848" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1850" n="HIAT:w" s="T455">Šobiʔi</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1853" n="HIAT:w" s="T456">dibər</ts>
                  <nts id="Seg_1854" n="HIAT:ip">.</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_1857" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1859" n="HIAT:w" s="T457">Dĭgəttə</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1862" n="HIAT:w" s="T458">volk</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1865" n="HIAT:w" s="T459">măndə</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1868" n="HIAT:w" s="T460">dĭʔnə:</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1870" n="HIAT:ip">"</nts>
                  <ts e="T462" id="Seg_1872" n="HIAT:w" s="T461">Tăn</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1874" n="HIAT:ip">(</nts>
                  <ts e="T463" id="Seg_1876" n="HIAT:w" s="T462">am-</ts>
                  <nts id="Seg_1877" n="HIAT:ip">)</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1880" n="HIAT:w" s="T463">kanaʔ</ts>
                  <nts id="Seg_1881" n="HIAT:ip">,</nts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1884" n="HIAT:w" s="T464">paraʔ</ts>
                  <nts id="Seg_1885" n="HIAT:ip">,</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1888" n="HIAT:w" s="T465">paraʔ</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1892" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_1894" n="HIAT:w" s="T466">A</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1897" n="HIAT:w" s="T467">măn</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1900" n="HIAT:w" s="T468">kallam</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1903" n="HIAT:w" s="T469">bostə</ts>
                  <nts id="Seg_1904" n="HIAT:ip">"</nts>
                  <nts id="Seg_1905" n="HIAT:ip">.</nts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1908" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1910" n="HIAT:w" s="T470">Dĭgəttə</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1913" n="HIAT:w" s="T471">suʔməluʔpi</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1916" n="HIAT:w" s="T472">sattə</ts>
                  <nts id="Seg_1917" n="HIAT:ip">.</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1920" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1922" n="HIAT:w" s="T473">Dĭn</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1925" n="HIAT:w" s="T474">šaʔlaːmbi</ts>
                  <nts id="Seg_1926" n="HIAT:ip">.</nts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1929" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1931" n="HIAT:w" s="T475">Dĭzeŋ</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1934" n="HIAT:w" s="T476">mĭnzəleʔbəʔjə</ts>
                  <nts id="Seg_1935" n="HIAT:ip">,</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1938" n="HIAT:w" s="T477">ijat</ts>
                  <nts id="Seg_1939" n="HIAT:ip">,</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1942" n="HIAT:w" s="T478">nʼanʼkaʔi</ts>
                  <nts id="Seg_1943" n="HIAT:ip">.</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1946" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1948" n="HIAT:w" s="T479">Dĭ</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1951" n="HIAT:w" s="T480">Elenat</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1954" n="HIAT:w" s="T481">maluʔpi</ts>
                  <nts id="Seg_1955" n="HIAT:ip">.</nts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1958" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1960" n="HIAT:w" s="T482">Dĭ</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1963" n="HIAT:w" s="T483">dĭm</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1966" n="HIAT:w" s="T484">kabarluʔpi</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1969" n="HIAT:w" s="T485">i</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1972" n="HIAT:w" s="T486">nuʔməluʔpi</ts>
                  <nts id="Seg_1973" n="HIAT:ip">,</nts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1976" n="HIAT:w" s="T487">vot</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1979" n="HIAT:w" s="T488">nuʔməlaʔbə</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1982" n="HIAT:w" s="T489">i</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1985" n="HIAT:w" s="T490">bĭdəbi</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1988" n="HIAT:w" s="T491">Ivanəm</ts>
                  <nts id="Seg_1989" n="HIAT:ip">.</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1992" n="HIAT:u" s="T492">
                  <nts id="Seg_1993" n="HIAT:ip">"</nts>
                  <ts e="T493" id="Seg_1995" n="HIAT:w" s="T492">Amnaʔ</ts>
                  <nts id="Seg_1996" n="HIAT:ip">!</nts>
                  <nts id="Seg_1997" n="HIAT:ip">"</nts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_2000" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_2002" n="HIAT:w" s="T493">Amnəlbiʔi</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_2006" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_2008" n="HIAT:w" s="T494">Dĭgəttə</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2011" n="HIAT:w" s="T495">šobiʔi</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2014" n="HIAT:w" s="T496">dibər</ts>
                  <nts id="Seg_2015" n="HIAT:ip">,</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2018" n="HIAT:w" s="T497">gijen</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2020" n="HIAT:ip">(</nts>
                  <ts e="T499" id="Seg_2022" n="HIAT:w" s="T498">in-</ts>
                  <nts id="Seg_2023" n="HIAT:ip">)</nts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2026" n="HIAT:w" s="T499">ine</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2029" n="HIAT:w" s="T500">pʼebiʔi</ts>
                  <nts id="Seg_2030" n="HIAT:ip">.</nts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2033" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_2035" n="HIAT:w" s="T501">Dĭgəttə</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2038" n="HIAT:w" s="T502">dĭzeŋ</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2041" n="HIAT:w" s="T503">šobiʔi</ts>
                  <nts id="Seg_2042" n="HIAT:ip">,</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2045" n="HIAT:w" s="T504">a</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2048" n="HIAT:w" s="T505">dĭ</ts>
                  <nts id="Seg_2049" n="HIAT:ip">,</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2052" n="HIAT:w" s="T506">tenöleʔbə</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2055" n="HIAT:w" s="T507">üge</ts>
                  <nts id="Seg_2056" n="HIAT:ip">,</nts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508.tx.1" id="Seg_2059" n="HIAT:w" s="T508">Ivan</ts>
                  <nts id="Seg_2060" n="HIAT:ip">_</nts>
                  <ts e="T509" id="Seg_2062" n="HIAT:w" s="T508.tx.1">Tsarevič</ts>
                  <nts id="Seg_2063" n="HIAT:ip">.</nts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_2066" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_2068" n="HIAT:w" s="T509">Ajiria</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2071" n="HIAT:w" s="T510">dĭ</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2074" n="HIAT:w" s="T511">koʔbdom</ts>
                  <nts id="Seg_2075" n="HIAT:ip">.</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_2078" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_2080" n="HIAT:w" s="T512">A</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2083" n="HIAT:w" s="T513">volktə</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2086" n="HIAT:w" s="T514">surarlaʔbə:</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2088" n="HIAT:ip">"</nts>
                  <ts e="T516" id="Seg_2090" n="HIAT:w" s="T515">Ĭmbi</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2093" n="HIAT:w" s="T516">tăn</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2096" n="HIAT:w" s="T517">dĭn</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2099" n="HIAT:w" s="T518">nulaʔbəl</ts>
                  <nts id="Seg_2100" n="HIAT:ip">?</nts>
                  <nts id="Seg_2101" n="HIAT:ip">"</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T523" id="Seg_2104" n="HIAT:u" s="T519">
                  <nts id="Seg_2105" n="HIAT:ip">"</nts>
                  <ts e="T520" id="Seg_2107" n="HIAT:w" s="T519">Dĭ</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2110" n="HIAT:w" s="T520">dĭrgit</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2113" n="HIAT:w" s="T521">kuvas</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2116" n="HIAT:w" s="T522">koʔbdo</ts>
                  <nts id="Seg_2117" n="HIAT:ip">.</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2120" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_2122" n="HIAT:w" s="T523">Măn</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2125" n="HIAT:w" s="T524">ajirbiom</ts>
                  <nts id="Seg_2126" n="HIAT:ip">,</nts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2128" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_2130" n="HIAT:w" s="T525">mĭzen-</ts>
                  <nts id="Seg_2131" n="HIAT:ip">)</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2134" n="HIAT:w" s="T526">mĭzittə</ts>
                  <nts id="Seg_2135" n="HIAT:ip">"</nts>
                  <nts id="Seg_2136" n="HIAT:ip">.</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2139" n="HIAT:u" s="T527">
                  <nts id="Seg_2140" n="HIAT:ip">"</nts>
                  <ts e="T528" id="Seg_2142" n="HIAT:w" s="T527">No</ts>
                  <nts id="Seg_2143" n="HIAT:ip">,</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2146" n="HIAT:w" s="T528">šaʔbdəlbəj</ts>
                  <nts id="Seg_2147" n="HIAT:ip">,</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2150" n="HIAT:w" s="T529">a</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2152" n="HIAT:ip">(</nts>
                  <ts e="T531" id="Seg_2154" n="HIAT:w" s="T530">măn</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2157" n="HIAT:w" s="T531">alaʔ-</ts>
                  <nts id="Seg_2158" n="HIAT:ip">)</nts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2161" n="HIAT:w" s="T532">măn</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2164" n="HIAT:w" s="T533">alaʔbəm</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2166" n="HIAT:ip">(</nts>
                  <ts e="T535" id="Seg_2168" n="HIAT:w" s="T534">dĭ-</ts>
                  <nts id="Seg_2169" n="HIAT:ip">)</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2172" n="HIAT:w" s="T535">dĭʔnə</ts>
                  <nts id="Seg_2173" n="HIAT:ip">"</nts>
                  <nts id="Seg_2174" n="HIAT:ip">.</nts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_2177" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_2179" n="HIAT:w" s="T536">Dĭgəttə</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2182" n="HIAT:w" s="T537">bostə</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2185" n="HIAT:w" s="T538">bögəldə</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2188" n="HIAT:w" s="T539">suʔməluʔpi</ts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_2192" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_2194" n="HIAT:w" s="T540">Dĭ</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2197" n="HIAT:w" s="T541">dĭm</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2200" n="HIAT:w" s="T542">ibi</ts>
                  <nts id="Seg_2201" n="HIAT:ip">,</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2204" n="HIAT:w" s="T543">kunnaːmbi</ts>
                  <nts id="Seg_2205" n="HIAT:ip">.</nts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2208" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2210" n="HIAT:w" s="T544">A</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2213" n="HIAT:w" s="T545">dĭ</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2216" n="HIAT:w" s="T546">ibi</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2219" n="HIAT:w" s="T547">dĭ</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2222" n="HIAT:w" s="T548">koʔbdom</ts>
                  <nts id="Seg_2223" n="HIAT:ip">.</nts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_2226" n="HIAT:u" s="T549">
                  <nts id="Seg_2227" n="HIAT:ip">(</nts>
                  <ts e="T550" id="Seg_2229" n="HIAT:w" s="T549">Dʼansaʔməj</ts>
                  <nts id="Seg_2230" n="HIAT:ip">)</nts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2233" n="HIAT:w" s="T550">ara</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2236" n="HIAT:w" s="T551">bĭʔpiʔi</ts>
                  <nts id="Seg_2237" n="HIAT:ip">,</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2240" n="HIAT:w" s="T552">suʔmileʔbəʔi</ts>
                  <nts id="Seg_2241" n="HIAT:ip">,</nts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2244" n="HIAT:w" s="T553">inebə</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2247" n="HIAT:w" s="T554">mĭbi</ts>
                  <nts id="Seg_2248" n="HIAT:ip">.</nts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2251" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_2253" n="HIAT:w" s="T555">Dĭ</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2256" n="HIAT:w" s="T556">inebə</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2259" n="HIAT:w" s="T557">ibi</ts>
                  <nts id="Seg_2260" n="HIAT:ip">.</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2263" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2265" n="HIAT:w" s="T558">Elenabə</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2268" n="HIAT:w" s="T559">amnəlbəbi</ts>
                  <nts id="Seg_2269" n="HIAT:ip">,</nts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2272" n="HIAT:w" s="T560">bostə</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2275" n="HIAT:w" s="T561">amnəlbi</ts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2279" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2281" n="HIAT:w" s="T562">Kambiʔi</ts>
                  <nts id="Seg_2282" n="HIAT:ip">.</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2285" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2287" n="HIAT:w" s="T563">A</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2290" n="HIAT:w" s="T564">nüdʼin</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2292" n="HIAT:ip">(</nts>
                  <ts e="T566" id="Seg_2294" n="HIAT:w" s="T565">iʔbibi-</ts>
                  <nts id="Seg_2295" n="HIAT:ip">)</nts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2298" n="HIAT:w" s="T566">iʔbəʔi</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2301" n="HIAT:w" s="T567">kunolzittə</ts>
                  <nts id="Seg_2302" n="HIAT:ip">.</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_2305" n="HIAT:u" s="T568">
                  <nts id="Seg_2306" n="HIAT:ip">(</nts>
                  <ts e="T569" id="Seg_2308" n="HIAT:w" s="T568">Dĭ=</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2311" n="HIAT:w" s="T569">uʔ-</ts>
                  <nts id="Seg_2312" n="HIAT:ip">)</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2315" n="HIAT:w" s="T570">Dĭ</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2318" n="HIAT:w" s="T571">Elenat</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2320" n="HIAT:ip">(</nts>
                  <ts e="T573" id="Seg_2322" n="HIAT:w" s="T572">mo-</ts>
                  <nts id="Seg_2323" n="HIAT:ip">)</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2326" n="HIAT:w" s="T573">molaːmbi</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2329" n="HIAT:w" s="T574">volkziʔ</ts>
                  <nts id="Seg_2330" n="HIAT:ip">.</nts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_2333" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_2335" n="HIAT:w" s="T575">I</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2338" n="HIAT:w" s="T576">dĭ</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2341" n="HIAT:w" s="T577">nerölüʔpi</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2344" n="HIAT:w" s="T578">i</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2347" n="HIAT:w" s="T579">saʔməluʔpi</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2351" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_2353" n="HIAT:w" s="T580">A</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2356" n="HIAT:w" s="T581">dĭ</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2359" n="HIAT:w" s="T582">nuʔməluʔpi</ts>
                  <nts id="Seg_2360" n="HIAT:ip">.</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2363" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2365" n="HIAT:w" s="T583">Dĭgəttə</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2368" n="HIAT:w" s="T584">bĭdəliet</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2371" n="HIAT:w" s="T585">Ivanuška</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2373" n="HIAT:ip">(</nts>
                  <ts e="T587" id="Seg_2375" n="HIAT:w" s="T586">Elenuškaʔiziʔ</ts>
                  <nts id="Seg_2376" n="HIAT:ip">)</nts>
                  <nts id="Seg_2377" n="HIAT:ip">.</nts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2380" n="HIAT:u" s="T587">
                  <nts id="Seg_2381" n="HIAT:ip">"</nts>
                  <ts e="T588" id="Seg_2383" n="HIAT:w" s="T587">Ĭmbi</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2386" n="HIAT:w" s="T588">tăn</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2389" n="HIAT:w" s="T589">tĭn</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2392" n="HIAT:w" s="T590">nulaʔliel</ts>
                  <nts id="Seg_2393" n="HIAT:ip">?</nts>
                  <nts id="Seg_2394" n="HIAT:ip">"</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2397" n="HIAT:u" s="T591">
                  <nts id="Seg_2398" n="HIAT:ip">"</nts>
                  <ts e="T592" id="Seg_2400" n="HIAT:w" s="T591">Da</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2403" n="HIAT:w" s="T592">nʼe</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2406" n="HIAT:w" s="T593">axota</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2409" n="HIAT:w" s="T594">mĭzittə</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2412" n="HIAT:w" s="T595">ine</ts>
                  <nts id="Seg_2413" n="HIAT:ip">,</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2416" n="HIAT:w" s="T596">kuvas</ts>
                  <nts id="Seg_2417" n="HIAT:ip">"</nts>
                  <nts id="Seg_2418" n="HIAT:ip">.</nts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_2421" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2423" n="HIAT:w" s="T597">A</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2426" n="HIAT:w" s="T598">dĭ</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2429" n="HIAT:w" s="T599">măndə:</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2431" n="HIAT:ip">"</nts>
                  <ts e="T601" id="Seg_2433" n="HIAT:w" s="T600">Šaʔlaːndə</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2436" n="HIAT:w" s="T601">inem</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2439" n="HIAT:w" s="T602">i</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2442" n="HIAT:w" s="T603">koʔbdom</ts>
                  <nts id="Seg_2443" n="HIAT:ip">"</nts>
                  <nts id="Seg_2444" n="HIAT:ip">.</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2447" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2449" n="HIAT:w" s="T604">Dĭgəttə</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2452" n="HIAT:w" s="T605">dĭ</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2455" n="HIAT:w" s="T606">aluʔpi</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2458" n="HIAT:w" s="T607">ineziʔ</ts>
                  <nts id="Seg_2459" n="HIAT:ip">,</nts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2462" n="HIAT:w" s="T608">dĭ</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2465" n="HIAT:w" s="T609">dĭm</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2468" n="HIAT:w" s="T610">kumbi</ts>
                  <nts id="Seg_2469" n="HIAT:ip">.</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2472" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2474" n="HIAT:w" s="T611">Dĭ</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2477" n="HIAT:w" s="T612">ibi</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2480" n="HIAT:w" s="T613">süjö</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2482" n="HIAT:ip">(</nts>
                  <ts e="T615" id="Seg_2484" n="HIAT:w" s="T614">nenʼinuʔ</ts>
                  <nts id="Seg_2485" n="HIAT:ip">)</nts>
                  <nts id="Seg_2486" n="HIAT:ip">,</nts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2488" n="HIAT:ip">(</nts>
                  <ts e="T616" id="Seg_2490" n="HIAT:w" s="T615">kak</ts>
                  <nts id="Seg_2491" n="HIAT:ip">)</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2494" n="HIAT:w" s="T616">šü</ts>
                  <nts id="Seg_2495" n="HIAT:ip">.</nts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2498" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_2500" n="HIAT:w" s="T617">I</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2503" n="HIAT:w" s="T618">bostə</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2506" n="HIAT:w" s="T619">parluʔpi</ts>
                  <nts id="Seg_2507" n="HIAT:ip">,</nts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2510" n="HIAT:w" s="T620">amnəlbi</ts>
                  <nts id="Seg_2511" n="HIAT:ip">.</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_2514" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2516" n="HIAT:w" s="T621">I</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2519" n="HIAT:w" s="T622">koʔbdobə</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2522" n="HIAT:w" s="T623">amnəlbi</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2525" n="HIAT:w" s="T624">inenə</ts>
                  <nts id="Seg_2526" n="HIAT:ip">,</nts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2529" n="HIAT:w" s="T625">bostə</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2532" n="HIAT:w" s="T626">amnəlbi</ts>
                  <nts id="Seg_2533" n="HIAT:ip">,</nts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2536" n="HIAT:w" s="T627">kambiʔi</ts>
                  <nts id="Seg_2537" n="HIAT:ip">.</nts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2540" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2542" n="HIAT:w" s="T628">Dĭgəttə</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2545" n="HIAT:w" s="T629">dĭ</ts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2548" n="HIAT:w" s="T630">koŋ:</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2550" n="HIAT:ip">"</nts>
                  <ts e="T632" id="Seg_2552" n="HIAT:w" s="T631">Da</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2555" n="HIAT:w" s="T632">deʔkeʔ</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2558" n="HIAT:w" s="T633">dĭ</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2561" n="HIAT:w" s="T634">ine</ts>
                  <nts id="Seg_2562" n="HIAT:ip">!</nts>
                  <nts id="Seg_2563" n="HIAT:ip">"</nts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2566" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2568" n="HIAT:w" s="T635">Dĭ</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2571" n="HIAT:w" s="T636">ine</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2574" n="HIAT:w" s="T637">šobi</ts>
                  <nts id="Seg_2575" n="HIAT:ip">,</nts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2578" n="HIAT:w" s="T638">volkziʔ</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2581" n="HIAT:w" s="T639">aluʔpi</ts>
                  <nts id="Seg_2582" n="HIAT:ip">.</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2585" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2587" n="HIAT:w" s="T640">Dĭ</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2590" n="HIAT:w" s="T641">neröluʔpi</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2593" n="HIAT:w" s="T642">i</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2596" n="HIAT:w" s="T643">saʔməluʔpi</ts>
                  <nts id="Seg_2597" n="HIAT:ip">,</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2600" n="HIAT:w" s="T644">a</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2603" n="HIAT:w" s="T645">dĭ</ts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2606" n="HIAT:w" s="T646">nuʔməluʔpi</ts>
                  <nts id="Seg_2607" n="HIAT:ip">.</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2610" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2612" n="HIAT:w" s="T647">Bĭdəleʔbə</ts>
                  <nts id="Seg_2613" n="HIAT:ip">,</nts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2616" n="HIAT:w" s="T648">Vanʼuškanə</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2619" n="HIAT:w" s="T649">măndə:</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2621" n="HIAT:ip">"</nts>
                  <ts e="T651" id="Seg_2623" n="HIAT:w" s="T650">No</ts>
                  <nts id="Seg_2624" n="HIAT:ip">,</nts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2627" n="HIAT:w" s="T651">tüj</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2630" n="HIAT:w" s="T652">măna</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2633" n="HIAT:w" s="T653">kanzittə</ts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2636" n="HIAT:w" s="T654">tănziʔ</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2639" n="HIAT:w" s="T655">nʼekuda</ts>
                  <nts id="Seg_2640" n="HIAT:ip">,</nts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2643" n="HIAT:w" s="T656">maləm</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2646" n="HIAT:w" s="T657">dön</ts>
                  <nts id="Seg_2647" n="HIAT:ip">"</nts>
                  <nts id="Seg_2648" n="HIAT:ip">.</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2651" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2653" n="HIAT:w" s="T658">Dĭ</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2656" n="HIAT:w" s="T659">suʔmiluʔpi</ts>
                  <nts id="Seg_2657" n="HIAT:ip">,</nts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2660" n="HIAT:w" s="T660">dĭʔnə</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2663" n="HIAT:w" s="T661">nagurgöʔ</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2666" n="HIAT:w" s="T662">dʼünə</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2669" n="HIAT:w" s="T663">saʔməluʔpi</ts>
                  <nts id="Seg_2670" n="HIAT:ip">.</nts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T665" id="Seg_2673" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2675" n="HIAT:w" s="T664">Kabarləj</ts>
                  <nts id="Seg_2676" n="HIAT:ip">.</nts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2679" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_2681" n="HIAT:w" s="T665">Dĭgəttə</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2683" n="HIAT:ip">(</nts>
                  <ts e="T667" id="Seg_2685" n="HIAT:w" s="T666">dĭ-</ts>
                  <nts id="Seg_2686" n="HIAT:ip">)</nts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2689" n="HIAT:w" s="T667">dĭzeŋ</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2692" n="HIAT:w" s="T668">šobiʔi</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2695" n="HIAT:w" s="T669">bostə</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2697" n="HIAT:ip">(</nts>
                  <ts e="T671" id="Seg_2699" n="HIAT:w" s="T670">dʼü-</ts>
                  <nts id="Seg_2700" n="HIAT:ip">)</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2703" n="HIAT:w" s="T671">dʼünə</ts>
                  <nts id="Seg_2704" n="HIAT:ip">.</nts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2707" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2709" n="HIAT:w" s="T672">Măndə:</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2711" n="HIAT:ip">"</nts>
                  <ts e="T674" id="Seg_2713" n="HIAT:w" s="T673">Davaj</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2716" n="HIAT:w" s="T674">amorləbəj</ts>
                  <nts id="Seg_2717" n="HIAT:ip">"</nts>
                  <nts id="Seg_2718" n="HIAT:ip">.</nts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2721" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2723" n="HIAT:w" s="T675">Amnəbəlbiʔi</ts>
                  <nts id="Seg_2724" n="HIAT:ip">,</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2727" n="HIAT:w" s="T676">amorbiʔi</ts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2731" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2733" n="HIAT:w" s="T677">I</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2735" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2737" n="HIAT:w" s="T678">iʔbəʔi-</ts>
                  <nts id="Seg_2738" n="HIAT:ip">)</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2741" n="HIAT:w" s="T679">iʔpiʔi</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2744" n="HIAT:w" s="T680">kunolzittə</ts>
                  <nts id="Seg_2745" n="HIAT:ip">.</nts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2748" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2750" n="HIAT:w" s="T681">Dĭgəttə</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2753" n="HIAT:w" s="T682">kagazaŋdə</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2756" n="HIAT:w" s="T683">šobiʔi</ts>
                  <nts id="Seg_2757" n="HIAT:ip">.</nts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2760" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_2762" n="HIAT:w" s="T684">Dĭm</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2765" n="HIAT:w" s="T685">kutlaːmbiʔi</ts>
                  <nts id="Seg_2766" n="HIAT:ip">.</nts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2769" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2771" n="HIAT:w" s="T686">A</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2774" n="HIAT:w" s="T687">bostə</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2777" n="HIAT:w" s="T688">ibiʔi</ts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2780" n="HIAT:w" s="T689">koʔbdo</ts>
                  <nts id="Seg_2781" n="HIAT:ip">.</nts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2784" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_2786" n="HIAT:w" s="T690">I</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2789" n="HIAT:w" s="T691">süjö</ts>
                  <nts id="Seg_2790" n="HIAT:ip">,</nts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2793" n="HIAT:w" s="T692">i</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2796" n="HIAT:w" s="T693">ine</ts>
                  <nts id="Seg_2797" n="HIAT:ip">,</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2800" n="HIAT:w" s="T694">kambi</ts>
                  <nts id="Seg_2801" n="HIAT:ip">.</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2804" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2806" n="HIAT:w" s="T695">Kandəlaʔbəʔjə</ts>
                  <nts id="Seg_2807" n="HIAT:ip">.</nts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2810" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2812" n="HIAT:w" s="T696">Dĭgəttə</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2815" n="HIAT:w" s="T697">volk</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2818" n="HIAT:w" s="T698">šobi</ts>
                  <nts id="Seg_2819" n="HIAT:ip">,</nts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2821" n="HIAT:ip">(</nts>
                  <ts e="T700" id="Seg_2823" n="HIAT:w" s="T699">kul-</ts>
                  <nts id="Seg_2824" n="HIAT:ip">)</nts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2827" n="HIAT:w" s="T700">kuliot:</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2830" n="HIAT:w" s="T701">dĭ</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2833" n="HIAT:w" s="T702">iʔbolaʔbə</ts>
                  <nts id="Seg_2834" n="HIAT:ip">.</nts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2837" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2839" n="HIAT:w" s="T703">Dʼaʔpi</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2842" n="HIAT:w" s="T704">süjö</ts>
                  <nts id="Seg_2843" n="HIAT:ip">,</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2846" n="HIAT:w" s="T705">vărona</ts>
                  <nts id="Seg_2847" n="HIAT:ip">,</nts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2850" n="HIAT:w" s="T706">üdʼüge</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2853" n="HIAT:w" s="T707">süjö</ts>
                  <nts id="Seg_2854" n="HIAT:ip">.</nts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2857" n="HIAT:u" s="T708">
                  <nts id="Seg_2858" n="HIAT:ip">"</nts>
                  <ts e="T709" id="Seg_2860" n="HIAT:w" s="T708">Da</ts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2863" n="HIAT:w" s="T709">deʔ</ts>
                  <nts id="Seg_2864" n="HIAT:ip">,</nts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2867" n="HIAT:w" s="T710">kübi</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2870" n="HIAT:w" s="T711">bü</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2873" n="HIAT:w" s="T712">i</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2876" n="HIAT:w" s="T713">tʼili</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2879" n="HIAT:w" s="T714">bü</ts>
                  <nts id="Seg_2880" n="HIAT:ip">!</nts>
                  <nts id="Seg_2881" n="HIAT:ip">"</nts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2884" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2886" n="HIAT:w" s="T715">Dĭ</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2889" n="HIAT:w" s="T716">süjö</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2892" n="HIAT:w" s="T717">nʼergölüʔpi</ts>
                  <nts id="Seg_2893" n="HIAT:ip">,</nts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2896" n="HIAT:w" s="T718">deʔpi</ts>
                  <nts id="Seg_2897" n="HIAT:ip">.</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2900" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2902" n="HIAT:w" s="T719">Dĭ</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2905" n="HIAT:w" s="T720">dĭm</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2908" n="HIAT:w" s="T721">bazəbi</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2910" n="HIAT:ip">(</nts>
                  <ts e="T723" id="Seg_2912" n="HIAT:w" s="T722">ku-</ts>
                  <nts id="Seg_2913" n="HIAT:ip">)</nts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2916" n="HIAT:w" s="T723">kule</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2919" n="HIAT:w" s="T724">büzʼiʔ</ts>
                  <nts id="Seg_2920" n="HIAT:ip">.</nts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2923" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2925" n="HIAT:w" s="T725">I</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2928" n="HIAT:w" s="T726">dĭgəttə</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2930" n="HIAT:ip">(</nts>
                  <ts e="T728" id="Seg_2932" n="HIAT:w" s="T727">dʼilʼuj</ts>
                  <nts id="Seg_2933" n="HIAT:ip">)</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2936" n="HIAT:w" s="T728">büziʔ</ts>
                  <nts id="Seg_2937" n="HIAT:ip">.</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_2940" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2942" n="HIAT:w" s="T729">Dĭ</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2945" n="HIAT:w" s="T730">suʔmileʔbə:</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2947" n="HIAT:ip">"</nts>
                  <ts e="T732" id="Seg_2949" n="HIAT:w" s="T731">Kondʼo</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2952" n="HIAT:w" s="T732">măn</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2955" n="HIAT:w" s="T733">kunolbiam</ts>
                  <nts id="Seg_2956" n="HIAT:ip">"</nts>
                  <nts id="Seg_2957" n="HIAT:ip">.</nts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2960" n="HIAT:u" s="T734">
                  <nts id="Seg_2961" n="HIAT:ip">"</nts>
                  <ts e="T735" id="Seg_2963" n="HIAT:w" s="T734">Kondʼo</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2966" n="HIAT:w" s="T735">kunolbial</ts>
                  <nts id="Seg_2967" n="HIAT:ip">,</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2969" n="HIAT:ip">(</nts>
                  <ts e="T737" id="Seg_2971" n="HIAT:w" s="T736">kak-</ts>
                  <nts id="Seg_2972" n="HIAT:ip">)</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2975" n="HIAT:w" s="T737">kabɨ</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2978" n="HIAT:w" s="T738">ej</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2981" n="HIAT:w" s="T739">măn</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2984" n="HIAT:w" s="T740">dak</ts>
                  <nts id="Seg_2985" n="HIAT:ip">,</nts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2988" n="HIAT:w" s="T741">tăn</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2991" n="HIAT:w" s="T742">uge</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2994" n="HIAT:w" s="T743">bɨ</ts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2997" n="HIAT:w" s="T744">kunolbial</ts>
                  <nts id="Seg_2998" n="HIAT:ip">.</nts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T750" id="Seg_3001" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_3003" n="HIAT:w" s="T745">Tănan</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3005" n="HIAT:ip">(</nts>
                  <ts e="T747" id="Seg_3007" n="HIAT:w" s="T746">kutluʔpiʔi</ts>
                  <nts id="Seg_3008" n="HIAT:ip">)</nts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3011" n="HIAT:w" s="T747">tăn</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3013" n="HIAT:ip">(</nts>
                  <ts e="T749" id="Seg_3015" n="HIAT:w" s="T748">kagazaŋ-</ts>
                  <nts id="Seg_3016" n="HIAT:ip">)</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3019" n="HIAT:w" s="T749">kagazaŋdə</ts>
                  <nts id="Seg_3020" n="HIAT:ip">.</nts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_3023" n="HIAT:u" s="T750">
                  <ts e="T751" id="Seg_3025" n="HIAT:w" s="T750">I</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3028" n="HIAT:w" s="T751">bar</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3031" n="HIAT:w" s="T752">iluʔpiʔi</ts>
                  <nts id="Seg_3032" n="HIAT:ip">,</nts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3035" n="HIAT:w" s="T753">koʔbdom</ts>
                  <nts id="Seg_3036" n="HIAT:ip">,</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3039" n="HIAT:w" s="T754">i</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3042" n="HIAT:w" s="T755">süjöm</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3045" n="HIAT:w" s="T756">i</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3048" n="HIAT:w" s="T757">inem</ts>
                  <nts id="Seg_3049" n="HIAT:ip">"</nts>
                  <nts id="Seg_3050" n="HIAT:ip">.</nts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_3053" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_3055" n="HIAT:w" s="T758">Dĭgəttə:</ts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3057" n="HIAT:ip">"</nts>
                  <ts e="T760" id="Seg_3059" n="HIAT:w" s="T759">Amnaʔ</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3062" n="HIAT:w" s="T760">măna</ts>
                  <nts id="Seg_3063" n="HIAT:ip">!</nts>
                  <nts id="Seg_3064" n="HIAT:ip">"</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_3067" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_3069" n="HIAT:w" s="T761">Amnolbi</ts>
                  <nts id="Seg_3070" n="HIAT:ip">.</nts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_3073" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3075" n="HIAT:w" s="T762">Dĭzeŋ</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3078" n="HIAT:w" s="T763">bĭdəbiʔi</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3081" n="HIAT:w" s="T764">dĭm</ts>
                  <nts id="Seg_3082" n="HIAT:ip">.</nts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T769" id="Seg_3085" n="HIAT:u" s="T765">
                  <ts e="T766" id="Seg_3087" n="HIAT:w" s="T765">Dĭ</ts>
                  <nts id="Seg_3088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3090" n="HIAT:w" s="T766">volk</ts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3093" n="HIAT:w" s="T767">kagazaŋdə</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3095" n="HIAT:ip">(</nts>
                  <ts e="T769" id="Seg_3097" n="HIAT:w" s="T768">sajnožuʔpi</ts>
                  <nts id="Seg_3098" n="HIAT:ip">)</nts>
                  <nts id="Seg_3099" n="HIAT:ip">.</nts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_3102" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_3104" n="HIAT:w" s="T769">I</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3107" n="HIAT:w" s="T770">bostə</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3110" n="HIAT:w" s="T771">ibi</ts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3113" n="HIAT:w" s="T772">koʔbdo</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3116" n="HIAT:w" s="T773">i</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3119" n="HIAT:w" s="T774">bar</ts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3122" n="HIAT:w" s="T775">ĭmbi</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3125" n="HIAT:w" s="T776">dĭn</ts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3128" n="HIAT:w" s="T777">ibi</ts>
                  <nts id="Seg_3129" n="HIAT:ip">.</nts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_3132" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_3134" n="HIAT:w" s="T778">Dĭgəttə</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3137" n="HIAT:w" s="T779">dĭ</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3140" n="HIAT:w" s="T780">Ivanuška</ts>
                  <nts id="Seg_3141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3143" n="HIAT:w" s="T781">dĭʔnə</ts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3146" n="HIAT:w" s="T782">saʔməluʔpi</ts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3149" n="HIAT:w" s="T783">üjündə</ts>
                  <nts id="Seg_3150" n="HIAT:ip">.</nts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_3153" n="HIAT:u" s="T784">
                  <ts e="T785" id="Seg_3155" n="HIAT:w" s="T784">I</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3158" n="HIAT:w" s="T785">parluʔpi</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3161" n="HIAT:w" s="T786">maːndə</ts>
                  <nts id="Seg_3162" n="HIAT:ip">.</nts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3165" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_3167" n="HIAT:w" s="T787">Šobi</ts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3170" n="HIAT:w" s="T788">abandə</ts>
                  <nts id="Seg_3171" n="HIAT:ip">,</nts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3174" n="HIAT:w" s="T789">bar</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3177" n="HIAT:w" s="T790">ĭmbi</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3180" n="HIAT:w" s="T791">nörbəbi</ts>
                  <nts id="Seg_3181" n="HIAT:ip">.</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3184" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3186" n="HIAT:w" s="T792">Dĭgəttə</ts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3189" n="HIAT:w" s="T793">abat</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3192" n="HIAT:w" s="T794">dʼorbi</ts>
                  <nts id="Seg_3193" n="HIAT:ip">,</nts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3196" n="HIAT:w" s="T795">ajirbi</ts>
                  <nts id="Seg_3197" n="HIAT:ip">.</nts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_3200" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3202" n="HIAT:w" s="T796">Dĭgəttə</ts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3205" n="HIAT:w" s="T797">dĭ</ts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3208" n="HIAT:w" s="T798">koʔbdom</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3211" n="HIAT:w" s="T799">ibi</ts>
                  <nts id="Seg_3212" n="HIAT:ip">.</nts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3215" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_3217" n="HIAT:w" s="T800">Monoʔkobiʔi</ts>
                  <nts id="Seg_3218" n="HIAT:ip">.</nts>
                  <nts id="Seg_3219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_3221" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3223" n="HIAT:w" s="T801">Ara</ts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3226" n="HIAT:w" s="T802">bĭʔpiʔi</ts>
                  <nts id="Seg_3227" n="HIAT:ip">.</nts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_3230" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_3232" n="HIAT:w" s="T803">I</ts>
                  <nts id="Seg_3233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3235" n="HIAT:w" s="T804">tüjö</ts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3238" n="HIAT:w" s="T805">uge</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3241" n="HIAT:w" s="T806">amnolaʔbəʔjə</ts>
                  <nts id="Seg_3242" n="HIAT:ip">.</nts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3245" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_3247" n="HIAT:w" s="T807">Kabarləj</ts>
                  <nts id="Seg_3248" n="HIAT:ip">.</nts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T808" id="Seg_3250" n="sc" s="T0">
               <ts e="T1" id="Seg_3252" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_3254" n="e" s="T1">koŋ </ts>
               <ts e="T3" id="Seg_3256" n="e" s="T2">amnolaʔpi. </ts>
               <ts e="T4" id="Seg_3258" n="e" s="T3">Dĭn </ts>
               <ts e="T5" id="Seg_3260" n="e" s="T4">ibiʔi </ts>
               <ts e="T6" id="Seg_3262" n="e" s="T5">nagurgöʔ </ts>
               <ts e="T7" id="Seg_3264" n="e" s="T6">nʼizeŋdə. </ts>
               <ts e="T8" id="Seg_3266" n="e" s="T7">Dĭn </ts>
               <ts e="T9" id="Seg_3268" n="e" s="T8">sad </ts>
               <ts e="T10" id="Seg_3270" n="e" s="T9">ibi, </ts>
               <ts e="T11" id="Seg_3272" n="e" s="T10">jabloki </ts>
               <ts e="T12" id="Seg_3274" n="e" s="T11">amnolaʔpiʔi, </ts>
               <ts e="T13" id="Seg_3276" n="e" s="T12">kuvazəʔi. </ts>
               <ts e="T14" id="Seg_3278" n="e" s="T13">Šindidə </ts>
               <ts e="T15" id="Seg_3280" n="e" s="T14">bar </ts>
               <ts e="T16" id="Seg_3282" n="e" s="T15">tojirlaʔbə. </ts>
               <ts e="T17" id="Seg_3284" n="e" s="T16">Dĭ </ts>
               <ts e="T18" id="Seg_3286" n="e" s="T17">bar </ts>
               <ts e="T19" id="Seg_3288" n="e" s="T18">kuliot </ts>
               <ts e="T20" id="Seg_3290" n="e" s="T19">šindidə </ts>
               <ts e="T21" id="Seg_3292" n="e" s="T20">(tojirlaʔbə). </ts>
               <ts e="T22" id="Seg_3294" n="e" s="T21">Nuldəbi </ts>
               <ts e="T23" id="Seg_3296" n="e" s="T22">iləm </ts>
               <ts e="T24" id="Seg_3298" n="e" s="T23">măndərzittə. </ts>
               <ts e="T25" id="Seg_3300" n="e" s="T24">Dĭzeŋ </ts>
               <ts e="T26" id="Seg_3302" n="e" s="T25">măndliaʔi, </ts>
               <ts e="T27" id="Seg_3304" n="e" s="T26">naga. </ts>
               <ts e="T28" id="Seg_3306" n="e" s="T27">Dĭgəttə </ts>
               <ts e="T29" id="Seg_3308" n="e" s="T28">onʼiʔ </ts>
               <ts e="T30" id="Seg_3310" n="e" s="T29">(nʼi) </ts>
               <ts e="T31" id="Seg_3312" n="e" s="T30">(mən-) </ts>
               <ts e="T32" id="Seg_3314" n="e" s="T31">mănlia: </ts>
               <ts e="T33" id="Seg_3316" n="e" s="T32">"Măn </ts>
               <ts e="T34" id="Seg_3318" n="e" s="T33">kallam!" </ts>
               <ts e="T35" id="Seg_3320" n="e" s="T34">Dĭ </ts>
               <ts e="T36" id="Seg_3322" n="e" s="T35">kambi. </ts>
               <ts e="T37" id="Seg_3324" n="e" s="T36">Kunolluʔpi. </ts>
               <ts e="T38" id="Seg_3326" n="e" s="T37">Ĭmbidə </ts>
               <ts e="T39" id="Seg_3328" n="e" s="T38">ej </ts>
               <ts e="T40" id="Seg_3330" n="e" s="T39">kubi. </ts>
               <ts e="T41" id="Seg_3332" n="e" s="T40">Dĭgəttə </ts>
               <ts e="T42" id="Seg_3334" n="e" s="T41">baška </ts>
               <ts e="T43" id="Seg_3336" n="e" s="T42">nʼi </ts>
               <ts e="T44" id="Seg_3338" n="e" s="T43">(ka-) </ts>
               <ts e="T45" id="Seg_3340" n="e" s="T44">kambi. </ts>
               <ts e="T46" id="Seg_3342" n="e" s="T45">Tože </ts>
               <ts e="T47" id="Seg_3344" n="e" s="T46">kunolluʔpi. </ts>
               <ts e="T48" id="Seg_3346" n="e" s="T47">Ĭmbidə </ts>
               <ts e="T49" id="Seg_3348" n="e" s="T48">ej </ts>
               <ts e="T50" id="Seg_3350" n="e" s="T49">kubi. </ts>
               <ts e="T51" id="Seg_3352" n="e" s="T50">Dĭgəttə </ts>
               <ts e="T52" id="Seg_3354" n="e" s="T51">Ivanuška </ts>
               <ts e="T53" id="Seg_3356" n="e" s="T52">kambi. </ts>
               <ts e="T54" id="Seg_3358" n="e" s="T53">Ej </ts>
               <ts e="T55" id="Seg_3360" n="e" s="T54">kunolia. </ts>
               <ts e="T56" id="Seg_3362" n="e" s="T55">Kunolzittə </ts>
               <ts e="T57" id="Seg_3364" n="e" s="T56">axota, </ts>
               <ts e="T58" id="Seg_3366" n="e" s="T57">dĭ </ts>
               <ts e="T59" id="Seg_3368" n="e" s="T58">bar </ts>
               <ts e="T60" id="Seg_3370" n="e" s="T59">büziʔ </ts>
               <ts e="T61" id="Seg_3372" n="e" s="T60">sʼimabə </ts>
               <ts e="T62" id="Seg_3374" n="e" s="T61">băzəlia </ts>
               <ts e="T63" id="Seg_3376" n="e" s="T62">da </ts>
               <ts e="T64" id="Seg_3378" n="e" s="T63">bazoʔ </ts>
               <ts e="T65" id="Seg_3380" n="e" s="T64">mĭlleʔbə. </ts>
               <ts e="T66" id="Seg_3382" n="e" s="T65">Dĭgəttə </ts>
               <ts e="T67" id="Seg_3384" n="e" s="T66">kuliot, </ts>
               <ts e="T68" id="Seg_3386" n="e" s="T67">kulaːmbi </ts>
               <ts e="T69" id="Seg_3388" n="e" s="T68">bar: </ts>
               <ts e="T70" id="Seg_3390" n="e" s="T69">nendluʔpi </ts>
               <ts e="T71" id="Seg_3392" n="e" s="T70">šü. </ts>
               <ts e="T72" id="Seg_3394" n="e" s="T71">Dĭ </ts>
               <ts e="T73" id="Seg_3396" n="e" s="T72">(ku-) </ts>
               <ts e="T74" id="Seg_3398" n="e" s="T73">kulaːmbi: </ts>
               <ts e="T75" id="Seg_3400" n="e" s="T74">(Süj-) </ts>
               <ts e="T76" id="Seg_3402" n="e" s="T75">Süjö </ts>
               <ts e="T77" id="Seg_3404" n="e" s="T76">amnolaʔbə. </ts>
               <ts e="T78" id="Seg_3406" n="e" s="T77">Šü </ts>
               <ts e="T79" id="Seg_3408" n="e" s="T78">dĭrgit. </ts>
               <ts e="T80" id="Seg_3410" n="e" s="T79">Dĭn </ts>
               <ts e="T81" id="Seg_3412" n="e" s="T80">(müʔluʔpi). </ts>
               <ts e="T82" id="Seg_3414" n="e" s="T81">Dʼabəluʔpi. </ts>
               <ts e="T83" id="Seg_3416" n="e" s="T82">Onʼiʔ </ts>
               <ts e="T84" id="Seg_3418" n="e" s="T83">pʼero </ts>
               <ts e="T85" id="Seg_3420" n="e" s="T84">(de-) </ts>
               <ts e="T86" id="Seg_3422" n="e" s="T85">deʔpi. </ts>
               <ts e="T87" id="Seg_3424" n="e" s="T86">Dĭgəttə </ts>
               <ts e="T88" id="Seg_3426" n="e" s="T87">šobi </ts>
               <ts e="T89" id="Seg_3428" n="e" s="T88">abanə. </ts>
               <ts e="T90" id="Seg_3430" n="e" s="T89">"No, </ts>
               <ts e="T91" id="Seg_3432" n="e" s="T90">ĭmbi </ts>
               <ts e="T92" id="Seg_3434" n="e" s="T91">nʼim?" </ts>
               <ts e="T93" id="Seg_3436" n="e" s="T92">"(D-) </ts>
               <ts e="T94" id="Seg_3438" n="e" s="T93">Măn </ts>
               <ts e="T95" id="Seg_3440" n="e" s="T94">kubiom, </ts>
               <ts e="T96" id="Seg_3442" n="e" s="T95">šində </ts>
               <ts e="T97" id="Seg_3444" n="e" s="T96">tojirlaʔbə. </ts>
               <ts e="T98" id="Seg_3446" n="e" s="T97">Dö, </ts>
               <ts e="T99" id="Seg_3448" n="e" s="T98">tănan </ts>
               <ts e="T100" id="Seg_3450" n="e" s="T99">deʔpiem </ts>
               <ts e="T101" id="Seg_3452" n="e" s="T100">pʼero. </ts>
               <ts e="T102" id="Seg_3454" n="e" s="T101">Dĭ </ts>
               <ts e="T105" id="Seg_3456" n="e" s="T102">Žar_Ptitsa </ts>
               <ts e="T106" id="Seg_3458" n="e" s="T105">mĭlleʔbə </ts>
               <ts e="T107" id="Seg_3460" n="e" s="T106">i </ts>
               <ts e="T108" id="Seg_3462" n="e" s="T107">tojirlaʔbə". </ts>
               <ts e="T109" id="Seg_3464" n="e" s="T108">Dĭgəttə </ts>
               <ts e="T110" id="Seg_3466" n="e" s="T109">abat </ts>
               <ts e="T111" id="Seg_3468" n="e" s="T110">stal </ts>
               <ts e="T112" id="Seg_3470" n="e" s="T111">amzittə. </ts>
               <ts e="T113" id="Seg_3472" n="e" s="T112">Dĭgəttə </ts>
               <ts e="T114" id="Seg_3474" n="e" s="T113">tenöbi, </ts>
               <ts e="T115" id="Seg_3476" n="e" s="T114">tenöbi: </ts>
               <ts e="T116" id="Seg_3478" n="e" s="T115">"Kangaʔ, </ts>
               <ts e="T117" id="Seg_3480" n="e" s="T116">nʼizeŋ </ts>
               <ts e="T118" id="Seg_3482" n="e" s="T117">gibər-nʼibudʼ. </ts>
               <ts e="T119" id="Seg_3484" n="e" s="T118">Bar </ts>
               <ts e="T120" id="Seg_3486" n="e" s="T119">dʼü </ts>
               <ts e="T121" id="Seg_3488" n="e" s="T120">mĭngeʔ! </ts>
               <ts e="T122" id="Seg_3490" n="e" s="T121">(Il </ts>
               <ts e="T123" id="Seg_3492" n="e" s="T122">mə- </ts>
               <ts e="T124" id="Seg_3494" n="e" s="T123">mə- </ts>
               <ts e="T125" id="Seg_3496" n="e" s="T124">mə-) </ts>
               <ts e="T126" id="Seg_3498" n="e" s="T125">Možet </ts>
               <ts e="T127" id="Seg_3500" n="e" s="T126">(gijən= </ts>
               <ts e="T128" id="Seg_3502" n="e" s="T127">paim- </ts>
               <ts e="T129" id="Seg_3504" n="e" s="T128">ku-) </ts>
               <ts e="T130" id="Seg_3506" n="e" s="T129">gijən-nʼibudʼ </ts>
               <ts e="T131" id="Seg_3508" n="e" s="T130">dĭ </ts>
               <ts e="T132" id="Seg_3510" n="e" s="T131">süjö </ts>
               <ts e="T133" id="Seg_3512" n="e" s="T132">(dʼapia </ts>
               <ts e="T134" id="Seg_3514" n="e" s="T133">- </ts>
               <ts e="T135" id="Seg_3516" n="e" s="T134">dʼabiluj-) </ts>
               <ts e="T136" id="Seg_3518" n="e" s="T135">dʼapləlaʔ. </ts>
               <ts e="T137" id="Seg_3520" n="e" s="T136">Girgit </ts>
               <ts e="T138" id="Seg_3522" n="e" s="T137">šü". </ts>
               <ts e="T139" id="Seg_3524" n="e" s="T138">Dĭzeŋ </ts>
               <ts e="T140" id="Seg_3526" n="e" s="T139">(ka-) </ts>
               <ts e="T141" id="Seg_3528" n="e" s="T140">kambiʔi </ts>
               <ts e="T142" id="Seg_3530" n="e" s="T141">onʼiʔ </ts>
               <ts e="T143" id="Seg_3532" n="e" s="T142">dibər, </ts>
               <ts e="T144" id="Seg_3534" n="e" s="T143">baška </ts>
               <ts e="T145" id="Seg_3536" n="e" s="T144">döbər. </ts>
               <ts e="T146" id="Seg_3538" n="e" s="T145">Vanʼuška </ts>
               <ts e="T147" id="Seg_3540" n="e" s="T146">tože </ts>
               <ts e="T148" id="Seg_3542" n="e" s="T147">kambi. </ts>
               <ts e="T149" id="Seg_3544" n="e" s="T148">Kabarləj. </ts>
               <ts e="T150" id="Seg_3546" n="e" s="T149">Dĭgəttə </ts>
               <ts e="T151" id="Seg_3548" n="e" s="T150">dĭ </ts>
               <ts e="T152" id="Seg_3550" n="e" s="T151">Ivan_Tsarevič </ts>
               <ts e="T153" id="Seg_3552" n="e" s="T152">kambi. </ts>
               <ts e="T154" id="Seg_3554" n="e" s="T153">Šobi, </ts>
               <ts e="T155" id="Seg_3556" n="e" s="T154">šobi. </ts>
               <ts e="T156" id="Seg_3558" n="e" s="T155">Inebə </ts>
               <ts e="T157" id="Seg_3560" n="e" s="T156">körerluʔpi. </ts>
               <ts e="T158" id="Seg_3562" n="e" s="T157">Ambi, </ts>
               <ts e="T159" id="Seg_3564" n="e" s="T158">(amnoʔ-) </ts>
               <ts e="T160" id="Seg_3566" n="e" s="T159">iʔbəbi </ts>
               <ts e="T161" id="Seg_3568" n="e" s="T160">kunolzittə. </ts>
               <ts e="T162" id="Seg_3570" n="e" s="T161">Inet </ts>
               <ts e="T163" id="Seg_3572" n="e" s="T162">mĭlleʔbə. </ts>
               <ts e="T164" id="Seg_3574" n="e" s="T163">Ujubə </ts>
               <ts e="T165" id="Seg_3576" n="e" s="T164">sarbi. </ts>
               <ts e="T166" id="Seg_3578" n="e" s="T165">Kundʼo </ts>
               <ts e="T167" id="Seg_3580" n="e" s="T166">kunolbi. </ts>
               <ts e="T168" id="Seg_3582" n="e" s="T167">Dĭgəttə </ts>
               <ts e="T169" id="Seg_3584" n="e" s="T168">uʔbdəbi. </ts>
               <ts e="T170" id="Seg_3586" n="e" s="T169">Kuliot: </ts>
               <ts e="T171" id="Seg_3588" n="e" s="T170">inet </ts>
               <ts e="T172" id="Seg_3590" n="e" s="T171">naga. </ts>
               <ts e="T173" id="Seg_3592" n="e" s="T172">Măndərbi, </ts>
               <ts e="T174" id="Seg_3594" n="e" s="T173">măndərbi. </ts>
               <ts e="T175" id="Seg_3596" n="e" s="T174">Tolʼko </ts>
               <ts e="T176" id="Seg_3598" n="e" s="T175">leʔi </ts>
               <ts e="T177" id="Seg_3600" n="e" s="T176">iʔbolaʔbəʔjə. </ts>
               <ts e="T178" id="Seg_3602" n="e" s="T177">Amnolaʔbə </ts>
               <ts e="T179" id="Seg_3604" n="e" s="T178">da </ts>
               <ts e="T180" id="Seg_3606" n="e" s="T179">dĭgəttə </ts>
               <ts e="T181" id="Seg_3608" n="e" s="T180">nada </ts>
               <ts e="T182" id="Seg_3610" n="e" s="T181">kanzittə </ts>
               <ts e="T183" id="Seg_3612" n="e" s="T182">üdʼi. </ts>
               <ts e="T184" id="Seg_3614" n="e" s="T183">Kandəga, </ts>
               <ts e="T185" id="Seg_3616" n="e" s="T184">kandəga, </ts>
               <ts e="T186" id="Seg_3618" n="e" s="T185">dĭgəttə </ts>
               <ts e="T187" id="Seg_3620" n="e" s="T186">tararluʔpi </ts>
               <ts e="T188" id="Seg_3622" n="e" s="T187">da </ts>
               <ts e="T189" id="Seg_3624" n="e" s="T188">bar </ts>
               <ts e="T190" id="Seg_3626" n="e" s="T189">amnəluʔpi, </ts>
               <ts e="T191" id="Seg_3628" n="e" s="T190">amnolaʔbə. </ts>
               <ts e="T192" id="Seg_3630" n="e" s="T191">Dĭgəttə </ts>
               <ts e="T193" id="Seg_3632" n="e" s="T192">volk </ts>
               <ts e="T194" id="Seg_3634" n="e" s="T193">nuʔməleʔbə. </ts>
               <ts e="T195" id="Seg_3636" n="e" s="T194">"Ĭmbi </ts>
               <ts e="T196" id="Seg_3638" n="e" s="T195">amnolaʔbəl, </ts>
               <ts e="T197" id="Seg_3640" n="e" s="T196">ulul </ts>
               <ts e="T198" id="Seg_3642" n="e" s="T197">edəbiom?" </ts>
               <ts e="T199" id="Seg_3644" n="e" s="T198">"Da </ts>
               <ts e="T200" id="Seg_3646" n="e" s="T199">măn </ts>
               <ts e="T201" id="Seg_3648" n="e" s="T200">inem </ts>
               <ts e="T202" id="Seg_3650" n="e" s="T201">amnuʔpi". </ts>
               <ts e="T203" id="Seg_3652" n="e" s="T202">"A </ts>
               <ts e="T204" id="Seg_3654" n="e" s="T203">dĭ </ts>
               <ts e="T205" id="Seg_3656" n="e" s="T204">măn </ts>
               <ts e="T206" id="Seg_3658" n="e" s="T205">ambiam. </ts>
               <ts e="T207" id="Seg_3660" n="e" s="T206">Dak </ts>
               <ts e="T208" id="Seg_3662" n="e" s="T207">(măʔ-) </ts>
               <ts e="T209" id="Seg_3664" n="e" s="T208">a </ts>
               <ts e="T210" id="Seg_3666" n="e" s="T209">gibər </ts>
               <ts e="T211" id="Seg_3668" n="e" s="T210">tăn </ts>
               <ts e="T212" id="Seg_3670" n="e" s="T211">kandəgal?" </ts>
               <ts e="T213" id="Seg_3672" n="e" s="T212">"Da </ts>
               <ts e="T214" id="Seg_3674" n="e" s="T213">kandəgam </ts>
               <ts e="T215" id="Seg_3676" n="e" s="T214">(süjö=) </ts>
               <ts e="T216" id="Seg_3678" n="e" s="T215">süjö </ts>
               <ts e="T217" id="Seg_3680" n="e" s="T216">(măndəzi-) </ts>
               <ts e="T218" id="Seg_3682" n="e" s="T217">măndərzittə, </ts>
               <ts e="T219" id="Seg_3684" n="e" s="T218">abam </ts>
               <ts e="T220" id="Seg_3686" n="e" s="T219">măndə: </ts>
               <ts e="T221" id="Seg_3688" n="e" s="T220">măndəraʔ </ts>
               <ts e="T222" id="Seg_3690" n="e" s="T221">süjö! </ts>
               <ts e="T223" id="Seg_3692" n="e" s="T222">Šü </ts>
               <ts e="T224" id="Seg_3694" n="e" s="T223">dĭrgit". </ts>
               <ts e="T225" id="Seg_3696" n="e" s="T224">Dĭgəttə </ts>
               <ts e="T226" id="Seg_3698" n="e" s="T225">dĭ: </ts>
               <ts e="T227" id="Seg_3700" n="e" s="T226">"No </ts>
               <ts e="T228" id="Seg_3702" n="e" s="T227">tăn </ts>
               <ts e="T229" id="Seg_3704" n="e" s="T228">bostə </ts>
               <ts e="T230" id="Seg_3706" n="e" s="T229">inelziʔ </ts>
               <ts e="T231" id="Seg_3708" n="e" s="T230">dăk, </ts>
               <ts e="T232" id="Seg_3710" n="e" s="T231">nagur </ts>
               <ts e="T233" id="Seg_3712" n="e" s="T232">kö </ts>
               <ts e="T234" id="Seg_3714" n="e" s="T233">dĭbər </ts>
               <ts e="T235" id="Seg_3716" n="e" s="T234">ej </ts>
               <ts e="T236" id="Seg_3718" n="e" s="T235">kallal. </ts>
               <ts e="T237" id="Seg_3720" n="e" s="T236">Amnaʔ </ts>
               <ts e="T238" id="Seg_3722" n="e" s="T237">(mănnə-) </ts>
               <ts e="T239" id="Seg_3724" n="e" s="T238">măna! </ts>
               <ts e="T240" id="Seg_3726" n="e" s="T239">Măn </ts>
               <ts e="T241" id="Seg_3728" n="e" s="T240">tănan </ts>
               <ts e="T242" id="Seg_3730" n="e" s="T241">kunnaːllim". </ts>
               <ts e="T243" id="Seg_3732" n="e" s="T242">Amnəbi, </ts>
               <ts e="T244" id="Seg_3734" n="e" s="T243">dĭgəttə </ts>
               <ts e="T245" id="Seg_3736" n="e" s="T244">dĭ </ts>
               <ts e="T246" id="Seg_3738" n="e" s="T245">deʔpi. </ts>
               <ts e="T247" id="Seg_3740" n="e" s="T246">"Kanaʔ! </ts>
               <ts e="T248" id="Seg_3742" n="e" s="T247">Tuj </ts>
               <ts e="T249" id="Seg_3744" n="e" s="T248">bar </ts>
               <ts e="T250" id="Seg_3746" n="e" s="T249">il </ts>
               <ts e="T251" id="Seg_3748" n="e" s="T250">kunollaʔbəʔjə. </ts>
               <ts e="T252" id="Seg_3750" n="e" s="T251">Iʔ </ts>
               <ts e="T253" id="Seg_3752" n="e" s="T252">dĭ </ts>
               <ts e="T254" id="Seg_3754" n="e" s="T253">süjö! </ts>
               <ts e="T255" id="Seg_3756" n="e" s="T254">A </ts>
               <ts e="T256" id="Seg_3758" n="e" s="T255">klʼetkabə </ts>
               <ts e="T257" id="Seg_3760" n="e" s="T256">iʔ </ts>
               <ts e="T258" id="Seg_3762" n="e" s="T257">it, </ts>
               <ts e="T260" id="Seg_3764" n="e" s="T258">a_to…" </ts>
               <ts e="T261" id="Seg_3766" n="e" s="T260">Dĭgəttə </ts>
               <ts e="T262" id="Seg_3768" n="e" s="T261">dĭ </ts>
               <ts e="T263" id="Seg_3770" n="e" s="T262">kambi. </ts>
               <ts e="T264" id="Seg_3772" n="e" s="T263">Ibi </ts>
               <ts e="T265" id="Seg_3774" n="e" s="T264">süjö. </ts>
               <ts e="T266" id="Seg_3776" n="e" s="T265">Măndərlia. </ts>
               <ts e="T267" id="Seg_3778" n="e" s="T266">Klʼetka </ts>
               <ts e="T268" id="Seg_3780" n="e" s="T267">kuvas, </ts>
               <ts e="T269" id="Seg_3782" n="e" s="T268">nada </ts>
               <ts e="T270" id="Seg_3784" n="e" s="T269">izittə. </ts>
               <ts e="T271" id="Seg_3786" n="e" s="T270">Kak </ts>
               <ts e="T272" id="Seg_3788" n="e" s="T271">dĭm </ts>
               <ts e="T273" id="Seg_3790" n="e" s="T272">mĭngəluʔpi </ts>
               <ts e="T274" id="Seg_3792" n="e" s="T273">bar </ts>
               <ts e="T275" id="Seg_3794" n="e" s="T274">kuzurluʔpi </ts>
               <ts e="T276" id="Seg_3796" n="e" s="T275">bar. </ts>
               <ts e="T277" id="Seg_3798" n="e" s="T276">Dĭm </ts>
               <ts e="T278" id="Seg_3800" n="e" s="T277">dʼabəluʔpi. </ts>
               <ts e="T279" id="Seg_3802" n="e" s="T278">I </ts>
               <ts e="T280" id="Seg_3804" n="e" s="T279">(kundlaʔ- </ts>
               <ts e="T281" id="Seg_3806" n="e" s="T280">kun-) </ts>
               <ts e="T282" id="Seg_3808" n="e" s="T281">kumbiʔi </ts>
               <ts e="T283" id="Seg_3810" n="e" s="T282">koŋdə. </ts>
               <ts e="T284" id="Seg_3812" n="e" s="T283">Kabarləj. </ts>
               <ts e="T285" id="Seg_3814" n="e" s="T284">Dĭgəttə </ts>
               <ts e="T286" id="Seg_3816" n="e" s="T285">dĭ </ts>
               <ts e="T287" id="Seg_3818" n="e" s="T286">šobi </ts>
               <ts e="T288" id="Seg_3820" n="e" s="T287">koŋdə. </ts>
               <ts e="T289" id="Seg_3822" n="e" s="T288">"Tăn </ts>
               <ts e="T290" id="Seg_3824" n="e" s="T289">(ĭmbi=) </ts>
               <ts e="T291" id="Seg_3826" n="e" s="T290">ĭmbi </ts>
               <ts e="T292" id="Seg_3828" n="e" s="T291">döbər </ts>
               <ts e="T293" id="Seg_3830" n="e" s="T292">šobiam, </ts>
               <ts e="T294" id="Seg_3832" n="e" s="T293">tojirzittə. </ts>
               <ts e="T295" id="Seg_3834" n="e" s="T294">Šindin </ts>
               <ts e="T296" id="Seg_3836" n="e" s="T295">tăn </ts>
               <ts e="T297" id="Seg_3838" n="e" s="T296">nʼi?" </ts>
               <ts e="T298" id="Seg_3840" n="e" s="T297">"Măn </ts>
               <ts e="T299" id="Seg_3842" n="e" s="T298">Ivan_Tsarevič". </ts>
               <ts e="T300" id="Seg_3844" n="e" s="T299">"Dăk </ts>
               <ts e="T301" id="Seg_3846" n="e" s="T300">tăn </ts>
               <ts e="T302" id="Seg_3848" n="e" s="T301">tojirzittə </ts>
               <ts e="T303" id="Seg_3850" n="e" s="T302">zăxatel?" </ts>
               <ts e="T304" id="Seg_3852" n="e" s="T303">Dĭgəttə </ts>
               <ts e="T305" id="Seg_3854" n="e" s="T304">dĭ </ts>
               <ts e="T306" id="Seg_3856" n="e" s="T305">măndə. </ts>
               <ts e="T307" id="Seg_3858" n="e" s="T306">"A </ts>
               <ts e="T308" id="Seg_3860" n="e" s="T307">dĭ </ts>
               <ts e="T309" id="Seg_3862" n="e" s="T308">miʔnʼibeʔ </ts>
               <ts e="T310" id="Seg_3864" n="e" s="T309">mĭmbi </ts>
               <ts e="T311" id="Seg_3866" n="e" s="T310">da </ts>
               <ts e="T312" id="Seg_3868" n="e" s="T311">jabloki </ts>
               <ts e="T313" id="Seg_3870" n="e" s="T312">ambiʔi". </ts>
               <ts e="T314" id="Seg_3872" n="e" s="T313">"A </ts>
               <ts e="T315" id="Seg_3874" n="e" s="T314">tăn </ts>
               <ts e="T316" id="Seg_3876" n="e" s="T315">šobial </ts>
               <ts e="T317" id="Seg_3878" n="e" s="T316">bɨ, </ts>
               <ts e="T318" id="Seg_3880" n="e" s="T317">a </ts>
               <ts e="T319" id="Seg_3882" n="e" s="T318">măn </ts>
               <ts e="T320" id="Seg_3884" n="e" s="T319">dăre </ts>
               <ts e="T321" id="Seg_3886" n="e" s="T320">tănan </ts>
               <ts e="T322" id="Seg_3888" n="e" s="T321">mĭbiem </ts>
               <ts e="T323" id="Seg_3890" n="e" s="T322">bɨ, </ts>
               <ts e="T324" id="Seg_3892" n="e" s="T323">a_to </ts>
               <ts e="T325" id="Seg_3894" n="e" s="T324">tojirzittə </ts>
               <ts e="T326" id="Seg_3896" n="e" s="T325">šobial. </ts>
               <ts e="T327" id="Seg_3898" n="e" s="T326">No, </ts>
               <ts e="T328" id="Seg_3900" n="e" s="T327">măn </ts>
               <ts e="T329" id="Seg_3902" n="e" s="T328">tănan </ts>
               <ts e="T330" id="Seg_3904" n="e" s="T329">ĭmbidə </ts>
               <ts e="T331" id="Seg_3906" n="e" s="T330">ej </ts>
               <ts e="T332" id="Seg_3908" n="e" s="T331">alam, </ts>
               <ts e="T333" id="Seg_3910" n="e" s="T332">kanaʔ, </ts>
               <ts e="T334" id="Seg_3912" n="e" s="T333">ine </ts>
               <ts e="T335" id="Seg_3914" n="e" s="T334">deʔ </ts>
               <ts e="T336" id="Seg_3916" n="e" s="T335">zălatoj </ts>
               <ts e="T337" id="Seg_3918" n="e" s="T336">grivazʼiʔ". </ts>
               <ts e="T338" id="Seg_3920" n="e" s="T337">Dĭgəttə </ts>
               <ts e="T339" id="Seg_3922" n="e" s="T338">dĭ </ts>
               <ts e="T340" id="Seg_3924" n="e" s="T339">kambi </ts>
               <ts e="T341" id="Seg_3926" n="e" s="T340">volktə. </ts>
               <ts e="T342" id="Seg_3928" n="e" s="T341">Volk </ts>
               <ts e="T343" id="Seg_3930" n="e" s="T342">măndə: </ts>
               <ts e="T344" id="Seg_3932" n="e" s="T343">"Măn </ts>
               <ts e="T345" id="Seg_3934" n="e" s="T344">mămbiam </ts>
               <ts e="T346" id="Seg_3936" n="e" s="T345">tănan, </ts>
               <ts e="T347" id="Seg_3938" n="e" s="T346">iʔ </ts>
               <ts e="T348" id="Seg_3940" n="e" s="T347">üžəmeʔ. </ts>
               <ts e="T349" id="Seg_3942" n="e" s="T348">A </ts>
               <ts e="T350" id="Seg_3944" n="e" s="T349">tăn </ts>
               <ts e="T351" id="Seg_3946" n="e" s="T350">(üžəmbiel), </ts>
               <ts e="T352" id="Seg_3948" n="e" s="T351">a </ts>
               <ts e="T353" id="Seg_3950" n="e" s="T352">tüj </ts>
               <ts e="T354" id="Seg_3952" n="e" s="T353">((PAUSE)) </ts>
               <ts e="T355" id="Seg_3954" n="e" s="T354">amnaʔ </ts>
               <ts e="T356" id="Seg_3956" n="e" s="T355">măna, </ts>
               <ts e="T357" id="Seg_3958" n="e" s="T356">kanžəbəj". </ts>
               <ts e="T358" id="Seg_3960" n="e" s="T357">Kambiʔi. </ts>
               <ts e="T359" id="Seg_3962" n="e" s="T358">No, </ts>
               <ts e="T360" id="Seg_3964" n="e" s="T359">šobiʔi </ts>
               <ts e="T361" id="Seg_3966" n="e" s="T360">dibər. </ts>
               <ts e="T362" id="Seg_3968" n="e" s="T361">Dĭgəttə: </ts>
               <ts e="T363" id="Seg_3970" n="e" s="T362">"No, </ts>
               <ts e="T364" id="Seg_3972" n="e" s="T363">kanaʔ! </ts>
               <ts e="T365" id="Seg_3974" n="e" s="T364">Da </ts>
               <ts e="T366" id="Seg_3976" n="e" s="T365">dʼabit </ts>
               <ts e="T367" id="Seg_3978" n="e" s="T366">inem, </ts>
               <ts e="T368" id="Seg_3980" n="e" s="T367">a </ts>
               <ts e="T369" id="Seg_3982" n="e" s="T368">uzdam </ts>
               <ts e="T370" id="Seg_3984" n="e" s="T369">(ej= </ts>
               <ts e="T371" id="Seg_3986" n="e" s="T370">uzu- </ts>
               <ts e="T372" id="Seg_3988" n="e" s="T371">iʔ= </ts>
               <ts e="T373" id="Seg_3990" n="e" s="T372">užum- </ts>
               <ts e="T374" id="Seg_3992" n="e" s="T373">iʔ </ts>
               <ts e="T375" id="Seg_3994" n="e" s="T374">u-) </ts>
               <ts e="T376" id="Seg_3996" n="e" s="T375">iʔ </ts>
               <ts e="T377" id="Seg_3998" n="e" s="T376">iʔ! </ts>
               <ts e="T378" id="Seg_4000" n="e" s="T377">A_to </ts>
               <ts e="T379" id="Seg_4002" n="e" s="T378">bazoʔ </ts>
               <ts e="T380" id="Seg_4004" n="e" s="T379">dăre </ts>
               <ts e="T381" id="Seg_4006" n="e" s="T380">molaːlləj!" </ts>
               <ts e="T382" id="Seg_4008" n="e" s="T381">Dĭ </ts>
               <ts e="T383" id="Seg_4010" n="e" s="T382">šobi. </ts>
               <ts e="T384" id="Seg_4012" n="e" s="T383">Inem </ts>
               <ts e="T385" id="Seg_4014" n="e" s="T384">ibi. </ts>
               <ts e="T386" id="Seg_4016" n="e" s="T385">A </ts>
               <ts e="T387" id="Seg_4018" n="e" s="T386">uzdat </ts>
               <ts e="T388" id="Seg_4020" n="e" s="T387">ugaːndə </ts>
               <ts e="T389" id="Seg_4022" n="e" s="T388">kuvas. </ts>
               <ts e="T390" id="Seg_4024" n="e" s="T389">Ĭmbi </ts>
               <ts e="T391" id="Seg_4026" n="e" s="T390">inegən, </ts>
               <ts e="T392" id="Seg_4028" n="e" s="T391">nada </ts>
               <ts e="T393" id="Seg_4030" n="e" s="T392">šerzittə </ts>
               <ts e="T394" id="Seg_4032" n="e" s="T393">tolʼko. </ts>
               <ts e="T395" id="Seg_4034" n="e" s="T394">Ibi </ts>
               <ts e="T396" id="Seg_4036" n="e" s="T395">dĭm. </ts>
               <ts e="T397" id="Seg_4038" n="e" s="T396">Dĭ </ts>
               <ts e="T398" id="Seg_4040" n="e" s="T397">bar </ts>
               <ts e="T399" id="Seg_4042" n="e" s="T398">kirgarluʔpi. </ts>
               <ts e="T400" id="Seg_4044" n="e" s="T399">Il </ts>
               <ts e="T401" id="Seg_4046" n="e" s="T400">suʔmiluʔpi, </ts>
               <ts e="T402" id="Seg_4048" n="e" s="T401">dĭm </ts>
               <ts e="T403" id="Seg_4050" n="e" s="T402">ibiʔi. </ts>
               <ts e="T404" id="Seg_4052" n="e" s="T403">(Kam-) </ts>
               <ts e="T405" id="Seg_4054" n="e" s="T404">Kunnaːmbiʔi </ts>
               <ts e="T406" id="Seg_4056" n="e" s="T405">koŋdə. </ts>
               <ts e="T407" id="Seg_4058" n="e" s="T406">"Ĭmbi </ts>
               <ts e="T408" id="Seg_4060" n="e" s="T407">šobial </ts>
               <ts e="T409" id="Seg_4062" n="e" s="T408">ine </ts>
               <ts e="T410" id="Seg_4064" n="e" s="T409">tojirzittə? </ts>
               <ts e="T411" id="Seg_4066" n="e" s="T410">Šindin </ts>
               <ts e="T412" id="Seg_4068" n="e" s="T411">tăn?" </ts>
               <ts e="T413" id="Seg_4070" n="e" s="T412">"(Măn=) </ts>
               <ts e="T414" id="Seg_4072" n="e" s="T413">Măn </ts>
               <ts e="T415" id="Seg_4074" n="e" s="T414">Ivan_Tsarevič". </ts>
               <ts e="T416" id="Seg_4076" n="e" s="T415">"Dăk, </ts>
               <ts e="T417" id="Seg_4078" n="e" s="T416">tojirzittə </ts>
               <ts e="T418" id="Seg_4080" n="e" s="T417">(s-) </ts>
               <ts e="T419" id="Seg_4082" n="e" s="T418">šobial? </ts>
               <ts e="T420" id="Seg_4084" n="e" s="T419">No, </ts>
               <ts e="T421" id="Seg_4086" n="e" s="T420">kanaʔ, </ts>
               <ts e="T422" id="Seg_4088" n="e" s="T421">măn </ts>
               <ts e="T423" id="Seg_4090" n="e" s="T422">tănan </ts>
               <ts e="T424" id="Seg_4092" n="e" s="T423">ĭmbidə </ts>
               <ts e="T425" id="Seg_4094" n="e" s="T424">ej </ts>
               <ts e="T426" id="Seg_4096" n="e" s="T425">alam. </ts>
               <ts e="T427" id="Seg_4098" n="e" s="T426">Dĭn </ts>
               <ts e="T428" id="Seg_4100" n="e" s="T427">ige </ts>
               <ts e="T429" id="Seg_4102" n="e" s="T428">koʔbdo </ts>
               <ts e="T430" id="Seg_4104" n="e" s="T429">Ilena </ts>
               <ts e="T431" id="Seg_4106" n="e" s="T430">Prĭkrasnaja. </ts>
               <ts e="T432" id="Seg_4108" n="e" s="T431">Dettə </ts>
               <ts e="T433" id="Seg_4110" n="e" s="T432">măna </ts>
               <ts e="T434" id="Seg_4112" n="e" s="T433">dĭm!" </ts>
               <ts e="T435" id="Seg_4114" n="e" s="T434">Dĭgəttə </ts>
               <ts e="T436" id="Seg_4116" n="e" s="T435">dĭ </ts>
               <ts e="T437" id="Seg_4118" n="e" s="T436">(šo-) </ts>
               <ts e="T438" id="Seg_4120" n="e" s="T437">šonəga. </ts>
               <ts e="T439" id="Seg_4122" n="e" s="T438">A </ts>
               <ts e="T440" id="Seg_4124" n="e" s="T439">volktə: </ts>
               <ts e="T441" id="Seg_4126" n="e" s="T440">"Mămbiam: </ts>
               <ts e="T442" id="Seg_4128" n="e" s="T441">iʔ </ts>
               <ts e="T443" id="Seg_4130" n="e" s="T442">üžəmeʔ, </ts>
               <ts e="T444" id="Seg_4132" n="e" s="T443">a </ts>
               <ts e="T445" id="Seg_4134" n="e" s="T444">tăn </ts>
               <ts e="T446" id="Seg_4136" n="e" s="T445">üge </ts>
               <ts e="T447" id="Seg_4138" n="e" s="T446">üžəmneʔpiem". </ts>
               <ts e="T448" id="Seg_4140" n="e" s="T447">No, </ts>
               <ts e="T449" id="Seg_4142" n="e" s="T448">ĭmbidə </ts>
               <ts e="T450" id="Seg_4144" n="e" s="T449">(emnajaʔa). </ts>
               <ts e="T451" id="Seg_4146" n="e" s="T450">Dĭ </ts>
               <ts e="T452" id="Seg_4148" n="e" s="T451">măndə: </ts>
               <ts e="T453" id="Seg_4150" n="e" s="T452">"No, </ts>
               <ts e="T454" id="Seg_4152" n="e" s="T453">amnaʔ, </ts>
               <ts e="T455" id="Seg_4154" n="e" s="T454">kanžəbəj!" </ts>
               <ts e="T456" id="Seg_4156" n="e" s="T455">Šobiʔi </ts>
               <ts e="T457" id="Seg_4158" n="e" s="T456">dibər. </ts>
               <ts e="T458" id="Seg_4160" n="e" s="T457">Dĭgəttə </ts>
               <ts e="T459" id="Seg_4162" n="e" s="T458">volk </ts>
               <ts e="T460" id="Seg_4164" n="e" s="T459">măndə </ts>
               <ts e="T461" id="Seg_4166" n="e" s="T460">dĭʔnə: </ts>
               <ts e="T462" id="Seg_4168" n="e" s="T461">"Tăn </ts>
               <ts e="T463" id="Seg_4170" n="e" s="T462">(am-) </ts>
               <ts e="T464" id="Seg_4172" n="e" s="T463">kanaʔ, </ts>
               <ts e="T465" id="Seg_4174" n="e" s="T464">paraʔ, </ts>
               <ts e="T466" id="Seg_4176" n="e" s="T465">paraʔ. </ts>
               <ts e="T467" id="Seg_4178" n="e" s="T466">A </ts>
               <ts e="T468" id="Seg_4180" n="e" s="T467">măn </ts>
               <ts e="T469" id="Seg_4182" n="e" s="T468">kallam </ts>
               <ts e="T470" id="Seg_4184" n="e" s="T469">bostə". </ts>
               <ts e="T471" id="Seg_4186" n="e" s="T470">Dĭgəttə </ts>
               <ts e="T472" id="Seg_4188" n="e" s="T471">suʔməluʔpi </ts>
               <ts e="T473" id="Seg_4190" n="e" s="T472">sattə. </ts>
               <ts e="T474" id="Seg_4192" n="e" s="T473">Dĭn </ts>
               <ts e="T475" id="Seg_4194" n="e" s="T474">šaʔlaːmbi. </ts>
               <ts e="T476" id="Seg_4196" n="e" s="T475">Dĭzeŋ </ts>
               <ts e="T477" id="Seg_4198" n="e" s="T476">mĭnzəleʔbəʔjə, </ts>
               <ts e="T478" id="Seg_4200" n="e" s="T477">ijat, </ts>
               <ts e="T479" id="Seg_4202" n="e" s="T478">nʼanʼkaʔi. </ts>
               <ts e="T480" id="Seg_4204" n="e" s="T479">Dĭ </ts>
               <ts e="T481" id="Seg_4206" n="e" s="T480">Elenat </ts>
               <ts e="T482" id="Seg_4208" n="e" s="T481">maluʔpi. </ts>
               <ts e="T483" id="Seg_4210" n="e" s="T482">Dĭ </ts>
               <ts e="T484" id="Seg_4212" n="e" s="T483">dĭm </ts>
               <ts e="T485" id="Seg_4214" n="e" s="T484">kabarluʔpi </ts>
               <ts e="T486" id="Seg_4216" n="e" s="T485">i </ts>
               <ts e="T487" id="Seg_4218" n="e" s="T486">nuʔməluʔpi, </ts>
               <ts e="T488" id="Seg_4220" n="e" s="T487">vot </ts>
               <ts e="T489" id="Seg_4222" n="e" s="T488">nuʔməlaʔbə </ts>
               <ts e="T490" id="Seg_4224" n="e" s="T489">i </ts>
               <ts e="T491" id="Seg_4226" n="e" s="T490">bĭdəbi </ts>
               <ts e="T492" id="Seg_4228" n="e" s="T491">Ivanəm. </ts>
               <ts e="T493" id="Seg_4230" n="e" s="T492">"Amnaʔ!" </ts>
               <ts e="T494" id="Seg_4232" n="e" s="T493">Amnəlbiʔi. </ts>
               <ts e="T495" id="Seg_4234" n="e" s="T494">Dĭgəttə </ts>
               <ts e="T496" id="Seg_4236" n="e" s="T495">šobiʔi </ts>
               <ts e="T497" id="Seg_4238" n="e" s="T496">dibər, </ts>
               <ts e="T498" id="Seg_4240" n="e" s="T497">gijen </ts>
               <ts e="T499" id="Seg_4242" n="e" s="T498">(in-) </ts>
               <ts e="T500" id="Seg_4244" n="e" s="T499">ine </ts>
               <ts e="T501" id="Seg_4246" n="e" s="T500">pʼebiʔi. </ts>
               <ts e="T502" id="Seg_4248" n="e" s="T501">Dĭgəttə </ts>
               <ts e="T503" id="Seg_4250" n="e" s="T502">dĭzeŋ </ts>
               <ts e="T504" id="Seg_4252" n="e" s="T503">šobiʔi, </ts>
               <ts e="T505" id="Seg_4254" n="e" s="T504">a </ts>
               <ts e="T506" id="Seg_4256" n="e" s="T505">dĭ, </ts>
               <ts e="T507" id="Seg_4258" n="e" s="T506">tenöleʔbə </ts>
               <ts e="T508" id="Seg_4260" n="e" s="T507">üge, </ts>
               <ts e="T509" id="Seg_4262" n="e" s="T508">Ivan_Tsarevič. </ts>
               <ts e="T510" id="Seg_4264" n="e" s="T509">Ajiria </ts>
               <ts e="T511" id="Seg_4266" n="e" s="T510">dĭ </ts>
               <ts e="T512" id="Seg_4268" n="e" s="T511">koʔbdom. </ts>
               <ts e="T513" id="Seg_4270" n="e" s="T512">A </ts>
               <ts e="T514" id="Seg_4272" n="e" s="T513">volktə </ts>
               <ts e="T515" id="Seg_4274" n="e" s="T514">surarlaʔbə: </ts>
               <ts e="T516" id="Seg_4276" n="e" s="T515">"Ĭmbi </ts>
               <ts e="T517" id="Seg_4278" n="e" s="T516">tăn </ts>
               <ts e="T518" id="Seg_4280" n="e" s="T517">dĭn </ts>
               <ts e="T519" id="Seg_4282" n="e" s="T518">nulaʔbəl?" </ts>
               <ts e="T520" id="Seg_4284" n="e" s="T519">"Dĭ </ts>
               <ts e="T521" id="Seg_4286" n="e" s="T520">dĭrgit </ts>
               <ts e="T522" id="Seg_4288" n="e" s="T521">kuvas </ts>
               <ts e="T523" id="Seg_4290" n="e" s="T522">koʔbdo. </ts>
               <ts e="T524" id="Seg_4292" n="e" s="T523">Măn </ts>
               <ts e="T525" id="Seg_4294" n="e" s="T524">ajirbiom, </ts>
               <ts e="T526" id="Seg_4296" n="e" s="T525">(mĭzen-) </ts>
               <ts e="T527" id="Seg_4298" n="e" s="T526">mĭzittə". </ts>
               <ts e="T528" id="Seg_4300" n="e" s="T527">"No, </ts>
               <ts e="T529" id="Seg_4302" n="e" s="T528">šaʔbdəlbəj, </ts>
               <ts e="T530" id="Seg_4304" n="e" s="T529">a </ts>
               <ts e="T531" id="Seg_4306" n="e" s="T530">(măn </ts>
               <ts e="T532" id="Seg_4308" n="e" s="T531">alaʔ-) </ts>
               <ts e="T533" id="Seg_4310" n="e" s="T532">măn </ts>
               <ts e="T534" id="Seg_4312" n="e" s="T533">alaʔbəm </ts>
               <ts e="T535" id="Seg_4314" n="e" s="T534">(dĭ-) </ts>
               <ts e="T536" id="Seg_4316" n="e" s="T535">dĭʔnə". </ts>
               <ts e="T537" id="Seg_4318" n="e" s="T536">Dĭgəttə </ts>
               <ts e="T538" id="Seg_4320" n="e" s="T537">bostə </ts>
               <ts e="T539" id="Seg_4322" n="e" s="T538">bögəldə </ts>
               <ts e="T540" id="Seg_4324" n="e" s="T539">suʔməluʔpi. </ts>
               <ts e="T541" id="Seg_4326" n="e" s="T540">Dĭ </ts>
               <ts e="T542" id="Seg_4328" n="e" s="T541">dĭm </ts>
               <ts e="T543" id="Seg_4330" n="e" s="T542">ibi, </ts>
               <ts e="T544" id="Seg_4332" n="e" s="T543">kunnaːmbi. </ts>
               <ts e="T545" id="Seg_4334" n="e" s="T544">A </ts>
               <ts e="T546" id="Seg_4336" n="e" s="T545">dĭ </ts>
               <ts e="T547" id="Seg_4338" n="e" s="T546">ibi </ts>
               <ts e="T548" id="Seg_4340" n="e" s="T547">dĭ </ts>
               <ts e="T549" id="Seg_4342" n="e" s="T548">koʔbdom. </ts>
               <ts e="T550" id="Seg_4344" n="e" s="T549">(Dʼansaʔməj) </ts>
               <ts e="T551" id="Seg_4346" n="e" s="T550">ara </ts>
               <ts e="T552" id="Seg_4348" n="e" s="T551">bĭʔpiʔi, </ts>
               <ts e="T553" id="Seg_4350" n="e" s="T552">suʔmileʔbəʔi, </ts>
               <ts e="T554" id="Seg_4352" n="e" s="T553">inebə </ts>
               <ts e="T555" id="Seg_4354" n="e" s="T554">mĭbi. </ts>
               <ts e="T556" id="Seg_4356" n="e" s="T555">Dĭ </ts>
               <ts e="T557" id="Seg_4358" n="e" s="T556">inebə </ts>
               <ts e="T558" id="Seg_4360" n="e" s="T557">ibi. </ts>
               <ts e="T559" id="Seg_4362" n="e" s="T558">Elenabə </ts>
               <ts e="T560" id="Seg_4364" n="e" s="T559">amnəlbəbi, </ts>
               <ts e="T561" id="Seg_4366" n="e" s="T560">bostə </ts>
               <ts e="T562" id="Seg_4368" n="e" s="T561">amnəlbi. </ts>
               <ts e="T563" id="Seg_4370" n="e" s="T562">Kambiʔi. </ts>
               <ts e="T564" id="Seg_4372" n="e" s="T563">A </ts>
               <ts e="T565" id="Seg_4374" n="e" s="T564">nüdʼin </ts>
               <ts e="T566" id="Seg_4376" n="e" s="T565">(iʔbibi-) </ts>
               <ts e="T567" id="Seg_4378" n="e" s="T566">iʔbəʔi </ts>
               <ts e="T568" id="Seg_4380" n="e" s="T567">kunolzittə. </ts>
               <ts e="T569" id="Seg_4382" n="e" s="T568">(Dĭ= </ts>
               <ts e="T570" id="Seg_4384" n="e" s="T569">uʔ-) </ts>
               <ts e="T571" id="Seg_4386" n="e" s="T570">Dĭ </ts>
               <ts e="T572" id="Seg_4388" n="e" s="T571">Elenat </ts>
               <ts e="T573" id="Seg_4390" n="e" s="T572">(mo-) </ts>
               <ts e="T574" id="Seg_4392" n="e" s="T573">molaːmbi </ts>
               <ts e="T575" id="Seg_4394" n="e" s="T574">volkziʔ. </ts>
               <ts e="T576" id="Seg_4396" n="e" s="T575">I </ts>
               <ts e="T577" id="Seg_4398" n="e" s="T576">dĭ </ts>
               <ts e="T578" id="Seg_4400" n="e" s="T577">nerölüʔpi </ts>
               <ts e="T579" id="Seg_4402" n="e" s="T578">i </ts>
               <ts e="T580" id="Seg_4404" n="e" s="T579">saʔməluʔpi. </ts>
               <ts e="T581" id="Seg_4406" n="e" s="T580">A </ts>
               <ts e="T582" id="Seg_4408" n="e" s="T581">dĭ </ts>
               <ts e="T583" id="Seg_4410" n="e" s="T582">nuʔməluʔpi. </ts>
               <ts e="T584" id="Seg_4412" n="e" s="T583">Dĭgəttə </ts>
               <ts e="T585" id="Seg_4414" n="e" s="T584">bĭdəliet </ts>
               <ts e="T586" id="Seg_4416" n="e" s="T585">Ivanuška </ts>
               <ts e="T587" id="Seg_4418" n="e" s="T586">(Elenuškaʔiziʔ). </ts>
               <ts e="T588" id="Seg_4420" n="e" s="T587">"Ĭmbi </ts>
               <ts e="T589" id="Seg_4422" n="e" s="T588">tăn </ts>
               <ts e="T590" id="Seg_4424" n="e" s="T589">tĭn </ts>
               <ts e="T591" id="Seg_4426" n="e" s="T590">nulaʔliel?" </ts>
               <ts e="T592" id="Seg_4428" n="e" s="T591">"Da </ts>
               <ts e="T593" id="Seg_4430" n="e" s="T592">nʼe </ts>
               <ts e="T594" id="Seg_4432" n="e" s="T593">axota </ts>
               <ts e="T595" id="Seg_4434" n="e" s="T594">mĭzittə </ts>
               <ts e="T596" id="Seg_4436" n="e" s="T595">ine, </ts>
               <ts e="T597" id="Seg_4438" n="e" s="T596">kuvas". </ts>
               <ts e="T598" id="Seg_4440" n="e" s="T597">A </ts>
               <ts e="T599" id="Seg_4442" n="e" s="T598">dĭ </ts>
               <ts e="T600" id="Seg_4444" n="e" s="T599">măndə: </ts>
               <ts e="T601" id="Seg_4446" n="e" s="T600">"Šaʔlaːndə </ts>
               <ts e="T602" id="Seg_4448" n="e" s="T601">inem </ts>
               <ts e="T603" id="Seg_4450" n="e" s="T602">i </ts>
               <ts e="T604" id="Seg_4452" n="e" s="T603">koʔbdom". </ts>
               <ts e="T605" id="Seg_4454" n="e" s="T604">Dĭgəttə </ts>
               <ts e="T606" id="Seg_4456" n="e" s="T605">dĭ </ts>
               <ts e="T607" id="Seg_4458" n="e" s="T606">aluʔpi </ts>
               <ts e="T608" id="Seg_4460" n="e" s="T607">ineziʔ, </ts>
               <ts e="T609" id="Seg_4462" n="e" s="T608">dĭ </ts>
               <ts e="T610" id="Seg_4464" n="e" s="T609">dĭm </ts>
               <ts e="T611" id="Seg_4466" n="e" s="T610">kumbi. </ts>
               <ts e="T612" id="Seg_4468" n="e" s="T611">Dĭ </ts>
               <ts e="T613" id="Seg_4470" n="e" s="T612">ibi </ts>
               <ts e="T614" id="Seg_4472" n="e" s="T613">süjö </ts>
               <ts e="T615" id="Seg_4474" n="e" s="T614">(nenʼinuʔ), </ts>
               <ts e="T616" id="Seg_4476" n="e" s="T615">(kak) </ts>
               <ts e="T617" id="Seg_4478" n="e" s="T616">šü. </ts>
               <ts e="T618" id="Seg_4480" n="e" s="T617">I </ts>
               <ts e="T619" id="Seg_4482" n="e" s="T618">bostə </ts>
               <ts e="T620" id="Seg_4484" n="e" s="T619">parluʔpi, </ts>
               <ts e="T621" id="Seg_4486" n="e" s="T620">amnəlbi. </ts>
               <ts e="T622" id="Seg_4488" n="e" s="T621">I </ts>
               <ts e="T623" id="Seg_4490" n="e" s="T622">koʔbdobə </ts>
               <ts e="T624" id="Seg_4492" n="e" s="T623">amnəlbi </ts>
               <ts e="T625" id="Seg_4494" n="e" s="T624">inenə, </ts>
               <ts e="T626" id="Seg_4496" n="e" s="T625">bostə </ts>
               <ts e="T627" id="Seg_4498" n="e" s="T626">amnəlbi, </ts>
               <ts e="T628" id="Seg_4500" n="e" s="T627">kambiʔi. </ts>
               <ts e="T629" id="Seg_4502" n="e" s="T628">Dĭgəttə </ts>
               <ts e="T630" id="Seg_4504" n="e" s="T629">dĭ </ts>
               <ts e="T631" id="Seg_4506" n="e" s="T630">koŋ: </ts>
               <ts e="T632" id="Seg_4508" n="e" s="T631">"Da </ts>
               <ts e="T633" id="Seg_4510" n="e" s="T632">deʔkeʔ </ts>
               <ts e="T634" id="Seg_4512" n="e" s="T633">dĭ </ts>
               <ts e="T635" id="Seg_4514" n="e" s="T634">ine!" </ts>
               <ts e="T636" id="Seg_4516" n="e" s="T635">Dĭ </ts>
               <ts e="T637" id="Seg_4518" n="e" s="T636">ine </ts>
               <ts e="T638" id="Seg_4520" n="e" s="T637">šobi, </ts>
               <ts e="T639" id="Seg_4522" n="e" s="T638">volkziʔ </ts>
               <ts e="T640" id="Seg_4524" n="e" s="T639">aluʔpi. </ts>
               <ts e="T641" id="Seg_4526" n="e" s="T640">Dĭ </ts>
               <ts e="T642" id="Seg_4528" n="e" s="T641">neröluʔpi </ts>
               <ts e="T643" id="Seg_4530" n="e" s="T642">i </ts>
               <ts e="T644" id="Seg_4532" n="e" s="T643">saʔməluʔpi, </ts>
               <ts e="T645" id="Seg_4534" n="e" s="T644">a </ts>
               <ts e="T646" id="Seg_4536" n="e" s="T645">dĭ </ts>
               <ts e="T647" id="Seg_4538" n="e" s="T646">nuʔməluʔpi. </ts>
               <ts e="T648" id="Seg_4540" n="e" s="T647">Bĭdəleʔbə, </ts>
               <ts e="T649" id="Seg_4542" n="e" s="T648">Vanʼuškanə </ts>
               <ts e="T650" id="Seg_4544" n="e" s="T649">măndə: </ts>
               <ts e="T651" id="Seg_4546" n="e" s="T650">"No, </ts>
               <ts e="T652" id="Seg_4548" n="e" s="T651">tüj </ts>
               <ts e="T653" id="Seg_4550" n="e" s="T652">măna </ts>
               <ts e="T654" id="Seg_4552" n="e" s="T653">kanzittə </ts>
               <ts e="T655" id="Seg_4554" n="e" s="T654">tănziʔ </ts>
               <ts e="T656" id="Seg_4556" n="e" s="T655">nʼekuda, </ts>
               <ts e="T657" id="Seg_4558" n="e" s="T656">maləm </ts>
               <ts e="T658" id="Seg_4560" n="e" s="T657">dön". </ts>
               <ts e="T659" id="Seg_4562" n="e" s="T658">Dĭ </ts>
               <ts e="T660" id="Seg_4564" n="e" s="T659">suʔmiluʔpi, </ts>
               <ts e="T661" id="Seg_4566" n="e" s="T660">dĭʔnə </ts>
               <ts e="T662" id="Seg_4568" n="e" s="T661">nagurgöʔ </ts>
               <ts e="T663" id="Seg_4570" n="e" s="T662">dʼünə </ts>
               <ts e="T664" id="Seg_4572" n="e" s="T663">saʔməluʔpi. </ts>
               <ts e="T665" id="Seg_4574" n="e" s="T664">Kabarləj. </ts>
               <ts e="T666" id="Seg_4576" n="e" s="T665">Dĭgəttə </ts>
               <ts e="T667" id="Seg_4578" n="e" s="T666">(dĭ-) </ts>
               <ts e="T668" id="Seg_4580" n="e" s="T667">dĭzeŋ </ts>
               <ts e="T669" id="Seg_4582" n="e" s="T668">šobiʔi </ts>
               <ts e="T670" id="Seg_4584" n="e" s="T669">bostə </ts>
               <ts e="T671" id="Seg_4586" n="e" s="T670">(dʼü-) </ts>
               <ts e="T672" id="Seg_4588" n="e" s="T671">dʼünə. </ts>
               <ts e="T673" id="Seg_4590" n="e" s="T672">Măndə: </ts>
               <ts e="T674" id="Seg_4592" n="e" s="T673">"Davaj </ts>
               <ts e="T675" id="Seg_4594" n="e" s="T674">amorləbəj". </ts>
               <ts e="T676" id="Seg_4596" n="e" s="T675">Amnəbəlbiʔi, </ts>
               <ts e="T677" id="Seg_4598" n="e" s="T676">amorbiʔi. </ts>
               <ts e="T678" id="Seg_4600" n="e" s="T677">I </ts>
               <ts e="T679" id="Seg_4602" n="e" s="T678">(iʔbəʔi-) </ts>
               <ts e="T680" id="Seg_4604" n="e" s="T679">iʔpiʔi </ts>
               <ts e="T681" id="Seg_4606" n="e" s="T680">kunolzittə. </ts>
               <ts e="T682" id="Seg_4608" n="e" s="T681">Dĭgəttə </ts>
               <ts e="T683" id="Seg_4610" n="e" s="T682">kagazaŋdə </ts>
               <ts e="T684" id="Seg_4612" n="e" s="T683">šobiʔi. </ts>
               <ts e="T685" id="Seg_4614" n="e" s="T684">Dĭm </ts>
               <ts e="T686" id="Seg_4616" n="e" s="T685">kutlaːmbiʔi. </ts>
               <ts e="T687" id="Seg_4618" n="e" s="T686">A </ts>
               <ts e="T688" id="Seg_4620" n="e" s="T687">bostə </ts>
               <ts e="T689" id="Seg_4622" n="e" s="T688">ibiʔi </ts>
               <ts e="T690" id="Seg_4624" n="e" s="T689">koʔbdo. </ts>
               <ts e="T691" id="Seg_4626" n="e" s="T690">I </ts>
               <ts e="T692" id="Seg_4628" n="e" s="T691">süjö, </ts>
               <ts e="T693" id="Seg_4630" n="e" s="T692">i </ts>
               <ts e="T694" id="Seg_4632" n="e" s="T693">ine, </ts>
               <ts e="T695" id="Seg_4634" n="e" s="T694">kambi. </ts>
               <ts e="T696" id="Seg_4636" n="e" s="T695">Kandəlaʔbəʔjə. </ts>
               <ts e="T697" id="Seg_4638" n="e" s="T696">Dĭgəttə </ts>
               <ts e="T698" id="Seg_4640" n="e" s="T697">volk </ts>
               <ts e="T699" id="Seg_4642" n="e" s="T698">šobi, </ts>
               <ts e="T700" id="Seg_4644" n="e" s="T699">(kul-) </ts>
               <ts e="T701" id="Seg_4646" n="e" s="T700">kuliot: </ts>
               <ts e="T702" id="Seg_4648" n="e" s="T701">dĭ </ts>
               <ts e="T703" id="Seg_4650" n="e" s="T702">iʔbolaʔbə. </ts>
               <ts e="T704" id="Seg_4652" n="e" s="T703">Dʼaʔpi </ts>
               <ts e="T705" id="Seg_4654" n="e" s="T704">süjö, </ts>
               <ts e="T706" id="Seg_4656" n="e" s="T705">vărona, </ts>
               <ts e="T707" id="Seg_4658" n="e" s="T706">üdʼüge </ts>
               <ts e="T708" id="Seg_4660" n="e" s="T707">süjö. </ts>
               <ts e="T709" id="Seg_4662" n="e" s="T708">"Da </ts>
               <ts e="T710" id="Seg_4664" n="e" s="T709">deʔ, </ts>
               <ts e="T711" id="Seg_4666" n="e" s="T710">kübi </ts>
               <ts e="T712" id="Seg_4668" n="e" s="T711">bü </ts>
               <ts e="T713" id="Seg_4670" n="e" s="T712">i </ts>
               <ts e="T714" id="Seg_4672" n="e" s="T713">tʼili </ts>
               <ts e="T715" id="Seg_4674" n="e" s="T714">bü!" </ts>
               <ts e="T716" id="Seg_4676" n="e" s="T715">Dĭ </ts>
               <ts e="T717" id="Seg_4678" n="e" s="T716">süjö </ts>
               <ts e="T718" id="Seg_4680" n="e" s="T717">nʼergölüʔpi, </ts>
               <ts e="T719" id="Seg_4682" n="e" s="T718">deʔpi. </ts>
               <ts e="T720" id="Seg_4684" n="e" s="T719">Dĭ </ts>
               <ts e="T721" id="Seg_4686" n="e" s="T720">dĭm </ts>
               <ts e="T722" id="Seg_4688" n="e" s="T721">bazəbi </ts>
               <ts e="T723" id="Seg_4690" n="e" s="T722">(ku-) </ts>
               <ts e="T724" id="Seg_4692" n="e" s="T723">kule </ts>
               <ts e="T725" id="Seg_4694" n="e" s="T724">büzʼiʔ. </ts>
               <ts e="T726" id="Seg_4696" n="e" s="T725">I </ts>
               <ts e="T727" id="Seg_4698" n="e" s="T726">dĭgəttə </ts>
               <ts e="T728" id="Seg_4700" n="e" s="T727">(dʼilʼuj) </ts>
               <ts e="T729" id="Seg_4702" n="e" s="T728">büziʔ. </ts>
               <ts e="T730" id="Seg_4704" n="e" s="T729">Dĭ </ts>
               <ts e="T731" id="Seg_4706" n="e" s="T730">suʔmileʔbə: </ts>
               <ts e="T732" id="Seg_4708" n="e" s="T731">"Kondʼo </ts>
               <ts e="T733" id="Seg_4710" n="e" s="T732">măn </ts>
               <ts e="T734" id="Seg_4712" n="e" s="T733">kunolbiam". </ts>
               <ts e="T735" id="Seg_4714" n="e" s="T734">"Kondʼo </ts>
               <ts e="T736" id="Seg_4716" n="e" s="T735">kunolbial, </ts>
               <ts e="T737" id="Seg_4718" n="e" s="T736">(kak-) </ts>
               <ts e="T738" id="Seg_4720" n="e" s="T737">kabɨ </ts>
               <ts e="T739" id="Seg_4722" n="e" s="T738">ej </ts>
               <ts e="T740" id="Seg_4724" n="e" s="T739">măn </ts>
               <ts e="T741" id="Seg_4726" n="e" s="T740">dak, </ts>
               <ts e="T742" id="Seg_4728" n="e" s="T741">tăn </ts>
               <ts e="T743" id="Seg_4730" n="e" s="T742">uge </ts>
               <ts e="T744" id="Seg_4732" n="e" s="T743">bɨ </ts>
               <ts e="T745" id="Seg_4734" n="e" s="T744">kunolbial. </ts>
               <ts e="T746" id="Seg_4736" n="e" s="T745">Tănan </ts>
               <ts e="T747" id="Seg_4738" n="e" s="T746">(kutluʔpiʔi) </ts>
               <ts e="T748" id="Seg_4740" n="e" s="T747">tăn </ts>
               <ts e="T749" id="Seg_4742" n="e" s="T748">(kagazaŋ-) </ts>
               <ts e="T750" id="Seg_4744" n="e" s="T749">kagazaŋdə. </ts>
               <ts e="T751" id="Seg_4746" n="e" s="T750">I </ts>
               <ts e="T752" id="Seg_4748" n="e" s="T751">bar </ts>
               <ts e="T753" id="Seg_4750" n="e" s="T752">iluʔpiʔi, </ts>
               <ts e="T754" id="Seg_4752" n="e" s="T753">koʔbdom, </ts>
               <ts e="T755" id="Seg_4754" n="e" s="T754">i </ts>
               <ts e="T756" id="Seg_4756" n="e" s="T755">süjöm </ts>
               <ts e="T757" id="Seg_4758" n="e" s="T756">i </ts>
               <ts e="T758" id="Seg_4760" n="e" s="T757">inem". </ts>
               <ts e="T759" id="Seg_4762" n="e" s="T758">Dĭgəttə: </ts>
               <ts e="T760" id="Seg_4764" n="e" s="T759">"Amnaʔ </ts>
               <ts e="T761" id="Seg_4766" n="e" s="T760">măna!" </ts>
               <ts e="T762" id="Seg_4768" n="e" s="T761">Amnolbi. </ts>
               <ts e="T763" id="Seg_4770" n="e" s="T762">Dĭzeŋ </ts>
               <ts e="T764" id="Seg_4772" n="e" s="T763">bĭdəbiʔi </ts>
               <ts e="T765" id="Seg_4774" n="e" s="T764">dĭm. </ts>
               <ts e="T766" id="Seg_4776" n="e" s="T765">Dĭ </ts>
               <ts e="T767" id="Seg_4778" n="e" s="T766">volk </ts>
               <ts e="T768" id="Seg_4780" n="e" s="T767">kagazaŋdə </ts>
               <ts e="T769" id="Seg_4782" n="e" s="T768">(sajnožuʔpi). </ts>
               <ts e="T770" id="Seg_4784" n="e" s="T769">I </ts>
               <ts e="T771" id="Seg_4786" n="e" s="T770">bostə </ts>
               <ts e="T772" id="Seg_4788" n="e" s="T771">ibi </ts>
               <ts e="T773" id="Seg_4790" n="e" s="T772">koʔbdo </ts>
               <ts e="T774" id="Seg_4792" n="e" s="T773">i </ts>
               <ts e="T775" id="Seg_4794" n="e" s="T774">bar </ts>
               <ts e="T776" id="Seg_4796" n="e" s="T775">ĭmbi </ts>
               <ts e="T777" id="Seg_4798" n="e" s="T776">dĭn </ts>
               <ts e="T778" id="Seg_4800" n="e" s="T777">ibi. </ts>
               <ts e="T779" id="Seg_4802" n="e" s="T778">Dĭgəttə </ts>
               <ts e="T780" id="Seg_4804" n="e" s="T779">dĭ </ts>
               <ts e="T781" id="Seg_4806" n="e" s="T780">Ivanuška </ts>
               <ts e="T782" id="Seg_4808" n="e" s="T781">dĭʔnə </ts>
               <ts e="T783" id="Seg_4810" n="e" s="T782">saʔməluʔpi </ts>
               <ts e="T784" id="Seg_4812" n="e" s="T783">üjündə. </ts>
               <ts e="T785" id="Seg_4814" n="e" s="T784">I </ts>
               <ts e="T786" id="Seg_4816" n="e" s="T785">parluʔpi </ts>
               <ts e="T787" id="Seg_4818" n="e" s="T786">maːndə. </ts>
               <ts e="T788" id="Seg_4820" n="e" s="T787">Šobi </ts>
               <ts e="T789" id="Seg_4822" n="e" s="T788">abandə, </ts>
               <ts e="T790" id="Seg_4824" n="e" s="T789">bar </ts>
               <ts e="T791" id="Seg_4826" n="e" s="T790">ĭmbi </ts>
               <ts e="T792" id="Seg_4828" n="e" s="T791">nörbəbi. </ts>
               <ts e="T793" id="Seg_4830" n="e" s="T792">Dĭgəttə </ts>
               <ts e="T794" id="Seg_4832" n="e" s="T793">abat </ts>
               <ts e="T795" id="Seg_4834" n="e" s="T794">dʼorbi, </ts>
               <ts e="T796" id="Seg_4836" n="e" s="T795">ajirbi. </ts>
               <ts e="T797" id="Seg_4838" n="e" s="T796">Dĭgəttə </ts>
               <ts e="T798" id="Seg_4840" n="e" s="T797">dĭ </ts>
               <ts e="T799" id="Seg_4842" n="e" s="T798">koʔbdom </ts>
               <ts e="T800" id="Seg_4844" n="e" s="T799">ibi. </ts>
               <ts e="T801" id="Seg_4846" n="e" s="T800">Monoʔkobiʔi. </ts>
               <ts e="T802" id="Seg_4848" n="e" s="T801">Ara </ts>
               <ts e="T803" id="Seg_4850" n="e" s="T802">bĭʔpiʔi. </ts>
               <ts e="T804" id="Seg_4852" n="e" s="T803">I </ts>
               <ts e="T805" id="Seg_4854" n="e" s="T804">tüjö </ts>
               <ts e="T806" id="Seg_4856" n="e" s="T805">uge </ts>
               <ts e="T807" id="Seg_4858" n="e" s="T806">amnolaʔbəʔjə. </ts>
               <ts e="T808" id="Seg_4860" n="e" s="T807">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_4861" s="T0">PKZ_196X_FireBird_flk.001 (001)</ta>
            <ta e="T7" id="Seg_4862" s="T3">PKZ_196X_FireBird_flk.002 (002)</ta>
            <ta e="T13" id="Seg_4863" s="T7">PKZ_196X_FireBird_flk.003 (003)</ta>
            <ta e="T16" id="Seg_4864" s="T13">PKZ_196X_FireBird_flk.004 (004)</ta>
            <ta e="T21" id="Seg_4865" s="T16">PKZ_196X_FireBird_flk.005 (005)</ta>
            <ta e="T24" id="Seg_4866" s="T21">PKZ_196X_FireBird_flk.006 (006)</ta>
            <ta e="T27" id="Seg_4867" s="T24">PKZ_196X_FireBird_flk.007 (007)</ta>
            <ta e="T34" id="Seg_4868" s="T27">PKZ_196X_FireBird_flk.008 (008)</ta>
            <ta e="T36" id="Seg_4869" s="T34">PKZ_196X_FireBird_flk.009 (010)</ta>
            <ta e="T37" id="Seg_4870" s="T36">PKZ_196X_FireBird_flk.010 (011)</ta>
            <ta e="T40" id="Seg_4871" s="T37">PKZ_196X_FireBird_flk.011 (012)</ta>
            <ta e="T45" id="Seg_4872" s="T40">PKZ_196X_FireBird_flk.012 (013)</ta>
            <ta e="T47" id="Seg_4873" s="T45">PKZ_196X_FireBird_flk.013 (014)</ta>
            <ta e="T50" id="Seg_4874" s="T47">PKZ_196X_FireBird_flk.014 (015)</ta>
            <ta e="T53" id="Seg_4875" s="T50">PKZ_196X_FireBird_flk.015 (016)</ta>
            <ta e="T55" id="Seg_4876" s="T53">PKZ_196X_FireBird_flk.016 (017)</ta>
            <ta e="T65" id="Seg_4877" s="T55">PKZ_196X_FireBird_flk.017 (018)</ta>
            <ta e="T71" id="Seg_4878" s="T65">PKZ_196X_FireBird_flk.018 (019)</ta>
            <ta e="T77" id="Seg_4879" s="T71">PKZ_196X_FireBird_flk.019 (020) </ta>
            <ta e="T79" id="Seg_4880" s="T77">PKZ_196X_FireBird_flk.020 (022)</ta>
            <ta e="T81" id="Seg_4881" s="T79">PKZ_196X_FireBird_flk.021 (023)</ta>
            <ta e="T82" id="Seg_4882" s="T81">PKZ_196X_FireBird_flk.022 (024)</ta>
            <ta e="T86" id="Seg_4883" s="T82">PKZ_196X_FireBird_flk.023 (025)</ta>
            <ta e="T89" id="Seg_4884" s="T86">PKZ_196X_FireBird_flk.024 (026)</ta>
            <ta e="T92" id="Seg_4885" s="T89">PKZ_196X_FireBird_flk.025 (027)</ta>
            <ta e="T97" id="Seg_4886" s="T92">PKZ_196X_FireBird_flk.026 (028)</ta>
            <ta e="T101" id="Seg_4887" s="T97">PKZ_196X_FireBird_flk.027 (029)</ta>
            <ta e="T108" id="Seg_4888" s="T101">PKZ_196X_FireBird_flk.028 (030)</ta>
            <ta e="T112" id="Seg_4889" s="T108">PKZ_196X_FireBird_flk.029 (031)</ta>
            <ta e="T118" id="Seg_4890" s="T112">PKZ_196X_FireBird_flk.030 (032)</ta>
            <ta e="T121" id="Seg_4891" s="T118">PKZ_196X_FireBird_flk.031 (033)</ta>
            <ta e="T136" id="Seg_4892" s="T121">PKZ_196X_FireBird_flk.032 (034)</ta>
            <ta e="T138" id="Seg_4893" s="T136">PKZ_196X_FireBird_flk.033 (035)</ta>
            <ta e="T145" id="Seg_4894" s="T138">PKZ_196X_FireBird_flk.034 (036)</ta>
            <ta e="T148" id="Seg_4895" s="T145">PKZ_196X_FireBird_flk.035 (037)</ta>
            <ta e="T149" id="Seg_4896" s="T148">PKZ_196X_FireBird_flk.036 (038)</ta>
            <ta e="T153" id="Seg_4897" s="T149">PKZ_196X_FireBird_flk.037 (039)</ta>
            <ta e="T155" id="Seg_4898" s="T153">PKZ_196X_FireBird_flk.038 (040)</ta>
            <ta e="T157" id="Seg_4899" s="T155">PKZ_196X_FireBird_flk.039 (041)</ta>
            <ta e="T161" id="Seg_4900" s="T157">PKZ_196X_FireBird_flk.040 (042)</ta>
            <ta e="T163" id="Seg_4901" s="T161">PKZ_196X_FireBird_flk.041 (043)</ta>
            <ta e="T165" id="Seg_4902" s="T163">PKZ_196X_FireBird_flk.042 (044)</ta>
            <ta e="T167" id="Seg_4903" s="T165">PKZ_196X_FireBird_flk.043 (045)</ta>
            <ta e="T169" id="Seg_4904" s="T167">PKZ_196X_FireBird_flk.044 (046)</ta>
            <ta e="T172" id="Seg_4905" s="T169">PKZ_196X_FireBird_flk.045 (047)</ta>
            <ta e="T174" id="Seg_4906" s="T172">PKZ_196X_FireBird_flk.046 (048)</ta>
            <ta e="T177" id="Seg_4907" s="T174">PKZ_196X_FireBird_flk.047 (049)</ta>
            <ta e="T183" id="Seg_4908" s="T177">PKZ_196X_FireBird_flk.048 (050)</ta>
            <ta e="T191" id="Seg_4909" s="T183">PKZ_196X_FireBird_flk.049 (051)</ta>
            <ta e="T194" id="Seg_4910" s="T191">PKZ_196X_FireBird_flk.050 (052)</ta>
            <ta e="T198" id="Seg_4911" s="T194">PKZ_196X_FireBird_flk.051 (053)</ta>
            <ta e="T202" id="Seg_4912" s="T198">PKZ_196X_FireBird_flk.052 (054)</ta>
            <ta e="T206" id="Seg_4913" s="T202">PKZ_196X_FireBird_flk.053 (055)</ta>
            <ta e="T212" id="Seg_4914" s="T206">PKZ_196X_FireBird_flk.054 (056)</ta>
            <ta e="T222" id="Seg_4915" s="T212">PKZ_196X_FireBird_flk.055 (057)</ta>
            <ta e="T224" id="Seg_4916" s="T222">PKZ_196X_FireBird_flk.056 (058)</ta>
            <ta e="T236" id="Seg_4917" s="T224">PKZ_196X_FireBird_flk.057 (059)</ta>
            <ta e="T239" id="Seg_4918" s="T236">PKZ_196X_FireBird_flk.058 (060)</ta>
            <ta e="T242" id="Seg_4919" s="T239">PKZ_196X_FireBird_flk.059 (061)</ta>
            <ta e="T246" id="Seg_4920" s="T242">PKZ_196X_FireBird_flk.060 (062)</ta>
            <ta e="T247" id="Seg_4921" s="T246">PKZ_196X_FireBird_flk.061 (063)</ta>
            <ta e="T251" id="Seg_4922" s="T247">PKZ_196X_FireBird_flk.062 (064)</ta>
            <ta e="T254" id="Seg_4923" s="T251">PKZ_196X_FireBird_flk.063 (065)</ta>
            <ta e="T260" id="Seg_4924" s="T254">PKZ_196X_FireBird_flk.064 (066)</ta>
            <ta e="T263" id="Seg_4925" s="T260">PKZ_196X_FireBird_flk.065 (067)</ta>
            <ta e="T265" id="Seg_4926" s="T263">PKZ_196X_FireBird_flk.066 (068)</ta>
            <ta e="T266" id="Seg_4927" s="T265">PKZ_196X_FireBird_flk.067 (069)</ta>
            <ta e="T270" id="Seg_4928" s="T266">PKZ_196X_FireBird_flk.068 (070)</ta>
            <ta e="T276" id="Seg_4929" s="T270">PKZ_196X_FireBird_flk.069 (071)</ta>
            <ta e="T278" id="Seg_4930" s="T276">PKZ_196X_FireBird_flk.070 (072)</ta>
            <ta e="T283" id="Seg_4931" s="T278">PKZ_196X_FireBird_flk.071 (073)</ta>
            <ta e="T284" id="Seg_4932" s="T283">PKZ_196X_FireBird_flk.072 (074)</ta>
            <ta e="T288" id="Seg_4933" s="T284">PKZ_196X_FireBird_flk.073 (075)</ta>
            <ta e="T294" id="Seg_4934" s="T288">PKZ_196X_FireBird_flk.074 (076)</ta>
            <ta e="T297" id="Seg_4935" s="T294">PKZ_196X_FireBird_flk.075 (077)</ta>
            <ta e="T299" id="Seg_4936" s="T297">PKZ_196X_FireBird_flk.076 (078)</ta>
            <ta e="T303" id="Seg_4937" s="T299">PKZ_196X_FireBird_flk.077 (079)</ta>
            <ta e="T306" id="Seg_4938" s="T303">PKZ_196X_FireBird_flk.078 (080)</ta>
            <ta e="T313" id="Seg_4939" s="T306">PKZ_196X_FireBird_flk.079 (081)</ta>
            <ta e="T326" id="Seg_4940" s="T313">PKZ_196X_FireBird_flk.080 (082)</ta>
            <ta e="T337" id="Seg_4941" s="T326">PKZ_196X_FireBird_flk.081 (083)</ta>
            <ta e="T341" id="Seg_4942" s="T337">PKZ_196X_FireBird_flk.082 (084)</ta>
            <ta e="T348" id="Seg_4943" s="T341">PKZ_196X_FireBird_flk.083 (085)</ta>
            <ta e="T357" id="Seg_4944" s="T348">PKZ_196X_FireBird_flk.084 (086)</ta>
            <ta e="T358" id="Seg_4945" s="T357">PKZ_196X_FireBird_flk.085 (087)</ta>
            <ta e="T361" id="Seg_4946" s="T358">PKZ_196X_FireBird_flk.086 (088)</ta>
            <ta e="T364" id="Seg_4947" s="T361">PKZ_196X_FireBird_flk.087 (089)</ta>
            <ta e="T377" id="Seg_4948" s="T364">PKZ_196X_FireBird_flk.088 (090)</ta>
            <ta e="T381" id="Seg_4949" s="T377">PKZ_196X_FireBird_flk.089 (091)</ta>
            <ta e="T383" id="Seg_4950" s="T381">PKZ_196X_FireBird_flk.090 (092)</ta>
            <ta e="T385" id="Seg_4951" s="T383">PKZ_196X_FireBird_flk.091 (093)</ta>
            <ta e="T389" id="Seg_4952" s="T385">PKZ_196X_FireBird_flk.092 (094)</ta>
            <ta e="T394" id="Seg_4953" s="T389">PKZ_196X_FireBird_flk.093 (095)</ta>
            <ta e="T396" id="Seg_4954" s="T394">PKZ_196X_FireBird_flk.094 (096)</ta>
            <ta e="T399" id="Seg_4955" s="T396">PKZ_196X_FireBird_flk.095 (097)</ta>
            <ta e="T403" id="Seg_4956" s="T399">PKZ_196X_FireBird_flk.096 (098)</ta>
            <ta e="T406" id="Seg_4957" s="T403">PKZ_196X_FireBird_flk.097 (099)</ta>
            <ta e="T410" id="Seg_4958" s="T406">PKZ_196X_FireBird_flk.098 (100)</ta>
            <ta e="T412" id="Seg_4959" s="T410">PKZ_196X_FireBird_flk.099 (101)</ta>
            <ta e="T415" id="Seg_4960" s="T412">PKZ_196X_FireBird_flk.100 (102)</ta>
            <ta e="T419" id="Seg_4961" s="T415">PKZ_196X_FireBird_flk.101 (103)</ta>
            <ta e="T426" id="Seg_4962" s="T419">PKZ_196X_FireBird_flk.102 (104)</ta>
            <ta e="T431" id="Seg_4963" s="T426">PKZ_196X_FireBird_flk.103 (105)</ta>
            <ta e="T434" id="Seg_4964" s="T431">PKZ_196X_FireBird_flk.104 (106)</ta>
            <ta e="T438" id="Seg_4965" s="T434">PKZ_196X_FireBird_flk.105 (107)</ta>
            <ta e="T447" id="Seg_4966" s="T438">PKZ_196X_FireBird_flk.106 (108)</ta>
            <ta e="T450" id="Seg_4967" s="T447">PKZ_196X_FireBird_flk.107 (109)</ta>
            <ta e="T455" id="Seg_4968" s="T450">PKZ_196X_FireBird_flk.108 (110)</ta>
            <ta e="T457" id="Seg_4969" s="T455">PKZ_196X_FireBird_flk.109 (111)</ta>
            <ta e="T466" id="Seg_4970" s="T457">PKZ_196X_FireBird_flk.110 (112) </ta>
            <ta e="T470" id="Seg_4971" s="T466">PKZ_196X_FireBird_flk.111 (114)</ta>
            <ta e="T473" id="Seg_4972" s="T470">PKZ_196X_FireBird_flk.112 (115)</ta>
            <ta e="T475" id="Seg_4973" s="T473">PKZ_196X_FireBird_flk.113 (116)</ta>
            <ta e="T479" id="Seg_4974" s="T475">PKZ_196X_FireBird_flk.114 (117)</ta>
            <ta e="T482" id="Seg_4975" s="T479">PKZ_196X_FireBird_flk.115 (118)</ta>
            <ta e="T492" id="Seg_4976" s="T482">PKZ_196X_FireBird_flk.116 (119)</ta>
            <ta e="T493" id="Seg_4977" s="T492">PKZ_196X_FireBird_flk.117 (120)</ta>
            <ta e="T494" id="Seg_4978" s="T493">PKZ_196X_FireBird_flk.118 (121)</ta>
            <ta e="T501" id="Seg_4979" s="T494">PKZ_196X_FireBird_flk.119 (122)</ta>
            <ta e="T509" id="Seg_4980" s="T501">PKZ_196X_FireBird_flk.120 (123)</ta>
            <ta e="T512" id="Seg_4981" s="T509">PKZ_196X_FireBird_flk.121 (124)</ta>
            <ta e="T519" id="Seg_4982" s="T512">PKZ_196X_FireBird_flk.122 (125) </ta>
            <ta e="T523" id="Seg_4983" s="T519">PKZ_196X_FireBird_flk.123 (127)</ta>
            <ta e="T527" id="Seg_4984" s="T523">PKZ_196X_FireBird_flk.124 (128)</ta>
            <ta e="T536" id="Seg_4985" s="T527">PKZ_196X_FireBird_flk.125 (129)</ta>
            <ta e="T540" id="Seg_4986" s="T536">PKZ_196X_FireBird_flk.126 (130)</ta>
            <ta e="T544" id="Seg_4987" s="T540">PKZ_196X_FireBird_flk.127 (131)</ta>
            <ta e="T549" id="Seg_4988" s="T544">PKZ_196X_FireBird_flk.128 (132)</ta>
            <ta e="T555" id="Seg_4989" s="T549">PKZ_196X_FireBird_flk.129 (133)</ta>
            <ta e="T558" id="Seg_4990" s="T555">PKZ_196X_FireBird_flk.130 (134)</ta>
            <ta e="T562" id="Seg_4991" s="T558">PKZ_196X_FireBird_flk.131 (135)</ta>
            <ta e="T563" id="Seg_4992" s="T562">PKZ_196X_FireBird_flk.132 (136)</ta>
            <ta e="T568" id="Seg_4993" s="T563">PKZ_196X_FireBird_flk.133 (137)</ta>
            <ta e="T575" id="Seg_4994" s="T568">PKZ_196X_FireBird_flk.134 (138)</ta>
            <ta e="T580" id="Seg_4995" s="T575">PKZ_196X_FireBird_flk.135 (139)</ta>
            <ta e="T583" id="Seg_4996" s="T580">PKZ_196X_FireBird_flk.136 (140)</ta>
            <ta e="T587" id="Seg_4997" s="T583">PKZ_196X_FireBird_flk.137 (141)</ta>
            <ta e="T591" id="Seg_4998" s="T587">PKZ_196X_FireBird_flk.138 (142)</ta>
            <ta e="T597" id="Seg_4999" s="T591">PKZ_196X_FireBird_flk.139 (143)</ta>
            <ta e="T604" id="Seg_5000" s="T597">PKZ_196X_FireBird_flk.140 (144)</ta>
            <ta e="T611" id="Seg_5001" s="T604">PKZ_196X_FireBird_flk.141 (145)</ta>
            <ta e="T617" id="Seg_5002" s="T611">PKZ_196X_FireBird_flk.142 (146)</ta>
            <ta e="T621" id="Seg_5003" s="T617">PKZ_196X_FireBird_flk.143 (147)</ta>
            <ta e="T628" id="Seg_5004" s="T621">PKZ_196X_FireBird_flk.144 (148)</ta>
            <ta e="T635" id="Seg_5005" s="T628">PKZ_196X_FireBird_flk.145 (149)</ta>
            <ta e="T640" id="Seg_5006" s="T635">PKZ_196X_FireBird_flk.146 (150)</ta>
            <ta e="T647" id="Seg_5007" s="T640">PKZ_196X_FireBird_flk.147 (151)</ta>
            <ta e="T658" id="Seg_5008" s="T647">PKZ_196X_FireBird_flk.148 (152) </ta>
            <ta e="T664" id="Seg_5009" s="T658">PKZ_196X_FireBird_flk.149 (154)</ta>
            <ta e="T665" id="Seg_5010" s="T664">PKZ_196X_FireBird_flk.150 (155)</ta>
            <ta e="T672" id="Seg_5011" s="T665">PKZ_196X_FireBird_flk.151 (156)</ta>
            <ta e="T675" id="Seg_5012" s="T672">PKZ_196X_FireBird_flk.152 (157)</ta>
            <ta e="T677" id="Seg_5013" s="T675">PKZ_196X_FireBird_flk.153 (158)</ta>
            <ta e="T681" id="Seg_5014" s="T677">PKZ_196X_FireBird_flk.154 (159)</ta>
            <ta e="T684" id="Seg_5015" s="T681">PKZ_196X_FireBird_flk.155 (160)</ta>
            <ta e="T686" id="Seg_5016" s="T684">PKZ_196X_FireBird_flk.156 (161)</ta>
            <ta e="T690" id="Seg_5017" s="T686">PKZ_196X_FireBird_flk.157 (162)</ta>
            <ta e="T695" id="Seg_5018" s="T690">PKZ_196X_FireBird_flk.158 (163)</ta>
            <ta e="T696" id="Seg_5019" s="T695">PKZ_196X_FireBird_flk.159 (164)</ta>
            <ta e="T703" id="Seg_5020" s="T696">PKZ_196X_FireBird_flk.160 (165)</ta>
            <ta e="T708" id="Seg_5021" s="T703">PKZ_196X_FireBird_flk.161 (166)</ta>
            <ta e="T715" id="Seg_5022" s="T708">PKZ_196X_FireBird_flk.162 (167)</ta>
            <ta e="T719" id="Seg_5023" s="T715">PKZ_196X_FireBird_flk.163 (168)</ta>
            <ta e="T725" id="Seg_5024" s="T719">PKZ_196X_FireBird_flk.164 (169)</ta>
            <ta e="T729" id="Seg_5025" s="T725">PKZ_196X_FireBird_flk.165 (170)</ta>
            <ta e="T734" id="Seg_5026" s="T729">PKZ_196X_FireBird_flk.166 (171)</ta>
            <ta e="T745" id="Seg_5027" s="T734">PKZ_196X_FireBird_flk.167 (172)</ta>
            <ta e="T750" id="Seg_5028" s="T745">PKZ_196X_FireBird_flk.168 (173)</ta>
            <ta e="T758" id="Seg_5029" s="T750">PKZ_196X_FireBird_flk.169 (174)</ta>
            <ta e="T761" id="Seg_5030" s="T758">PKZ_196X_FireBird_flk.170 (175)</ta>
            <ta e="T762" id="Seg_5031" s="T761">PKZ_196X_FireBird_flk.171 (176)</ta>
            <ta e="T765" id="Seg_5032" s="T762">PKZ_196X_FireBird_flk.172 (177)</ta>
            <ta e="T769" id="Seg_5033" s="T765">PKZ_196X_FireBird_flk.173 (178)</ta>
            <ta e="T778" id="Seg_5034" s="T769">PKZ_196X_FireBird_flk.174 (179)</ta>
            <ta e="T784" id="Seg_5035" s="T778">PKZ_196X_FireBird_flk.175 (180)</ta>
            <ta e="T787" id="Seg_5036" s="T784">PKZ_196X_FireBird_flk.176 (181)</ta>
            <ta e="T792" id="Seg_5037" s="T787">PKZ_196X_FireBird_flk.177 (182)</ta>
            <ta e="T796" id="Seg_5038" s="T792">PKZ_196X_FireBird_flk.178 (183)</ta>
            <ta e="T800" id="Seg_5039" s="T796">PKZ_196X_FireBird_flk.179 (184)</ta>
            <ta e="T801" id="Seg_5040" s="T800">PKZ_196X_FireBird_flk.180 (185)</ta>
            <ta e="T803" id="Seg_5041" s="T801">PKZ_196X_FireBird_flk.181 (186)</ta>
            <ta e="T807" id="Seg_5042" s="T803">PKZ_196X_FireBird_flk.182 (187)</ta>
            <ta e="T808" id="Seg_5043" s="T807">PKZ_196X_FireBird_flk.183 (188)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_5044" s="T0">Onʼiʔ koŋ amnolaʔpi. </ta>
            <ta e="T7" id="Seg_5045" s="T3">Dĭn ibiʔi nagurgöʔ ((NOISE)) nʼizeŋdə. ((NOISE)) </ta>
            <ta e="T13" id="Seg_5046" s="T7">Dĭn sad ibi, jabloki amnolaʔpiʔi, kuvazəʔi. </ta>
            <ta e="T16" id="Seg_5047" s="T13">Šindidə bar tojirlaʔbə. </ta>
            <ta e="T21" id="Seg_5048" s="T16">Dĭ bar kuliot šindidə (tojirlaʔbə). </ta>
            <ta e="T24" id="Seg_5049" s="T21">Nuldəbi iləm măndərzittə. </ta>
            <ta e="T27" id="Seg_5050" s="T24">Dĭzeŋ măndliaʔi, naga. </ta>
            <ta e="T34" id="Seg_5051" s="T27">Dĭgəttə onʼiʔ (nʼi) (mən-) mănlia: "Măn kallam!" </ta>
            <ta e="T36" id="Seg_5052" s="T34">Dĭ kambi. </ta>
            <ta e="T37" id="Seg_5053" s="T36">Kunolluʔpi. </ta>
            <ta e="T40" id="Seg_5054" s="T37">Ĭmbidə ej kubi. </ta>
            <ta e="T45" id="Seg_5055" s="T40">Dĭgəttə baška nʼi (ka-) kambi. </ta>
            <ta e="T47" id="Seg_5056" s="T45">Tože kunolluʔpi. </ta>
            <ta e="T50" id="Seg_5057" s="T47">Ĭmbidə ej kubi. </ta>
            <ta e="T53" id="Seg_5058" s="T50">Dĭgəttə Ivanuška kambi. </ta>
            <ta e="T55" id="Seg_5059" s="T53">Ej kunolia. </ta>
            <ta e="T65" id="Seg_5060" s="T55">Kunolzittə axota, dĭ bar büziʔ sʼimabə băzəlia da bazoʔ mĭlleʔbə. </ta>
            <ta e="T71" id="Seg_5061" s="T65">Dĭgəttə kuliot, kulaːmbi bar: nendluʔpi šü. </ta>
            <ta e="T77" id="Seg_5062" s="T71">Dĭ (ku-) kulaːmbi: (Süj-) Süjö amnolaʔbə. </ta>
            <ta e="T79" id="Seg_5063" s="T77">Šü dĭrgit. </ta>
            <ta e="T81" id="Seg_5064" s="T79">Dĭn (müʔluʔpi). </ta>
            <ta e="T82" id="Seg_5065" s="T81">Dʼabəluʔpi. </ta>
            <ta e="T86" id="Seg_5066" s="T82">Onʼiʔ pʼero (de-) deʔpi. </ta>
            <ta e="T89" id="Seg_5067" s="T86">Dĭgəttə šobi abanə. </ta>
            <ta e="T92" id="Seg_5068" s="T89">"No, ĭmbi nʼim?" </ta>
            <ta e="T97" id="Seg_5069" s="T92">"(D-) Măn kubiom, šində tojirlaʔbə. </ta>
            <ta e="T101" id="Seg_5070" s="T97">Dö, tănan deʔpiem pʼero. </ta>
            <ta e="T108" id="Seg_5071" s="T101">Dĭ Žar-Ptitsa mĭlleʔbə i tojirlaʔbə". </ta>
            <ta e="T112" id="Seg_5072" s="T108">Dĭgəttə abat stal amzittə. </ta>
            <ta e="T118" id="Seg_5073" s="T112">Dĭgəttə tenöbi, tenöbi: "Kangaʔ, nʼizeŋ gibər-nʼibudʼ. </ta>
            <ta e="T121" id="Seg_5074" s="T118">Bar dʼü mĭngeʔ! </ta>
            <ta e="T136" id="Seg_5075" s="T121">(Il mə- mə- mə-) Možet (gijən= paim- ku-) gijən-nʼibudʼ dĭ süjö (dʼapia - dʼabiluj-) dʼapləlaʔ. </ta>
            <ta e="T138" id="Seg_5076" s="T136">Girgit šü". </ta>
            <ta e="T145" id="Seg_5077" s="T138">Dĭzeŋ (ka-) kambiʔi onʼiʔ dibər, baška döbər. </ta>
            <ta e="T148" id="Seg_5078" s="T145">Vanʼuška tože kambi. </ta>
            <ta e="T149" id="Seg_5079" s="T148">Kabarləj. </ta>
            <ta e="T153" id="Seg_5080" s="T149">Dĭgəttə dĭ Ivan Tsarevič kambi. </ta>
            <ta e="T155" id="Seg_5081" s="T153">Šobi, šobi. </ta>
            <ta e="T157" id="Seg_5082" s="T155">Inebə körerluʔpi. </ta>
            <ta e="T161" id="Seg_5083" s="T157">Ambi, (amnoʔ-) iʔbəbi kunolzittə. </ta>
            <ta e="T163" id="Seg_5084" s="T161">Inet mĭlleʔbə. </ta>
            <ta e="T165" id="Seg_5085" s="T163">Ujubə sarbi. </ta>
            <ta e="T167" id="Seg_5086" s="T165">Kundʼo kunolbi. </ta>
            <ta e="T169" id="Seg_5087" s="T167">Dĭgəttə uʔbdəbi. </ta>
            <ta e="T172" id="Seg_5088" s="T169">Kuliot: inet naga. </ta>
            <ta e="T174" id="Seg_5089" s="T172">Măndərbi, măndərbi. </ta>
            <ta e="T177" id="Seg_5090" s="T174">Tolʼko leʔi iʔbolaʔbəʔjə. </ta>
            <ta e="T183" id="Seg_5091" s="T177">Amnolaʔbə da dĭgəttə nada kanzittə üdʼi. </ta>
            <ta e="T191" id="Seg_5092" s="T183">Kandəga, kandəga, dĭgəttə tararluʔpi da bar amnəluʔpi, amnolaʔbə. </ta>
            <ta e="T194" id="Seg_5093" s="T191">Dĭgəttə volk nuʔməleʔbə. </ta>
            <ta e="T198" id="Seg_5094" s="T194">"Ĭmbi amnolaʔbəl, ulul edəbiom?" </ta>
            <ta e="T202" id="Seg_5095" s="T198">"Da măn inem amnuʔpi". </ta>
            <ta e="T206" id="Seg_5096" s="T202">"A dĭ măn ambiam. </ta>
            <ta e="T212" id="Seg_5097" s="T206">Dak (măʔ-) a gibər tăn kandəgal?" </ta>
            <ta e="T222" id="Seg_5098" s="T212">"Da kandəgam (süjö=) süjö (măndəzi-) măndərzittə, abam măndə: măndəraʔ süjö! </ta>
            <ta e="T224" id="Seg_5099" s="T222">Šü dĭrgit". </ta>
            <ta e="T236" id="Seg_5100" s="T224">Dĭgəttə dĭ: "No tăn bostə inelziʔ dăk, nagur kö dĭbər ej kallal. </ta>
            <ta e="T239" id="Seg_5101" s="T236">Amnaʔ (mănnə-) măna! </ta>
            <ta e="T242" id="Seg_5102" s="T239">Măn tănan kunnaːllim". </ta>
            <ta e="T246" id="Seg_5103" s="T242">Amnəbi, dĭgəttə dĭ deʔpi. </ta>
            <ta e="T247" id="Seg_5104" s="T246">"Kanaʔ! </ta>
            <ta e="T251" id="Seg_5105" s="T247">Tuj bar il kunollaʔbəʔjə. </ta>
            <ta e="T254" id="Seg_5106" s="T251">Iʔ dĭ süjö! </ta>
            <ta e="T260" id="Seg_5107" s="T254">A klʼetkabə iʔ it, a to…" </ta>
            <ta e="T263" id="Seg_5108" s="T260">Dĭgəttə dĭ kambi. </ta>
            <ta e="T265" id="Seg_5109" s="T263">Ibi süjö. </ta>
            <ta e="T266" id="Seg_5110" s="T265">Măndərlia. </ta>
            <ta e="T270" id="Seg_5111" s="T266">Klʼetka kuvas, nada izittə. </ta>
            <ta e="T276" id="Seg_5112" s="T270">Kak dĭm mĭngəluʔpi bar kuzurluʔpi bar. </ta>
            <ta e="T278" id="Seg_5113" s="T276">Dĭm dʼabəluʔpi. </ta>
            <ta e="T283" id="Seg_5114" s="T278">I (kundlaʔ- kun-) kumbiʔi koŋdə. </ta>
            <ta e="T284" id="Seg_5115" s="T283">Kabarləj. </ta>
            <ta e="T288" id="Seg_5116" s="T284">Dĭgəttə dĭ šobi koŋdə. </ta>
            <ta e="T294" id="Seg_5117" s="T288">"Tăn (ĭmbi=) ĭmbi döbər šobiam, tojirzittə. </ta>
            <ta e="T297" id="Seg_5118" s="T294">Šindin tăn nʼi?" </ta>
            <ta e="T299" id="Seg_5119" s="T297">"Măn Ivan Tsarevič". </ta>
            <ta e="T303" id="Seg_5120" s="T299">"Dăk tăn tojirzittə zăxatel?" </ta>
            <ta e="T306" id="Seg_5121" s="T303">Dĭgəttə dĭ măndə. </ta>
            <ta e="T313" id="Seg_5122" s="T306">"A dĭ miʔnʼibeʔ mĭmbi da jabloki ambiʔi". </ta>
            <ta e="T326" id="Seg_5123" s="T313">"A tăn šobial bɨ, a măn dăre tănan mĭbiem bɨ, a to tojirzittə šobial. </ta>
            <ta e="T337" id="Seg_5124" s="T326">No, măn tănan ĭmbidə ej alam, kanaʔ, ine deʔ zălatoj grivazʼiʔ". </ta>
            <ta e="T341" id="Seg_5125" s="T337">Dĭgəttə dĭ kambi volktə. </ta>
            <ta e="T348" id="Seg_5126" s="T341">Volk măndə: "Măn mămbiam tănan, iʔ üžəmeʔ. </ta>
            <ta e="T357" id="Seg_5127" s="T348">A tăn (üžəmbiel), a tüj ((PAUSE)) amnaʔ măna, kanžəbəj". </ta>
            <ta e="T358" id="Seg_5128" s="T357">Kambiʔi. </ta>
            <ta e="T361" id="Seg_5129" s="T358">No, šobiʔi dibər. </ta>
            <ta e="T364" id="Seg_5130" s="T361">Dĭgəttə: "No, kanaʔ! </ta>
            <ta e="T377" id="Seg_5131" s="T364">Da dʼabit inem, a uzdam (ej= uzu- iʔ= užum- iʔ u-) iʔ iʔ! </ta>
            <ta e="T381" id="Seg_5132" s="T377">A to bazoʔ dăre molaːlləj!" </ta>
            <ta e="T383" id="Seg_5133" s="T381">Dĭ šobi. </ta>
            <ta e="T385" id="Seg_5134" s="T383">Inem ibi. </ta>
            <ta e="T389" id="Seg_5135" s="T385">A uzdat ugaːndə kuvas. </ta>
            <ta e="T394" id="Seg_5136" s="T389">Ĭmbi inegən, nada šerzittə tolʼko. </ta>
            <ta e="T396" id="Seg_5137" s="T394">Ibi dĭm. </ta>
            <ta e="T399" id="Seg_5138" s="T396">Dĭ bar kirgarluʔpi. </ta>
            <ta e="T403" id="Seg_5139" s="T399">Il suʔmiluʔpi, dĭm ibiʔi. </ta>
            <ta e="T406" id="Seg_5140" s="T403">(Kam-) Kunnaːmbiʔi koŋdə. </ta>
            <ta e="T410" id="Seg_5141" s="T406">"Ĭmbi šobial ine tojirzittə? </ta>
            <ta e="T412" id="Seg_5142" s="T410">Šindin tăn?" </ta>
            <ta e="T415" id="Seg_5143" s="T412">"(Măn=) Măn Ivan Tsarevič". </ta>
            <ta e="T419" id="Seg_5144" s="T415">"Dăk, tojirzittə (s-) šobial? </ta>
            <ta e="T426" id="Seg_5145" s="T419">No, kanaʔ, măn tănan ĭmbidə ej alam. </ta>
            <ta e="T431" id="Seg_5146" s="T426">Dĭn ige koʔbdo Ilena Prĭkrasnaja. </ta>
            <ta e="T434" id="Seg_5147" s="T431">Dettə măna dĭm!" </ta>
            <ta e="T438" id="Seg_5148" s="T434">Dĭgəttə dĭ (šo-) šonəga. </ta>
            <ta e="T447" id="Seg_5149" s="T438">A volktə: "Mămbiam: iʔ üžəmeʔ, a tăn üge üžəmneʔpiem". </ta>
            <ta e="T450" id="Seg_5150" s="T447">No, ĭmbidə (emnajaʔa). </ta>
            <ta e="T455" id="Seg_5151" s="T450">Dĭ măndə: "No, amnaʔ, kanžəbəj!" </ta>
            <ta e="T457" id="Seg_5152" s="T455">Šobiʔi dibər. </ta>
            <ta e="T466" id="Seg_5153" s="T457">Dĭgəttə volk măndə dĭʔnə: "Tăn (am-) kanaʔ, paraʔ, paraʔ. </ta>
            <ta e="T470" id="Seg_5154" s="T466">A măn kallam bostə". </ta>
            <ta e="T473" id="Seg_5155" s="T470">Dĭgəttə suʔməluʔpi sattə. </ta>
            <ta e="T475" id="Seg_5156" s="T473">Dĭn šaʔlaːmbi. </ta>
            <ta e="T479" id="Seg_5157" s="T475">Dĭzeŋ mĭnzəleʔbəʔjə, ijat, nʼanʼkaʔi. </ta>
            <ta e="T482" id="Seg_5158" s="T479">Dĭ Elenat maluʔpi. </ta>
            <ta e="T492" id="Seg_5159" s="T482">Dĭ dĭm kabarluʔpi i nuʔməluʔpi, vot nuʔməlaʔbə i bĭdəbi Ivanəm. </ta>
            <ta e="T493" id="Seg_5160" s="T492">"Amnaʔ!" </ta>
            <ta e="T494" id="Seg_5161" s="T493">Amnəlbiʔi. </ta>
            <ta e="T501" id="Seg_5162" s="T494">Dĭgəttə šobiʔi dibər, gijen (in-) ine pʼebiʔi. </ta>
            <ta e="T509" id="Seg_5163" s="T501">Dĭgəttə dĭzeŋ šobiʔi, a dĭ, tenöleʔbə üge, Ivan Tsarevič. </ta>
            <ta e="T512" id="Seg_5164" s="T509">Ajiria dĭ koʔbdom. </ta>
            <ta e="T519" id="Seg_5165" s="T512">A volktə surarlaʔbə: "Ĭmbi tăn dĭn nulaʔbəl?" </ta>
            <ta e="T523" id="Seg_5166" s="T519">"Dĭ dĭrgit kuvas koʔbdo. </ta>
            <ta e="T527" id="Seg_5167" s="T523">Măn ajirbiom, (mĭzen-) mĭzittə". </ta>
            <ta e="T536" id="Seg_5168" s="T527">"No, šaʔbdəlbəj, a (măn alaʔ-) măn alaʔbəm (dĭ-) dĭʔnə". </ta>
            <ta e="T540" id="Seg_5169" s="T536">Dĭgəttə bostə bögəldə suʔməluʔpi. </ta>
            <ta e="T544" id="Seg_5170" s="T540">Dĭ dĭm ibi, kunnaːmbi. </ta>
            <ta e="T549" id="Seg_5171" s="T544">A dĭ ibi dĭ koʔbdom. </ta>
            <ta e="T555" id="Seg_5172" s="T549">(Dʼansaʔməj) ara bĭʔpiʔi, suʔmileʔbəʔi, inebə mĭbi. </ta>
            <ta e="T558" id="Seg_5173" s="T555">Dĭ inebə ibi. </ta>
            <ta e="T562" id="Seg_5174" s="T558">Elenabə amnəlbəbi, bostə amnəlbi. </ta>
            <ta e="T563" id="Seg_5175" s="T562">Kambiʔi. </ta>
            <ta e="T568" id="Seg_5176" s="T563">A nüdʼin (iʔbibi-) iʔbəʔi kunolzittə. </ta>
            <ta e="T575" id="Seg_5177" s="T568">(Dĭ= uʔ-) Dĭ Elenat (mo-) molaːmbi volkziʔ. </ta>
            <ta e="T580" id="Seg_5178" s="T575">I dĭ nerölüʔpi i saʔməluʔpi. </ta>
            <ta e="T583" id="Seg_5179" s="T580">A dĭ nuʔməluʔpi. </ta>
            <ta e="T587" id="Seg_5180" s="T583">Dĭgəttə bĭdəliet Ivanuška (Elenuškaʔiziʔ). </ta>
            <ta e="T591" id="Seg_5181" s="T587">"Ĭmbi tăn tĭn nulaʔliel?" </ta>
            <ta e="T597" id="Seg_5182" s="T591">"Da nʼe axota mĭzittə ine, kuvas". </ta>
            <ta e="T604" id="Seg_5183" s="T597">A dĭ măndə: "Šaʔlaːndə inem i koʔbdom". </ta>
            <ta e="T611" id="Seg_5184" s="T604">Dĭgəttə dĭ aluʔpi ineziʔ, dĭ dĭm kumbi. </ta>
            <ta e="T617" id="Seg_5185" s="T611">Dĭ ibi süjö (nenʼinuʔ), (kak) šü. </ta>
            <ta e="T621" id="Seg_5186" s="T617">I bostə parluʔpi, amnəlbi. </ta>
            <ta e="T628" id="Seg_5187" s="T621">I koʔbdobə amnəlbi inenə, bostə amnəlbi, kambiʔi. </ta>
            <ta e="T635" id="Seg_5188" s="T628">Dĭgəttə dĭ koŋ: "Da deʔkeʔ dĭ ine!" </ta>
            <ta e="T640" id="Seg_5189" s="T635">Dĭ ine šobi, volkziʔ aluʔpi. </ta>
            <ta e="T647" id="Seg_5190" s="T640">Dĭ neröluʔpi i saʔməluʔpi, a dĭ nuʔməluʔpi. </ta>
            <ta e="T658" id="Seg_5191" s="T647">Bĭdəleʔbə, Vanʼuškanə măndə: "No, tüj măna kanzittə tănziʔ nekuda, maləm dön". </ta>
            <ta e="T664" id="Seg_5192" s="T658">Dĭ suʔmiluʔpi, dĭʔnə nagurgöʔ dʼünə saʔməluʔpi. </ta>
            <ta e="T665" id="Seg_5193" s="T664">Kabarləj. </ta>
            <ta e="T672" id="Seg_5194" s="T665">Dĭgəttə (dĭ-) dĭzeŋ šobiʔi bostə (dʼü-) dʼünə. </ta>
            <ta e="T675" id="Seg_5195" s="T672">Măndə: "Davaj amorləbəj". </ta>
            <ta e="T677" id="Seg_5196" s="T675">Amnəbəlbiʔi, amorbiʔi. </ta>
            <ta e="T681" id="Seg_5197" s="T677">I (iʔbəʔi-) iʔpiʔi kunolzittə. </ta>
            <ta e="T684" id="Seg_5198" s="T681">Dĭgəttə kagazaŋdə šobiʔi. </ta>
            <ta e="T686" id="Seg_5199" s="T684">Dĭm kutlaːmbiʔi. </ta>
            <ta e="T690" id="Seg_5200" s="T686">A bostə ibiʔi koʔbdo. </ta>
            <ta e="T695" id="Seg_5201" s="T690">I süjö, i ine, kambi. </ta>
            <ta e="T696" id="Seg_5202" s="T695">Kandəlaʔbəʔjə. </ta>
            <ta e="T703" id="Seg_5203" s="T696">Dĭgəttə volk šobi, (kul-) kuliot: dĭ iʔbolaʔbə. </ta>
            <ta e="T708" id="Seg_5204" s="T703">Dʼaʔpi süjö, vărona, üdʼüge süjö. </ta>
            <ta e="T715" id="Seg_5205" s="T708">"Da deʔ, kübi bü i tʼili bü!" </ta>
            <ta e="T719" id="Seg_5206" s="T715">Dĭ süjö nʼergölüʔpi, deʔpi. </ta>
            <ta e="T725" id="Seg_5207" s="T719">Dĭ dĭm bazəbi (ku-) kule büzʼiʔ. </ta>
            <ta e="T729" id="Seg_5208" s="T725">I dĭgəttə (dʼilʼuj) büziʔ. </ta>
            <ta e="T734" id="Seg_5209" s="T729">Dĭ suʔmileʔbə: "Kondʼo măn kunolbiam". </ta>
            <ta e="T745" id="Seg_5210" s="T734">"Kondʼo kunolbial, (kak-) kabɨ ej măn dak, tăn uge bɨ kunolbial. </ta>
            <ta e="T750" id="Seg_5211" s="T745">Tănan (kutluʔpiʔi) tăn (kagazaŋ-) kagazaŋdə. </ta>
            <ta e="T758" id="Seg_5212" s="T750">I bar iluʔpiʔi, koʔbdom, i süjöm i inem". </ta>
            <ta e="T761" id="Seg_5213" s="T758">Dĭgəttə: "Amnaʔ măna!" </ta>
            <ta e="T762" id="Seg_5214" s="T761">Amnolbi. </ta>
            <ta e="T765" id="Seg_5215" s="T762">Dĭzeŋ bĭdəbiʔi dĭm. </ta>
            <ta e="T769" id="Seg_5216" s="T765">Dĭ volk kagazaŋdə (sajnožuʔpi). </ta>
            <ta e="T778" id="Seg_5217" s="T769">I bostə ibi koʔbdo i bar ĭmbi dĭn ibi. </ta>
            <ta e="T784" id="Seg_5218" s="T778">Dĭgəttə dĭ Ivanuška dĭʔnə saʔməluʔpi üjündə. </ta>
            <ta e="T787" id="Seg_5219" s="T784">I parluʔpi maːndə. </ta>
            <ta e="T792" id="Seg_5220" s="T787">Šobi abandə, bar ĭmbi nörbəbi. </ta>
            <ta e="T796" id="Seg_5221" s="T792">Dĭgəttə abat dʼorbi, ajirbi. </ta>
            <ta e="T800" id="Seg_5222" s="T796">Dĭgəttə dĭ koʔbdom ibi. </ta>
            <ta e="T801" id="Seg_5223" s="T800">Monoʔkobiʔi. </ta>
            <ta e="T803" id="Seg_5224" s="T801">Ara bĭʔpiʔi. </ta>
            <ta e="T807" id="Seg_5225" s="T803">I tüjö uge amnolaʔbəʔjə. </ta>
            <ta e="T808" id="Seg_5226" s="T807">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5227" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_5228" s="T1">koŋ</ta>
            <ta e="T3" id="Seg_5229" s="T2">amno-laʔ-pi</ta>
            <ta e="T4" id="Seg_5230" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_5231" s="T4">i-bi-ʔi</ta>
            <ta e="T6" id="Seg_5232" s="T5">nagur-göʔ</ta>
            <ta e="T7" id="Seg_5233" s="T6">nʼi-zeŋ-də</ta>
            <ta e="T8" id="Seg_5234" s="T7">dĭ-n</ta>
            <ta e="T9" id="Seg_5235" s="T8">sad</ta>
            <ta e="T10" id="Seg_5236" s="T9">i-bi</ta>
            <ta e="T12" id="Seg_5237" s="T11">amno-laʔpi-ʔi</ta>
            <ta e="T13" id="Seg_5238" s="T12">kuvazə-ʔi</ta>
            <ta e="T14" id="Seg_5239" s="T13">šindi=də</ta>
            <ta e="T15" id="Seg_5240" s="T14">bar</ta>
            <ta e="T16" id="Seg_5241" s="T15">tojir-laʔbə</ta>
            <ta e="T17" id="Seg_5242" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_5243" s="T17">bar</ta>
            <ta e="T19" id="Seg_5244" s="T18">ku-lio-t</ta>
            <ta e="T20" id="Seg_5245" s="T19">šindi=də</ta>
            <ta e="T21" id="Seg_5246" s="T20">tojir-laʔbə</ta>
            <ta e="T22" id="Seg_5247" s="T21">nuldə-bi</ta>
            <ta e="T23" id="Seg_5248" s="T22">il-ə-m</ta>
            <ta e="T24" id="Seg_5249" s="T23">măndə-r-zittə</ta>
            <ta e="T25" id="Seg_5250" s="T24">dĭ-zeŋ</ta>
            <ta e="T26" id="Seg_5251" s="T25">mănd-lia-ʔi</ta>
            <ta e="T27" id="Seg_5252" s="T26">naga</ta>
            <ta e="T28" id="Seg_5253" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_5254" s="T28">onʼiʔ</ta>
            <ta e="T30" id="Seg_5255" s="T29">nʼi</ta>
            <ta e="T32" id="Seg_5256" s="T31">măn-lia</ta>
            <ta e="T33" id="Seg_5257" s="T32">măn</ta>
            <ta e="T34" id="Seg_5258" s="T33">kal-la-m</ta>
            <ta e="T35" id="Seg_5259" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_5260" s="T35">kam-bi</ta>
            <ta e="T37" id="Seg_5261" s="T36">kunol-luʔ-pi</ta>
            <ta e="T38" id="Seg_5262" s="T37">ĭmbi=də</ta>
            <ta e="T39" id="Seg_5263" s="T38">ej</ta>
            <ta e="T40" id="Seg_5264" s="T39">ku-bi</ta>
            <ta e="T41" id="Seg_5265" s="T40">dĭgəttə</ta>
            <ta e="T42" id="Seg_5266" s="T41">baška</ta>
            <ta e="T43" id="Seg_5267" s="T42">nʼi</ta>
            <ta e="T45" id="Seg_5268" s="T44">kam-bi</ta>
            <ta e="T46" id="Seg_5269" s="T45">tože</ta>
            <ta e="T47" id="Seg_5270" s="T46">kunol-luʔ-pi</ta>
            <ta e="T48" id="Seg_5271" s="T47">ĭmbi=də</ta>
            <ta e="T49" id="Seg_5272" s="T48">ej</ta>
            <ta e="T50" id="Seg_5273" s="T49">ku-bi</ta>
            <ta e="T51" id="Seg_5274" s="T50">dĭgəttə</ta>
            <ta e="T52" id="Seg_5275" s="T51">Ivanuška</ta>
            <ta e="T53" id="Seg_5276" s="T52">kam-bi</ta>
            <ta e="T54" id="Seg_5277" s="T53">ej</ta>
            <ta e="T55" id="Seg_5278" s="T54">kunol-ia</ta>
            <ta e="T56" id="Seg_5279" s="T55">kunol-zittə</ta>
            <ta e="T57" id="Seg_5280" s="T56">axota</ta>
            <ta e="T58" id="Seg_5281" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_5282" s="T58">bar</ta>
            <ta e="T60" id="Seg_5283" s="T59">bü-ziʔ</ta>
            <ta e="T61" id="Seg_5284" s="T60">sʼima-bə</ta>
            <ta e="T62" id="Seg_5285" s="T61">băzə-lia</ta>
            <ta e="T63" id="Seg_5286" s="T62">da</ta>
            <ta e="T64" id="Seg_5287" s="T63">bazoʔ</ta>
            <ta e="T65" id="Seg_5288" s="T64">mĭl-leʔbə</ta>
            <ta e="T66" id="Seg_5289" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_5290" s="T66">ku-lio-t</ta>
            <ta e="T68" id="Seg_5291" s="T67">ku-laːm-bi</ta>
            <ta e="T69" id="Seg_5292" s="T68">bar</ta>
            <ta e="T70" id="Seg_5293" s="T69">nend-luʔ-pi</ta>
            <ta e="T71" id="Seg_5294" s="T70">šü</ta>
            <ta e="T72" id="Seg_5295" s="T71">dĭ</ta>
            <ta e="T74" id="Seg_5296" s="T73">ku-laːm-bi</ta>
            <ta e="T76" id="Seg_5297" s="T75">süjö</ta>
            <ta e="T77" id="Seg_5298" s="T76">amno-laʔbə</ta>
            <ta e="T78" id="Seg_5299" s="T77">šü</ta>
            <ta e="T79" id="Seg_5300" s="T78">dĭrgit</ta>
            <ta e="T80" id="Seg_5301" s="T79">dĭ-n</ta>
            <ta e="T81" id="Seg_5302" s="T80">müʔ-luʔ-pi</ta>
            <ta e="T82" id="Seg_5303" s="T81">dʼabə-luʔ-pi</ta>
            <ta e="T83" id="Seg_5304" s="T82">onʼiʔ</ta>
            <ta e="T84" id="Seg_5305" s="T83">pʼero</ta>
            <ta e="T86" id="Seg_5306" s="T85">deʔ-pi</ta>
            <ta e="T87" id="Seg_5307" s="T86">dĭgəttə</ta>
            <ta e="T88" id="Seg_5308" s="T87">šo-bi</ta>
            <ta e="T89" id="Seg_5309" s="T88">aba-nə</ta>
            <ta e="T90" id="Seg_5310" s="T89">no</ta>
            <ta e="T91" id="Seg_5311" s="T90">ĭmbi</ta>
            <ta e="T92" id="Seg_5312" s="T91">nʼi-m</ta>
            <ta e="T94" id="Seg_5313" s="T93">măn</ta>
            <ta e="T95" id="Seg_5314" s="T94">ku-bio-m</ta>
            <ta e="T96" id="Seg_5315" s="T95">šində</ta>
            <ta e="T97" id="Seg_5316" s="T96">tojir-laʔbə</ta>
            <ta e="T98" id="Seg_5317" s="T97">dö</ta>
            <ta e="T99" id="Seg_5318" s="T98">tănan</ta>
            <ta e="T100" id="Seg_5319" s="T99">deʔ-pie-m</ta>
            <ta e="T101" id="Seg_5320" s="T100">pʼero</ta>
            <ta e="T102" id="Seg_5321" s="T101">dĭ</ta>
            <ta e="T106" id="Seg_5322" s="T105">mĭl-leʔbə</ta>
            <ta e="T107" id="Seg_5323" s="T106">i</ta>
            <ta e="T108" id="Seg_5324" s="T107">tojir-laʔbə</ta>
            <ta e="T109" id="Seg_5325" s="T108">dĭgəttə</ta>
            <ta e="T110" id="Seg_5326" s="T109">aba-t</ta>
            <ta e="T112" id="Seg_5327" s="T111">am-zittə</ta>
            <ta e="T113" id="Seg_5328" s="T112">dĭgəttə</ta>
            <ta e="T114" id="Seg_5329" s="T113">tenö-bi</ta>
            <ta e="T115" id="Seg_5330" s="T114">tenö-bi</ta>
            <ta e="T116" id="Seg_5331" s="T115">kan-gaʔ</ta>
            <ta e="T117" id="Seg_5332" s="T116">nʼi-zeŋ</ta>
            <ta e="T118" id="Seg_5333" s="T117">gibər=nʼibudʼ</ta>
            <ta e="T119" id="Seg_5334" s="T118">bar</ta>
            <ta e="T120" id="Seg_5335" s="T119">dʼü</ta>
            <ta e="T121" id="Seg_5336" s="T120">mĭn-geʔ</ta>
            <ta e="T122" id="Seg_5337" s="T121">il</ta>
            <ta e="T126" id="Seg_5338" s="T125">možet</ta>
            <ta e="T127" id="Seg_5339" s="T126">gijən</ta>
            <ta e="T130" id="Seg_5340" s="T129">gijən=nʼibudʼ</ta>
            <ta e="T131" id="Seg_5341" s="T130">dĭ</ta>
            <ta e="T132" id="Seg_5342" s="T131">süjö</ta>
            <ta e="T136" id="Seg_5343" s="T135">dʼap-lə-laʔ</ta>
            <ta e="T137" id="Seg_5344" s="T136">girgit</ta>
            <ta e="T138" id="Seg_5345" s="T137">šü</ta>
            <ta e="T139" id="Seg_5346" s="T138">dĭ-zeŋ</ta>
            <ta e="T141" id="Seg_5347" s="T140">kam-bi-ʔi</ta>
            <ta e="T142" id="Seg_5348" s="T141">onʼiʔ</ta>
            <ta e="T143" id="Seg_5349" s="T142">dibər</ta>
            <ta e="T144" id="Seg_5350" s="T143">baška</ta>
            <ta e="T145" id="Seg_5351" s="T144">döbər</ta>
            <ta e="T146" id="Seg_5352" s="T145">Vanʼuška</ta>
            <ta e="T147" id="Seg_5353" s="T146">tože</ta>
            <ta e="T148" id="Seg_5354" s="T147">kam-bi</ta>
            <ta e="T149" id="Seg_5355" s="T148">kabarləj</ta>
            <ta e="T150" id="Seg_5356" s="T149">dĭgəttə</ta>
            <ta e="T151" id="Seg_5357" s="T150">dĭ</ta>
            <ta e="T152" id="Seg_5358" s="T151">Ivan_Tsarevič</ta>
            <ta e="T153" id="Seg_5359" s="T152">kam-bi</ta>
            <ta e="T154" id="Seg_5360" s="T153">šo-bi</ta>
            <ta e="T155" id="Seg_5361" s="T154">šo-bi</ta>
            <ta e="T156" id="Seg_5362" s="T155">ine-bə</ta>
            <ta e="T157" id="Seg_5363" s="T156">körer-luʔ-pi</ta>
            <ta e="T158" id="Seg_5364" s="T157">am-bi</ta>
            <ta e="T160" id="Seg_5365" s="T159">iʔbə-bi</ta>
            <ta e="T161" id="Seg_5366" s="T160">kunol-zittə</ta>
            <ta e="T162" id="Seg_5367" s="T161">ine-t</ta>
            <ta e="T163" id="Seg_5368" s="T162">mĭl-leʔbə</ta>
            <ta e="T164" id="Seg_5369" s="T163">uju-bə</ta>
            <ta e="T165" id="Seg_5370" s="T164">sar-bi</ta>
            <ta e="T166" id="Seg_5371" s="T165">kundʼo</ta>
            <ta e="T167" id="Seg_5372" s="T166">kunol-bi</ta>
            <ta e="T168" id="Seg_5373" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_5374" s="T168">uʔbdə-bi</ta>
            <ta e="T170" id="Seg_5375" s="T169">ku-lio-t</ta>
            <ta e="T171" id="Seg_5376" s="T170">ine-t</ta>
            <ta e="T172" id="Seg_5377" s="T171">naga</ta>
            <ta e="T173" id="Seg_5378" s="T172">măndə-r-bi</ta>
            <ta e="T174" id="Seg_5379" s="T173">măndə-r-bi</ta>
            <ta e="T175" id="Seg_5380" s="T174">tolʼko</ta>
            <ta e="T176" id="Seg_5381" s="T175">le-ʔi</ta>
            <ta e="T177" id="Seg_5382" s="T176">iʔbo-laʔbə-ʔjə</ta>
            <ta e="T178" id="Seg_5383" s="T177">amno-laʔbə</ta>
            <ta e="T179" id="Seg_5384" s="T178">da</ta>
            <ta e="T180" id="Seg_5385" s="T179">dĭgəttə</ta>
            <ta e="T181" id="Seg_5386" s="T180">nada</ta>
            <ta e="T182" id="Seg_5387" s="T181">kan-zittə</ta>
            <ta e="T183" id="Seg_5388" s="T182">üdʼi</ta>
            <ta e="T184" id="Seg_5389" s="T183">kandə-ga</ta>
            <ta e="T185" id="Seg_5390" s="T184">kandə-ga</ta>
            <ta e="T186" id="Seg_5391" s="T185">dĭgəttə</ta>
            <ta e="T187" id="Seg_5392" s="T186">tarar-luʔ-pi</ta>
            <ta e="T188" id="Seg_5393" s="T187">da</ta>
            <ta e="T189" id="Seg_5394" s="T188">bar</ta>
            <ta e="T190" id="Seg_5395" s="T189">amnə-luʔ-pi</ta>
            <ta e="T191" id="Seg_5396" s="T190">amno-laʔbə</ta>
            <ta e="T192" id="Seg_5397" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_5398" s="T192">volk</ta>
            <ta e="T194" id="Seg_5399" s="T193">nuʔmə-leʔbə</ta>
            <ta e="T195" id="Seg_5400" s="T194">ĭmbi</ta>
            <ta e="T196" id="Seg_5401" s="T195">amno-laʔbə-l</ta>
            <ta e="T197" id="Seg_5402" s="T196">ulu-l</ta>
            <ta e="T198" id="Seg_5403" s="T197">edə-bio-m</ta>
            <ta e="T199" id="Seg_5404" s="T198">da</ta>
            <ta e="T200" id="Seg_5405" s="T199">măn</ta>
            <ta e="T201" id="Seg_5406" s="T200">ine-m</ta>
            <ta e="T202" id="Seg_5407" s="T201">am-nuʔ-pi</ta>
            <ta e="T203" id="Seg_5408" s="T202">a</ta>
            <ta e="T204" id="Seg_5409" s="T203">dĭ</ta>
            <ta e="T205" id="Seg_5410" s="T204">măn</ta>
            <ta e="T206" id="Seg_5411" s="T205">am-bia-m</ta>
            <ta e="T207" id="Seg_5412" s="T206">dak</ta>
            <ta e="T209" id="Seg_5413" s="T208">a</ta>
            <ta e="T210" id="Seg_5414" s="T209">gibər</ta>
            <ta e="T211" id="Seg_5415" s="T210">tăn</ta>
            <ta e="T212" id="Seg_5416" s="T211">kandə-ga-l</ta>
            <ta e="T213" id="Seg_5417" s="T212">da</ta>
            <ta e="T214" id="Seg_5418" s="T213">kandə-ga-m</ta>
            <ta e="T215" id="Seg_5419" s="T214">süjö</ta>
            <ta e="T216" id="Seg_5420" s="T215">süjö</ta>
            <ta e="T218" id="Seg_5421" s="T217">măndə-r-zittə</ta>
            <ta e="T219" id="Seg_5422" s="T218">aba-m</ta>
            <ta e="T220" id="Seg_5423" s="T219">măn-də</ta>
            <ta e="T221" id="Seg_5424" s="T220">măndə-r-a-ʔ</ta>
            <ta e="T222" id="Seg_5425" s="T221">süjö</ta>
            <ta e="T223" id="Seg_5426" s="T222">šü</ta>
            <ta e="T224" id="Seg_5427" s="T223">dĭrgit</ta>
            <ta e="T225" id="Seg_5428" s="T224">dĭgəttə</ta>
            <ta e="T226" id="Seg_5429" s="T225">dĭ</ta>
            <ta e="T227" id="Seg_5430" s="T226">no</ta>
            <ta e="T228" id="Seg_5431" s="T227">tăn</ta>
            <ta e="T229" id="Seg_5432" s="T228">bos-tə</ta>
            <ta e="T230" id="Seg_5433" s="T229">ine-l-ziʔ</ta>
            <ta e="T231" id="Seg_5434" s="T230">dăk</ta>
            <ta e="T232" id="Seg_5435" s="T231">nagur</ta>
            <ta e="T233" id="Seg_5436" s="T232">kö</ta>
            <ta e="T234" id="Seg_5437" s="T233">dĭbər</ta>
            <ta e="T235" id="Seg_5438" s="T234">ej</ta>
            <ta e="T236" id="Seg_5439" s="T235">kal-la-l</ta>
            <ta e="T237" id="Seg_5440" s="T236">amna-ʔ</ta>
            <ta e="T239" id="Seg_5441" s="T238">măna</ta>
            <ta e="T240" id="Seg_5442" s="T239">măn</ta>
            <ta e="T241" id="Seg_5443" s="T240">tănan</ta>
            <ta e="T242" id="Seg_5444" s="T241">kun-naːl-li-m</ta>
            <ta e="T243" id="Seg_5445" s="T242">amnə-bi</ta>
            <ta e="T244" id="Seg_5446" s="T243">dĭgəttə</ta>
            <ta e="T245" id="Seg_5447" s="T244">dĭ</ta>
            <ta e="T246" id="Seg_5448" s="T245">deʔ-pi</ta>
            <ta e="T247" id="Seg_5449" s="T246">kan-a-ʔ</ta>
            <ta e="T248" id="Seg_5450" s="T247">tuj</ta>
            <ta e="T249" id="Seg_5451" s="T248">bar</ta>
            <ta e="T250" id="Seg_5452" s="T249">il</ta>
            <ta e="T251" id="Seg_5453" s="T250">kunol-laʔbə-ʔjə</ta>
            <ta e="T252" id="Seg_5454" s="T251">i-ʔ</ta>
            <ta e="T253" id="Seg_5455" s="T252">dĭ</ta>
            <ta e="T254" id="Seg_5456" s="T253">süjö</ta>
            <ta e="T255" id="Seg_5457" s="T254">a</ta>
            <ta e="T256" id="Seg_5458" s="T255">klʼetka-bə</ta>
            <ta e="T257" id="Seg_5459" s="T256">i-ʔ</ta>
            <ta e="T258" id="Seg_5460" s="T257">i-t</ta>
            <ta e="T260" id="Seg_5461" s="T258">ato</ta>
            <ta e="T261" id="Seg_5462" s="T260">dĭgəttə</ta>
            <ta e="T262" id="Seg_5463" s="T261">dĭ</ta>
            <ta e="T263" id="Seg_5464" s="T262">kam-bi</ta>
            <ta e="T264" id="Seg_5465" s="T263">i-bi</ta>
            <ta e="T265" id="Seg_5466" s="T264">süjö</ta>
            <ta e="T266" id="Seg_5467" s="T265">măndə-r-lia</ta>
            <ta e="T267" id="Seg_5468" s="T266">klʼetka</ta>
            <ta e="T268" id="Seg_5469" s="T267">kuvas</ta>
            <ta e="T269" id="Seg_5470" s="T268">nada</ta>
            <ta e="T270" id="Seg_5471" s="T269">i-zittə</ta>
            <ta e="T271" id="Seg_5472" s="T270">kak</ta>
            <ta e="T272" id="Seg_5473" s="T271">dĭ-m</ta>
            <ta e="T273" id="Seg_5474" s="T272">mĭngə-luʔ-pi</ta>
            <ta e="T274" id="Seg_5475" s="T273">bar</ta>
            <ta e="T275" id="Seg_5476" s="T274">kuzur-luʔ-pi</ta>
            <ta e="T276" id="Seg_5477" s="T275">bar</ta>
            <ta e="T277" id="Seg_5478" s="T276">dĭ-m</ta>
            <ta e="T278" id="Seg_5479" s="T277">dʼabə-luʔ-pi</ta>
            <ta e="T279" id="Seg_5480" s="T278">i</ta>
            <ta e="T282" id="Seg_5481" s="T281">kum-bi-ʔi</ta>
            <ta e="T283" id="Seg_5482" s="T282">koŋ-də</ta>
            <ta e="T284" id="Seg_5483" s="T283">kabarləj</ta>
            <ta e="T285" id="Seg_5484" s="T284">dĭgəttə</ta>
            <ta e="T286" id="Seg_5485" s="T285">dĭ</ta>
            <ta e="T287" id="Seg_5486" s="T286">šo-bi</ta>
            <ta e="T288" id="Seg_5487" s="T287">koŋ-də</ta>
            <ta e="T289" id="Seg_5488" s="T288">tăn</ta>
            <ta e="T290" id="Seg_5489" s="T289">ĭmbi</ta>
            <ta e="T291" id="Seg_5490" s="T290">ĭmbi</ta>
            <ta e="T292" id="Seg_5491" s="T291">döbər</ta>
            <ta e="T293" id="Seg_5492" s="T292">šo-bia-m</ta>
            <ta e="T294" id="Seg_5493" s="T293">tojir-zittə</ta>
            <ta e="T295" id="Seg_5494" s="T294">šindi-n</ta>
            <ta e="T296" id="Seg_5495" s="T295">tăn</ta>
            <ta e="T297" id="Seg_5496" s="T296">nʼi</ta>
            <ta e="T298" id="Seg_5497" s="T297">măn</ta>
            <ta e="T299" id="Seg_5498" s="T298">Ivan_Tsarevič</ta>
            <ta e="T300" id="Seg_5499" s="T299">dăk</ta>
            <ta e="T301" id="Seg_5500" s="T300">tăn</ta>
            <ta e="T302" id="Seg_5501" s="T301">tojir-zittə</ta>
            <ta e="T304" id="Seg_5502" s="T303">dĭgəttə</ta>
            <ta e="T305" id="Seg_5503" s="T304">dĭ</ta>
            <ta e="T306" id="Seg_5504" s="T305">măn-də</ta>
            <ta e="T307" id="Seg_5505" s="T306">a</ta>
            <ta e="T308" id="Seg_5506" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_5507" s="T308">miʔnʼibeʔ</ta>
            <ta e="T310" id="Seg_5508" s="T309">mĭm-bi</ta>
            <ta e="T311" id="Seg_5509" s="T310">da</ta>
            <ta e="T313" id="Seg_5510" s="T312">am-bi-ʔi</ta>
            <ta e="T314" id="Seg_5511" s="T313">a</ta>
            <ta e="T315" id="Seg_5512" s="T314">tăn</ta>
            <ta e="T316" id="Seg_5513" s="T315">šo-bia-l</ta>
            <ta e="T317" id="Seg_5514" s="T316">bɨ</ta>
            <ta e="T318" id="Seg_5515" s="T317">a</ta>
            <ta e="T319" id="Seg_5516" s="T318">măn</ta>
            <ta e="T320" id="Seg_5517" s="T319">dăre</ta>
            <ta e="T321" id="Seg_5518" s="T320">tănan</ta>
            <ta e="T322" id="Seg_5519" s="T321">mĭ-bie-m</ta>
            <ta e="T323" id="Seg_5520" s="T322">bɨ</ta>
            <ta e="T324" id="Seg_5521" s="T323">ato</ta>
            <ta e="T325" id="Seg_5522" s="T324">tojir-zittə</ta>
            <ta e="T326" id="Seg_5523" s="T325">šo-bia-l</ta>
            <ta e="T327" id="Seg_5524" s="T326">no</ta>
            <ta e="T328" id="Seg_5525" s="T327">măn</ta>
            <ta e="T329" id="Seg_5526" s="T328">tănan</ta>
            <ta e="T330" id="Seg_5527" s="T329">ĭmbi=də</ta>
            <ta e="T331" id="Seg_5528" s="T330">ej</ta>
            <ta e="T332" id="Seg_5529" s="T331">a-la-m</ta>
            <ta e="T333" id="Seg_5530" s="T332">kan-a-ʔ</ta>
            <ta e="T334" id="Seg_5531" s="T333">ine</ta>
            <ta e="T335" id="Seg_5532" s="T334">de-ʔ</ta>
            <ta e="T337" id="Seg_5533" s="T336">griva-zʼiʔ</ta>
            <ta e="T338" id="Seg_5534" s="T337">dĭgəttə</ta>
            <ta e="T339" id="Seg_5535" s="T338">dĭ</ta>
            <ta e="T340" id="Seg_5536" s="T339">kam-bi</ta>
            <ta e="T341" id="Seg_5537" s="T340">volk-tə</ta>
            <ta e="T342" id="Seg_5538" s="T341">volk</ta>
            <ta e="T343" id="Seg_5539" s="T342">măn-də</ta>
            <ta e="T344" id="Seg_5540" s="T343">măn</ta>
            <ta e="T345" id="Seg_5541" s="T344">măm-bia-m</ta>
            <ta e="T346" id="Seg_5542" s="T345">tănan</ta>
            <ta e="T347" id="Seg_5543" s="T346">i-ʔ</ta>
            <ta e="T348" id="Seg_5544" s="T347">üžəm-e-ʔ</ta>
            <ta e="T349" id="Seg_5545" s="T348">a</ta>
            <ta e="T350" id="Seg_5546" s="T349">tăn</ta>
            <ta e="T351" id="Seg_5547" s="T350">üžəm-bie-l</ta>
            <ta e="T352" id="Seg_5548" s="T351">a</ta>
            <ta e="T353" id="Seg_5549" s="T352">tüj</ta>
            <ta e="T355" id="Seg_5550" s="T354">amna-ʔ</ta>
            <ta e="T356" id="Seg_5551" s="T355">măna</ta>
            <ta e="T357" id="Seg_5552" s="T356">kan-žə-bəj</ta>
            <ta e="T358" id="Seg_5553" s="T357">kam-bi-ʔi</ta>
            <ta e="T359" id="Seg_5554" s="T358">no</ta>
            <ta e="T360" id="Seg_5555" s="T359">šo-bi-ʔi</ta>
            <ta e="T361" id="Seg_5556" s="T360">dibər</ta>
            <ta e="T362" id="Seg_5557" s="T361">dĭgəttə</ta>
            <ta e="T363" id="Seg_5558" s="T362">no</ta>
            <ta e="T364" id="Seg_5559" s="T363">kan-a-ʔ</ta>
            <ta e="T365" id="Seg_5560" s="T364">da</ta>
            <ta e="T366" id="Seg_5561" s="T365">dʼabi-t</ta>
            <ta e="T367" id="Seg_5562" s="T366">ine-m</ta>
            <ta e="T368" id="Seg_5563" s="T367">a</ta>
            <ta e="T369" id="Seg_5564" s="T368">uzda-m</ta>
            <ta e="T370" id="Seg_5565" s="T369">ej</ta>
            <ta e="T372" id="Seg_5566" s="T371">i-ʔ</ta>
            <ta e="T374" id="Seg_5567" s="T373">i-ʔ</ta>
            <ta e="T376" id="Seg_5568" s="T375">i-ʔ</ta>
            <ta e="T377" id="Seg_5569" s="T376">i-ʔ</ta>
            <ta e="T378" id="Seg_5570" s="T377">ato</ta>
            <ta e="T379" id="Seg_5571" s="T378">bazoʔ</ta>
            <ta e="T380" id="Seg_5572" s="T379">dăre</ta>
            <ta e="T381" id="Seg_5573" s="T380">mo-laːl-lə-j</ta>
            <ta e="T382" id="Seg_5574" s="T381">dĭ</ta>
            <ta e="T383" id="Seg_5575" s="T382">šo-bi</ta>
            <ta e="T384" id="Seg_5576" s="T383">ine-m</ta>
            <ta e="T385" id="Seg_5577" s="T384">i-bi</ta>
            <ta e="T386" id="Seg_5578" s="T385">a</ta>
            <ta e="T387" id="Seg_5579" s="T386">uzda-t</ta>
            <ta e="T388" id="Seg_5580" s="T387">ugaːndə</ta>
            <ta e="T389" id="Seg_5581" s="T388">kuvas</ta>
            <ta e="T390" id="Seg_5582" s="T389">ĭmbi</ta>
            <ta e="T391" id="Seg_5583" s="T390">ine-gən</ta>
            <ta e="T392" id="Seg_5584" s="T391">nada</ta>
            <ta e="T393" id="Seg_5585" s="T392">šer-zittə</ta>
            <ta e="T394" id="Seg_5586" s="T393">tolʼko</ta>
            <ta e="T395" id="Seg_5587" s="T394">i-bi</ta>
            <ta e="T396" id="Seg_5588" s="T395">dĭ-m</ta>
            <ta e="T397" id="Seg_5589" s="T396">dĭ</ta>
            <ta e="T398" id="Seg_5590" s="T397">bar</ta>
            <ta e="T399" id="Seg_5591" s="T398">kirgar-luʔ-pi</ta>
            <ta e="T400" id="Seg_5592" s="T399">il</ta>
            <ta e="T401" id="Seg_5593" s="T400">suʔmi-luʔ-pi</ta>
            <ta e="T402" id="Seg_5594" s="T401">dĭ-m</ta>
            <ta e="T403" id="Seg_5595" s="T402">i-bi-ʔi</ta>
            <ta e="T405" id="Seg_5596" s="T404">kun-naːm-bi-ʔi</ta>
            <ta e="T406" id="Seg_5597" s="T405">koŋ-də</ta>
            <ta e="T407" id="Seg_5598" s="T406">ĭmbi</ta>
            <ta e="T408" id="Seg_5599" s="T407">šo-bia-l</ta>
            <ta e="T409" id="Seg_5600" s="T408">ine</ta>
            <ta e="T410" id="Seg_5601" s="T409">tojir-zittə</ta>
            <ta e="T411" id="Seg_5602" s="T410">šindi-n</ta>
            <ta e="T412" id="Seg_5603" s="T411">tăn</ta>
            <ta e="T413" id="Seg_5604" s="T412">măn</ta>
            <ta e="T414" id="Seg_5605" s="T413">măn</ta>
            <ta e="T415" id="Seg_5606" s="T414">Ivan_Tsarevič</ta>
            <ta e="T416" id="Seg_5607" s="T415">dăk</ta>
            <ta e="T417" id="Seg_5608" s="T416">tojir-zittə</ta>
            <ta e="T419" id="Seg_5609" s="T418">šo-bia-l</ta>
            <ta e="T420" id="Seg_5610" s="T419">no</ta>
            <ta e="T421" id="Seg_5611" s="T420">kan-a-ʔ</ta>
            <ta e="T422" id="Seg_5612" s="T421">măn</ta>
            <ta e="T423" id="Seg_5613" s="T422">tănan</ta>
            <ta e="T424" id="Seg_5614" s="T423">ĭmbi=də</ta>
            <ta e="T425" id="Seg_5615" s="T424">ej</ta>
            <ta e="T426" id="Seg_5616" s="T425">a-la-m</ta>
            <ta e="T427" id="Seg_5617" s="T426">dĭn</ta>
            <ta e="T428" id="Seg_5618" s="T427">i-ge</ta>
            <ta e="T429" id="Seg_5619" s="T428">koʔbdo</ta>
            <ta e="T432" id="Seg_5620" s="T431">det-tə</ta>
            <ta e="T433" id="Seg_5621" s="T432">măna</ta>
            <ta e="T434" id="Seg_5622" s="T433">dĭ-m</ta>
            <ta e="T435" id="Seg_5623" s="T434">dĭgəttə</ta>
            <ta e="T436" id="Seg_5624" s="T435">dĭ</ta>
            <ta e="T438" id="Seg_5625" s="T437">šonə-ga</ta>
            <ta e="T439" id="Seg_5626" s="T438">a</ta>
            <ta e="T440" id="Seg_5627" s="T439">volk-tə</ta>
            <ta e="T441" id="Seg_5628" s="T440">măm-bia-m</ta>
            <ta e="T442" id="Seg_5629" s="T441">i-ʔ</ta>
            <ta e="T443" id="Seg_5630" s="T442">üžəm-e-ʔ</ta>
            <ta e="T444" id="Seg_5631" s="T443">a</ta>
            <ta e="T445" id="Seg_5632" s="T444">tăn</ta>
            <ta e="T446" id="Seg_5633" s="T445">üge</ta>
            <ta e="T447" id="Seg_5634" s="T446">üžəm-neʔ-pie-m</ta>
            <ta e="T448" id="Seg_5635" s="T447">no</ta>
            <ta e="T449" id="Seg_5636" s="T448">ĭmbi=də</ta>
            <ta e="T450" id="Seg_5637" s="T449">emnajaʔa</ta>
            <ta e="T451" id="Seg_5638" s="T450">dĭ</ta>
            <ta e="T452" id="Seg_5639" s="T451">măn-də</ta>
            <ta e="T453" id="Seg_5640" s="T452">no</ta>
            <ta e="T454" id="Seg_5641" s="T453">amna-ʔ</ta>
            <ta e="T455" id="Seg_5642" s="T454">kan-žə-bəj</ta>
            <ta e="T456" id="Seg_5643" s="T455">šo-bi-ʔi</ta>
            <ta e="T457" id="Seg_5644" s="T456">dibər</ta>
            <ta e="T458" id="Seg_5645" s="T457">dĭgəttə</ta>
            <ta e="T459" id="Seg_5646" s="T458">volk</ta>
            <ta e="T460" id="Seg_5647" s="T459">măn-də</ta>
            <ta e="T461" id="Seg_5648" s="T460">dĭʔ-nə</ta>
            <ta e="T462" id="Seg_5649" s="T461">tăn</ta>
            <ta e="T464" id="Seg_5650" s="T463">kan-a-ʔ</ta>
            <ta e="T465" id="Seg_5651" s="T464">par-a-ʔ</ta>
            <ta e="T466" id="Seg_5652" s="T465">par-a-ʔ</ta>
            <ta e="T467" id="Seg_5653" s="T466">a</ta>
            <ta e="T468" id="Seg_5654" s="T467">măn</ta>
            <ta e="T469" id="Seg_5655" s="T468">kal-la-m</ta>
            <ta e="T470" id="Seg_5656" s="T469">bos-tə</ta>
            <ta e="T471" id="Seg_5657" s="T470">dĭgəttə</ta>
            <ta e="T472" id="Seg_5658" s="T471">suʔmə-luʔ-pi</ta>
            <ta e="T473" id="Seg_5659" s="T472">sat-tə</ta>
            <ta e="T474" id="Seg_5660" s="T473">dĭn</ta>
            <ta e="T475" id="Seg_5661" s="T474">šaʔ-laːm-bi</ta>
            <ta e="T476" id="Seg_5662" s="T475">dĭ-zeŋ</ta>
            <ta e="T477" id="Seg_5663" s="T476">mĭn-zə-leʔbə-ʔjə</ta>
            <ta e="T478" id="Seg_5664" s="T477">ija-t</ta>
            <ta e="T479" id="Seg_5665" s="T478">nʼanʼka-ʔi</ta>
            <ta e="T480" id="Seg_5666" s="T479">dĭ</ta>
            <ta e="T481" id="Seg_5667" s="T480">Elena-t</ta>
            <ta e="T482" id="Seg_5668" s="T481">ma-luʔ-pi</ta>
            <ta e="T483" id="Seg_5669" s="T482">dĭ</ta>
            <ta e="T484" id="Seg_5670" s="T483">dĭ-m</ta>
            <ta e="T485" id="Seg_5671" s="T484">kabar-luʔ-pi</ta>
            <ta e="T486" id="Seg_5672" s="T485">i</ta>
            <ta e="T487" id="Seg_5673" s="T486">nuʔmə-luʔ-pi</ta>
            <ta e="T488" id="Seg_5674" s="T487">vot</ta>
            <ta e="T489" id="Seg_5675" s="T488">nuʔmə-laʔbə</ta>
            <ta e="T490" id="Seg_5676" s="T489">i</ta>
            <ta e="T491" id="Seg_5677" s="T490">bĭdə-bi</ta>
            <ta e="T492" id="Seg_5678" s="T491">Ivan-əm</ta>
            <ta e="T493" id="Seg_5679" s="T492">amna-ʔ</ta>
            <ta e="T494" id="Seg_5680" s="T493">amnəl-bi-ʔi</ta>
            <ta e="T495" id="Seg_5681" s="T494">dĭgəttə</ta>
            <ta e="T496" id="Seg_5682" s="T495">šo-bi-ʔi</ta>
            <ta e="T497" id="Seg_5683" s="T496">dibər</ta>
            <ta e="T498" id="Seg_5684" s="T497">gijen</ta>
            <ta e="T500" id="Seg_5685" s="T499">ine</ta>
            <ta e="T501" id="Seg_5686" s="T500">pʼe-bi-ʔi</ta>
            <ta e="T502" id="Seg_5687" s="T501">dĭgəttə</ta>
            <ta e="T503" id="Seg_5688" s="T502">dĭ-zeŋ</ta>
            <ta e="T504" id="Seg_5689" s="T503">šo-bi-ʔi</ta>
            <ta e="T505" id="Seg_5690" s="T504">a</ta>
            <ta e="T506" id="Seg_5691" s="T505">dĭ</ta>
            <ta e="T507" id="Seg_5692" s="T506">tenö-leʔbə</ta>
            <ta e="T508" id="Seg_5693" s="T507">üge</ta>
            <ta e="T509" id="Seg_5694" s="T508">Ivan_Tsarevič</ta>
            <ta e="T510" id="Seg_5695" s="T509">ajir-ia</ta>
            <ta e="T511" id="Seg_5696" s="T510">dĭ</ta>
            <ta e="T512" id="Seg_5697" s="T511">koʔbdo-m</ta>
            <ta e="T513" id="Seg_5698" s="T512">a</ta>
            <ta e="T514" id="Seg_5699" s="T513">volk-tə</ta>
            <ta e="T515" id="Seg_5700" s="T514">surar-laʔbə</ta>
            <ta e="T516" id="Seg_5701" s="T515">ĭmbi</ta>
            <ta e="T517" id="Seg_5702" s="T516">tăn</ta>
            <ta e="T518" id="Seg_5703" s="T517">dĭn</ta>
            <ta e="T519" id="Seg_5704" s="T518">nu-laʔbə-l</ta>
            <ta e="T520" id="Seg_5705" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_5706" s="T520">dĭrgit</ta>
            <ta e="T522" id="Seg_5707" s="T521">kuvas</ta>
            <ta e="T523" id="Seg_5708" s="T522">koʔbdo</ta>
            <ta e="T524" id="Seg_5709" s="T523">măn</ta>
            <ta e="T525" id="Seg_5710" s="T524">ajir-bio-m</ta>
            <ta e="T527" id="Seg_5711" s="T526">mĭ-zittə</ta>
            <ta e="T528" id="Seg_5712" s="T527">no</ta>
            <ta e="T529" id="Seg_5713" s="T528">šaʔbdə-l-bəj</ta>
            <ta e="T530" id="Seg_5714" s="T529">a</ta>
            <ta e="T531" id="Seg_5715" s="T530">măn</ta>
            <ta e="T533" id="Seg_5716" s="T532">măn</ta>
            <ta e="T534" id="Seg_5717" s="T533">a-laʔbə-m</ta>
            <ta e="T536" id="Seg_5718" s="T535">dĭʔ-nə</ta>
            <ta e="T537" id="Seg_5719" s="T536">dĭgəttə</ta>
            <ta e="T538" id="Seg_5720" s="T537">bos-tə</ta>
            <ta e="T539" id="Seg_5721" s="T538">bögəl-də</ta>
            <ta e="T540" id="Seg_5722" s="T539">suʔmə-luʔ-pi</ta>
            <ta e="T541" id="Seg_5723" s="T540">dĭ</ta>
            <ta e="T542" id="Seg_5724" s="T541">dĭ-m</ta>
            <ta e="T543" id="Seg_5725" s="T542">i-bi</ta>
            <ta e="T544" id="Seg_5726" s="T543">kun-naːm-bi</ta>
            <ta e="T545" id="Seg_5727" s="T544">a</ta>
            <ta e="T546" id="Seg_5728" s="T545">dĭ</ta>
            <ta e="T547" id="Seg_5729" s="T546">i-bi</ta>
            <ta e="T548" id="Seg_5730" s="T547">dĭ</ta>
            <ta e="T549" id="Seg_5731" s="T548">koʔbdo-m</ta>
            <ta e="T550" id="Seg_5732" s="T549">dʼansaʔməj</ta>
            <ta e="T551" id="Seg_5733" s="T550">ara</ta>
            <ta e="T552" id="Seg_5734" s="T551">bĭʔ-pi-ʔi</ta>
            <ta e="T553" id="Seg_5735" s="T552">suʔmi-leʔbə-ʔi</ta>
            <ta e="T554" id="Seg_5736" s="T553">ine-bə</ta>
            <ta e="T555" id="Seg_5737" s="T554">mĭ-bi</ta>
            <ta e="T556" id="Seg_5738" s="T555">dĭ</ta>
            <ta e="T557" id="Seg_5739" s="T556">ine-bə</ta>
            <ta e="T558" id="Seg_5740" s="T557">i-bi</ta>
            <ta e="T559" id="Seg_5741" s="T558">Elena-bə</ta>
            <ta e="T560" id="Seg_5742" s="T559">amnəl-bə-bi</ta>
            <ta e="T561" id="Seg_5743" s="T560">bos-tə</ta>
            <ta e="T562" id="Seg_5744" s="T561">amnəl-bi</ta>
            <ta e="T563" id="Seg_5745" s="T562">kam-bi-ʔi</ta>
            <ta e="T564" id="Seg_5746" s="T563">a</ta>
            <ta e="T565" id="Seg_5747" s="T564">nüdʼi-n</ta>
            <ta e="T567" id="Seg_5748" s="T566">iʔbə-ʔi</ta>
            <ta e="T568" id="Seg_5749" s="T567">kunol-zittə</ta>
            <ta e="T569" id="Seg_5750" s="T568">dĭ</ta>
            <ta e="T571" id="Seg_5751" s="T570">dĭ</ta>
            <ta e="T572" id="Seg_5752" s="T571">Elena-t</ta>
            <ta e="T574" id="Seg_5753" s="T573">mo-laːm-bi</ta>
            <ta e="T575" id="Seg_5754" s="T574">volk-ziʔ</ta>
            <ta e="T576" id="Seg_5755" s="T575">i</ta>
            <ta e="T577" id="Seg_5756" s="T576">dĭ</ta>
            <ta e="T578" id="Seg_5757" s="T577">nerö-lüʔ-pi</ta>
            <ta e="T579" id="Seg_5758" s="T578">i</ta>
            <ta e="T580" id="Seg_5759" s="T579">saʔmə-luʔ-pi</ta>
            <ta e="T581" id="Seg_5760" s="T580">a</ta>
            <ta e="T582" id="Seg_5761" s="T581">dĭ</ta>
            <ta e="T583" id="Seg_5762" s="T582">nuʔmə-luʔ-pi</ta>
            <ta e="T584" id="Seg_5763" s="T583">dĭgəttə</ta>
            <ta e="T585" id="Seg_5764" s="T584">bĭdə-lie-t</ta>
            <ta e="T586" id="Seg_5765" s="T585">Ivanuška</ta>
            <ta e="T587" id="Seg_5766" s="T586">Elenuška-ʔi-ziʔ</ta>
            <ta e="T588" id="Seg_5767" s="T587">ĭmbi</ta>
            <ta e="T589" id="Seg_5768" s="T588">tăn</ta>
            <ta e="T590" id="Seg_5769" s="T589">tĭn</ta>
            <ta e="T591" id="Seg_5770" s="T590">nu-laʔ-lie-l</ta>
            <ta e="T592" id="Seg_5771" s="T591">da</ta>
            <ta e="T593" id="Seg_5772" s="T592">nʼe</ta>
            <ta e="T594" id="Seg_5773" s="T593">axota</ta>
            <ta e="T595" id="Seg_5774" s="T594">mĭ-zittə</ta>
            <ta e="T596" id="Seg_5775" s="T595">ine</ta>
            <ta e="T597" id="Seg_5776" s="T596">kuvas</ta>
            <ta e="T598" id="Seg_5777" s="T597">a</ta>
            <ta e="T599" id="Seg_5778" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_5779" s="T599">măn-də</ta>
            <ta e="T601" id="Seg_5780" s="T600">šaʔ-laːn-də</ta>
            <ta e="T602" id="Seg_5781" s="T601">ine-m</ta>
            <ta e="T603" id="Seg_5782" s="T602">i</ta>
            <ta e="T604" id="Seg_5783" s="T603">koʔbdo-m</ta>
            <ta e="T605" id="Seg_5784" s="T604">dĭgəttə</ta>
            <ta e="T606" id="Seg_5785" s="T605">dĭ</ta>
            <ta e="T607" id="Seg_5786" s="T606">a-luʔ-pi</ta>
            <ta e="T608" id="Seg_5787" s="T607">ine-ziʔ</ta>
            <ta e="T609" id="Seg_5788" s="T608">dĭ</ta>
            <ta e="T610" id="Seg_5789" s="T609">dĭ-m</ta>
            <ta e="T611" id="Seg_5790" s="T610">kum-bi</ta>
            <ta e="T612" id="Seg_5791" s="T611">dĭ</ta>
            <ta e="T613" id="Seg_5792" s="T612">i-bi</ta>
            <ta e="T614" id="Seg_5793" s="T613">süjö</ta>
            <ta e="T615" id="Seg_5794" s="T614">nenʼinuʔ</ta>
            <ta e="T616" id="Seg_5795" s="T615">kak</ta>
            <ta e="T617" id="Seg_5796" s="T616">šü</ta>
            <ta e="T618" id="Seg_5797" s="T617">i</ta>
            <ta e="T619" id="Seg_5798" s="T618">bos-tə</ta>
            <ta e="T620" id="Seg_5799" s="T619">par-luʔ-pi</ta>
            <ta e="T621" id="Seg_5800" s="T620">amnəl-bi</ta>
            <ta e="T622" id="Seg_5801" s="T621">i</ta>
            <ta e="T623" id="Seg_5802" s="T622">koʔbdo-bə</ta>
            <ta e="T624" id="Seg_5803" s="T623">amnəl-bi</ta>
            <ta e="T625" id="Seg_5804" s="T624">ine-nə</ta>
            <ta e="T626" id="Seg_5805" s="T625">bos-tə</ta>
            <ta e="T627" id="Seg_5806" s="T626">amnəl-bi</ta>
            <ta e="T628" id="Seg_5807" s="T627">kam-bi-ʔi</ta>
            <ta e="T629" id="Seg_5808" s="T628">dĭgəttə</ta>
            <ta e="T630" id="Seg_5809" s="T629">dĭ</ta>
            <ta e="T631" id="Seg_5810" s="T630">koŋ</ta>
            <ta e="T632" id="Seg_5811" s="T631">da</ta>
            <ta e="T633" id="Seg_5812" s="T632">deʔ-keʔ</ta>
            <ta e="T634" id="Seg_5813" s="T633">dĭ</ta>
            <ta e="T635" id="Seg_5814" s="T634">ine</ta>
            <ta e="T636" id="Seg_5815" s="T635">dĭ</ta>
            <ta e="T637" id="Seg_5816" s="T636">ine</ta>
            <ta e="T638" id="Seg_5817" s="T637">šo-bi</ta>
            <ta e="T639" id="Seg_5818" s="T638">volk-ziʔ</ta>
            <ta e="T640" id="Seg_5819" s="T639">a-luʔ-pi</ta>
            <ta e="T641" id="Seg_5820" s="T640">dĭ</ta>
            <ta e="T642" id="Seg_5821" s="T641">nerö-luʔ-pi</ta>
            <ta e="T643" id="Seg_5822" s="T642">i</ta>
            <ta e="T644" id="Seg_5823" s="T643">saʔmə-luʔ-pi</ta>
            <ta e="T645" id="Seg_5824" s="T644">a</ta>
            <ta e="T646" id="Seg_5825" s="T645">dĭ</ta>
            <ta e="T647" id="Seg_5826" s="T646">nuʔmə-luʔ-pi</ta>
            <ta e="T648" id="Seg_5827" s="T647">bĭdə-leʔbə</ta>
            <ta e="T649" id="Seg_5828" s="T648">Vanʼuška-nə</ta>
            <ta e="T650" id="Seg_5829" s="T649">măn-də</ta>
            <ta e="T651" id="Seg_5830" s="T650">no</ta>
            <ta e="T652" id="Seg_5831" s="T651">tüj</ta>
            <ta e="T653" id="Seg_5832" s="T652">măna</ta>
            <ta e="T654" id="Seg_5833" s="T653">kan-zittə</ta>
            <ta e="T655" id="Seg_5834" s="T654">tăn-ziʔ</ta>
            <ta e="T657" id="Seg_5835" s="T656">ma-lə-m</ta>
            <ta e="T658" id="Seg_5836" s="T657">dön</ta>
            <ta e="T659" id="Seg_5837" s="T658">dĭ</ta>
            <ta e="T660" id="Seg_5838" s="T659">suʔmi-luʔ-pi</ta>
            <ta e="T661" id="Seg_5839" s="T660">dĭʔ-nə</ta>
            <ta e="T662" id="Seg_5840" s="T661">nagur-göʔ</ta>
            <ta e="T663" id="Seg_5841" s="T662">dʼü-nə</ta>
            <ta e="T664" id="Seg_5842" s="T663">saʔmə-luʔ-pi</ta>
            <ta e="T665" id="Seg_5843" s="T664">kabarləj</ta>
            <ta e="T666" id="Seg_5844" s="T665">dĭgəttə</ta>
            <ta e="T668" id="Seg_5845" s="T667">dĭ-zeŋ</ta>
            <ta e="T669" id="Seg_5846" s="T668">šo-bi-ʔi</ta>
            <ta e="T670" id="Seg_5847" s="T669">bos-tə</ta>
            <ta e="T672" id="Seg_5848" s="T671">dʼü-nə</ta>
            <ta e="T673" id="Seg_5849" s="T672">măn-də</ta>
            <ta e="T674" id="Seg_5850" s="T673">davaj</ta>
            <ta e="T675" id="Seg_5851" s="T674">amor-lə-bəj</ta>
            <ta e="T676" id="Seg_5852" s="T675">amnə-bəl-bi-ʔi</ta>
            <ta e="T677" id="Seg_5853" s="T676">amor-bi-ʔi</ta>
            <ta e="T678" id="Seg_5854" s="T677">i</ta>
            <ta e="T680" id="Seg_5855" s="T679">iʔ-pi-ʔi</ta>
            <ta e="T681" id="Seg_5856" s="T680">kunol-zittə</ta>
            <ta e="T682" id="Seg_5857" s="T681">dĭgəttə</ta>
            <ta e="T683" id="Seg_5858" s="T682">kaga-zaŋ-də</ta>
            <ta e="T684" id="Seg_5859" s="T683">šo-bi-ʔi</ta>
            <ta e="T685" id="Seg_5860" s="T684">dĭ-m</ta>
            <ta e="T686" id="Seg_5861" s="T685">kut-laːm-bi-ʔi</ta>
            <ta e="T687" id="Seg_5862" s="T686">a</ta>
            <ta e="T688" id="Seg_5863" s="T687">bos-tə</ta>
            <ta e="T689" id="Seg_5864" s="T688">i-bi-ʔi</ta>
            <ta e="T690" id="Seg_5865" s="T689">koʔbdo</ta>
            <ta e="T691" id="Seg_5866" s="T690">i</ta>
            <ta e="T692" id="Seg_5867" s="T691">süjö</ta>
            <ta e="T693" id="Seg_5868" s="T692">i</ta>
            <ta e="T694" id="Seg_5869" s="T693">ine</ta>
            <ta e="T695" id="Seg_5870" s="T694">kam-bi</ta>
            <ta e="T696" id="Seg_5871" s="T695">kandə-laʔbə-ʔjə</ta>
            <ta e="T697" id="Seg_5872" s="T696">dĭgəttə</ta>
            <ta e="T698" id="Seg_5873" s="T697">volk</ta>
            <ta e="T699" id="Seg_5874" s="T698">šo-bi</ta>
            <ta e="T701" id="Seg_5875" s="T700">ku-lio-t</ta>
            <ta e="T702" id="Seg_5876" s="T701">dĭ</ta>
            <ta e="T703" id="Seg_5877" s="T702">iʔbo-laʔbə</ta>
            <ta e="T704" id="Seg_5878" s="T703">dʼaʔ-pi</ta>
            <ta e="T705" id="Seg_5879" s="T704">süjö</ta>
            <ta e="T707" id="Seg_5880" s="T706">üdʼüge</ta>
            <ta e="T708" id="Seg_5881" s="T707">süjö</ta>
            <ta e="T709" id="Seg_5882" s="T708">da</ta>
            <ta e="T710" id="Seg_5883" s="T709">de-ʔ</ta>
            <ta e="T711" id="Seg_5884" s="T710">kü-bi</ta>
            <ta e="T712" id="Seg_5885" s="T711">bü</ta>
            <ta e="T713" id="Seg_5886" s="T712">i</ta>
            <ta e="T714" id="Seg_5887" s="T713">tʼili</ta>
            <ta e="T715" id="Seg_5888" s="T714">bü</ta>
            <ta e="T716" id="Seg_5889" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_5890" s="T716">süjö</ta>
            <ta e="T718" id="Seg_5891" s="T717">nʼergö-lüʔ-pi</ta>
            <ta e="T719" id="Seg_5892" s="T718">deʔ-pi</ta>
            <ta e="T720" id="Seg_5893" s="T719">dĭ</ta>
            <ta e="T721" id="Seg_5894" s="T720">dĭ-m</ta>
            <ta e="T722" id="Seg_5895" s="T721">bazə-bi</ta>
            <ta e="T724" id="Seg_5896" s="T723">kule</ta>
            <ta e="T725" id="Seg_5897" s="T724">bü-zʼiʔ</ta>
            <ta e="T726" id="Seg_5898" s="T725">i</ta>
            <ta e="T727" id="Seg_5899" s="T726">dĭgəttə</ta>
            <ta e="T728" id="Seg_5900" s="T727">dʼilʼuj</ta>
            <ta e="T729" id="Seg_5901" s="T728">bü-ziʔ</ta>
            <ta e="T730" id="Seg_5902" s="T729">dĭ</ta>
            <ta e="T731" id="Seg_5903" s="T730">suʔmi-leʔbə</ta>
            <ta e="T732" id="Seg_5904" s="T731">kondʼo</ta>
            <ta e="T733" id="Seg_5905" s="T732">măn</ta>
            <ta e="T734" id="Seg_5906" s="T733">kunol-bia-m</ta>
            <ta e="T735" id="Seg_5907" s="T734">kondʼo</ta>
            <ta e="T736" id="Seg_5908" s="T735">kunol-bia-l</ta>
            <ta e="T738" id="Seg_5909" s="T737">kabɨ</ta>
            <ta e="T739" id="Seg_5910" s="T738">ej</ta>
            <ta e="T740" id="Seg_5911" s="T739">măn</ta>
            <ta e="T741" id="Seg_5912" s="T740">dak</ta>
            <ta e="T742" id="Seg_5913" s="T741">tăn</ta>
            <ta e="T743" id="Seg_5914" s="T742">uge</ta>
            <ta e="T744" id="Seg_5915" s="T743">bɨ</ta>
            <ta e="T745" id="Seg_5916" s="T744">kunol-bia-l</ta>
            <ta e="T746" id="Seg_5917" s="T745">tănan</ta>
            <ta e="T747" id="Seg_5918" s="T746">kut-luʔ-pi-ʔi</ta>
            <ta e="T748" id="Seg_5919" s="T747">tăn</ta>
            <ta e="T750" id="Seg_5920" s="T749">kaga-zaŋ-də</ta>
            <ta e="T751" id="Seg_5921" s="T750">i</ta>
            <ta e="T752" id="Seg_5922" s="T751">bar</ta>
            <ta e="T753" id="Seg_5923" s="T752">i-luʔ-pi-ʔi</ta>
            <ta e="T754" id="Seg_5924" s="T753">koʔbdo-m</ta>
            <ta e="T755" id="Seg_5925" s="T754">i</ta>
            <ta e="T756" id="Seg_5926" s="T755">süjö-m</ta>
            <ta e="T757" id="Seg_5927" s="T756">i</ta>
            <ta e="T758" id="Seg_5928" s="T757">ine-m</ta>
            <ta e="T759" id="Seg_5929" s="T758">dĭgəttə</ta>
            <ta e="T760" id="Seg_5930" s="T759">amna-ʔ</ta>
            <ta e="T761" id="Seg_5931" s="T760">măna</ta>
            <ta e="T762" id="Seg_5932" s="T761">amnol-bi</ta>
            <ta e="T763" id="Seg_5933" s="T762">dĭ-zeŋ</ta>
            <ta e="T764" id="Seg_5934" s="T763">bĭdə-bi-ʔi</ta>
            <ta e="T765" id="Seg_5935" s="T764">dĭ-m</ta>
            <ta e="T766" id="Seg_5936" s="T765">dĭ</ta>
            <ta e="T767" id="Seg_5937" s="T766">volk</ta>
            <ta e="T768" id="Seg_5938" s="T767">kaga-zaŋ-də</ta>
            <ta e="T769" id="Seg_5939" s="T768">saj-nožuʔ-pi</ta>
            <ta e="T770" id="Seg_5940" s="T769">i</ta>
            <ta e="T771" id="Seg_5941" s="T770">bos-tə</ta>
            <ta e="T772" id="Seg_5942" s="T771">i-bi</ta>
            <ta e="T773" id="Seg_5943" s="T772">koʔbdo</ta>
            <ta e="T774" id="Seg_5944" s="T773">i</ta>
            <ta e="T775" id="Seg_5945" s="T774">bar</ta>
            <ta e="T776" id="Seg_5946" s="T775">ĭmbi</ta>
            <ta e="T777" id="Seg_5947" s="T776">dĭn</ta>
            <ta e="T778" id="Seg_5948" s="T777">i-bi</ta>
            <ta e="T779" id="Seg_5949" s="T778">dĭgəttə</ta>
            <ta e="T780" id="Seg_5950" s="T779">dĭ</ta>
            <ta e="T781" id="Seg_5951" s="T780">Ivanuška</ta>
            <ta e="T782" id="Seg_5952" s="T781">dĭʔ-nə</ta>
            <ta e="T783" id="Seg_5953" s="T782">saʔmə-luʔ-pi</ta>
            <ta e="T784" id="Seg_5954" s="T783">üjü-ndə</ta>
            <ta e="T785" id="Seg_5955" s="T784">i</ta>
            <ta e="T786" id="Seg_5956" s="T785">par-luʔ-pi</ta>
            <ta e="T787" id="Seg_5957" s="T786">ma-ndə</ta>
            <ta e="T788" id="Seg_5958" s="T787">šo-bi</ta>
            <ta e="T789" id="Seg_5959" s="T788">aba-ndə</ta>
            <ta e="T790" id="Seg_5960" s="T789">bar</ta>
            <ta e="T791" id="Seg_5961" s="T790">ĭmbi</ta>
            <ta e="T792" id="Seg_5962" s="T791">nörbə-bi</ta>
            <ta e="T793" id="Seg_5963" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_5964" s="T793">aba-t</ta>
            <ta e="T795" id="Seg_5965" s="T794">dʼor-bi</ta>
            <ta e="T796" id="Seg_5966" s="T795">ajir-bi</ta>
            <ta e="T797" id="Seg_5967" s="T796">dĭgəttə</ta>
            <ta e="T798" id="Seg_5968" s="T797">dĭ</ta>
            <ta e="T799" id="Seg_5969" s="T798">koʔbdo-m</ta>
            <ta e="T800" id="Seg_5970" s="T799">i-bi</ta>
            <ta e="T801" id="Seg_5971" s="T800">monoʔko-bi-ʔi</ta>
            <ta e="T802" id="Seg_5972" s="T801">ara</ta>
            <ta e="T803" id="Seg_5973" s="T802">bĭʔ-pi-ʔi</ta>
            <ta e="T804" id="Seg_5974" s="T803">i</ta>
            <ta e="T805" id="Seg_5975" s="T804">tüjö</ta>
            <ta e="T806" id="Seg_5976" s="T805">uge</ta>
            <ta e="T807" id="Seg_5977" s="T806">amno-laʔbə-ʔjə</ta>
            <ta e="T808" id="Seg_5978" s="T807">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5979" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_5980" s="T1">koŋ</ta>
            <ta e="T3" id="Seg_5981" s="T2">amno-laʔbə-bi</ta>
            <ta e="T4" id="Seg_5982" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_5983" s="T4">i-bi-jəʔ</ta>
            <ta e="T6" id="Seg_5984" s="T5">nagur-göʔ</ta>
            <ta e="T7" id="Seg_5985" s="T6">nʼi-zAŋ-də</ta>
            <ta e="T8" id="Seg_5986" s="T7">dĭ-n</ta>
            <ta e="T9" id="Seg_5987" s="T8">sad</ta>
            <ta e="T10" id="Seg_5988" s="T9">i-bi</ta>
            <ta e="T12" id="Seg_5989" s="T11">amno-laʔpi-jəʔ</ta>
            <ta e="T13" id="Seg_5990" s="T12">kuvas-jəʔ</ta>
            <ta e="T14" id="Seg_5991" s="T13">šində=də</ta>
            <ta e="T15" id="Seg_5992" s="T14">bar</ta>
            <ta e="T16" id="Seg_5993" s="T15">tojar-laʔbə</ta>
            <ta e="T17" id="Seg_5994" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_5995" s="T17">bar</ta>
            <ta e="T19" id="Seg_5996" s="T18">ku-liA-t</ta>
            <ta e="T20" id="Seg_5997" s="T19">šində=də</ta>
            <ta e="T21" id="Seg_5998" s="T20">tojar-laʔbə</ta>
            <ta e="T22" id="Seg_5999" s="T21">nuldə-bi</ta>
            <ta e="T23" id="Seg_6000" s="T22">il-ə-m</ta>
            <ta e="T24" id="Seg_6001" s="T23">măndo-r-zittə</ta>
            <ta e="T25" id="Seg_6002" s="T24">dĭ-zAŋ</ta>
            <ta e="T26" id="Seg_6003" s="T25">măndo-liA-jəʔ</ta>
            <ta e="T27" id="Seg_6004" s="T26">naga</ta>
            <ta e="T28" id="Seg_6005" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_6006" s="T28">onʼiʔ</ta>
            <ta e="T30" id="Seg_6007" s="T29">nʼi</ta>
            <ta e="T32" id="Seg_6008" s="T31">măn-liA</ta>
            <ta e="T33" id="Seg_6009" s="T32">măn</ta>
            <ta e="T34" id="Seg_6010" s="T33">kan-lV-m</ta>
            <ta e="T35" id="Seg_6011" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_6012" s="T35">kan-bi</ta>
            <ta e="T37" id="Seg_6013" s="T36">kunol-luʔbdə-bi</ta>
            <ta e="T38" id="Seg_6014" s="T37">ĭmbi=də</ta>
            <ta e="T39" id="Seg_6015" s="T38">ej</ta>
            <ta e="T40" id="Seg_6016" s="T39">ku-bi</ta>
            <ta e="T41" id="Seg_6017" s="T40">dĭgəttə</ta>
            <ta e="T42" id="Seg_6018" s="T41">baška</ta>
            <ta e="T43" id="Seg_6019" s="T42">nʼi</ta>
            <ta e="T45" id="Seg_6020" s="T44">kan-bi</ta>
            <ta e="T46" id="Seg_6021" s="T45">tože</ta>
            <ta e="T47" id="Seg_6022" s="T46">kunol-luʔbdə-bi</ta>
            <ta e="T48" id="Seg_6023" s="T47">ĭmbi=də</ta>
            <ta e="T49" id="Seg_6024" s="T48">ej</ta>
            <ta e="T50" id="Seg_6025" s="T49">ku-bi</ta>
            <ta e="T51" id="Seg_6026" s="T50">dĭgəttə</ta>
            <ta e="T52" id="Seg_6027" s="T51">Ivanuška</ta>
            <ta e="T53" id="Seg_6028" s="T52">kan-bi</ta>
            <ta e="T54" id="Seg_6029" s="T53">ej</ta>
            <ta e="T55" id="Seg_6030" s="T54">kunol-liA</ta>
            <ta e="T56" id="Seg_6031" s="T55">kunol-zittə</ta>
            <ta e="T57" id="Seg_6032" s="T56">axota</ta>
            <ta e="T58" id="Seg_6033" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_6034" s="T58">bar</ta>
            <ta e="T60" id="Seg_6035" s="T59">bü-ziʔ</ta>
            <ta e="T61" id="Seg_6036" s="T60">sima-bə</ta>
            <ta e="T62" id="Seg_6037" s="T61">bazə-liA</ta>
            <ta e="T63" id="Seg_6038" s="T62">da</ta>
            <ta e="T64" id="Seg_6039" s="T63">bazoʔ</ta>
            <ta e="T65" id="Seg_6040" s="T64">mĭn-laʔbə</ta>
            <ta e="T66" id="Seg_6041" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_6042" s="T66">ku-liA-t</ta>
            <ta e="T68" id="Seg_6043" s="T67">ku-laːm-bi</ta>
            <ta e="T69" id="Seg_6044" s="T68">bar</ta>
            <ta e="T70" id="Seg_6045" s="T69">nend-luʔbdə-bi</ta>
            <ta e="T71" id="Seg_6046" s="T70">šü</ta>
            <ta e="T72" id="Seg_6047" s="T71">dĭ</ta>
            <ta e="T74" id="Seg_6048" s="T73">ku-laːm-bi</ta>
            <ta e="T76" id="Seg_6049" s="T75">süjö</ta>
            <ta e="T77" id="Seg_6050" s="T76">amno-laʔbə</ta>
            <ta e="T78" id="Seg_6051" s="T77">šü</ta>
            <ta e="T79" id="Seg_6052" s="T78">dĭrgit</ta>
            <ta e="T80" id="Seg_6053" s="T79">dĭ-n</ta>
            <ta e="T81" id="Seg_6054" s="T80">müʔbdə-luʔbdə-bi</ta>
            <ta e="T82" id="Seg_6055" s="T81">dʼabə-luʔbdə-bi</ta>
            <ta e="T83" id="Seg_6056" s="T82">onʼiʔ</ta>
            <ta e="T84" id="Seg_6057" s="T83">pʼero</ta>
            <ta e="T86" id="Seg_6058" s="T85">det-bi</ta>
            <ta e="T87" id="Seg_6059" s="T86">dĭgəttə</ta>
            <ta e="T88" id="Seg_6060" s="T87">šo-bi</ta>
            <ta e="T89" id="Seg_6061" s="T88">aba-Tə</ta>
            <ta e="T90" id="Seg_6062" s="T89">no</ta>
            <ta e="T91" id="Seg_6063" s="T90">ĭmbi</ta>
            <ta e="T92" id="Seg_6064" s="T91">nʼi-m</ta>
            <ta e="T94" id="Seg_6065" s="T93">măn</ta>
            <ta e="T95" id="Seg_6066" s="T94">ku-bi-m</ta>
            <ta e="T96" id="Seg_6067" s="T95">šində</ta>
            <ta e="T97" id="Seg_6068" s="T96">tojar-laʔbə</ta>
            <ta e="T98" id="Seg_6069" s="T97">dö</ta>
            <ta e="T99" id="Seg_6070" s="T98">tănan</ta>
            <ta e="T100" id="Seg_6071" s="T99">det-bi-m</ta>
            <ta e="T101" id="Seg_6072" s="T100">pʼero</ta>
            <ta e="T102" id="Seg_6073" s="T101">dĭ</ta>
            <ta e="T106" id="Seg_6074" s="T105">mĭn-laʔbə</ta>
            <ta e="T107" id="Seg_6075" s="T106">i</ta>
            <ta e="T108" id="Seg_6076" s="T107">tojar-laʔbə</ta>
            <ta e="T109" id="Seg_6077" s="T108">dĭgəttə</ta>
            <ta e="T110" id="Seg_6078" s="T109">aba-t</ta>
            <ta e="T112" id="Seg_6079" s="T111">am-zittə</ta>
            <ta e="T113" id="Seg_6080" s="T112">dĭgəttə</ta>
            <ta e="T114" id="Seg_6081" s="T113">tenö-bi</ta>
            <ta e="T115" id="Seg_6082" s="T114">tenö-bi</ta>
            <ta e="T116" id="Seg_6083" s="T115">kan-KAʔ</ta>
            <ta e="T117" id="Seg_6084" s="T116">nʼi-zAŋ</ta>
            <ta e="T118" id="Seg_6085" s="T117">gibər=nʼibudʼ</ta>
            <ta e="T119" id="Seg_6086" s="T118">bar</ta>
            <ta e="T120" id="Seg_6087" s="T119">tʼo</ta>
            <ta e="T121" id="Seg_6088" s="T120">mĭn-KAʔ</ta>
            <ta e="T122" id="Seg_6089" s="T121">il</ta>
            <ta e="T126" id="Seg_6090" s="T125">možet</ta>
            <ta e="T127" id="Seg_6091" s="T126">gijen</ta>
            <ta e="T130" id="Seg_6092" s="T129">gijen=nʼibudʼ</ta>
            <ta e="T131" id="Seg_6093" s="T130">dĭ</ta>
            <ta e="T132" id="Seg_6094" s="T131">süjö</ta>
            <ta e="T136" id="Seg_6095" s="T135">dʼabə-lV-lAʔ</ta>
            <ta e="T137" id="Seg_6096" s="T136">girgit</ta>
            <ta e="T138" id="Seg_6097" s="T137">šü</ta>
            <ta e="T139" id="Seg_6098" s="T138">dĭ-zAŋ</ta>
            <ta e="T141" id="Seg_6099" s="T140">kan-bi-jəʔ</ta>
            <ta e="T142" id="Seg_6100" s="T141">onʼiʔ</ta>
            <ta e="T143" id="Seg_6101" s="T142">dĭbər</ta>
            <ta e="T144" id="Seg_6102" s="T143">baška</ta>
            <ta e="T145" id="Seg_6103" s="T144">döbər</ta>
            <ta e="T146" id="Seg_6104" s="T145">Vanʼuška</ta>
            <ta e="T147" id="Seg_6105" s="T146">tože</ta>
            <ta e="T148" id="Seg_6106" s="T147">kan-bi</ta>
            <ta e="T149" id="Seg_6107" s="T148">kabarləj</ta>
            <ta e="T150" id="Seg_6108" s="T149">dĭgəttə</ta>
            <ta e="T151" id="Seg_6109" s="T150">dĭ</ta>
            <ta e="T152" id="Seg_6110" s="T151">Ivan_Tsarevič</ta>
            <ta e="T153" id="Seg_6111" s="T152">kan-bi</ta>
            <ta e="T154" id="Seg_6112" s="T153">šo-bi</ta>
            <ta e="T155" id="Seg_6113" s="T154">šo-bi</ta>
            <ta e="T156" id="Seg_6114" s="T155">ine-bə</ta>
            <ta e="T157" id="Seg_6115" s="T156">körer-luʔbdə-bi</ta>
            <ta e="T158" id="Seg_6116" s="T157">am-bi</ta>
            <ta e="T160" id="Seg_6117" s="T159">iʔbə-bi</ta>
            <ta e="T161" id="Seg_6118" s="T160">kunol-zittə</ta>
            <ta e="T162" id="Seg_6119" s="T161">ine-t</ta>
            <ta e="T163" id="Seg_6120" s="T162">mĭn-laʔbə</ta>
            <ta e="T164" id="Seg_6121" s="T163">üjü-bə</ta>
            <ta e="T165" id="Seg_6122" s="T164">sar-bi</ta>
            <ta e="T166" id="Seg_6123" s="T165">kondʼo</ta>
            <ta e="T167" id="Seg_6124" s="T166">kunol-bi</ta>
            <ta e="T168" id="Seg_6125" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_6126" s="T168">uʔbdə-bi</ta>
            <ta e="T170" id="Seg_6127" s="T169">ku-liA-t</ta>
            <ta e="T171" id="Seg_6128" s="T170">ine-t</ta>
            <ta e="T172" id="Seg_6129" s="T171">naga</ta>
            <ta e="T173" id="Seg_6130" s="T172">măndo-r-bi</ta>
            <ta e="T174" id="Seg_6131" s="T173">măndo-r-bi</ta>
            <ta e="T175" id="Seg_6132" s="T174">tolʼko</ta>
            <ta e="T176" id="Seg_6133" s="T175">le-jəʔ</ta>
            <ta e="T177" id="Seg_6134" s="T176">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T178" id="Seg_6135" s="T177">amno-laʔbə</ta>
            <ta e="T179" id="Seg_6136" s="T178">da</ta>
            <ta e="T180" id="Seg_6137" s="T179">dĭgəttə</ta>
            <ta e="T181" id="Seg_6138" s="T180">nadə</ta>
            <ta e="T182" id="Seg_6139" s="T181">kan-zittə</ta>
            <ta e="T183" id="Seg_6140" s="T182">üdʼi</ta>
            <ta e="T184" id="Seg_6141" s="T183">kandə-gA</ta>
            <ta e="T185" id="Seg_6142" s="T184">kandə-gA</ta>
            <ta e="T186" id="Seg_6143" s="T185">dĭgəttə</ta>
            <ta e="T187" id="Seg_6144" s="T186">tarar-luʔbdə-bi</ta>
            <ta e="T188" id="Seg_6145" s="T187">da</ta>
            <ta e="T189" id="Seg_6146" s="T188">bar</ta>
            <ta e="T190" id="Seg_6147" s="T189">amnə-luʔbdə-bi</ta>
            <ta e="T191" id="Seg_6148" s="T190">amno-laʔbə</ta>
            <ta e="T192" id="Seg_6149" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_6150" s="T192">volk</ta>
            <ta e="T194" id="Seg_6151" s="T193">nuʔmə-laʔbə</ta>
            <ta e="T195" id="Seg_6152" s="T194">ĭmbi</ta>
            <ta e="T196" id="Seg_6153" s="T195">amno-laʔbə-l</ta>
            <ta e="T197" id="Seg_6154" s="T196">ulu-l</ta>
            <ta e="T198" id="Seg_6155" s="T197">edə-bi-m</ta>
            <ta e="T199" id="Seg_6156" s="T198">da</ta>
            <ta e="T200" id="Seg_6157" s="T199">măn</ta>
            <ta e="T201" id="Seg_6158" s="T200">ine-m</ta>
            <ta e="T202" id="Seg_6159" s="T201">am-luʔbdə-bi</ta>
            <ta e="T203" id="Seg_6160" s="T202">a</ta>
            <ta e="T204" id="Seg_6161" s="T203">dĭ</ta>
            <ta e="T205" id="Seg_6162" s="T204">măn</ta>
            <ta e="T206" id="Seg_6163" s="T205">am-bi-m</ta>
            <ta e="T207" id="Seg_6164" s="T206">tak</ta>
            <ta e="T209" id="Seg_6165" s="T208">a</ta>
            <ta e="T210" id="Seg_6166" s="T209">gibər</ta>
            <ta e="T211" id="Seg_6167" s="T210">tăn</ta>
            <ta e="T212" id="Seg_6168" s="T211">kandə-gA-l</ta>
            <ta e="T213" id="Seg_6169" s="T212">da</ta>
            <ta e="T214" id="Seg_6170" s="T213">kandə-gA-m</ta>
            <ta e="T215" id="Seg_6171" s="T214">süjö</ta>
            <ta e="T216" id="Seg_6172" s="T215">süjö</ta>
            <ta e="T218" id="Seg_6173" s="T217">măndo-r-zittə</ta>
            <ta e="T219" id="Seg_6174" s="T218">aba-m</ta>
            <ta e="T220" id="Seg_6175" s="T219">măn-ntə</ta>
            <ta e="T221" id="Seg_6176" s="T220">măndo-r-ə-ʔ</ta>
            <ta e="T222" id="Seg_6177" s="T221">süjö</ta>
            <ta e="T223" id="Seg_6178" s="T222">šü</ta>
            <ta e="T224" id="Seg_6179" s="T223">dĭrgit</ta>
            <ta e="T225" id="Seg_6180" s="T224">dĭgəttə</ta>
            <ta e="T226" id="Seg_6181" s="T225">dĭ</ta>
            <ta e="T227" id="Seg_6182" s="T226">no</ta>
            <ta e="T228" id="Seg_6183" s="T227">tăn</ta>
            <ta e="T229" id="Seg_6184" s="T228">bos-də</ta>
            <ta e="T230" id="Seg_6185" s="T229">ine-l-ziʔ</ta>
            <ta e="T231" id="Seg_6186" s="T230">tak</ta>
            <ta e="T232" id="Seg_6187" s="T231">nagur</ta>
            <ta e="T233" id="Seg_6188" s="T232">kö</ta>
            <ta e="T234" id="Seg_6189" s="T233">dĭbər</ta>
            <ta e="T235" id="Seg_6190" s="T234">ej</ta>
            <ta e="T236" id="Seg_6191" s="T235">kan-lV-l</ta>
            <ta e="T237" id="Seg_6192" s="T236">amnə-ʔ</ta>
            <ta e="T239" id="Seg_6193" s="T238">măna</ta>
            <ta e="T240" id="Seg_6194" s="T239">măn</ta>
            <ta e="T241" id="Seg_6195" s="T240">tănan</ta>
            <ta e="T242" id="Seg_6196" s="T241">kun-laːm-lV-m</ta>
            <ta e="T243" id="Seg_6197" s="T242">amnə-bi</ta>
            <ta e="T244" id="Seg_6198" s="T243">dĭgəttə</ta>
            <ta e="T245" id="Seg_6199" s="T244">dĭ</ta>
            <ta e="T246" id="Seg_6200" s="T245">det-bi</ta>
            <ta e="T247" id="Seg_6201" s="T246">kan-ə-ʔ</ta>
            <ta e="T248" id="Seg_6202" s="T247">tüj</ta>
            <ta e="T249" id="Seg_6203" s="T248">bar</ta>
            <ta e="T250" id="Seg_6204" s="T249">il</ta>
            <ta e="T251" id="Seg_6205" s="T250">kunol-laʔbə-jəʔ</ta>
            <ta e="T252" id="Seg_6206" s="T251">i-ʔ</ta>
            <ta e="T253" id="Seg_6207" s="T252">dĭ</ta>
            <ta e="T254" id="Seg_6208" s="T253">süjö</ta>
            <ta e="T255" id="Seg_6209" s="T254">a</ta>
            <ta e="T256" id="Seg_6210" s="T255">klʼetka-bə</ta>
            <ta e="T257" id="Seg_6211" s="T256">e-ʔ</ta>
            <ta e="T258" id="Seg_6212" s="T257">i-t</ta>
            <ta e="T260" id="Seg_6213" s="T258">ato</ta>
            <ta e="T261" id="Seg_6214" s="T260">dĭgəttə</ta>
            <ta e="T262" id="Seg_6215" s="T261">dĭ</ta>
            <ta e="T263" id="Seg_6216" s="T262">kan-bi</ta>
            <ta e="T264" id="Seg_6217" s="T263">i-bi</ta>
            <ta e="T265" id="Seg_6218" s="T264">süjö</ta>
            <ta e="T266" id="Seg_6219" s="T265">măndo-r-liA</ta>
            <ta e="T267" id="Seg_6220" s="T266">klʼetka</ta>
            <ta e="T268" id="Seg_6221" s="T267">kuvas</ta>
            <ta e="T269" id="Seg_6222" s="T268">nadə</ta>
            <ta e="T270" id="Seg_6223" s="T269">i-zittə</ta>
            <ta e="T271" id="Seg_6224" s="T270">kak</ta>
            <ta e="T272" id="Seg_6225" s="T271">dĭ-m</ta>
            <ta e="T273" id="Seg_6226" s="T272">mĭnžə-luʔbdə-bi</ta>
            <ta e="T274" id="Seg_6227" s="T273">bar</ta>
            <ta e="T275" id="Seg_6228" s="T274">kuzur-luʔbdə-bi</ta>
            <ta e="T276" id="Seg_6229" s="T275">bar</ta>
            <ta e="T277" id="Seg_6230" s="T276">dĭ-m</ta>
            <ta e="T278" id="Seg_6231" s="T277">dʼabə-luʔbdə-bi</ta>
            <ta e="T279" id="Seg_6232" s="T278">i</ta>
            <ta e="T282" id="Seg_6233" s="T281">kun-bi-jəʔ</ta>
            <ta e="T283" id="Seg_6234" s="T282">koŋ-Tə</ta>
            <ta e="T284" id="Seg_6235" s="T283">kabarləj</ta>
            <ta e="T285" id="Seg_6236" s="T284">dĭgəttə</ta>
            <ta e="T286" id="Seg_6237" s="T285">dĭ</ta>
            <ta e="T287" id="Seg_6238" s="T286">šo-bi</ta>
            <ta e="T288" id="Seg_6239" s="T287">koŋ-Tə</ta>
            <ta e="T289" id="Seg_6240" s="T288">tăn</ta>
            <ta e="T290" id="Seg_6241" s="T289">ĭmbi</ta>
            <ta e="T291" id="Seg_6242" s="T290">ĭmbi</ta>
            <ta e="T292" id="Seg_6243" s="T291">döbər</ta>
            <ta e="T293" id="Seg_6244" s="T292">šo-bi-m</ta>
            <ta e="T294" id="Seg_6245" s="T293">tojar-zittə</ta>
            <ta e="T295" id="Seg_6246" s="T294">šində-n</ta>
            <ta e="T296" id="Seg_6247" s="T295">tăn</ta>
            <ta e="T297" id="Seg_6248" s="T296">nʼi</ta>
            <ta e="T298" id="Seg_6249" s="T297">măn</ta>
            <ta e="T299" id="Seg_6250" s="T298">Ivan_Tsarevič</ta>
            <ta e="T300" id="Seg_6251" s="T299">tak</ta>
            <ta e="T301" id="Seg_6252" s="T300">tăn</ta>
            <ta e="T302" id="Seg_6253" s="T301">tojar-zittə</ta>
            <ta e="T304" id="Seg_6254" s="T303">dĭgəttə</ta>
            <ta e="T305" id="Seg_6255" s="T304">dĭ</ta>
            <ta e="T306" id="Seg_6256" s="T305">măn-ntə</ta>
            <ta e="T307" id="Seg_6257" s="T306">a</ta>
            <ta e="T308" id="Seg_6258" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_6259" s="T308">miʔnʼibeʔ</ta>
            <ta e="T310" id="Seg_6260" s="T309">mĭn-bi</ta>
            <ta e="T311" id="Seg_6261" s="T310">da</ta>
            <ta e="T313" id="Seg_6262" s="T312">am-bi-jəʔ</ta>
            <ta e="T314" id="Seg_6263" s="T313">a</ta>
            <ta e="T315" id="Seg_6264" s="T314">tăn</ta>
            <ta e="T316" id="Seg_6265" s="T315">šo-bi-l</ta>
            <ta e="T317" id="Seg_6266" s="T316">bɨ</ta>
            <ta e="T318" id="Seg_6267" s="T317">a</ta>
            <ta e="T319" id="Seg_6268" s="T318">măn</ta>
            <ta e="T320" id="Seg_6269" s="T319">dărəʔ</ta>
            <ta e="T321" id="Seg_6270" s="T320">tănan</ta>
            <ta e="T322" id="Seg_6271" s="T321">mĭ-bi-m</ta>
            <ta e="T323" id="Seg_6272" s="T322">bɨ</ta>
            <ta e="T324" id="Seg_6273" s="T323">ato</ta>
            <ta e="T325" id="Seg_6274" s="T324">tojar-zittə</ta>
            <ta e="T326" id="Seg_6275" s="T325">šo-bi-l</ta>
            <ta e="T327" id="Seg_6276" s="T326">no</ta>
            <ta e="T328" id="Seg_6277" s="T327">măn</ta>
            <ta e="T329" id="Seg_6278" s="T328">tănan</ta>
            <ta e="T330" id="Seg_6279" s="T329">ĭmbi=də</ta>
            <ta e="T331" id="Seg_6280" s="T330">ej</ta>
            <ta e="T332" id="Seg_6281" s="T331">a-lV-m</ta>
            <ta e="T333" id="Seg_6282" s="T332">kan-ə-ʔ</ta>
            <ta e="T334" id="Seg_6283" s="T333">ine</ta>
            <ta e="T335" id="Seg_6284" s="T334">det-ʔ</ta>
            <ta e="T337" id="Seg_6285" s="T336">griva-ziʔ</ta>
            <ta e="T338" id="Seg_6286" s="T337">dĭgəttə</ta>
            <ta e="T339" id="Seg_6287" s="T338">dĭ</ta>
            <ta e="T340" id="Seg_6288" s="T339">kan-bi</ta>
            <ta e="T341" id="Seg_6289" s="T340">volk-Tə</ta>
            <ta e="T342" id="Seg_6290" s="T341">volk</ta>
            <ta e="T343" id="Seg_6291" s="T342">măn-ntə</ta>
            <ta e="T344" id="Seg_6292" s="T343">măn</ta>
            <ta e="T345" id="Seg_6293" s="T344">măn-bi-m</ta>
            <ta e="T346" id="Seg_6294" s="T345">tănan</ta>
            <ta e="T347" id="Seg_6295" s="T346">e-ʔ</ta>
            <ta e="T348" id="Seg_6296" s="T347">üžəm-ə-ʔ</ta>
            <ta e="T349" id="Seg_6297" s="T348">a</ta>
            <ta e="T350" id="Seg_6298" s="T349">tăn</ta>
            <ta e="T351" id="Seg_6299" s="T350">üžəm-bi-l</ta>
            <ta e="T352" id="Seg_6300" s="T351">a</ta>
            <ta e="T353" id="Seg_6301" s="T352">tüj</ta>
            <ta e="T355" id="Seg_6302" s="T354">amnə-ʔ</ta>
            <ta e="T356" id="Seg_6303" s="T355">măna</ta>
            <ta e="T357" id="Seg_6304" s="T356">kan-žə-bəj</ta>
            <ta e="T358" id="Seg_6305" s="T357">kan-bi-jəʔ</ta>
            <ta e="T359" id="Seg_6306" s="T358">no</ta>
            <ta e="T360" id="Seg_6307" s="T359">šo-bi-jəʔ</ta>
            <ta e="T361" id="Seg_6308" s="T360">dĭbər</ta>
            <ta e="T362" id="Seg_6309" s="T361">dĭgəttə</ta>
            <ta e="T363" id="Seg_6310" s="T362">no</ta>
            <ta e="T364" id="Seg_6311" s="T363">kan-ə-ʔ</ta>
            <ta e="T365" id="Seg_6312" s="T364">da</ta>
            <ta e="T366" id="Seg_6313" s="T365">dʼabə-t</ta>
            <ta e="T367" id="Seg_6314" s="T366">ine-m</ta>
            <ta e="T368" id="Seg_6315" s="T367">a</ta>
            <ta e="T369" id="Seg_6316" s="T368">uzda-m</ta>
            <ta e="T370" id="Seg_6317" s="T369">ej</ta>
            <ta e="T372" id="Seg_6318" s="T371">e-ʔ</ta>
            <ta e="T374" id="Seg_6319" s="T373">e-ʔ</ta>
            <ta e="T376" id="Seg_6320" s="T375">e-ʔ</ta>
            <ta e="T377" id="Seg_6321" s="T376">i-ʔ</ta>
            <ta e="T378" id="Seg_6322" s="T377">ato</ta>
            <ta e="T379" id="Seg_6323" s="T378">bazoʔ</ta>
            <ta e="T380" id="Seg_6324" s="T379">dărəʔ</ta>
            <ta e="T381" id="Seg_6325" s="T380">mo-laːm-lV-j</ta>
            <ta e="T382" id="Seg_6326" s="T381">dĭ</ta>
            <ta e="T383" id="Seg_6327" s="T382">šo-bi</ta>
            <ta e="T384" id="Seg_6328" s="T383">ine-m</ta>
            <ta e="T385" id="Seg_6329" s="T384">i-bi</ta>
            <ta e="T386" id="Seg_6330" s="T385">a</ta>
            <ta e="T387" id="Seg_6331" s="T386">uzda-t</ta>
            <ta e="T388" id="Seg_6332" s="T387">ugaːndə</ta>
            <ta e="T389" id="Seg_6333" s="T388">kuvas</ta>
            <ta e="T390" id="Seg_6334" s="T389">ĭmbi</ta>
            <ta e="T391" id="Seg_6335" s="T390">ine-Kən</ta>
            <ta e="T392" id="Seg_6336" s="T391">nadə</ta>
            <ta e="T393" id="Seg_6337" s="T392">šer-zittə</ta>
            <ta e="T394" id="Seg_6338" s="T393">tolʼko</ta>
            <ta e="T395" id="Seg_6339" s="T394">i-bi</ta>
            <ta e="T396" id="Seg_6340" s="T395">dĭ-m</ta>
            <ta e="T397" id="Seg_6341" s="T396">dĭ</ta>
            <ta e="T398" id="Seg_6342" s="T397">bar</ta>
            <ta e="T399" id="Seg_6343" s="T398">kirgaːr-luʔbdə-bi</ta>
            <ta e="T400" id="Seg_6344" s="T399">il</ta>
            <ta e="T401" id="Seg_6345" s="T400">süʔmə-luʔbdə-bi</ta>
            <ta e="T402" id="Seg_6346" s="T401">dĭ-m</ta>
            <ta e="T403" id="Seg_6347" s="T402">i-bi-jəʔ</ta>
            <ta e="T405" id="Seg_6348" s="T404">kun-laːm-bi-jəʔ</ta>
            <ta e="T406" id="Seg_6349" s="T405">koŋ-Tə</ta>
            <ta e="T407" id="Seg_6350" s="T406">ĭmbi</ta>
            <ta e="T408" id="Seg_6351" s="T407">šo-bi-l</ta>
            <ta e="T409" id="Seg_6352" s="T408">ine</ta>
            <ta e="T410" id="Seg_6353" s="T409">tojar-zittə</ta>
            <ta e="T411" id="Seg_6354" s="T410">šində-n</ta>
            <ta e="T412" id="Seg_6355" s="T411">tăn</ta>
            <ta e="T413" id="Seg_6356" s="T412">măn</ta>
            <ta e="T414" id="Seg_6357" s="T413">măn</ta>
            <ta e="T415" id="Seg_6358" s="T414">Ivan_Tsarevič</ta>
            <ta e="T416" id="Seg_6359" s="T415">tak</ta>
            <ta e="T417" id="Seg_6360" s="T416">tojar-zittə</ta>
            <ta e="T419" id="Seg_6361" s="T418">šo-bi-l</ta>
            <ta e="T420" id="Seg_6362" s="T419">no</ta>
            <ta e="T421" id="Seg_6363" s="T420">kan-ə-ʔ</ta>
            <ta e="T422" id="Seg_6364" s="T421">măn</ta>
            <ta e="T423" id="Seg_6365" s="T422">tănan</ta>
            <ta e="T424" id="Seg_6366" s="T423">ĭmbi=də</ta>
            <ta e="T425" id="Seg_6367" s="T424">ej</ta>
            <ta e="T426" id="Seg_6368" s="T425">a-lV-m</ta>
            <ta e="T427" id="Seg_6369" s="T426">dĭn</ta>
            <ta e="T428" id="Seg_6370" s="T427">i-gA</ta>
            <ta e="T429" id="Seg_6371" s="T428">koʔbdo</ta>
            <ta e="T432" id="Seg_6372" s="T431">det-t</ta>
            <ta e="T433" id="Seg_6373" s="T432">măna</ta>
            <ta e="T434" id="Seg_6374" s="T433">dĭ-m</ta>
            <ta e="T435" id="Seg_6375" s="T434">dĭgəttə</ta>
            <ta e="T436" id="Seg_6376" s="T435">dĭ</ta>
            <ta e="T438" id="Seg_6377" s="T437">šonə-gA</ta>
            <ta e="T439" id="Seg_6378" s="T438">a</ta>
            <ta e="T440" id="Seg_6379" s="T439">volk-də</ta>
            <ta e="T441" id="Seg_6380" s="T440">măn-bi-m</ta>
            <ta e="T442" id="Seg_6381" s="T441">e-ʔ</ta>
            <ta e="T443" id="Seg_6382" s="T442">üžəm-ə-ʔ</ta>
            <ta e="T444" id="Seg_6383" s="T443">a</ta>
            <ta e="T445" id="Seg_6384" s="T444">tăn</ta>
            <ta e="T446" id="Seg_6385" s="T445">üge</ta>
            <ta e="T447" id="Seg_6386" s="T446">üžəm-luʔbdə-bi-m</ta>
            <ta e="T448" id="Seg_6387" s="T447">no</ta>
            <ta e="T449" id="Seg_6388" s="T448">ĭmbi=də</ta>
            <ta e="T450" id="Seg_6389" s="T449">emnajaʔa</ta>
            <ta e="T451" id="Seg_6390" s="T450">dĭ</ta>
            <ta e="T452" id="Seg_6391" s="T451">măn-ntə</ta>
            <ta e="T453" id="Seg_6392" s="T452">no</ta>
            <ta e="T454" id="Seg_6393" s="T453">amnə-ʔ</ta>
            <ta e="T455" id="Seg_6394" s="T454">kan-žə-bəj</ta>
            <ta e="T456" id="Seg_6395" s="T455">šo-bi-jəʔ</ta>
            <ta e="T457" id="Seg_6396" s="T456">dĭbər</ta>
            <ta e="T458" id="Seg_6397" s="T457">dĭgəttə</ta>
            <ta e="T459" id="Seg_6398" s="T458">volk</ta>
            <ta e="T460" id="Seg_6399" s="T459">măn-ntə</ta>
            <ta e="T461" id="Seg_6400" s="T460">dĭ-Tə</ta>
            <ta e="T462" id="Seg_6401" s="T461">tăn</ta>
            <ta e="T464" id="Seg_6402" s="T463">kan-ə-ʔ</ta>
            <ta e="T465" id="Seg_6403" s="T464">par-ə-ʔ</ta>
            <ta e="T466" id="Seg_6404" s="T465">par-ə-ʔ</ta>
            <ta e="T467" id="Seg_6405" s="T466">a</ta>
            <ta e="T468" id="Seg_6406" s="T467">măn</ta>
            <ta e="T469" id="Seg_6407" s="T468">kan-lV-m</ta>
            <ta e="T470" id="Seg_6408" s="T469">bos-də</ta>
            <ta e="T471" id="Seg_6409" s="T470">dĭgəttə</ta>
            <ta e="T472" id="Seg_6410" s="T471">süʔmə-luʔbdə-bi</ta>
            <ta e="T473" id="Seg_6411" s="T472">sat-Tə</ta>
            <ta e="T474" id="Seg_6412" s="T473">dĭn</ta>
            <ta e="T475" id="Seg_6413" s="T474">šaʔ-laːm-bi</ta>
            <ta e="T476" id="Seg_6414" s="T475">dĭ-zAŋ</ta>
            <ta e="T477" id="Seg_6415" s="T476">mĭn-nzə-laʔbə-jəʔ</ta>
            <ta e="T478" id="Seg_6416" s="T477">ija-t</ta>
            <ta e="T479" id="Seg_6417" s="T478">nʼanʼka-jəʔ</ta>
            <ta e="T480" id="Seg_6418" s="T479">dĭ</ta>
            <ta e="T481" id="Seg_6419" s="T480">Elena-t</ta>
            <ta e="T482" id="Seg_6420" s="T481">ma-luʔbdə-bi</ta>
            <ta e="T483" id="Seg_6421" s="T482">dĭ</ta>
            <ta e="T484" id="Seg_6422" s="T483">dĭ-m</ta>
            <ta e="T485" id="Seg_6423" s="T484">kabar-luʔbdə-bi</ta>
            <ta e="T486" id="Seg_6424" s="T485">i</ta>
            <ta e="T487" id="Seg_6425" s="T486">nuʔmə-luʔbdə-bi</ta>
            <ta e="T488" id="Seg_6426" s="T487">vot</ta>
            <ta e="T489" id="Seg_6427" s="T488">nuʔmə-laʔbə</ta>
            <ta e="T490" id="Seg_6428" s="T489">i</ta>
            <ta e="T491" id="Seg_6429" s="T490">bĭdə-bi</ta>
            <ta e="T492" id="Seg_6430" s="T491">Ivan-m</ta>
            <ta e="T493" id="Seg_6431" s="T492">amnə-ʔ</ta>
            <ta e="T494" id="Seg_6432" s="T493">amnəl-bi-jəʔ</ta>
            <ta e="T495" id="Seg_6433" s="T494">dĭgəttə</ta>
            <ta e="T496" id="Seg_6434" s="T495">šo-bi-jəʔ</ta>
            <ta e="T497" id="Seg_6435" s="T496">dĭbər</ta>
            <ta e="T498" id="Seg_6436" s="T497">gijen</ta>
            <ta e="T500" id="Seg_6437" s="T499">ine</ta>
            <ta e="T501" id="Seg_6438" s="T500">pi-bi-jəʔ</ta>
            <ta e="T502" id="Seg_6439" s="T501">dĭgəttə</ta>
            <ta e="T503" id="Seg_6440" s="T502">dĭ-zAŋ</ta>
            <ta e="T504" id="Seg_6441" s="T503">šo-bi-jəʔ</ta>
            <ta e="T505" id="Seg_6442" s="T504">a</ta>
            <ta e="T506" id="Seg_6443" s="T505">dĭ</ta>
            <ta e="T507" id="Seg_6444" s="T506">tenö-laʔbə</ta>
            <ta e="T508" id="Seg_6445" s="T507">üge</ta>
            <ta e="T509" id="Seg_6446" s="T508">Ivan_Tsarevič</ta>
            <ta e="T510" id="Seg_6447" s="T509">ajir-liA</ta>
            <ta e="T511" id="Seg_6448" s="T510">dĭ</ta>
            <ta e="T512" id="Seg_6449" s="T511">koʔbdo-m</ta>
            <ta e="T513" id="Seg_6450" s="T512">a</ta>
            <ta e="T514" id="Seg_6451" s="T513">volk-də</ta>
            <ta e="T515" id="Seg_6452" s="T514">surar-laʔbə</ta>
            <ta e="T516" id="Seg_6453" s="T515">ĭmbi</ta>
            <ta e="T517" id="Seg_6454" s="T516">tăn</ta>
            <ta e="T518" id="Seg_6455" s="T517">dĭn</ta>
            <ta e="T519" id="Seg_6456" s="T518">nu-laʔbə-l</ta>
            <ta e="T520" id="Seg_6457" s="T519">dĭ</ta>
            <ta e="T521" id="Seg_6458" s="T520">dĭrgit</ta>
            <ta e="T522" id="Seg_6459" s="T521">kuvas</ta>
            <ta e="T523" id="Seg_6460" s="T522">koʔbdo</ta>
            <ta e="T524" id="Seg_6461" s="T523">măn</ta>
            <ta e="T525" id="Seg_6462" s="T524">ajir-bi-m</ta>
            <ta e="T527" id="Seg_6463" s="T526">mĭ-zittə</ta>
            <ta e="T528" id="Seg_6464" s="T527">no</ta>
            <ta e="T529" id="Seg_6465" s="T528">šaʔbdə-l-bəj</ta>
            <ta e="T530" id="Seg_6466" s="T529">a</ta>
            <ta e="T531" id="Seg_6467" s="T530">măn</ta>
            <ta e="T533" id="Seg_6468" s="T532">măn</ta>
            <ta e="T534" id="Seg_6469" s="T533">a-laʔbə-m</ta>
            <ta e="T536" id="Seg_6470" s="T535">dĭ-Tə</ta>
            <ta e="T537" id="Seg_6471" s="T536">dĭgəttə</ta>
            <ta e="T538" id="Seg_6472" s="T537">bos-də</ta>
            <ta e="T539" id="Seg_6473" s="T538">bögəl-Tə</ta>
            <ta e="T540" id="Seg_6474" s="T539">süʔmə-luʔbdə-bi</ta>
            <ta e="T541" id="Seg_6475" s="T540">dĭ</ta>
            <ta e="T542" id="Seg_6476" s="T541">dĭ-m</ta>
            <ta e="T543" id="Seg_6477" s="T542">i-bi</ta>
            <ta e="T544" id="Seg_6478" s="T543">kun-laːm-bi</ta>
            <ta e="T545" id="Seg_6479" s="T544">a</ta>
            <ta e="T546" id="Seg_6480" s="T545">dĭ</ta>
            <ta e="T547" id="Seg_6481" s="T546">i-bi</ta>
            <ta e="T548" id="Seg_6482" s="T547">dĭ</ta>
            <ta e="T549" id="Seg_6483" s="T548">koʔbdo-m</ta>
            <ta e="T550" id="Seg_6484" s="T549">dʼansaʔməj</ta>
            <ta e="T551" id="Seg_6485" s="T550">ara</ta>
            <ta e="T552" id="Seg_6486" s="T551">bĭs-bi-jəʔ</ta>
            <ta e="T553" id="Seg_6487" s="T552">süʔmə-laʔbə-jəʔ</ta>
            <ta e="T554" id="Seg_6488" s="T553">ine-bə</ta>
            <ta e="T555" id="Seg_6489" s="T554">mĭ-bi</ta>
            <ta e="T556" id="Seg_6490" s="T555">dĭ</ta>
            <ta e="T557" id="Seg_6491" s="T556">ine-bə</ta>
            <ta e="T558" id="Seg_6492" s="T557">i-bi</ta>
            <ta e="T559" id="Seg_6493" s="T558">Elena-bə</ta>
            <ta e="T560" id="Seg_6494" s="T559">amnəl-bə-bi</ta>
            <ta e="T561" id="Seg_6495" s="T560">bos-də</ta>
            <ta e="T562" id="Seg_6496" s="T561">amnəl-bi</ta>
            <ta e="T563" id="Seg_6497" s="T562">kan-bi-jəʔ</ta>
            <ta e="T564" id="Seg_6498" s="T563">a</ta>
            <ta e="T565" id="Seg_6499" s="T564">nüdʼi-n</ta>
            <ta e="T567" id="Seg_6500" s="T566">iʔbə-jəʔ</ta>
            <ta e="T568" id="Seg_6501" s="T567">kunol-zittə</ta>
            <ta e="T569" id="Seg_6502" s="T568">dĭ</ta>
            <ta e="T571" id="Seg_6503" s="T570">dĭ</ta>
            <ta e="T572" id="Seg_6504" s="T571">Elena-t</ta>
            <ta e="T574" id="Seg_6505" s="T573">mo-laːm-bi</ta>
            <ta e="T575" id="Seg_6506" s="T574">volk-ziʔ</ta>
            <ta e="T576" id="Seg_6507" s="T575">i</ta>
            <ta e="T577" id="Seg_6508" s="T576">dĭ</ta>
            <ta e="T578" id="Seg_6509" s="T577">nerö-luʔbdə-bi</ta>
            <ta e="T579" id="Seg_6510" s="T578">i</ta>
            <ta e="T580" id="Seg_6511" s="T579">saʔmə-luʔbdə-bi</ta>
            <ta e="T581" id="Seg_6512" s="T580">a</ta>
            <ta e="T582" id="Seg_6513" s="T581">dĭ</ta>
            <ta e="T583" id="Seg_6514" s="T582">nuʔmə-luʔbdə-bi</ta>
            <ta e="T584" id="Seg_6515" s="T583">dĭgəttə</ta>
            <ta e="T585" id="Seg_6516" s="T584">bĭdə-liA-t</ta>
            <ta e="T586" id="Seg_6517" s="T585">Ivanuška</ta>
            <ta e="T587" id="Seg_6518" s="T586">Elenuška-jəʔ-ziʔ</ta>
            <ta e="T588" id="Seg_6519" s="T587">ĭmbi</ta>
            <ta e="T589" id="Seg_6520" s="T588">tăn</ta>
            <ta e="T590" id="Seg_6521" s="T589">dĭn</ta>
            <ta e="T591" id="Seg_6522" s="T590">nu-laʔbə-liA-l</ta>
            <ta e="T592" id="Seg_6523" s="T591">da</ta>
            <ta e="T593" id="Seg_6524" s="T592">nʼe</ta>
            <ta e="T594" id="Seg_6525" s="T593">axota</ta>
            <ta e="T595" id="Seg_6526" s="T594">mĭ-zittə</ta>
            <ta e="T596" id="Seg_6527" s="T595">ine</ta>
            <ta e="T597" id="Seg_6528" s="T596">kuvas</ta>
            <ta e="T598" id="Seg_6529" s="T597">a</ta>
            <ta e="T599" id="Seg_6530" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_6531" s="T599">măn-ntə</ta>
            <ta e="T601" id="Seg_6532" s="T600">šaʔ-laːm-t</ta>
            <ta e="T602" id="Seg_6533" s="T601">ine-m</ta>
            <ta e="T603" id="Seg_6534" s="T602">i</ta>
            <ta e="T604" id="Seg_6535" s="T603">koʔbdo-m</ta>
            <ta e="T605" id="Seg_6536" s="T604">dĭgəttə</ta>
            <ta e="T606" id="Seg_6537" s="T605">dĭ</ta>
            <ta e="T607" id="Seg_6538" s="T606">a-luʔbdə-bi</ta>
            <ta e="T608" id="Seg_6539" s="T607">ine-ziʔ</ta>
            <ta e="T609" id="Seg_6540" s="T608">dĭ</ta>
            <ta e="T610" id="Seg_6541" s="T609">dĭ-m</ta>
            <ta e="T611" id="Seg_6542" s="T610">kun-bi</ta>
            <ta e="T612" id="Seg_6543" s="T611">dĭ</ta>
            <ta e="T613" id="Seg_6544" s="T612">i-bi</ta>
            <ta e="T614" id="Seg_6545" s="T613">süjö</ta>
            <ta e="T615" id="Seg_6546" s="T614">nenʼinuʔ</ta>
            <ta e="T616" id="Seg_6547" s="T615">kak</ta>
            <ta e="T617" id="Seg_6548" s="T616">šü</ta>
            <ta e="T618" id="Seg_6549" s="T617">i</ta>
            <ta e="T619" id="Seg_6550" s="T618">bos-də</ta>
            <ta e="T620" id="Seg_6551" s="T619">par-luʔbdə-bi</ta>
            <ta e="T621" id="Seg_6552" s="T620">amnəl-bi</ta>
            <ta e="T622" id="Seg_6553" s="T621">i</ta>
            <ta e="T623" id="Seg_6554" s="T622">koʔbdo-bə</ta>
            <ta e="T624" id="Seg_6555" s="T623">amnəl-bi</ta>
            <ta e="T625" id="Seg_6556" s="T624">ine-Tə</ta>
            <ta e="T626" id="Seg_6557" s="T625">bos-də</ta>
            <ta e="T627" id="Seg_6558" s="T626">amnəl-bi</ta>
            <ta e="T628" id="Seg_6559" s="T627">kan-bi-jəʔ</ta>
            <ta e="T629" id="Seg_6560" s="T628">dĭgəttə</ta>
            <ta e="T630" id="Seg_6561" s="T629">dĭ</ta>
            <ta e="T631" id="Seg_6562" s="T630">koŋ</ta>
            <ta e="T632" id="Seg_6563" s="T631">da</ta>
            <ta e="T633" id="Seg_6564" s="T632">det-KAʔ</ta>
            <ta e="T634" id="Seg_6565" s="T633">dĭ</ta>
            <ta e="T635" id="Seg_6566" s="T634">ine</ta>
            <ta e="T636" id="Seg_6567" s="T635">dĭ</ta>
            <ta e="T637" id="Seg_6568" s="T636">ine</ta>
            <ta e="T638" id="Seg_6569" s="T637">šo-bi</ta>
            <ta e="T639" id="Seg_6570" s="T638">volk-ziʔ</ta>
            <ta e="T640" id="Seg_6571" s="T639">a-luʔbdə-bi</ta>
            <ta e="T641" id="Seg_6572" s="T640">dĭ</ta>
            <ta e="T642" id="Seg_6573" s="T641">nerö-luʔbdə-bi</ta>
            <ta e="T643" id="Seg_6574" s="T642">i</ta>
            <ta e="T644" id="Seg_6575" s="T643">saʔmə-luʔbdə-bi</ta>
            <ta e="T645" id="Seg_6576" s="T644">a</ta>
            <ta e="T646" id="Seg_6577" s="T645">dĭ</ta>
            <ta e="T647" id="Seg_6578" s="T646">nuʔmə-luʔbdə-bi</ta>
            <ta e="T648" id="Seg_6579" s="T647">bĭdə-laʔbə</ta>
            <ta e="T649" id="Seg_6580" s="T648">Vanʼuška-Tə</ta>
            <ta e="T650" id="Seg_6581" s="T649">măn-ntə</ta>
            <ta e="T651" id="Seg_6582" s="T650">no</ta>
            <ta e="T652" id="Seg_6583" s="T651">tüj</ta>
            <ta e="T653" id="Seg_6584" s="T652">măna</ta>
            <ta e="T654" id="Seg_6585" s="T653">kan-zittə</ta>
            <ta e="T655" id="Seg_6586" s="T654">tăn-ziʔ</ta>
            <ta e="T657" id="Seg_6587" s="T656">ma-lV-m</ta>
            <ta e="T658" id="Seg_6588" s="T657">dön</ta>
            <ta e="T659" id="Seg_6589" s="T658">dĭ</ta>
            <ta e="T660" id="Seg_6590" s="T659">süʔmə-luʔbdə-bi</ta>
            <ta e="T661" id="Seg_6591" s="T660">dĭ-Tə</ta>
            <ta e="T662" id="Seg_6592" s="T661">nagur-göʔ</ta>
            <ta e="T663" id="Seg_6593" s="T662">tʼo-Tə</ta>
            <ta e="T664" id="Seg_6594" s="T663">saʔmə-luʔbdə-bi</ta>
            <ta e="T665" id="Seg_6595" s="T664">kabarləj</ta>
            <ta e="T666" id="Seg_6596" s="T665">dĭgəttə</ta>
            <ta e="T668" id="Seg_6597" s="T667">dĭ-zAŋ</ta>
            <ta e="T669" id="Seg_6598" s="T668">šo-bi-jəʔ</ta>
            <ta e="T670" id="Seg_6599" s="T669">bos-də</ta>
            <ta e="T672" id="Seg_6600" s="T671">tʼo-Tə</ta>
            <ta e="T673" id="Seg_6601" s="T672">măn-ntə</ta>
            <ta e="T674" id="Seg_6602" s="T673">davaj</ta>
            <ta e="T675" id="Seg_6603" s="T674">amor-lV-bəj</ta>
            <ta e="T676" id="Seg_6604" s="T675">amnə-bəl-bi-jəʔ</ta>
            <ta e="T677" id="Seg_6605" s="T676">amor-bi-jəʔ</ta>
            <ta e="T678" id="Seg_6606" s="T677">i</ta>
            <ta e="T680" id="Seg_6607" s="T679">iʔbə-bi-jəʔ</ta>
            <ta e="T681" id="Seg_6608" s="T680">kunol-zittə</ta>
            <ta e="T682" id="Seg_6609" s="T681">dĭgəttə</ta>
            <ta e="T683" id="Seg_6610" s="T682">kaga-zAŋ-də</ta>
            <ta e="T684" id="Seg_6611" s="T683">šo-bi-jəʔ</ta>
            <ta e="T685" id="Seg_6612" s="T684">dĭ-m</ta>
            <ta e="T686" id="Seg_6613" s="T685">kut-laːm-bi-jəʔ</ta>
            <ta e="T687" id="Seg_6614" s="T686">a</ta>
            <ta e="T688" id="Seg_6615" s="T687">bos-də</ta>
            <ta e="T689" id="Seg_6616" s="T688">i-bi-jəʔ</ta>
            <ta e="T690" id="Seg_6617" s="T689">koʔbdo</ta>
            <ta e="T691" id="Seg_6618" s="T690">i</ta>
            <ta e="T692" id="Seg_6619" s="T691">süjö</ta>
            <ta e="T693" id="Seg_6620" s="T692">i</ta>
            <ta e="T694" id="Seg_6621" s="T693">ine</ta>
            <ta e="T695" id="Seg_6622" s="T694">kan-bi</ta>
            <ta e="T696" id="Seg_6623" s="T695">kandə-laʔbə-jəʔ</ta>
            <ta e="T697" id="Seg_6624" s="T696">dĭgəttə</ta>
            <ta e="T698" id="Seg_6625" s="T697">volk</ta>
            <ta e="T699" id="Seg_6626" s="T698">šo-bi</ta>
            <ta e="T701" id="Seg_6627" s="T700">ku-liA-t</ta>
            <ta e="T702" id="Seg_6628" s="T701">dĭ</ta>
            <ta e="T703" id="Seg_6629" s="T702">iʔbö-laʔbə</ta>
            <ta e="T704" id="Seg_6630" s="T703">dʼabə-bi</ta>
            <ta e="T705" id="Seg_6631" s="T704">süjö</ta>
            <ta e="T707" id="Seg_6632" s="T706">üdʼüge</ta>
            <ta e="T708" id="Seg_6633" s="T707">süjö</ta>
            <ta e="T709" id="Seg_6634" s="T708">da</ta>
            <ta e="T710" id="Seg_6635" s="T709">det-ʔ</ta>
            <ta e="T711" id="Seg_6636" s="T710">kü-bi</ta>
            <ta e="T712" id="Seg_6637" s="T711">bü</ta>
            <ta e="T713" id="Seg_6638" s="T712">i</ta>
            <ta e="T714" id="Seg_6639" s="T713">tʼili</ta>
            <ta e="T715" id="Seg_6640" s="T714">bü</ta>
            <ta e="T716" id="Seg_6641" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_6642" s="T716">süjö</ta>
            <ta e="T718" id="Seg_6643" s="T717">nʼergö-luʔbdə-bi</ta>
            <ta e="T719" id="Seg_6644" s="T718">det-bi</ta>
            <ta e="T720" id="Seg_6645" s="T719">dĭ</ta>
            <ta e="T721" id="Seg_6646" s="T720">dĭ-m</ta>
            <ta e="T722" id="Seg_6647" s="T721">bazə-bi</ta>
            <ta e="T724" id="Seg_6648" s="T723">kule</ta>
            <ta e="T725" id="Seg_6649" s="T724">bü-ziʔ</ta>
            <ta e="T726" id="Seg_6650" s="T725">i</ta>
            <ta e="T727" id="Seg_6651" s="T726">dĭgəttə</ta>
            <ta e="T728" id="Seg_6652" s="T727">dʼilʼuj</ta>
            <ta e="T729" id="Seg_6653" s="T728">bü-ziʔ</ta>
            <ta e="T730" id="Seg_6654" s="T729">dĭ</ta>
            <ta e="T731" id="Seg_6655" s="T730">süʔmə-laʔbə</ta>
            <ta e="T732" id="Seg_6656" s="T731">kondʼo</ta>
            <ta e="T733" id="Seg_6657" s="T732">măn</ta>
            <ta e="T734" id="Seg_6658" s="T733">kunol-bi-m</ta>
            <ta e="T735" id="Seg_6659" s="T734">kondʼo</ta>
            <ta e="T736" id="Seg_6660" s="T735">kunol-bi-l</ta>
            <ta e="T738" id="Seg_6661" s="T737">kabɨ</ta>
            <ta e="T739" id="Seg_6662" s="T738">ej</ta>
            <ta e="T740" id="Seg_6663" s="T739">măn</ta>
            <ta e="T741" id="Seg_6664" s="T740">tak</ta>
            <ta e="T742" id="Seg_6665" s="T741">tăn</ta>
            <ta e="T743" id="Seg_6666" s="T742">üge</ta>
            <ta e="T744" id="Seg_6667" s="T743">bɨ</ta>
            <ta e="T745" id="Seg_6668" s="T744">kunol-bi-l</ta>
            <ta e="T746" id="Seg_6669" s="T745">tănan</ta>
            <ta e="T747" id="Seg_6670" s="T746">kut-luʔbdə-bi-jəʔ</ta>
            <ta e="T748" id="Seg_6671" s="T747">tăn</ta>
            <ta e="T750" id="Seg_6672" s="T749">kaga-zAŋ-l</ta>
            <ta e="T751" id="Seg_6673" s="T750">i</ta>
            <ta e="T752" id="Seg_6674" s="T751">bar</ta>
            <ta e="T753" id="Seg_6675" s="T752">i-luʔbdə-bi-jəʔ</ta>
            <ta e="T754" id="Seg_6676" s="T753">koʔbdo-m</ta>
            <ta e="T755" id="Seg_6677" s="T754">i</ta>
            <ta e="T756" id="Seg_6678" s="T755">süjö-m</ta>
            <ta e="T757" id="Seg_6679" s="T756">i</ta>
            <ta e="T758" id="Seg_6680" s="T757">ine-m</ta>
            <ta e="T759" id="Seg_6681" s="T758">dĭgəttə</ta>
            <ta e="T760" id="Seg_6682" s="T759">amnə-ʔ</ta>
            <ta e="T761" id="Seg_6683" s="T760">măna</ta>
            <ta e="T762" id="Seg_6684" s="T761">amnol-bi</ta>
            <ta e="T763" id="Seg_6685" s="T762">dĭ-zAŋ</ta>
            <ta e="T764" id="Seg_6686" s="T763">bĭdə-bi-jəʔ</ta>
            <ta e="T765" id="Seg_6687" s="T764">dĭ-m</ta>
            <ta e="T766" id="Seg_6688" s="T765">dĭ</ta>
            <ta e="T767" id="Seg_6689" s="T766">volk</ta>
            <ta e="T768" id="Seg_6690" s="T767">kaga-zAŋ-də</ta>
            <ta e="T769" id="Seg_6691" s="T768">saj-nüzə-bi</ta>
            <ta e="T770" id="Seg_6692" s="T769">i</ta>
            <ta e="T771" id="Seg_6693" s="T770">bos-də</ta>
            <ta e="T772" id="Seg_6694" s="T771">i-bi</ta>
            <ta e="T773" id="Seg_6695" s="T772">koʔbdo</ta>
            <ta e="T774" id="Seg_6696" s="T773">i</ta>
            <ta e="T775" id="Seg_6697" s="T774">bar</ta>
            <ta e="T776" id="Seg_6698" s="T775">ĭmbi</ta>
            <ta e="T777" id="Seg_6699" s="T776">dĭn</ta>
            <ta e="T778" id="Seg_6700" s="T777">i-bi</ta>
            <ta e="T779" id="Seg_6701" s="T778">dĭgəttə</ta>
            <ta e="T780" id="Seg_6702" s="T779">dĭ</ta>
            <ta e="T781" id="Seg_6703" s="T780">Ivanuška</ta>
            <ta e="T782" id="Seg_6704" s="T781">dĭ-Tə</ta>
            <ta e="T783" id="Seg_6705" s="T782">saʔmə-luʔbdə-bi</ta>
            <ta e="T784" id="Seg_6706" s="T783">üjü-gəndə</ta>
            <ta e="T785" id="Seg_6707" s="T784">i</ta>
            <ta e="T786" id="Seg_6708" s="T785">par-luʔbdə-bi</ta>
            <ta e="T787" id="Seg_6709" s="T786">maʔ-gəndə</ta>
            <ta e="T788" id="Seg_6710" s="T787">šo-bi</ta>
            <ta e="T789" id="Seg_6711" s="T788">aba-gəndə</ta>
            <ta e="T790" id="Seg_6712" s="T789">bar</ta>
            <ta e="T791" id="Seg_6713" s="T790">ĭmbi</ta>
            <ta e="T792" id="Seg_6714" s="T791">nörbə-bi</ta>
            <ta e="T793" id="Seg_6715" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_6716" s="T793">aba-t</ta>
            <ta e="T795" id="Seg_6717" s="T794">tʼor-bi</ta>
            <ta e="T796" id="Seg_6718" s="T795">ajir-bi</ta>
            <ta e="T797" id="Seg_6719" s="T796">dĭgəttə</ta>
            <ta e="T798" id="Seg_6720" s="T797">dĭ</ta>
            <ta e="T799" id="Seg_6721" s="T798">koʔbdo-m</ta>
            <ta e="T800" id="Seg_6722" s="T799">i-bi</ta>
            <ta e="T801" id="Seg_6723" s="T800">monoʔko-bi-jəʔ</ta>
            <ta e="T802" id="Seg_6724" s="T801">ara</ta>
            <ta e="T803" id="Seg_6725" s="T802">bĭs-bi-jəʔ</ta>
            <ta e="T804" id="Seg_6726" s="T803">i</ta>
            <ta e="T805" id="Seg_6727" s="T804">tüjö</ta>
            <ta e="T806" id="Seg_6728" s="T805">üge</ta>
            <ta e="T807" id="Seg_6729" s="T806">amno-laʔbə-jəʔ</ta>
            <ta e="T808" id="Seg_6730" s="T807">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6731" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_6732" s="T1">chief.[NOM.SG]</ta>
            <ta e="T3" id="Seg_6733" s="T2">live-DUR-PST</ta>
            <ta e="T4" id="Seg_6734" s="T3">this-GEN</ta>
            <ta e="T5" id="Seg_6735" s="T4">be-PST-3PL</ta>
            <ta e="T6" id="Seg_6736" s="T5">three-COLL</ta>
            <ta e="T7" id="Seg_6737" s="T6">boy-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T8" id="Seg_6738" s="T7">this-GEN</ta>
            <ta e="T9" id="Seg_6739" s="T8">garden.[NOM.SG]</ta>
            <ta e="T10" id="Seg_6740" s="T9">be-PST.[3SG]</ta>
            <ta e="T12" id="Seg_6741" s="T11">sit-DUR.PST-3PL</ta>
            <ta e="T13" id="Seg_6742" s="T12">beautiful-PL</ta>
            <ta e="T14" id="Seg_6743" s="T13">who.[NOM.SG]=INDEF</ta>
            <ta e="T15" id="Seg_6744" s="T14">PTCL</ta>
            <ta e="T16" id="Seg_6745" s="T15">steal-DUR.[3SG]</ta>
            <ta e="T17" id="Seg_6746" s="T16">this.[NOM.SG]</ta>
            <ta e="T18" id="Seg_6747" s="T17">PTCL</ta>
            <ta e="T19" id="Seg_6748" s="T18">see-PRS-3SG.O</ta>
            <ta e="T20" id="Seg_6749" s="T19">who.[NOM.SG]=INDEF</ta>
            <ta e="T21" id="Seg_6750" s="T20">steal-DUR.[3SG]</ta>
            <ta e="T22" id="Seg_6751" s="T21">place-PST.[3SG]</ta>
            <ta e="T23" id="Seg_6752" s="T22">people-EP-ACC</ta>
            <ta e="T24" id="Seg_6753" s="T23">look-FRQ-INF.LAT</ta>
            <ta e="T25" id="Seg_6754" s="T24">this-PL</ta>
            <ta e="T26" id="Seg_6755" s="T25">look-PRS-3PL</ta>
            <ta e="T27" id="Seg_6756" s="T26">NEG.EX.[3SG]</ta>
            <ta e="T28" id="Seg_6757" s="T27">then</ta>
            <ta e="T29" id="Seg_6758" s="T28">one.[NOM.SG]</ta>
            <ta e="T30" id="Seg_6759" s="T29">boy.[NOM.SG]</ta>
            <ta e="T32" id="Seg_6760" s="T31">say-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_6761" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_6762" s="T33">go-FUT-1SG</ta>
            <ta e="T35" id="Seg_6763" s="T34">this.[NOM.SG]</ta>
            <ta e="T36" id="Seg_6764" s="T35">go-PST.[3SG]</ta>
            <ta e="T37" id="Seg_6765" s="T36">sleep-MOM-PST.[3SG]</ta>
            <ta e="T38" id="Seg_6766" s="T37">what.[NOM.SG]=INDEF</ta>
            <ta e="T39" id="Seg_6767" s="T38">NEG</ta>
            <ta e="T40" id="Seg_6768" s="T39">see-PST.[3SG]</ta>
            <ta e="T41" id="Seg_6769" s="T40">then</ta>
            <ta e="T42" id="Seg_6770" s="T41">another.[NOM.SG]</ta>
            <ta e="T43" id="Seg_6771" s="T42">boy.[NOM.SG]</ta>
            <ta e="T45" id="Seg_6772" s="T44">go-PST.[3SG]</ta>
            <ta e="T46" id="Seg_6773" s="T45">also</ta>
            <ta e="T47" id="Seg_6774" s="T46">sleep-MOM-PST.[3SG]</ta>
            <ta e="T48" id="Seg_6775" s="T47">what.[NOM.SG]=INDEF</ta>
            <ta e="T49" id="Seg_6776" s="T48">NEG</ta>
            <ta e="T50" id="Seg_6777" s="T49">see-PST.[3SG]</ta>
            <ta e="T51" id="Seg_6778" s="T50">then</ta>
            <ta e="T52" id="Seg_6779" s="T51">Ivanushka.[NOM.SG]</ta>
            <ta e="T53" id="Seg_6780" s="T52">go-PST.[3SG]</ta>
            <ta e="T54" id="Seg_6781" s="T53">NEG</ta>
            <ta e="T55" id="Seg_6782" s="T54">sleep-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_6783" s="T55">sleep-INF.LAT</ta>
            <ta e="T57" id="Seg_6784" s="T56">one.wants</ta>
            <ta e="T58" id="Seg_6785" s="T57">this.[NOM.SG]</ta>
            <ta e="T59" id="Seg_6786" s="T58">PTCL</ta>
            <ta e="T60" id="Seg_6787" s="T59">water-INS</ta>
            <ta e="T61" id="Seg_6788" s="T60">eye-ACC.3SG</ta>
            <ta e="T62" id="Seg_6789" s="T61">wash-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_6790" s="T62">and</ta>
            <ta e="T64" id="Seg_6791" s="T63">again</ta>
            <ta e="T65" id="Seg_6792" s="T64">go-DUR.[3SG]</ta>
            <ta e="T66" id="Seg_6793" s="T65">then</ta>
            <ta e="T67" id="Seg_6794" s="T66">see-PRS-3SG.O</ta>
            <ta e="T68" id="Seg_6795" s="T67">see-RES-PST.[3SG]</ta>
            <ta e="T69" id="Seg_6796" s="T68">PTCL</ta>
            <ta e="T70" id="Seg_6797" s="T69">burn-MOM-PST.[3SG]</ta>
            <ta e="T71" id="Seg_6798" s="T70">fire.[NOM.SG]</ta>
            <ta e="T72" id="Seg_6799" s="T71">this.[NOM.SG]</ta>
            <ta e="T74" id="Seg_6800" s="T73">see-RES-PST.[3SG]</ta>
            <ta e="T76" id="Seg_6801" s="T75">bird.[NOM.SG]</ta>
            <ta e="T77" id="Seg_6802" s="T76">sit-DUR.[3SG]</ta>
            <ta e="T78" id="Seg_6803" s="T77">fire.[NOM.SG]</ta>
            <ta e="T79" id="Seg_6804" s="T78">such.[NOM.SG]</ta>
            <ta e="T80" id="Seg_6805" s="T79">this-GEN</ta>
            <ta e="T81" id="Seg_6806" s="T80">punch-MOM-PST.[3SG]</ta>
            <ta e="T82" id="Seg_6807" s="T81">capture-MOM-PST.[3SG]</ta>
            <ta e="T83" id="Seg_6808" s="T82">one.[NOM.SG]</ta>
            <ta e="T84" id="Seg_6809" s="T83">feather.[NOM.SG]</ta>
            <ta e="T86" id="Seg_6810" s="T85">bring-PST.[3SG]</ta>
            <ta e="T87" id="Seg_6811" s="T86">then</ta>
            <ta e="T88" id="Seg_6812" s="T87">come-PST.[3SG]</ta>
            <ta e="T89" id="Seg_6813" s="T88">father-LAT</ta>
            <ta e="T90" id="Seg_6814" s="T89">well</ta>
            <ta e="T91" id="Seg_6815" s="T90">what.[NOM.SG]</ta>
            <ta e="T92" id="Seg_6816" s="T91">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T94" id="Seg_6817" s="T93">I.NOM</ta>
            <ta e="T95" id="Seg_6818" s="T94">see-PST-1SG</ta>
            <ta e="T96" id="Seg_6819" s="T95">who.[NOM.SG]</ta>
            <ta e="T97" id="Seg_6820" s="T96">steal-DUR.[3SG]</ta>
            <ta e="T98" id="Seg_6821" s="T97">that.[NOM.SG]</ta>
            <ta e="T99" id="Seg_6822" s="T98">you.DAT</ta>
            <ta e="T100" id="Seg_6823" s="T99">bring-PST-1SG</ta>
            <ta e="T101" id="Seg_6824" s="T100">feather.[NOM.SG]</ta>
            <ta e="T102" id="Seg_6825" s="T101">this.[NOM.SG]</ta>
            <ta e="T106" id="Seg_6826" s="T105">go-DUR.[3SG]</ta>
            <ta e="T107" id="Seg_6827" s="T106">and</ta>
            <ta e="T108" id="Seg_6828" s="T107">steal-DUR.[3SG]</ta>
            <ta e="T109" id="Seg_6829" s="T108">then</ta>
            <ta e="T110" id="Seg_6830" s="T109">father-NOM/GEN.3SG</ta>
            <ta e="T112" id="Seg_6831" s="T111">eat-INF.LAT</ta>
            <ta e="T113" id="Seg_6832" s="T112">then</ta>
            <ta e="T114" id="Seg_6833" s="T113">think-PST.[3SG]</ta>
            <ta e="T115" id="Seg_6834" s="T114">think-PST.[3SG]</ta>
            <ta e="T116" id="Seg_6835" s="T115">go-IMP.2PL</ta>
            <ta e="T117" id="Seg_6836" s="T116">boy-PL</ta>
            <ta e="T118" id="Seg_6837" s="T117">where.to=INDEF</ta>
            <ta e="T119" id="Seg_6838" s="T118">all</ta>
            <ta e="T120" id="Seg_6839" s="T119">earth.[NOM.SG]</ta>
            <ta e="T121" id="Seg_6840" s="T120">go-IMP.2PL</ta>
            <ta e="T122" id="Seg_6841" s="T121">people.[NOM.SG]</ta>
            <ta e="T126" id="Seg_6842" s="T125">maybe</ta>
            <ta e="T127" id="Seg_6843" s="T126">where</ta>
            <ta e="T130" id="Seg_6844" s="T129">where=INDEF</ta>
            <ta e="T131" id="Seg_6845" s="T130">this.[NOM.SG]</ta>
            <ta e="T132" id="Seg_6846" s="T131">bird.[NOM.SG]</ta>
            <ta e="T136" id="Seg_6847" s="T135">capture-FUT-2PL</ta>
            <ta e="T137" id="Seg_6848" s="T136">what.kind</ta>
            <ta e="T138" id="Seg_6849" s="T137">fire.[NOM.SG]</ta>
            <ta e="T139" id="Seg_6850" s="T138">this-PL</ta>
            <ta e="T141" id="Seg_6851" s="T140">go-PST-3PL</ta>
            <ta e="T142" id="Seg_6852" s="T141">one.[NOM.SG]</ta>
            <ta e="T143" id="Seg_6853" s="T142">there</ta>
            <ta e="T144" id="Seg_6854" s="T143">another.[NOM.SG]</ta>
            <ta e="T145" id="Seg_6855" s="T144">here</ta>
            <ta e="T146" id="Seg_6856" s="T145">Vanyushka.[NOM.SG]</ta>
            <ta e="T147" id="Seg_6857" s="T146">also</ta>
            <ta e="T148" id="Seg_6858" s="T147">go-PST.[3SG]</ta>
            <ta e="T149" id="Seg_6859" s="T148">enough</ta>
            <ta e="T150" id="Seg_6860" s="T149">then</ta>
            <ta e="T151" id="Seg_6861" s="T150">this.[NOM.SG]</ta>
            <ta e="T152" id="Seg_6862" s="T151">Ivan_Tsarevich.[NOM.SG]</ta>
            <ta e="T153" id="Seg_6863" s="T152">go-PST.[3SG]</ta>
            <ta e="T154" id="Seg_6864" s="T153">come-PST.[3SG]</ta>
            <ta e="T155" id="Seg_6865" s="T154">come-PST.[3SG]</ta>
            <ta e="T156" id="Seg_6866" s="T155">horse-ACC.3SG</ta>
            <ta e="T157" id="Seg_6867" s="T156">harness-MOM-PST.[3SG]</ta>
            <ta e="T158" id="Seg_6868" s="T157">eat-PST.[3SG]</ta>
            <ta e="T160" id="Seg_6869" s="T159">lie.down-PST.[3SG]</ta>
            <ta e="T161" id="Seg_6870" s="T160">sleep-INF.LAT</ta>
            <ta e="T162" id="Seg_6871" s="T161">horse-NOM/GEN.3SG</ta>
            <ta e="T163" id="Seg_6872" s="T162">go-DUR.[3SG]</ta>
            <ta e="T164" id="Seg_6873" s="T163">foot-ACC.3SG</ta>
            <ta e="T165" id="Seg_6874" s="T164">bind-PST.[3SG]</ta>
            <ta e="T166" id="Seg_6875" s="T165">long.time</ta>
            <ta e="T167" id="Seg_6876" s="T166">sleep-PST.[3SG]</ta>
            <ta e="T168" id="Seg_6877" s="T167">then</ta>
            <ta e="T169" id="Seg_6878" s="T168">get.up-PST.[3SG]</ta>
            <ta e="T170" id="Seg_6879" s="T169">see-PRS-3SG.O</ta>
            <ta e="T171" id="Seg_6880" s="T170">horse-NOM/GEN.3SG</ta>
            <ta e="T172" id="Seg_6881" s="T171">NEG.EX.[3SG]</ta>
            <ta e="T173" id="Seg_6882" s="T172">look-FRQ-PST.[3SG]</ta>
            <ta e="T174" id="Seg_6883" s="T173">look-FRQ-PST.[3SG]</ta>
            <ta e="T175" id="Seg_6884" s="T174">only</ta>
            <ta e="T176" id="Seg_6885" s="T175">bone-NOM/GEN/ACC.3PL</ta>
            <ta e="T177" id="Seg_6886" s="T176">lie-DUR-3PL</ta>
            <ta e="T178" id="Seg_6887" s="T177">sit-DUR.[3SG]</ta>
            <ta e="T179" id="Seg_6888" s="T178">and</ta>
            <ta e="T180" id="Seg_6889" s="T179">then</ta>
            <ta e="T181" id="Seg_6890" s="T180">one.should</ta>
            <ta e="T182" id="Seg_6891" s="T181">go-INF.LAT</ta>
            <ta e="T183" id="Seg_6892" s="T182">on.foot</ta>
            <ta e="T184" id="Seg_6893" s="T183">walk-PRS.[3SG]</ta>
            <ta e="T185" id="Seg_6894" s="T184">walk-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_6895" s="T185">then</ta>
            <ta e="T187" id="Seg_6896" s="T186">get.tired-MOM-PST.[3SG]</ta>
            <ta e="T188" id="Seg_6897" s="T187">and</ta>
            <ta e="T189" id="Seg_6898" s="T188">PTCL</ta>
            <ta e="T190" id="Seg_6899" s="T189">sit.down-MOM-PST.[3SG]</ta>
            <ta e="T191" id="Seg_6900" s="T190">sit-DUR.[3SG]</ta>
            <ta e="T192" id="Seg_6901" s="T191">then</ta>
            <ta e="T193" id="Seg_6902" s="T192">wolf.[NOM.SG]</ta>
            <ta e="T194" id="Seg_6903" s="T193">run-DUR.[3SG]</ta>
            <ta e="T195" id="Seg_6904" s="T194">what.[NOM.SG]</ta>
            <ta e="T196" id="Seg_6905" s="T195">sit-DUR-2SG</ta>
            <ta e="T197" id="Seg_6906" s="T196">head-NOM/GEN/ACC.2SG</ta>
            <ta e="T198" id="Seg_6907" s="T197">hang.up-PST-1SG</ta>
            <ta e="T199" id="Seg_6908" s="T198">PTCL</ta>
            <ta e="T200" id="Seg_6909" s="T199">I.NOM</ta>
            <ta e="T201" id="Seg_6910" s="T200">horse-ACC</ta>
            <ta e="T202" id="Seg_6911" s="T201">eat-MOM-PST.[3SG]</ta>
            <ta e="T203" id="Seg_6912" s="T202">and</ta>
            <ta e="T204" id="Seg_6913" s="T203">this.[NOM.SG]</ta>
            <ta e="T205" id="Seg_6914" s="T204">I.NOM</ta>
            <ta e="T206" id="Seg_6915" s="T205">eat-PST-1SG</ta>
            <ta e="T207" id="Seg_6916" s="T206">so</ta>
            <ta e="T209" id="Seg_6917" s="T208">and</ta>
            <ta e="T210" id="Seg_6918" s="T209">where.to</ta>
            <ta e="T211" id="Seg_6919" s="T210">you.NOM</ta>
            <ta e="T212" id="Seg_6920" s="T211">walk-PRS-2SG</ta>
            <ta e="T213" id="Seg_6921" s="T212">PTCL</ta>
            <ta e="T214" id="Seg_6922" s="T213">walk-PRS-1SG</ta>
            <ta e="T215" id="Seg_6923" s="T214">bird.[NOM.SG]</ta>
            <ta e="T216" id="Seg_6924" s="T215">bird.[NOM.SG]</ta>
            <ta e="T218" id="Seg_6925" s="T217">look-FRQ-INF.LAT</ta>
            <ta e="T219" id="Seg_6926" s="T218">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T220" id="Seg_6927" s="T219">say-IPFVZ.[3SG]</ta>
            <ta e="T221" id="Seg_6928" s="T220">look-FRQ-EP-IMP.2SG</ta>
            <ta e="T222" id="Seg_6929" s="T221">bird.[NOM.SG]</ta>
            <ta e="T223" id="Seg_6930" s="T222">fire.[NOM.SG]</ta>
            <ta e="T224" id="Seg_6931" s="T223">such.[NOM.SG]</ta>
            <ta e="T225" id="Seg_6932" s="T224">then</ta>
            <ta e="T226" id="Seg_6933" s="T225">this.[NOM.SG]</ta>
            <ta e="T227" id="Seg_6934" s="T226">well</ta>
            <ta e="T228" id="Seg_6935" s="T227">you.NOM</ta>
            <ta e="T229" id="Seg_6936" s="T228">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T230" id="Seg_6937" s="T229">horse-2SG-INS</ta>
            <ta e="T231" id="Seg_6938" s="T230">so</ta>
            <ta e="T232" id="Seg_6939" s="T231">three.[NOM.SG]</ta>
            <ta e="T233" id="Seg_6940" s="T232">winter.[NOM.SG]</ta>
            <ta e="T234" id="Seg_6941" s="T233">there</ta>
            <ta e="T235" id="Seg_6942" s="T234">NEG</ta>
            <ta e="T236" id="Seg_6943" s="T235">go-FUT-2SG</ta>
            <ta e="T237" id="Seg_6944" s="T236">sit.down-IMP.2SG</ta>
            <ta e="T239" id="Seg_6945" s="T238">I.LAT</ta>
            <ta e="T240" id="Seg_6946" s="T239">I.NOM</ta>
            <ta e="T241" id="Seg_6947" s="T240">you.ACC</ta>
            <ta e="T242" id="Seg_6948" s="T241">bring-RES-FUT-1SG</ta>
            <ta e="T243" id="Seg_6949" s="T242">sit.down-PST.[3SG]</ta>
            <ta e="T244" id="Seg_6950" s="T243">then</ta>
            <ta e="T245" id="Seg_6951" s="T244">this.[NOM.SG]</ta>
            <ta e="T246" id="Seg_6952" s="T245">bring-PST.[3SG]</ta>
            <ta e="T247" id="Seg_6953" s="T246">go-EP-IMP.2SG</ta>
            <ta e="T248" id="Seg_6954" s="T247">now</ta>
            <ta e="T249" id="Seg_6955" s="T248">PTCL</ta>
            <ta e="T250" id="Seg_6956" s="T249">people.[NOM.SG]</ta>
            <ta e="T251" id="Seg_6957" s="T250">sleep-DUR-3PL</ta>
            <ta e="T252" id="Seg_6958" s="T251">take-IMP.2SG</ta>
            <ta e="T253" id="Seg_6959" s="T252">this.[NOM.SG]</ta>
            <ta e="T254" id="Seg_6960" s="T253">bird.[NOM.SG]</ta>
            <ta e="T255" id="Seg_6961" s="T254">and</ta>
            <ta e="T256" id="Seg_6962" s="T255">coop-ACC.3SG</ta>
            <ta e="T257" id="Seg_6963" s="T256">NEG.AUX-IMP.2SG</ta>
            <ta e="T258" id="Seg_6964" s="T257">take-IMP.2SG.O</ta>
            <ta e="T260" id="Seg_6965" s="T258">otherwise</ta>
            <ta e="T261" id="Seg_6966" s="T260">then</ta>
            <ta e="T262" id="Seg_6967" s="T261">this.[NOM.SG]</ta>
            <ta e="T263" id="Seg_6968" s="T262">go-PST.[3SG]</ta>
            <ta e="T264" id="Seg_6969" s="T263">take-PST.[3SG]</ta>
            <ta e="T265" id="Seg_6970" s="T264">bird.[NOM.SG]</ta>
            <ta e="T266" id="Seg_6971" s="T265">look-FRQ-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_6972" s="T266">coop.[NOM.SG]</ta>
            <ta e="T268" id="Seg_6973" s="T267">beautiful.[NOM.SG]</ta>
            <ta e="T269" id="Seg_6974" s="T268">one.should</ta>
            <ta e="T270" id="Seg_6975" s="T269">take-INF.LAT</ta>
            <ta e="T271" id="Seg_6976" s="T270">like</ta>
            <ta e="T272" id="Seg_6977" s="T271">this-ACC</ta>
            <ta e="T273" id="Seg_6978" s="T272">lift-MOM-PST.[3SG]</ta>
            <ta e="T274" id="Seg_6979" s="T273">PTCL</ta>
            <ta e="T275" id="Seg_6980" s="T274">rumble-MOM-PST.[3SG]</ta>
            <ta e="T276" id="Seg_6981" s="T275">PTCL</ta>
            <ta e="T277" id="Seg_6982" s="T276">this-ACC</ta>
            <ta e="T278" id="Seg_6983" s="T277">capture-MOM-PST.[3SG]</ta>
            <ta e="T279" id="Seg_6984" s="T278">and</ta>
            <ta e="T282" id="Seg_6985" s="T281">bring-PST-3PL</ta>
            <ta e="T283" id="Seg_6986" s="T282">chief-LAT</ta>
            <ta e="T284" id="Seg_6987" s="T283">enough</ta>
            <ta e="T285" id="Seg_6988" s="T284">then</ta>
            <ta e="T286" id="Seg_6989" s="T285">this.[NOM.SG]</ta>
            <ta e="T287" id="Seg_6990" s="T286">come-PST.[3SG]</ta>
            <ta e="T288" id="Seg_6991" s="T287">chief-LAT</ta>
            <ta e="T289" id="Seg_6992" s="T288">you.NOM</ta>
            <ta e="T290" id="Seg_6993" s="T289">what.[NOM.SG]</ta>
            <ta e="T291" id="Seg_6994" s="T290">what.[NOM.SG]</ta>
            <ta e="T292" id="Seg_6995" s="T291">here</ta>
            <ta e="T293" id="Seg_6996" s="T292">come-PST-1SG</ta>
            <ta e="T294" id="Seg_6997" s="T293">steal-INF.LAT</ta>
            <ta e="T295" id="Seg_6998" s="T294">who-GEN</ta>
            <ta e="T296" id="Seg_6999" s="T295">you.NOM</ta>
            <ta e="T297" id="Seg_7000" s="T296">boy.[NOM.SG]</ta>
            <ta e="T298" id="Seg_7001" s="T297">I.NOM</ta>
            <ta e="T299" id="Seg_7002" s="T298">Ivan_Tsarevich.[NOM.SG]</ta>
            <ta e="T300" id="Seg_7003" s="T299">so</ta>
            <ta e="T301" id="Seg_7004" s="T300">you.NOM</ta>
            <ta e="T302" id="Seg_7005" s="T301">steal-INF.LAT</ta>
            <ta e="T304" id="Seg_7006" s="T303">then</ta>
            <ta e="T305" id="Seg_7007" s="T304">this.[NOM.SG]</ta>
            <ta e="T306" id="Seg_7008" s="T305">say-IPFVZ.[3SG]</ta>
            <ta e="T307" id="Seg_7009" s="T306">and</ta>
            <ta e="T308" id="Seg_7010" s="T307">this.[NOM.SG]</ta>
            <ta e="T309" id="Seg_7011" s="T308">we.LAT</ta>
            <ta e="T310" id="Seg_7012" s="T309">go-PST.[3SG]</ta>
            <ta e="T311" id="Seg_7013" s="T310">and</ta>
            <ta e="T313" id="Seg_7014" s="T312">eat-PST-3PL</ta>
            <ta e="T314" id="Seg_7015" s="T313">and</ta>
            <ta e="T315" id="Seg_7016" s="T314">you.NOM</ta>
            <ta e="T316" id="Seg_7017" s="T315">come-PST-2SG</ta>
            <ta e="T317" id="Seg_7018" s="T316">IRREAL</ta>
            <ta e="T318" id="Seg_7019" s="T317">and</ta>
            <ta e="T319" id="Seg_7020" s="T318">I.NOM</ta>
            <ta e="T320" id="Seg_7021" s="T319">so</ta>
            <ta e="T321" id="Seg_7022" s="T320">you.DAT</ta>
            <ta e="T322" id="Seg_7023" s="T321">give-PST-1SG</ta>
            <ta e="T323" id="Seg_7024" s="T322">IRREAL</ta>
            <ta e="T324" id="Seg_7025" s="T323">otherwise</ta>
            <ta e="T325" id="Seg_7026" s="T324">steal-INF.LAT</ta>
            <ta e="T326" id="Seg_7027" s="T325">come-PST-2SG</ta>
            <ta e="T327" id="Seg_7028" s="T326">well</ta>
            <ta e="T328" id="Seg_7029" s="T327">I.NOM</ta>
            <ta e="T329" id="Seg_7030" s="T328">you.DAT</ta>
            <ta e="T330" id="Seg_7031" s="T329">what.[NOM.SG]=INDEF</ta>
            <ta e="T331" id="Seg_7032" s="T330">NEG</ta>
            <ta e="T332" id="Seg_7033" s="T331">make-FUT-1SG</ta>
            <ta e="T333" id="Seg_7034" s="T332">go-EP-IMP.2SG</ta>
            <ta e="T334" id="Seg_7035" s="T333">horse.[NOM.SG]</ta>
            <ta e="T335" id="Seg_7036" s="T334">bring-IMP.2SG</ta>
            <ta e="T337" id="Seg_7037" s="T336">mane-INS</ta>
            <ta e="T338" id="Seg_7038" s="T337">then</ta>
            <ta e="T339" id="Seg_7039" s="T338">this.[NOM.SG]</ta>
            <ta e="T340" id="Seg_7040" s="T339">go-PST.[3SG]</ta>
            <ta e="T341" id="Seg_7041" s="T340">wolf-LAT</ta>
            <ta e="T342" id="Seg_7042" s="T341">wolf.[NOM.SG]</ta>
            <ta e="T343" id="Seg_7043" s="T342">say-IPFVZ.[3SG]</ta>
            <ta e="T344" id="Seg_7044" s="T343">I.NOM</ta>
            <ta e="T345" id="Seg_7045" s="T344">say-PST-1SG</ta>
            <ta e="T346" id="Seg_7046" s="T345">you.DAT</ta>
            <ta e="T347" id="Seg_7047" s="T346">NEG.AUX-IMP.2SG</ta>
            <ta e="T348" id="Seg_7048" s="T347">%touch-EP-CNG</ta>
            <ta e="T349" id="Seg_7049" s="T348">and</ta>
            <ta e="T350" id="Seg_7050" s="T349">you.NOM</ta>
            <ta e="T351" id="Seg_7051" s="T350">%touch-PST-2SG</ta>
            <ta e="T352" id="Seg_7052" s="T351">and</ta>
            <ta e="T353" id="Seg_7053" s="T352">now</ta>
            <ta e="T355" id="Seg_7054" s="T354">sit.down-IMP.2SG</ta>
            <ta e="T356" id="Seg_7055" s="T355">I.LAT</ta>
            <ta e="T357" id="Seg_7056" s="T356">go-OPT.DU/PL-1DU</ta>
            <ta e="T358" id="Seg_7057" s="T357">go-PST-3PL</ta>
            <ta e="T359" id="Seg_7058" s="T358">well</ta>
            <ta e="T360" id="Seg_7059" s="T359">come-PST-3PL</ta>
            <ta e="T361" id="Seg_7060" s="T360">there</ta>
            <ta e="T362" id="Seg_7061" s="T361">then</ta>
            <ta e="T363" id="Seg_7062" s="T362">well</ta>
            <ta e="T364" id="Seg_7063" s="T363">go-EP-IMP.2SG</ta>
            <ta e="T365" id="Seg_7064" s="T364">and</ta>
            <ta e="T366" id="Seg_7065" s="T365">capture-IMP.2SG.O</ta>
            <ta e="T367" id="Seg_7066" s="T366">horse-ACC</ta>
            <ta e="T368" id="Seg_7067" s="T367">and</ta>
            <ta e="T369" id="Seg_7068" s="T368">halter-NOM/GEN/ACC.1SG</ta>
            <ta e="T370" id="Seg_7069" s="T369">NEG</ta>
            <ta e="T372" id="Seg_7070" s="T371">NEG.AUX-IMP.2SG</ta>
            <ta e="T374" id="Seg_7071" s="T373">NEG.AUX-IMP.2SG</ta>
            <ta e="T376" id="Seg_7072" s="T375">NEG.AUX-IMP.2SG</ta>
            <ta e="T377" id="Seg_7073" s="T376">take-CNG</ta>
            <ta e="T378" id="Seg_7074" s="T377">otherwise</ta>
            <ta e="T379" id="Seg_7075" s="T378">again</ta>
            <ta e="T380" id="Seg_7076" s="T379">so</ta>
            <ta e="T381" id="Seg_7077" s="T380">become-RES-FUT-3SG</ta>
            <ta e="T382" id="Seg_7078" s="T381">this.[NOM.SG]</ta>
            <ta e="T383" id="Seg_7079" s="T382">come-PST.[3SG]</ta>
            <ta e="T384" id="Seg_7080" s="T383">horse-ACC</ta>
            <ta e="T385" id="Seg_7081" s="T384">take-PST.[3SG]</ta>
            <ta e="T386" id="Seg_7082" s="T385">and</ta>
            <ta e="T387" id="Seg_7083" s="T386">halter-NOM/GEN.3SG</ta>
            <ta e="T388" id="Seg_7084" s="T387">very</ta>
            <ta e="T389" id="Seg_7085" s="T388">beautiful.[NOM.SG]</ta>
            <ta e="T390" id="Seg_7086" s="T389">what.[NOM.SG]</ta>
            <ta e="T391" id="Seg_7087" s="T390">horse-LOC</ta>
            <ta e="T392" id="Seg_7088" s="T391">one.should</ta>
            <ta e="T393" id="Seg_7089" s="T392">dress-INF.LAT</ta>
            <ta e="T394" id="Seg_7090" s="T393">only</ta>
            <ta e="T395" id="Seg_7091" s="T394">take-PST.[3SG]</ta>
            <ta e="T396" id="Seg_7092" s="T395">this-ACC</ta>
            <ta e="T397" id="Seg_7093" s="T396">this.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7094" s="T397">PTCL</ta>
            <ta e="T399" id="Seg_7095" s="T398">shout-MOM-PST.[3SG]</ta>
            <ta e="T400" id="Seg_7096" s="T399">people.[NOM.SG]</ta>
            <ta e="T401" id="Seg_7097" s="T400">jump-MOM-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7098" s="T401">this-ACC</ta>
            <ta e="T403" id="Seg_7099" s="T402">take-PST-3PL</ta>
            <ta e="T405" id="Seg_7100" s="T404">bring-RES-PST-3PL</ta>
            <ta e="T406" id="Seg_7101" s="T405">chief-LAT</ta>
            <ta e="T407" id="Seg_7102" s="T406">what.[NOM.SG]</ta>
            <ta e="T408" id="Seg_7103" s="T407">come-PST-2SG</ta>
            <ta e="T409" id="Seg_7104" s="T408">horse.[NOM.SG]</ta>
            <ta e="T410" id="Seg_7105" s="T409">steal-INF.LAT</ta>
            <ta e="T411" id="Seg_7106" s="T410">who-GEN</ta>
            <ta e="T412" id="Seg_7107" s="T411">you.NOM</ta>
            <ta e="T413" id="Seg_7108" s="T412">I.NOM</ta>
            <ta e="T414" id="Seg_7109" s="T413">I.NOM</ta>
            <ta e="T415" id="Seg_7110" s="T414">Ivan_Tsarevich.[NOM.SG]</ta>
            <ta e="T416" id="Seg_7111" s="T415">so</ta>
            <ta e="T417" id="Seg_7112" s="T416">steal-INF.LAT</ta>
            <ta e="T419" id="Seg_7113" s="T418">come-PST-2SG</ta>
            <ta e="T420" id="Seg_7114" s="T419">well</ta>
            <ta e="T421" id="Seg_7115" s="T420">go-EP-IMP.2SG</ta>
            <ta e="T422" id="Seg_7116" s="T421">I.NOM</ta>
            <ta e="T423" id="Seg_7117" s="T422">you.DAT</ta>
            <ta e="T424" id="Seg_7118" s="T423">what.[NOM.SG]=INDEF</ta>
            <ta e="T425" id="Seg_7119" s="T424">NEG</ta>
            <ta e="T426" id="Seg_7120" s="T425">make-FUT-1SG</ta>
            <ta e="T427" id="Seg_7121" s="T426">there</ta>
            <ta e="T428" id="Seg_7122" s="T427">be-PRS.[3SG]</ta>
            <ta e="T429" id="Seg_7123" s="T428">girl.[NOM.SG]</ta>
            <ta e="T432" id="Seg_7124" s="T431">bring-IMP.2SG.O</ta>
            <ta e="T433" id="Seg_7125" s="T432">I.LAT</ta>
            <ta e="T434" id="Seg_7126" s="T433">this-ACC</ta>
            <ta e="T435" id="Seg_7127" s="T434">then</ta>
            <ta e="T436" id="Seg_7128" s="T435">this.[NOM.SG]</ta>
            <ta e="T438" id="Seg_7129" s="T437">come-PRS.[3SG]</ta>
            <ta e="T439" id="Seg_7130" s="T438">and</ta>
            <ta e="T440" id="Seg_7131" s="T439">wolf-NOM/GEN/ACC.3SG</ta>
            <ta e="T441" id="Seg_7132" s="T440">say-PST-1SG</ta>
            <ta e="T442" id="Seg_7133" s="T441">NEG.AUX-IMP.2SG</ta>
            <ta e="T443" id="Seg_7134" s="T442">%touch-EP-CNG</ta>
            <ta e="T444" id="Seg_7135" s="T443">and</ta>
            <ta e="T445" id="Seg_7136" s="T444">you.NOM</ta>
            <ta e="T446" id="Seg_7137" s="T445">always</ta>
            <ta e="T447" id="Seg_7138" s="T446">%touch-MOM-PST-1SG</ta>
            <ta e="T448" id="Seg_7139" s="T447">well</ta>
            <ta e="T449" id="Seg_7140" s="T448">what.[NOM.SG]=INDEF</ta>
            <ta e="T450" id="Seg_7141" s="T449">%%</ta>
            <ta e="T451" id="Seg_7142" s="T450">this.[NOM.SG]</ta>
            <ta e="T452" id="Seg_7143" s="T451">say-IPFVZ.[3SG]</ta>
            <ta e="T453" id="Seg_7144" s="T452">well</ta>
            <ta e="T454" id="Seg_7145" s="T453">sit-IMP.2SG</ta>
            <ta e="T455" id="Seg_7146" s="T454">go-OPT.DU/PL-1DU</ta>
            <ta e="T456" id="Seg_7147" s="T455">come-PST-3PL</ta>
            <ta e="T457" id="Seg_7148" s="T456">there</ta>
            <ta e="T458" id="Seg_7149" s="T457">then</ta>
            <ta e="T459" id="Seg_7150" s="T458">wolf.[NOM.SG]</ta>
            <ta e="T460" id="Seg_7151" s="T459">say-IPFVZ.[3SG]</ta>
            <ta e="T461" id="Seg_7152" s="T460">this-LAT</ta>
            <ta e="T462" id="Seg_7153" s="T461">you.NOM</ta>
            <ta e="T464" id="Seg_7154" s="T463">go-EP-IMP.2SG</ta>
            <ta e="T465" id="Seg_7155" s="T464">return-EP-IMP.2SG</ta>
            <ta e="T466" id="Seg_7156" s="T465">return-EP-IMP.2SG</ta>
            <ta e="T467" id="Seg_7157" s="T466">and</ta>
            <ta e="T468" id="Seg_7158" s="T467">I.NOM</ta>
            <ta e="T469" id="Seg_7159" s="T468">go-FUT-1SG</ta>
            <ta e="T470" id="Seg_7160" s="T469">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T471" id="Seg_7161" s="T470">then</ta>
            <ta e="T472" id="Seg_7162" s="T471">jump-MOM-PST.[3SG]</ta>
            <ta e="T473" id="Seg_7163" s="T472">garden-LAT</ta>
            <ta e="T474" id="Seg_7164" s="T473">there</ta>
            <ta e="T475" id="Seg_7165" s="T474">hide-RES-PST.[3SG]</ta>
            <ta e="T476" id="Seg_7166" s="T475">this-PL</ta>
            <ta e="T477" id="Seg_7167" s="T476">go-DES-DUR-3PL</ta>
            <ta e="T478" id="Seg_7168" s="T477">mother-NOM/GEN.3SG</ta>
            <ta e="T479" id="Seg_7169" s="T478">nanny-NOM/GEN/ACC.3PL</ta>
            <ta e="T480" id="Seg_7170" s="T479">this.[NOM.SG]</ta>
            <ta e="T481" id="Seg_7171" s="T480">Elena-NOM/GEN.3SG</ta>
            <ta e="T482" id="Seg_7172" s="T481">remain-MOM-PST.[3SG]</ta>
            <ta e="T483" id="Seg_7173" s="T482">this.[NOM.SG]</ta>
            <ta e="T484" id="Seg_7174" s="T483">this-ACC</ta>
            <ta e="T485" id="Seg_7175" s="T484">grab-MOM-PST.[3SG]</ta>
            <ta e="T486" id="Seg_7176" s="T485">and</ta>
            <ta e="T487" id="Seg_7177" s="T486">run-MOM-PST.[3SG]</ta>
            <ta e="T488" id="Seg_7178" s="T487">look</ta>
            <ta e="T489" id="Seg_7179" s="T488">run-DUR.[3SG]</ta>
            <ta e="T490" id="Seg_7180" s="T489">and</ta>
            <ta e="T491" id="Seg_7181" s="T490">catch.up-PST.[3SG]</ta>
            <ta e="T492" id="Seg_7182" s="T491">Ivan-ACC</ta>
            <ta e="T493" id="Seg_7183" s="T492">sit-IMP.2SG</ta>
            <ta e="T494" id="Seg_7184" s="T493">sit.down-PST-3PL</ta>
            <ta e="T495" id="Seg_7185" s="T494">then</ta>
            <ta e="T496" id="Seg_7186" s="T495">come-PST-3PL</ta>
            <ta e="T497" id="Seg_7187" s="T496">there</ta>
            <ta e="T498" id="Seg_7188" s="T497">where</ta>
            <ta e="T500" id="Seg_7189" s="T499">horse.[NOM.SG]</ta>
            <ta e="T501" id="Seg_7190" s="T500">look.for-PST-3PL</ta>
            <ta e="T502" id="Seg_7191" s="T501">then</ta>
            <ta e="T503" id="Seg_7192" s="T502">this-PL</ta>
            <ta e="T504" id="Seg_7193" s="T503">come-PST-3PL</ta>
            <ta e="T505" id="Seg_7194" s="T504">and</ta>
            <ta e="T506" id="Seg_7195" s="T505">this.[NOM.SG]</ta>
            <ta e="T507" id="Seg_7196" s="T506">think-DUR.[3SG]</ta>
            <ta e="T508" id="Seg_7197" s="T507">always</ta>
            <ta e="T509" id="Seg_7198" s="T508">Ivan_Tsarevich.[NOM.SG]</ta>
            <ta e="T510" id="Seg_7199" s="T509">pity-PRS.[3SG]</ta>
            <ta e="T511" id="Seg_7200" s="T510">this.[NOM.SG]</ta>
            <ta e="T512" id="Seg_7201" s="T511">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T513" id="Seg_7202" s="T512">and</ta>
            <ta e="T514" id="Seg_7203" s="T513">wolf-NOM/GEN/ACC.3SG</ta>
            <ta e="T515" id="Seg_7204" s="T514">ask-DUR.[3SG]</ta>
            <ta e="T516" id="Seg_7205" s="T515">what.[NOM.SG]</ta>
            <ta e="T517" id="Seg_7206" s="T516">you.NOM</ta>
            <ta e="T518" id="Seg_7207" s="T517">there</ta>
            <ta e="T519" id="Seg_7208" s="T518">stand-DUR-2SG</ta>
            <ta e="T520" id="Seg_7209" s="T519">this</ta>
            <ta e="T521" id="Seg_7210" s="T520">such.[NOM.SG]</ta>
            <ta e="T522" id="Seg_7211" s="T521">beautiful.[NOM.SG]</ta>
            <ta e="T523" id="Seg_7212" s="T522">girl.[NOM.SG]</ta>
            <ta e="T524" id="Seg_7213" s="T523">I.NOM</ta>
            <ta e="T525" id="Seg_7214" s="T524">pity-PST-1SG</ta>
            <ta e="T527" id="Seg_7215" s="T526">give-INF.LAT</ta>
            <ta e="T528" id="Seg_7216" s="T527">well</ta>
            <ta e="T529" id="Seg_7217" s="T528">hide-TR-1DU</ta>
            <ta e="T530" id="Seg_7218" s="T529">and</ta>
            <ta e="T531" id="Seg_7219" s="T530">I.NOM</ta>
            <ta e="T533" id="Seg_7220" s="T532">I.NOM</ta>
            <ta e="T534" id="Seg_7221" s="T533">make-DUR-1SG</ta>
            <ta e="T536" id="Seg_7222" s="T535">this-LAT</ta>
            <ta e="T537" id="Seg_7223" s="T536">then</ta>
            <ta e="T538" id="Seg_7224" s="T537">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T539" id="Seg_7225" s="T538">back-LAT</ta>
            <ta e="T540" id="Seg_7226" s="T539">jump-MOM-PST.[3SG]</ta>
            <ta e="T541" id="Seg_7227" s="T540">this.[NOM.SG]</ta>
            <ta e="T542" id="Seg_7228" s="T541">this-ACC</ta>
            <ta e="T543" id="Seg_7229" s="T542">take-PST.[3SG]</ta>
            <ta e="T544" id="Seg_7230" s="T543">bring-RES-PST.[3SG]</ta>
            <ta e="T545" id="Seg_7231" s="T544">and</ta>
            <ta e="T546" id="Seg_7232" s="T545">this.[NOM.SG]</ta>
            <ta e="T547" id="Seg_7233" s="T546">take-PST.[3SG]</ta>
            <ta e="T548" id="Seg_7234" s="T547">this.[NOM.SG]</ta>
            <ta e="T549" id="Seg_7235" s="T548">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T550" id="Seg_7236" s="T549">%%</ta>
            <ta e="T551" id="Seg_7237" s="T550">vodka.[NOM.SG]</ta>
            <ta e="T552" id="Seg_7238" s="T551">drink-PST-3PL</ta>
            <ta e="T553" id="Seg_7239" s="T552">jump-DUR-3PL</ta>
            <ta e="T554" id="Seg_7240" s="T553">horse-ACC.3SG</ta>
            <ta e="T555" id="Seg_7241" s="T554">give-PST.[3SG]</ta>
            <ta e="T556" id="Seg_7242" s="T555">this.[NOM.SG]</ta>
            <ta e="T557" id="Seg_7243" s="T556">horse-ACC.3SG</ta>
            <ta e="T558" id="Seg_7244" s="T557">take-PST.[3SG]</ta>
            <ta e="T559" id="Seg_7245" s="T558">Elena-ACC.3SG</ta>
            <ta e="T560" id="Seg_7246" s="T559">sit.down-%%-PST.[3SG]</ta>
            <ta e="T561" id="Seg_7247" s="T560">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T562" id="Seg_7248" s="T561">sit.down-PST.[3SG]</ta>
            <ta e="T563" id="Seg_7249" s="T562">go-PST-3PL</ta>
            <ta e="T564" id="Seg_7250" s="T563">and</ta>
            <ta e="T565" id="Seg_7251" s="T564">evening-LOC.ADV</ta>
            <ta e="T567" id="Seg_7252" s="T566">lie.down.[PRS]-3PL</ta>
            <ta e="T568" id="Seg_7253" s="T567">sleep-INF.LAT</ta>
            <ta e="T569" id="Seg_7254" s="T568">this</ta>
            <ta e="T571" id="Seg_7255" s="T570">this</ta>
            <ta e="T572" id="Seg_7256" s="T571">Elena-NOM/GEN.3SG</ta>
            <ta e="T574" id="Seg_7257" s="T573">become-RES-PST.[3SG]</ta>
            <ta e="T575" id="Seg_7258" s="T574">wolf-INS</ta>
            <ta e="T576" id="Seg_7259" s="T575">and</ta>
            <ta e="T577" id="Seg_7260" s="T576">this.[NOM.SG]</ta>
            <ta e="T578" id="Seg_7261" s="T577">get.frightened-MOM-PST.[3SG]</ta>
            <ta e="T579" id="Seg_7262" s="T578">and</ta>
            <ta e="T580" id="Seg_7263" s="T579">fall-MOM-PST.[3SG]</ta>
            <ta e="T581" id="Seg_7264" s="T580">and</ta>
            <ta e="T582" id="Seg_7265" s="T581">this.[NOM.SG]</ta>
            <ta e="T583" id="Seg_7266" s="T582">run-MOM-PST.[3SG]</ta>
            <ta e="T584" id="Seg_7267" s="T583">then</ta>
            <ta e="T585" id="Seg_7268" s="T584">catch.up-PRS-3SG.O</ta>
            <ta e="T586" id="Seg_7269" s="T585">Ivanushka.[NOM.SG]</ta>
            <ta e="T587" id="Seg_7270" s="T586">Elenushka-3PL-INS</ta>
            <ta e="T588" id="Seg_7271" s="T587">what.[NOM.SG]</ta>
            <ta e="T589" id="Seg_7272" s="T588">you.NOM</ta>
            <ta e="T590" id="Seg_7273" s="T589">there</ta>
            <ta e="T591" id="Seg_7274" s="T590">stand-DUR-PRS-2SG</ta>
            <ta e="T592" id="Seg_7275" s="T591">PTCL</ta>
            <ta e="T593" id="Seg_7276" s="T592">not</ta>
            <ta e="T594" id="Seg_7277" s="T593">one.wants</ta>
            <ta e="T595" id="Seg_7278" s="T594">give-INF.LAT</ta>
            <ta e="T596" id="Seg_7279" s="T595">horse.[NOM.SG]</ta>
            <ta e="T597" id="Seg_7280" s="T596">beautiful.[NOM.SG]</ta>
            <ta e="T598" id="Seg_7281" s="T597">and</ta>
            <ta e="T599" id="Seg_7282" s="T598">this.[NOM.SG]</ta>
            <ta e="T600" id="Seg_7283" s="T599">say-IPFVZ.[3SG]</ta>
            <ta e="T601" id="Seg_7284" s="T600">hide-RES-IMP.2SG.O</ta>
            <ta e="T602" id="Seg_7285" s="T601">horse-ACC</ta>
            <ta e="T603" id="Seg_7286" s="T602">and</ta>
            <ta e="T604" id="Seg_7287" s="T603">girl-NOM/GEN/ACC.1SG</ta>
            <ta e="T605" id="Seg_7288" s="T604">then</ta>
            <ta e="T606" id="Seg_7289" s="T605">this.[NOM.SG]</ta>
            <ta e="T607" id="Seg_7290" s="T606">make-MOM-PST.[3SG]</ta>
            <ta e="T608" id="Seg_7291" s="T607">horse-INS</ta>
            <ta e="T609" id="Seg_7292" s="T608">this.[NOM.SG]</ta>
            <ta e="T610" id="Seg_7293" s="T609">this-ACC</ta>
            <ta e="T611" id="Seg_7294" s="T610">bring-PST.[3SG]</ta>
            <ta e="T612" id="Seg_7295" s="T611">this.[NOM.SG]</ta>
            <ta e="T613" id="Seg_7296" s="T612">take-PST.[3SG]</ta>
            <ta e="T614" id="Seg_7297" s="T613">bird.[NOM.SG]</ta>
            <ta e="T615" id="Seg_7298" s="T614">%%</ta>
            <ta e="T616" id="Seg_7299" s="T615">like</ta>
            <ta e="T617" id="Seg_7300" s="T616">fire.[NOM.SG]</ta>
            <ta e="T618" id="Seg_7301" s="T617">and</ta>
            <ta e="T619" id="Seg_7302" s="T618">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T620" id="Seg_7303" s="T619">return-MOM-PST.[3SG]</ta>
            <ta e="T621" id="Seg_7304" s="T620">sit.down-PST.[3SG]</ta>
            <ta e="T622" id="Seg_7305" s="T621">and</ta>
            <ta e="T623" id="Seg_7306" s="T622">daughter-ACC.3SG</ta>
            <ta e="T624" id="Seg_7307" s="T623">sit.down-PST.[3SG]</ta>
            <ta e="T625" id="Seg_7308" s="T624">horse-LAT</ta>
            <ta e="T626" id="Seg_7309" s="T625">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T627" id="Seg_7310" s="T626">sit.down-PST.[3SG]</ta>
            <ta e="T628" id="Seg_7311" s="T627">go-PST-3PL</ta>
            <ta e="T629" id="Seg_7312" s="T628">then</ta>
            <ta e="T630" id="Seg_7313" s="T629">this.[NOM.SG]</ta>
            <ta e="T631" id="Seg_7314" s="T630">chief.[NOM.SG]</ta>
            <ta e="T632" id="Seg_7315" s="T631">PTCL</ta>
            <ta e="T633" id="Seg_7316" s="T632">bring-IMP.2PL</ta>
            <ta e="T634" id="Seg_7317" s="T633">this.[NOM.SG]</ta>
            <ta e="T635" id="Seg_7318" s="T634">horse.[NOM.SG]</ta>
            <ta e="T636" id="Seg_7319" s="T635">this.[NOM.SG]</ta>
            <ta e="T637" id="Seg_7320" s="T636">horse.[NOM.SG]</ta>
            <ta e="T638" id="Seg_7321" s="T637">come-PST.[3SG]</ta>
            <ta e="T639" id="Seg_7322" s="T638">wolf-INS</ta>
            <ta e="T640" id="Seg_7323" s="T639">make-MOM-PST.[3SG]</ta>
            <ta e="T641" id="Seg_7324" s="T640">this.[NOM.SG]</ta>
            <ta e="T642" id="Seg_7325" s="T641">get.frightened-MOM-PST.[3SG]</ta>
            <ta e="T643" id="Seg_7326" s="T642">and</ta>
            <ta e="T644" id="Seg_7327" s="T643">fall-MOM-PST.[3SG]</ta>
            <ta e="T645" id="Seg_7328" s="T644">and</ta>
            <ta e="T646" id="Seg_7329" s="T645">this.[NOM.SG]</ta>
            <ta e="T647" id="Seg_7330" s="T646">run-MOM-PST.[3SG]</ta>
            <ta e="T648" id="Seg_7331" s="T647">catch.up-DUR.[3SG]</ta>
            <ta e="T649" id="Seg_7332" s="T648">Vanyushka-LAT</ta>
            <ta e="T650" id="Seg_7333" s="T649">say-IPFVZ.[3SG]</ta>
            <ta e="T651" id="Seg_7334" s="T650">well</ta>
            <ta e="T652" id="Seg_7335" s="T651">now</ta>
            <ta e="T653" id="Seg_7336" s="T652">I.LAT</ta>
            <ta e="T654" id="Seg_7337" s="T653">go-INF.LAT</ta>
            <ta e="T655" id="Seg_7338" s="T654">you.NOM-INS</ta>
            <ta e="T657" id="Seg_7339" s="T656">remain-FUT-1SG</ta>
            <ta e="T658" id="Seg_7340" s="T657">here</ta>
            <ta e="T659" id="Seg_7341" s="T658">this.[NOM.SG]</ta>
            <ta e="T660" id="Seg_7342" s="T659">jump-MOM-PST.[3SG]</ta>
            <ta e="T661" id="Seg_7343" s="T660">this-LAT</ta>
            <ta e="T662" id="Seg_7344" s="T661">three-COLL</ta>
            <ta e="T663" id="Seg_7345" s="T662">place-LAT</ta>
            <ta e="T664" id="Seg_7346" s="T663">fall-MOM-PST.[3SG]</ta>
            <ta e="T665" id="Seg_7347" s="T664">enough</ta>
            <ta e="T666" id="Seg_7348" s="T665">then</ta>
            <ta e="T668" id="Seg_7349" s="T667">this-PL</ta>
            <ta e="T669" id="Seg_7350" s="T668">come-PST-3PL</ta>
            <ta e="T670" id="Seg_7351" s="T669">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T672" id="Seg_7352" s="T671">place-LAT</ta>
            <ta e="T673" id="Seg_7353" s="T672">say-IPFVZ.[3SG]</ta>
            <ta e="T674" id="Seg_7354" s="T673">HORT</ta>
            <ta e="T675" id="Seg_7355" s="T674">eat-FUT-1DU</ta>
            <ta e="T676" id="Seg_7356" s="T675">sit.down-%%-PST-3PL</ta>
            <ta e="T677" id="Seg_7357" s="T676">eat-PST-3PL</ta>
            <ta e="T678" id="Seg_7358" s="T677">and</ta>
            <ta e="T680" id="Seg_7359" s="T679">lie.down-PST-3PL</ta>
            <ta e="T681" id="Seg_7360" s="T680">sleep-INF.LAT</ta>
            <ta e="T682" id="Seg_7361" s="T681">then</ta>
            <ta e="T683" id="Seg_7362" s="T682">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T684" id="Seg_7363" s="T683">come-PST-3PL</ta>
            <ta e="T685" id="Seg_7364" s="T684">this-ACC</ta>
            <ta e="T686" id="Seg_7365" s="T685">kill-RES-PST-3PL</ta>
            <ta e="T687" id="Seg_7366" s="T686">and</ta>
            <ta e="T688" id="Seg_7367" s="T687">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T689" id="Seg_7368" s="T688">take-PST-3PL</ta>
            <ta e="T690" id="Seg_7369" s="T689">girl.[NOM.SG]</ta>
            <ta e="T691" id="Seg_7370" s="T690">and</ta>
            <ta e="T692" id="Seg_7371" s="T691">bird.[NOM.SG]</ta>
            <ta e="T693" id="Seg_7372" s="T692">and</ta>
            <ta e="T694" id="Seg_7373" s="T693">horse.[NOM.SG]</ta>
            <ta e="T695" id="Seg_7374" s="T694">go-PST.[3SG]</ta>
            <ta e="T696" id="Seg_7375" s="T695">walk-DUR-3PL</ta>
            <ta e="T697" id="Seg_7376" s="T696">then</ta>
            <ta e="T698" id="Seg_7377" s="T697">wolf.[NOM.SG]</ta>
            <ta e="T699" id="Seg_7378" s="T698">come-PST.[3SG]</ta>
            <ta e="T701" id="Seg_7379" s="T700">see-PRS-3SG.O</ta>
            <ta e="T702" id="Seg_7380" s="T701">this.[NOM.SG]</ta>
            <ta e="T703" id="Seg_7381" s="T702">lie-DUR.[3SG]</ta>
            <ta e="T704" id="Seg_7382" s="T703">capture-PST.[3SG]</ta>
            <ta e="T705" id="Seg_7383" s="T704">bird.[NOM.SG]</ta>
            <ta e="T707" id="Seg_7384" s="T706">small.[NOM.SG]</ta>
            <ta e="T708" id="Seg_7385" s="T707">bird.[NOM.SG]</ta>
            <ta e="T709" id="Seg_7386" s="T708">PTCL</ta>
            <ta e="T710" id="Seg_7387" s="T709">bring-IMP.2SG</ta>
            <ta e="T711" id="Seg_7388" s="T710">die-PST.[3SG]</ta>
            <ta e="T712" id="Seg_7389" s="T711">water.[NOM.SG]</ta>
            <ta e="T713" id="Seg_7390" s="T712">and</ta>
            <ta e="T714" id="Seg_7391" s="T713">alive.[NOM.SG]</ta>
            <ta e="T715" id="Seg_7392" s="T714">water.[NOM.SG]</ta>
            <ta e="T716" id="Seg_7393" s="T715">this.[NOM.SG]</ta>
            <ta e="T717" id="Seg_7394" s="T716">bird.[NOM.SG]</ta>
            <ta e="T718" id="Seg_7395" s="T717">fly-MOM-PST.[3SG]</ta>
            <ta e="T719" id="Seg_7396" s="T718">bring-PST.[3SG]</ta>
            <ta e="T720" id="Seg_7397" s="T719">this.[NOM.SG]</ta>
            <ta e="T721" id="Seg_7398" s="T720">this-ACC</ta>
            <ta e="T722" id="Seg_7399" s="T721">wash-PST.[3SG]</ta>
            <ta e="T724" id="Seg_7400" s="T723">dead</ta>
            <ta e="T725" id="Seg_7401" s="T724">water-INS</ta>
            <ta e="T726" id="Seg_7402" s="T725">and</ta>
            <ta e="T727" id="Seg_7403" s="T726">then</ta>
            <ta e="T728" id="Seg_7404" s="T727">alive</ta>
            <ta e="T729" id="Seg_7405" s="T728">water-INS</ta>
            <ta e="T730" id="Seg_7406" s="T729">this.[NOM.SG]</ta>
            <ta e="T731" id="Seg_7407" s="T730">jump-DUR.[3SG]</ta>
            <ta e="T732" id="Seg_7408" s="T731">long.time</ta>
            <ta e="T733" id="Seg_7409" s="T732">I.NOM</ta>
            <ta e="T734" id="Seg_7410" s="T733">sleep-PST-1SG</ta>
            <ta e="T735" id="Seg_7411" s="T734">long.time</ta>
            <ta e="T736" id="Seg_7412" s="T735">sleep-PST-2SG</ta>
            <ta e="T738" id="Seg_7413" s="T737">if</ta>
            <ta e="T739" id="Seg_7414" s="T738">NEG</ta>
            <ta e="T740" id="Seg_7415" s="T739">I.NOM</ta>
            <ta e="T741" id="Seg_7416" s="T740">so</ta>
            <ta e="T742" id="Seg_7417" s="T741">you.NOM</ta>
            <ta e="T743" id="Seg_7418" s="T742">always</ta>
            <ta e="T744" id="Seg_7419" s="T743">IRREAL</ta>
            <ta e="T745" id="Seg_7420" s="T744">sleep-PST-2SG</ta>
            <ta e="T746" id="Seg_7421" s="T745">you.ACC</ta>
            <ta e="T747" id="Seg_7422" s="T746">kill-MOM-PST-3PL</ta>
            <ta e="T748" id="Seg_7423" s="T747">you.NOM</ta>
            <ta e="T750" id="Seg_7424" s="T749">brother-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T751" id="Seg_7425" s="T750">and</ta>
            <ta e="T752" id="Seg_7426" s="T751">all</ta>
            <ta e="T753" id="Seg_7427" s="T752">take-MOM-PST-3PL</ta>
            <ta e="T754" id="Seg_7428" s="T753">daughter-ACC</ta>
            <ta e="T755" id="Seg_7429" s="T754">and</ta>
            <ta e="T756" id="Seg_7430" s="T755">bird-ACC</ta>
            <ta e="T757" id="Seg_7431" s="T756">and</ta>
            <ta e="T758" id="Seg_7432" s="T757">horse-ACC</ta>
            <ta e="T759" id="Seg_7433" s="T758">then</ta>
            <ta e="T760" id="Seg_7434" s="T759">sit.down-IMP.2SG</ta>
            <ta e="T761" id="Seg_7435" s="T760">I.LAT</ta>
            <ta e="T762" id="Seg_7436" s="T761">seat-PST.[3SG]</ta>
            <ta e="T763" id="Seg_7437" s="T762">this-PL</ta>
            <ta e="T764" id="Seg_7438" s="T763">catch.up-PST-3PL</ta>
            <ta e="T765" id="Seg_7439" s="T764">this-ACC</ta>
            <ta e="T766" id="Seg_7440" s="T765">this.[NOM.SG]</ta>
            <ta e="T767" id="Seg_7441" s="T766">wolf.[NOM.SG]</ta>
            <ta e="T768" id="Seg_7442" s="T767">brother-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T769" id="Seg_7443" s="T768">off-tear-PST</ta>
            <ta e="T770" id="Seg_7444" s="T769">and</ta>
            <ta e="T771" id="Seg_7445" s="T770">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T772" id="Seg_7446" s="T771">take-PST.[3SG]</ta>
            <ta e="T773" id="Seg_7447" s="T772">girl.[NOM.SG]</ta>
            <ta e="T774" id="Seg_7448" s="T773">and</ta>
            <ta e="T775" id="Seg_7449" s="T774">all</ta>
            <ta e="T776" id="Seg_7450" s="T775">what.[NOM.SG]</ta>
            <ta e="T777" id="Seg_7451" s="T776">there</ta>
            <ta e="T778" id="Seg_7452" s="T777">be-PST.[3SG]</ta>
            <ta e="T779" id="Seg_7453" s="T778">then</ta>
            <ta e="T780" id="Seg_7454" s="T779">this.[NOM.SG]</ta>
            <ta e="T781" id="Seg_7455" s="T780">Ivanushka.[NOM.SG]</ta>
            <ta e="T782" id="Seg_7456" s="T781">this-LAT</ta>
            <ta e="T783" id="Seg_7457" s="T782">fall-MOM-PST.[3SG]</ta>
            <ta e="T784" id="Seg_7458" s="T783">foot-LAT/LOC.3SG</ta>
            <ta e="T785" id="Seg_7459" s="T784">and</ta>
            <ta e="T786" id="Seg_7460" s="T785">return-MOM-PST.[3SG]</ta>
            <ta e="T787" id="Seg_7461" s="T786">tent-LAT/LOC.3SG</ta>
            <ta e="T788" id="Seg_7462" s="T787">come-PST.[3SG]</ta>
            <ta e="T789" id="Seg_7463" s="T788">father-LAT/LOC.3SG</ta>
            <ta e="T790" id="Seg_7464" s="T789">all</ta>
            <ta e="T791" id="Seg_7465" s="T790">what.[NOM.SG]</ta>
            <ta e="T792" id="Seg_7466" s="T791">tell-PST.[3SG]</ta>
            <ta e="T793" id="Seg_7467" s="T792">then</ta>
            <ta e="T794" id="Seg_7468" s="T793">father-NOM/GEN.3SG</ta>
            <ta e="T795" id="Seg_7469" s="T794">cry-PST.[3SG]</ta>
            <ta e="T796" id="Seg_7470" s="T795">pity-PST.[3SG]</ta>
            <ta e="T797" id="Seg_7471" s="T796">then</ta>
            <ta e="T798" id="Seg_7472" s="T797">this.[NOM.SG]</ta>
            <ta e="T799" id="Seg_7473" s="T798">daughter-ACC</ta>
            <ta e="T800" id="Seg_7474" s="T799">take-PST.[3SG]</ta>
            <ta e="T801" id="Seg_7475" s="T800">marry-PST-3PL</ta>
            <ta e="T802" id="Seg_7476" s="T801">vodka.[NOM.SG]</ta>
            <ta e="T803" id="Seg_7477" s="T802">drink-PST-3PL</ta>
            <ta e="T804" id="Seg_7478" s="T803">and</ta>
            <ta e="T805" id="Seg_7479" s="T804">soon</ta>
            <ta e="T806" id="Seg_7480" s="T805">always</ta>
            <ta e="T807" id="Seg_7481" s="T806">live-DUR-3PL</ta>
            <ta e="T808" id="Seg_7482" s="T807">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7483" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_7484" s="T1">вождь.[NOM.SG]</ta>
            <ta e="T3" id="Seg_7485" s="T2">жить-DUR-PST</ta>
            <ta e="T4" id="Seg_7486" s="T3">этот-GEN</ta>
            <ta e="T5" id="Seg_7487" s="T4">быть-PST-3PL</ta>
            <ta e="T6" id="Seg_7488" s="T5">три-COLL</ta>
            <ta e="T7" id="Seg_7489" s="T6">мальчик-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T8" id="Seg_7490" s="T7">этот-GEN</ta>
            <ta e="T9" id="Seg_7491" s="T8">сад.[NOM.SG]</ta>
            <ta e="T10" id="Seg_7492" s="T9">быть-PST.[3SG]</ta>
            <ta e="T12" id="Seg_7493" s="T11">сидеть-DUR.PST-3PL</ta>
            <ta e="T13" id="Seg_7494" s="T12">красивый-PL</ta>
            <ta e="T14" id="Seg_7495" s="T13">кто.[NOM.SG]=INDEF</ta>
            <ta e="T15" id="Seg_7496" s="T14">PTCL</ta>
            <ta e="T16" id="Seg_7497" s="T15">украсть-DUR.[3SG]</ta>
            <ta e="T17" id="Seg_7498" s="T16">этот.[NOM.SG]</ta>
            <ta e="T18" id="Seg_7499" s="T17">PTCL</ta>
            <ta e="T19" id="Seg_7500" s="T18">видеть-PRS-3SG.O</ta>
            <ta e="T20" id="Seg_7501" s="T19">кто.[NOM.SG]=INDEF</ta>
            <ta e="T21" id="Seg_7502" s="T20">украсть-DUR.[3SG]</ta>
            <ta e="T22" id="Seg_7503" s="T21">поставить-PST.[3SG]</ta>
            <ta e="T23" id="Seg_7504" s="T22">люди-EP-ACC</ta>
            <ta e="T24" id="Seg_7505" s="T23">смотреть-FRQ-INF.LAT</ta>
            <ta e="T25" id="Seg_7506" s="T24">этот-PL</ta>
            <ta e="T26" id="Seg_7507" s="T25">смотреть-PRS-3PL</ta>
            <ta e="T27" id="Seg_7508" s="T26">NEG.EX.[3SG]</ta>
            <ta e="T28" id="Seg_7509" s="T27">тогда</ta>
            <ta e="T29" id="Seg_7510" s="T28">один.[NOM.SG]</ta>
            <ta e="T30" id="Seg_7511" s="T29">мальчик.[NOM.SG]</ta>
            <ta e="T32" id="Seg_7512" s="T31">сказать-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_7513" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_7514" s="T33">пойти-FUT-1SG</ta>
            <ta e="T35" id="Seg_7515" s="T34">этот.[NOM.SG]</ta>
            <ta e="T36" id="Seg_7516" s="T35">пойти-PST.[3SG]</ta>
            <ta e="T37" id="Seg_7517" s="T36">спать-MOM-PST.[3SG]</ta>
            <ta e="T38" id="Seg_7518" s="T37">что.[NOM.SG]=INDEF</ta>
            <ta e="T39" id="Seg_7519" s="T38">NEG</ta>
            <ta e="T40" id="Seg_7520" s="T39">видеть-PST.[3SG]</ta>
            <ta e="T41" id="Seg_7521" s="T40">тогда</ta>
            <ta e="T42" id="Seg_7522" s="T41">другой.[NOM.SG]</ta>
            <ta e="T43" id="Seg_7523" s="T42">мальчик.[NOM.SG]</ta>
            <ta e="T45" id="Seg_7524" s="T44">пойти-PST.[3SG]</ta>
            <ta e="T46" id="Seg_7525" s="T45">тоже</ta>
            <ta e="T47" id="Seg_7526" s="T46">спать-MOM-PST.[3SG]</ta>
            <ta e="T48" id="Seg_7527" s="T47">что.[NOM.SG]=INDEF</ta>
            <ta e="T49" id="Seg_7528" s="T48">NEG</ta>
            <ta e="T50" id="Seg_7529" s="T49">видеть-PST.[3SG]</ta>
            <ta e="T51" id="Seg_7530" s="T50">тогда</ta>
            <ta e="T52" id="Seg_7531" s="T51">Иванушка.[NOM.SG]</ta>
            <ta e="T53" id="Seg_7532" s="T52">пойти-PST.[3SG]</ta>
            <ta e="T54" id="Seg_7533" s="T53">NEG</ta>
            <ta e="T55" id="Seg_7534" s="T54">спать-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_7535" s="T55">спать-INF.LAT</ta>
            <ta e="T57" id="Seg_7536" s="T56">хочется</ta>
            <ta e="T58" id="Seg_7537" s="T57">этот.[NOM.SG]</ta>
            <ta e="T59" id="Seg_7538" s="T58">PTCL</ta>
            <ta e="T60" id="Seg_7539" s="T59">вода-INS</ta>
            <ta e="T61" id="Seg_7540" s="T60">глаз-ACC.3SG</ta>
            <ta e="T62" id="Seg_7541" s="T61">мыть-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_7542" s="T62">и</ta>
            <ta e="T64" id="Seg_7543" s="T63">опять</ta>
            <ta e="T65" id="Seg_7544" s="T64">идти-DUR.[3SG]</ta>
            <ta e="T66" id="Seg_7545" s="T65">тогда</ta>
            <ta e="T67" id="Seg_7546" s="T66">видеть-PRS-3SG.O</ta>
            <ta e="T68" id="Seg_7547" s="T67">видеть-RES-PST.[3SG]</ta>
            <ta e="T69" id="Seg_7548" s="T68">PTCL</ta>
            <ta e="T70" id="Seg_7549" s="T69">гореть-MOM-PST.[3SG]</ta>
            <ta e="T71" id="Seg_7550" s="T70">огонь.[NOM.SG]</ta>
            <ta e="T72" id="Seg_7551" s="T71">этот.[NOM.SG]</ta>
            <ta e="T74" id="Seg_7552" s="T73">видеть-RES-PST.[3SG]</ta>
            <ta e="T76" id="Seg_7553" s="T75">птица.[NOM.SG]</ta>
            <ta e="T77" id="Seg_7554" s="T76">сидеть-DUR.[3SG]</ta>
            <ta e="T78" id="Seg_7555" s="T77">огонь.[NOM.SG]</ta>
            <ta e="T79" id="Seg_7556" s="T78">такой.[NOM.SG]</ta>
            <ta e="T80" id="Seg_7557" s="T79">этот-GEN</ta>
            <ta e="T81" id="Seg_7558" s="T80">уколоть-MOM-PST.[3SG]</ta>
            <ta e="T82" id="Seg_7559" s="T81">ловить-MOM-PST.[3SG]</ta>
            <ta e="T83" id="Seg_7560" s="T82">один.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7561" s="T83">перо.[NOM.SG]</ta>
            <ta e="T86" id="Seg_7562" s="T85">принести-PST.[3SG]</ta>
            <ta e="T87" id="Seg_7563" s="T86">тогда</ta>
            <ta e="T88" id="Seg_7564" s="T87">прийти-PST.[3SG]</ta>
            <ta e="T89" id="Seg_7565" s="T88">отец-LAT</ta>
            <ta e="T90" id="Seg_7566" s="T89">ну</ta>
            <ta e="T91" id="Seg_7567" s="T90">что.[NOM.SG]</ta>
            <ta e="T92" id="Seg_7568" s="T91">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T94" id="Seg_7569" s="T93">я.NOM</ta>
            <ta e="T95" id="Seg_7570" s="T94">видеть-PST-1SG</ta>
            <ta e="T96" id="Seg_7571" s="T95">кто.[NOM.SG]</ta>
            <ta e="T97" id="Seg_7572" s="T96">украсть-DUR.[3SG]</ta>
            <ta e="T98" id="Seg_7573" s="T97">тот.[NOM.SG]</ta>
            <ta e="T99" id="Seg_7574" s="T98">ты.DAT</ta>
            <ta e="T100" id="Seg_7575" s="T99">принести-PST-1SG</ta>
            <ta e="T101" id="Seg_7576" s="T100">перо.[NOM.SG]</ta>
            <ta e="T102" id="Seg_7577" s="T101">этот.[NOM.SG]</ta>
            <ta e="T106" id="Seg_7578" s="T105">идти-DUR.[3SG]</ta>
            <ta e="T107" id="Seg_7579" s="T106">и</ta>
            <ta e="T108" id="Seg_7580" s="T107">украсть-DUR.[3SG]</ta>
            <ta e="T109" id="Seg_7581" s="T108">тогда</ta>
            <ta e="T110" id="Seg_7582" s="T109">отец-NOM/GEN.3SG</ta>
            <ta e="T112" id="Seg_7583" s="T111">съесть-INF.LAT</ta>
            <ta e="T113" id="Seg_7584" s="T112">тогда</ta>
            <ta e="T114" id="Seg_7585" s="T113">думать-PST.[3SG]</ta>
            <ta e="T115" id="Seg_7586" s="T114">думать-PST.[3SG]</ta>
            <ta e="T116" id="Seg_7587" s="T115">пойти-IMP.2PL</ta>
            <ta e="T117" id="Seg_7588" s="T116">мальчик-PL</ta>
            <ta e="T118" id="Seg_7589" s="T117">куда=INDEF</ta>
            <ta e="T119" id="Seg_7590" s="T118">весь</ta>
            <ta e="T120" id="Seg_7591" s="T119">земля.[NOM.SG]</ta>
            <ta e="T121" id="Seg_7592" s="T120">идти-IMP.2PL</ta>
            <ta e="T122" id="Seg_7593" s="T121">люди.[NOM.SG]</ta>
            <ta e="T126" id="Seg_7594" s="T125">может.быть</ta>
            <ta e="T127" id="Seg_7595" s="T126">где</ta>
            <ta e="T130" id="Seg_7596" s="T129">где=INDEF</ta>
            <ta e="T131" id="Seg_7597" s="T130">этот.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7598" s="T131">птица.[NOM.SG]</ta>
            <ta e="T136" id="Seg_7599" s="T135">ловить-FUT-2PL</ta>
            <ta e="T137" id="Seg_7600" s="T136">какой</ta>
            <ta e="T138" id="Seg_7601" s="T137">огонь.[NOM.SG]</ta>
            <ta e="T139" id="Seg_7602" s="T138">этот-PL</ta>
            <ta e="T141" id="Seg_7603" s="T140">пойти-PST-3PL</ta>
            <ta e="T142" id="Seg_7604" s="T141">один.[NOM.SG]</ta>
            <ta e="T143" id="Seg_7605" s="T142">там</ta>
            <ta e="T144" id="Seg_7606" s="T143">другой.[NOM.SG]</ta>
            <ta e="T145" id="Seg_7607" s="T144">здесь</ta>
            <ta e="T146" id="Seg_7608" s="T145">Ванюшка.[NOM.SG]</ta>
            <ta e="T147" id="Seg_7609" s="T146">тоже</ta>
            <ta e="T148" id="Seg_7610" s="T147">пойти-PST.[3SG]</ta>
            <ta e="T149" id="Seg_7611" s="T148">хватит</ta>
            <ta e="T150" id="Seg_7612" s="T149">тогда</ta>
            <ta e="T151" id="Seg_7613" s="T150">этот.[NOM.SG]</ta>
            <ta e="T152" id="Seg_7614" s="T151">Иван_Царевич.[NOM.SG]</ta>
            <ta e="T153" id="Seg_7615" s="T152">пойти-PST.[3SG]</ta>
            <ta e="T154" id="Seg_7616" s="T153">прийти-PST.[3SG]</ta>
            <ta e="T155" id="Seg_7617" s="T154">прийти-PST.[3SG]</ta>
            <ta e="T156" id="Seg_7618" s="T155">лошадь-ACC.3SG</ta>
            <ta e="T157" id="Seg_7619" s="T156">запрячь-MOM-PST.[3SG]</ta>
            <ta e="T158" id="Seg_7620" s="T157">съесть-PST.[3SG]</ta>
            <ta e="T160" id="Seg_7621" s="T159">ложиться-PST.[3SG]</ta>
            <ta e="T161" id="Seg_7622" s="T160">спать-INF.LAT</ta>
            <ta e="T162" id="Seg_7623" s="T161">лошадь-NOM/GEN.3SG</ta>
            <ta e="T163" id="Seg_7624" s="T162">идти-DUR.[3SG]</ta>
            <ta e="T164" id="Seg_7625" s="T163">нога-ACC.3SG</ta>
            <ta e="T165" id="Seg_7626" s="T164">завязать-PST.[3SG]</ta>
            <ta e="T166" id="Seg_7627" s="T165">долго</ta>
            <ta e="T167" id="Seg_7628" s="T166">спать-PST.[3SG]</ta>
            <ta e="T168" id="Seg_7629" s="T167">тогда</ta>
            <ta e="T169" id="Seg_7630" s="T168">встать-PST.[3SG]</ta>
            <ta e="T170" id="Seg_7631" s="T169">видеть-PRS-3SG.O</ta>
            <ta e="T171" id="Seg_7632" s="T170">лошадь-NOM/GEN.3SG</ta>
            <ta e="T172" id="Seg_7633" s="T171">NEG.EX.[3SG]</ta>
            <ta e="T173" id="Seg_7634" s="T172">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T174" id="Seg_7635" s="T173">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T175" id="Seg_7636" s="T174">только</ta>
            <ta e="T176" id="Seg_7637" s="T175">кость-NOM/GEN/ACC.3PL</ta>
            <ta e="T177" id="Seg_7638" s="T176">лежать-DUR-3PL</ta>
            <ta e="T178" id="Seg_7639" s="T177">сидеть-DUR.[3SG]</ta>
            <ta e="T179" id="Seg_7640" s="T178">и</ta>
            <ta e="T180" id="Seg_7641" s="T179">тогда</ta>
            <ta e="T181" id="Seg_7642" s="T180">надо</ta>
            <ta e="T182" id="Seg_7643" s="T181">пойти-INF.LAT</ta>
            <ta e="T183" id="Seg_7644" s="T182">пешком</ta>
            <ta e="T184" id="Seg_7645" s="T183">идти-PRS.[3SG]</ta>
            <ta e="T185" id="Seg_7646" s="T184">идти-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_7647" s="T185">тогда</ta>
            <ta e="T187" id="Seg_7648" s="T186">устать-MOM-PST.[3SG]</ta>
            <ta e="T188" id="Seg_7649" s="T187">и</ta>
            <ta e="T189" id="Seg_7650" s="T188">PTCL</ta>
            <ta e="T190" id="Seg_7651" s="T189">сесть-MOM-PST.[3SG]</ta>
            <ta e="T191" id="Seg_7652" s="T190">сидеть-DUR.[3SG]</ta>
            <ta e="T192" id="Seg_7653" s="T191">тогда</ta>
            <ta e="T193" id="Seg_7654" s="T192">волк.[NOM.SG]</ta>
            <ta e="T194" id="Seg_7655" s="T193">бежать-DUR.[3SG]</ta>
            <ta e="T195" id="Seg_7656" s="T194">что.[NOM.SG]</ta>
            <ta e="T196" id="Seg_7657" s="T195">сидеть-DUR-2SG</ta>
            <ta e="T197" id="Seg_7658" s="T196">голова-NOM/GEN/ACC.2SG</ta>
            <ta e="T198" id="Seg_7659" s="T197">вешать-PST-1SG</ta>
            <ta e="T199" id="Seg_7660" s="T198">PTCL</ta>
            <ta e="T200" id="Seg_7661" s="T199">я.NOM</ta>
            <ta e="T201" id="Seg_7662" s="T200">лошадь-ACC</ta>
            <ta e="T202" id="Seg_7663" s="T201">съесть-MOM-PST.[3SG]</ta>
            <ta e="T203" id="Seg_7664" s="T202">а</ta>
            <ta e="T204" id="Seg_7665" s="T203">этот.[NOM.SG]</ta>
            <ta e="T205" id="Seg_7666" s="T204">я.NOM</ta>
            <ta e="T206" id="Seg_7667" s="T205">съесть-PST-1SG</ta>
            <ta e="T207" id="Seg_7668" s="T206">так</ta>
            <ta e="T209" id="Seg_7669" s="T208">а</ta>
            <ta e="T210" id="Seg_7670" s="T209">куда</ta>
            <ta e="T211" id="Seg_7671" s="T210">ты.NOM</ta>
            <ta e="T212" id="Seg_7672" s="T211">идти-PRS-2SG</ta>
            <ta e="T213" id="Seg_7673" s="T212">PTCL</ta>
            <ta e="T214" id="Seg_7674" s="T213">идти-PRS-1SG</ta>
            <ta e="T215" id="Seg_7675" s="T214">птица.[NOM.SG]</ta>
            <ta e="T216" id="Seg_7676" s="T215">птица.[NOM.SG]</ta>
            <ta e="T218" id="Seg_7677" s="T217">смотреть-FRQ-INF.LAT</ta>
            <ta e="T219" id="Seg_7678" s="T218">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T220" id="Seg_7679" s="T219">сказать-IPFVZ.[3SG]</ta>
            <ta e="T221" id="Seg_7680" s="T220">смотреть-FRQ-EP-IMP.2SG</ta>
            <ta e="T222" id="Seg_7681" s="T221">птица.[NOM.SG]</ta>
            <ta e="T223" id="Seg_7682" s="T222">огонь.[NOM.SG]</ta>
            <ta e="T224" id="Seg_7683" s="T223">такой.[NOM.SG]</ta>
            <ta e="T225" id="Seg_7684" s="T224">тогда</ta>
            <ta e="T226" id="Seg_7685" s="T225">этот.[NOM.SG]</ta>
            <ta e="T227" id="Seg_7686" s="T226">ну</ta>
            <ta e="T228" id="Seg_7687" s="T227">ты.NOM</ta>
            <ta e="T229" id="Seg_7688" s="T228">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T230" id="Seg_7689" s="T229">лошадь-2SG-INS</ta>
            <ta e="T231" id="Seg_7690" s="T230">так</ta>
            <ta e="T232" id="Seg_7691" s="T231">три.[NOM.SG]</ta>
            <ta e="T233" id="Seg_7692" s="T232">зима.[NOM.SG]</ta>
            <ta e="T234" id="Seg_7693" s="T233">там</ta>
            <ta e="T235" id="Seg_7694" s="T234">NEG</ta>
            <ta e="T236" id="Seg_7695" s="T235">пойти-FUT-2SG</ta>
            <ta e="T237" id="Seg_7696" s="T236">сесть-IMP.2SG</ta>
            <ta e="T239" id="Seg_7697" s="T238">я.LAT</ta>
            <ta e="T240" id="Seg_7698" s="T239">я.NOM</ta>
            <ta e="T241" id="Seg_7699" s="T240">ты.ACC</ta>
            <ta e="T242" id="Seg_7700" s="T241">нести-RES-FUT-1SG</ta>
            <ta e="T243" id="Seg_7701" s="T242">сесть-PST.[3SG]</ta>
            <ta e="T244" id="Seg_7702" s="T243">тогда</ta>
            <ta e="T245" id="Seg_7703" s="T244">этот.[NOM.SG]</ta>
            <ta e="T246" id="Seg_7704" s="T245">принести-PST.[3SG]</ta>
            <ta e="T247" id="Seg_7705" s="T246">пойти-EP-IMP.2SG</ta>
            <ta e="T248" id="Seg_7706" s="T247">сейчас</ta>
            <ta e="T249" id="Seg_7707" s="T248">PTCL</ta>
            <ta e="T250" id="Seg_7708" s="T249">люди.[NOM.SG]</ta>
            <ta e="T251" id="Seg_7709" s="T250">спать-DUR-3PL</ta>
            <ta e="T252" id="Seg_7710" s="T251">взять-IMP.2SG</ta>
            <ta e="T253" id="Seg_7711" s="T252">этот.[NOM.SG]</ta>
            <ta e="T254" id="Seg_7712" s="T253">птица.[NOM.SG]</ta>
            <ta e="T255" id="Seg_7713" s="T254">а</ta>
            <ta e="T256" id="Seg_7714" s="T255">клетка-ACC.3SG</ta>
            <ta e="T257" id="Seg_7715" s="T256">NEG.AUX-IMP.2SG</ta>
            <ta e="T258" id="Seg_7716" s="T257">взять-IMP.2SG.O</ta>
            <ta e="T260" id="Seg_7717" s="T258">а.то</ta>
            <ta e="T261" id="Seg_7718" s="T260">тогда</ta>
            <ta e="T262" id="Seg_7719" s="T261">этот.[NOM.SG]</ta>
            <ta e="T263" id="Seg_7720" s="T262">пойти-PST.[3SG]</ta>
            <ta e="T264" id="Seg_7721" s="T263">взять-PST.[3SG]</ta>
            <ta e="T265" id="Seg_7722" s="T264">птица.[NOM.SG]</ta>
            <ta e="T266" id="Seg_7723" s="T265">смотреть-FRQ-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_7724" s="T266">клетка.[NOM.SG]</ta>
            <ta e="T268" id="Seg_7725" s="T267">красивый.[NOM.SG]</ta>
            <ta e="T269" id="Seg_7726" s="T268">надо</ta>
            <ta e="T270" id="Seg_7727" s="T269">взять-INF.LAT</ta>
            <ta e="T271" id="Seg_7728" s="T270">как</ta>
            <ta e="T272" id="Seg_7729" s="T271">этот-ACC</ta>
            <ta e="T273" id="Seg_7730" s="T272">поднять-MOM-PST.[3SG]</ta>
            <ta e="T274" id="Seg_7731" s="T273">PTCL</ta>
            <ta e="T275" id="Seg_7732" s="T274">греметь-MOM-PST.[3SG]</ta>
            <ta e="T276" id="Seg_7733" s="T275">PTCL</ta>
            <ta e="T277" id="Seg_7734" s="T276">этот-ACC</ta>
            <ta e="T278" id="Seg_7735" s="T277">ловить-MOM-PST.[3SG]</ta>
            <ta e="T279" id="Seg_7736" s="T278">и</ta>
            <ta e="T282" id="Seg_7737" s="T281">нести-PST-3PL</ta>
            <ta e="T283" id="Seg_7738" s="T282">вождь-LAT</ta>
            <ta e="T284" id="Seg_7739" s="T283">хватит</ta>
            <ta e="T285" id="Seg_7740" s="T284">тогда</ta>
            <ta e="T286" id="Seg_7741" s="T285">этот.[NOM.SG]</ta>
            <ta e="T287" id="Seg_7742" s="T286">прийти-PST.[3SG]</ta>
            <ta e="T288" id="Seg_7743" s="T287">вождь-LAT</ta>
            <ta e="T289" id="Seg_7744" s="T288">ты.NOM</ta>
            <ta e="T290" id="Seg_7745" s="T289">что.[NOM.SG]</ta>
            <ta e="T291" id="Seg_7746" s="T290">что.[NOM.SG]</ta>
            <ta e="T292" id="Seg_7747" s="T291">здесь</ta>
            <ta e="T293" id="Seg_7748" s="T292">прийти-PST-1SG</ta>
            <ta e="T294" id="Seg_7749" s="T293">украсть-INF.LAT</ta>
            <ta e="T295" id="Seg_7750" s="T294">кто-GEN</ta>
            <ta e="T296" id="Seg_7751" s="T295">ты.NOM</ta>
            <ta e="T297" id="Seg_7752" s="T296">мальчик.[NOM.SG]</ta>
            <ta e="T298" id="Seg_7753" s="T297">я.NOM</ta>
            <ta e="T299" id="Seg_7754" s="T298">Иван_Царевич.[NOM.SG]</ta>
            <ta e="T300" id="Seg_7755" s="T299">так</ta>
            <ta e="T301" id="Seg_7756" s="T300">ты.NOM</ta>
            <ta e="T302" id="Seg_7757" s="T301">украсть-INF.LAT</ta>
            <ta e="T304" id="Seg_7758" s="T303">тогда</ta>
            <ta e="T305" id="Seg_7759" s="T304">этот.[NOM.SG]</ta>
            <ta e="T306" id="Seg_7760" s="T305">сказать-IPFVZ.[3SG]</ta>
            <ta e="T307" id="Seg_7761" s="T306">а</ta>
            <ta e="T308" id="Seg_7762" s="T307">этот.[NOM.SG]</ta>
            <ta e="T309" id="Seg_7763" s="T308">мы.LAT</ta>
            <ta e="T310" id="Seg_7764" s="T309">идти-PST.[3SG]</ta>
            <ta e="T311" id="Seg_7765" s="T310">и</ta>
            <ta e="T313" id="Seg_7766" s="T312">съесть-PST-3PL</ta>
            <ta e="T314" id="Seg_7767" s="T313">а</ta>
            <ta e="T315" id="Seg_7768" s="T314">ты.NOM</ta>
            <ta e="T316" id="Seg_7769" s="T315">прийти-PST-2SG</ta>
            <ta e="T317" id="Seg_7770" s="T316">IRREAL</ta>
            <ta e="T318" id="Seg_7771" s="T317">а</ta>
            <ta e="T319" id="Seg_7772" s="T318">я.NOM</ta>
            <ta e="T320" id="Seg_7773" s="T319">так</ta>
            <ta e="T321" id="Seg_7774" s="T320">ты.DAT</ta>
            <ta e="T322" id="Seg_7775" s="T321">дать-PST-1SG</ta>
            <ta e="T323" id="Seg_7776" s="T322">IRREAL</ta>
            <ta e="T324" id="Seg_7777" s="T323">а.то</ta>
            <ta e="T325" id="Seg_7778" s="T324">украсть-INF.LAT</ta>
            <ta e="T326" id="Seg_7779" s="T325">прийти-PST-2SG</ta>
            <ta e="T327" id="Seg_7780" s="T326">ну</ta>
            <ta e="T328" id="Seg_7781" s="T327">я.NOM</ta>
            <ta e="T329" id="Seg_7782" s="T328">ты.DAT</ta>
            <ta e="T330" id="Seg_7783" s="T329">что.[NOM.SG]=INDEF</ta>
            <ta e="T331" id="Seg_7784" s="T330">NEG</ta>
            <ta e="T332" id="Seg_7785" s="T331">делать-FUT-1SG</ta>
            <ta e="T333" id="Seg_7786" s="T332">пойти-EP-IMP.2SG</ta>
            <ta e="T334" id="Seg_7787" s="T333">лошадь.[NOM.SG]</ta>
            <ta e="T335" id="Seg_7788" s="T334">принести-IMP.2SG</ta>
            <ta e="T337" id="Seg_7789" s="T336">грива-INS</ta>
            <ta e="T338" id="Seg_7790" s="T337">тогда</ta>
            <ta e="T339" id="Seg_7791" s="T338">этот.[NOM.SG]</ta>
            <ta e="T340" id="Seg_7792" s="T339">пойти-PST.[3SG]</ta>
            <ta e="T341" id="Seg_7793" s="T340">волк-LAT</ta>
            <ta e="T342" id="Seg_7794" s="T341">волк.[NOM.SG]</ta>
            <ta e="T343" id="Seg_7795" s="T342">сказать-IPFVZ.[3SG]</ta>
            <ta e="T344" id="Seg_7796" s="T343">я.NOM</ta>
            <ta e="T345" id="Seg_7797" s="T344">сказать-PST-1SG</ta>
            <ta e="T346" id="Seg_7798" s="T345">ты.DAT</ta>
            <ta e="T347" id="Seg_7799" s="T346">NEG.AUX-IMP.2SG</ta>
            <ta e="T348" id="Seg_7800" s="T347">%трогать-EP-CNG</ta>
            <ta e="T349" id="Seg_7801" s="T348">а</ta>
            <ta e="T350" id="Seg_7802" s="T349">ты.NOM</ta>
            <ta e="T351" id="Seg_7803" s="T350">%трогать-PST-2SG</ta>
            <ta e="T352" id="Seg_7804" s="T351">а</ta>
            <ta e="T353" id="Seg_7805" s="T352">сейчас</ta>
            <ta e="T355" id="Seg_7806" s="T354">сесть-IMP.2SG</ta>
            <ta e="T356" id="Seg_7807" s="T355">я.LAT</ta>
            <ta e="T357" id="Seg_7808" s="T356">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T358" id="Seg_7809" s="T357">пойти-PST-3PL</ta>
            <ta e="T359" id="Seg_7810" s="T358">ну</ta>
            <ta e="T360" id="Seg_7811" s="T359">прийти-PST-3PL</ta>
            <ta e="T361" id="Seg_7812" s="T360">там</ta>
            <ta e="T362" id="Seg_7813" s="T361">тогда</ta>
            <ta e="T363" id="Seg_7814" s="T362">ну</ta>
            <ta e="T364" id="Seg_7815" s="T363">пойти-EP-IMP.2SG</ta>
            <ta e="T365" id="Seg_7816" s="T364">и</ta>
            <ta e="T366" id="Seg_7817" s="T365">ловить-IMP.2SG.O</ta>
            <ta e="T367" id="Seg_7818" s="T366">лошадь-ACC</ta>
            <ta e="T368" id="Seg_7819" s="T367">а</ta>
            <ta e="T369" id="Seg_7820" s="T368">узда-NOM/GEN/ACC.1SG</ta>
            <ta e="T370" id="Seg_7821" s="T369">NEG</ta>
            <ta e="T372" id="Seg_7822" s="T371">NEG.AUX-IMP.2SG</ta>
            <ta e="T374" id="Seg_7823" s="T373">NEG.AUX-IMP.2SG</ta>
            <ta e="T376" id="Seg_7824" s="T375">NEG.AUX-IMP.2SG</ta>
            <ta e="T377" id="Seg_7825" s="T376">взять-CNG</ta>
            <ta e="T378" id="Seg_7826" s="T377">а.то</ta>
            <ta e="T379" id="Seg_7827" s="T378">опять</ta>
            <ta e="T380" id="Seg_7828" s="T379">так</ta>
            <ta e="T381" id="Seg_7829" s="T380">стать-RES-FUT-3SG</ta>
            <ta e="T382" id="Seg_7830" s="T381">этот.[NOM.SG]</ta>
            <ta e="T383" id="Seg_7831" s="T382">прийти-PST.[3SG]</ta>
            <ta e="T384" id="Seg_7832" s="T383">лошадь-ACC</ta>
            <ta e="T385" id="Seg_7833" s="T384">взять-PST.[3SG]</ta>
            <ta e="T386" id="Seg_7834" s="T385">а</ta>
            <ta e="T387" id="Seg_7835" s="T386">узда-NOM/GEN.3SG</ta>
            <ta e="T388" id="Seg_7836" s="T387">очень</ta>
            <ta e="T389" id="Seg_7837" s="T388">красивый.[NOM.SG]</ta>
            <ta e="T390" id="Seg_7838" s="T389">что.[NOM.SG]</ta>
            <ta e="T391" id="Seg_7839" s="T390">лошадь-LOC</ta>
            <ta e="T392" id="Seg_7840" s="T391">надо</ta>
            <ta e="T393" id="Seg_7841" s="T392">надеть-INF.LAT</ta>
            <ta e="T394" id="Seg_7842" s="T393">только</ta>
            <ta e="T395" id="Seg_7843" s="T394">взять-PST.[3SG]</ta>
            <ta e="T396" id="Seg_7844" s="T395">этот-ACC</ta>
            <ta e="T397" id="Seg_7845" s="T396">этот.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7846" s="T397">PTCL</ta>
            <ta e="T399" id="Seg_7847" s="T398">кричать-MOM-PST.[3SG]</ta>
            <ta e="T400" id="Seg_7848" s="T399">люди.[NOM.SG]</ta>
            <ta e="T401" id="Seg_7849" s="T400">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7850" s="T401">этот-ACC</ta>
            <ta e="T403" id="Seg_7851" s="T402">взять-PST-3PL</ta>
            <ta e="T405" id="Seg_7852" s="T404">нести-RES-PST-3PL</ta>
            <ta e="T406" id="Seg_7853" s="T405">вождь-LAT</ta>
            <ta e="T407" id="Seg_7854" s="T406">что.[NOM.SG]</ta>
            <ta e="T408" id="Seg_7855" s="T407">прийти-PST-2SG</ta>
            <ta e="T409" id="Seg_7856" s="T408">лошадь.[NOM.SG]</ta>
            <ta e="T410" id="Seg_7857" s="T409">украсть-INF.LAT</ta>
            <ta e="T411" id="Seg_7858" s="T410">кто-GEN</ta>
            <ta e="T412" id="Seg_7859" s="T411">ты.NOM</ta>
            <ta e="T413" id="Seg_7860" s="T412">я.NOM</ta>
            <ta e="T414" id="Seg_7861" s="T413">я.NOM</ta>
            <ta e="T415" id="Seg_7862" s="T414">Иван_Царевич.[NOM.SG]</ta>
            <ta e="T416" id="Seg_7863" s="T415">так</ta>
            <ta e="T417" id="Seg_7864" s="T416">украсть-INF.LAT</ta>
            <ta e="T419" id="Seg_7865" s="T418">прийти-PST-2SG</ta>
            <ta e="T420" id="Seg_7866" s="T419">ну</ta>
            <ta e="T421" id="Seg_7867" s="T420">пойти-EP-IMP.2SG</ta>
            <ta e="T422" id="Seg_7868" s="T421">я.NOM</ta>
            <ta e="T423" id="Seg_7869" s="T422">ты.DAT</ta>
            <ta e="T424" id="Seg_7870" s="T423">что.[NOM.SG]=INDEF</ta>
            <ta e="T425" id="Seg_7871" s="T424">NEG</ta>
            <ta e="T426" id="Seg_7872" s="T425">делать-FUT-1SG</ta>
            <ta e="T427" id="Seg_7873" s="T426">там</ta>
            <ta e="T428" id="Seg_7874" s="T427">быть-PRS.[3SG]</ta>
            <ta e="T429" id="Seg_7875" s="T428">девушка.[NOM.SG]</ta>
            <ta e="T432" id="Seg_7876" s="T431">принести-IMP.2SG.O</ta>
            <ta e="T433" id="Seg_7877" s="T432">я.LAT</ta>
            <ta e="T434" id="Seg_7878" s="T433">этот-ACC</ta>
            <ta e="T435" id="Seg_7879" s="T434">тогда</ta>
            <ta e="T436" id="Seg_7880" s="T435">этот.[NOM.SG]</ta>
            <ta e="T438" id="Seg_7881" s="T437">прийти-PRS.[3SG]</ta>
            <ta e="T439" id="Seg_7882" s="T438">а</ta>
            <ta e="T440" id="Seg_7883" s="T439">волк-NOM/GEN/ACC.3SG</ta>
            <ta e="T441" id="Seg_7884" s="T440">сказать-PST-1SG</ta>
            <ta e="T442" id="Seg_7885" s="T441">NEG.AUX-IMP.2SG</ta>
            <ta e="T443" id="Seg_7886" s="T442">%трогать-EP-CNG</ta>
            <ta e="T444" id="Seg_7887" s="T443">а</ta>
            <ta e="T445" id="Seg_7888" s="T444">ты.NOM</ta>
            <ta e="T446" id="Seg_7889" s="T445">всегда</ta>
            <ta e="T447" id="Seg_7890" s="T446">%трогать-MOM-PST-1SG</ta>
            <ta e="T448" id="Seg_7891" s="T447">ну</ta>
            <ta e="T449" id="Seg_7892" s="T448">что.[NOM.SG]=INDEF</ta>
            <ta e="T450" id="Seg_7893" s="T449">%%</ta>
            <ta e="T451" id="Seg_7894" s="T450">этот.[NOM.SG]</ta>
            <ta e="T452" id="Seg_7895" s="T451">сказать-IPFVZ.[3SG]</ta>
            <ta e="T453" id="Seg_7896" s="T452">ну</ta>
            <ta e="T454" id="Seg_7897" s="T453">сидеть-IMP.2SG</ta>
            <ta e="T455" id="Seg_7898" s="T454">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T456" id="Seg_7899" s="T455">прийти-PST-3PL</ta>
            <ta e="T457" id="Seg_7900" s="T456">там</ta>
            <ta e="T458" id="Seg_7901" s="T457">тогда</ta>
            <ta e="T459" id="Seg_7902" s="T458">волк.[NOM.SG]</ta>
            <ta e="T460" id="Seg_7903" s="T459">сказать-IPFVZ.[3SG]</ta>
            <ta e="T461" id="Seg_7904" s="T460">этот-LAT</ta>
            <ta e="T462" id="Seg_7905" s="T461">ты.NOM</ta>
            <ta e="T464" id="Seg_7906" s="T463">пойти-EP-IMP.2SG</ta>
            <ta e="T465" id="Seg_7907" s="T464">вернуться-EP-IMP.2SG</ta>
            <ta e="T466" id="Seg_7908" s="T465">вернуться-EP-IMP.2SG</ta>
            <ta e="T467" id="Seg_7909" s="T466">а</ta>
            <ta e="T468" id="Seg_7910" s="T467">я.NOM</ta>
            <ta e="T469" id="Seg_7911" s="T468">пойти-FUT-1SG</ta>
            <ta e="T470" id="Seg_7912" s="T469">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T471" id="Seg_7913" s="T470">тогда</ta>
            <ta e="T472" id="Seg_7914" s="T471">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T473" id="Seg_7915" s="T472">сад-LAT</ta>
            <ta e="T474" id="Seg_7916" s="T473">там</ta>
            <ta e="T475" id="Seg_7917" s="T474">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T476" id="Seg_7918" s="T475">этот-PL</ta>
            <ta e="T477" id="Seg_7919" s="T476">идти-DES-DUR-3PL</ta>
            <ta e="T478" id="Seg_7920" s="T477">мать-NOM/GEN.3SG</ta>
            <ta e="T479" id="Seg_7921" s="T478">нянька-NOM/GEN/ACC.3PL</ta>
            <ta e="T480" id="Seg_7922" s="T479">этот.[NOM.SG]</ta>
            <ta e="T481" id="Seg_7923" s="T480">Елена-NOM/GEN.3SG</ta>
            <ta e="T482" id="Seg_7924" s="T481">остаться-MOM-PST.[3SG]</ta>
            <ta e="T483" id="Seg_7925" s="T482">этот.[NOM.SG]</ta>
            <ta e="T484" id="Seg_7926" s="T483">этот-ACC</ta>
            <ta e="T485" id="Seg_7927" s="T484">хватать-MOM-PST.[3SG]</ta>
            <ta e="T486" id="Seg_7928" s="T485">и</ta>
            <ta e="T487" id="Seg_7929" s="T486">бежать-MOM-PST.[3SG]</ta>
            <ta e="T488" id="Seg_7930" s="T487">вот</ta>
            <ta e="T489" id="Seg_7931" s="T488">бежать-DUR.[3SG]</ta>
            <ta e="T490" id="Seg_7932" s="T489">и</ta>
            <ta e="T491" id="Seg_7933" s="T490">догонять-PST.[3SG]</ta>
            <ta e="T492" id="Seg_7934" s="T491">Иван-ACC</ta>
            <ta e="T493" id="Seg_7935" s="T492">сидеть-IMP.2SG</ta>
            <ta e="T494" id="Seg_7936" s="T493">сесть-PST-3PL</ta>
            <ta e="T495" id="Seg_7937" s="T494">тогда</ta>
            <ta e="T496" id="Seg_7938" s="T495">прийти-PST-3PL</ta>
            <ta e="T497" id="Seg_7939" s="T496">там</ta>
            <ta e="T498" id="Seg_7940" s="T497">где</ta>
            <ta e="T500" id="Seg_7941" s="T499">лошадь.[NOM.SG]</ta>
            <ta e="T501" id="Seg_7942" s="T500">искать-PST-3PL</ta>
            <ta e="T502" id="Seg_7943" s="T501">тогда</ta>
            <ta e="T503" id="Seg_7944" s="T502">этот-PL</ta>
            <ta e="T504" id="Seg_7945" s="T503">прийти-PST-3PL</ta>
            <ta e="T505" id="Seg_7946" s="T504">а</ta>
            <ta e="T506" id="Seg_7947" s="T505">этот.[NOM.SG]</ta>
            <ta e="T507" id="Seg_7948" s="T506">думать-DUR.[3SG]</ta>
            <ta e="T508" id="Seg_7949" s="T507">всегда</ta>
            <ta e="T509" id="Seg_7950" s="T508">Иван_Царевич.[NOM.SG]</ta>
            <ta e="T510" id="Seg_7951" s="T509">жалеть-PRS.[3SG]</ta>
            <ta e="T511" id="Seg_7952" s="T510">этот.[NOM.SG]</ta>
            <ta e="T512" id="Seg_7953" s="T511">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T513" id="Seg_7954" s="T512">а</ta>
            <ta e="T514" id="Seg_7955" s="T513">волк-NOM/GEN/ACC.3SG</ta>
            <ta e="T515" id="Seg_7956" s="T514">спросить-DUR.[3SG]</ta>
            <ta e="T516" id="Seg_7957" s="T515">что.[NOM.SG]</ta>
            <ta e="T517" id="Seg_7958" s="T516">ты.NOM</ta>
            <ta e="T518" id="Seg_7959" s="T517">там</ta>
            <ta e="T519" id="Seg_7960" s="T518">стоять-DUR-2SG</ta>
            <ta e="T520" id="Seg_7961" s="T519">этот</ta>
            <ta e="T521" id="Seg_7962" s="T520">такой.[NOM.SG]</ta>
            <ta e="T522" id="Seg_7963" s="T521">красивый.[NOM.SG]</ta>
            <ta e="T523" id="Seg_7964" s="T522">девушка.[NOM.SG]</ta>
            <ta e="T524" id="Seg_7965" s="T523">я.NOM</ta>
            <ta e="T525" id="Seg_7966" s="T524">жалеть-PST-1SG</ta>
            <ta e="T527" id="Seg_7967" s="T526">дать-INF.LAT</ta>
            <ta e="T528" id="Seg_7968" s="T527">ну</ta>
            <ta e="T529" id="Seg_7969" s="T528">прятать-TR-1DU</ta>
            <ta e="T530" id="Seg_7970" s="T529">а</ta>
            <ta e="T531" id="Seg_7971" s="T530">я.NOM</ta>
            <ta e="T533" id="Seg_7972" s="T532">я.NOM</ta>
            <ta e="T534" id="Seg_7973" s="T533">делать-DUR-1SG</ta>
            <ta e="T536" id="Seg_7974" s="T535">этот-LAT</ta>
            <ta e="T537" id="Seg_7975" s="T536">тогда</ta>
            <ta e="T538" id="Seg_7976" s="T537">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T539" id="Seg_7977" s="T538">спина-LAT</ta>
            <ta e="T540" id="Seg_7978" s="T539">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T541" id="Seg_7979" s="T540">этот.[NOM.SG]</ta>
            <ta e="T542" id="Seg_7980" s="T541">этот-ACC</ta>
            <ta e="T543" id="Seg_7981" s="T542">взять-PST.[3SG]</ta>
            <ta e="T544" id="Seg_7982" s="T543">нести-RES-PST.[3SG]</ta>
            <ta e="T545" id="Seg_7983" s="T544">а</ta>
            <ta e="T546" id="Seg_7984" s="T545">этот.[NOM.SG]</ta>
            <ta e="T547" id="Seg_7985" s="T546">взять-PST.[3SG]</ta>
            <ta e="T548" id="Seg_7986" s="T547">этот.[NOM.SG]</ta>
            <ta e="T549" id="Seg_7987" s="T548">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T550" id="Seg_7988" s="T549">%%</ta>
            <ta e="T551" id="Seg_7989" s="T550">водка.[NOM.SG]</ta>
            <ta e="T552" id="Seg_7990" s="T551">пить-PST-3PL</ta>
            <ta e="T553" id="Seg_7991" s="T552">прыгнуть-DUR-3PL</ta>
            <ta e="T554" id="Seg_7992" s="T553">лошадь-ACC.3SG</ta>
            <ta e="T555" id="Seg_7993" s="T554">дать-PST.[3SG]</ta>
            <ta e="T556" id="Seg_7994" s="T555">этот.[NOM.SG]</ta>
            <ta e="T557" id="Seg_7995" s="T556">лошадь-ACC.3SG</ta>
            <ta e="T558" id="Seg_7996" s="T557">взять-PST.[3SG]</ta>
            <ta e="T559" id="Seg_7997" s="T558">Елена-ACC.3SG</ta>
            <ta e="T560" id="Seg_7998" s="T559">сесть-%%-PST.[3SG]</ta>
            <ta e="T561" id="Seg_7999" s="T560">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T562" id="Seg_8000" s="T561">сесть-PST.[3SG]</ta>
            <ta e="T563" id="Seg_8001" s="T562">пойти-PST-3PL</ta>
            <ta e="T564" id="Seg_8002" s="T563">а</ta>
            <ta e="T565" id="Seg_8003" s="T564">вечер-LOC.ADV</ta>
            <ta e="T567" id="Seg_8004" s="T566">ложиться.[PRS]-3PL</ta>
            <ta e="T568" id="Seg_8005" s="T567">спать-INF.LAT</ta>
            <ta e="T569" id="Seg_8006" s="T568">этот</ta>
            <ta e="T571" id="Seg_8007" s="T570">этот</ta>
            <ta e="T572" id="Seg_8008" s="T571">Елена-NOM/GEN.3SG</ta>
            <ta e="T574" id="Seg_8009" s="T573">стать-RES-PST.[3SG]</ta>
            <ta e="T575" id="Seg_8010" s="T574">волк-INS</ta>
            <ta e="T576" id="Seg_8011" s="T575">и</ta>
            <ta e="T577" id="Seg_8012" s="T576">этот.[NOM.SG]</ta>
            <ta e="T578" id="Seg_8013" s="T577">пугаться-MOM-PST.[3SG]</ta>
            <ta e="T579" id="Seg_8014" s="T578">и</ta>
            <ta e="T580" id="Seg_8015" s="T579">упасть-MOM-PST.[3SG]</ta>
            <ta e="T581" id="Seg_8016" s="T580">а</ta>
            <ta e="T582" id="Seg_8017" s="T581">этот.[NOM.SG]</ta>
            <ta e="T583" id="Seg_8018" s="T582">бежать-MOM-PST.[3SG]</ta>
            <ta e="T584" id="Seg_8019" s="T583">тогда</ta>
            <ta e="T585" id="Seg_8020" s="T584">догонять-PRS-3SG.O</ta>
            <ta e="T586" id="Seg_8021" s="T585">Иванушка.[NOM.SG]</ta>
            <ta e="T587" id="Seg_8022" s="T586">Еленушка-3PL-INS</ta>
            <ta e="T588" id="Seg_8023" s="T587">что.[NOM.SG]</ta>
            <ta e="T589" id="Seg_8024" s="T588">ты.NOM</ta>
            <ta e="T590" id="Seg_8025" s="T589">там</ta>
            <ta e="T591" id="Seg_8026" s="T590">стоять-DUR-PRS-2SG</ta>
            <ta e="T592" id="Seg_8027" s="T591">PTCL</ta>
            <ta e="T593" id="Seg_8028" s="T592">не</ta>
            <ta e="T594" id="Seg_8029" s="T593">хочется</ta>
            <ta e="T595" id="Seg_8030" s="T594">дать-INF.LAT</ta>
            <ta e="T596" id="Seg_8031" s="T595">лошадь.[NOM.SG]</ta>
            <ta e="T597" id="Seg_8032" s="T596">красивый.[NOM.SG]</ta>
            <ta e="T598" id="Seg_8033" s="T597">а</ta>
            <ta e="T599" id="Seg_8034" s="T598">этот.[NOM.SG]</ta>
            <ta e="T600" id="Seg_8035" s="T599">сказать-IPFVZ.[3SG]</ta>
            <ta e="T601" id="Seg_8036" s="T600">спрятаться-RES-IMP.2SG.O</ta>
            <ta e="T602" id="Seg_8037" s="T601">лошадь-ACC</ta>
            <ta e="T603" id="Seg_8038" s="T602">и</ta>
            <ta e="T604" id="Seg_8039" s="T603">девушка-NOM/GEN/ACC.1SG</ta>
            <ta e="T605" id="Seg_8040" s="T604">тогда</ta>
            <ta e="T606" id="Seg_8041" s="T605">этот.[NOM.SG]</ta>
            <ta e="T607" id="Seg_8042" s="T606">делать-MOM-PST.[3SG]</ta>
            <ta e="T608" id="Seg_8043" s="T607">лошадь-INS</ta>
            <ta e="T609" id="Seg_8044" s="T608">этот.[NOM.SG]</ta>
            <ta e="T610" id="Seg_8045" s="T609">этот-ACC</ta>
            <ta e="T611" id="Seg_8046" s="T610">нести-PST.[3SG]</ta>
            <ta e="T612" id="Seg_8047" s="T611">этот.[NOM.SG]</ta>
            <ta e="T613" id="Seg_8048" s="T612">взять-PST.[3SG]</ta>
            <ta e="T614" id="Seg_8049" s="T613">птица.[NOM.SG]</ta>
            <ta e="T615" id="Seg_8050" s="T614">%%</ta>
            <ta e="T616" id="Seg_8051" s="T615">как</ta>
            <ta e="T617" id="Seg_8052" s="T616">огонь.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8053" s="T617">и</ta>
            <ta e="T619" id="Seg_8054" s="T618">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T620" id="Seg_8055" s="T619">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T621" id="Seg_8056" s="T620">сесть-PST.[3SG]</ta>
            <ta e="T622" id="Seg_8057" s="T621">и</ta>
            <ta e="T623" id="Seg_8058" s="T622">дочь-ACC.3SG</ta>
            <ta e="T624" id="Seg_8059" s="T623">сесть-PST.[3SG]</ta>
            <ta e="T625" id="Seg_8060" s="T624">лошадь-LAT</ta>
            <ta e="T626" id="Seg_8061" s="T625">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T627" id="Seg_8062" s="T626">сесть-PST.[3SG]</ta>
            <ta e="T628" id="Seg_8063" s="T627">пойти-PST-3PL</ta>
            <ta e="T629" id="Seg_8064" s="T628">тогда</ta>
            <ta e="T630" id="Seg_8065" s="T629">этот.[NOM.SG]</ta>
            <ta e="T631" id="Seg_8066" s="T630">вождь.[NOM.SG]</ta>
            <ta e="T632" id="Seg_8067" s="T631">PTCL</ta>
            <ta e="T633" id="Seg_8068" s="T632">принести-IMP.2PL</ta>
            <ta e="T634" id="Seg_8069" s="T633">этот.[NOM.SG]</ta>
            <ta e="T635" id="Seg_8070" s="T634">лошадь.[NOM.SG]</ta>
            <ta e="T636" id="Seg_8071" s="T635">этот.[NOM.SG]</ta>
            <ta e="T637" id="Seg_8072" s="T636">лошадь.[NOM.SG]</ta>
            <ta e="T638" id="Seg_8073" s="T637">прийти-PST.[3SG]</ta>
            <ta e="T639" id="Seg_8074" s="T638">волк-INS</ta>
            <ta e="T640" id="Seg_8075" s="T639">делать-MOM-PST.[3SG]</ta>
            <ta e="T641" id="Seg_8076" s="T640">этот.[NOM.SG]</ta>
            <ta e="T642" id="Seg_8077" s="T641">пугаться-MOM-PST.[3SG]</ta>
            <ta e="T643" id="Seg_8078" s="T642">и</ta>
            <ta e="T644" id="Seg_8079" s="T643">упасть-MOM-PST.[3SG]</ta>
            <ta e="T645" id="Seg_8080" s="T644">а</ta>
            <ta e="T646" id="Seg_8081" s="T645">этот.[NOM.SG]</ta>
            <ta e="T647" id="Seg_8082" s="T646">бежать-MOM-PST.[3SG]</ta>
            <ta e="T648" id="Seg_8083" s="T647">догонять-DUR.[3SG]</ta>
            <ta e="T649" id="Seg_8084" s="T648">Ванюшка-LAT</ta>
            <ta e="T650" id="Seg_8085" s="T649">сказать-IPFVZ.[3SG]</ta>
            <ta e="T651" id="Seg_8086" s="T650">ну</ta>
            <ta e="T652" id="Seg_8087" s="T651">сейчас</ta>
            <ta e="T653" id="Seg_8088" s="T652">я.LAT</ta>
            <ta e="T654" id="Seg_8089" s="T653">пойти-INF.LAT</ta>
            <ta e="T655" id="Seg_8090" s="T654">ты.NOM-INS</ta>
            <ta e="T657" id="Seg_8091" s="T656">остаться-FUT-1SG</ta>
            <ta e="T658" id="Seg_8092" s="T657">здесь</ta>
            <ta e="T659" id="Seg_8093" s="T658">этот.[NOM.SG]</ta>
            <ta e="T660" id="Seg_8094" s="T659">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T661" id="Seg_8095" s="T660">этот-LAT</ta>
            <ta e="T662" id="Seg_8096" s="T661">три-COLL</ta>
            <ta e="T663" id="Seg_8097" s="T662">место-LAT</ta>
            <ta e="T664" id="Seg_8098" s="T663">упасть-MOM-PST.[3SG]</ta>
            <ta e="T665" id="Seg_8099" s="T664">хватит</ta>
            <ta e="T666" id="Seg_8100" s="T665">тогда</ta>
            <ta e="T668" id="Seg_8101" s="T667">этот-PL</ta>
            <ta e="T669" id="Seg_8102" s="T668">прийти-PST-3PL</ta>
            <ta e="T670" id="Seg_8103" s="T669">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T672" id="Seg_8104" s="T671">место-LAT</ta>
            <ta e="T673" id="Seg_8105" s="T672">сказать-IPFVZ.[3SG]</ta>
            <ta e="T674" id="Seg_8106" s="T673">HORT</ta>
            <ta e="T675" id="Seg_8107" s="T674">есть-FUT-1DU</ta>
            <ta e="T676" id="Seg_8108" s="T675">сесть-%%-PST-3PL</ta>
            <ta e="T677" id="Seg_8109" s="T676">есть-PST-3PL</ta>
            <ta e="T678" id="Seg_8110" s="T677">и</ta>
            <ta e="T680" id="Seg_8111" s="T679">ложиться-PST-3PL</ta>
            <ta e="T681" id="Seg_8112" s="T680">спать-INF.LAT</ta>
            <ta e="T682" id="Seg_8113" s="T681">тогда</ta>
            <ta e="T683" id="Seg_8114" s="T682">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T684" id="Seg_8115" s="T683">прийти-PST-3PL</ta>
            <ta e="T685" id="Seg_8116" s="T684">этот-ACC</ta>
            <ta e="T686" id="Seg_8117" s="T685">убить-RES-PST-3PL</ta>
            <ta e="T687" id="Seg_8118" s="T686">а</ta>
            <ta e="T688" id="Seg_8119" s="T687">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T689" id="Seg_8120" s="T688">взять-PST-3PL</ta>
            <ta e="T690" id="Seg_8121" s="T689">девушка.[NOM.SG]</ta>
            <ta e="T691" id="Seg_8122" s="T690">и</ta>
            <ta e="T692" id="Seg_8123" s="T691">птица.[NOM.SG]</ta>
            <ta e="T693" id="Seg_8124" s="T692">и</ta>
            <ta e="T694" id="Seg_8125" s="T693">лошадь.[NOM.SG]</ta>
            <ta e="T695" id="Seg_8126" s="T694">пойти-PST.[3SG]</ta>
            <ta e="T696" id="Seg_8127" s="T695">идти-DUR-3PL</ta>
            <ta e="T697" id="Seg_8128" s="T696">тогда</ta>
            <ta e="T698" id="Seg_8129" s="T697">волк.[NOM.SG]</ta>
            <ta e="T699" id="Seg_8130" s="T698">прийти-PST.[3SG]</ta>
            <ta e="T701" id="Seg_8131" s="T700">видеть-PRS-3SG.O</ta>
            <ta e="T702" id="Seg_8132" s="T701">этот.[NOM.SG]</ta>
            <ta e="T703" id="Seg_8133" s="T702">лежать-DUR.[3SG]</ta>
            <ta e="T704" id="Seg_8134" s="T703">ловить-PST.[3SG]</ta>
            <ta e="T705" id="Seg_8135" s="T704">птица.[NOM.SG]</ta>
            <ta e="T707" id="Seg_8136" s="T706">маленький.[NOM.SG]</ta>
            <ta e="T708" id="Seg_8137" s="T707">птица.[NOM.SG]</ta>
            <ta e="T709" id="Seg_8138" s="T708">PTCL</ta>
            <ta e="T710" id="Seg_8139" s="T709">принести-IMP.2SG</ta>
            <ta e="T711" id="Seg_8140" s="T710">умереть-PST.[3SG]</ta>
            <ta e="T712" id="Seg_8141" s="T711">вода.[NOM.SG]</ta>
            <ta e="T713" id="Seg_8142" s="T712">и</ta>
            <ta e="T714" id="Seg_8143" s="T713">живой.[NOM.SG]</ta>
            <ta e="T715" id="Seg_8144" s="T714">вода.[NOM.SG]</ta>
            <ta e="T716" id="Seg_8145" s="T715">этот.[NOM.SG]</ta>
            <ta e="T717" id="Seg_8146" s="T716">птица.[NOM.SG]</ta>
            <ta e="T718" id="Seg_8147" s="T717">лететь-MOM-PST.[3SG]</ta>
            <ta e="T719" id="Seg_8148" s="T718">принести-PST.[3SG]</ta>
            <ta e="T720" id="Seg_8149" s="T719">этот.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8150" s="T720">этот-ACC</ta>
            <ta e="T722" id="Seg_8151" s="T721">мыть-PST.[3SG]</ta>
            <ta e="T724" id="Seg_8152" s="T723">мертвый</ta>
            <ta e="T725" id="Seg_8153" s="T724">вода-INS</ta>
            <ta e="T726" id="Seg_8154" s="T725">и</ta>
            <ta e="T727" id="Seg_8155" s="T726">тогда</ta>
            <ta e="T728" id="Seg_8156" s="T727">живой</ta>
            <ta e="T729" id="Seg_8157" s="T728">вода-INS</ta>
            <ta e="T730" id="Seg_8158" s="T729">этот.[NOM.SG]</ta>
            <ta e="T731" id="Seg_8159" s="T730">прыгнуть-DUR.[3SG]</ta>
            <ta e="T732" id="Seg_8160" s="T731">долго</ta>
            <ta e="T733" id="Seg_8161" s="T732">я.NOM</ta>
            <ta e="T734" id="Seg_8162" s="T733">спать-PST-1SG</ta>
            <ta e="T735" id="Seg_8163" s="T734">долго</ta>
            <ta e="T736" id="Seg_8164" s="T735">спать-PST-2SG</ta>
            <ta e="T738" id="Seg_8165" s="T737">кабы</ta>
            <ta e="T739" id="Seg_8166" s="T738">NEG</ta>
            <ta e="T740" id="Seg_8167" s="T739">я.NOM</ta>
            <ta e="T741" id="Seg_8168" s="T740">так</ta>
            <ta e="T742" id="Seg_8169" s="T741">ты.NOM</ta>
            <ta e="T743" id="Seg_8170" s="T742">всегда</ta>
            <ta e="T744" id="Seg_8171" s="T743">IRREAL</ta>
            <ta e="T745" id="Seg_8172" s="T744">спать-PST-2SG</ta>
            <ta e="T746" id="Seg_8173" s="T745">ты.ACC</ta>
            <ta e="T747" id="Seg_8174" s="T746">убить-MOM-PST-3PL</ta>
            <ta e="T748" id="Seg_8175" s="T747">ты.NOM</ta>
            <ta e="T750" id="Seg_8176" s="T749">брат-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T751" id="Seg_8177" s="T750">и</ta>
            <ta e="T752" id="Seg_8178" s="T751">весь</ta>
            <ta e="T753" id="Seg_8179" s="T752">взять-MOM-PST-3PL</ta>
            <ta e="T754" id="Seg_8180" s="T753">дочь-ACC</ta>
            <ta e="T755" id="Seg_8181" s="T754">и</ta>
            <ta e="T756" id="Seg_8182" s="T755">птица-ACC</ta>
            <ta e="T757" id="Seg_8183" s="T756">и</ta>
            <ta e="T758" id="Seg_8184" s="T757">лошадь-ACC</ta>
            <ta e="T759" id="Seg_8185" s="T758">тогда</ta>
            <ta e="T760" id="Seg_8186" s="T759">сесть-IMP.2SG</ta>
            <ta e="T761" id="Seg_8187" s="T760">я.LAT</ta>
            <ta e="T762" id="Seg_8188" s="T761">сажать-PST.[3SG]</ta>
            <ta e="T763" id="Seg_8189" s="T762">этот-PL</ta>
            <ta e="T764" id="Seg_8190" s="T763">догонять-PST-3PL</ta>
            <ta e="T765" id="Seg_8191" s="T764">этот-ACC</ta>
            <ta e="T766" id="Seg_8192" s="T765">этот.[NOM.SG]</ta>
            <ta e="T767" id="Seg_8193" s="T766">волк.[NOM.SG]</ta>
            <ta e="T768" id="Seg_8194" s="T767">брат-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T769" id="Seg_8195" s="T768">от-рвать-PST</ta>
            <ta e="T770" id="Seg_8196" s="T769">и</ta>
            <ta e="T771" id="Seg_8197" s="T770">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T772" id="Seg_8198" s="T771">взять-PST.[3SG]</ta>
            <ta e="T773" id="Seg_8199" s="T772">девушка.[NOM.SG]</ta>
            <ta e="T774" id="Seg_8200" s="T773">и</ta>
            <ta e="T775" id="Seg_8201" s="T774">весь</ta>
            <ta e="T776" id="Seg_8202" s="T775">что.[NOM.SG]</ta>
            <ta e="T777" id="Seg_8203" s="T776">там</ta>
            <ta e="T778" id="Seg_8204" s="T777">быть-PST.[3SG]</ta>
            <ta e="T779" id="Seg_8205" s="T778">тогда</ta>
            <ta e="T780" id="Seg_8206" s="T779">этот.[NOM.SG]</ta>
            <ta e="T781" id="Seg_8207" s="T780">Иванушка.[NOM.SG]</ta>
            <ta e="T782" id="Seg_8208" s="T781">этот-LAT</ta>
            <ta e="T783" id="Seg_8209" s="T782">упасть-MOM-PST.[3SG]</ta>
            <ta e="T784" id="Seg_8210" s="T783">нога-LAT/LOC.3SG</ta>
            <ta e="T785" id="Seg_8211" s="T784">и</ta>
            <ta e="T786" id="Seg_8212" s="T785">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T787" id="Seg_8213" s="T786">чум-LAT/LOC.3SG</ta>
            <ta e="T788" id="Seg_8214" s="T787">прийти-PST.[3SG]</ta>
            <ta e="T789" id="Seg_8215" s="T788">отец-LAT/LOC.3SG</ta>
            <ta e="T790" id="Seg_8216" s="T789">весь</ta>
            <ta e="T791" id="Seg_8217" s="T790">что.[NOM.SG]</ta>
            <ta e="T792" id="Seg_8218" s="T791">сказать-PST.[3SG]</ta>
            <ta e="T793" id="Seg_8219" s="T792">тогда</ta>
            <ta e="T794" id="Seg_8220" s="T793">отец-NOM/GEN.3SG</ta>
            <ta e="T795" id="Seg_8221" s="T794">плакать-PST.[3SG]</ta>
            <ta e="T796" id="Seg_8222" s="T795">жалеть-PST.[3SG]</ta>
            <ta e="T797" id="Seg_8223" s="T796">тогда</ta>
            <ta e="T798" id="Seg_8224" s="T797">этот.[NOM.SG]</ta>
            <ta e="T799" id="Seg_8225" s="T798">дочь-ACC</ta>
            <ta e="T800" id="Seg_8226" s="T799">взять-PST.[3SG]</ta>
            <ta e="T801" id="Seg_8227" s="T800">жениться-PST-3PL</ta>
            <ta e="T802" id="Seg_8228" s="T801">водка.[NOM.SG]</ta>
            <ta e="T803" id="Seg_8229" s="T802">пить-PST-3PL</ta>
            <ta e="T804" id="Seg_8230" s="T803">и</ta>
            <ta e="T805" id="Seg_8231" s="T804">скоро</ta>
            <ta e="T806" id="Seg_8232" s="T805">всегда</ta>
            <ta e="T807" id="Seg_8233" s="T806">жить-DUR-3PL</ta>
            <ta e="T808" id="Seg_8234" s="T807">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_8235" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_8236" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_8237" s="T2">v-v&gt;v-v:tense</ta>
            <ta e="T4" id="Seg_8238" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_8239" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_8240" s="T5">num-num&gt;num</ta>
            <ta e="T7" id="Seg_8241" s="T6">n-n:num-n:case.poss</ta>
            <ta e="T8" id="Seg_8242" s="T7">dempro-n:case</ta>
            <ta e="T9" id="Seg_8243" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_8244" s="T9">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_8245" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_8246" s="T12">adj-n:num</ta>
            <ta e="T14" id="Seg_8247" s="T13">que-n:case=ptcl</ta>
            <ta e="T15" id="Seg_8248" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_8249" s="T15">v-v&gt;v-v:pn</ta>
            <ta e="T17" id="Seg_8250" s="T16">dempro-n:case</ta>
            <ta e="T18" id="Seg_8251" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_8252" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_8253" s="T19">que-n:case=ptcl</ta>
            <ta e="T21" id="Seg_8254" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_8255" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_8256" s="T22">n-n:ins-n:case</ta>
            <ta e="T24" id="Seg_8257" s="T23">v-v&gt;v-v:n.fin</ta>
            <ta e="T25" id="Seg_8258" s="T24">dempro-n:num</ta>
            <ta e="T26" id="Seg_8259" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_8260" s="T26">v</ta>
            <ta e="T28" id="Seg_8261" s="T27">adv</ta>
            <ta e="T29" id="Seg_8262" s="T28">num-n:case</ta>
            <ta e="T30" id="Seg_8263" s="T29">n-n:case</ta>
            <ta e="T32" id="Seg_8264" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_8265" s="T32">pers</ta>
            <ta e="T34" id="Seg_8266" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_8267" s="T34">dempro-n:case</ta>
            <ta e="T36" id="Seg_8268" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_8269" s="T36">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_8270" s="T37">que-n:case=ptcl</ta>
            <ta e="T39" id="Seg_8271" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_8272" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_8273" s="T40">adv</ta>
            <ta e="T42" id="Seg_8274" s="T41">adj-n:case</ta>
            <ta e="T43" id="Seg_8275" s="T42">n-n:case</ta>
            <ta e="T45" id="Seg_8276" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_8277" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_8278" s="T46">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_8279" s="T47">que-n:case=ptcl</ta>
            <ta e="T49" id="Seg_8280" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_8281" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_8282" s="T50">adv</ta>
            <ta e="T52" id="Seg_8283" s="T51">propr-n:case</ta>
            <ta e="T53" id="Seg_8284" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_8285" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_8286" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_8287" s="T55">v-v:n.fin</ta>
            <ta e="T57" id="Seg_8288" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8289" s="T57">dempro-n:case</ta>
            <ta e="T59" id="Seg_8290" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_8291" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_8292" s="T60">n-n:case.poss</ta>
            <ta e="T62" id="Seg_8293" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_8294" s="T62">conj</ta>
            <ta e="T64" id="Seg_8295" s="T63">adv</ta>
            <ta e="T65" id="Seg_8296" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_8297" s="T65">adv</ta>
            <ta e="T67" id="Seg_8298" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_8299" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_8300" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_8301" s="T69">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_8302" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_8303" s="T71">dempro-n:case</ta>
            <ta e="T74" id="Seg_8304" s="T73">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_8305" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_8306" s="T76">v-v&gt;v-v:pn</ta>
            <ta e="T78" id="Seg_8307" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_8308" s="T78">adj-n:case</ta>
            <ta e="T80" id="Seg_8309" s="T79">dempro-n:case</ta>
            <ta e="T81" id="Seg_8310" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_8311" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_8312" s="T82">num-n:case</ta>
            <ta e="T84" id="Seg_8313" s="T83">n-n:case</ta>
            <ta e="T86" id="Seg_8314" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_8315" s="T86">adv</ta>
            <ta e="T88" id="Seg_8316" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_8317" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_8318" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_8319" s="T90">que-n:case</ta>
            <ta e="T92" id="Seg_8320" s="T91">n-n:case.poss</ta>
            <ta e="T94" id="Seg_8321" s="T93">pers</ta>
            <ta e="T95" id="Seg_8322" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_8323" s="T95">que</ta>
            <ta e="T97" id="Seg_8324" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_8325" s="T97">dempro-n:case</ta>
            <ta e="T99" id="Seg_8326" s="T98">pers</ta>
            <ta e="T100" id="Seg_8327" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_8328" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_8329" s="T101">dempro-n:case</ta>
            <ta e="T106" id="Seg_8330" s="T105">v-v&gt;v-v:pn</ta>
            <ta e="T107" id="Seg_8331" s="T106">conj</ta>
            <ta e="T108" id="Seg_8332" s="T107">v-v&gt;v-v:pn</ta>
            <ta e="T109" id="Seg_8333" s="T108">adv</ta>
            <ta e="T110" id="Seg_8334" s="T109">n-n:case.poss</ta>
            <ta e="T112" id="Seg_8335" s="T111">v-v:n.fin</ta>
            <ta e="T113" id="Seg_8336" s="T112">adv</ta>
            <ta e="T114" id="Seg_8337" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_8338" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_8339" s="T115">v-v:mood.pn</ta>
            <ta e="T117" id="Seg_8340" s="T116">n-n:num</ta>
            <ta e="T118" id="Seg_8341" s="T117">que=ptcl</ta>
            <ta e="T119" id="Seg_8342" s="T118">quant</ta>
            <ta e="T120" id="Seg_8343" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_8344" s="T120">v-v:mood.pn</ta>
            <ta e="T122" id="Seg_8345" s="T121">n-n:case</ta>
            <ta e="T126" id="Seg_8346" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_8347" s="T126">que</ta>
            <ta e="T130" id="Seg_8348" s="T129">que=ptcl</ta>
            <ta e="T131" id="Seg_8349" s="T130">dempro-n:case</ta>
            <ta e="T132" id="Seg_8350" s="T131">n-n:case</ta>
            <ta e="T136" id="Seg_8351" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_8352" s="T136">que</ta>
            <ta e="T138" id="Seg_8353" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_8354" s="T138">dempro-n:num</ta>
            <ta e="T141" id="Seg_8355" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_8356" s="T141">num-n:case</ta>
            <ta e="T143" id="Seg_8357" s="T142">adv</ta>
            <ta e="T144" id="Seg_8358" s="T143">adj-n:case</ta>
            <ta e="T145" id="Seg_8359" s="T144">adv</ta>
            <ta e="T146" id="Seg_8360" s="T145">propr-n:case</ta>
            <ta e="T147" id="Seg_8361" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_8362" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_8363" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_8364" s="T149">adv</ta>
            <ta e="T151" id="Seg_8365" s="T150">dempro-n:case</ta>
            <ta e="T152" id="Seg_8366" s="T151">propr-n:case</ta>
            <ta e="T153" id="Seg_8367" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_8368" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_8369" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_8370" s="T155">n-n:case.poss</ta>
            <ta e="T157" id="Seg_8371" s="T156">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_8372" s="T157">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_8373" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_8374" s="T160">v-v:n.fin</ta>
            <ta e="T162" id="Seg_8375" s="T161">n-n:case.poss</ta>
            <ta e="T163" id="Seg_8376" s="T162">v-v&gt;v-v:pn</ta>
            <ta e="T164" id="Seg_8377" s="T163">n-n:case.poss</ta>
            <ta e="T165" id="Seg_8378" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_8379" s="T165">adv</ta>
            <ta e="T167" id="Seg_8380" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_8381" s="T167">adv</ta>
            <ta e="T169" id="Seg_8382" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_8383" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_8384" s="T170">n-n:case.poss</ta>
            <ta e="T172" id="Seg_8385" s="T171">v</ta>
            <ta e="T173" id="Seg_8386" s="T172">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_8387" s="T173">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T175" id="Seg_8388" s="T174">adv</ta>
            <ta e="T176" id="Seg_8389" s="T175">n-n:case.poss</ta>
            <ta e="T177" id="Seg_8390" s="T176">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_8391" s="T177">v-v&gt;v-v:pn</ta>
            <ta e="T179" id="Seg_8392" s="T178">conj</ta>
            <ta e="T180" id="Seg_8393" s="T179">adv</ta>
            <ta e="T181" id="Seg_8394" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_8395" s="T181">v-v:n.fin</ta>
            <ta e="T183" id="Seg_8396" s="T182">adv</ta>
            <ta e="T184" id="Seg_8397" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_8398" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_8399" s="T185">adv</ta>
            <ta e="T187" id="Seg_8400" s="T186">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_8401" s="T187">conj</ta>
            <ta e="T189" id="Seg_8402" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_8403" s="T189">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_8404" s="T190">v-v&gt;v-v:pn</ta>
            <ta e="T192" id="Seg_8405" s="T191">adv</ta>
            <ta e="T193" id="Seg_8406" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_8407" s="T193">v-v&gt;v-v:pn</ta>
            <ta e="T195" id="Seg_8408" s="T194">que-n:case</ta>
            <ta e="T196" id="Seg_8409" s="T195">v-v&gt;v-v:pn</ta>
            <ta e="T197" id="Seg_8410" s="T196">n-n:case.poss</ta>
            <ta e="T198" id="Seg_8411" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_8412" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_8413" s="T199">pers</ta>
            <ta e="T201" id="Seg_8414" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_8415" s="T201">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_8416" s="T202">conj</ta>
            <ta e="T204" id="Seg_8417" s="T203">dempro-n:case</ta>
            <ta e="T205" id="Seg_8418" s="T204">pers</ta>
            <ta e="T206" id="Seg_8419" s="T205">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_8420" s="T206">ptcl</ta>
            <ta e="T209" id="Seg_8421" s="T208">conj</ta>
            <ta e="T210" id="Seg_8422" s="T209">que</ta>
            <ta e="T211" id="Seg_8423" s="T210">pers</ta>
            <ta e="T212" id="Seg_8424" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_8425" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_8426" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_8427" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_8428" s="T215">n-n:case</ta>
            <ta e="T218" id="Seg_8429" s="T217">v-v&gt;v-v:n.fin</ta>
            <ta e="T219" id="Seg_8430" s="T218">n-n:case.poss</ta>
            <ta e="T220" id="Seg_8431" s="T219">v-v&gt;v-v:pn</ta>
            <ta e="T221" id="Seg_8432" s="T220">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T222" id="Seg_8433" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_8434" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_8435" s="T223">adj-n:case</ta>
            <ta e="T225" id="Seg_8436" s="T224">adv</ta>
            <ta e="T226" id="Seg_8437" s="T225">dempro-n:case</ta>
            <ta e="T227" id="Seg_8438" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_8439" s="T227">pers</ta>
            <ta e="T229" id="Seg_8440" s="T228">refl-n:case.poss</ta>
            <ta e="T230" id="Seg_8441" s="T229">n-n:case.poss-n:case</ta>
            <ta e="T231" id="Seg_8442" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_8443" s="T231">num-n:case</ta>
            <ta e="T233" id="Seg_8444" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_8445" s="T233">adv</ta>
            <ta e="T235" id="Seg_8446" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_8447" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_8448" s="T236">v-v:mood.pn</ta>
            <ta e="T239" id="Seg_8449" s="T238">pers</ta>
            <ta e="T240" id="Seg_8450" s="T239">pers</ta>
            <ta e="T241" id="Seg_8451" s="T240">pers</ta>
            <ta e="T242" id="Seg_8452" s="T241">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_8453" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_8454" s="T243">adv</ta>
            <ta e="T245" id="Seg_8455" s="T244">dempro-n:case</ta>
            <ta e="T246" id="Seg_8456" s="T245">v-v:tense-v:pn</ta>
            <ta e="T247" id="Seg_8457" s="T246">v-v:ins-v:mood.pn</ta>
            <ta e="T248" id="Seg_8458" s="T247">adv</ta>
            <ta e="T249" id="Seg_8459" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_8460" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_8461" s="T250">v-v&gt;v-v:pn</ta>
            <ta e="T252" id="Seg_8462" s="T251">v-v:mood.pn</ta>
            <ta e="T253" id="Seg_8463" s="T252">dempro-n:case</ta>
            <ta e="T254" id="Seg_8464" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_8465" s="T254">conj</ta>
            <ta e="T256" id="Seg_8466" s="T255">n-n:case.poss</ta>
            <ta e="T257" id="Seg_8467" s="T256">aux-v:mood.pn</ta>
            <ta e="T258" id="Seg_8468" s="T257">v-v:mood.pn</ta>
            <ta e="T260" id="Seg_8469" s="T258">ptcl</ta>
            <ta e="T261" id="Seg_8470" s="T260">adv</ta>
            <ta e="T262" id="Seg_8471" s="T261">dempro-n:case</ta>
            <ta e="T263" id="Seg_8472" s="T262">v-v:tense-v:pn</ta>
            <ta e="T264" id="Seg_8473" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_8474" s="T264">n-n:case</ta>
            <ta e="T266" id="Seg_8475" s="T265">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_8476" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_8477" s="T267">adj-n:case</ta>
            <ta e="T269" id="Seg_8478" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_8479" s="T269">v-v:n.fin</ta>
            <ta e="T271" id="Seg_8480" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_8481" s="T271">dempro-n:case</ta>
            <ta e="T273" id="Seg_8482" s="T272">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_8483" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_8484" s="T274">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_8485" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_8486" s="T276">dempro-n:case</ta>
            <ta e="T278" id="Seg_8487" s="T277">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_8488" s="T278">conj</ta>
            <ta e="T282" id="Seg_8489" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_8490" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_8491" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_8492" s="T284">adv</ta>
            <ta e="T286" id="Seg_8493" s="T285">dempro-n:case</ta>
            <ta e="T287" id="Seg_8494" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_8495" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_8496" s="T288">pers</ta>
            <ta e="T290" id="Seg_8497" s="T289">que-n:case</ta>
            <ta e="T291" id="Seg_8498" s="T290">que-n:case</ta>
            <ta e="T292" id="Seg_8499" s="T291">adv</ta>
            <ta e="T293" id="Seg_8500" s="T292">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_8501" s="T293">v-v:n.fin</ta>
            <ta e="T295" id="Seg_8502" s="T294">que-n:case</ta>
            <ta e="T296" id="Seg_8503" s="T295">pers</ta>
            <ta e="T297" id="Seg_8504" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_8505" s="T297">pers</ta>
            <ta e="T299" id="Seg_8506" s="T298">propr-n:case</ta>
            <ta e="T300" id="Seg_8507" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_8508" s="T300">pers</ta>
            <ta e="T302" id="Seg_8509" s="T301">v-v:n.fin</ta>
            <ta e="T304" id="Seg_8510" s="T303">adv</ta>
            <ta e="T305" id="Seg_8511" s="T304">dempro-n:case</ta>
            <ta e="T306" id="Seg_8512" s="T305">v-v&gt;v-v:pn</ta>
            <ta e="T307" id="Seg_8513" s="T306">conj</ta>
            <ta e="T308" id="Seg_8514" s="T307">dempro-n:case</ta>
            <ta e="T309" id="Seg_8515" s="T308">pers</ta>
            <ta e="T310" id="Seg_8516" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_8517" s="T310">conj</ta>
            <ta e="T313" id="Seg_8518" s="T312">v-v:tense-v:pn</ta>
            <ta e="T314" id="Seg_8519" s="T313">conj</ta>
            <ta e="T315" id="Seg_8520" s="T314">pers</ta>
            <ta e="T316" id="Seg_8521" s="T315">v-v:tense-v:pn</ta>
            <ta e="T317" id="Seg_8522" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_8523" s="T317">conj</ta>
            <ta e="T319" id="Seg_8524" s="T318">pers</ta>
            <ta e="T320" id="Seg_8525" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_8526" s="T320">pers</ta>
            <ta e="T322" id="Seg_8527" s="T321">v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_8528" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_8529" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_8530" s="T324">v-v:n.fin</ta>
            <ta e="T326" id="Seg_8531" s="T325">v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_8532" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_8533" s="T327">pers</ta>
            <ta e="T329" id="Seg_8534" s="T328">pers</ta>
            <ta e="T330" id="Seg_8535" s="T329">que-n:case=ptcl</ta>
            <ta e="T331" id="Seg_8536" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_8537" s="T331">v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_8538" s="T332">v-v:ins-v:mood.pn</ta>
            <ta e="T334" id="Seg_8539" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_8540" s="T334">v-v:mood.pn</ta>
            <ta e="T337" id="Seg_8541" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_8542" s="T337">adv</ta>
            <ta e="T339" id="Seg_8543" s="T338">dempro-n:case</ta>
            <ta e="T340" id="Seg_8544" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_8545" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_8546" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_8547" s="T342">v-v&gt;v-v:pn</ta>
            <ta e="T344" id="Seg_8548" s="T343">pers</ta>
            <ta e="T345" id="Seg_8549" s="T344">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_8550" s="T345">pers</ta>
            <ta e="T347" id="Seg_8551" s="T346">aux-v:mood.pn</ta>
            <ta e="T348" id="Seg_8552" s="T347">v-v:ins-v:mood.pn</ta>
            <ta e="T349" id="Seg_8553" s="T348">conj</ta>
            <ta e="T350" id="Seg_8554" s="T349">pers</ta>
            <ta e="T351" id="Seg_8555" s="T350">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_8556" s="T351">conj</ta>
            <ta e="T353" id="Seg_8557" s="T352">adv</ta>
            <ta e="T355" id="Seg_8558" s="T354">v-v:mood.pn</ta>
            <ta e="T356" id="Seg_8559" s="T355">pers</ta>
            <ta e="T357" id="Seg_8560" s="T356">v-v:mood-v:pn</ta>
            <ta e="T358" id="Seg_8561" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_8562" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_8563" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_8564" s="T360">adv</ta>
            <ta e="T362" id="Seg_8565" s="T361">adv</ta>
            <ta e="T363" id="Seg_8566" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_8567" s="T363">v-v:ins-v:mood.pn</ta>
            <ta e="T365" id="Seg_8568" s="T364">conj</ta>
            <ta e="T366" id="Seg_8569" s="T365">v-v:mood.pn</ta>
            <ta e="T367" id="Seg_8570" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_8571" s="T367">conj</ta>
            <ta e="T369" id="Seg_8572" s="T368">n-n:case.poss</ta>
            <ta e="T370" id="Seg_8573" s="T369">ptcl</ta>
            <ta e="T372" id="Seg_8574" s="T371">aux-v:mood.pn</ta>
            <ta e="T374" id="Seg_8575" s="T373">aux-v:mood.pn</ta>
            <ta e="T376" id="Seg_8576" s="T375">aux-v:mood.pn</ta>
            <ta e="T377" id="Seg_8577" s="T376">v-v:mood.pn</ta>
            <ta e="T378" id="Seg_8578" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_8579" s="T378">adv</ta>
            <ta e="T380" id="Seg_8580" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_8581" s="T380">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_8582" s="T381">dempro-n:case</ta>
            <ta e="T383" id="Seg_8583" s="T382">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_8584" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_8585" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_8586" s="T385">conj</ta>
            <ta e="T387" id="Seg_8587" s="T386">n-n:case.poss</ta>
            <ta e="T388" id="Seg_8588" s="T387">adv</ta>
            <ta e="T389" id="Seg_8589" s="T388">adj-n:case</ta>
            <ta e="T390" id="Seg_8590" s="T389">que-n:case</ta>
            <ta e="T391" id="Seg_8591" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_8592" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_8593" s="T392">v-v:n.fin</ta>
            <ta e="T394" id="Seg_8594" s="T393">adv</ta>
            <ta e="T395" id="Seg_8595" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_8596" s="T395">dempro-n:case</ta>
            <ta e="T397" id="Seg_8597" s="T396">dempro-n:case</ta>
            <ta e="T398" id="Seg_8598" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_8599" s="T398">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_8600" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_8601" s="T400">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T402" id="Seg_8602" s="T401">dempro-n:case</ta>
            <ta e="T403" id="Seg_8603" s="T402">v-v:tense-v:pn</ta>
            <ta e="T405" id="Seg_8604" s="T404">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T406" id="Seg_8605" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_8606" s="T406">que-n:case</ta>
            <ta e="T408" id="Seg_8607" s="T407">v-v:tense-v:pn</ta>
            <ta e="T409" id="Seg_8608" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_8609" s="T409">v-v:n.fin</ta>
            <ta e="T411" id="Seg_8610" s="T410">que-n:case</ta>
            <ta e="T412" id="Seg_8611" s="T411">pers</ta>
            <ta e="T413" id="Seg_8612" s="T412">pers</ta>
            <ta e="T414" id="Seg_8613" s="T413">pers</ta>
            <ta e="T415" id="Seg_8614" s="T414">propr-n:case</ta>
            <ta e="T416" id="Seg_8615" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_8616" s="T416">v-v:n.fin</ta>
            <ta e="T419" id="Seg_8617" s="T418">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_8618" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_8619" s="T420">v-v:ins-v:mood.pn</ta>
            <ta e="T422" id="Seg_8620" s="T421">pers</ta>
            <ta e="T423" id="Seg_8621" s="T422">pers</ta>
            <ta e="T424" id="Seg_8622" s="T423">que-n:case=ptcl</ta>
            <ta e="T425" id="Seg_8623" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_8624" s="T425">v-v:tense-v:pn</ta>
            <ta e="T427" id="Seg_8625" s="T426">adv</ta>
            <ta e="T428" id="Seg_8626" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_8627" s="T428">n-n:case</ta>
            <ta e="T432" id="Seg_8628" s="T431">v-v:mood.pn</ta>
            <ta e="T433" id="Seg_8629" s="T432">pers</ta>
            <ta e="T434" id="Seg_8630" s="T433">dempro-n:case</ta>
            <ta e="T435" id="Seg_8631" s="T434">adv</ta>
            <ta e="T436" id="Seg_8632" s="T435">dempro-n:case</ta>
            <ta e="T438" id="Seg_8633" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_8634" s="T438">conj</ta>
            <ta e="T440" id="Seg_8635" s="T439">n-n:case.poss</ta>
            <ta e="T441" id="Seg_8636" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_8637" s="T441">aux-v:mood.pn</ta>
            <ta e="T443" id="Seg_8638" s="T442">v-v:ins-v:mood.pn</ta>
            <ta e="T444" id="Seg_8639" s="T443">conj</ta>
            <ta e="T445" id="Seg_8640" s="T444">pers</ta>
            <ta e="T446" id="Seg_8641" s="T445">adv</ta>
            <ta e="T447" id="Seg_8642" s="T446">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T448" id="Seg_8643" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_8644" s="T448">que-n:case=ptcl</ta>
            <ta e="T450" id="Seg_8645" s="T449">%%</ta>
            <ta e="T451" id="Seg_8646" s="T450">dempro-n:case</ta>
            <ta e="T452" id="Seg_8647" s="T451">v-v&gt;v-v:pn</ta>
            <ta e="T453" id="Seg_8648" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_8649" s="T453">v-v:mood.pn</ta>
            <ta e="T455" id="Seg_8650" s="T454">v-v:mood-v:pn</ta>
            <ta e="T456" id="Seg_8651" s="T455">v-v:tense-v:pn</ta>
            <ta e="T457" id="Seg_8652" s="T456">adv</ta>
            <ta e="T458" id="Seg_8653" s="T457">adv</ta>
            <ta e="T459" id="Seg_8654" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_8655" s="T459">v-v&gt;v-v:pn</ta>
            <ta e="T461" id="Seg_8656" s="T460">dempro-n:case</ta>
            <ta e="T462" id="Seg_8657" s="T461">pers</ta>
            <ta e="T464" id="Seg_8658" s="T463">v-v:ins-v:mood.pn</ta>
            <ta e="T465" id="Seg_8659" s="T464">v-v:ins-v:mood.pn</ta>
            <ta e="T466" id="Seg_8660" s="T465">v-v:ins-v:mood.pn</ta>
            <ta e="T467" id="Seg_8661" s="T466">conj</ta>
            <ta e="T468" id="Seg_8662" s="T467">pers</ta>
            <ta e="T469" id="Seg_8663" s="T468">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_8664" s="T469">refl-n:case.poss</ta>
            <ta e="T471" id="Seg_8665" s="T470">adv</ta>
            <ta e="T472" id="Seg_8666" s="T471">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T473" id="Seg_8667" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_8668" s="T473">adv</ta>
            <ta e="T475" id="Seg_8669" s="T474">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T476" id="Seg_8670" s="T475">dempro-n:num</ta>
            <ta e="T477" id="Seg_8671" s="T476">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T478" id="Seg_8672" s="T477">n-n:case.poss</ta>
            <ta e="T479" id="Seg_8673" s="T478">n-n:case.poss</ta>
            <ta e="T480" id="Seg_8674" s="T479">dempro-n:case</ta>
            <ta e="T481" id="Seg_8675" s="T480">propr-n:case.poss</ta>
            <ta e="T482" id="Seg_8676" s="T481">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_8677" s="T482">dempro-n:case</ta>
            <ta e="T484" id="Seg_8678" s="T483">dempro-n:case</ta>
            <ta e="T485" id="Seg_8679" s="T484">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_8680" s="T485">conj</ta>
            <ta e="T487" id="Seg_8681" s="T486">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_8682" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_8683" s="T488">v-v&gt;v-v:pn</ta>
            <ta e="T490" id="Seg_8684" s="T489">conj</ta>
            <ta e="T491" id="Seg_8685" s="T490">v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_8686" s="T491">propr-n:case</ta>
            <ta e="T493" id="Seg_8687" s="T492">v-v:mood.pn</ta>
            <ta e="T494" id="Seg_8688" s="T493">v-v:tense-v:pn</ta>
            <ta e="T495" id="Seg_8689" s="T494">adv</ta>
            <ta e="T496" id="Seg_8690" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_8691" s="T496">adv</ta>
            <ta e="T498" id="Seg_8692" s="T497">que</ta>
            <ta e="T500" id="Seg_8693" s="T499">n-n:case</ta>
            <ta e="T501" id="Seg_8694" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_8695" s="T501">adv</ta>
            <ta e="T503" id="Seg_8696" s="T502">dempro-n:num</ta>
            <ta e="T504" id="Seg_8697" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_8698" s="T504">conj</ta>
            <ta e="T506" id="Seg_8699" s="T505">dempro-n:case</ta>
            <ta e="T507" id="Seg_8700" s="T506">v-v&gt;v-v:pn</ta>
            <ta e="T508" id="Seg_8701" s="T507">adv</ta>
            <ta e="T509" id="Seg_8702" s="T508">propr-n:case</ta>
            <ta e="T510" id="Seg_8703" s="T509">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_8704" s="T510">dempro-n:case</ta>
            <ta e="T512" id="Seg_8705" s="T511">n-n:case.poss</ta>
            <ta e="T513" id="Seg_8706" s="T512">conj</ta>
            <ta e="T514" id="Seg_8707" s="T513">n-n:case.poss</ta>
            <ta e="T515" id="Seg_8708" s="T514">v-v&gt;v-v:pn</ta>
            <ta e="T516" id="Seg_8709" s="T515">que-n:case</ta>
            <ta e="T517" id="Seg_8710" s="T516">pers</ta>
            <ta e="T518" id="Seg_8711" s="T517">adv</ta>
            <ta e="T519" id="Seg_8712" s="T518">v-v&gt;v-v:pn</ta>
            <ta e="T520" id="Seg_8713" s="T519">dempro</ta>
            <ta e="T521" id="Seg_8714" s="T520">adj-n:case</ta>
            <ta e="T522" id="Seg_8715" s="T521">adj-n:case</ta>
            <ta e="T523" id="Seg_8716" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_8717" s="T523">pers</ta>
            <ta e="T525" id="Seg_8718" s="T524">v-v:tense-v:pn</ta>
            <ta e="T527" id="Seg_8719" s="T526">v-v:n.fin</ta>
            <ta e="T528" id="Seg_8720" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_8721" s="T528">v-v:pn</ta>
            <ta e="T530" id="Seg_8722" s="T529">conj</ta>
            <ta e="T531" id="Seg_8723" s="T530">pers</ta>
            <ta e="T533" id="Seg_8724" s="T532">pers</ta>
            <ta e="T534" id="Seg_8725" s="T533">v-v&gt;v-v:pn</ta>
            <ta e="T536" id="Seg_8726" s="T535">dempro-n:case</ta>
            <ta e="T537" id="Seg_8727" s="T536">adv</ta>
            <ta e="T538" id="Seg_8728" s="T537">refl-n:case.poss</ta>
            <ta e="T539" id="Seg_8729" s="T538">n-n:case</ta>
            <ta e="T540" id="Seg_8730" s="T539">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T541" id="Seg_8731" s="T540">dempro-n:case</ta>
            <ta e="T542" id="Seg_8732" s="T541">dempro-n:case</ta>
            <ta e="T543" id="Seg_8733" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_8734" s="T543">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T545" id="Seg_8735" s="T544">conj</ta>
            <ta e="T546" id="Seg_8736" s="T545">dempro-n:case</ta>
            <ta e="T547" id="Seg_8737" s="T546">v-v:tense-v:pn</ta>
            <ta e="T548" id="Seg_8738" s="T547">dempro-n:case</ta>
            <ta e="T549" id="Seg_8739" s="T548">n-n:case.poss</ta>
            <ta e="T550" id="Seg_8740" s="T549">%%</ta>
            <ta e="T551" id="Seg_8741" s="T550">n.[n:case]</ta>
            <ta e="T552" id="Seg_8742" s="T551">v-v:tense-v:pn</ta>
            <ta e="T553" id="Seg_8743" s="T552">v-v&gt;v-v:pn</ta>
            <ta e="T554" id="Seg_8744" s="T553">n-n:case.poss</ta>
            <ta e="T555" id="Seg_8745" s="T554">v-v:tense-v:pn</ta>
            <ta e="T556" id="Seg_8746" s="T555">dempro-n:case</ta>
            <ta e="T557" id="Seg_8747" s="T556">n-n:case.poss</ta>
            <ta e="T558" id="Seg_8748" s="T557">v-v:tense-v:pn</ta>
            <ta e="T559" id="Seg_8749" s="T558">propr-n:case.poss</ta>
            <ta e="T560" id="Seg_8750" s="T559">v-v&gt;v-%%-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_8751" s="T560">refl-n:case.poss</ta>
            <ta e="T562" id="Seg_8752" s="T561">v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_8753" s="T562">v-v:tense-v:pn</ta>
            <ta e="T564" id="Seg_8754" s="T563">conj</ta>
            <ta e="T565" id="Seg_8755" s="T564">n-n:case</ta>
            <ta e="T567" id="Seg_8756" s="T566">v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_8757" s="T567">v-v:n.fin</ta>
            <ta e="T569" id="Seg_8758" s="T568">dempro</ta>
            <ta e="T571" id="Seg_8759" s="T570">dempro</ta>
            <ta e="T572" id="Seg_8760" s="T571">propr-n:case.poss</ta>
            <ta e="T574" id="Seg_8761" s="T573">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_8762" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_8763" s="T575">conj</ta>
            <ta e="T577" id="Seg_8764" s="T576">dempro-n:case</ta>
            <ta e="T578" id="Seg_8765" s="T577">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T579" id="Seg_8766" s="T578">conj</ta>
            <ta e="T580" id="Seg_8767" s="T579">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T581" id="Seg_8768" s="T580">conj</ta>
            <ta e="T582" id="Seg_8769" s="T581">dempro-n:case</ta>
            <ta e="T583" id="Seg_8770" s="T582">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_8771" s="T583">adv</ta>
            <ta e="T585" id="Seg_8772" s="T584">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_8773" s="T585">propr-n:case</ta>
            <ta e="T587" id="Seg_8774" s="T586">propr-n:poss-n:case</ta>
            <ta e="T588" id="Seg_8775" s="T587">que-n:case</ta>
            <ta e="T589" id="Seg_8776" s="T588">pers</ta>
            <ta e="T590" id="Seg_8777" s="T589">adv</ta>
            <ta e="T591" id="Seg_8778" s="T590">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T592" id="Seg_8779" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_8780" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_8781" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_8782" s="T594">v-v:n.fin</ta>
            <ta e="T596" id="Seg_8783" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_8784" s="T596">adj-n:case</ta>
            <ta e="T598" id="Seg_8785" s="T597">conj</ta>
            <ta e="T599" id="Seg_8786" s="T598">dempro-n:case</ta>
            <ta e="T600" id="Seg_8787" s="T599">v-v&gt;v-v:pn</ta>
            <ta e="T601" id="Seg_8788" s="T600">v-v&gt;v-v:mood.pn</ta>
            <ta e="T602" id="Seg_8789" s="T601">n-n:case</ta>
            <ta e="T603" id="Seg_8790" s="T602">conj</ta>
            <ta e="T604" id="Seg_8791" s="T603">n-n:case.poss</ta>
            <ta e="T605" id="Seg_8792" s="T604">adv</ta>
            <ta e="T606" id="Seg_8793" s="T605">dempro-n:case</ta>
            <ta e="T607" id="Seg_8794" s="T606">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_8795" s="T607">n-n:case</ta>
            <ta e="T609" id="Seg_8796" s="T608">dempro-n:case</ta>
            <ta e="T610" id="Seg_8797" s="T609">dempro-n:case</ta>
            <ta e="T611" id="Seg_8798" s="T610">v-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_8799" s="T611">dempro-n:case</ta>
            <ta e="T613" id="Seg_8800" s="T612">v-v:tense-v:pn</ta>
            <ta e="T614" id="Seg_8801" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_8802" s="T614">%%</ta>
            <ta e="T616" id="Seg_8803" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_8804" s="T616">n-n:case</ta>
            <ta e="T618" id="Seg_8805" s="T617">conj</ta>
            <ta e="T619" id="Seg_8806" s="T618">refl-n:case.poss</ta>
            <ta e="T620" id="Seg_8807" s="T619">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_8808" s="T620">v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_8809" s="T621">conj</ta>
            <ta e="T623" id="Seg_8810" s="T622">n-n:case.poss</ta>
            <ta e="T624" id="Seg_8811" s="T623">v-v:tense-v:pn</ta>
            <ta e="T625" id="Seg_8812" s="T624">n-n:case</ta>
            <ta e="T626" id="Seg_8813" s="T625">refl-n:case.poss</ta>
            <ta e="T627" id="Seg_8814" s="T626">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_8815" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_8816" s="T628">adv</ta>
            <ta e="T630" id="Seg_8817" s="T629">dempro-n:case</ta>
            <ta e="T631" id="Seg_8818" s="T630">n-n:case</ta>
            <ta e="T632" id="Seg_8819" s="T631">ptcl</ta>
            <ta e="T633" id="Seg_8820" s="T632">v-v:mood.pn</ta>
            <ta e="T634" id="Seg_8821" s="T633">dempro-n:case</ta>
            <ta e="T635" id="Seg_8822" s="T634">n-n:case</ta>
            <ta e="T636" id="Seg_8823" s="T635">dempro-n:case</ta>
            <ta e="T637" id="Seg_8824" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_8825" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_8826" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_8827" s="T639">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T641" id="Seg_8828" s="T640">dempro-n:case</ta>
            <ta e="T642" id="Seg_8829" s="T641">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T643" id="Seg_8830" s="T642">conj</ta>
            <ta e="T644" id="Seg_8831" s="T643">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T645" id="Seg_8832" s="T644">conj</ta>
            <ta e="T646" id="Seg_8833" s="T645">dempro-n:case</ta>
            <ta e="T647" id="Seg_8834" s="T646">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T648" id="Seg_8835" s="T647">v-v&gt;v-v:pn</ta>
            <ta e="T649" id="Seg_8836" s="T648">propr-n:case</ta>
            <ta e="T650" id="Seg_8837" s="T649">v-v&gt;v-v:pn</ta>
            <ta e="T651" id="Seg_8838" s="T650">ptcl</ta>
            <ta e="T652" id="Seg_8839" s="T651">adv</ta>
            <ta e="T653" id="Seg_8840" s="T652">pers</ta>
            <ta e="T654" id="Seg_8841" s="T653">v-v:n.fin</ta>
            <ta e="T655" id="Seg_8842" s="T654">pers-n:case</ta>
            <ta e="T657" id="Seg_8843" s="T656">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_8844" s="T657">adv</ta>
            <ta e="T659" id="Seg_8845" s="T658">dempro-n:case</ta>
            <ta e="T660" id="Seg_8846" s="T659">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T661" id="Seg_8847" s="T660">dempro-n:case</ta>
            <ta e="T662" id="Seg_8848" s="T661">num-num&gt;num</ta>
            <ta e="T663" id="Seg_8849" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_8850" s="T663">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T665" id="Seg_8851" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_8852" s="T665">adv</ta>
            <ta e="T668" id="Seg_8853" s="T667">dempro-n:num</ta>
            <ta e="T669" id="Seg_8854" s="T668">v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_8855" s="T669">refl-n:case.poss</ta>
            <ta e="T672" id="Seg_8856" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_8857" s="T672">v-v&gt;v-v:pn</ta>
            <ta e="T674" id="Seg_8858" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_8859" s="T674">v-v:tense-v:pn</ta>
            <ta e="T676" id="Seg_8860" s="T675">v-%%-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_8861" s="T676">v-v:tense-v:pn</ta>
            <ta e="T678" id="Seg_8862" s="T677">conj</ta>
            <ta e="T680" id="Seg_8863" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_8864" s="T680">v-v:n.fin</ta>
            <ta e="T682" id="Seg_8865" s="T681">adv</ta>
            <ta e="T683" id="Seg_8866" s="T682">n-n:num-n:case.poss</ta>
            <ta e="T684" id="Seg_8867" s="T683">v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_8868" s="T684">dempro-n:case</ta>
            <ta e="T686" id="Seg_8869" s="T685">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_8870" s="T686">conj</ta>
            <ta e="T688" id="Seg_8871" s="T687">refl-n:case.poss</ta>
            <ta e="T689" id="Seg_8872" s="T688">v-v:tense-v:pn</ta>
            <ta e="T690" id="Seg_8873" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_8874" s="T690">conj</ta>
            <ta e="T692" id="Seg_8875" s="T691">n-n:case</ta>
            <ta e="T693" id="Seg_8876" s="T692">conj</ta>
            <ta e="T694" id="Seg_8877" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_8878" s="T694">v-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_8879" s="T695">v-v&gt;v-v:pn</ta>
            <ta e="T697" id="Seg_8880" s="T696">adv</ta>
            <ta e="T698" id="Seg_8881" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_8882" s="T698">v-v:tense-v:pn</ta>
            <ta e="T701" id="Seg_8883" s="T700">v-v:tense-v:pn</ta>
            <ta e="T702" id="Seg_8884" s="T701">dempro-n:case</ta>
            <ta e="T703" id="Seg_8885" s="T702">v-v&gt;v-v:pn</ta>
            <ta e="T704" id="Seg_8886" s="T703">v-v:tense-v:pn</ta>
            <ta e="T705" id="Seg_8887" s="T704">n-n:case</ta>
            <ta e="T707" id="Seg_8888" s="T706">adj-n:case</ta>
            <ta e="T708" id="Seg_8889" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_8890" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_8891" s="T709">v-v:mood.pn</ta>
            <ta e="T711" id="Seg_8892" s="T710">v-v:tense-v:pn</ta>
            <ta e="T712" id="Seg_8893" s="T711">n-n:case</ta>
            <ta e="T713" id="Seg_8894" s="T712">conj</ta>
            <ta e="T714" id="Seg_8895" s="T713">adj-n:case</ta>
            <ta e="T715" id="Seg_8896" s="T714">n-n:case</ta>
            <ta e="T716" id="Seg_8897" s="T715">dempro-n:case</ta>
            <ta e="T717" id="Seg_8898" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_8899" s="T717">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T719" id="Seg_8900" s="T718">v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_8901" s="T719">dempro-n:case</ta>
            <ta e="T721" id="Seg_8902" s="T720">dempro-n:case</ta>
            <ta e="T722" id="Seg_8903" s="T721">v-v:tense-v:pn</ta>
            <ta e="T724" id="Seg_8904" s="T723">adj</ta>
            <ta e="T725" id="Seg_8905" s="T724">n-n:case</ta>
            <ta e="T726" id="Seg_8906" s="T725">conj</ta>
            <ta e="T727" id="Seg_8907" s="T726">adv</ta>
            <ta e="T728" id="Seg_8908" s="T727">adj</ta>
            <ta e="T729" id="Seg_8909" s="T728">n-n:case</ta>
            <ta e="T730" id="Seg_8910" s="T729">dempro-n:case</ta>
            <ta e="T731" id="Seg_8911" s="T730">v-v&gt;v-v:pn</ta>
            <ta e="T732" id="Seg_8912" s="T731">adv</ta>
            <ta e="T733" id="Seg_8913" s="T732">pers</ta>
            <ta e="T734" id="Seg_8914" s="T733">v-v:tense-v:pn</ta>
            <ta e="T735" id="Seg_8915" s="T734">adv</ta>
            <ta e="T736" id="Seg_8916" s="T735">v-v:tense-v:pn</ta>
            <ta e="T738" id="Seg_8917" s="T737">conj</ta>
            <ta e="T739" id="Seg_8918" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_8919" s="T739">pers</ta>
            <ta e="T741" id="Seg_8920" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_8921" s="T741">pers</ta>
            <ta e="T743" id="Seg_8922" s="T742">adv</ta>
            <ta e="T744" id="Seg_8923" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_8924" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_8925" s="T745">pers</ta>
            <ta e="T747" id="Seg_8926" s="T746">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T748" id="Seg_8927" s="T747">pers</ta>
            <ta e="T750" id="Seg_8928" s="T749">n-n:num-n:case.poss</ta>
            <ta e="T751" id="Seg_8929" s="T750">conj</ta>
            <ta e="T752" id="Seg_8930" s="T751">quant</ta>
            <ta e="T753" id="Seg_8931" s="T752">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T754" id="Seg_8932" s="T753">n-n:case</ta>
            <ta e="T755" id="Seg_8933" s="T754">conj</ta>
            <ta e="T756" id="Seg_8934" s="T755">n-n:case</ta>
            <ta e="T757" id="Seg_8935" s="T756">conj</ta>
            <ta e="T758" id="Seg_8936" s="T757">n-n:case</ta>
            <ta e="T759" id="Seg_8937" s="T758">adv</ta>
            <ta e="T760" id="Seg_8938" s="T759">v-v:mood.pn</ta>
            <ta e="T761" id="Seg_8939" s="T760">pers</ta>
            <ta e="T762" id="Seg_8940" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_8941" s="T762">dempro-n:num</ta>
            <ta e="T764" id="Seg_8942" s="T763">v-v:tense-v:pn</ta>
            <ta e="T765" id="Seg_8943" s="T764">dempro-n:case</ta>
            <ta e="T766" id="Seg_8944" s="T765">dempro-n:case</ta>
            <ta e="T767" id="Seg_8945" s="T766">n-n:case</ta>
            <ta e="T768" id="Seg_8946" s="T767">n-n:num-n:case.poss</ta>
            <ta e="T769" id="Seg_8947" s="T768">v-v-v-v:tense</ta>
            <ta e="T770" id="Seg_8948" s="T769">conj</ta>
            <ta e="T771" id="Seg_8949" s="T770">refl-n:case.poss</ta>
            <ta e="T772" id="Seg_8950" s="T771">v-v:tense-v:pn</ta>
            <ta e="T773" id="Seg_8951" s="T772">n-n:case</ta>
            <ta e="T774" id="Seg_8952" s="T773">conj</ta>
            <ta e="T775" id="Seg_8953" s="T774">quant</ta>
            <ta e="T776" id="Seg_8954" s="T775">que-n:case</ta>
            <ta e="T777" id="Seg_8955" s="T776">adv</ta>
            <ta e="T778" id="Seg_8956" s="T777">v-v:tense-v:pn</ta>
            <ta e="T779" id="Seg_8957" s="T778">adv</ta>
            <ta e="T780" id="Seg_8958" s="T779">dempro-n:case</ta>
            <ta e="T781" id="Seg_8959" s="T780">propr-n:case</ta>
            <ta e="T782" id="Seg_8960" s="T781">dempro-n:case</ta>
            <ta e="T783" id="Seg_8961" s="T782">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T784" id="Seg_8962" s="T783">n-n:case.poss</ta>
            <ta e="T785" id="Seg_8963" s="T784">conj</ta>
            <ta e="T786" id="Seg_8964" s="T785">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T787" id="Seg_8965" s="T786">n-n:case.poss</ta>
            <ta e="T788" id="Seg_8966" s="T787">v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_8967" s="T788">n-n:case.poss</ta>
            <ta e="T790" id="Seg_8968" s="T789">quant</ta>
            <ta e="T791" id="Seg_8969" s="T790">que-n:case</ta>
            <ta e="T792" id="Seg_8970" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_8971" s="T792">adv</ta>
            <ta e="T794" id="Seg_8972" s="T793">n-n:case.poss</ta>
            <ta e="T795" id="Seg_8973" s="T794">v-v:tense-v:pn</ta>
            <ta e="T796" id="Seg_8974" s="T795">v-v:tense-v:pn</ta>
            <ta e="T797" id="Seg_8975" s="T796">adv</ta>
            <ta e="T798" id="Seg_8976" s="T797">dempro-n:case</ta>
            <ta e="T799" id="Seg_8977" s="T798">n-n:case.poss</ta>
            <ta e="T800" id="Seg_8978" s="T799">v-v:tense-v:pn</ta>
            <ta e="T801" id="Seg_8979" s="T800">v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_8980" s="T801">n.[n:case]</ta>
            <ta e="T803" id="Seg_8981" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_8982" s="T803">conj</ta>
            <ta e="T805" id="Seg_8983" s="T804">adv</ta>
            <ta e="T806" id="Seg_8984" s="T805">adv</ta>
            <ta e="T807" id="Seg_8985" s="T806">v-v&gt;v-v:pn</ta>
            <ta e="T808" id="Seg_8986" s="T807">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_8987" s="T0">num</ta>
            <ta e="T2" id="Seg_8988" s="T1">n</ta>
            <ta e="T3" id="Seg_8989" s="T2">v</ta>
            <ta e="T4" id="Seg_8990" s="T3">dempro</ta>
            <ta e="T5" id="Seg_8991" s="T4">v</ta>
            <ta e="T6" id="Seg_8992" s="T5">num</ta>
            <ta e="T7" id="Seg_8993" s="T6">n</ta>
            <ta e="T8" id="Seg_8994" s="T7">dempro</ta>
            <ta e="T9" id="Seg_8995" s="T8">n</ta>
            <ta e="T10" id="Seg_8996" s="T9">v</ta>
            <ta e="T12" id="Seg_8997" s="T11">v</ta>
            <ta e="T13" id="Seg_8998" s="T12">adj</ta>
            <ta e="T14" id="Seg_8999" s="T13">que</ta>
            <ta e="T15" id="Seg_9000" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_9001" s="T15">v</ta>
            <ta e="T17" id="Seg_9002" s="T16">dempro</ta>
            <ta e="T18" id="Seg_9003" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_9004" s="T18">v</ta>
            <ta e="T20" id="Seg_9005" s="T19">que</ta>
            <ta e="T21" id="Seg_9006" s="T20">v</ta>
            <ta e="T22" id="Seg_9007" s="T21">v</ta>
            <ta e="T23" id="Seg_9008" s="T22">n</ta>
            <ta e="T24" id="Seg_9009" s="T23">v</ta>
            <ta e="T25" id="Seg_9010" s="T24">dempro</ta>
            <ta e="T26" id="Seg_9011" s="T25">v</ta>
            <ta e="T27" id="Seg_9012" s="T26">v</ta>
            <ta e="T28" id="Seg_9013" s="T27">adv</ta>
            <ta e="T29" id="Seg_9014" s="T28">num</ta>
            <ta e="T30" id="Seg_9015" s="T29">n</ta>
            <ta e="T32" id="Seg_9016" s="T31">v</ta>
            <ta e="T33" id="Seg_9017" s="T32">pers</ta>
            <ta e="T34" id="Seg_9018" s="T33">v</ta>
            <ta e="T35" id="Seg_9019" s="T34">dempro</ta>
            <ta e="T36" id="Seg_9020" s="T35">v</ta>
            <ta e="T37" id="Seg_9021" s="T36">v</ta>
            <ta e="T38" id="Seg_9022" s="T37">que</ta>
            <ta e="T39" id="Seg_9023" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_9024" s="T39">v</ta>
            <ta e="T41" id="Seg_9025" s="T40">adv</ta>
            <ta e="T42" id="Seg_9026" s="T41">adj</ta>
            <ta e="T43" id="Seg_9027" s="T42">n</ta>
            <ta e="T45" id="Seg_9028" s="T44">v</ta>
            <ta e="T46" id="Seg_9029" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_9030" s="T46">v</ta>
            <ta e="T48" id="Seg_9031" s="T47">que</ta>
            <ta e="T49" id="Seg_9032" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_9033" s="T49">v</ta>
            <ta e="T51" id="Seg_9034" s="T50">adv</ta>
            <ta e="T52" id="Seg_9035" s="T51">propr</ta>
            <ta e="T53" id="Seg_9036" s="T52">v</ta>
            <ta e="T54" id="Seg_9037" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_9038" s="T54">v</ta>
            <ta e="T56" id="Seg_9039" s="T55">v</ta>
            <ta e="T57" id="Seg_9040" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9041" s="T57">dempro</ta>
            <ta e="T59" id="Seg_9042" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_9043" s="T59">n</ta>
            <ta e="T61" id="Seg_9044" s="T60">n</ta>
            <ta e="T62" id="Seg_9045" s="T61">v</ta>
            <ta e="T63" id="Seg_9046" s="T62">conj</ta>
            <ta e="T64" id="Seg_9047" s="T63">adv</ta>
            <ta e="T65" id="Seg_9048" s="T64">v</ta>
            <ta e="T66" id="Seg_9049" s="T65">adv</ta>
            <ta e="T67" id="Seg_9050" s="T66">v</ta>
            <ta e="T68" id="Seg_9051" s="T67">v</ta>
            <ta e="T69" id="Seg_9052" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_9053" s="T69">v</ta>
            <ta e="T71" id="Seg_9054" s="T70">n</ta>
            <ta e="T72" id="Seg_9055" s="T71">dempro</ta>
            <ta e="T74" id="Seg_9056" s="T73">v</ta>
            <ta e="T76" id="Seg_9057" s="T75">n</ta>
            <ta e="T77" id="Seg_9058" s="T76">v</ta>
            <ta e="T78" id="Seg_9059" s="T77">n</ta>
            <ta e="T79" id="Seg_9060" s="T78">adj</ta>
            <ta e="T80" id="Seg_9061" s="T79">dempro</ta>
            <ta e="T81" id="Seg_9062" s="T80">v</ta>
            <ta e="T82" id="Seg_9063" s="T81">v</ta>
            <ta e="T83" id="Seg_9064" s="T82">num</ta>
            <ta e="T84" id="Seg_9065" s="T83">n</ta>
            <ta e="T86" id="Seg_9066" s="T85">v</ta>
            <ta e="T87" id="Seg_9067" s="T86">adv</ta>
            <ta e="T88" id="Seg_9068" s="T87">v</ta>
            <ta e="T89" id="Seg_9069" s="T88">n</ta>
            <ta e="T90" id="Seg_9070" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_9071" s="T90">que</ta>
            <ta e="T92" id="Seg_9072" s="T91">n</ta>
            <ta e="T94" id="Seg_9073" s="T93">pers</ta>
            <ta e="T95" id="Seg_9074" s="T94">v</ta>
            <ta e="T96" id="Seg_9075" s="T95">que</ta>
            <ta e="T97" id="Seg_9076" s="T96">v</ta>
            <ta e="T98" id="Seg_9077" s="T97">dempro</ta>
            <ta e="T99" id="Seg_9078" s="T98">pers</ta>
            <ta e="T100" id="Seg_9079" s="T99">v</ta>
            <ta e="T101" id="Seg_9080" s="T100">n</ta>
            <ta e="T102" id="Seg_9081" s="T101">dempro</ta>
            <ta e="T106" id="Seg_9082" s="T105">v</ta>
            <ta e="T107" id="Seg_9083" s="T106">conj</ta>
            <ta e="T108" id="Seg_9084" s="T107">v</ta>
            <ta e="T109" id="Seg_9085" s="T108">adv</ta>
            <ta e="T110" id="Seg_9086" s="T109">n</ta>
            <ta e="T112" id="Seg_9087" s="T111">v</ta>
            <ta e="T113" id="Seg_9088" s="T112">adv</ta>
            <ta e="T114" id="Seg_9089" s="T113">v</ta>
            <ta e="T115" id="Seg_9090" s="T114">v</ta>
            <ta e="T116" id="Seg_9091" s="T115">v</ta>
            <ta e="T117" id="Seg_9092" s="T116">n</ta>
            <ta e="T118" id="Seg_9093" s="T117">que</ta>
            <ta e="T119" id="Seg_9094" s="T118">quant</ta>
            <ta e="T120" id="Seg_9095" s="T119">n</ta>
            <ta e="T121" id="Seg_9096" s="T120">v</ta>
            <ta e="T122" id="Seg_9097" s="T121">n</ta>
            <ta e="T126" id="Seg_9098" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_9099" s="T126">que</ta>
            <ta e="T130" id="Seg_9100" s="T129">que</ta>
            <ta e="T131" id="Seg_9101" s="T130">dempro</ta>
            <ta e="T132" id="Seg_9102" s="T131">n</ta>
            <ta e="T136" id="Seg_9103" s="T135">v</ta>
            <ta e="T137" id="Seg_9104" s="T136">que</ta>
            <ta e="T138" id="Seg_9105" s="T137">n</ta>
            <ta e="T139" id="Seg_9106" s="T138">dempro</ta>
            <ta e="T141" id="Seg_9107" s="T140">v</ta>
            <ta e="T142" id="Seg_9108" s="T141">num</ta>
            <ta e="T143" id="Seg_9109" s="T142">adv</ta>
            <ta e="T144" id="Seg_9110" s="T143">adj</ta>
            <ta e="T145" id="Seg_9111" s="T144">adv</ta>
            <ta e="T146" id="Seg_9112" s="T145">propr</ta>
            <ta e="T147" id="Seg_9113" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_9114" s="T147">v</ta>
            <ta e="T149" id="Seg_9115" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_9116" s="T149">adv</ta>
            <ta e="T151" id="Seg_9117" s="T150">dempro</ta>
            <ta e="T152" id="Seg_9118" s="T151">propr</ta>
            <ta e="T153" id="Seg_9119" s="T152">v</ta>
            <ta e="T154" id="Seg_9120" s="T153">v</ta>
            <ta e="T155" id="Seg_9121" s="T154">v</ta>
            <ta e="T156" id="Seg_9122" s="T155">n</ta>
            <ta e="T157" id="Seg_9123" s="T156">v</ta>
            <ta e="T158" id="Seg_9124" s="T157">v</ta>
            <ta e="T160" id="Seg_9125" s="T159">v</ta>
            <ta e="T161" id="Seg_9126" s="T160">v</ta>
            <ta e="T162" id="Seg_9127" s="T161">n</ta>
            <ta e="T163" id="Seg_9128" s="T162">v</ta>
            <ta e="T164" id="Seg_9129" s="T163">n</ta>
            <ta e="T165" id="Seg_9130" s="T164">v</ta>
            <ta e="T166" id="Seg_9131" s="T165">adv</ta>
            <ta e="T167" id="Seg_9132" s="T166">v</ta>
            <ta e="T168" id="Seg_9133" s="T167">adv</ta>
            <ta e="T169" id="Seg_9134" s="T168">v</ta>
            <ta e="T170" id="Seg_9135" s="T169">v</ta>
            <ta e="T171" id="Seg_9136" s="T170">n</ta>
            <ta e="T172" id="Seg_9137" s="T171">v</ta>
            <ta e="T173" id="Seg_9138" s="T172">v</ta>
            <ta e="T174" id="Seg_9139" s="T173">v</ta>
            <ta e="T175" id="Seg_9140" s="T174">adv</ta>
            <ta e="T176" id="Seg_9141" s="T175">n</ta>
            <ta e="T177" id="Seg_9142" s="T176">v</ta>
            <ta e="T178" id="Seg_9143" s="T177">v</ta>
            <ta e="T179" id="Seg_9144" s="T178">conj</ta>
            <ta e="T180" id="Seg_9145" s="T179">adv</ta>
            <ta e="T181" id="Seg_9146" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_9147" s="T181">v</ta>
            <ta e="T183" id="Seg_9148" s="T182">adv</ta>
            <ta e="T184" id="Seg_9149" s="T183">v</ta>
            <ta e="T185" id="Seg_9150" s="T184">v</ta>
            <ta e="T186" id="Seg_9151" s="T185">adv</ta>
            <ta e="T187" id="Seg_9152" s="T186">v</ta>
            <ta e="T188" id="Seg_9153" s="T187">conj</ta>
            <ta e="T189" id="Seg_9154" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_9155" s="T189">v</ta>
            <ta e="T191" id="Seg_9156" s="T190">v</ta>
            <ta e="T192" id="Seg_9157" s="T191">adv</ta>
            <ta e="T193" id="Seg_9158" s="T192">n</ta>
            <ta e="T194" id="Seg_9159" s="T193">v</ta>
            <ta e="T195" id="Seg_9160" s="T194">que</ta>
            <ta e="T196" id="Seg_9161" s="T195">v</ta>
            <ta e="T197" id="Seg_9162" s="T196">n</ta>
            <ta e="T198" id="Seg_9163" s="T197">v</ta>
            <ta e="T199" id="Seg_9164" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_9165" s="T199">pers</ta>
            <ta e="T201" id="Seg_9166" s="T200">n</ta>
            <ta e="T202" id="Seg_9167" s="T201">v</ta>
            <ta e="T203" id="Seg_9168" s="T202">conj</ta>
            <ta e="T204" id="Seg_9169" s="T203">dempro</ta>
            <ta e="T205" id="Seg_9170" s="T204">pers</ta>
            <ta e="T206" id="Seg_9171" s="T205">v</ta>
            <ta e="T207" id="Seg_9172" s="T206">ptcl</ta>
            <ta e="T209" id="Seg_9173" s="T208">conj</ta>
            <ta e="T210" id="Seg_9174" s="T209">que</ta>
            <ta e="T211" id="Seg_9175" s="T210">pers</ta>
            <ta e="T212" id="Seg_9176" s="T211">v</ta>
            <ta e="T213" id="Seg_9177" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_9178" s="T213">v</ta>
            <ta e="T215" id="Seg_9179" s="T214">n</ta>
            <ta e="T216" id="Seg_9180" s="T215">n</ta>
            <ta e="T218" id="Seg_9181" s="T217">v</ta>
            <ta e="T219" id="Seg_9182" s="T218">n</ta>
            <ta e="T220" id="Seg_9183" s="T219">v</ta>
            <ta e="T221" id="Seg_9184" s="T220">v</ta>
            <ta e="T222" id="Seg_9185" s="T221">n</ta>
            <ta e="T223" id="Seg_9186" s="T222">n</ta>
            <ta e="T224" id="Seg_9187" s="T223">adj</ta>
            <ta e="T225" id="Seg_9188" s="T224">adv</ta>
            <ta e="T226" id="Seg_9189" s="T225">dempro</ta>
            <ta e="T227" id="Seg_9190" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_9191" s="T227">pers</ta>
            <ta e="T229" id="Seg_9192" s="T228">refl</ta>
            <ta e="T230" id="Seg_9193" s="T229">n</ta>
            <ta e="T231" id="Seg_9194" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_9195" s="T231">num</ta>
            <ta e="T233" id="Seg_9196" s="T232">n</ta>
            <ta e="T234" id="Seg_9197" s="T233">adv</ta>
            <ta e="T235" id="Seg_9198" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_9199" s="T235">v</ta>
            <ta e="T237" id="Seg_9200" s="T236">v</ta>
            <ta e="T239" id="Seg_9201" s="T238">pers</ta>
            <ta e="T240" id="Seg_9202" s="T239">pers</ta>
            <ta e="T241" id="Seg_9203" s="T240">pers</ta>
            <ta e="T242" id="Seg_9204" s="T241">v</ta>
            <ta e="T243" id="Seg_9205" s="T242">v</ta>
            <ta e="T244" id="Seg_9206" s="T243">adv</ta>
            <ta e="T245" id="Seg_9207" s="T244">dempro</ta>
            <ta e="T246" id="Seg_9208" s="T245">v</ta>
            <ta e="T247" id="Seg_9209" s="T246">v</ta>
            <ta e="T248" id="Seg_9210" s="T247">adv</ta>
            <ta e="T249" id="Seg_9211" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_9212" s="T249">n</ta>
            <ta e="T251" id="Seg_9213" s="T250">v</ta>
            <ta e="T252" id="Seg_9214" s="T251">v</ta>
            <ta e="T253" id="Seg_9215" s="T252">dempro</ta>
            <ta e="T254" id="Seg_9216" s="T253">n</ta>
            <ta e="T255" id="Seg_9217" s="T254">conj</ta>
            <ta e="T256" id="Seg_9218" s="T255">n</ta>
            <ta e="T257" id="Seg_9219" s="T256">aux</ta>
            <ta e="T258" id="Seg_9220" s="T257">v</ta>
            <ta e="T260" id="Seg_9221" s="T258">ptcl</ta>
            <ta e="T261" id="Seg_9222" s="T260">adv</ta>
            <ta e="T262" id="Seg_9223" s="T261">dempro</ta>
            <ta e="T263" id="Seg_9224" s="T262">v</ta>
            <ta e="T264" id="Seg_9225" s="T263">v</ta>
            <ta e="T265" id="Seg_9226" s="T264">n</ta>
            <ta e="T266" id="Seg_9227" s="T265">v</ta>
            <ta e="T267" id="Seg_9228" s="T266">n</ta>
            <ta e="T268" id="Seg_9229" s="T267">adj</ta>
            <ta e="T269" id="Seg_9230" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_9231" s="T269">v</ta>
            <ta e="T271" id="Seg_9232" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_9233" s="T271">dempro</ta>
            <ta e="T273" id="Seg_9234" s="T272">v</ta>
            <ta e="T274" id="Seg_9235" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_9236" s="T274">v</ta>
            <ta e="T276" id="Seg_9237" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_9238" s="T276">dempro</ta>
            <ta e="T278" id="Seg_9239" s="T277">v</ta>
            <ta e="T279" id="Seg_9240" s="T278">conj</ta>
            <ta e="T282" id="Seg_9241" s="T281">v</ta>
            <ta e="T283" id="Seg_9242" s="T282">n</ta>
            <ta e="T284" id="Seg_9243" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_9244" s="T284">adv</ta>
            <ta e="T286" id="Seg_9245" s="T285">dempro</ta>
            <ta e="T287" id="Seg_9246" s="T286">v</ta>
            <ta e="T288" id="Seg_9247" s="T287">n</ta>
            <ta e="T289" id="Seg_9248" s="T288">pers</ta>
            <ta e="T290" id="Seg_9249" s="T289">que</ta>
            <ta e="T291" id="Seg_9250" s="T290">que</ta>
            <ta e="T292" id="Seg_9251" s="T291">adv</ta>
            <ta e="T293" id="Seg_9252" s="T292">v</ta>
            <ta e="T294" id="Seg_9253" s="T293">v</ta>
            <ta e="T295" id="Seg_9254" s="T294">que</ta>
            <ta e="T296" id="Seg_9255" s="T295">pers</ta>
            <ta e="T297" id="Seg_9256" s="T296">n</ta>
            <ta e="T298" id="Seg_9257" s="T297">pers</ta>
            <ta e="T299" id="Seg_9258" s="T298">propr</ta>
            <ta e="T300" id="Seg_9259" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_9260" s="T300">pers</ta>
            <ta e="T302" id="Seg_9261" s="T301">v</ta>
            <ta e="T304" id="Seg_9262" s="T303">adv</ta>
            <ta e="T305" id="Seg_9263" s="T304">dempro</ta>
            <ta e="T306" id="Seg_9264" s="T305">v</ta>
            <ta e="T307" id="Seg_9265" s="T306">conj</ta>
            <ta e="T308" id="Seg_9266" s="T307">dempro</ta>
            <ta e="T309" id="Seg_9267" s="T308">pers</ta>
            <ta e="T310" id="Seg_9268" s="T309">v</ta>
            <ta e="T311" id="Seg_9269" s="T310">conj</ta>
            <ta e="T313" id="Seg_9270" s="T312">v</ta>
            <ta e="T314" id="Seg_9271" s="T313">conj</ta>
            <ta e="T315" id="Seg_9272" s="T314">pers</ta>
            <ta e="T316" id="Seg_9273" s="T315">v</ta>
            <ta e="T317" id="Seg_9274" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_9275" s="T317">conj</ta>
            <ta e="T319" id="Seg_9276" s="T318">pers</ta>
            <ta e="T320" id="Seg_9277" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_9278" s="T320">pers</ta>
            <ta e="T322" id="Seg_9279" s="T321">v</ta>
            <ta e="T323" id="Seg_9280" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_9281" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_9282" s="T324">v</ta>
            <ta e="T326" id="Seg_9283" s="T325">v</ta>
            <ta e="T327" id="Seg_9284" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_9285" s="T327">pers</ta>
            <ta e="T329" id="Seg_9286" s="T328">pers</ta>
            <ta e="T330" id="Seg_9287" s="T329">que</ta>
            <ta e="T331" id="Seg_9288" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_9289" s="T331">v</ta>
            <ta e="T333" id="Seg_9290" s="T332">v</ta>
            <ta e="T334" id="Seg_9291" s="T333">n</ta>
            <ta e="T335" id="Seg_9292" s="T334">v</ta>
            <ta e="T337" id="Seg_9293" s="T336">n</ta>
            <ta e="T338" id="Seg_9294" s="T337">adv</ta>
            <ta e="T339" id="Seg_9295" s="T338">dempro</ta>
            <ta e="T340" id="Seg_9296" s="T339">v</ta>
            <ta e="T341" id="Seg_9297" s="T340">n</ta>
            <ta e="T342" id="Seg_9298" s="T341">n</ta>
            <ta e="T343" id="Seg_9299" s="T342">v</ta>
            <ta e="T344" id="Seg_9300" s="T343">pers</ta>
            <ta e="T345" id="Seg_9301" s="T344">v</ta>
            <ta e="T346" id="Seg_9302" s="T345">pers</ta>
            <ta e="T347" id="Seg_9303" s="T346">aux</ta>
            <ta e="T348" id="Seg_9304" s="T347">v</ta>
            <ta e="T349" id="Seg_9305" s="T348">conj</ta>
            <ta e="T350" id="Seg_9306" s="T349">pers</ta>
            <ta e="T351" id="Seg_9307" s="T350">v</ta>
            <ta e="T352" id="Seg_9308" s="T351">conj</ta>
            <ta e="T353" id="Seg_9309" s="T352">adv</ta>
            <ta e="T355" id="Seg_9310" s="T354">v</ta>
            <ta e="T356" id="Seg_9311" s="T355">pers</ta>
            <ta e="T357" id="Seg_9312" s="T356">v</ta>
            <ta e="T358" id="Seg_9313" s="T357">v</ta>
            <ta e="T359" id="Seg_9314" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_9315" s="T359">v</ta>
            <ta e="T361" id="Seg_9316" s="T360">adv</ta>
            <ta e="T362" id="Seg_9317" s="T361">adv</ta>
            <ta e="T363" id="Seg_9318" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_9319" s="T363">v</ta>
            <ta e="T365" id="Seg_9320" s="T364">conj</ta>
            <ta e="T366" id="Seg_9321" s="T365">v</ta>
            <ta e="T367" id="Seg_9322" s="T366">n</ta>
            <ta e="T368" id="Seg_9323" s="T367">conj</ta>
            <ta e="T369" id="Seg_9324" s="T368">n</ta>
            <ta e="T370" id="Seg_9325" s="T369">ptcl</ta>
            <ta e="T372" id="Seg_9326" s="T371">aux</ta>
            <ta e="T374" id="Seg_9327" s="T373">aux</ta>
            <ta e="T376" id="Seg_9328" s="T375">aux</ta>
            <ta e="T377" id="Seg_9329" s="T376">v</ta>
            <ta e="T378" id="Seg_9330" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_9331" s="T378">adv</ta>
            <ta e="T380" id="Seg_9332" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_9333" s="T380">v</ta>
            <ta e="T382" id="Seg_9334" s="T381">dempro</ta>
            <ta e="T383" id="Seg_9335" s="T382">v</ta>
            <ta e="T384" id="Seg_9336" s="T383">n</ta>
            <ta e="T385" id="Seg_9337" s="T384">v</ta>
            <ta e="T386" id="Seg_9338" s="T385">conj</ta>
            <ta e="T387" id="Seg_9339" s="T386">n</ta>
            <ta e="T388" id="Seg_9340" s="T387">adv</ta>
            <ta e="T389" id="Seg_9341" s="T388">adj</ta>
            <ta e="T390" id="Seg_9342" s="T389">que</ta>
            <ta e="T391" id="Seg_9343" s="T390">n</ta>
            <ta e="T392" id="Seg_9344" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_9345" s="T392">v</ta>
            <ta e="T394" id="Seg_9346" s="T393">adv</ta>
            <ta e="T395" id="Seg_9347" s="T394">v</ta>
            <ta e="T396" id="Seg_9348" s="T395">dempro</ta>
            <ta e="T397" id="Seg_9349" s="T396">dempro</ta>
            <ta e="T398" id="Seg_9350" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_9351" s="T398">v</ta>
            <ta e="T400" id="Seg_9352" s="T399">n</ta>
            <ta e="T401" id="Seg_9353" s="T400">v</ta>
            <ta e="T402" id="Seg_9354" s="T401">dempro</ta>
            <ta e="T403" id="Seg_9355" s="T402">v</ta>
            <ta e="T405" id="Seg_9356" s="T404">v</ta>
            <ta e="T406" id="Seg_9357" s="T405">n</ta>
            <ta e="T407" id="Seg_9358" s="T406">que</ta>
            <ta e="T408" id="Seg_9359" s="T407">v</ta>
            <ta e="T409" id="Seg_9360" s="T408">n</ta>
            <ta e="T410" id="Seg_9361" s="T409">v</ta>
            <ta e="T411" id="Seg_9362" s="T410">que</ta>
            <ta e="T412" id="Seg_9363" s="T411">pers</ta>
            <ta e="T413" id="Seg_9364" s="T412">pers</ta>
            <ta e="T414" id="Seg_9365" s="T413">pers</ta>
            <ta e="T415" id="Seg_9366" s="T414">propr</ta>
            <ta e="T416" id="Seg_9367" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_9368" s="T416">v</ta>
            <ta e="T419" id="Seg_9369" s="T418">v</ta>
            <ta e="T420" id="Seg_9370" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_9371" s="T420">v</ta>
            <ta e="T422" id="Seg_9372" s="T421">pers</ta>
            <ta e="T423" id="Seg_9373" s="T422">pers</ta>
            <ta e="T424" id="Seg_9374" s="T423">que</ta>
            <ta e="T425" id="Seg_9375" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_9376" s="T425">v</ta>
            <ta e="T427" id="Seg_9377" s="T426">adv</ta>
            <ta e="T428" id="Seg_9378" s="T427">v</ta>
            <ta e="T429" id="Seg_9379" s="T428">n</ta>
            <ta e="T432" id="Seg_9380" s="T431">v</ta>
            <ta e="T433" id="Seg_9381" s="T432">pers</ta>
            <ta e="T434" id="Seg_9382" s="T433">dempro</ta>
            <ta e="T435" id="Seg_9383" s="T434">adv</ta>
            <ta e="T436" id="Seg_9384" s="T435">dempro</ta>
            <ta e="T438" id="Seg_9385" s="T437">v</ta>
            <ta e="T439" id="Seg_9386" s="T438">conj</ta>
            <ta e="T440" id="Seg_9387" s="T439">n</ta>
            <ta e="T441" id="Seg_9388" s="T440">v</ta>
            <ta e="T442" id="Seg_9389" s="T441">aux</ta>
            <ta e="T443" id="Seg_9390" s="T442">v</ta>
            <ta e="T444" id="Seg_9391" s="T443">conj</ta>
            <ta e="T445" id="Seg_9392" s="T444">pers</ta>
            <ta e="T446" id="Seg_9393" s="T445">adv</ta>
            <ta e="T447" id="Seg_9394" s="T446">v</ta>
            <ta e="T448" id="Seg_9395" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_9396" s="T448">que</ta>
            <ta e="T451" id="Seg_9397" s="T450">dempro</ta>
            <ta e="T452" id="Seg_9398" s="T451">v</ta>
            <ta e="T453" id="Seg_9399" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_9400" s="T453">v</ta>
            <ta e="T455" id="Seg_9401" s="T454">v</ta>
            <ta e="T456" id="Seg_9402" s="T455">v</ta>
            <ta e="T457" id="Seg_9403" s="T456">adv</ta>
            <ta e="T458" id="Seg_9404" s="T457">adv</ta>
            <ta e="T459" id="Seg_9405" s="T458">n</ta>
            <ta e="T460" id="Seg_9406" s="T459">v</ta>
            <ta e="T461" id="Seg_9407" s="T460">dempro</ta>
            <ta e="T462" id="Seg_9408" s="T461">pers</ta>
            <ta e="T464" id="Seg_9409" s="T463">v</ta>
            <ta e="T465" id="Seg_9410" s="T464">v</ta>
            <ta e="T466" id="Seg_9411" s="T465">v</ta>
            <ta e="T467" id="Seg_9412" s="T466">conj</ta>
            <ta e="T468" id="Seg_9413" s="T467">pers</ta>
            <ta e="T469" id="Seg_9414" s="T468">v</ta>
            <ta e="T470" id="Seg_9415" s="T469">refl</ta>
            <ta e="T471" id="Seg_9416" s="T470">adv</ta>
            <ta e="T472" id="Seg_9417" s="T471">v</ta>
            <ta e="T473" id="Seg_9418" s="T472">n</ta>
            <ta e="T474" id="Seg_9419" s="T473">adv</ta>
            <ta e="T475" id="Seg_9420" s="T474">v</ta>
            <ta e="T476" id="Seg_9421" s="T475">dempro</ta>
            <ta e="T477" id="Seg_9422" s="T476">v</ta>
            <ta e="T478" id="Seg_9423" s="T477">n</ta>
            <ta e="T479" id="Seg_9424" s="T478">n</ta>
            <ta e="T480" id="Seg_9425" s="T479">dempro</ta>
            <ta e="T481" id="Seg_9426" s="T480">propr</ta>
            <ta e="T482" id="Seg_9427" s="T481">v</ta>
            <ta e="T483" id="Seg_9428" s="T482">dempro</ta>
            <ta e="T484" id="Seg_9429" s="T483">dempro</ta>
            <ta e="T485" id="Seg_9430" s="T484">v</ta>
            <ta e="T486" id="Seg_9431" s="T485">conj</ta>
            <ta e="T487" id="Seg_9432" s="T486">v</ta>
            <ta e="T488" id="Seg_9433" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_9434" s="T488">v</ta>
            <ta e="T490" id="Seg_9435" s="T489">conj</ta>
            <ta e="T491" id="Seg_9436" s="T490">v</ta>
            <ta e="T492" id="Seg_9437" s="T491">propr</ta>
            <ta e="T493" id="Seg_9438" s="T492">v</ta>
            <ta e="T494" id="Seg_9439" s="T493">v</ta>
            <ta e="T495" id="Seg_9440" s="T494">adv</ta>
            <ta e="T496" id="Seg_9441" s="T495">v</ta>
            <ta e="T497" id="Seg_9442" s="T496">adv</ta>
            <ta e="T498" id="Seg_9443" s="T497">que</ta>
            <ta e="T500" id="Seg_9444" s="T499">n</ta>
            <ta e="T501" id="Seg_9445" s="T500">v</ta>
            <ta e="T502" id="Seg_9446" s="T501">adv</ta>
            <ta e="T503" id="Seg_9447" s="T502">dempro</ta>
            <ta e="T504" id="Seg_9448" s="T503">v</ta>
            <ta e="T505" id="Seg_9449" s="T504">conj</ta>
            <ta e="T506" id="Seg_9450" s="T505">dempro</ta>
            <ta e="T507" id="Seg_9451" s="T506">v</ta>
            <ta e="T508" id="Seg_9452" s="T507">adv</ta>
            <ta e="T509" id="Seg_9453" s="T508">propr</ta>
            <ta e="T510" id="Seg_9454" s="T509">v</ta>
            <ta e="T511" id="Seg_9455" s="T510">dempro</ta>
            <ta e="T512" id="Seg_9456" s="T511">n</ta>
            <ta e="T513" id="Seg_9457" s="T512">conj</ta>
            <ta e="T514" id="Seg_9458" s="T513">n</ta>
            <ta e="T515" id="Seg_9459" s="T514">v</ta>
            <ta e="T516" id="Seg_9460" s="T515">que</ta>
            <ta e="T517" id="Seg_9461" s="T516">pers</ta>
            <ta e="T518" id="Seg_9462" s="T517">adv</ta>
            <ta e="T519" id="Seg_9463" s="T518">v</ta>
            <ta e="T520" id="Seg_9464" s="T519">dempro</ta>
            <ta e="T521" id="Seg_9465" s="T520">adj</ta>
            <ta e="T522" id="Seg_9466" s="T521">adj</ta>
            <ta e="T523" id="Seg_9467" s="T522">n</ta>
            <ta e="T524" id="Seg_9468" s="T523">pers</ta>
            <ta e="T525" id="Seg_9469" s="T524">v</ta>
            <ta e="T527" id="Seg_9470" s="T526">v</ta>
            <ta e="T528" id="Seg_9471" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_9472" s="T528">v</ta>
            <ta e="T530" id="Seg_9473" s="T529">conj</ta>
            <ta e="T531" id="Seg_9474" s="T530">pers</ta>
            <ta e="T533" id="Seg_9475" s="T532">pers</ta>
            <ta e="T534" id="Seg_9476" s="T533">v</ta>
            <ta e="T536" id="Seg_9477" s="T535">dempro</ta>
            <ta e="T537" id="Seg_9478" s="T536">adv</ta>
            <ta e="T538" id="Seg_9479" s="T537">refl</ta>
            <ta e="T539" id="Seg_9480" s="T538">n</ta>
            <ta e="T540" id="Seg_9481" s="T539">v</ta>
            <ta e="T541" id="Seg_9482" s="T540">dempro</ta>
            <ta e="T542" id="Seg_9483" s="T541">dempro</ta>
            <ta e="T543" id="Seg_9484" s="T542">v</ta>
            <ta e="T544" id="Seg_9485" s="T543">v</ta>
            <ta e="T545" id="Seg_9486" s="T544">conj</ta>
            <ta e="T546" id="Seg_9487" s="T545">dempro</ta>
            <ta e="T547" id="Seg_9488" s="T546">v</ta>
            <ta e="T548" id="Seg_9489" s="T547">dempro</ta>
            <ta e="T549" id="Seg_9490" s="T548">n</ta>
            <ta e="T551" id="Seg_9491" s="T550">n</ta>
            <ta e="T552" id="Seg_9492" s="T551">v</ta>
            <ta e="T553" id="Seg_9493" s="T552">v</ta>
            <ta e="T554" id="Seg_9494" s="T553">n</ta>
            <ta e="T555" id="Seg_9495" s="T554">v</ta>
            <ta e="T556" id="Seg_9496" s="T555">dempro</ta>
            <ta e="T557" id="Seg_9497" s="T556">n</ta>
            <ta e="T558" id="Seg_9498" s="T557">v</ta>
            <ta e="T559" id="Seg_9499" s="T558">propr</ta>
            <ta e="T560" id="Seg_9500" s="T559">v</ta>
            <ta e="T561" id="Seg_9501" s="T560">refl</ta>
            <ta e="T562" id="Seg_9502" s="T561">v</ta>
            <ta e="T563" id="Seg_9503" s="T562">v</ta>
            <ta e="T564" id="Seg_9504" s="T563">conj</ta>
            <ta e="T565" id="Seg_9505" s="T564">n</ta>
            <ta e="T567" id="Seg_9506" s="T566">v</ta>
            <ta e="T568" id="Seg_9507" s="T567">v</ta>
            <ta e="T569" id="Seg_9508" s="T568">dempro</ta>
            <ta e="T571" id="Seg_9509" s="T570">dempro</ta>
            <ta e="T572" id="Seg_9510" s="T571">propr</ta>
            <ta e="T574" id="Seg_9511" s="T573">v</ta>
            <ta e="T576" id="Seg_9512" s="T575">conj</ta>
            <ta e="T577" id="Seg_9513" s="T576">dempro</ta>
            <ta e="T578" id="Seg_9514" s="T577">v</ta>
            <ta e="T579" id="Seg_9515" s="T578">conj</ta>
            <ta e="T580" id="Seg_9516" s="T579">v</ta>
            <ta e="T581" id="Seg_9517" s="T580">conj</ta>
            <ta e="T582" id="Seg_9518" s="T581">dempro</ta>
            <ta e="T583" id="Seg_9519" s="T582">v</ta>
            <ta e="T584" id="Seg_9520" s="T583">adv</ta>
            <ta e="T585" id="Seg_9521" s="T584">v</ta>
            <ta e="T586" id="Seg_9522" s="T585">propr</ta>
            <ta e="T587" id="Seg_9523" s="T586">propr</ta>
            <ta e="T588" id="Seg_9524" s="T587">que</ta>
            <ta e="T589" id="Seg_9525" s="T588">pers</ta>
            <ta e="T590" id="Seg_9526" s="T589">adv</ta>
            <ta e="T591" id="Seg_9527" s="T590">v</ta>
            <ta e="T592" id="Seg_9528" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_9529" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_9530" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_9531" s="T594">v</ta>
            <ta e="T596" id="Seg_9532" s="T595">n</ta>
            <ta e="T597" id="Seg_9533" s="T596">adj</ta>
            <ta e="T598" id="Seg_9534" s="T597">conj</ta>
            <ta e="T599" id="Seg_9535" s="T598">dempro</ta>
            <ta e="T600" id="Seg_9536" s="T599">v</ta>
            <ta e="T601" id="Seg_9537" s="T600">v</ta>
            <ta e="T602" id="Seg_9538" s="T601">n</ta>
            <ta e="T603" id="Seg_9539" s="T602">conj</ta>
            <ta e="T604" id="Seg_9540" s="T603">n</ta>
            <ta e="T605" id="Seg_9541" s="T604">adv</ta>
            <ta e="T606" id="Seg_9542" s="T605">dempro</ta>
            <ta e="T607" id="Seg_9543" s="T606">v</ta>
            <ta e="T608" id="Seg_9544" s="T607">n</ta>
            <ta e="T609" id="Seg_9545" s="T608">dempro</ta>
            <ta e="T610" id="Seg_9546" s="T609">dempro</ta>
            <ta e="T611" id="Seg_9547" s="T610">v</ta>
            <ta e="T612" id="Seg_9548" s="T611">dempro</ta>
            <ta e="T613" id="Seg_9549" s="T612">v</ta>
            <ta e="T614" id="Seg_9550" s="T613">n</ta>
            <ta e="T616" id="Seg_9551" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_9552" s="T616">n</ta>
            <ta e="T618" id="Seg_9553" s="T617">conj</ta>
            <ta e="T619" id="Seg_9554" s="T618">refl</ta>
            <ta e="T620" id="Seg_9555" s="T619">v</ta>
            <ta e="T621" id="Seg_9556" s="T620">v</ta>
            <ta e="T622" id="Seg_9557" s="T621">conj</ta>
            <ta e="T623" id="Seg_9558" s="T622">n</ta>
            <ta e="T624" id="Seg_9559" s="T623">v</ta>
            <ta e="T625" id="Seg_9560" s="T624">n</ta>
            <ta e="T626" id="Seg_9561" s="T625">refl</ta>
            <ta e="T627" id="Seg_9562" s="T626">v</ta>
            <ta e="T628" id="Seg_9563" s="T627">v</ta>
            <ta e="T629" id="Seg_9564" s="T628">adv</ta>
            <ta e="T630" id="Seg_9565" s="T629">dempro</ta>
            <ta e="T631" id="Seg_9566" s="T630">n</ta>
            <ta e="T632" id="Seg_9567" s="T631">ptcl</ta>
            <ta e="T633" id="Seg_9568" s="T632">v</ta>
            <ta e="T634" id="Seg_9569" s="T633">dempro</ta>
            <ta e="T635" id="Seg_9570" s="T634">n</ta>
            <ta e="T636" id="Seg_9571" s="T635">dempro</ta>
            <ta e="T637" id="Seg_9572" s="T636">n</ta>
            <ta e="T638" id="Seg_9573" s="T637">v</ta>
            <ta e="T639" id="Seg_9574" s="T638">n</ta>
            <ta e="T640" id="Seg_9575" s="T639">v</ta>
            <ta e="T641" id="Seg_9576" s="T640">dempro</ta>
            <ta e="T642" id="Seg_9577" s="T641">v</ta>
            <ta e="T643" id="Seg_9578" s="T642">conj</ta>
            <ta e="T644" id="Seg_9579" s="T643">v</ta>
            <ta e="T645" id="Seg_9580" s="T644">conj</ta>
            <ta e="T646" id="Seg_9581" s="T645">dempro</ta>
            <ta e="T647" id="Seg_9582" s="T646">v</ta>
            <ta e="T648" id="Seg_9583" s="T647">v</ta>
            <ta e="T649" id="Seg_9584" s="T648">propr</ta>
            <ta e="T650" id="Seg_9585" s="T649">v</ta>
            <ta e="T651" id="Seg_9586" s="T650">ptcl</ta>
            <ta e="T652" id="Seg_9587" s="T651">adv</ta>
            <ta e="T653" id="Seg_9588" s="T652">pers</ta>
            <ta e="T654" id="Seg_9589" s="T653">v</ta>
            <ta e="T655" id="Seg_9590" s="T654">pers</ta>
            <ta e="T657" id="Seg_9591" s="T656">v</ta>
            <ta e="T658" id="Seg_9592" s="T657">adv</ta>
            <ta e="T659" id="Seg_9593" s="T658">dempro</ta>
            <ta e="T660" id="Seg_9594" s="T659">v</ta>
            <ta e="T661" id="Seg_9595" s="T660">dempro</ta>
            <ta e="T662" id="Seg_9596" s="T661">num</ta>
            <ta e="T663" id="Seg_9597" s="T662">n</ta>
            <ta e="T664" id="Seg_9598" s="T663">v</ta>
            <ta e="T665" id="Seg_9599" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_9600" s="T665">adv</ta>
            <ta e="T668" id="Seg_9601" s="T667">dempro</ta>
            <ta e="T669" id="Seg_9602" s="T668">v</ta>
            <ta e="T670" id="Seg_9603" s="T669">refl</ta>
            <ta e="T672" id="Seg_9604" s="T671">n</ta>
            <ta e="T673" id="Seg_9605" s="T672">v</ta>
            <ta e="T674" id="Seg_9606" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_9607" s="T674">v</ta>
            <ta e="T676" id="Seg_9608" s="T675">v</ta>
            <ta e="T677" id="Seg_9609" s="T676">v</ta>
            <ta e="T678" id="Seg_9610" s="T677">conj</ta>
            <ta e="T680" id="Seg_9611" s="T679">v</ta>
            <ta e="T681" id="Seg_9612" s="T680">v</ta>
            <ta e="T682" id="Seg_9613" s="T681">adv</ta>
            <ta e="T683" id="Seg_9614" s="T682">n</ta>
            <ta e="T684" id="Seg_9615" s="T683">v</ta>
            <ta e="T685" id="Seg_9616" s="T684">dempro</ta>
            <ta e="T686" id="Seg_9617" s="T685">v</ta>
            <ta e="T687" id="Seg_9618" s="T686">conj</ta>
            <ta e="T688" id="Seg_9619" s="T687">refl</ta>
            <ta e="T689" id="Seg_9620" s="T688">v</ta>
            <ta e="T690" id="Seg_9621" s="T689">n</ta>
            <ta e="T691" id="Seg_9622" s="T690">conj</ta>
            <ta e="T692" id="Seg_9623" s="T691">n</ta>
            <ta e="T693" id="Seg_9624" s="T692">conj</ta>
            <ta e="T694" id="Seg_9625" s="T693">n</ta>
            <ta e="T695" id="Seg_9626" s="T694">v</ta>
            <ta e="T696" id="Seg_9627" s="T695">v</ta>
            <ta e="T697" id="Seg_9628" s="T696">adv</ta>
            <ta e="T698" id="Seg_9629" s="T697">n</ta>
            <ta e="T699" id="Seg_9630" s="T698">v</ta>
            <ta e="T701" id="Seg_9631" s="T700">v</ta>
            <ta e="T702" id="Seg_9632" s="T701">dempro</ta>
            <ta e="T703" id="Seg_9633" s="T702">v</ta>
            <ta e="T704" id="Seg_9634" s="T703">v</ta>
            <ta e="T705" id="Seg_9635" s="T704">n</ta>
            <ta e="T707" id="Seg_9636" s="T706">adj</ta>
            <ta e="T708" id="Seg_9637" s="T707">n</ta>
            <ta e="T709" id="Seg_9638" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_9639" s="T709">v</ta>
            <ta e="T711" id="Seg_9640" s="T710">v</ta>
            <ta e="T712" id="Seg_9641" s="T711">n</ta>
            <ta e="T713" id="Seg_9642" s="T712">conj</ta>
            <ta e="T714" id="Seg_9643" s="T713">adj</ta>
            <ta e="T715" id="Seg_9644" s="T714">n</ta>
            <ta e="T716" id="Seg_9645" s="T715">dempro</ta>
            <ta e="T717" id="Seg_9646" s="T716">n</ta>
            <ta e="T718" id="Seg_9647" s="T717">v</ta>
            <ta e="T719" id="Seg_9648" s="T718">v</ta>
            <ta e="T720" id="Seg_9649" s="T719">dempro</ta>
            <ta e="T721" id="Seg_9650" s="T720">dempro</ta>
            <ta e="T722" id="Seg_9651" s="T721">v</ta>
            <ta e="T724" id="Seg_9652" s="T723">adj</ta>
            <ta e="T725" id="Seg_9653" s="T724">n</ta>
            <ta e="T726" id="Seg_9654" s="T725">conj</ta>
            <ta e="T727" id="Seg_9655" s="T726">adv</ta>
            <ta e="T728" id="Seg_9656" s="T727">adj</ta>
            <ta e="T729" id="Seg_9657" s="T728">n</ta>
            <ta e="T730" id="Seg_9658" s="T729">dempro</ta>
            <ta e="T731" id="Seg_9659" s="T730">v</ta>
            <ta e="T732" id="Seg_9660" s="T731">adv</ta>
            <ta e="T733" id="Seg_9661" s="T732">pers</ta>
            <ta e="T734" id="Seg_9662" s="T733">v</ta>
            <ta e="T735" id="Seg_9663" s="T734">adv</ta>
            <ta e="T736" id="Seg_9664" s="T735">v</ta>
            <ta e="T738" id="Seg_9665" s="T737">conj</ta>
            <ta e="T739" id="Seg_9666" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_9667" s="T739">pers</ta>
            <ta e="T741" id="Seg_9668" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_9669" s="T741">pers</ta>
            <ta e="T743" id="Seg_9670" s="T742">adv</ta>
            <ta e="T744" id="Seg_9671" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_9672" s="T744">v</ta>
            <ta e="T746" id="Seg_9673" s="T745">pers</ta>
            <ta e="T747" id="Seg_9674" s="T746">v</ta>
            <ta e="T748" id="Seg_9675" s="T747">pers</ta>
            <ta e="T750" id="Seg_9676" s="T749">n</ta>
            <ta e="T751" id="Seg_9677" s="T750">conj</ta>
            <ta e="T752" id="Seg_9678" s="T751">quant</ta>
            <ta e="T753" id="Seg_9679" s="T752">v</ta>
            <ta e="T754" id="Seg_9680" s="T753">n</ta>
            <ta e="T755" id="Seg_9681" s="T754">conj</ta>
            <ta e="T756" id="Seg_9682" s="T755">n</ta>
            <ta e="T757" id="Seg_9683" s="T756">conj</ta>
            <ta e="T758" id="Seg_9684" s="T757">n</ta>
            <ta e="T759" id="Seg_9685" s="T758">adv</ta>
            <ta e="T760" id="Seg_9686" s="T759">v</ta>
            <ta e="T761" id="Seg_9687" s="T760">pers</ta>
            <ta e="T762" id="Seg_9688" s="T761">v</ta>
            <ta e="T763" id="Seg_9689" s="T762">dempro</ta>
            <ta e="T764" id="Seg_9690" s="T763">v</ta>
            <ta e="T765" id="Seg_9691" s="T764">dempro</ta>
            <ta e="T766" id="Seg_9692" s="T765">dempro</ta>
            <ta e="T767" id="Seg_9693" s="T766">n</ta>
            <ta e="T768" id="Seg_9694" s="T767">n</ta>
            <ta e="T769" id="Seg_9695" s="T768">v</ta>
            <ta e="T770" id="Seg_9696" s="T769">conj</ta>
            <ta e="T771" id="Seg_9697" s="T770">refl</ta>
            <ta e="T772" id="Seg_9698" s="T771">v</ta>
            <ta e="T773" id="Seg_9699" s="T772">n</ta>
            <ta e="T774" id="Seg_9700" s="T773">conj</ta>
            <ta e="T775" id="Seg_9701" s="T774">quant</ta>
            <ta e="T776" id="Seg_9702" s="T775">que</ta>
            <ta e="T777" id="Seg_9703" s="T776">adv</ta>
            <ta e="T778" id="Seg_9704" s="T777">v</ta>
            <ta e="T779" id="Seg_9705" s="T778">adv</ta>
            <ta e="T780" id="Seg_9706" s="T779">dempro</ta>
            <ta e="T781" id="Seg_9707" s="T780">propr</ta>
            <ta e="T782" id="Seg_9708" s="T781">dempro</ta>
            <ta e="T783" id="Seg_9709" s="T782">v</ta>
            <ta e="T784" id="Seg_9710" s="T783">n</ta>
            <ta e="T785" id="Seg_9711" s="T784">conj</ta>
            <ta e="T786" id="Seg_9712" s="T785">v</ta>
            <ta e="T787" id="Seg_9713" s="T786">n</ta>
            <ta e="T788" id="Seg_9714" s="T787">v</ta>
            <ta e="T789" id="Seg_9715" s="T788">n</ta>
            <ta e="T790" id="Seg_9716" s="T789">quant</ta>
            <ta e="T791" id="Seg_9717" s="T790">que</ta>
            <ta e="T792" id="Seg_9718" s="T791">v</ta>
            <ta e="T793" id="Seg_9719" s="T792">adv</ta>
            <ta e="T794" id="Seg_9720" s="T793">n</ta>
            <ta e="T795" id="Seg_9721" s="T794">v</ta>
            <ta e="T796" id="Seg_9722" s="T795">v</ta>
            <ta e="T797" id="Seg_9723" s="T796">adv</ta>
            <ta e="T798" id="Seg_9724" s="T797">dempro</ta>
            <ta e="T799" id="Seg_9725" s="T798">n</ta>
            <ta e="T800" id="Seg_9726" s="T799">v</ta>
            <ta e="T801" id="Seg_9727" s="T800">v</ta>
            <ta e="T802" id="Seg_9728" s="T801">n</ta>
            <ta e="T803" id="Seg_9729" s="T802">v</ta>
            <ta e="T804" id="Seg_9730" s="T803">conj</ta>
            <ta e="T805" id="Seg_9731" s="T804">adv</ta>
            <ta e="T806" id="Seg_9732" s="T805">adv</ta>
            <ta e="T807" id="Seg_9733" s="T806">v</ta>
            <ta e="T808" id="Seg_9734" s="T807">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_9735" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_9736" s="T3">pro.h:Poss</ta>
            <ta e="T7" id="Seg_9737" s="T6">np.h:Th</ta>
            <ta e="T8" id="Seg_9738" s="T7">pro.h:Poss</ta>
            <ta e="T9" id="Seg_9739" s="T8">np:Th</ta>
            <ta e="T14" id="Seg_9740" s="T13">pro:A</ta>
            <ta e="T17" id="Seg_9741" s="T16">pro.h:E</ta>
            <ta e="T20" id="Seg_9742" s="T19">pro:A</ta>
            <ta e="T22" id="Seg_9743" s="T21">0.3.h:A</ta>
            <ta e="T23" id="Seg_9744" s="T22">np.h:Th</ta>
            <ta e="T25" id="Seg_9745" s="T24">pro.h:A</ta>
            <ta e="T27" id="Seg_9746" s="T26">0.3:A</ta>
            <ta e="T28" id="Seg_9747" s="T27">adv:Time</ta>
            <ta e="T30" id="Seg_9748" s="T29">np.h:A</ta>
            <ta e="T33" id="Seg_9749" s="T32">pro.h:A</ta>
            <ta e="T35" id="Seg_9750" s="T34">pro.h:A</ta>
            <ta e="T37" id="Seg_9751" s="T36">0.3.h:E</ta>
            <ta e="T38" id="Seg_9752" s="T37">pro:Th</ta>
            <ta e="T40" id="Seg_9753" s="T39">0.3.h:E</ta>
            <ta e="T41" id="Seg_9754" s="T40">adv:Time</ta>
            <ta e="T43" id="Seg_9755" s="T42">np.h:A</ta>
            <ta e="T47" id="Seg_9756" s="T46">0.3.h:E</ta>
            <ta e="T48" id="Seg_9757" s="T47">pro:Th</ta>
            <ta e="T50" id="Seg_9758" s="T49">0.3.h:E</ta>
            <ta e="T51" id="Seg_9759" s="T50">adv:Time</ta>
            <ta e="T52" id="Seg_9760" s="T51">np.h:A</ta>
            <ta e="T55" id="Seg_9761" s="T54">0.3.h:E</ta>
            <ta e="T58" id="Seg_9762" s="T57">pro.h:A</ta>
            <ta e="T60" id="Seg_9763" s="T59">np:Ins</ta>
            <ta e="T61" id="Seg_9764" s="T60">np:P</ta>
            <ta e="T65" id="Seg_9765" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_9766" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_9767" s="T66">0.3.h:E</ta>
            <ta e="T68" id="Seg_9768" s="T67">0.3.h:E</ta>
            <ta e="T71" id="Seg_9769" s="T70">np:Th</ta>
            <ta e="T72" id="Seg_9770" s="T71">pro.h:A</ta>
            <ta e="T76" id="Seg_9771" s="T75">np:E</ta>
            <ta e="T80" id="Seg_9772" s="T79">pro:Th</ta>
            <ta e="T81" id="Seg_9773" s="T80">0.3.h:A</ta>
            <ta e="T82" id="Seg_9774" s="T81">0.3.h:A</ta>
            <ta e="T84" id="Seg_9775" s="T83">np:Th</ta>
            <ta e="T86" id="Seg_9776" s="T85">0.3.h:A</ta>
            <ta e="T87" id="Seg_9777" s="T86">adv:Time</ta>
            <ta e="T88" id="Seg_9778" s="T87">0.3.h:A</ta>
            <ta e="T89" id="Seg_9779" s="T88">np:G</ta>
            <ta e="T92" id="Seg_9780" s="T91">0.1.h:Poss</ta>
            <ta e="T94" id="Seg_9781" s="T93">pro.h:E</ta>
            <ta e="T96" id="Seg_9782" s="T95">pro:A</ta>
            <ta e="T99" id="Seg_9783" s="T98">pro.h:B</ta>
            <ta e="T100" id="Seg_9784" s="T99">0.1.h:A</ta>
            <ta e="T101" id="Seg_9785" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_9786" s="T101">pro:A</ta>
            <ta e="T108" id="Seg_9787" s="T107">0.3:A</ta>
            <ta e="T109" id="Seg_9788" s="T108">adv:Time</ta>
            <ta e="T110" id="Seg_9789" s="T109">np.h:A 0.3.h:Poss</ta>
            <ta e="T113" id="Seg_9790" s="T112">adv:Time</ta>
            <ta e="T114" id="Seg_9791" s="T113">0.3.h:E</ta>
            <ta e="T115" id="Seg_9792" s="T114">0.3.h:E</ta>
            <ta e="T116" id="Seg_9793" s="T115">0.2.h:A</ta>
            <ta e="T120" id="Seg_9794" s="T119">np:L</ta>
            <ta e="T121" id="Seg_9795" s="T120">0.2.h:A</ta>
            <ta e="T130" id="Seg_9796" s="T129">pro:L</ta>
            <ta e="T131" id="Seg_9797" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_9798" s="T131">np:Th</ta>
            <ta e="T139" id="Seg_9799" s="T138">pro.h:A</ta>
            <ta e="T142" id="Seg_9800" s="T141">np.h:A</ta>
            <ta e="T143" id="Seg_9801" s="T142">adv:L</ta>
            <ta e="T144" id="Seg_9802" s="T143">np.h:A</ta>
            <ta e="T145" id="Seg_9803" s="T144">adv:L</ta>
            <ta e="T146" id="Seg_9804" s="T145">np.h:A</ta>
            <ta e="T150" id="Seg_9805" s="T149">adv:Time</ta>
            <ta e="T152" id="Seg_9806" s="T151">np.h:A</ta>
            <ta e="T154" id="Seg_9807" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_9808" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_9809" s="T155">np:Th</ta>
            <ta e="T157" id="Seg_9810" s="T156">0.3.h:A</ta>
            <ta e="T158" id="Seg_9811" s="T157">0.3.h:A</ta>
            <ta e="T160" id="Seg_9812" s="T159">0.3.h:A</ta>
            <ta e="T162" id="Seg_9813" s="T161">np:A</ta>
            <ta e="T164" id="Seg_9814" s="T163">np:Th</ta>
            <ta e="T165" id="Seg_9815" s="T164">0.3.h:A</ta>
            <ta e="T167" id="Seg_9816" s="T166">0.3.h:E</ta>
            <ta e="T168" id="Seg_9817" s="T167">adv:Time</ta>
            <ta e="T169" id="Seg_9818" s="T168">0.3.h:A</ta>
            <ta e="T170" id="Seg_9819" s="T169">0.3.h:E</ta>
            <ta e="T171" id="Seg_9820" s="T170">np:Th</ta>
            <ta e="T173" id="Seg_9821" s="T172">0.3.h:A</ta>
            <ta e="T174" id="Seg_9822" s="T173">0.3.h:A</ta>
            <ta e="T176" id="Seg_9823" s="T175">np:Th</ta>
            <ta e="T178" id="Seg_9824" s="T177">0.3.h:E</ta>
            <ta e="T180" id="Seg_9825" s="T179">adv:Time</ta>
            <ta e="T184" id="Seg_9826" s="T183">0.3.h:A</ta>
            <ta e="T185" id="Seg_9827" s="T184">0.3.h:A</ta>
            <ta e="T187" id="Seg_9828" s="T186">0.3.h:E</ta>
            <ta e="T190" id="Seg_9829" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_9830" s="T190">0.3.h:E</ta>
            <ta e="T192" id="Seg_9831" s="T191">adv:Time</ta>
            <ta e="T193" id="Seg_9832" s="T192">np.h:A</ta>
            <ta e="T196" id="Seg_9833" s="T195">0.2.h:E</ta>
            <ta e="T197" id="Seg_9834" s="T196">np:Th</ta>
            <ta e="T200" id="Seg_9835" s="T199">pro.h:Poss</ta>
            <ta e="T201" id="Seg_9836" s="T200">np:P</ta>
            <ta e="T202" id="Seg_9837" s="T201">0.3.h:A</ta>
            <ta e="T204" id="Seg_9838" s="T203">pro:P</ta>
            <ta e="T205" id="Seg_9839" s="T204">pro.h:A</ta>
            <ta e="T211" id="Seg_9840" s="T210">pro.h:A</ta>
            <ta e="T214" id="Seg_9841" s="T213">0.1.h:A</ta>
            <ta e="T216" id="Seg_9842" s="T215">np:Th</ta>
            <ta e="T219" id="Seg_9843" s="T218">np.h:A</ta>
            <ta e="T221" id="Seg_9844" s="T220">0.2.h:A</ta>
            <ta e="T222" id="Seg_9845" s="T221">np:Th</ta>
            <ta e="T225" id="Seg_9846" s="T224">adv:Time</ta>
            <ta e="T226" id="Seg_9847" s="T225">pro.h:A</ta>
            <ta e="T228" id="Seg_9848" s="T227">pro.h:A</ta>
            <ta e="T230" id="Seg_9849" s="T229">np:Ins 0.2.h:Poss</ta>
            <ta e="T233" id="Seg_9850" s="T232">n:Time</ta>
            <ta e="T234" id="Seg_9851" s="T233">adv:L</ta>
            <ta e="T237" id="Seg_9852" s="T236">0.2.h:A</ta>
            <ta e="T239" id="Seg_9853" s="T238">pro:G</ta>
            <ta e="T240" id="Seg_9854" s="T239">pro.h:A</ta>
            <ta e="T241" id="Seg_9855" s="T240">pro.h:Th</ta>
            <ta e="T243" id="Seg_9856" s="T242">0.3.h:A</ta>
            <ta e="T244" id="Seg_9857" s="T243">adv:Time</ta>
            <ta e="T245" id="Seg_9858" s="T244">pro.h:Th</ta>
            <ta e="T246" id="Seg_9859" s="T245">0.3.h:A</ta>
            <ta e="T247" id="Seg_9860" s="T246">0.2.h:A</ta>
            <ta e="T248" id="Seg_9861" s="T247">adv:Time</ta>
            <ta e="T250" id="Seg_9862" s="T249">np.h:E</ta>
            <ta e="T252" id="Seg_9863" s="T251">0.2.h:A</ta>
            <ta e="T254" id="Seg_9864" s="T253">np:Th</ta>
            <ta e="T256" id="Seg_9865" s="T255">np:Th</ta>
            <ta e="T257" id="Seg_9866" s="T256">0.2.h:A</ta>
            <ta e="T261" id="Seg_9867" s="T260">adv:Time</ta>
            <ta e="T262" id="Seg_9868" s="T261">pro.h:A</ta>
            <ta e="T264" id="Seg_9869" s="T263">0.3.h:A</ta>
            <ta e="T265" id="Seg_9870" s="T264">np:Th</ta>
            <ta e="T266" id="Seg_9871" s="T265">0.3.h:A</ta>
            <ta e="T267" id="Seg_9872" s="T266">np:Th</ta>
            <ta e="T272" id="Seg_9873" s="T271">pro:Th</ta>
            <ta e="T273" id="Seg_9874" s="T272">0.3.h:A</ta>
            <ta e="T275" id="Seg_9875" s="T274">0.3:A</ta>
            <ta e="T277" id="Seg_9876" s="T276">pro.h:Th</ta>
            <ta e="T278" id="Seg_9877" s="T277">0.3:A</ta>
            <ta e="T282" id="Seg_9878" s="T281">0.3:A</ta>
            <ta e="T283" id="Seg_9879" s="T282">np:G</ta>
            <ta e="T285" id="Seg_9880" s="T284">adv:Time</ta>
            <ta e="T286" id="Seg_9881" s="T285">pro.h:A</ta>
            <ta e="T288" id="Seg_9882" s="T287">np:G</ta>
            <ta e="T289" id="Seg_9883" s="T288">pro.h:A</ta>
            <ta e="T292" id="Seg_9884" s="T291">adv:L</ta>
            <ta e="T295" id="Seg_9885" s="T294">pro.h:Poss</ta>
            <ta e="T296" id="Seg_9886" s="T295">pro.h:Th</ta>
            <ta e="T297" id="Seg_9887" s="T296">np.h:Th</ta>
            <ta e="T298" id="Seg_9888" s="T297">pro.h:Th</ta>
            <ta e="T299" id="Seg_9889" s="T298">np.h:Th</ta>
            <ta e="T301" id="Seg_9890" s="T300">pro.h:A</ta>
            <ta e="T304" id="Seg_9891" s="T303">adv:Time</ta>
            <ta e="T305" id="Seg_9892" s="T304">pro.h:A</ta>
            <ta e="T308" id="Seg_9893" s="T307">pro:A</ta>
            <ta e="T309" id="Seg_9894" s="T308">pro:G</ta>
            <ta e="T313" id="Seg_9895" s="T312">0.3:A</ta>
            <ta e="T315" id="Seg_9896" s="T314">pro.h:A</ta>
            <ta e="T319" id="Seg_9897" s="T318">pro.h:A</ta>
            <ta e="T321" id="Seg_9898" s="T320">pro.h:R</ta>
            <ta e="T326" id="Seg_9899" s="T325">0.2.h:A</ta>
            <ta e="T328" id="Seg_9900" s="T327">pro.h:A</ta>
            <ta e="T329" id="Seg_9901" s="T328">pro.h:B</ta>
            <ta e="T330" id="Seg_9902" s="T329">pro:Th</ta>
            <ta e="T333" id="Seg_9903" s="T332">0.2.h:A</ta>
            <ta e="T334" id="Seg_9904" s="T333">np:Th</ta>
            <ta e="T335" id="Seg_9905" s="T334">0.2.h:A</ta>
            <ta e="T338" id="Seg_9906" s="T337">adv:Time</ta>
            <ta e="T339" id="Seg_9907" s="T338">pro.h:A</ta>
            <ta e="T341" id="Seg_9908" s="T340">np:G</ta>
            <ta e="T342" id="Seg_9909" s="T341">np.h:A</ta>
            <ta e="T344" id="Seg_9910" s="T343">pro.h:A</ta>
            <ta e="T346" id="Seg_9911" s="T345">pro.h:R</ta>
            <ta e="T347" id="Seg_9912" s="T346">0.2.h:A</ta>
            <ta e="T350" id="Seg_9913" s="T349">pro.h:A</ta>
            <ta e="T353" id="Seg_9914" s="T352">adv:Time</ta>
            <ta e="T355" id="Seg_9915" s="T354">0.2.h:A</ta>
            <ta e="T356" id="Seg_9916" s="T355">pro:G</ta>
            <ta e="T357" id="Seg_9917" s="T356">0.1.h:A</ta>
            <ta e="T358" id="Seg_9918" s="T357">0.3.h:A</ta>
            <ta e="T360" id="Seg_9919" s="T359">0.3.h:A</ta>
            <ta e="T361" id="Seg_9920" s="T360">adv:L</ta>
            <ta e="T362" id="Seg_9921" s="T361">adv:Time</ta>
            <ta e="T364" id="Seg_9922" s="T363">0.2.h:A</ta>
            <ta e="T366" id="Seg_9923" s="T365">0.2.h:A</ta>
            <ta e="T367" id="Seg_9924" s="T366">np:Th</ta>
            <ta e="T369" id="Seg_9925" s="T368">np:Th</ta>
            <ta e="T376" id="Seg_9926" s="T375">0.2.h:A</ta>
            <ta e="T382" id="Seg_9927" s="T381">pro.h:A</ta>
            <ta e="T384" id="Seg_9928" s="T383">np:Th</ta>
            <ta e="T385" id="Seg_9929" s="T384">0.3.h:A</ta>
            <ta e="T387" id="Seg_9930" s="T386">np:Th</ta>
            <ta e="T391" id="Seg_9931" s="T390">np:L</ta>
            <ta e="T395" id="Seg_9932" s="T394">0.3.h:A</ta>
            <ta e="T396" id="Seg_9933" s="T395">pro:Th</ta>
            <ta e="T397" id="Seg_9934" s="T396">pro:A</ta>
            <ta e="T400" id="Seg_9935" s="T399">np.h:A</ta>
            <ta e="T402" id="Seg_9936" s="T401">pro.h:Th</ta>
            <ta e="T403" id="Seg_9937" s="T402">0.3.h:A</ta>
            <ta e="T405" id="Seg_9938" s="T404">0.3.h:A</ta>
            <ta e="T406" id="Seg_9939" s="T405">np:G</ta>
            <ta e="T408" id="Seg_9940" s="T407">0.2.h:A</ta>
            <ta e="T409" id="Seg_9941" s="T408">np:Th</ta>
            <ta e="T411" id="Seg_9942" s="T410">pro.h:Poss np.h:Th</ta>
            <ta e="T412" id="Seg_9943" s="T411">pro.h:Th</ta>
            <ta e="T414" id="Seg_9944" s="T413">pro.h:Th</ta>
            <ta e="T415" id="Seg_9945" s="T414">np.h:Th</ta>
            <ta e="T419" id="Seg_9946" s="T418">0.2.h:A</ta>
            <ta e="T421" id="Seg_9947" s="T420">0.2.h:A</ta>
            <ta e="T422" id="Seg_9948" s="T421">pro.h:A</ta>
            <ta e="T423" id="Seg_9949" s="T422">pro.h:B</ta>
            <ta e="T424" id="Seg_9950" s="T423">pro:Th</ta>
            <ta e="T427" id="Seg_9951" s="T426">adv:L</ta>
            <ta e="T429" id="Seg_9952" s="T428">np.h:Th</ta>
            <ta e="T432" id="Seg_9953" s="T431">0.2.h:A</ta>
            <ta e="T433" id="Seg_9954" s="T432">pro:G</ta>
            <ta e="T434" id="Seg_9955" s="T433">pro.h:Th</ta>
            <ta e="T435" id="Seg_9956" s="T434">adv:Time</ta>
            <ta e="T436" id="Seg_9957" s="T435">pro.h:A</ta>
            <ta e="T440" id="Seg_9958" s="T439">np.h:A</ta>
            <ta e="T442" id="Seg_9959" s="T441">0.2.h:A</ta>
            <ta e="T445" id="Seg_9960" s="T444">pro.h:A</ta>
            <ta e="T451" id="Seg_9961" s="T450">pro.h:A</ta>
            <ta e="T454" id="Seg_9962" s="T453">0.2.h:A</ta>
            <ta e="T455" id="Seg_9963" s="T454">0.1.h:A</ta>
            <ta e="T456" id="Seg_9964" s="T455">0.3.h:A</ta>
            <ta e="T457" id="Seg_9965" s="T456">adv:L</ta>
            <ta e="T458" id="Seg_9966" s="T457">adv:Time</ta>
            <ta e="T459" id="Seg_9967" s="T458">np.h:A</ta>
            <ta e="T461" id="Seg_9968" s="T460">pro.h:R</ta>
            <ta e="T462" id="Seg_9969" s="T461">pro.h:A</ta>
            <ta e="T465" id="Seg_9970" s="T464">0.2.h:A</ta>
            <ta e="T466" id="Seg_9971" s="T465">0.2.h:A</ta>
            <ta e="T468" id="Seg_9972" s="T467">pro.h:A</ta>
            <ta e="T471" id="Seg_9973" s="T470">adv:Time</ta>
            <ta e="T472" id="Seg_9974" s="T471">0.3.h:A</ta>
            <ta e="T473" id="Seg_9975" s="T472">np:G</ta>
            <ta e="T474" id="Seg_9976" s="T473">adv:L</ta>
            <ta e="T475" id="Seg_9977" s="T474">0.3.h:A</ta>
            <ta e="T476" id="Seg_9978" s="T475">pro.h:A</ta>
            <ta e="T481" id="Seg_9979" s="T480">np.h:Th</ta>
            <ta e="T483" id="Seg_9980" s="T482">pro.h:A</ta>
            <ta e="T484" id="Seg_9981" s="T483">pro.h:Th</ta>
            <ta e="T487" id="Seg_9982" s="T486">0.3.h:A</ta>
            <ta e="T489" id="Seg_9983" s="T488">0.3.h:A</ta>
            <ta e="T491" id="Seg_9984" s="T490">0.3.h:A</ta>
            <ta e="T492" id="Seg_9985" s="T491">np.h:Th</ta>
            <ta e="T493" id="Seg_9986" s="T492">0.2.h:A</ta>
            <ta e="T494" id="Seg_9987" s="T493">0.3.h:A</ta>
            <ta e="T495" id="Seg_9988" s="T494">adv:Time</ta>
            <ta e="T496" id="Seg_9989" s="T495">0.3.h:A</ta>
            <ta e="T497" id="Seg_9990" s="T496">adv:L</ta>
            <ta e="T500" id="Seg_9991" s="T499">np:Th</ta>
            <ta e="T501" id="Seg_9992" s="T500">0.3.h:A</ta>
            <ta e="T502" id="Seg_9993" s="T501">adv:Time</ta>
            <ta e="T503" id="Seg_9994" s="T502">pro.h:A</ta>
            <ta e="T506" id="Seg_9995" s="T505">pro.h:E</ta>
            <ta e="T511" id="Seg_9996" s="T510">pro.h:E</ta>
            <ta e="T512" id="Seg_9997" s="T511">np.h:B</ta>
            <ta e="T514" id="Seg_9998" s="T513">np.h:A</ta>
            <ta e="T517" id="Seg_9999" s="T516">pro.h:E</ta>
            <ta e="T518" id="Seg_10000" s="T517">adv:L</ta>
            <ta e="T520" id="Seg_10001" s="T519">pro.h:Th</ta>
            <ta e="T523" id="Seg_10002" s="T522">np.h:Th</ta>
            <ta e="T524" id="Seg_10003" s="T523">pro.h:E</ta>
            <ta e="T529" id="Seg_10004" s="T528">0.1.h:A</ta>
            <ta e="T533" id="Seg_10005" s="T532">pro.h:A</ta>
            <ta e="T536" id="Seg_10006" s="T535">pro:G</ta>
            <ta e="T537" id="Seg_10007" s="T536">adv:Time</ta>
            <ta e="T539" id="Seg_10008" s="T538">np:G</ta>
            <ta e="T540" id="Seg_10009" s="T539">0.3.h:A</ta>
            <ta e="T541" id="Seg_10010" s="T540">pro.h:A</ta>
            <ta e="T542" id="Seg_10011" s="T541">pro.h:Th</ta>
            <ta e="T546" id="Seg_10012" s="T545">pro.h:A</ta>
            <ta e="T549" id="Seg_10013" s="T548">np.h:Th</ta>
            <ta e="T551" id="Seg_10014" s="T550">np:P</ta>
            <ta e="T552" id="Seg_10015" s="T551">0.3.h:A</ta>
            <ta e="T553" id="Seg_10016" s="T552">0.3.h:A</ta>
            <ta e="T554" id="Seg_10017" s="T553">np:Th</ta>
            <ta e="T555" id="Seg_10018" s="T554">0.3.h:A</ta>
            <ta e="T556" id="Seg_10019" s="T555">pro.h:A</ta>
            <ta e="T557" id="Seg_10020" s="T556">np:Th</ta>
            <ta e="T559" id="Seg_10021" s="T558">np.h:Th</ta>
            <ta e="T560" id="Seg_10022" s="T559">0.3.h:A</ta>
            <ta e="T561" id="Seg_10023" s="T560">pro.h:Th</ta>
            <ta e="T562" id="Seg_10024" s="T561">0.3.h:A</ta>
            <ta e="T563" id="Seg_10025" s="T562">0.3.h:A</ta>
            <ta e="T565" id="Seg_10026" s="T564">n:Time</ta>
            <ta e="T567" id="Seg_10027" s="T566">0.3.h:A</ta>
            <ta e="T572" id="Seg_10028" s="T571">np.h:A</ta>
            <ta e="T575" id="Seg_10029" s="T574">np.h:Th</ta>
            <ta e="T577" id="Seg_10030" s="T576">pro.h:E</ta>
            <ta e="T580" id="Seg_10031" s="T579">0.3.h:E</ta>
            <ta e="T582" id="Seg_10032" s="T581">pro.h:A</ta>
            <ta e="T584" id="Seg_10033" s="T583">adv:Time</ta>
            <ta e="T585" id="Seg_10034" s="T584">0.3.h:A</ta>
            <ta e="T586" id="Seg_10035" s="T585">np.h:Th</ta>
            <ta e="T587" id="Seg_10036" s="T586">np.h:Th</ta>
            <ta e="T589" id="Seg_10037" s="T588">pro.h:E</ta>
            <ta e="T590" id="Seg_10038" s="T589">adv:L</ta>
            <ta e="T596" id="Seg_10039" s="T595">np:Th</ta>
            <ta e="T599" id="Seg_10040" s="T598">pro.h:A</ta>
            <ta e="T601" id="Seg_10041" s="T600">0.2.h:A</ta>
            <ta e="T602" id="Seg_10042" s="T601">np:Th</ta>
            <ta e="T604" id="Seg_10043" s="T603">np.h:Th</ta>
            <ta e="T605" id="Seg_10044" s="T604">adv:Time</ta>
            <ta e="T606" id="Seg_10045" s="T605">pro.h:A</ta>
            <ta e="T608" id="Seg_10046" s="T607">np:Th</ta>
            <ta e="T609" id="Seg_10047" s="T608">pro.h:A</ta>
            <ta e="T610" id="Seg_10048" s="T609">pro.h:Th</ta>
            <ta e="T612" id="Seg_10049" s="T611">pro.h:A</ta>
            <ta e="T614" id="Seg_10050" s="T613">np:Th</ta>
            <ta e="T620" id="Seg_10051" s="T619">0.3.h:A</ta>
            <ta e="T621" id="Seg_10052" s="T620">0.3.h:A</ta>
            <ta e="T623" id="Seg_10053" s="T622">np.h:Th</ta>
            <ta e="T624" id="Seg_10054" s="T623">0.3.h:A</ta>
            <ta e="T625" id="Seg_10055" s="T624">np:G</ta>
            <ta e="T626" id="Seg_10056" s="T625">pro.h:Th</ta>
            <ta e="T627" id="Seg_10057" s="T626">0.3.h:A</ta>
            <ta e="T628" id="Seg_10058" s="T627">0.3.h:A</ta>
            <ta e="T629" id="Seg_10059" s="T628">adv:Time</ta>
            <ta e="T631" id="Seg_10060" s="T630">np.h:A</ta>
            <ta e="T633" id="Seg_10061" s="T632">0.2.h:A</ta>
            <ta e="T635" id="Seg_10062" s="T634">np:Th</ta>
            <ta e="T637" id="Seg_10063" s="T636">np.h:A</ta>
            <ta e="T639" id="Seg_10064" s="T638">np.h:Th</ta>
            <ta e="T640" id="Seg_10065" s="T639">0.3.h:A</ta>
            <ta e="T641" id="Seg_10066" s="T640">pro.h:E</ta>
            <ta e="T644" id="Seg_10067" s="T643">0.3.h:E</ta>
            <ta e="T646" id="Seg_10068" s="T645">pro.h:A</ta>
            <ta e="T648" id="Seg_10069" s="T647">0.3.h:A</ta>
            <ta e="T649" id="Seg_10070" s="T648">np.h:Th</ta>
            <ta e="T650" id="Seg_10071" s="T649">0.3.h:A</ta>
            <ta e="T652" id="Seg_10072" s="T651">adv:Time</ta>
            <ta e="T653" id="Seg_10073" s="T652">pro.h:A</ta>
            <ta e="T655" id="Seg_10074" s="T654">np.h:Com</ta>
            <ta e="T657" id="Seg_10075" s="T656">0.1.h:Th</ta>
            <ta e="T658" id="Seg_10076" s="T657">adv:L</ta>
            <ta e="T659" id="Seg_10077" s="T658">pro.h:A</ta>
            <ta e="T661" id="Seg_10078" s="T660">pro.h:B</ta>
            <ta e="T663" id="Seg_10079" s="T662">np:G</ta>
            <ta e="T664" id="Seg_10080" s="T663">0.3.h:A</ta>
            <ta e="T666" id="Seg_10081" s="T665">adv:Time</ta>
            <ta e="T668" id="Seg_10082" s="T667">pro.h:A</ta>
            <ta e="T670" id="Seg_10083" s="T669">pro.h:Poss</ta>
            <ta e="T672" id="Seg_10084" s="T671">np:G</ta>
            <ta e="T673" id="Seg_10085" s="T672">0.3.h:A</ta>
            <ta e="T675" id="Seg_10086" s="T674">0.1.h:A</ta>
            <ta e="T676" id="Seg_10087" s="T675">0.3.h:A</ta>
            <ta e="T677" id="Seg_10088" s="T676">0.3.h:A</ta>
            <ta e="T680" id="Seg_10089" s="T679">0.3.h:A</ta>
            <ta e="T682" id="Seg_10090" s="T681">adv:Time</ta>
            <ta e="T683" id="Seg_10091" s="T682">np.h:A</ta>
            <ta e="T685" id="Seg_10092" s="T684">pro.h:P</ta>
            <ta e="T686" id="Seg_10093" s="T685">0.3.h:A</ta>
            <ta e="T689" id="Seg_10094" s="T688">0.3.h:A</ta>
            <ta e="T690" id="Seg_10095" s="T689">np.h:Th</ta>
            <ta e="T692" id="Seg_10096" s="T691">np:Th</ta>
            <ta e="T694" id="Seg_10097" s="T693">np:Th</ta>
            <ta e="T695" id="Seg_10098" s="T694">0.3.h:A</ta>
            <ta e="T696" id="Seg_10099" s="T695">0.3.h:A</ta>
            <ta e="T697" id="Seg_10100" s="T696">adv:Time</ta>
            <ta e="T698" id="Seg_10101" s="T697">np.h:A</ta>
            <ta e="T701" id="Seg_10102" s="T700">0.3.h:E</ta>
            <ta e="T702" id="Seg_10103" s="T701">pro.h:E</ta>
            <ta e="T704" id="Seg_10104" s="T703">0.3.h:A</ta>
            <ta e="T705" id="Seg_10105" s="T704">np:Th</ta>
            <ta e="T708" id="Seg_10106" s="T707">np:Th</ta>
            <ta e="T710" id="Seg_10107" s="T709">0.2.h:A</ta>
            <ta e="T712" id="Seg_10108" s="T711">np:Th</ta>
            <ta e="T715" id="Seg_10109" s="T714">np:Th</ta>
            <ta e="T717" id="Seg_10110" s="T716">np:A</ta>
            <ta e="T719" id="Seg_10111" s="T718">0.3:A</ta>
            <ta e="T720" id="Seg_10112" s="T719">pro.h:A</ta>
            <ta e="T721" id="Seg_10113" s="T720">pro.h:P</ta>
            <ta e="T725" id="Seg_10114" s="T724">np:Ins</ta>
            <ta e="T727" id="Seg_10115" s="T726">adv:Time</ta>
            <ta e="T729" id="Seg_10116" s="T728">np:Ins</ta>
            <ta e="T730" id="Seg_10117" s="T729">pro.h:A</ta>
            <ta e="T733" id="Seg_10118" s="T732">pro.h:E</ta>
            <ta e="T736" id="Seg_10119" s="T735">0.2.h:E</ta>
            <ta e="T740" id="Seg_10120" s="T739">pro.h:Th</ta>
            <ta e="T742" id="Seg_10121" s="T741">pro.h:E</ta>
            <ta e="T746" id="Seg_10122" s="T745">pro.h:P</ta>
            <ta e="T748" id="Seg_10123" s="T747">pro.h:Poss</ta>
            <ta e="T750" id="Seg_10124" s="T749">np.h:A</ta>
            <ta e="T752" id="Seg_10125" s="T751">np:Th</ta>
            <ta e="T753" id="Seg_10126" s="T752">0.3.h:A</ta>
            <ta e="T754" id="Seg_10127" s="T753">np.h:Th</ta>
            <ta e="T756" id="Seg_10128" s="T755">np:Th</ta>
            <ta e="T758" id="Seg_10129" s="T757">np:Th</ta>
            <ta e="T759" id="Seg_10130" s="T758">adv:Time</ta>
            <ta e="T760" id="Seg_10131" s="T759">0.2.h:A</ta>
            <ta e="T761" id="Seg_10132" s="T760">pro:G</ta>
            <ta e="T762" id="Seg_10133" s="T761">0.3.h:A</ta>
            <ta e="T763" id="Seg_10134" s="T762">pro.h:A</ta>
            <ta e="T765" id="Seg_10135" s="T764">pro.h:Th</ta>
            <ta e="T767" id="Seg_10136" s="T766">np.h:A</ta>
            <ta e="T768" id="Seg_10137" s="T767">np.h:P</ta>
            <ta e="T771" id="Seg_10138" s="T770">pro.h:A</ta>
            <ta e="T773" id="Seg_10139" s="T772">np.h:Th</ta>
            <ta e="T776" id="Seg_10140" s="T775">pro:Th</ta>
            <ta e="T777" id="Seg_10141" s="T776">adv:L</ta>
            <ta e="T779" id="Seg_10142" s="T778">adv:Time</ta>
            <ta e="T781" id="Seg_10143" s="T780">np.h:A</ta>
            <ta e="T782" id="Seg_10144" s="T781">pro.h:B</ta>
            <ta e="T784" id="Seg_10145" s="T783">np:G</ta>
            <ta e="T786" id="Seg_10146" s="T785">0.2.h:A</ta>
            <ta e="T787" id="Seg_10147" s="T786">np:G</ta>
            <ta e="T788" id="Seg_10148" s="T787">0.3.h:A</ta>
            <ta e="T789" id="Seg_10149" s="T788">np:G</ta>
            <ta e="T790" id="Seg_10150" s="T789">np:Th</ta>
            <ta e="T792" id="Seg_10151" s="T791">0.3.h:A</ta>
            <ta e="T793" id="Seg_10152" s="T792">adv:Time</ta>
            <ta e="T794" id="Seg_10153" s="T793">np.h:E</ta>
            <ta e="T796" id="Seg_10154" s="T795">0.3.h:E</ta>
            <ta e="T797" id="Seg_10155" s="T796">adv:Time</ta>
            <ta e="T798" id="Seg_10156" s="T797">pro.h:A</ta>
            <ta e="T799" id="Seg_10157" s="T798">np.h:Th</ta>
            <ta e="T801" id="Seg_10158" s="T800">0.3.h:A</ta>
            <ta e="T802" id="Seg_10159" s="T801">np:P</ta>
            <ta e="T803" id="Seg_10160" s="T802">0.3.h:A</ta>
            <ta e="T805" id="Seg_10161" s="T804">adv:Time</ta>
            <ta e="T807" id="Seg_10162" s="T806">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_10163" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_10164" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_10165" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_10166" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_10167" s="T8">np:S</ta>
            <ta e="T10" id="Seg_10168" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_10169" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_10170" s="T13">pro:S</ta>
            <ta e="T16" id="Seg_10171" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_10172" s="T16">pro.h:S</ta>
            <ta e="T19" id="Seg_10173" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_10174" s="T19">pro:S</ta>
            <ta e="T21" id="Seg_10175" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_10176" s="T21">v:pred 0.3.h:S</ta>
            <ta e="T23" id="Seg_10177" s="T22">np.h:O</ta>
            <ta e="T24" id="Seg_10178" s="T23">s:purp</ta>
            <ta e="T25" id="Seg_10179" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_10180" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_10181" s="T26">v:pred 0.3:S</ta>
            <ta e="T30" id="Seg_10182" s="T29">np.h:S</ta>
            <ta e="T32" id="Seg_10183" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_10184" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_10185" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_10186" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_10187" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_10188" s="T36">v:pred 0.3.h:S</ta>
            <ta e="T38" id="Seg_10189" s="T37">pro:O</ta>
            <ta e="T39" id="Seg_10190" s="T38">ptcl.neg</ta>
            <ta e="T40" id="Seg_10191" s="T39">v:pred 0.3.h:S</ta>
            <ta e="T43" id="Seg_10192" s="T42">np.h:S</ta>
            <ta e="T45" id="Seg_10193" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_10194" s="T46">v:pred 0.3.h:S</ta>
            <ta e="T48" id="Seg_10195" s="T47">pro:O</ta>
            <ta e="T49" id="Seg_10196" s="T48">ptcl.neg</ta>
            <ta e="T50" id="Seg_10197" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_10198" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_10199" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_10200" s="T53">ptcl.neg</ta>
            <ta e="T55" id="Seg_10201" s="T54">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_10202" s="T56">ptcl:pred</ta>
            <ta e="T58" id="Seg_10203" s="T57">pro.h:S</ta>
            <ta e="T61" id="Seg_10204" s="T60">np:O</ta>
            <ta e="T62" id="Seg_10205" s="T61">v:pred</ta>
            <ta e="T65" id="Seg_10206" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T67" id="Seg_10207" s="T66">v:pred 0.3.h:S</ta>
            <ta e="T68" id="Seg_10208" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T70" id="Seg_10209" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_10210" s="T70">np:S</ta>
            <ta e="T72" id="Seg_10211" s="T71">pro.h:S</ta>
            <ta e="T74" id="Seg_10212" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_10213" s="T75">np:S</ta>
            <ta e="T77" id="Seg_10214" s="T76">v:pred</ta>
            <ta e="T81" id="Seg_10215" s="T80">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_10216" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_10217" s="T83">np:O</ta>
            <ta e="T86" id="Seg_10218" s="T85">v:pred 0.3.h:S</ta>
            <ta e="T88" id="Seg_10219" s="T87">v:pred 0.3.h:S</ta>
            <ta e="T94" id="Seg_10220" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_10221" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_10222" s="T95">pro:S</ta>
            <ta e="T97" id="Seg_10223" s="T96">v:pred</ta>
            <ta e="T100" id="Seg_10224" s="T99">v:pred 0.1.h:S</ta>
            <ta e="T101" id="Seg_10225" s="T100">np:O</ta>
            <ta e="T102" id="Seg_10226" s="T101">pro:S</ta>
            <ta e="T106" id="Seg_10227" s="T105">v:pred</ta>
            <ta e="T108" id="Seg_10228" s="T107">v:pred 0.3:S</ta>
            <ta e="T110" id="Seg_10229" s="T109">np.h:S</ta>
            <ta e="T112" id="Seg_10230" s="T111">v:pred</ta>
            <ta e="T114" id="Seg_10231" s="T113">v:pred 0.3.h:S</ta>
            <ta e="T115" id="Seg_10232" s="T114">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_10233" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T121" id="Seg_10234" s="T120">v:pred 0.2.h:S</ta>
            <ta e="T131" id="Seg_10235" s="T130">pro.h:S</ta>
            <ta e="T132" id="Seg_10236" s="T131">np:O</ta>
            <ta e="T136" id="Seg_10237" s="T135">v:pred</ta>
            <ta e="T139" id="Seg_10238" s="T138">pro.h:S</ta>
            <ta e="T141" id="Seg_10239" s="T140">v:pred</ta>
            <ta e="T142" id="Seg_10240" s="T141">np.h:S</ta>
            <ta e="T144" id="Seg_10241" s="T143">np.h:S</ta>
            <ta e="T146" id="Seg_10242" s="T145">np.h:S</ta>
            <ta e="T148" id="Seg_10243" s="T147">v:pred</ta>
            <ta e="T152" id="Seg_10244" s="T151">np.h:S</ta>
            <ta e="T153" id="Seg_10245" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_10246" s="T153">v:pred 0.3.h:S</ta>
            <ta e="T155" id="Seg_10247" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T156" id="Seg_10248" s="T155">np:O</ta>
            <ta e="T157" id="Seg_10249" s="T156">v:pred 0.3.h:S</ta>
            <ta e="T158" id="Seg_10250" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_10251" s="T159">v:pred 0.3.h:S</ta>
            <ta e="T161" id="Seg_10252" s="T160">s:purp</ta>
            <ta e="T162" id="Seg_10253" s="T161">np:S</ta>
            <ta e="T163" id="Seg_10254" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_10255" s="T163">np:O</ta>
            <ta e="T165" id="Seg_10256" s="T164">v:pred 0.3.h:S</ta>
            <ta e="T167" id="Seg_10257" s="T166">v:pred 0.3.h:S</ta>
            <ta e="T169" id="Seg_10258" s="T168">v:pred 0.3.h:S</ta>
            <ta e="T170" id="Seg_10259" s="T169">v:pred 0.3.h:S</ta>
            <ta e="T171" id="Seg_10260" s="T170">np:S</ta>
            <ta e="T172" id="Seg_10261" s="T171">v:pred</ta>
            <ta e="T173" id="Seg_10262" s="T172">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_10263" s="T173">v:pred 0.3.h:S</ta>
            <ta e="T176" id="Seg_10264" s="T175">np:S</ta>
            <ta e="T177" id="Seg_10265" s="T176">v:pred</ta>
            <ta e="T178" id="Seg_10266" s="T177">v:pred 0.3.h:S</ta>
            <ta e="T181" id="Seg_10267" s="T180">ptcl:pred</ta>
            <ta e="T184" id="Seg_10268" s="T183">v:pred 0.3.h:S</ta>
            <ta e="T185" id="Seg_10269" s="T184">v:pred 0.3.h:S</ta>
            <ta e="T187" id="Seg_10270" s="T186">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_10271" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_10272" s="T190">v:pred 0.3.h:S</ta>
            <ta e="T193" id="Seg_10273" s="T192">np.h:S</ta>
            <ta e="T194" id="Seg_10274" s="T193">v:pred</ta>
            <ta e="T196" id="Seg_10275" s="T195">v:pred 0.2.h:S</ta>
            <ta e="T197" id="Seg_10276" s="T196">np:S</ta>
            <ta e="T198" id="Seg_10277" s="T197">v:pred</ta>
            <ta e="T201" id="Seg_10278" s="T200">np:O</ta>
            <ta e="T202" id="Seg_10279" s="T201">v:pred 0.3.h:S</ta>
            <ta e="T204" id="Seg_10280" s="T203">pro:O</ta>
            <ta e="T205" id="Seg_10281" s="T204">pro.h:S</ta>
            <ta e="T206" id="Seg_10282" s="T205">v:pred</ta>
            <ta e="T211" id="Seg_10283" s="T210">pro.h:S</ta>
            <ta e="T212" id="Seg_10284" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_10285" s="T213">v:pred 0.1.h:S</ta>
            <ta e="T216" id="Seg_10286" s="T215">np:O</ta>
            <ta e="T218" id="Seg_10287" s="T217">s:purp</ta>
            <ta e="T219" id="Seg_10288" s="T218">np.h:S</ta>
            <ta e="T220" id="Seg_10289" s="T219">v:pred</ta>
            <ta e="T221" id="Seg_10290" s="T220">v:pred 0.2.h:S</ta>
            <ta e="T222" id="Seg_10291" s="T221">np:O</ta>
            <ta e="T226" id="Seg_10292" s="T225">pro.h:S</ta>
            <ta e="T228" id="Seg_10293" s="T227">pro.h:S</ta>
            <ta e="T235" id="Seg_10294" s="T234">ptcl.neg</ta>
            <ta e="T236" id="Seg_10295" s="T235">v:pred </ta>
            <ta e="T237" id="Seg_10296" s="T236">v:pred 0.2.h:S</ta>
            <ta e="T240" id="Seg_10297" s="T239">pro.h:S</ta>
            <ta e="T241" id="Seg_10298" s="T240">pro.h:O</ta>
            <ta e="T242" id="Seg_10299" s="T241">v:pred</ta>
            <ta e="T243" id="Seg_10300" s="T242">v:pred 0.3.h:S</ta>
            <ta e="T245" id="Seg_10301" s="T244">pro.h:O</ta>
            <ta e="T246" id="Seg_10302" s="T245">v:pred 0.3.h:S</ta>
            <ta e="T247" id="Seg_10303" s="T246">v:pred 0.2.h:S</ta>
            <ta e="T250" id="Seg_10304" s="T249">np.h:S</ta>
            <ta e="T251" id="Seg_10305" s="T250">v:pred</ta>
            <ta e="T252" id="Seg_10306" s="T251">v:pred 0.2.h:S</ta>
            <ta e="T254" id="Seg_10307" s="T253">np:O</ta>
            <ta e="T256" id="Seg_10308" s="T255">np:O</ta>
            <ta e="T257" id="Seg_10309" s="T256">v:pred 0.2.h:S</ta>
            <ta e="T262" id="Seg_10310" s="T261">pro.h:S</ta>
            <ta e="T263" id="Seg_10311" s="T262">v:pred</ta>
            <ta e="T264" id="Seg_10312" s="T263">v:pred 0.3.h:S</ta>
            <ta e="T265" id="Seg_10313" s="T264">np:O</ta>
            <ta e="T266" id="Seg_10314" s="T265">v:pred 0.3.h:S</ta>
            <ta e="T267" id="Seg_10315" s="T266">np:S</ta>
            <ta e="T268" id="Seg_10316" s="T267">adj:pred</ta>
            <ta e="T269" id="Seg_10317" s="T268">ptcl:pred</ta>
            <ta e="T272" id="Seg_10318" s="T271">pro:O</ta>
            <ta e="T273" id="Seg_10319" s="T272">v:pred 0.3.h:S</ta>
            <ta e="T275" id="Seg_10320" s="T274">v:pred 0.3:S</ta>
            <ta e="T277" id="Seg_10321" s="T276">pro.h:O</ta>
            <ta e="T278" id="Seg_10322" s="T277">v:pred 0.3:S</ta>
            <ta e="T282" id="Seg_10323" s="T281">v:pred 0.3:S</ta>
            <ta e="T286" id="Seg_10324" s="T285">pro.h:S</ta>
            <ta e="T287" id="Seg_10325" s="T286">v:pred</ta>
            <ta e="T289" id="Seg_10326" s="T288">pro.h:S</ta>
            <ta e="T293" id="Seg_10327" s="T292">v:pred</ta>
            <ta e="T294" id="Seg_10328" s="T293">s:purp</ta>
            <ta e="T296" id="Seg_10329" s="T295">pro.h:S</ta>
            <ta e="T297" id="Seg_10330" s="T296">n:pred</ta>
            <ta e="T298" id="Seg_10331" s="T297">pro.h:S</ta>
            <ta e="T299" id="Seg_10332" s="T298">n:pred</ta>
            <ta e="T301" id="Seg_10333" s="T300">pro.h:S</ta>
            <ta e="T305" id="Seg_10334" s="T304">pro.h:S</ta>
            <ta e="T306" id="Seg_10335" s="T305">v:pred</ta>
            <ta e="T308" id="Seg_10336" s="T307">pro:S</ta>
            <ta e="T310" id="Seg_10337" s="T309">v:pred</ta>
            <ta e="T313" id="Seg_10338" s="T312">v:pred 0.3:S</ta>
            <ta e="T315" id="Seg_10339" s="T314">pro.h:S</ta>
            <ta e="T316" id="Seg_10340" s="T315">v:pred</ta>
            <ta e="T319" id="Seg_10341" s="T318">pro.h:S</ta>
            <ta e="T322" id="Seg_10342" s="T321">v:pred</ta>
            <ta e="T325" id="Seg_10343" s="T324">s:purp</ta>
            <ta e="T326" id="Seg_10344" s="T325">v:pred 0.2.h:S</ta>
            <ta e="T328" id="Seg_10345" s="T327">pro.h:S</ta>
            <ta e="T330" id="Seg_10346" s="T329">pro:O</ta>
            <ta e="T331" id="Seg_10347" s="T330">ptcl.neg</ta>
            <ta e="T332" id="Seg_10348" s="T331">v:pred</ta>
            <ta e="T333" id="Seg_10349" s="T332">v:pred 0.2.h:S</ta>
            <ta e="T334" id="Seg_10350" s="T333">np:O</ta>
            <ta e="T335" id="Seg_10351" s="T334">v:pred 0.2.h:S</ta>
            <ta e="T339" id="Seg_10352" s="T338">pro.h:S</ta>
            <ta e="T340" id="Seg_10353" s="T339">v:pred</ta>
            <ta e="T342" id="Seg_10354" s="T341">np.h:S</ta>
            <ta e="T343" id="Seg_10355" s="T342">v:pred</ta>
            <ta e="T344" id="Seg_10356" s="T343">pro.h:S</ta>
            <ta e="T345" id="Seg_10357" s="T344">v:pred</ta>
            <ta e="T347" id="Seg_10358" s="T346">v:pred 0.2.h:S</ta>
            <ta e="T350" id="Seg_10359" s="T349">pro.h:S</ta>
            <ta e="T351" id="Seg_10360" s="T350">v:pred</ta>
            <ta e="T355" id="Seg_10361" s="T354">v:pred 0.2.h:S</ta>
            <ta e="T357" id="Seg_10362" s="T356">v:pred 0.1.h:S</ta>
            <ta e="T358" id="Seg_10363" s="T357">v:pred 0.3.h:S</ta>
            <ta e="T360" id="Seg_10364" s="T359">v:pred 0.3.h:S</ta>
            <ta e="T364" id="Seg_10365" s="T363">v:pred 0.2.h:S</ta>
            <ta e="T366" id="Seg_10366" s="T365">v:pred 0.2.h:S</ta>
            <ta e="T367" id="Seg_10367" s="T366">np:O</ta>
            <ta e="T369" id="Seg_10368" s="T368">np:O</ta>
            <ta e="T370" id="Seg_10369" s="T369">ptcl.neg</ta>
            <ta e="T376" id="Seg_10370" s="T375">v:pred 0.2.h:S</ta>
            <ta e="T380" id="Seg_10371" s="T379">ptcl:pred</ta>
            <ta e="T381" id="Seg_10372" s="T380">cop 0.3:S</ta>
            <ta e="T382" id="Seg_10373" s="T381">pro.h:S</ta>
            <ta e="T383" id="Seg_10374" s="T382">v:pred</ta>
            <ta e="T384" id="Seg_10375" s="T383">np:O</ta>
            <ta e="T385" id="Seg_10376" s="T384">v:pred 0.3.h:S</ta>
            <ta e="T387" id="Seg_10377" s="T386">np:S</ta>
            <ta e="T389" id="Seg_10378" s="T388">adj:pred</ta>
            <ta e="T392" id="Seg_10379" s="T391">ptcl:pred</ta>
            <ta e="T395" id="Seg_10380" s="T394">v:pred 0.3.h:S</ta>
            <ta e="T396" id="Seg_10381" s="T395">pro:O</ta>
            <ta e="T397" id="Seg_10382" s="T396">pro:S</ta>
            <ta e="T399" id="Seg_10383" s="T398">v:pred</ta>
            <ta e="T400" id="Seg_10384" s="T399">np.h:S</ta>
            <ta e="T401" id="Seg_10385" s="T400">v:pred</ta>
            <ta e="T402" id="Seg_10386" s="T401">pro.h:O</ta>
            <ta e="T403" id="Seg_10387" s="T402">v:pred 0.3.h:S</ta>
            <ta e="T405" id="Seg_10388" s="T404">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_10389" s="T407"> 0.2.h:S v:pred</ta>
            <ta e="T409" id="Seg_10390" s="T408">np:O</ta>
            <ta e="T410" id="Seg_10391" s="T409">s:purp</ta>
            <ta e="T411" id="Seg_10392" s="T410">n:pred</ta>
            <ta e="T412" id="Seg_10393" s="T411">pro.h:S</ta>
            <ta e="T414" id="Seg_10394" s="T413">pro.h:S</ta>
            <ta e="T415" id="Seg_10395" s="T414">n:pred</ta>
            <ta e="T417" id="Seg_10396" s="T416">s:purp</ta>
            <ta e="T419" id="Seg_10397" s="T418">v:pred 0.2.h:S</ta>
            <ta e="T421" id="Seg_10398" s="T420">v:pred 0.2.h:S</ta>
            <ta e="T422" id="Seg_10399" s="T421">pro.h:S</ta>
            <ta e="T424" id="Seg_10400" s="T423">pro:O</ta>
            <ta e="T425" id="Seg_10401" s="T424">ptcl.neg</ta>
            <ta e="T426" id="Seg_10402" s="T425">v:pred</ta>
            <ta e="T428" id="Seg_10403" s="T427">v:pred</ta>
            <ta e="T429" id="Seg_10404" s="T428">np.h:S</ta>
            <ta e="T432" id="Seg_10405" s="T431">v:pred 0.2.h:S</ta>
            <ta e="T434" id="Seg_10406" s="T433">pro.h:O</ta>
            <ta e="T436" id="Seg_10407" s="T435">pro.h:S</ta>
            <ta e="T438" id="Seg_10408" s="T437">v:pred</ta>
            <ta e="T440" id="Seg_10409" s="T439">np.h:S</ta>
            <ta e="T441" id="Seg_10410" s="T440">v:pred</ta>
            <ta e="T442" id="Seg_10411" s="T441">v:pred 0.2.h:S</ta>
            <ta e="T445" id="Seg_10412" s="T444">pro.h:S</ta>
            <ta e="T447" id="Seg_10413" s="T446">v:pred</ta>
            <ta e="T451" id="Seg_10414" s="T450">pro.h:S</ta>
            <ta e="T452" id="Seg_10415" s="T451">v:pred</ta>
            <ta e="T454" id="Seg_10416" s="T453">v:pred 0.2.h:S</ta>
            <ta e="T455" id="Seg_10417" s="T454">v:pred 0.1.h:S</ta>
            <ta e="T456" id="Seg_10418" s="T455">v:pred 0.3.h:S</ta>
            <ta e="T459" id="Seg_10419" s="T458">np.h:S</ta>
            <ta e="T460" id="Seg_10420" s="T459">v:pred</ta>
            <ta e="T462" id="Seg_10421" s="T461">pro.h:S</ta>
            <ta e="T464" id="Seg_10422" s="T463">v:pred</ta>
            <ta e="T465" id="Seg_10423" s="T464">v:pred 0.2.h:S</ta>
            <ta e="T466" id="Seg_10424" s="T465">v:pred 0.2.h:S</ta>
            <ta e="T468" id="Seg_10425" s="T467">pro.h:S</ta>
            <ta e="T469" id="Seg_10426" s="T468">v:pred</ta>
            <ta e="T472" id="Seg_10427" s="T471">v:pred 0.3.h:S</ta>
            <ta e="T475" id="Seg_10428" s="T474">v:pred 0.3.h:S</ta>
            <ta e="T476" id="Seg_10429" s="T475">pro.h:S</ta>
            <ta e="T477" id="Seg_10430" s="T476">v:pred</ta>
            <ta e="T481" id="Seg_10431" s="T480">np.h:S</ta>
            <ta e="T482" id="Seg_10432" s="T481">v:pred</ta>
            <ta e="T483" id="Seg_10433" s="T482">pro.h:S</ta>
            <ta e="T484" id="Seg_10434" s="T483">pro.h:O</ta>
            <ta e="T485" id="Seg_10435" s="T484">v:pred</ta>
            <ta e="T487" id="Seg_10436" s="T486">v:pred 0.3.h:S</ta>
            <ta e="T489" id="Seg_10437" s="T488">v:pred 0.3.h:S</ta>
            <ta e="T491" id="Seg_10438" s="T490">v:pred 0.3.h:S</ta>
            <ta e="T492" id="Seg_10439" s="T491">np.h:O</ta>
            <ta e="T493" id="Seg_10440" s="T492">v:pred 0.2.h:S</ta>
            <ta e="T494" id="Seg_10441" s="T493">v:pred 0.3.h:S</ta>
            <ta e="T496" id="Seg_10442" s="T495">v:pred 0.3.h:S</ta>
            <ta e="T500" id="Seg_10443" s="T499">np:O</ta>
            <ta e="T501" id="Seg_10444" s="T500">v:pred 0.3.h:S</ta>
            <ta e="T503" id="Seg_10445" s="T502">pro.h:S</ta>
            <ta e="T504" id="Seg_10446" s="T503">v:pred</ta>
            <ta e="T506" id="Seg_10447" s="T505">pro.h:S</ta>
            <ta e="T507" id="Seg_10448" s="T506">v:pred</ta>
            <ta e="T510" id="Seg_10449" s="T509">v:pred</ta>
            <ta e="T511" id="Seg_10450" s="T510">pro.h:S</ta>
            <ta e="T512" id="Seg_10451" s="T511">np.h:O</ta>
            <ta e="T514" id="Seg_10452" s="T513">np.h:S</ta>
            <ta e="T515" id="Seg_10453" s="T514">v:pred</ta>
            <ta e="T517" id="Seg_10454" s="T516">pro.h:S</ta>
            <ta e="T519" id="Seg_10455" s="T518">v:pred</ta>
            <ta e="T520" id="Seg_10456" s="T519">pro.h:S</ta>
            <ta e="T523" id="Seg_10457" s="T522">n:pred</ta>
            <ta e="T524" id="Seg_10458" s="T523">pro.h:S</ta>
            <ta e="T525" id="Seg_10459" s="T524">v:pred</ta>
            <ta e="T529" id="Seg_10460" s="T528">v:pred 0.1.h:S</ta>
            <ta e="T533" id="Seg_10461" s="T532">pro.h:S</ta>
            <ta e="T534" id="Seg_10462" s="T533">v:pred</ta>
            <ta e="T540" id="Seg_10463" s="T539">v:pred 0.3.h:S</ta>
            <ta e="T541" id="Seg_10464" s="T540">pro.h:S</ta>
            <ta e="T542" id="Seg_10465" s="T541">pro.h:O</ta>
            <ta e="T543" id="Seg_10466" s="T542">v:pred</ta>
            <ta e="T546" id="Seg_10467" s="T545">pro.h:S</ta>
            <ta e="T547" id="Seg_10468" s="T546">v:pred</ta>
            <ta e="T549" id="Seg_10469" s="T548">np.h:O</ta>
            <ta e="T551" id="Seg_10470" s="T550">np:O</ta>
            <ta e="T552" id="Seg_10471" s="T551">v:pred 0.3.h:S</ta>
            <ta e="T553" id="Seg_10472" s="T552">v:pred 0.3.h:S</ta>
            <ta e="T554" id="Seg_10473" s="T553">np:O</ta>
            <ta e="T555" id="Seg_10474" s="T554">v:pred 0.3.h:S</ta>
            <ta e="T556" id="Seg_10475" s="T555">pro.h:S</ta>
            <ta e="T557" id="Seg_10476" s="T556">np:O</ta>
            <ta e="T558" id="Seg_10477" s="T557">v:pred</ta>
            <ta e="T559" id="Seg_10478" s="T558">np.h:O</ta>
            <ta e="T560" id="Seg_10479" s="T559">v:pred 0.3.h:S</ta>
            <ta e="T561" id="Seg_10480" s="T560">pro.h:O</ta>
            <ta e="T562" id="Seg_10481" s="T561">v:pred 0.3.h:S</ta>
            <ta e="T563" id="Seg_10482" s="T562">v:pred 0.3.h:S</ta>
            <ta e="T567" id="Seg_10483" s="T566">v:pred 0.3.h:S</ta>
            <ta e="T568" id="Seg_10484" s="T567">s:purp</ta>
            <ta e="T572" id="Seg_10485" s="T571">np.h:S</ta>
            <ta e="T574" id="Seg_10486" s="T573">cop</ta>
            <ta e="T575" id="Seg_10487" s="T574">n:pred</ta>
            <ta e="T577" id="Seg_10488" s="T576">pro.h:S</ta>
            <ta e="T578" id="Seg_10489" s="T577">v:pred</ta>
            <ta e="T580" id="Seg_10490" s="T579">v:pred 0.3.h:S</ta>
            <ta e="T582" id="Seg_10491" s="T581">pro.h:S</ta>
            <ta e="T583" id="Seg_10492" s="T582">v:pred</ta>
            <ta e="T585" id="Seg_10493" s="T584">v:pred 0.3.h:S</ta>
            <ta e="T586" id="Seg_10494" s="T585">np.h:O</ta>
            <ta e="T589" id="Seg_10495" s="T588">pro.h:S</ta>
            <ta e="T591" id="Seg_10496" s="T590">v:pred</ta>
            <ta e="T593" id="Seg_10497" s="T592">ptcl.neg</ta>
            <ta e="T594" id="Seg_10498" s="T593">ptcl:pred</ta>
            <ta e="T596" id="Seg_10499" s="T595">np:O</ta>
            <ta e="T599" id="Seg_10500" s="T598">pro.h:S</ta>
            <ta e="T600" id="Seg_10501" s="T599">v:pred</ta>
            <ta e="T601" id="Seg_10502" s="T600">v:pred 0.2.h:S</ta>
            <ta e="T602" id="Seg_10503" s="T601">np:O</ta>
            <ta e="T604" id="Seg_10504" s="T603">np.h:O</ta>
            <ta e="T606" id="Seg_10505" s="T605">pro.h:S</ta>
            <ta e="T607" id="Seg_10506" s="T606">v:pred</ta>
            <ta e="T608" id="Seg_10507" s="T607">np:O</ta>
            <ta e="T609" id="Seg_10508" s="T608">pro.h:S</ta>
            <ta e="T610" id="Seg_10509" s="T609">pro.h:O</ta>
            <ta e="T611" id="Seg_10510" s="T610">v:pred</ta>
            <ta e="T612" id="Seg_10511" s="T611">pro.h:S</ta>
            <ta e="T613" id="Seg_10512" s="T612">v:pred</ta>
            <ta e="T614" id="Seg_10513" s="T613">np:O</ta>
            <ta e="T620" id="Seg_10514" s="T619">v:pred 0.3.h:S</ta>
            <ta e="T621" id="Seg_10515" s="T620">v:pred 0.3.h:S</ta>
            <ta e="T623" id="Seg_10516" s="T622">np.h:O</ta>
            <ta e="T624" id="Seg_10517" s="T623">v:pred 0.3.h:S</ta>
            <ta e="T626" id="Seg_10518" s="T625">pro.h:O</ta>
            <ta e="T627" id="Seg_10519" s="T626">v:pred 0.3.h:S</ta>
            <ta e="T628" id="Seg_10520" s="T627">v:pred 0.3.h:S</ta>
            <ta e="T631" id="Seg_10521" s="T630">np.h:S</ta>
            <ta e="T633" id="Seg_10522" s="T632">v:pred 0.2.h:S</ta>
            <ta e="T635" id="Seg_10523" s="T634">np:O</ta>
            <ta e="T637" id="Seg_10524" s="T636">np.h:S</ta>
            <ta e="T638" id="Seg_10525" s="T637">v:pred</ta>
            <ta e="T639" id="Seg_10526" s="T638">np.h:O</ta>
            <ta e="T640" id="Seg_10527" s="T639">v:pred 0.3.h:S</ta>
            <ta e="T641" id="Seg_10528" s="T640">pro.h:S</ta>
            <ta e="T642" id="Seg_10529" s="T641">v:pred</ta>
            <ta e="T644" id="Seg_10530" s="T643">v:pred 0.3.h:S</ta>
            <ta e="T646" id="Seg_10531" s="T645">pro.h:S</ta>
            <ta e="T647" id="Seg_10532" s="T646">v:pred</ta>
            <ta e="T648" id="Seg_10533" s="T647">v:pred 0.3.h:S</ta>
            <ta e="T650" id="Seg_10534" s="T649">v:pred 0.3.h:S</ta>
            <ta e="T653" id="Seg_10535" s="T652">pro.h:S</ta>
            <ta e="T654" id="Seg_10536" s="T653">v:pred</ta>
            <ta e="T657" id="Seg_10537" s="T656">v:pred 0.1.h:S</ta>
            <ta e="T659" id="Seg_10538" s="T658">pro.h:S</ta>
            <ta e="T660" id="Seg_10539" s="T659">v:pred</ta>
            <ta e="T664" id="Seg_10540" s="T663">v:pred 0.3.h:S</ta>
            <ta e="T668" id="Seg_10541" s="T667">pro.h:S</ta>
            <ta e="T669" id="Seg_10542" s="T668">v:pred</ta>
            <ta e="T673" id="Seg_10543" s="T672">v:pred 0.3.h:S</ta>
            <ta e="T674" id="Seg_10544" s="T673">ptcl:pred</ta>
            <ta e="T675" id="Seg_10545" s="T674">v:pred</ta>
            <ta e="T676" id="Seg_10546" s="T675">v:pred 0.3.h:S</ta>
            <ta e="T677" id="Seg_10547" s="T676">v:pred 0.3.h:S</ta>
            <ta e="T680" id="Seg_10548" s="T679">v:pred 0.3.h:S</ta>
            <ta e="T681" id="Seg_10549" s="T680">s:purp</ta>
            <ta e="T683" id="Seg_10550" s="T682">np.h:S</ta>
            <ta e="T684" id="Seg_10551" s="T683">v:pred</ta>
            <ta e="T685" id="Seg_10552" s="T684">pro.h:O</ta>
            <ta e="T686" id="Seg_10553" s="T685">v:pred 0.3.h:S</ta>
            <ta e="T689" id="Seg_10554" s="T688">v:pred 0.3.h:S</ta>
            <ta e="T690" id="Seg_10555" s="T689">np.h:O</ta>
            <ta e="T692" id="Seg_10556" s="T691">np:O</ta>
            <ta e="T694" id="Seg_10557" s="T693">np:O</ta>
            <ta e="T695" id="Seg_10558" s="T694">v:pred 0.3.h:S</ta>
            <ta e="T696" id="Seg_10559" s="T695">v:pred 0.3.h:S</ta>
            <ta e="T698" id="Seg_10560" s="T697">np.h:S</ta>
            <ta e="T699" id="Seg_10561" s="T698">v:pred</ta>
            <ta e="T701" id="Seg_10562" s="T700">v:pred 0.3.h:S</ta>
            <ta e="T702" id="Seg_10563" s="T701">pro.h:S</ta>
            <ta e="T703" id="Seg_10564" s="T702">v:pred</ta>
            <ta e="T704" id="Seg_10565" s="T703">v:pred 0.3.h:S</ta>
            <ta e="T705" id="Seg_10566" s="T704">np:O</ta>
            <ta e="T708" id="Seg_10567" s="T707">np:O</ta>
            <ta e="T710" id="Seg_10568" s="T709">v:pred 0.2.h:S</ta>
            <ta e="T712" id="Seg_10569" s="T711">np:O</ta>
            <ta e="T715" id="Seg_10570" s="T714">np:O</ta>
            <ta e="T717" id="Seg_10571" s="T716">np:S</ta>
            <ta e="T718" id="Seg_10572" s="T717">v:pred</ta>
            <ta e="T719" id="Seg_10573" s="T718">v:pred 0.3:S</ta>
            <ta e="T720" id="Seg_10574" s="T719">pro.h:S</ta>
            <ta e="T721" id="Seg_10575" s="T720">pro.h:O</ta>
            <ta e="T722" id="Seg_10576" s="T721">v:pred</ta>
            <ta e="T730" id="Seg_10577" s="T729">pro.h:S</ta>
            <ta e="T731" id="Seg_10578" s="T730">v:pred</ta>
            <ta e="T733" id="Seg_10579" s="T732">pro.h:S</ta>
            <ta e="T734" id="Seg_10580" s="T733">v:pred</ta>
            <ta e="T736" id="Seg_10581" s="T735">v:pred 0.2.h:S</ta>
            <ta e="T738" id="Seg_10582" s="T737">s:cond</ta>
            <ta e="T739" id="Seg_10583" s="T738">ptcl:pred</ta>
            <ta e="T740" id="Seg_10584" s="T739">pro.h:S</ta>
            <ta e="T742" id="Seg_10585" s="T741">pro.h:S</ta>
            <ta e="T745" id="Seg_10586" s="T744">v:pred</ta>
            <ta e="T746" id="Seg_10587" s="T745">pro.h:O</ta>
            <ta e="T747" id="Seg_10588" s="T746">v:pred </ta>
            <ta e="T750" id="Seg_10589" s="T749">np.h:S</ta>
            <ta e="T752" id="Seg_10590" s="T751">np:O</ta>
            <ta e="T753" id="Seg_10591" s="T752">v:pred 0.3.h:S</ta>
            <ta e="T754" id="Seg_10592" s="T753">np.h:O</ta>
            <ta e="T756" id="Seg_10593" s="T755">np:O</ta>
            <ta e="T758" id="Seg_10594" s="T757">np:O</ta>
            <ta e="T760" id="Seg_10595" s="T759">v:pred 0.2.h:S</ta>
            <ta e="T762" id="Seg_10596" s="T761">v:pred 0.3.h:S</ta>
            <ta e="T763" id="Seg_10597" s="T762">pro.h:S</ta>
            <ta e="T764" id="Seg_10598" s="T763">v:pred</ta>
            <ta e="T765" id="Seg_10599" s="T764">pro.h:O</ta>
            <ta e="T767" id="Seg_10600" s="T766">np.h:S</ta>
            <ta e="T768" id="Seg_10601" s="T767">np.h:O</ta>
            <ta e="T769" id="Seg_10602" s="T768">v:pred</ta>
            <ta e="T771" id="Seg_10603" s="T770">pro.h:S</ta>
            <ta e="T772" id="Seg_10604" s="T771">v:pred</ta>
            <ta e="T773" id="Seg_10605" s="T772">np.h:O</ta>
            <ta e="T776" id="Seg_10606" s="T775">pro:S</ta>
            <ta e="T778" id="Seg_10607" s="T777">v:pred</ta>
            <ta e="T781" id="Seg_10608" s="T780">np.h:S</ta>
            <ta e="T783" id="Seg_10609" s="T782">v:pred</ta>
            <ta e="T786" id="Seg_10610" s="T785">v:pred 0.3.h:S</ta>
            <ta e="T788" id="Seg_10611" s="T787">v:pred 0.3.h:S</ta>
            <ta e="T790" id="Seg_10612" s="T789">np:O</ta>
            <ta e="T792" id="Seg_10613" s="T791">v:pred 0.3.h:S</ta>
            <ta e="T794" id="Seg_10614" s="T793">np.h:S</ta>
            <ta e="T795" id="Seg_10615" s="T794">v:pred</ta>
            <ta e="T796" id="Seg_10616" s="T795">v:pred 0.3.h:S</ta>
            <ta e="T798" id="Seg_10617" s="T797">pro.h:S</ta>
            <ta e="T799" id="Seg_10618" s="T798">np.h:O</ta>
            <ta e="T800" id="Seg_10619" s="T799">v:pred</ta>
            <ta e="T801" id="Seg_10620" s="T800">v:pred 0.3.h:S</ta>
            <ta e="T802" id="Seg_10621" s="T801">np:O</ta>
            <ta e="T803" id="Seg_10622" s="T802">v:pred 0.3.h:S</ta>
            <ta e="T807" id="Seg_10623" s="T806">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_10624" s="T1">TURK:cult</ta>
            <ta e="T9" id="Seg_10625" s="T8">RUS:cult</ta>
            <ta e="T11" id="Seg_10626" s="T10">RUS:cult</ta>
            <ta e="T14" id="Seg_10627" s="T13">TURK:gram(INDEF)</ta>
            <ta e="T15" id="Seg_10628" s="T14">TURK:disc</ta>
            <ta e="T18" id="Seg_10629" s="T17">TURK:disc</ta>
            <ta e="T20" id="Seg_10630" s="T19">TURK:gram(INDEF)</ta>
            <ta e="T38" id="Seg_10631" s="T37">TURK:gram(INDEF)</ta>
            <ta e="T42" id="Seg_10632" s="T41">TURK:core</ta>
            <ta e="T46" id="Seg_10633" s="T45">RUS:mod</ta>
            <ta e="T48" id="Seg_10634" s="T47">TURK:gram(INDEF)</ta>
            <ta e="T52" id="Seg_10635" s="T51">RUS:cult</ta>
            <ta e="T57" id="Seg_10636" s="T56">RUS:mod</ta>
            <ta e="T59" id="Seg_10637" s="T58">TURK:disc</ta>
            <ta e="T63" id="Seg_10638" s="T62">RUS:gram</ta>
            <ta e="T69" id="Seg_10639" s="T68">TURK:disc</ta>
            <ta e="T84" id="Seg_10640" s="T83">RUS:core</ta>
            <ta e="T90" id="Seg_10641" s="T89">RUS:disc</ta>
            <ta e="T101" id="Seg_10642" s="T100">RUS:core</ta>
            <ta e="T105" id="Seg_10643" s="T102">RUS:cult</ta>
            <ta e="T107" id="Seg_10644" s="T106">RUS:gram</ta>
            <ta e="T118" id="Seg_10645" s="T117">RUS:gram(INDEF)</ta>
            <ta e="T119" id="Seg_10646" s="T118">TURK:disc</ta>
            <ta e="T126" id="Seg_10647" s="T125">RUS:mod</ta>
            <ta e="T130" id="Seg_10648" s="T129">RUS:gram(INDEF)</ta>
            <ta e="T144" id="Seg_10649" s="T143">TURK:core</ta>
            <ta e="T146" id="Seg_10650" s="T145">RUS:cult</ta>
            <ta e="T147" id="Seg_10651" s="T146">RUS:mod</ta>
            <ta e="T152" id="Seg_10652" s="T151">RUS:cult</ta>
            <ta e="T175" id="Seg_10653" s="T174">RUS:mod</ta>
            <ta e="T179" id="Seg_10654" s="T178">RUS:gram</ta>
            <ta e="T181" id="Seg_10655" s="T180">RUS:mod</ta>
            <ta e="T188" id="Seg_10656" s="T187">RUS:gram</ta>
            <ta e="T189" id="Seg_10657" s="T188">TURK:disc</ta>
            <ta e="T193" id="Seg_10658" s="T192">RUS:cult</ta>
            <ta e="T199" id="Seg_10659" s="T198">RUS:disc</ta>
            <ta e="T203" id="Seg_10660" s="T202">RUS:gram</ta>
            <ta e="T207" id="Seg_10661" s="T206">RUS:gram</ta>
            <ta e="T209" id="Seg_10662" s="T208">RUS:gram</ta>
            <ta e="T213" id="Seg_10663" s="T212">RUS:disc</ta>
            <ta e="T227" id="Seg_10664" s="T226">RUS:disc</ta>
            <ta e="T231" id="Seg_10665" s="T230">RUS:gram</ta>
            <ta e="T249" id="Seg_10666" s="T248">TURK:disc</ta>
            <ta e="T255" id="Seg_10667" s="T254">RUS:gram</ta>
            <ta e="T260" id="Seg_10668" s="T258">RUS:gram</ta>
            <ta e="T269" id="Seg_10669" s="T268">RUS:mod</ta>
            <ta e="T271" id="Seg_10670" s="T270">RUS:gram</ta>
            <ta e="T274" id="Seg_10671" s="T273">TURK:disc</ta>
            <ta e="T276" id="Seg_10672" s="T275">TURK:disc</ta>
            <ta e="T279" id="Seg_10673" s="T278">RUS:gram</ta>
            <ta e="T283" id="Seg_10674" s="T282">TURK:cult</ta>
            <ta e="T288" id="Seg_10675" s="T287">TURK:cult</ta>
            <ta e="T299" id="Seg_10676" s="T298">RUS:cult</ta>
            <ta e="T300" id="Seg_10677" s="T299">RUS:gram</ta>
            <ta e="T307" id="Seg_10678" s="T306">RUS:gram</ta>
            <ta e="T311" id="Seg_10679" s="T310">RUS:gram</ta>
            <ta e="T312" id="Seg_10680" s="T311">RUS:cult</ta>
            <ta e="T314" id="Seg_10681" s="T313">RUS:gram</ta>
            <ta e="T317" id="Seg_10682" s="T316">RUS:gram</ta>
            <ta e="T318" id="Seg_10683" s="T317">RUS:gram</ta>
            <ta e="T323" id="Seg_10684" s="T322">RUS:gram</ta>
            <ta e="T324" id="Seg_10685" s="T323">RUS:gram</ta>
            <ta e="T327" id="Seg_10686" s="T326">RUS:disc</ta>
            <ta e="T330" id="Seg_10687" s="T329">TURK:gram(INDEF)</ta>
            <ta e="T336" id="Seg_10688" s="T335">RUS:cult</ta>
            <ta e="T337" id="Seg_10689" s="T336">RUS:core</ta>
            <ta e="T341" id="Seg_10690" s="T340">RUS:cult</ta>
            <ta e="T342" id="Seg_10691" s="T341">RUS:cult</ta>
            <ta e="T349" id="Seg_10692" s="T348">RUS:gram</ta>
            <ta e="T352" id="Seg_10693" s="T351">RUS:gram</ta>
            <ta e="T359" id="Seg_10694" s="T358">RUS:disc</ta>
            <ta e="T363" id="Seg_10695" s="T362">RUS:disc</ta>
            <ta e="T365" id="Seg_10696" s="T364">RUS:gram</ta>
            <ta e="T368" id="Seg_10697" s="T367">RUS:gram</ta>
            <ta e="T369" id="Seg_10698" s="T368">RUS:cult</ta>
            <ta e="T378" id="Seg_10699" s="T377">RUS:gram</ta>
            <ta e="T386" id="Seg_10700" s="T385">RUS:gram</ta>
            <ta e="T387" id="Seg_10701" s="T386">RUS:cult</ta>
            <ta e="T392" id="Seg_10702" s="T391">RUS:mod</ta>
            <ta e="T394" id="Seg_10703" s="T393">RUS:mod</ta>
            <ta e="T398" id="Seg_10704" s="T397">TURK:disc</ta>
            <ta e="T406" id="Seg_10705" s="T405">TURK:cult</ta>
            <ta e="T415" id="Seg_10706" s="T414">RUS:cult</ta>
            <ta e="T416" id="Seg_10707" s="T415">RUS:gram</ta>
            <ta e="T420" id="Seg_10708" s="T419">RUS:disc</ta>
            <ta e="T424" id="Seg_10709" s="T423">TURK:gram(INDEF)</ta>
            <ta e="T430" id="Seg_10710" s="T429">RUS:cult</ta>
            <ta e="T431" id="Seg_10711" s="T430">RUS:cult</ta>
            <ta e="T439" id="Seg_10712" s="T438">RUS:gram</ta>
            <ta e="T440" id="Seg_10713" s="T439">RUS:cult</ta>
            <ta e="T444" id="Seg_10714" s="T443">RUS:gram</ta>
            <ta e="T448" id="Seg_10715" s="T447">RUS:disc</ta>
            <ta e="T449" id="Seg_10716" s="T448">TURK:gram(INDEF)</ta>
            <ta e="T453" id="Seg_10717" s="T452">RUS:disc</ta>
            <ta e="T459" id="Seg_10718" s="T458">RUS:cult</ta>
            <ta e="T467" id="Seg_10719" s="T466">RUS:gram</ta>
            <ta e="T473" id="Seg_10720" s="T472">RUS:cult</ta>
            <ta e="T479" id="Seg_10721" s="T478">RUS:cult</ta>
            <ta e="T481" id="Seg_10722" s="T480">RUS:cult</ta>
            <ta e="T486" id="Seg_10723" s="T485">RUS:gram</ta>
            <ta e="T488" id="Seg_10724" s="T487">RUS:mod</ta>
            <ta e="T490" id="Seg_10725" s="T489">RUS:gram</ta>
            <ta e="T492" id="Seg_10726" s="T491">RUS:cult</ta>
            <ta e="T505" id="Seg_10727" s="T504">RUS:gram</ta>
            <ta e="T509" id="Seg_10728" s="T508">RUS:cult</ta>
            <ta e="T513" id="Seg_10729" s="T512">RUS:gram</ta>
            <ta e="T514" id="Seg_10730" s="T513">RUS:cult</ta>
            <ta e="T528" id="Seg_10731" s="T527">RUS:disc</ta>
            <ta e="T530" id="Seg_10732" s="T529">RUS:gram</ta>
            <ta e="T545" id="Seg_10733" s="T544">RUS:gram</ta>
            <ta e="T551" id="Seg_10734" s="T550">TURK:cult</ta>
            <ta e="T559" id="Seg_10735" s="T558">RUS:cult</ta>
            <ta e="T564" id="Seg_10736" s="T563">RUS:gram</ta>
            <ta e="T572" id="Seg_10737" s="T571">RUS:cult</ta>
            <ta e="T575" id="Seg_10738" s="T574">RUS:cult</ta>
            <ta e="T576" id="Seg_10739" s="T575">RUS:gram</ta>
            <ta e="T579" id="Seg_10740" s="T578">RUS:gram</ta>
            <ta e="T581" id="Seg_10741" s="T580">RUS:gram</ta>
            <ta e="T586" id="Seg_10742" s="T585">RUS:cult</ta>
            <ta e="T587" id="Seg_10743" s="T586">RUS:cult</ta>
            <ta e="T592" id="Seg_10744" s="T591">RUS:disc</ta>
            <ta e="T593" id="Seg_10745" s="T592">RUS:gram</ta>
            <ta e="T594" id="Seg_10746" s="T593">RUS:mod</ta>
            <ta e="T598" id="Seg_10747" s="T597">RUS:gram</ta>
            <ta e="T603" id="Seg_10748" s="T602">RUS:gram</ta>
            <ta e="T616" id="Seg_10749" s="T615">RUS:gram</ta>
            <ta e="T618" id="Seg_10750" s="T617">RUS:gram</ta>
            <ta e="T622" id="Seg_10751" s="T621">RUS:gram</ta>
            <ta e="T631" id="Seg_10752" s="T630">TURK:cult</ta>
            <ta e="T632" id="Seg_10753" s="T631">RUS:disc</ta>
            <ta e="T639" id="Seg_10754" s="T638">RUS:cult</ta>
            <ta e="T643" id="Seg_10755" s="T642">RUS:gram</ta>
            <ta e="T645" id="Seg_10756" s="T644">RUS:gram</ta>
            <ta e="T649" id="Seg_10757" s="T648">RUS:cult</ta>
            <ta e="T651" id="Seg_10758" s="T650">RUS:disc</ta>
            <ta e="T674" id="Seg_10759" s="T673">RUS:gram</ta>
            <ta e="T678" id="Seg_10760" s="T677">RUS:gram</ta>
            <ta e="T687" id="Seg_10761" s="T686">RUS:gram</ta>
            <ta e="T691" id="Seg_10762" s="T690">RUS:gram</ta>
            <ta e="T693" id="Seg_10763" s="T692">RUS:gram</ta>
            <ta e="T698" id="Seg_10764" s="T697">RUS:cult</ta>
            <ta e="T706" id="Seg_10765" s="T705">RUS:cult</ta>
            <ta e="T709" id="Seg_10766" s="T708">RUS:disc</ta>
            <ta e="T713" id="Seg_10767" s="T712">RUS:gram</ta>
            <ta e="T726" id="Seg_10768" s="T725">RUS:gram</ta>
            <ta e="T738" id="Seg_10769" s="T737">RUS:gram</ta>
            <ta e="T741" id="Seg_10770" s="T740">RUS:gram</ta>
            <ta e="T744" id="Seg_10771" s="T743">RUS:gram</ta>
            <ta e="T751" id="Seg_10772" s="T750">RUS:gram</ta>
            <ta e="T752" id="Seg_10773" s="T751">TURK:disc</ta>
            <ta e="T755" id="Seg_10774" s="T754">RUS:gram</ta>
            <ta e="T757" id="Seg_10775" s="T756">RUS:gram</ta>
            <ta e="T767" id="Seg_10776" s="T766">RUS:cult</ta>
            <ta e="T770" id="Seg_10777" s="T769">RUS:gram</ta>
            <ta e="T774" id="Seg_10778" s="T773">RUS:gram</ta>
            <ta e="T775" id="Seg_10779" s="T774">TURK:disc</ta>
            <ta e="T781" id="Seg_10780" s="T780">RUS:cult</ta>
            <ta e="T785" id="Seg_10781" s="T784">RUS:gram</ta>
            <ta e="T790" id="Seg_10782" s="T789">TURK:disc</ta>
            <ta e="T802" id="Seg_10783" s="T801">TURK:cult</ta>
            <ta e="T804" id="Seg_10784" s="T803">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T387" id="Seg_10785" s="T386">dir:infl</ta>
            <ta e="T415" id="Seg_10786" s="T414">dir:bare</ta>
            <ta e="T440" id="Seg_10787" s="T439">dir:infl</ta>
            <ta e="T459" id="Seg_10788" s="T458">dir:bare</ta>
            <ta e="T473" id="Seg_10789" s="T472">dir:infl</ta>
            <ta e="T479" id="Seg_10790" s="T478">dir:infl</ta>
            <ta e="T481" id="Seg_10791" s="T480">dir:infl</ta>
            <ta e="T492" id="Seg_10792" s="T491">dir:infl</ta>
            <ta e="T509" id="Seg_10793" s="T508">dir:bare</ta>
            <ta e="T514" id="Seg_10794" s="T513">dir:infl</ta>
            <ta e="T559" id="Seg_10795" s="T558">dir:infl</ta>
            <ta e="T572" id="Seg_10796" s="T571">dir:infl</ta>
            <ta e="T575" id="Seg_10797" s="T574">dir:infl</ta>
            <ta e="T586" id="Seg_10798" s="T585">dir:bare</ta>
            <ta e="T587" id="Seg_10799" s="T586">dir:infl</ta>
            <ta e="T639" id="Seg_10800" s="T638">dir:infl</ta>
            <ta e="T649" id="Seg_10801" s="T648">dir:infl</ta>
            <ta e="T698" id="Seg_10802" s="T697">dir:bare</ta>
            <ta e="T767" id="Seg_10803" s="T766">dir:bare</ta>
            <ta e="T781" id="Seg_10804" s="T780">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T111" id="Seg_10805" s="T110">RUS:int.ins</ta>
            <ta e="T303" id="Seg_10806" s="T302">RUS:int.ins</ta>
            <ta e="T656" id="Seg_10807" s="T655">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_10808" s="T0">Жил-был царь.</ta>
            <ta e="T7" id="Seg_10809" s="T3">У него было три сына.</ta>
            <ta e="T13" id="Seg_10810" s="T7">У него был сад, там росли яблоки, красивые.</ta>
            <ta e="T16" id="Seg_10811" s="T13">Кто-то [их] крал.</ta>
            <ta e="T21" id="Seg_10812" s="T16">Он видит, что кто-то их крадет.</ta>
            <ta e="T24" id="Seg_10813" s="T21">Он поставил людей следить [=сторожить].</ta>
            <ta e="T27" id="Seg_10814" s="T24">Они следят, ничего [не видят].</ta>
            <ta e="T32" id="Seg_10815" s="T27">Тогда один сын говорит:</ta>
            <ta e="T34" id="Seg_10816" s="T32">"Я пойду!"</ta>
            <ta e="T36" id="Seg_10817" s="T34">Он пошел.</ta>
            <ta e="T37" id="Seg_10818" s="T36">Заснул.</ta>
            <ta e="T40" id="Seg_10819" s="T37">Ничего не увидел.</ta>
            <ta e="T45" id="Seg_10820" s="T40">Потом другой сын пошел.</ta>
            <ta e="T47" id="Seg_10821" s="T45">Тоже заснул.</ta>
            <ta e="T50" id="Seg_10822" s="T47">Ничего не увидел.</ta>
            <ta e="T53" id="Seg_10823" s="T50">Тогда Иванушка пошел.</ta>
            <ta e="T55" id="Seg_10824" s="T53">Он не спит.</ta>
            <ta e="T65" id="Seg_10825" s="T55">Спать хочется, он водой глаза промывает и дальше ходит.</ta>
            <ta e="T71" id="Seg_10826" s="T65">Потом видит: огонь горит.</ta>
            <ta e="T74" id="Seg_10827" s="T71">Он увидел:</ta>
            <ta e="T77" id="Seg_10828" s="T74">Птица сидит.</ta>
            <ta e="T79" id="Seg_10829" s="T77">Как огонь.</ta>
            <ta e="T81" id="Seg_10830" s="T79">Он (схватил?) ее.</ta>
            <ta e="T82" id="Seg_10831" s="T81">Поймал.</ta>
            <ta e="T86" id="Seg_10832" s="T82">[Она вырвалась, но] отдала одно перо.</ta>
            <ta e="T89" id="Seg_10833" s="T86">Он пришел к отцу.</ta>
            <ta e="T92" id="Seg_10834" s="T89">"Ну что, сын?"</ta>
            <ta e="T97" id="Seg_10835" s="T92">"Я видел, кто крадет!</ta>
            <ta e="T101" id="Seg_10836" s="T97">Вот, я принес тебе перо.</ta>
            <ta e="T108" id="Seg_10837" s="T101">Это Жар-Птица ходит [=летает] и крадет".</ta>
            <ta e="T112" id="Seg_10838" s="T108">Отец начал есть.</ta>
            <ta e="T118" id="Seg_10839" s="T112">Потом думает, думает: "Идите, сыновья, куда-нибудь.</ta>
            <ta e="T121" id="Seg_10840" s="T118">Обойдите всю землю!</ta>
            <ta e="T136" id="Seg_10841" s="T121">Может, где-нибудь вы поймаете эту птицу.</ta>
            <ta e="T138" id="Seg_10842" s="T136">Которая [как] огонь".</ta>
            <ta e="T145" id="Seg_10843" s="T138">Они пошли, один туда, другой сюда.</ta>
            <ta e="T148" id="Seg_10844" s="T145">Ванюшка тоже пошел.</ta>
            <ta e="T149" id="Seg_10845" s="T148">Стоп.</ta>
            <ta e="T153" id="Seg_10846" s="T149">Потом этот Иван-Царевич пошел.</ta>
            <ta e="T155" id="Seg_10847" s="T153">Шел-шел. [?]</ta>
            <ta e="T157" id="Seg_10848" s="T155">Лошадь запряг.</ta>
            <ta e="T161" id="Seg_10849" s="T157">Он поел, лег спать.</ta>
            <ta e="T163" id="Seg_10850" s="T161">Лошадь ходит [вокруг].</ta>
            <ta e="T165" id="Seg_10851" s="T163">Он связал ей ноги [=стреножил].</ta>
            <ta e="T167" id="Seg_10852" s="T165">Он долго спал.</ta>
            <ta e="T169" id="Seg_10853" s="T167">Потом встал.</ta>
            <ta e="T172" id="Seg_10854" s="T169">Видит: лошади нет.</ta>
            <ta e="T174" id="Seg_10855" s="T172">Он искал, искал.</ta>
            <ta e="T177" id="Seg_10856" s="T174">Только косточки лежат.</ta>
            <ta e="T183" id="Seg_10857" s="T177">Сидит; потом пришлось идти пешком.</ta>
            <ta e="T191" id="Seg_10858" s="T183">Идет-идет, устал; сел, сидит.</ta>
            <ta e="T194" id="Seg_10859" s="T191">Волк бежит.</ta>
            <ta e="T198" id="Seg_10860" s="T194">"Что ты сидишь, голову повесил?"</ta>
            <ta e="T202" id="Seg_10861" s="T198">"Да лошадь мою съели".</ta>
            <ta e="T206" id="Seg_10862" s="T202">"А, это я съел.</ta>
            <ta e="T212" id="Seg_10863" s="T206">А куда ты идешь?"</ta>
            <ta e="T222" id="Seg_10864" s="T212">"Я иду птицу ищу, мой отец сказал: [иди] искать птицу!</ta>
            <ta e="T224" id="Seg_10865" s="T222">[Которая] как огонь".</ta>
            <ta e="T236" id="Seg_10866" s="T224">Тогда тот [говорит]: "Ну, на твоей лошади туда за три года не доедешь.</ta>
            <ta e="T239" id="Seg_10867" s="T236">Садись на меня!</ta>
            <ta e="T242" id="Seg_10868" s="T239">Я тебя довезу".</ta>
            <ta e="T246" id="Seg_10869" s="T242">Он сел, тот его довез.</ta>
            <ta e="T247" id="Seg_10870" s="T246">"Иди!</ta>
            <ta e="T251" id="Seg_10871" s="T247">Люди сейчас спят.</ta>
            <ta e="T254" id="Seg_10872" s="T251">Возьми птицу!</ta>
            <ta e="T260" id="Seg_10873" s="T254">Но не бери ее клетку, иначе…"</ta>
            <ta e="T263" id="Seg_10874" s="T260">Тот пошел.</ta>
            <ta e="T265" id="Seg_10875" s="T263">Взял птицу.</ta>
            <ta e="T266" id="Seg_10876" s="T265">Смотрит.</ta>
            <ta e="T270" id="Seg_10877" s="T266">Клетка красивая, надо ее взять.</ta>
            <ta e="T276" id="Seg_10878" s="T270">Как только он ее тронул, раздался звон.</ta>
            <ta e="T278" id="Seg_10879" s="T276">Его схватили.</ta>
            <ta e="T283" id="Seg_10880" s="T278">И привели его к царю.</ta>
            <ta e="T284" id="Seg_10881" s="T283">Стоп.</ta>
            <ta e="T288" id="Seg_10882" s="T284">Он пришел к царю.</ta>
            <ta e="T294" id="Seg_10883" s="T288">"Ты зачем сюда пришел красть?</ta>
            <ta e="T297" id="Seg_10884" s="T294">Чей ты сын?"</ta>
            <ta e="T299" id="Seg_10885" s="T297">"Я Иван-Царевич".</ta>
            <ta e="T303" id="Seg_10886" s="T299">"Так ты красть захотел?"</ta>
            <ta e="T306" id="Seg_10887" s="T303">Тогда [Иван-царевич] говорит.</ta>
            <ta e="T313" id="Seg_10888" s="T306">"Она пришла к нам и ела яблоки".</ta>
            <ta e="T326" id="Seg_10889" s="T313">"А ты пришел бы, я бы тебе так дал, а ты красть пришел.</ta>
            <ta e="T337" id="Seg_10890" s="T326">Ну, я тебе ничего не сделаю, иди приведи мне коня с золотой гривой!"</ta>
            <ta e="T341" id="Seg_10891" s="T337">Он пошел к волку.</ta>
            <ta e="T348" id="Seg_10892" s="T341">Волк говорит: "Я же говорил тебе, не трогай [клетку].</ta>
            <ta e="T357" id="Seg_10893" s="T348">А ты тронул ее, а теперь… садись на меня, поехали".</ta>
            <ta e="T358" id="Seg_10894" s="T357">Они поехали.</ta>
            <ta e="T361" id="Seg_10895" s="T358">Ну, приехали туда.</ta>
            <ta e="T364" id="Seg_10896" s="T361">"Ну, иди!</ta>
            <ta e="T377" id="Seg_10897" s="T364">Поймай коня, а уздечку не бери!</ta>
            <ta e="T381" id="Seg_10898" s="T377">А то опять так будет!"</ta>
            <ta e="T383" id="Seg_10899" s="T381">Тот пошел.</ta>
            <ta e="T385" id="Seg_10900" s="T383">Коня взял.</ta>
            <ta e="T389" id="Seg_10901" s="T385">А уздечка очень красивая.</ta>
            <ta e="T394" id="Seg_10902" s="T389">Что на (коне?), надо ее надеть только.</ta>
            <ta e="T396" id="Seg_10903" s="T394">Взял ее.</ta>
            <ta e="T399" id="Seg_10904" s="T396">Она зазвенела.</ta>
            <ta e="T403" id="Seg_10905" s="T399">Люди прибежали, его схватили.</ta>
            <ta e="T406" id="Seg_10906" s="T403">Отвели к царю.</ta>
            <ta e="T410" id="Seg_10907" s="T406">"Зачем ты пришел сюда красть коня?</ta>
            <ta e="T412" id="Seg_10908" s="T410">Чей ты?"</ta>
            <ta e="T415" id="Seg_10909" s="T412">"Я Иван-Царевич".</ta>
            <ta e="T419" id="Seg_10910" s="T415">"Так ты воровать пришел?</ta>
            <ta e="T426" id="Seg_10911" s="T419">Ну, иди, я тебе ничего не сделаю.</ta>
            <ta e="T431" id="Seg_10912" s="T426">Есть девушка, Елена Прекрасная.</ta>
            <ta e="T434" id="Seg_10913" s="T431">Привези мне ее!"</ta>
            <ta e="T438" id="Seg_10914" s="T434">Он пошел.</ta>
            <ta e="T447" id="Seg_10915" s="T438">А волк [говорит]: "Я же говорил: не трогай ее, а ты все время трогаешь".</ta>
            <ta e="T450" id="Seg_10916" s="T447">Ну, ничего (?).</ta>
            <ta e="T455" id="Seg_10917" s="T450">Говорит: "Ну, садись, поехали".</ta>
            <ta e="T457" id="Seg_10918" s="T455">Приехали туда.</ta>
            <ta e="T461" id="Seg_10919" s="T457">Волк говорит ему:</ta>
            <ta e="T466" id="Seg_10920" s="T461">"Ты [не?] ходи, вернись!</ta>
            <ta e="T470" id="Seg_10921" s="T466">Я сам пойду".</ta>
            <ta e="T473" id="Seg_10922" s="T470">Он [волк] прыгнул в сад.</ta>
            <ta e="T475" id="Seg_10923" s="T473">Там спрятался.</ta>
            <ta e="T479" id="Seg_10924" s="T475">Они собрались идти, мать и няньки.</ta>
            <ta e="T482" id="Seg_10925" s="T479">Елена осталась [одна].</ta>
            <ta e="T492" id="Seg_10926" s="T482">Он схватил ее и убежал, бежит, догнал Ивана.</ta>
            <ta e="T493" id="Seg_10927" s="T492">"Садись!"</ta>
            <ta e="T494" id="Seg_10928" s="T493">Они сели.</ta>
            <ta e="T501" id="Seg_10929" s="T494">Потом приехали туда, где хотели взять коня.</ta>
            <ta e="T509" id="Seg_10930" s="T501">Приехали, а тот все думает, Иван-Царевич.</ta>
            <ta e="T512" id="Seg_10931" s="T509">Жалко ему девушку.</ta>
            <ta e="T515" id="Seg_10932" s="T512">А волк спрашивает:</ta>
            <ta e="T519" id="Seg_10933" s="T515">"Что ты здесь стоишь?"</ta>
            <ta e="T523" id="Seg_10934" s="T519">"Она такая красивая девушка.</ta>
            <ta e="T527" id="Seg_10935" s="T523">Мне жалко отдавать [ее]".</ta>
            <ta e="T536" id="Seg_10936" s="T527">"Ну, давай ее спрячем, а я превращусь в нее".</ta>
            <ta e="T540" id="Seg_10937" s="T536">Тогда он на спину перекувырнулся.</ta>
            <ta e="T544" id="Seg_10938" s="T540">[Иван-Царевич] взял его [волка], повел [во дворец].</ta>
            <ta e="T549" id="Seg_10939" s="T544">И тот [царь] взял девушку.</ta>
            <ta e="T555" id="Seg_10940" s="T549">(…) водку пили, танцевали, он дал коня.</ta>
            <ta e="T558" id="Seg_10941" s="T555">Тот взял коня.</ta>
            <ta e="T562" id="Seg_10942" s="T558">Елену посадил, сам сел.</ta>
            <ta e="T563" id="Seg_10943" s="T562">Они уехали.</ta>
            <ta e="T568" id="Seg_10944" s="T563">А ночью [царь и девушка] легли спать.</ta>
            <ta e="T575" id="Seg_10945" s="T568">Эта Елена превратилась в волка.</ta>
            <ta e="T580" id="Seg_10946" s="T575">Тот испугался, упал [с кровати].</ta>
            <ta e="T583" id="Seg_10947" s="T580">А [волк] убежал.</ta>
            <ta e="T587" id="Seg_10948" s="T583">Догоняет Иванушку с Еленушкой.</ta>
            <ta e="T591" id="Seg_10949" s="T587">"Что ты здесь стоишь?"</ta>
            <ta e="T597" id="Seg_10950" s="T591">"Да не хочется мне отдавать коня, он красивый".</ta>
            <ta e="T604" id="Seg_10951" s="T597">А тот говорит: "Спрячь коня и девушку".</ta>
            <ta e="T611" id="Seg_10952" s="T604">Потом он превратился в коня, [Иван-Царевич] взял его.</ta>
            <ta e="T617" id="Seg_10953" s="T611">Взял птицу, [которая] как огонь.</ta>
            <ta e="T621" id="Seg_10954" s="T617">И сам вернулся, сел.</ta>
            <ta e="T628" id="Seg_10955" s="T621">И девушку посадил на лошадь, сам сел, они поехали.</ta>
            <ta e="T635" id="Seg_10956" s="T628">А царь [говорит]: "Приведите этого коня!"</ta>
            <ta e="T640" id="Seg_10957" s="T635">Конь пришел, превратился в волка.</ta>
            <ta e="T647" id="Seg_10958" s="T640">Тот испугался и упал, а [волк] убежал.</ta>
            <ta e="T650" id="Seg_10959" s="T647">Догоняет, Ванюшке говорит:</ta>
            <ta e="T658" id="Seg_10960" s="T650">"Ну, теперь мне некуда с тобой идти, я останусь здесь.</ta>
            <ta e="T664" id="Seg_10961" s="T658">Тот спрыгнул, поклонился ему три раза. [?]</ta>
            <ta e="T665" id="Seg_10962" s="T664">Стоп.</ta>
            <ta e="T672" id="Seg_10963" s="T665">Потом они пошли в свою землю.</ta>
            <ta e="T675" id="Seg_10964" s="T672">Он говорит: "Давай поедим!"</ta>
            <ta e="T677" id="Seg_10965" s="T675">Они сели, поели.</ta>
            <ta e="T681" id="Seg_10966" s="T677">И легли спать.</ta>
            <ta e="T684" id="Seg_10967" s="T681">Потом братья его пришли.</ta>
            <ta e="T686" id="Seg_10968" s="T684">Его убили.</ta>
            <ta e="T690" id="Seg_10969" s="T686">А сами забрали девушку.</ta>
            <ta e="T695" id="Seg_10970" s="T690">И птицу, и коня, и ушли.</ta>
            <ta e="T696" id="Seg_10971" s="T695">Идут.</ta>
            <ta e="T703" id="Seg_10972" s="T696">Волк пришел, смотрит: [Иван-Царевич] лежит.</ta>
            <ta e="T708" id="Seg_10973" s="T703">Поймал птицу, ворону, маленькую птичку.</ta>
            <ta e="T715" id="Seg_10974" s="T708">"Принеси мертвой воды и живой воды!"</ta>
            <ta e="T719" id="Seg_10975" s="T715">Птица полетела, принесла.</ta>
            <ta e="T725" id="Seg_10976" s="T719">Он его помыл мертвой водой.</ta>
            <ta e="T729" id="Seg_10977" s="T725">И потом живой водой.</ta>
            <ta e="T734" id="Seg_10978" s="T729">Тот подпрыгнул: "[Наверное,] долго я спал".</ta>
            <ta e="T745" id="Seg_10979" s="T734">"Ты долго спал, если бы не я, так вечно бы спал.</ta>
            <ta e="T750" id="Seg_10980" s="T745">Тебя убили твои братья.</ta>
            <ta e="T758" id="Seg_10981" s="T750">И все забрали: девушку, птицу, лошадь".</ta>
            <ta e="T761" id="Seg_10982" s="T758">"Садись на меня!"</ta>
            <ta e="T762" id="Seg_10983" s="T761">[Тот] сел.</ta>
            <ta e="T765" id="Seg_10984" s="T762">Они догнали его [=их].</ta>
            <ta e="T769" id="Seg_10985" s="T765">Волк братьев разорвал.</ta>
            <ta e="T778" id="Seg_10986" s="T769">И сам забрал девушку и все, что там было.</ta>
            <ta e="T784" id="Seg_10987" s="T778">Тогда Иванушка поклонился ему в ноги.</ta>
            <ta e="T787" id="Seg_10988" s="T784">И вернулся домой.</ta>
            <ta e="T792" id="Seg_10989" s="T787">Пришел к отцу, все ему рассказал.</ta>
            <ta e="T796" id="Seg_10990" s="T792">Тогда отец заплакал, ему стало жалко.</ta>
            <ta e="T800" id="Seg_10991" s="T796">Потом он девушку взял.</ta>
            <ta e="T801" id="Seg_10992" s="T800">Они поженились.</ta>
            <ta e="T803" id="Seg_10993" s="T801">Водку пили.</ta>
            <ta e="T807" id="Seg_10994" s="T803">И до сих пор все живут.</ta>
            <ta e="T808" id="Seg_10995" s="T807">Все.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_10996" s="T0">There lived a king.</ta>
            <ta e="T7" id="Seg_10997" s="T3">He had three sons.</ta>
            <ta e="T13" id="Seg_10998" s="T7">He had a garden, apples were sitting [=growing] there, beautiful.</ta>
            <ta e="T16" id="Seg_10999" s="T13">Someone is stealing [them].</ta>
            <ta e="T21" id="Seg_11000" s="T16">He sees that someone is stealing them.</ta>
            <ta e="T24" id="Seg_11001" s="T21">He set up people to watch.</ta>
            <ta e="T27" id="Seg_11002" s="T24">They’re watching, [there is] nothing.</ta>
            <ta e="T32" id="Seg_11003" s="T27">Then one son says:</ta>
            <ta e="T34" id="Seg_11004" s="T32">"I will go!"</ta>
            <ta e="T36" id="Seg_11005" s="T34">He went.</ta>
            <ta e="T37" id="Seg_11006" s="T36">He fell asleep.</ta>
            <ta e="T40" id="Seg_11007" s="T37">He did not see anything.</ta>
            <ta e="T45" id="Seg_11008" s="T40">Then another son went.</ta>
            <ta e="T47" id="Seg_11009" s="T45">He fell asleep, too.</ta>
            <ta e="T50" id="Seg_11010" s="T47">He did not see anything.</ta>
            <ta e="T53" id="Seg_11011" s="T50">Then Ivanushka went.</ta>
            <ta e="T55" id="Seg_11012" s="T53">He does not sleep.</ta>
            <ta e="T65" id="Seg_11013" s="T55">He gets sleepy, he washes his eyes with water and he moves again.</ta>
            <ta e="T71" id="Seg_11014" s="T65">Then he sees, he saw, a fire was burning.</ta>
            <ta e="T74" id="Seg_11015" s="T71">He saw:</ta>
            <ta e="T77" id="Seg_11016" s="T74">A bird is sitting.</ta>
            <ta e="T79" id="Seg_11017" s="T77">Like the fire.</ta>
            <ta e="T81" id="Seg_11018" s="T79">He (grasped?) [it].</ta>
            <ta e="T82" id="Seg_11019" s="T81">He caught.</ta>
            <ta e="T86" id="Seg_11020" s="T82">[It escaped, but] it gave [=he tore out] one feather.</ta>
            <ta e="T89" id="Seg_11021" s="T86">Then he came to his father.</ta>
            <ta e="T92" id="Seg_11022" s="T89">"Well, what, my son?"</ta>
            <ta e="T97" id="Seg_11023" s="T92">"I saw who steals [apples]!</ta>
            <ta e="T101" id="Seg_11024" s="T97">Here [=Look], I brought you a feather.</ta>
            <ta e="T108" id="Seg_11025" s="T101">This firebird is going [around] and stealing."</ta>
            <ta e="T112" id="Seg_11026" s="T108">Then his father started to eat.</ta>
            <ta e="T118" id="Seg_11027" s="T112">Then he thought, he thought: "Go, [my] sons, somewhere!</ta>
            <ta e="T121" id="Seg_11028" s="T118">Go everywhere on the land!</ta>
            <ta e="T136" id="Seg_11029" s="T121">Maybe somewhere you'll catch this bird somewhere.</ta>
            <ta e="T138" id="Seg_11030" s="T136">Which [is like] fire."</ta>
            <ta e="T145" id="Seg_11031" s="T138">They went, one there, the other here.</ta>
            <ta e="T148" id="Seg_11032" s="T145">Vanyushka also went.</ta>
            <ta e="T149" id="Seg_11033" s="T148">Stop.</ta>
            <ta e="T153" id="Seg_11034" s="T149">Then this Ivan Tsarevich went.</ta>
            <ta e="T155" id="Seg_11035" s="T153">He came, he came. [?]</ta>
            <ta e="T157" id="Seg_11036" s="T155">He harnessed his horse.</ta>
            <ta e="T161" id="Seg_11037" s="T157">He ate, lied down to sleep.</ta>
            <ta e="T163" id="Seg_11038" s="T161">His horse is going around.</ta>
            <ta e="T165" id="Seg_11039" s="T163">He tied [=hobbled] its feet.</ta>
            <ta e="T167" id="Seg_11040" s="T165">He slept long.</ta>
            <ta e="T169" id="Seg_11041" s="T167">Then he got up.</ta>
            <ta e="T172" id="Seg_11042" s="T169">He sees: his horse is not there.</ta>
            <ta e="T174" id="Seg_11043" s="T172">He searched, he searched.</ta>
            <ta e="T177" id="Seg_11044" s="T174">Only bones are lying.</ta>
            <ta e="T183" id="Seg_11045" s="T177">He is sitting and then he needs to go on foot.</ta>
            <ta e="T191" id="Seg_11046" s="T183">He goes, he goes, then he got tired and he sat down, he is sitting.</ta>
            <ta e="T194" id="Seg_11047" s="T191">Then a wolf is running.</ta>
            <ta e="T198" id="Seg_11048" s="T194">"Why are you sitting, why did you hang your head down [=why are you so sad]?"</ta>
            <ta e="T202" id="Seg_11049" s="T198">"My horse has been eaten."</ta>
            <ta e="T206" id="Seg_11050" s="T202">"I ate it.</ta>
            <ta e="T212" id="Seg_11051" s="T206">(So I-) But where are you going?"</ta>
            <ta e="T222" id="Seg_11052" s="T212">"I am going searching a bird, my father said: search the bird!</ta>
            <ta e="T224" id="Seg_11053" s="T222">[Which is] like the fire."</ta>
            <ta e="T236" id="Seg_11054" s="T224">Then it [says]: "Well, with your horse [=on your horse], you will not get there in three years.</ta>
            <ta e="T239" id="Seg_11055" s="T236">Mount me!</ta>
            <ta e="T242" id="Seg_11056" s="T239">I will carry you."</ta>
            <ta e="T246" id="Seg_11057" s="T242">He mounted, then he brought him.</ta>
            <ta e="T247" id="Seg_11058" s="T246">"Go!</ta>
            <ta e="T251" id="Seg_11059" s="T247">People are sleeping now.</ta>
            <ta e="T254" id="Seg_11060" s="T251">Take this bird!</ta>
            <ta e="T260" id="Seg_11061" s="T254">But don't take its coop, or else…"</ta>
            <ta e="T263" id="Seg_11062" s="T260">Then he went.</ta>
            <ta e="T265" id="Seg_11063" s="T263">He took the bird.</ta>
            <ta e="T266" id="Seg_11064" s="T265">He is looking.</ta>
            <ta e="T270" id="Seg_11065" s="T266">The coop is beautiful, [I] should take it.</ta>
            <ta e="T276" id="Seg_11066" s="T270">As he moved it, it started to rumble.</ta>
            <ta e="T278" id="Seg_11067" s="T276">He was caught.</ta>
            <ta e="T283" id="Seg_11068" s="T278">And took [him] to the king.</ta>
            <ta e="T284" id="Seg_11069" s="T283">Stop.</ta>
            <ta e="T288" id="Seg_11070" s="T284">Then he came to the king.</ta>
            <ta e="T294" id="Seg_11071" s="T288">"Why did you come here to steal.</ta>
            <ta e="T297" id="Seg_11072" s="T294">Whose son are you?"</ta>
            <ta e="T299" id="Seg_11073" s="T297">"I am Ivan-Tsarevich."</ta>
            <ta e="T303" id="Seg_11074" s="T299">"So you wanted to steal?"</ta>
            <ta e="T306" id="Seg_11075" s="T303">Then [Ivan] says.</ta>
            <ta e="T313" id="Seg_11076" s="T306">"It came to us and ate apples."</ta>
            <ta e="T326" id="Seg_11077" s="T313">"[But] if you would have come, I would have given you like this, but you came to steal.</ta>
            <ta e="T337" id="Seg_11078" s="T326">Well, I will not do anything to you, go and bring the horse with the golden mane!"</ta>
            <ta e="T341" id="Seg_11079" s="T337">Then he went to the wolf.</ta>
            <ta e="T348" id="Seg_11080" s="T341">The wolf says: "I told you, do not move [the cage].</ta>
            <ta e="T357" id="Seg_11081" s="T348">But you [moved it], and now… mount me, let's go."</ta>
            <ta e="T358" id="Seg_11082" s="T357">They went.</ta>
            <ta e="T361" id="Seg_11083" s="T358">Well, they came there.</ta>
            <ta e="T364" id="Seg_11084" s="T361">Then: "Well, go!</ta>
            <ta e="T377" id="Seg_11085" s="T364">And catch the horse, but don't take the halter!</ta>
            <ta e="T381" id="Seg_11086" s="T377">Or it will be the same again!"</ta>
            <ta e="T383" id="Seg_11087" s="T381">He came.</ta>
            <ta e="T385" id="Seg_11088" s="T383">He took the horse.</ta>
            <ta e="T389" id="Seg_11089" s="T385">But the halter is very beautiful.</ta>
            <ta e="T394" id="Seg_11090" s="T389">What is on the (horse?), one needs just to put it on.</ta>
            <ta e="T396" id="Seg_11091" s="T394">He took it.</ta>
            <ta e="T399" id="Seg_11092" s="T396">It started to ring.</ta>
            <ta e="T403" id="Seg_11093" s="T399">People jumped, took him.</ta>
            <ta e="T406" id="Seg_11094" s="T403">They took him to the king.</ta>
            <ta e="T410" id="Seg_11095" s="T406">"Why did you come to steal the horse?</ta>
            <ta e="T412" id="Seg_11096" s="T410">Whose [son] are you?"</ta>
            <ta e="T415" id="Seg_11097" s="T412">"I am Ivan-Tsarevich."</ta>
            <ta e="T419" id="Seg_11098" s="T415">"So, did you come to steal?</ta>
            <ta e="T426" id="Seg_11099" s="T419">Well, go, I will not do anything to you.</ta>
            <ta e="T431" id="Seg_11100" s="T426">There is a girl, Elena the Beautiful.</ta>
            <ta e="T434" id="Seg_11101" s="T431">Bring her to me!"</ta>
            <ta e="T438" id="Seg_11102" s="T434">Then he goes back.</ta>
            <ta e="T447" id="Seg_11103" s="T438">And the wolf [says]: “I told [you]: don't (touch?) it, and you always touch [things].”</ta>
            <ta e="T450" id="Seg_11104" s="T447">Well, nothing (?).</ta>
            <ta e="T455" id="Seg_11105" s="T450">He says: "Well, mount, let's go."</ta>
            <ta e="T457" id="Seg_11106" s="T455">They came there.</ta>
            <ta e="T461" id="Seg_11107" s="T457">Then the wolf tells him:</ta>
            <ta e="T466" id="Seg_11108" s="T461">"You [do not?] go, come back, come back!</ta>
            <ta e="T470" id="Seg_11109" s="T466">I will go myself."</ta>
            <ta e="T473" id="Seg_11110" s="T470">Then he [the wolf] jumped into the garden.</ta>
            <ta e="T475" id="Seg_11111" s="T473">Hid there.</ta>
            <ta e="T479" id="Seg_11112" s="T475">They are going to leave, her mother, the nannies.</ta>
            <ta e="T482" id="Seg_11113" s="T479">This Elena stayed [alone].</ta>
            <ta e="T492" id="Seg_11114" s="T482">He grabbed her and ran away, so he runs and caught up with Ivan.</ta>
            <ta e="T493" id="Seg_11115" s="T492">"Sit!"</ta>
            <ta e="T494" id="Seg_11116" s="T493">They sat down.</ta>
            <ta e="T501" id="Seg_11117" s="T494">Then they came there where they were looking for the horse.</ta>
            <ta e="T509" id="Seg_11118" s="T501">Then they came [there], but he is still thinking, Ivan Tsarevich.</ta>
            <ta e="T512" id="Seg_11119" s="T509">He does not want to give away the girl.</ta>
            <ta e="T515" id="Seg_11120" s="T512">The wolf asks:</ta>
            <ta e="T519" id="Seg_11121" s="T515">"Why are you standing there?"</ta>
            <ta e="T523" id="Seg_11122" s="T519">"She is such a beautiful girl.</ta>
            <ta e="T527" id="Seg_11123" s="T523">I don't want to give [her away]."</ta>
            <ta e="T536" id="Seg_11124" s="T527">"Well, let’s hide her, and I will turn myself into her."</ta>
            <ta e="T540" id="Seg_11125" s="T536">Then he turned over on his back [=made a somersault] [turning into the girl].</ta>
            <ta e="T544" id="Seg_11126" s="T540">[Ivan Tsarevich] took [the wolf in shape of Elena], led [her to the palace].</ta>
            <ta e="T549" id="Seg_11127" s="T544">And he [the king] took the girl.</ta>
            <ta e="T555" id="Seg_11128" s="T549">(…) they drank vodka, they danced, he gave his horse.</ta>
            <ta e="T558" id="Seg_11129" s="T555">[Ivan] took his horse.</ta>
            <ta e="T562" id="Seg_11130" s="T558">He put his Elena [on horseback], mounted himself.</ta>
            <ta e="T563" id="Seg_11131" s="T562">They went.</ta>
            <ta e="T568" id="Seg_11132" s="T563">But in the evening [the king and the girl] lied down to sleep.</ta>
            <ta e="T575" id="Seg_11133" s="T568">This Elena turned into a wolf.</ta>
            <ta e="T580" id="Seg_11134" s="T575">And [the king] got scared and fell down [from the bed].</ta>
            <ta e="T583" id="Seg_11135" s="T580">And [the wolf] ran away.</ta>
            <ta e="T587" id="Seg_11136" s="T583">Then he catches up with Ivanushka and Elenushka.</ta>
            <ta e="T591" id="Seg_11137" s="T587">"Why are you standing there?"</ta>
            <ta e="T597" id="Seg_11138" s="T591">"Well, I don't want to give away the horse, [so] beautiful."</ta>
            <ta e="T604" id="Seg_11139" s="T597">And he [the wolf] says: "Hide the horse and the girl."</ta>
            <ta e="T611" id="Seg_11140" s="T604">Then he turned itself into a horse, [Ivan] took it.</ta>
            <ta e="T617" id="Seg_11141" s="T611">He took the (burning?) bird, [that is] like fire.</ta>
            <ta e="T621" id="Seg_11142" s="T617">And himself returned, mounted.</ta>
            <ta e="T628" id="Seg_11143" s="T621">And seated the girl on the horse, mounted himself, they went.</ta>
            <ta e="T635" id="Seg_11144" s="T628">Then the king [says]: "Well, bring the horse!"</ta>
            <ta e="T640" id="Seg_11145" s="T635">The horse came and turned into a wolf.</ta>
            <ta e="T647" id="Seg_11146" s="T640">He got scared and fell down, and [the wolf] ran away.</ta>
            <ta e="T650" id="Seg_11147" s="T647">He catches up, says to Vanyushka:</ta>
            <ta e="T658" id="Seg_11148" s="T650">"Well, now I have nowhere to go with you, I will stay here.</ta>
            <ta e="T664" id="Seg_11149" s="T658">He jumped off, bowed to [the wolf] three times. [?]</ta>
            <ta e="T665" id="Seg_11150" s="T664">Stop!</ta>
            <ta e="T672" id="Seg_11151" s="T665">Then they came to their own land.</ta>
            <ta e="T675" id="Seg_11152" s="T672">He says: "Let's eat!"</ta>
            <ta e="T677" id="Seg_11153" s="T675">They sat down, they ate.</ta>
            <ta e="T681" id="Seg_11154" s="T677">They lied down to sleep.</ta>
            <ta e="T684" id="Seg_11155" s="T681">Then his brothers came.</ta>
            <ta e="T686" id="Seg_11156" s="T684">They killed him.</ta>
            <ta e="T690" id="Seg_11157" s="T686">And they themselves took the girl.</ta>
            <ta e="T695" id="Seg_11158" s="T690">And the bird, and the horse [too], [and] went [away].</ta>
            <ta e="T696" id="Seg_11159" s="T695">They are going.</ta>
            <ta e="T703" id="Seg_11160" s="T696">Then the wolf came, he sees: [Ivan] is lying.</ta>
            <ta e="T708" id="Seg_11161" s="T703">[The wolf] caught a bird, a crow, a small bird.</ta>
            <ta e="T715" id="Seg_11162" s="T708">"Go on, bring water of death and water of life!"</ta>
            <ta e="T719" id="Seg_11163" s="T715">The bird flew [away and] brought [the water].</ta>
            <ta e="T725" id="Seg_11164" s="T719">[The wolf] washed him with the water of death.</ta>
            <ta e="T729" id="Seg_11165" s="T725">And then with the water of life.</ta>
            <ta e="T734" id="Seg_11166" s="T729">He jumped up: "I must have slept for a long time."</ta>
            <ta e="T745" id="Seg_11167" s="T734">"You slept for a long time, if not for me, you would have slept forever.</ta>
            <ta e="T750" id="Seg_11168" s="T745">Your brothers killed you.</ta>
            <ta e="T758" id="Seg_11169" s="T750">And they took everything, the girl, and the bird, and the horse."</ta>
            <ta e="T761" id="Seg_11170" s="T758">Then [the wolf says]: "Mount me!"</ta>
            <ta e="T762" id="Seg_11171" s="T761">He mounted.</ta>
            <ta e="T765" id="Seg_11172" s="T762">They caught up with him [=with them].</ta>
            <ta e="T769" id="Seg_11173" s="T765">The wolf tore his brothers into pieces.</ta>
            <ta e="T778" id="Seg_11174" s="T769">And himself took the girl and everything there was.</ta>
            <ta e="T784" id="Seg_11175" s="T778">Then this Ivanushka fell down at his feet (bowed).</ta>
            <ta e="T787" id="Seg_11176" s="T784">And he returned home.</ta>
            <ta e="T792" id="Seg_11177" s="T787">He came to his father, he told everything.</ta>
            <ta e="T796" id="Seg_11178" s="T792">Then his father cried, felt sorry.</ta>
            <ta e="T800" id="Seg_11179" s="T796">Then he took the girl.</ta>
            <ta e="T801" id="Seg_11180" s="T800">They got married.</ta>
            <ta e="T803" id="Seg_11181" s="T801">They drank vodka.</ta>
            <ta e="T807" id="Seg_11182" s="T803">And now they are still living.</ta>
            <ta e="T808" id="Seg_11183" s="T807">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_11184" s="T0">Es lebte ein König.</ta>
            <ta e="T7" id="Seg_11185" s="T3">Er hatte drei Söhne.</ta>
            <ta e="T13" id="Seg_11186" s="T7">Er hatte einen Garten, Äpfel saßen [=wuchsen] dort, schöne.</ta>
            <ta e="T16" id="Seg_11187" s="T13">Irgendjemand stiehlt [sie].</ta>
            <ta e="T21" id="Seg_11188" s="T16">Er sieht, dass sie jemand stiehlt.</ta>
            <ta e="T24" id="Seg_11189" s="T21">Er stellte Leute auf, um zu schauen [=sie zu bewachen].</ta>
            <ta e="T27" id="Seg_11190" s="T24">Sie schauen, [dort ist] nichts.</ta>
            <ta e="T32" id="Seg_11191" s="T27">Dann sagt ein Sohn:</ta>
            <ta e="T34" id="Seg_11192" s="T32">"Ich gehe!"</ta>
            <ta e="T36" id="Seg_11193" s="T34">Er ging.</ta>
            <ta e="T37" id="Seg_11194" s="T36">Er schlief ein.</ta>
            <ta e="T40" id="Seg_11195" s="T37">Er sah nichts.</ta>
            <ta e="T45" id="Seg_11196" s="T40">Dann ging der nächste Sohn.</ta>
            <ta e="T47" id="Seg_11197" s="T45">Er schlief auch ein.</ta>
            <ta e="T50" id="Seg_11198" s="T47">Er sah nichts.</ta>
            <ta e="T53" id="Seg_11199" s="T50">Dann ging Iwanuschka.</ta>
            <ta e="T55" id="Seg_11200" s="T53">Er schläft nicht.</ta>
            <ta e="T65" id="Seg_11201" s="T55">Er wird schläfrig, wäscht seine Augen mit Wasser und geht weiter.</ta>
            <ta e="T71" id="Seg_11202" s="T65">Dann sieht er, er sah ein Feuer brennen.</ta>
            <ta e="T74" id="Seg_11203" s="T71">Er sah:</ta>
            <ta e="T77" id="Seg_11204" s="T74">Ein Vogel sitzt [dort].</ta>
            <ta e="T79" id="Seg_11205" s="T77">Wie Feuer.</ta>
            <ta e="T81" id="Seg_11206" s="T79">Er (griff?) [ihn].</ta>
            <ta e="T82" id="Seg_11207" s="T81">Er fing.</ta>
            <ta e="T86" id="Seg_11208" s="T82">[Der Vogel entkam], aber er gab [=der Sohn zog heraus] eine Feder.</ta>
            <ta e="T89" id="Seg_11209" s="T86">Dann kam er zu seinem Vater.</ta>
            <ta e="T92" id="Seg_11210" s="T89">"Nun, was, mein Sohn?"</ta>
            <ta e="T97" id="Seg_11211" s="T92">"Ich sah, wer [die Äpfel] stiehlt!</ta>
            <ta e="T101" id="Seg_11212" s="T97">Schau, ich habe dir eine Feder mitgebracht.</ta>
            <ta e="T108" id="Seg_11213" s="T101">Dieser Feuervogel geht [=fliegt] umher und stiehlt."</ta>
            <ta e="T112" id="Seg_11214" s="T108">Dann fing sein Vater an zu essen.</ta>
            <ta e="T118" id="Seg_11215" s="T112">Dann dachte er, er dachte: "Geht, [meine] Söhne, irgendwohin!</ta>
            <ta e="T121" id="Seg_11216" s="T118">Geht das ganze Land ab!</ta>
            <ta e="T136" id="Seg_11217" s="T121">Vielleicht werdet ihr irgendwo diesen Vogel fangen.</ta>
            <ta e="T138" id="Seg_11218" s="T136">Der [wie] Feuer [ist]."</ta>
            <ta e="T145" id="Seg_11219" s="T138">Sie gingen, einer dorthin, der andere hierhin.</ta>
            <ta e="T148" id="Seg_11220" s="T145">Vanjuschka ging auch.</ta>
            <ta e="T149" id="Seg_11221" s="T148">Stop.</ta>
            <ta e="T153" id="Seg_11222" s="T149">Dann ging dieser Iwan Zarewitsch.</ta>
            <ta e="T155" id="Seg_11223" s="T153">Er kam, er kam. [?]</ta>
            <ta e="T157" id="Seg_11224" s="T155">Er spannte sein Pferd an.</ta>
            <ta e="T161" id="Seg_11225" s="T157">Er aß, legte sich schlafen.</ta>
            <ta e="T163" id="Seg_11226" s="T161">Sein Pferd läuft [umher].</ta>
            <ta e="T165" id="Seg_11227" s="T163">Er band seine Füße fest.</ta>
            <ta e="T167" id="Seg_11228" s="T165">Er schlief lange.</ta>
            <ta e="T169" id="Seg_11229" s="T167">Dann stand er auf.</ta>
            <ta e="T172" id="Seg_11230" s="T169">Er sieht: Sein Pferd ist nicht da.</ta>
            <ta e="T174" id="Seg_11231" s="T172">Er suchte und suchte.</ta>
            <ta e="T177" id="Seg_11232" s="T174">Nur Knochen liegen herum.</ta>
            <ta e="T183" id="Seg_11233" s="T177">Er sitzt und dann muss er zu Fuß gehen.</ta>
            <ta e="T191" id="Seg_11234" s="T183">Er geht, er geht, dann wurde er müde und setzte sich, er sitzt.</ta>
            <ta e="T194" id="Seg_11235" s="T191">Dann läuft ein Wolf [umher].</ta>
            <ta e="T198" id="Seg_11236" s="T194">"Warum sitzt, warum lässt du den Kopf hängen?"</ta>
            <ta e="T202" id="Seg_11237" s="T198">"Mein Pferd wurde gefressen."</ta>
            <ta e="T206" id="Seg_11238" s="T202">"Ich habe es gefressen.</ta>
            <ta e="T212" id="Seg_11239" s="T206">Aber wo gehst du hin?"</ta>
            <ta e="T222" id="Seg_11240" s="T212">"Ich gehe und suche einen Vogel, mein Vater sagte: "Such den Vogel!</ta>
            <ta e="T224" id="Seg_11241" s="T222">[Der] wie Feuer ist.""</ta>
            <ta e="T236" id="Seg_11242" s="T224">Dann [sagt] er: "Nun, mit deinem Pferd wirst du in drei Jahren nicht dorthin kommen.</ta>
            <ta e="T239" id="Seg_11243" s="T236">Setz dich auf mich!</ta>
            <ta e="T242" id="Seg_11244" s="T239">Ich werde dich tragen."</ta>
            <ta e="T246" id="Seg_11245" s="T242">Er setzte sich auf ihn, dann brachte der ihn.</ta>
            <ta e="T247" id="Seg_11246" s="T246">"Geh!</ta>
            <ta e="T251" id="Seg_11247" s="T247">Die Leute schlafen jetzt.</ta>
            <ta e="T254" id="Seg_11248" s="T251">Nimm diesen Vogel!</ta>
            <ta e="T260" id="Seg_11249" s="T254">Aber nimm nicht seinen Käfig, sonst…"</ta>
            <ta e="T263" id="Seg_11250" s="T260">Dann ging er.</ta>
            <ta e="T265" id="Seg_11251" s="T263">Er nahm den Vogel.</ta>
            <ta e="T266" id="Seg_11252" s="T265">Er schaut.</ta>
            <ta e="T270" id="Seg_11253" s="T266">Der Käfig ist schön, [ich] sollte ihn mitnehmen.</ta>
            <ta e="T276" id="Seg_11254" s="T270">Als er ihn bewegte, fing er an zu lärmen.</ta>
            <ta e="T278" id="Seg_11255" s="T276">Er wurde gefangen.</ta>
            <ta e="T283" id="Seg_11256" s="T278">Und zum König gebracht.</ta>
            <ta e="T284" id="Seg_11257" s="T283">Stop.</ta>
            <ta e="T288" id="Seg_11258" s="T284">Dann kam er zum König.</ta>
            <ta e="T294" id="Seg_11259" s="T288">"Warum bist du hierher gekommen, um zu stehlen?</ta>
            <ta e="T297" id="Seg_11260" s="T294">Wessen Sohn bist du?"</ta>
            <ta e="T299" id="Seg_11261" s="T297">"Ich bin Iwan Zarewitsch."</ta>
            <ta e="T303" id="Seg_11262" s="T299">"Und so wolltest du stehlen?"</ta>
            <ta e="T306" id="Seg_11263" s="T303">Dann sagt [Iwan]:</ta>
            <ta e="T313" id="Seg_11264" s="T306">"Er kam zu uns fraß die Äpfel."</ta>
            <ta e="T326" id="Seg_11265" s="T313">"[Aber] wenn du gekommen wärst, hätte ich ihn dir so gegeben, aber du kamst, um zu stehlen.</ta>
            <ta e="T337" id="Seg_11266" s="T326">Nun, ich werde dir nichts tun, geh und bring das Pferd mit der goldenen Mähne!"</ta>
            <ta e="T341" id="Seg_11267" s="T337">Dann ging er zum Wolf.</ta>
            <ta e="T348" id="Seg_11268" s="T341">Der Wolf sagt: "Ich habe es dir gesagt, bewege [den Käfig] nicht.</ta>
            <ta e="T357" id="Seg_11269" s="T348">Aber du [hast ihn bewegt], und jetzt… setz dich auf mich, lass uns gehen."</ta>
            <ta e="T358" id="Seg_11270" s="T357">Sie gingen.</ta>
            <ta e="T361" id="Seg_11271" s="T358">Nun, sie kamen dorthin.</ta>
            <ta e="T364" id="Seg_11272" s="T361">Dann: "Nun, geh!</ta>
            <ta e="T377" id="Seg_11273" s="T364">Und fang das Pferd, aber nimm nicht das Halfter!</ta>
            <ta e="T381" id="Seg_11274" s="T377">Oder es wird wieder so sein!"</ta>
            <ta e="T383" id="Seg_11275" s="T381">Er kam.</ta>
            <ta e="T385" id="Seg_11276" s="T383">Er nahm das Pferd.</ta>
            <ta e="T389" id="Seg_11277" s="T385">Aber das Halfter ist sehr schön.</ta>
            <ta e="T394" id="Seg_11278" s="T389">Was ist auf dem (Pferd?), man muss es nur anlegen.</ta>
            <ta e="T396" id="Seg_11279" s="T394">Er nahm es.</ta>
            <ta e="T399" id="Seg_11280" s="T396">Es fing an zu klingen.</ta>
            <ta e="T403" id="Seg_11281" s="T399">Leute kamen angesprungen, sie nahmen ihn mit.</ta>
            <ta e="T406" id="Seg_11282" s="T403">Sie brachten ihn zum König.</ta>
            <ta e="T410" id="Seg_11283" s="T406">"Warum bist du gekommen, um das Pferd zu stehlen?</ta>
            <ta e="T412" id="Seg_11284" s="T410">Wessen [Sohn] bist du?"</ta>
            <ta e="T415" id="Seg_11285" s="T412">"Ich bin Iwan Zarewitsch."</ta>
            <ta e="T419" id="Seg_11286" s="T415">"Und so kamst du, um zu stehlen?</ta>
            <ta e="T426" id="Seg_11287" s="T419">Nun, geh, ich werde dir nichts tun.</ta>
            <ta e="T431" id="Seg_11288" s="T426">Es gibt ein Mädchen, Elena die Schöne.</ta>
            <ta e="T434" id="Seg_11289" s="T431">Bring sie zu mir!"</ta>
            <ta e="T438" id="Seg_11290" s="T434">Dann geht er zurück.</ta>
            <ta e="T447" id="Seg_11291" s="T438">Und der Wolf [sagt]: "Ich habe es [dir] gesagt: (fass?) es nicht an und du fässt immer [alles] an."</ta>
            <ta e="T450" id="Seg_11292" s="T447">Nun, nichts (?).</ta>
            <ta e="T455" id="Seg_11293" s="T450">Er sagt: "Nun, sitz auf, lass uns gehen."</ta>
            <ta e="T457" id="Seg_11294" s="T455">Sie kamen dorthin.</ta>
            <ta e="T461" id="Seg_11295" s="T457">Dann sagt der Wolf ihm:</ta>
            <ta e="T466" id="Seg_11296" s="T461">"Geh [nicht?], komm zurück, komm zurück!</ta>
            <ta e="T470" id="Seg_11297" s="T466">Ich gehe selbst."</ta>
            <ta e="T473" id="Seg_11298" s="T470">Dann sprang er [der Wolf] in den Garten.</ta>
            <ta e="T475" id="Seg_11299" s="T473">Versteckte sich dort.</ta>
            <ta e="T479" id="Seg_11300" s="T475">Sie sind dabei zu gehen, ihre Mutter, die Kindermädchen.</ta>
            <ta e="T482" id="Seg_11301" s="T479">Diese Elena blieb [alleine].</ta>
            <ta e="T492" id="Seg_11302" s="T482">Er griff sie und lief davon, er rennt und erreichte Iwan.</ta>
            <ta e="T493" id="Seg_11303" s="T492">"Sitz!</ta>
            <ta e="T494" id="Seg_11304" s="T493">Sie setzten sich.</ta>
            <ta e="T501" id="Seg_11305" s="T494">Dann kamen sie dorthin, wo sie nach dem Pferd suchten.</ta>
            <ta e="T509" id="Seg_11306" s="T501">Dann kamen sie [dorthin], aber er denkt immer noch, Iwan Zarewitsch.</ta>
            <ta e="T512" id="Seg_11307" s="T509">Er wollt das Mädchen nicht weggeben.</ta>
            <ta e="T515" id="Seg_11308" s="T512">Der Wolf fragt:</ta>
            <ta e="T519" id="Seg_11309" s="T515">"Warum stehst du hier?"</ta>
            <ta e="T523" id="Seg_11310" s="T519">"Sie ist so ein schönes Mädchen.</ta>
            <ta e="T527" id="Seg_11311" s="T523">Ich will [sie] nicht weggeben."</ta>
            <ta e="T536" id="Seg_11312" s="T527">"Nun, lass uns sie verstecken und ich selbst verwandele mich in sie."</ta>
            <ta e="T540" id="Seg_11313" s="T536">Dann machte er einen Purzelbaum [und verwandelte sich in das Mädchen].</ta>
            <ta e="T544" id="Seg_11314" s="T540">[Iwan Zarewitsch] nahm [den Wolf in der Gestalt von Elena], führte [sie zum Palast].</ta>
            <ta e="T549" id="Seg_11315" s="T544">Und er [der König] nahm das Mädchen.</ta>
            <ta e="T555" id="Seg_11316" s="T549">(…) sie tranken Wodka, sie tanzten, er gab sein Pferd.</ta>
            <ta e="T558" id="Seg_11317" s="T555">[Iwan] nahm sein Pferd.</ta>
            <ta e="T562" id="Seg_11318" s="T558">Er setzte seine Elena [auf das Pferd], bestieg es selbst.</ta>
            <ta e="T563" id="Seg_11319" s="T562">Sie ritten.</ta>
            <ta e="T568" id="Seg_11320" s="T563">Aber am Abend legten sich [der König und das Mädchen] schlafen.</ta>
            <ta e="T575" id="Seg_11321" s="T568">Diese Elena verwandelte sich in einen Wolf.</ta>
            <ta e="T580" id="Seg_11322" s="T575">Und [der König] erschrak und fiel hinunter [vom Bett].</ta>
            <ta e="T583" id="Seg_11323" s="T580">Und [der Wolf] lief davon.</ta>
            <ta e="T587" id="Seg_11324" s="T583">Dann erreicht er Iwanuschka und Elenuschka.</ta>
            <ta e="T591" id="Seg_11325" s="T587">"Warum stehst du hier?"</ta>
            <ta e="T597" id="Seg_11326" s="T591">"Nun, ich möchte das Pferd nicht weggeben, es ist [so] schön."</ta>
            <ta e="T604" id="Seg_11327" s="T597">Und er [der Wolf] sagt: "Versteck das Pferd und das Mädchen."</ta>
            <ta e="T611" id="Seg_11328" s="T604">Dann verwandelte er sich selbst in ein Pferd, [Iwan] nahm es.</ta>
            <ta e="T617" id="Seg_11329" s="T611">Er nahm den (brennenden?) Vogel, [der] wie Feuer [ist].</ta>
            <ta e="T621" id="Seg_11330" s="T617">Und er selbst kam zurück, kam zurück.</ta>
            <ta e="T628" id="Seg_11331" s="T621">Und setzte das Mädchen auf das Pferd, bestieg es selbst, sie ritten.</ta>
            <ta e="T635" id="Seg_11332" s="T628">Dann [sagt] der König: "Nun, bringt das Pferd!"</ta>
            <ta e="T640" id="Seg_11333" s="T635">Das Pferd kam und verwandelte sich in einen Wolf.</ta>
            <ta e="T647" id="Seg_11334" s="T640">Er erschrank und fiel hinunter, und [der Wolf] rannte davon.</ta>
            <ta e="T650" id="Seg_11335" s="T647">Er erreicht [ihn], sagt zu Wanjuschka:</ta>
            <ta e="T658" id="Seg_11336" s="T650">"Nun, jetzt muss ich nirgendwo mit euch hingehen, ich bleibe hier."</ta>
            <ta e="T664" id="Seg_11337" s="T658">Er sprang davon, verbeugte sich dreimal vor [dem Wolf]. [?]</ta>
            <ta e="T665" id="Seg_11338" s="T664">Stop!</ta>
            <ta e="T672" id="Seg_11339" s="T665">Dann kamen sie in ihr eigenes Land.</ta>
            <ta e="T675" id="Seg_11340" s="T672">Er sagt: "Lass uns essen!"</ta>
            <ta e="T677" id="Seg_11341" s="T675">Sie setzten sich, sie aßen.</ta>
            <ta e="T681" id="Seg_11342" s="T677">Sie legten sich schlafen.</ta>
            <ta e="T684" id="Seg_11343" s="T681">Dann kamen seine Brüder.</ta>
            <ta e="T686" id="Seg_11344" s="T684">Sie töteten ihn.</ta>
            <ta e="T690" id="Seg_11345" s="T686">Und sie selbst nahmen das Mädchen.</ta>
            <ta e="T695" id="Seg_11346" s="T690">Und den Vogel, und [auch] das Pferd, [und] sie gingen [weg].</ta>
            <ta e="T696" id="Seg_11347" s="T695">Sie gehen.</ta>
            <ta e="T703" id="Seg_11348" s="T696">Dann kam der Wolf, er sieht: [Iwan] liegt dort.</ta>
            <ta e="T708" id="Seg_11349" s="T703">[Der Wolf] find einen Vogel, eine Krähe, einen kleinen Vogel.</ta>
            <ta e="T715" id="Seg_11350" s="T708">"Geh, bring Todeswasser und Lebenswasser!"</ta>
            <ta e="T719" id="Seg_11351" s="T715">Der Vogel flog [davon und] brachte [das Wasser].</ta>
            <ta e="T725" id="Seg_11352" s="T719">[Der Wolf] wusch ihn mit dem Todeswasser.</ta>
            <ta e="T729" id="Seg_11353" s="T725">Und dann mit dem Lebenswasser.</ta>
            <ta e="T734" id="Seg_11354" s="T729">Er sprang auf: "Ich muss eine lange Zeit geschlafen haben."</ta>
            <ta e="T745" id="Seg_11355" s="T734">"Du hast lange geschlafen, wenn ich nicht wäre, würdest du für immer schlafen.</ta>
            <ta e="T750" id="Seg_11356" s="T745">Deine Brüder haben dich getötet.</ta>
            <ta e="T758" id="Seg_11357" s="T750">Und sie haben alles mitgenommen, das Mädchen, den Vogel und das Pferd."</ta>
            <ta e="T761" id="Seg_11358" s="T758">Dann [sagt der Wolf]: "Setz dich auf mich!"</ta>
            <ta e="T762" id="Seg_11359" s="T761">Er stieg auf.</ta>
            <ta e="T765" id="Seg_11360" s="T762">Sie erreichten ihn [=sie].</ta>
            <ta e="T769" id="Seg_11361" s="T765">Der Wolf zerriss seine Brüder.</ta>
            <ta e="T778" id="Seg_11362" s="T769">Und er selbst nahm das Mädchen und alles, was da war.</ta>
            <ta e="T784" id="Seg_11363" s="T778">Dann fiel dieser Iwanuschka auf seine Füße [er verbeugte sich].</ta>
            <ta e="T787" id="Seg_11364" s="T784">Und er kehrte nach Hause zurück.</ta>
            <ta e="T792" id="Seg_11365" s="T787">Er kam zu seinem Vater, er erzählte alles.</ta>
            <ta e="T796" id="Seg_11366" s="T792">Dann weinte sein Vater, es tat ihm leid.</ta>
            <ta e="T800" id="Seg_11367" s="T796">Dann nahm er das Mädchen.</ta>
            <ta e="T801" id="Seg_11368" s="T800">Sie heirateten.</ta>
            <ta e="T803" id="Seg_11369" s="T801">Sie tranken Wodka.</ta>
            <ta e="T807" id="Seg_11370" s="T803">Und jetzt leben sie immer noch.</ta>
            <ta e="T808" id="Seg_11371" s="T807">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_11372" s="T0">[GVY:] The Russian tale about the Fire Bird (Жар-птица), see e.g. http://www.planetaskazok.ru/rusnarskz/ivantsarevichiseryjvolkrusskz</ta>
            <ta e="T7" id="Seg_11373" s="T3">[KlT:] Coll. numeral form instead of card. (arch. Russian collective influence?).</ta>
            <ta e="T21" id="Seg_11374" s="T16">[KlT:] tojirbəlaʔbə: Unknown -bə- syllable (mistake? tojirbi- -laʔbə?).</ta>
            <ta e="T32" id="Seg_11375" s="T27">[GVY:] Unclear. Sounds rather like numən 'sky'.</ta>
            <ta e="T34" id="Seg_11376" s="T32">[GVY:] Rather kalam, as often in PKZ's speech.</ta>
            <ta e="T81" id="Seg_11377" s="T79">[KT:] müʔ- ’push, punch’; mün- ’bend down’? [GVY:] dĭn = dĭm? Unclear</ta>
            <ta e="T153" id="Seg_11378" s="T149">[GVY:] Tsarejevič</ta>
            <ta e="T157" id="Seg_11379" s="T155">[GVY:] Probably mistaken for kedərluʔpi "unharnessed"?</ta>
            <ta e="T172" id="Seg_11380" s="T169">[GVY:] self-correction ine (naga) -&gt; inet naga</ta>
            <ta e="T191" id="Seg_11381" s="T183">[GVY:] amnəlluʔpi? But that would be transitive.</ta>
            <ta e="T198" id="Seg_11382" s="T194">[KlT:] most likely edəbiom = edəbiol [hang-PST-2SG].</ta>
            <ta e="T202" id="Seg_11383" s="T198">[KlT:] An impersonal use of 3SG or an attempt at participle formation?</ta>
            <ta e="T206" id="Seg_11384" s="T202">[GVY:] A calque from the Russian cleft construction "Это я ее съел".</ta>
            <ta e="T236" id="Seg_11385" s="T224">[KlT:] = you would not have gotten there in three years</ta>
            <ta e="T294" id="Seg_11386" s="T288">[KlT:] šobiam 1SG instead of 2SG.</ta>
            <ta e="T313" id="Seg_11387" s="T306">3PL instead of 3SG.</ta>
            <ta e="T348" id="Seg_11388" s="T341">[KlT:] Currently unknown verb; ?? denom. form of Turk. iš ’matter’ as in ’bother’? &lt; loanword in Mator ’work’.</ta>
            <ta e="T377" id="Seg_11389" s="T364">[KlT:] Узда Ru. ’halter’; same unknown verb as before.</ta>
            <ta e="T394" id="Seg_11390" s="T389">[GVY:] Not fully clear.</ta>
            <ta e="T447" id="Seg_11391" s="T438">[KlT:] 1SG instead of 2SG</ta>
            <ta e="T450" id="Seg_11392" s="T447">[KlT:] An uninterpretable fragment.</ta>
            <ta e="T479" id="Seg_11393" s="T475">[KlT:] Mĭn+nzə DES ’they want to go’.</ta>
            <ta e="T494" id="Seg_11394" s="T493">[GVY:] Or probably 'They seated Elena"</ta>
            <ta e="T501" id="Seg_11395" s="T494">[KlT:] Pi- ’look for’.</ta>
            <ta e="T536" id="Seg_11396" s="T527">[KlT:] OPT.1DU without OPT marker.</ta>
            <ta e="T562" id="Seg_11397" s="T558">[AAV] amnol- instead of amnə-? For amnol-bə-bi, cf. PKZ_196X_SU0222.150 (150)</ta>
            <ta e="T587" id="Seg_11398" s="T583">[GVY:] the plural as a single suffix for two nouns?</ta>
            <ta e="T604" id="Seg_11399" s="T597">[KlT:] Intr. + RES instead of tr. formation.</ta>
            <ta e="T664" id="Seg_11400" s="T658">[KlT:] Dʼünə saʔmə- as ’bow’</ta>
            <ta e="T677" id="Seg_11401" s="T675">[KlT:] Irregular formation.</ta>
            <ta e="T715" id="Seg_11402" s="T708">Death water, life water.</ta>
            <ta e="T725" id="Seg_11403" s="T719">Küle ’dead’, productive participle.</ta>
            <ta e="T745" id="Seg_11404" s="T734">[KlT:] Ru. кабы ’if’ (in a conditional sentence).</ta>
            <ta e="T750" id="Seg_11405" s="T745">[KlT:] POSS.3SG instead of POSS.2SG.</ta>
            <ta e="T765" id="Seg_11406" s="T762">[KlT:] SG object instead of PL.</ta>
            <ta e="T769" id="Seg_11407" s="T765">[KlT:] Nüzə ’tear up’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
