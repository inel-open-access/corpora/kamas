<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID391F92D6-A1BD-E2B9-B76B-58E80B43A226">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\NN_1914_Birds_flk\NN_1914_Birds_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">24</ud-information>
            <ud-information attribute-name="# HIAT:w">19</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NN">
            <abbreviation>NN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T19" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Tʼüjüŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">sejenə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nörbəbie:</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">"</nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">Tăn</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">sagəššət</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">igel</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Nen</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">piʔmeendə</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">tülində</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">sĭktöleʔ</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">külel</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Măn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">toru</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">inen</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">toru</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">kunda</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">tora</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">toʔlaʔ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">kunnim</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip">"</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T19" id="Seg_69" n="sc" s="T0">
               <ts e="T1" id="Seg_71" n="e" s="T0">Tʼüjüŋ </ts>
               <ts e="T2" id="Seg_73" n="e" s="T1">sejenə </ts>
               <ts e="T3" id="Seg_75" n="e" s="T2">nörbəbie: </ts>
               <ts e="T4" id="Seg_77" n="e" s="T3">"Tăn </ts>
               <ts e="T5" id="Seg_79" n="e" s="T4">sagəššət </ts>
               <ts e="T6" id="Seg_81" n="e" s="T5">igel. </ts>
               <ts e="T7" id="Seg_83" n="e" s="T6">Nen </ts>
               <ts e="T8" id="Seg_85" n="e" s="T7">piʔmeendə </ts>
               <ts e="T9" id="Seg_87" n="e" s="T8">tülində </ts>
               <ts e="T10" id="Seg_89" n="e" s="T9">sĭktöleʔ </ts>
               <ts e="T11" id="Seg_91" n="e" s="T10">külel. </ts>
               <ts e="T12" id="Seg_93" n="e" s="T11">Măn </ts>
               <ts e="T13" id="Seg_95" n="e" s="T12">toru </ts>
               <ts e="T14" id="Seg_97" n="e" s="T13">inen </ts>
               <ts e="T15" id="Seg_99" n="e" s="T14">toru </ts>
               <ts e="T16" id="Seg_101" n="e" s="T15">kunda </ts>
               <ts e="T17" id="Seg_103" n="e" s="T16">tora </ts>
               <ts e="T18" id="Seg_105" n="e" s="T17">toʔlaʔ </ts>
               <ts e="T19" id="Seg_107" n="e" s="T18">kunnim." </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_108" s="T0">NN_1914_Birds_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_109" s="T6">NN_1914_Birds_flk.002 (001.003)</ta>
            <ta e="T19" id="Seg_110" s="T11">NN_1914_Birds_flk.003 (001.004)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_111" s="T0">Tʼüjüŋ sejenə nörbəbie: "Tăn sagəššət igel. </ta>
            <ta e="T11" id="Seg_112" s="T6">Nen piʔmeendə tülində sĭktöleʔ külel. </ta>
            <ta e="T19" id="Seg_113" s="T11">Măn toru inen toru kunda tora toʔlaʔ kunnim." </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_114" s="T0">tʼüjüŋ</ta>
            <ta e="T2" id="Seg_115" s="T1">seje-nə</ta>
            <ta e="T3" id="Seg_116" s="T2">nörbə-bie</ta>
            <ta e="T4" id="Seg_117" s="T3">tăn</ta>
            <ta e="T5" id="Seg_118" s="T4">sagəš-šət</ta>
            <ta e="T6" id="Seg_119" s="T5">i-ge-l</ta>
            <ta e="T7" id="Seg_120" s="T6">ne-n</ta>
            <ta e="T8" id="Seg_121" s="T7">piʔme-endə</ta>
            <ta e="T9" id="Seg_122" s="T8">tüli-ndə</ta>
            <ta e="T10" id="Seg_123" s="T9">sĭkt-ö-leʔ</ta>
            <ta e="T11" id="Seg_124" s="T10">kü-le-l</ta>
            <ta e="T12" id="Seg_125" s="T11">măn</ta>
            <ta e="T13" id="Seg_126" s="T12">toru</ta>
            <ta e="T14" id="Seg_127" s="T13">ine-n</ta>
            <ta e="T15" id="Seg_128" s="T14">toru</ta>
            <ta e="T16" id="Seg_129" s="T15">kun-da</ta>
            <ta e="T17" id="Seg_130" s="T16">tora</ta>
            <ta e="T18" id="Seg_131" s="T17">toʔ-laʔ</ta>
            <ta e="T19" id="Seg_132" s="T18">kun-ni-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_133" s="T0">tʼüjüŋ</ta>
            <ta e="T2" id="Seg_134" s="T1">seje-Tə</ta>
            <ta e="T3" id="Seg_135" s="T2">nörbə-bi</ta>
            <ta e="T4" id="Seg_136" s="T3">tăn</ta>
            <ta e="T5" id="Seg_137" s="T4">sagəš-žət</ta>
            <ta e="T6" id="Seg_138" s="T5">i-gA-l</ta>
            <ta e="T7" id="Seg_139" s="T6">ne-n</ta>
            <ta e="T8" id="Seg_140" s="T7">piʔme-gəndə</ta>
            <ta e="T9" id="Seg_141" s="T8">tüli-gəndə</ta>
            <ta e="T10" id="Seg_142" s="T9">sĭkt-o-lAʔ</ta>
            <ta e="T11" id="Seg_143" s="T10">kü-lV-l</ta>
            <ta e="T12" id="Seg_144" s="T11">măn</ta>
            <ta e="T13" id="Seg_145" s="T12">toru</ta>
            <ta e="T14" id="Seg_146" s="T13">ine-n</ta>
            <ta e="T15" id="Seg_147" s="T14">toru</ta>
            <ta e="T16" id="Seg_148" s="T15">kun-Tə</ta>
            <ta e="T17" id="Seg_149" s="T16">toru</ta>
            <ta e="T18" id="Seg_150" s="T17">toʔbdə-lAʔ</ta>
            <ta e="T19" id="Seg_151" s="T18">kun-lV-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_152" s="T0">black.grouse.[NOM.SG]</ta>
            <ta e="T2" id="Seg_153" s="T1">capercaillie-LAT</ta>
            <ta e="T3" id="Seg_154" s="T2">tell-PST.[3SG]</ta>
            <ta e="T4" id="Seg_155" s="T3">you.NOM</ta>
            <ta e="T5" id="Seg_156" s="T4">mind-CAR.ADJ</ta>
            <ta e="T6" id="Seg_157" s="T5">be-PRS-2SG</ta>
            <ta e="T7" id="Seg_158" s="T6">woman-GEN</ta>
            <ta e="T8" id="Seg_159" s="T7">pants-LAT/LOC.3SG</ta>
            <ta e="T9" id="Seg_160" s="T8">ribbon-LAT/LOC.3SG</ta>
            <ta e="T10" id="Seg_161" s="T9">choke-DETR-CVB</ta>
            <ta e="T11" id="Seg_162" s="T10">die-FUT-2SG</ta>
            <ta e="T12" id="Seg_163" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_164" s="T12">brown.[NOM.SG]</ta>
            <ta e="T14" id="Seg_165" s="T13">horse-GEN</ta>
            <ta e="T15" id="Seg_166" s="T14">brown.[NOM.SG]</ta>
            <ta e="T16" id="Seg_167" s="T15">mane-LAT</ta>
            <ta e="T17" id="Seg_168" s="T16">brown.[NOM.SG]</ta>
            <ta e="T18" id="Seg_169" s="T17">hit-CVB</ta>
            <ta e="T19" id="Seg_170" s="T18">bring-FUT-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_171" s="T0">черная.куропатка.[NOM.SG]</ta>
            <ta e="T2" id="Seg_172" s="T1">глухарь-LAT</ta>
            <ta e="T3" id="Seg_173" s="T2">сказать-PST.[3SG]</ta>
            <ta e="T4" id="Seg_174" s="T3">ты.NOM</ta>
            <ta e="T5" id="Seg_175" s="T4">ум-CAR.ADJ</ta>
            <ta e="T6" id="Seg_176" s="T5">быть-PRS-2SG</ta>
            <ta e="T7" id="Seg_177" s="T6">женщина-GEN</ta>
            <ta e="T8" id="Seg_178" s="T7">штаны-LAT/LOC.3SG</ta>
            <ta e="T9" id="Seg_179" s="T8">резинка-LAT/LOC.3SG</ta>
            <ta e="T10" id="Seg_180" s="T9">задушить-DETR-CVB</ta>
            <ta e="T11" id="Seg_181" s="T10">умереть-FUT-2SG</ta>
            <ta e="T12" id="Seg_182" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_183" s="T12">коричневый.[NOM.SG]</ta>
            <ta e="T14" id="Seg_184" s="T13">лошадь-GEN</ta>
            <ta e="T15" id="Seg_185" s="T14">коричневый.[NOM.SG]</ta>
            <ta e="T16" id="Seg_186" s="T15">грива-LAT</ta>
            <ta e="T17" id="Seg_187" s="T16">коричневый.[NOM.SG]</ta>
            <ta e="T18" id="Seg_188" s="T17">ударить-CVB</ta>
            <ta e="T19" id="Seg_189" s="T18">нести-FUT-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_190" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_191" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_192" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_193" s="T3">pers</ta>
            <ta e="T5" id="Seg_194" s="T4">n-n&gt;adj</ta>
            <ta e="T6" id="Seg_195" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_196" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_197" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_198" s="T8">n-n:case.poss</ta>
            <ta e="T10" id="Seg_199" s="T9">v-v&gt;v-v:n.fin</ta>
            <ta e="T11" id="Seg_200" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_201" s="T11">pers</ta>
            <ta e="T13" id="Seg_202" s="T12">adj-n:case</ta>
            <ta e="T14" id="Seg_203" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_204" s="T14">adj-n:case</ta>
            <ta e="T16" id="Seg_205" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_206" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_207" s="T17">v-v:n.fin</ta>
            <ta e="T19" id="Seg_208" s="T18">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_209" s="T0">n</ta>
            <ta e="T2" id="Seg_210" s="T1">n</ta>
            <ta e="T3" id="Seg_211" s="T2">v</ta>
            <ta e="T4" id="Seg_212" s="T3">pers</ta>
            <ta e="T5" id="Seg_213" s="T4">n</ta>
            <ta e="T6" id="Seg_214" s="T5">v</ta>
            <ta e="T7" id="Seg_215" s="T6">n</ta>
            <ta e="T8" id="Seg_216" s="T7">n</ta>
            <ta e="T9" id="Seg_217" s="T8">n</ta>
            <ta e="T10" id="Seg_218" s="T9">v</ta>
            <ta e="T11" id="Seg_219" s="T10">v</ta>
            <ta e="T12" id="Seg_220" s="T11">pers</ta>
            <ta e="T13" id="Seg_221" s="T12">adj</ta>
            <ta e="T14" id="Seg_222" s="T13">n</ta>
            <ta e="T15" id="Seg_223" s="T14">adj</ta>
            <ta e="T16" id="Seg_224" s="T15">n</ta>
            <ta e="T17" id="Seg_225" s="T16">adj</ta>
            <ta e="T18" id="Seg_226" s="T17">v</ta>
            <ta e="T19" id="Seg_227" s="T18">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_228" s="T0">np.h:A</ta>
            <ta e="T2" id="Seg_229" s="T1">np.h:R</ta>
            <ta e="T4" id="Seg_230" s="T3">pro.h:Th</ta>
            <ta e="T7" id="Seg_231" s="T6">np.h:Poss</ta>
            <ta e="T8" id="Seg_232" s="T7">np:G</ta>
            <ta e="T9" id="Seg_233" s="T8">np:G</ta>
            <ta e="T11" id="Seg_234" s="T10">0.2.h:E</ta>
            <ta e="T12" id="Seg_235" s="T11">pro.h:A</ta>
            <ta e="T14" id="Seg_236" s="T13">0.3:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_237" s="T0">np.h:S</ta>
            <ta e="T3" id="Seg_238" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_239" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_240" s="T4">adj:pred</ta>
            <ta e="T6" id="Seg_241" s="T5">cop</ta>
            <ta e="T10" id="Seg_242" s="T6">s:adv</ta>
            <ta e="T11" id="Seg_243" s="T10">v:pred 0.2.h:S</ta>
            <ta e="T12" id="Seg_244" s="T11">pro.h:S</ta>
            <ta e="T19" id="Seg_245" s="T18">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_246" s="T0">new </ta>
            <ta e="T2" id="Seg_247" s="T1">new </ta>
            <ta e="T3" id="Seg_248" s="T2">quot-sp</ta>
            <ta e="T4" id="Seg_249" s="T3">giv-active-Q </ta>
            <ta e="T8" id="Seg_250" s="T7">new-Q </ta>
            <ta e="T9" id="Seg_251" s="T8">new-Q </ta>
            <ta e="T11" id="Seg_252" s="T10">0.giv-active-Q</ta>
            <ta e="T12" id="Seg_253" s="T11">giv-active-Q </ta>
            <ta e="T16" id="Seg_254" s="T15">new-Q </ta>
            <ta e="T19" id="Seg_255" s="T18">0.giv-active-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_256" s="T0">Тетерев сказал глухарю: </ta>
            <ta e="T6" id="Seg_257" s="T3">"Ты глупец.</ta>
            <ta e="T11" id="Seg_258" s="T6">Задохнёшься, подвешенный к ремню на женских штанах.</ta>
            <ta e="T19" id="Seg_259" s="T11">Я [же] буду (украшать?) бурую морду гнедого коня." [?]</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_260" s="T0">The black grouse said to the capercaillie:</ta>
            <ta e="T6" id="Seg_261" s="T3">"You are crazy.</ta>
            <ta e="T11" id="Seg_262" s="T6">You will die strangled on a ribbon of a woman’s trousers.</ta>
            <ta e="T19" id="Seg_263" s="T11">I will beat brown on the brown mane of a brown horse." [?]</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_264" s="T0">Der Birkhahn sagte zum Auerhahn:</ta>
            <ta e="T6" id="Seg_265" s="T3">"Du bist verrückt.</ta>
            <ta e="T11" id="Seg_266" s="T6">Du wirst am Band einer Frauenhose hängend sterben.</ta>
            <ta e="T19" id="Seg_267" s="T11">Ich werde die braune Mähne eines braunen Pferdes braun prügeln.“ [?]</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T3" id="Seg_268" s="T0">Der Birkhahn sagte zum Auerhahn:</ta>
            <ta e="T6" id="Seg_269" s="T3">»Du bist verrückt,</ta>
            <ta e="T11" id="Seg_270" s="T6">am Bande eines Frauenrockes dich aufhängend stirbst du.</ta>
            <ta e="T19" id="Seg_271" s="T11">Ich zerreisse die braune Mähne des braunen Pferdes.»</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T19" id="Seg_272" s="T11">[AAV] Unclear.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
