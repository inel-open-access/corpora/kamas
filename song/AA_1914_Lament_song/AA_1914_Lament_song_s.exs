<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID684257C6-5194-BC7A-516B-A849381185AA">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\song\AA_1914_Lament_song\AA_1914_Lament_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">59</ud-information>
            <ud-information attribute-name="# HIAT:w">48</ud-information>
            <ud-information attribute-name="# e">48</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T48" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T47" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kijen</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">mĭlleʔbiem</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">sagər</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">măjazaŋbə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">iʔbəleʔ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kojobi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Mĭlleʔbəne</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">tʼüm</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">kük</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">noʔtsʼəʔ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">özerleʔ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">baʔbi</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Sagər</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">măjazaŋbə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">iʔbəleʔ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">kojobi</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">sĭri</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">tʼălamzaŋbə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">maʔlaʔ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">kojobiiʔ</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_72" n="HIAT:u" s="T20">
                  <ts e="T48" id="Seg_74" n="HIAT:w" s="T20">Küš</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T48">šalbə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">iʔbəleʔ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">kojobiiʔ</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_87" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">Iʔgö</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">örengetʼi</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">iʔbəleʔ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">maʔlaʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">kojobiam</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">Tuganbəgətʼi</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">tʼüržöleʔ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">kojobiam</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_117" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">Kijen</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">kolajlaʔbiam</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">tuzaŋbə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">maʔlaʔ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">kojobiiʔ</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_135" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">Tüjə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">dĭzeŋbə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">em</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">kuʔ</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_150" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">Iʔbəleʔ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">jadozaŋbə</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">teʔlaːmbi</ts>
                  <nts id="Seg_159" n="HIAT:ip">,</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">šüdöne</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">tüʔzeŋbə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">bar</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">tʼămaʔtolaːmbiiʔ</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T47" id="Seg_174" n="sc" s="T0">
               <ts e="T1" id="Seg_176" n="e" s="T0">Kijen </ts>
               <ts e="T2" id="Seg_178" n="e" s="T1">mĭlleʔbiem </ts>
               <ts e="T3" id="Seg_180" n="e" s="T2">sagər </ts>
               <ts e="T4" id="Seg_182" n="e" s="T3">măjazaŋbə </ts>
               <ts e="T5" id="Seg_184" n="e" s="T4">iʔbəleʔ </ts>
               <ts e="T6" id="Seg_186" n="e" s="T5">kojobi. </ts>
               <ts e="T7" id="Seg_188" n="e" s="T6">Mĭlleʔbəne </ts>
               <ts e="T8" id="Seg_190" n="e" s="T7">tʼüm </ts>
               <ts e="T9" id="Seg_192" n="e" s="T8">kük </ts>
               <ts e="T10" id="Seg_194" n="e" s="T9">noʔtsʼəʔ </ts>
               <ts e="T11" id="Seg_196" n="e" s="T10">özerleʔ </ts>
               <ts e="T12" id="Seg_198" n="e" s="T11">baʔbi. </ts>
               <ts e="T13" id="Seg_200" n="e" s="T12">Sagər </ts>
               <ts e="T14" id="Seg_202" n="e" s="T13">măjazaŋbə </ts>
               <ts e="T15" id="Seg_204" n="e" s="T14">iʔbəleʔ </ts>
               <ts e="T16" id="Seg_206" n="e" s="T15">kojobi, </ts>
               <ts e="T17" id="Seg_208" n="e" s="T16">sĭri </ts>
               <ts e="T18" id="Seg_210" n="e" s="T17">tʼălamzaŋbə </ts>
               <ts e="T19" id="Seg_212" n="e" s="T18">maʔlaʔ </ts>
               <ts e="T20" id="Seg_214" n="e" s="T19">kojobiiʔ. </ts>
               <ts e="T48" id="Seg_216" n="e" s="T20">Küš </ts>
               <ts e="T21" id="Seg_218" n="e" s="T48">šalbə </ts>
               <ts e="T22" id="Seg_220" n="e" s="T21">iʔbəleʔ </ts>
               <ts e="T23" id="Seg_222" n="e" s="T22">kojobiiʔ. </ts>
               <ts e="T24" id="Seg_224" n="e" s="T23">Iʔgö </ts>
               <ts e="T25" id="Seg_226" n="e" s="T24">örengetʼi </ts>
               <ts e="T26" id="Seg_228" n="e" s="T25">iʔbəleʔ </ts>
               <ts e="T27" id="Seg_230" n="e" s="T26">maʔlaʔ </ts>
               <ts e="T28" id="Seg_232" n="e" s="T27">kojobiam. </ts>
               <ts e="T29" id="Seg_234" n="e" s="T28">Tuganbəgətʼi </ts>
               <ts e="T30" id="Seg_236" n="e" s="T29">tʼüržöleʔ </ts>
               <ts e="T31" id="Seg_238" n="e" s="T30">kojobiam. </ts>
               <ts e="T32" id="Seg_240" n="e" s="T31">Kijen </ts>
               <ts e="T33" id="Seg_242" n="e" s="T32">kolajlaʔbiam </ts>
               <ts e="T34" id="Seg_244" n="e" s="T33">tuzaŋbə </ts>
               <ts e="T35" id="Seg_246" n="e" s="T34">maʔlaʔ </ts>
               <ts e="T36" id="Seg_248" n="e" s="T35">kojobiiʔ. </ts>
               <ts e="T37" id="Seg_250" n="e" s="T36">Tüjə </ts>
               <ts e="T38" id="Seg_252" n="e" s="T37">dĭzeŋbə </ts>
               <ts e="T39" id="Seg_254" n="e" s="T38">em </ts>
               <ts e="T40" id="Seg_256" n="e" s="T39">kuʔ. </ts>
               <ts e="T41" id="Seg_258" n="e" s="T40">Iʔbəleʔ </ts>
               <ts e="T42" id="Seg_260" n="e" s="T41">jadozaŋbə </ts>
               <ts e="T43" id="Seg_262" n="e" s="T42">teʔlaːmbi, </ts>
               <ts e="T44" id="Seg_264" n="e" s="T43">šüdöne </ts>
               <ts e="T45" id="Seg_266" n="e" s="T44">tüʔzeŋbə </ts>
               <ts e="T46" id="Seg_268" n="e" s="T45">bar </ts>
               <ts e="T47" id="Seg_270" n="e" s="T46">tʼămaʔtolaːmbiiʔ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_271" s="T0">AA_1914_Lament_song.001 (001.001)</ta>
            <ta e="T12" id="Seg_272" s="T6">AA_1914_Lament_song.002 (001.002)</ta>
            <ta e="T20" id="Seg_273" s="T12">AA_1914_Lament_song.003 (001.003)</ta>
            <ta e="T23" id="Seg_274" s="T20">AA_1914_Lament_song.004 (001.004)</ta>
            <ta e="T28" id="Seg_275" s="T23">AA_1914_Lament_song.005 (001.005)</ta>
            <ta e="T31" id="Seg_276" s="T28">AA_1914_Lament_song.006 (001.006)</ta>
            <ta e="T36" id="Seg_277" s="T31">AA_1914_Lament_song.007 (001.007)</ta>
            <ta e="T40" id="Seg_278" s="T36">AA_1914_Lament_song.008 (001.008)</ta>
            <ta e="T47" id="Seg_279" s="T40">AA_1914_Lament_song.009 (001.009)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_280" s="T0">Kijen mĭlleʔbiem sagər măjazaŋbə iʔbəleʔ kojobi. </ta>
            <ta e="T12" id="Seg_281" s="T6">Mĭlleʔbəne tʼüm kük noʔtsʼəʔ özerleʔ baʔbi. </ta>
            <ta e="T20" id="Seg_282" s="T12">Sagər măjazaŋbə iʔbəleʔ kojobi, sĭri tʼălamzaŋbə maʔlaʔ kojobiiʔ. </ta>
            <ta e="T23" id="Seg_283" s="T20">Küš šalbə iʔbəleʔ kojobiiʔ. </ta>
            <ta e="T28" id="Seg_284" s="T23">Iʔgö örengetʼi iʔbəleʔ maʔlaʔ kojobiam. </ta>
            <ta e="T31" id="Seg_285" s="T28">Tuganbəgətʼi tʼüržöleʔ kojobiam. </ta>
            <ta e="T36" id="Seg_286" s="T31">Kijen kolajlaʔbiam tuzaŋbə maʔlaʔ kojobiiʔ. </ta>
            <ta e="T40" id="Seg_287" s="T36">Tüjə dĭzeŋbə em kuʔ. </ta>
            <ta e="T47" id="Seg_288" s="T40">Iʔbəleʔ jadozaŋbə teʔlaːmbi, šüdöne tüʔzeŋbə bar tʼămaʔtolaːmbiiʔ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_289" s="T0">kijen</ta>
            <ta e="T2" id="Seg_290" s="T1">mĭl-leʔ-bie-m</ta>
            <ta e="T3" id="Seg_291" s="T2">sagər</ta>
            <ta e="T4" id="Seg_292" s="T3">măja-zaŋ-bə</ta>
            <ta e="T5" id="Seg_293" s="T4">iʔbə-leʔ</ta>
            <ta e="T6" id="Seg_294" s="T5">kojo-bi</ta>
            <ta e="T7" id="Seg_295" s="T6">mĭl-leʔbə-ne</ta>
            <ta e="T8" id="Seg_296" s="T7">tʼü-m</ta>
            <ta e="T9" id="Seg_297" s="T8">kük</ta>
            <ta e="T10" id="Seg_298" s="T9">noʔ-t-sʼəʔ</ta>
            <ta e="T11" id="Seg_299" s="T10">özer-leʔ</ta>
            <ta e="T12" id="Seg_300" s="T11">baʔ-bi</ta>
            <ta e="T13" id="Seg_301" s="T12">sagər</ta>
            <ta e="T14" id="Seg_302" s="T13">măja-zaŋ-bə</ta>
            <ta e="T15" id="Seg_303" s="T14">iʔbə-leʔ</ta>
            <ta e="T16" id="Seg_304" s="T15">kojo-bi</ta>
            <ta e="T17" id="Seg_305" s="T16">sĭri</ta>
            <ta e="T18" id="Seg_306" s="T17">tʼălam-zaŋ-bə</ta>
            <ta e="T19" id="Seg_307" s="T18">maʔ-laʔ</ta>
            <ta e="T20" id="Seg_308" s="T19">kojo-bi-iʔ</ta>
            <ta e="T48" id="Seg_309" s="T20">küš</ta>
            <ta e="T21" id="Seg_310" s="T48">šal-bə</ta>
            <ta e="T22" id="Seg_311" s="T21">iʔbə-leʔ</ta>
            <ta e="T23" id="Seg_312" s="T22">kojo-bi-iʔ</ta>
            <ta e="T24" id="Seg_313" s="T23">iʔgö</ta>
            <ta e="T25" id="Seg_314" s="T24">ören-getʼi</ta>
            <ta e="T26" id="Seg_315" s="T25">iʔbə-leʔ</ta>
            <ta e="T27" id="Seg_316" s="T26">maʔ-laʔ</ta>
            <ta e="T28" id="Seg_317" s="T27">kojo-bia-m</ta>
            <ta e="T29" id="Seg_318" s="T28">tugan-bə-gətʼi</ta>
            <ta e="T30" id="Seg_319" s="T29">tʼüržö-leʔ</ta>
            <ta e="T31" id="Seg_320" s="T30">kojo-bia-m</ta>
            <ta e="T32" id="Seg_321" s="T31">kijen</ta>
            <ta e="T33" id="Seg_322" s="T32">kola-j-laʔ-bia-m</ta>
            <ta e="T34" id="Seg_323" s="T33">tu-zaŋ-bə</ta>
            <ta e="T35" id="Seg_324" s="T34">maʔ-laʔ</ta>
            <ta e="T36" id="Seg_325" s="T35">kojo-bi-iʔ</ta>
            <ta e="T37" id="Seg_326" s="T36">tüjə</ta>
            <ta e="T38" id="Seg_327" s="T37">dĭ-zeŋ-bə</ta>
            <ta e="T39" id="Seg_328" s="T38">e-m</ta>
            <ta e="T40" id="Seg_329" s="T39">ku-ʔ</ta>
            <ta e="T41" id="Seg_330" s="T40">iʔbə-leʔ</ta>
            <ta e="T42" id="Seg_331" s="T41">jado-zaŋ-bə</ta>
            <ta e="T43" id="Seg_332" s="T42">teʔ-laːm-bi</ta>
            <ta e="T44" id="Seg_333" s="T43">šüd-ö-ne</ta>
            <ta e="T45" id="Seg_334" s="T44">tüʔ-zeŋ-bə</ta>
            <ta e="T46" id="Seg_335" s="T45">bar</ta>
            <ta e="T47" id="Seg_336" s="T46">tʼămaʔt-o-laːm-bi-iʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_337" s="T0">gijen</ta>
            <ta e="T2" id="Seg_338" s="T1">mĭn-laʔbə-bi-m</ta>
            <ta e="T3" id="Seg_339" s="T2">sagər</ta>
            <ta e="T4" id="Seg_340" s="T3">măja-zAŋ-m</ta>
            <ta e="T5" id="Seg_341" s="T4">iʔbə-lAʔ</ta>
            <ta e="T6" id="Seg_342" s="T5">kojo-bi</ta>
            <ta e="T7" id="Seg_343" s="T6">mĭn-laʔbə-NTA</ta>
            <ta e="T8" id="Seg_344" s="T7">tʼü-m</ta>
            <ta e="T9" id="Seg_345" s="T8">kük</ta>
            <ta e="T10" id="Seg_346" s="T9">noʔ-t-ziʔ</ta>
            <ta e="T11" id="Seg_347" s="T10">özer-lAʔ</ta>
            <ta e="T12" id="Seg_348" s="T11">baʔbdə-bi</ta>
            <ta e="T13" id="Seg_349" s="T12">sagər</ta>
            <ta e="T14" id="Seg_350" s="T13">măja-zAŋ-m</ta>
            <ta e="T15" id="Seg_351" s="T14">iʔbə-lAʔ</ta>
            <ta e="T16" id="Seg_352" s="T15">kojo-bi</ta>
            <ta e="T17" id="Seg_353" s="T16">sĭri</ta>
            <ta e="T18" id="Seg_354" s="T17">Dʼelam-zAŋ-m</ta>
            <ta e="T19" id="Seg_355" s="T18">ma-lAʔ</ta>
            <ta e="T20" id="Seg_356" s="T19">kojo-bi-jəʔ</ta>
            <ta e="T48" id="Seg_357" s="T20">küš</ta>
            <ta e="T21" id="Seg_358" s="T48">šal-m</ta>
            <ta e="T22" id="Seg_359" s="T21">iʔbə-lAʔ</ta>
            <ta e="T23" id="Seg_360" s="T22">kojo-bi-jəʔ</ta>
            <ta e="T24" id="Seg_361" s="T23">iʔgö</ta>
            <ta e="T25" id="Seg_362" s="T24">ören-gətʼi</ta>
            <ta e="T26" id="Seg_363" s="T25">iʔbə-lAʔ</ta>
            <ta e="T27" id="Seg_364" s="T26">ma-lAʔ</ta>
            <ta e="T28" id="Seg_365" s="T27">kojo-bi-m</ta>
            <ta e="T29" id="Seg_366" s="T28">tugan-m-gətʼi</ta>
            <ta e="T30" id="Seg_367" s="T29">tʼüržö-lAʔ</ta>
            <ta e="T31" id="Seg_368" s="T30">kojo-bi-m</ta>
            <ta e="T32" id="Seg_369" s="T31">gijen</ta>
            <ta e="T33" id="Seg_370" s="T32">kola-j-laʔbə-bi-m</ta>
            <ta e="T34" id="Seg_371" s="T33">tu-zAŋ-m</ta>
            <ta e="T35" id="Seg_372" s="T34">ma-lAʔ</ta>
            <ta e="T36" id="Seg_373" s="T35">kojo-bi-jəʔ</ta>
            <ta e="T37" id="Seg_374" s="T36">tüj</ta>
            <ta e="T38" id="Seg_375" s="T37">dĭ-zAŋ-m</ta>
            <ta e="T39" id="Seg_376" s="T38">e-m</ta>
            <ta e="T40" id="Seg_377" s="T39">ku-ʔ</ta>
            <ta e="T41" id="Seg_378" s="T40">iʔbə-lAʔ</ta>
            <ta e="T42" id="Seg_379" s="T41">jadə-zAŋ-m</ta>
            <ta e="T43" id="Seg_380" s="T42">teʔ-laːm-bi</ta>
            <ta e="T44" id="Seg_381" s="T43">šöʔ-o-NTA</ta>
            <ta e="T45" id="Seg_382" s="T44">töʔ-zAŋ-m</ta>
            <ta e="T46" id="Seg_383" s="T45">bar</ta>
            <ta e="T47" id="Seg_384" s="T46">tʼămaʔt-o-laːm-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_385" s="T0">where</ta>
            <ta e="T2" id="Seg_386" s="T1">go-DUR-PST-1SG</ta>
            <ta e="T3" id="Seg_387" s="T2">black.[NOM.SG]</ta>
            <ta e="T4" id="Seg_388" s="T3">mountain-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T5" id="Seg_389" s="T4">lie.down-CVB</ta>
            <ta e="T6" id="Seg_390" s="T5">stay-PST.[3SG]</ta>
            <ta e="T7" id="Seg_391" s="T6">go-DUR-PTCP</ta>
            <ta e="T8" id="Seg_392" s="T7">land-NOM/GEN/ACC.1SG</ta>
            <ta e="T9" id="Seg_393" s="T8">green.[NOM.SG]</ta>
            <ta e="T10" id="Seg_394" s="T9">grass-3SG-INS</ta>
            <ta e="T11" id="Seg_395" s="T10">grow-CVB</ta>
            <ta e="T12" id="Seg_396" s="T11">throw-PST.[3SG]</ta>
            <ta e="T13" id="Seg_397" s="T12">black.[NOM.SG]</ta>
            <ta e="T14" id="Seg_398" s="T13">mountain-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T15" id="Seg_399" s="T14">lie.down-CVB</ta>
            <ta e="T16" id="Seg_400" s="T15">stay-PST.[3SG]</ta>
            <ta e="T17" id="Seg_401" s="T16">white.[NOM.SG]</ta>
            <ta e="T18" id="Seg_402" s="T17">Sayan.mountains-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T19" id="Seg_403" s="T18">remain-CVB</ta>
            <ta e="T20" id="Seg_404" s="T19">stay-PST-3PL</ta>
            <ta e="T48" id="Seg_405" s="T20">power</ta>
            <ta e="T21" id="Seg_406" s="T48">strength-NOM/GEN/ACC.1SG</ta>
            <ta e="T22" id="Seg_407" s="T21">lie.down-CVB</ta>
            <ta e="T23" id="Seg_408" s="T22">stay-PST-3PL</ta>
            <ta e="T24" id="Seg_409" s="T23">many</ta>
            <ta e="T25" id="Seg_410" s="T24">kin-ABL.1SG</ta>
            <ta e="T26" id="Seg_411" s="T25">lie.down-CVB</ta>
            <ta e="T27" id="Seg_412" s="T26">remain-CVB</ta>
            <ta e="T28" id="Seg_413" s="T27">stay-PST-1SG</ta>
            <ta e="T29" id="Seg_414" s="T28">relative-NOM/GEN/ACC.1SG-ABL.1SG</ta>
            <ta e="T30" id="Seg_415" s="T29">stray-CVB</ta>
            <ta e="T31" id="Seg_416" s="T30">stay-PST-1SG</ta>
            <ta e="T32" id="Seg_417" s="T31">where</ta>
            <ta e="T33" id="Seg_418" s="T32">fish-VBLZ-DUR-PST-1SG</ta>
            <ta e="T34" id="Seg_419" s="T33">lake-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T35" id="Seg_420" s="T34">remain-CVB</ta>
            <ta e="T36" id="Seg_421" s="T35">stay-PST-3PL</ta>
            <ta e="T37" id="Seg_422" s="T36">now</ta>
            <ta e="T38" id="Seg_423" s="T37">this-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T39" id="Seg_424" s="T38">NEG.AUX-1SG</ta>
            <ta e="T40" id="Seg_425" s="T39">see-CNG</ta>
            <ta e="T41" id="Seg_426" s="T40">lie.down-CVB</ta>
            <ta e="T42" id="Seg_427" s="T41">tent.pole-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T43" id="Seg_428" s="T42">rot-RES-PST.[3SG]</ta>
            <ta e="T44" id="Seg_429" s="T43">sew-DETR-PTCP</ta>
            <ta e="T45" id="Seg_430" s="T44">tent.cover-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T46" id="Seg_431" s="T45">all</ta>
            <ta e="T47" id="Seg_432" s="T46">curl.up-DETR-RES-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_433" s="T0">где</ta>
            <ta e="T2" id="Seg_434" s="T1">идти-DUR-PST-1SG</ta>
            <ta e="T3" id="Seg_435" s="T2">черный.[NOM.SG]</ta>
            <ta e="T4" id="Seg_436" s="T3">гора-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T5" id="Seg_437" s="T4">ложиться-CVB</ta>
            <ta e="T6" id="Seg_438" s="T5">остаться-PST.[3SG]</ta>
            <ta e="T7" id="Seg_439" s="T6">идти-DUR-PTCP</ta>
            <ta e="T8" id="Seg_440" s="T7">земля-NOM/GEN/ACC.1SG</ta>
            <ta e="T9" id="Seg_441" s="T8">зеленый.[NOM.SG]</ta>
            <ta e="T10" id="Seg_442" s="T9">трава-3SG-INS</ta>
            <ta e="T11" id="Seg_443" s="T10">расти-CVB</ta>
            <ta e="T12" id="Seg_444" s="T11">бросить-PST.[3SG]</ta>
            <ta e="T13" id="Seg_445" s="T12">черный.[NOM.SG]</ta>
            <ta e="T14" id="Seg_446" s="T13">гора-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T15" id="Seg_447" s="T14">ложиться-CVB</ta>
            <ta e="T16" id="Seg_448" s="T15">остаться-PST.[3SG]</ta>
            <ta e="T17" id="Seg_449" s="T16">белый.[NOM.SG]</ta>
            <ta e="T18" id="Seg_450" s="T17">Саяны-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T19" id="Seg_451" s="T18">остаться-CVB</ta>
            <ta e="T20" id="Seg_452" s="T19">остаться-PST-3PL</ta>
            <ta e="T48" id="Seg_453" s="T20">мощь</ta>
            <ta e="T21" id="Seg_454" s="T48">сила-NOM/GEN/ACC.1SG</ta>
            <ta e="T22" id="Seg_455" s="T21">ложиться-CVB</ta>
            <ta e="T23" id="Seg_456" s="T22">остаться-PST-3PL</ta>
            <ta e="T24" id="Seg_457" s="T23">много</ta>
            <ta e="T25" id="Seg_458" s="T24">родство-ABL.1SG</ta>
            <ta e="T26" id="Seg_459" s="T25">ложиться-CVB</ta>
            <ta e="T27" id="Seg_460" s="T26">остаться-CVB</ta>
            <ta e="T28" id="Seg_461" s="T27">остаться-PST-1SG</ta>
            <ta e="T29" id="Seg_462" s="T28">родственник-NOM/GEN/ACC.1SG-ABL.1SG</ta>
            <ta e="T30" id="Seg_463" s="T29">заблудиться-CVB</ta>
            <ta e="T31" id="Seg_464" s="T30">остаться-PST-1SG</ta>
            <ta e="T32" id="Seg_465" s="T31">где</ta>
            <ta e="T33" id="Seg_466" s="T32">рыба-VBLZ-DUR-PST-1SG</ta>
            <ta e="T34" id="Seg_467" s="T33">озеро-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T35" id="Seg_468" s="T34">остаться-CVB</ta>
            <ta e="T36" id="Seg_469" s="T35">остаться-PST-3PL</ta>
            <ta e="T37" id="Seg_470" s="T36">сейчас</ta>
            <ta e="T38" id="Seg_471" s="T37">этот-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T39" id="Seg_472" s="T38">NEG.AUX-1SG</ta>
            <ta e="T40" id="Seg_473" s="T39">видеть-CNG</ta>
            <ta e="T41" id="Seg_474" s="T40">ложиться-CVB</ta>
            <ta e="T42" id="Seg_475" s="T41">шест-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T43" id="Seg_476" s="T42">красный-RES-PST.[3SG]</ta>
            <ta e="T44" id="Seg_477" s="T43">шить-DETR-PTCP</ta>
            <ta e="T45" id="Seg_478" s="T44">нюк-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T46" id="Seg_479" s="T45">весь</ta>
            <ta e="T47" id="Seg_480" s="T46">скручиваться-DETR-RES-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_481" s="T0">que</ta>
            <ta e="T2" id="Seg_482" s="T1">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_483" s="T2">adj-n:case</ta>
            <ta e="T4" id="Seg_484" s="T3">n-n:num-n:case.poss</ta>
            <ta e="T5" id="Seg_485" s="T4">v-v:n.fin</ta>
            <ta e="T6" id="Seg_486" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_487" s="T6">v-v&gt;v-v:n.fin</ta>
            <ta e="T8" id="Seg_488" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_489" s="T8">adj-n:case</ta>
            <ta e="T10" id="Seg_490" s="T9">n-n:case.poss-n:case</ta>
            <ta e="T11" id="Seg_491" s="T10">v-v:n.fin</ta>
            <ta e="T12" id="Seg_492" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_493" s="T12">adj-n:case</ta>
            <ta e="T14" id="Seg_494" s="T13">n-n:num-n:case.poss</ta>
            <ta e="T15" id="Seg_495" s="T14">v-v:n.fin</ta>
            <ta e="T16" id="Seg_496" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_497" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_498" s="T17">propr-n:num-n:case.poss</ta>
            <ta e="T19" id="Seg_499" s="T18">v-v:n.fin</ta>
            <ta e="T20" id="Seg_500" s="T19">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_501" s="T20">n</ta>
            <ta e="T21" id="Seg_502" s="T48">n-n:case.poss</ta>
            <ta e="T22" id="Seg_503" s="T21">v-v:n.fin</ta>
            <ta e="T23" id="Seg_504" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_505" s="T23">quant</ta>
            <ta e="T25" id="Seg_506" s="T24">n-n:case.poss</ta>
            <ta e="T26" id="Seg_507" s="T25">v-v:n.fin</ta>
            <ta e="T27" id="Seg_508" s="T26">v-v:n.fin</ta>
            <ta e="T28" id="Seg_509" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_510" s="T28">n-n:case.poss-n:case.poss</ta>
            <ta e="T30" id="Seg_511" s="T29">v-v:n.fin</ta>
            <ta e="T31" id="Seg_512" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_513" s="T31">que</ta>
            <ta e="T33" id="Seg_514" s="T32">n-n&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_515" s="T33">n-n:num-n:case.poss</ta>
            <ta e="T35" id="Seg_516" s="T34">v-v:n.fin</ta>
            <ta e="T36" id="Seg_517" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_518" s="T36">adv</ta>
            <ta e="T38" id="Seg_519" s="T37">dempro-n:num-n:case.poss</ta>
            <ta e="T39" id="Seg_520" s="T38">aux-v:pn</ta>
            <ta e="T40" id="Seg_521" s="T39">v-v:n.fin</ta>
            <ta e="T41" id="Seg_522" s="T40">v-v:n.fin</ta>
            <ta e="T42" id="Seg_523" s="T41">n-n:num-n:case.poss</ta>
            <ta e="T43" id="Seg_524" s="T42">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_525" s="T43">v-v&gt;v-v:n.fin</ta>
            <ta e="T45" id="Seg_526" s="T44">n-n:num-n:case.poss</ta>
            <ta e="T46" id="Seg_527" s="T45">quant</ta>
            <ta e="T47" id="Seg_528" s="T46">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_529" s="T0">que</ta>
            <ta e="T2" id="Seg_530" s="T1">v</ta>
            <ta e="T3" id="Seg_531" s="T2">adj</ta>
            <ta e="T4" id="Seg_532" s="T3">n</ta>
            <ta e="T5" id="Seg_533" s="T4">v</ta>
            <ta e="T6" id="Seg_534" s="T5">v</ta>
            <ta e="T7" id="Seg_535" s="T6">adj</ta>
            <ta e="T8" id="Seg_536" s="T7">n</ta>
            <ta e="T9" id="Seg_537" s="T8">adj</ta>
            <ta e="T10" id="Seg_538" s="T9">n</ta>
            <ta e="T11" id="Seg_539" s="T10">v</ta>
            <ta e="T12" id="Seg_540" s="T11">v</ta>
            <ta e="T13" id="Seg_541" s="T12">adj</ta>
            <ta e="T14" id="Seg_542" s="T13">n</ta>
            <ta e="T15" id="Seg_543" s="T14">v</ta>
            <ta e="T16" id="Seg_544" s="T15">v</ta>
            <ta e="T17" id="Seg_545" s="T16">adj</ta>
            <ta e="T18" id="Seg_546" s="T17">propr</ta>
            <ta e="T19" id="Seg_547" s="T18">v</ta>
            <ta e="T20" id="Seg_548" s="T19">v</ta>
            <ta e="T48" id="Seg_549" s="T20">n</ta>
            <ta e="T21" id="Seg_550" s="T48">n</ta>
            <ta e="T22" id="Seg_551" s="T21">v</ta>
            <ta e="T23" id="Seg_552" s="T22">v</ta>
            <ta e="T24" id="Seg_553" s="T23">quant</ta>
            <ta e="T25" id="Seg_554" s="T24">n</ta>
            <ta e="T26" id="Seg_555" s="T25">v</ta>
            <ta e="T27" id="Seg_556" s="T26">v</ta>
            <ta e="T28" id="Seg_557" s="T27">v</ta>
            <ta e="T29" id="Seg_558" s="T28">n</ta>
            <ta e="T30" id="Seg_559" s="T29">v</ta>
            <ta e="T31" id="Seg_560" s="T30">v</ta>
            <ta e="T32" id="Seg_561" s="T31">que</ta>
            <ta e="T33" id="Seg_562" s="T32">v</ta>
            <ta e="T34" id="Seg_563" s="T33">n</ta>
            <ta e="T35" id="Seg_564" s="T34">v</ta>
            <ta e="T36" id="Seg_565" s="T35">v</ta>
            <ta e="T37" id="Seg_566" s="T36">adv</ta>
            <ta e="T38" id="Seg_567" s="T37">dempro</ta>
            <ta e="T39" id="Seg_568" s="T38">aux</ta>
            <ta e="T40" id="Seg_569" s="T39">v</ta>
            <ta e="T41" id="Seg_570" s="T40">v</ta>
            <ta e="T42" id="Seg_571" s="T41">n</ta>
            <ta e="T43" id="Seg_572" s="T42">v</ta>
            <ta e="T44" id="Seg_573" s="T43">adj</ta>
            <ta e="T45" id="Seg_574" s="T44">n</ta>
            <ta e="T46" id="Seg_575" s="T45">quant</ta>
            <ta e="T47" id="Seg_576" s="T46">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_577" s="T1">0.1.h:Th</ta>
            <ta e="T4" id="Seg_578" s="T3">np:Th 0.1.h:Poss</ta>
            <ta e="T8" id="Seg_579" s="T7">np:Th 0.1.h:Poss</ta>
            <ta e="T14" id="Seg_580" s="T13">np:Th 0.1.h:Poss</ta>
            <ta e="T18" id="Seg_581" s="T17">np:Th 0.1.h:Poss</ta>
            <ta e="T21" id="Seg_582" s="T20">np:Th 0.1.h:Poss</ta>
            <ta e="T25" id="Seg_583" s="T24">np:So 0.1.h:Poss</ta>
            <ta e="T28" id="Seg_584" s="T27">0.1.h:Th</ta>
            <ta e="T29" id="Seg_585" s="T28">np:So 0.1.h:Poss</ta>
            <ta e="T31" id="Seg_586" s="T30">0.1.h:Th</ta>
            <ta e="T33" id="Seg_587" s="T32">0.1.h:A</ta>
            <ta e="T34" id="Seg_588" s="T33">np:Th 0.1.h:Poss</ta>
            <ta e="T37" id="Seg_589" s="T36">adv:Time</ta>
            <ta e="T38" id="Seg_590" s="T37">pro:Th 0.1.h:Poss</ta>
            <ta e="T39" id="Seg_591" s="T38">0.1.h:E</ta>
            <ta e="T42" id="Seg_592" s="T41">np:Th 0.1.h:Poss</ta>
            <ta e="T45" id="Seg_593" s="T44">np:Th 0.1.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_594" s="T1">v:pred 0.1.h:S</ta>
            <ta e="T4" id="Seg_595" s="T3">np:S</ta>
            <ta e="T6" id="Seg_596" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_597" s="T6">s:adv</ta>
            <ta e="T8" id="Seg_598" s="T7">np:S</ta>
            <ta e="T12" id="Seg_599" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_600" s="T13">np:S</ta>
            <ta e="T16" id="Seg_601" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_602" s="T17">np:S</ta>
            <ta e="T20" id="Seg_603" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_604" s="T20">np:S</ta>
            <ta e="T23" id="Seg_605" s="T22">v:pred</ta>
            <ta e="T28" id="Seg_606" s="T27">v:pred 0.1.h:S</ta>
            <ta e="T31" id="Seg_607" s="T30">v:pred 0.1.h:S</ta>
            <ta e="T33" id="Seg_608" s="T32">v:pred 0.1.h:S</ta>
            <ta e="T34" id="Seg_609" s="T33">np:S</ta>
            <ta e="T36" id="Seg_610" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_611" s="T37">pro:O</ta>
            <ta e="T39" id="Seg_612" s="T38">v:pred 0.1.h:S</ta>
            <ta e="T41" id="Seg_613" s="T40">s:adv</ta>
            <ta e="T42" id="Seg_614" s="T41">np:S</ta>
            <ta e="T43" id="Seg_615" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_616" s="T44">np:S</ta>
            <ta e="T47" id="Seg_617" s="T46">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_618" s="T1">0.new</ta>
            <ta e="T4" id="Seg_619" s="T3">new</ta>
            <ta e="T8" id="Seg_620" s="T7">accs-gen </ta>
            <ta e="T10" id="Seg_621" s="T9">accs-gen </ta>
            <ta e="T14" id="Seg_622" s="T13">giv-inactive </ta>
            <ta e="T18" id="Seg_623" s="T17">accs-gen </ta>
            <ta e="T21" id="Seg_624" s="T20">new </ta>
            <ta e="T25" id="Seg_625" s="T24">accs-gen </ta>
            <ta e="T28" id="Seg_626" s="T27">0.giv-active</ta>
            <ta e="T29" id="Seg_627" s="T28">giv-inactive </ta>
            <ta e="T31" id="Seg_628" s="T30">0.giv-active</ta>
            <ta e="T33" id="Seg_629" s="T32">0.giv-active</ta>
            <ta e="T34" id="Seg_630" s="T33">accs-gen </ta>
            <ta e="T38" id="Seg_631" s="T37">accs-aggr </ta>
            <ta e="T39" id="Seg_632" s="T38">0.giv-active</ta>
            <ta e="T42" id="Seg_633" s="T41">new </ta>
            <ta e="T45" id="Seg_634" s="T44">accs-sit </ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_635" s="T8">TAT:core</ta>
            <ta e="T29" id="Seg_636" s="T28">TURK:core</ta>
            <ta e="T46" id="Seg_637" s="T45">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_638" s="T0">Where I was going, my black mountains stayed behind</ta>
            <ta e="T12" id="Seg_639" s="T6">The land which I walked was grown over with green grass</ta>
            <ta e="T20" id="Seg_640" s="T12">My black mountains stayed behind, my white Sayan mountains were left behind.</ta>
            <ta e="T23" id="Seg_641" s="T20">My strength and power were left behind.</ta>
            <ta e="T28" id="Seg_642" s="T23">From many kin I’m left over.</ta>
            <ta e="T31" id="Seg_643" s="T28">From my relatives I strayed.</ta>
            <ta e="T36" id="Seg_644" s="T31">Where I used to fish, my lakes stayed behind.</ta>
            <ta e="T40" id="Seg_645" s="T36">Now I don’t see these of mine.</ta>
            <ta e="T47" id="Seg_646" s="T40">My tent poles have rotten forever, my tent covers sewn [from birchbark] have all curled up.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_647" s="T0">Wo ich wandelte, blieben meine schwarzen Berge zurück.</ta>
            <ta e="T12" id="Seg_648" s="T6">Auf dem Land, das ich beschritt, wuchs grünes Gras.</ta>
            <ta e="T20" id="Seg_649" s="T12">Meine schwarzen Berge blieben zurück, meine weißen sajanischen Berge blieben zurück.</ta>
            <ta e="T23" id="Seg_650" s="T20">Meine Kraft und meine Stärke blieben zurück.</ta>
            <ta e="T28" id="Seg_651" s="T23">Von vielen Verwandten bin ich übrig geblieben.</ta>
            <ta e="T31" id="Seg_652" s="T28">[Fern?] von meinen Angehörigen blieb ich umherirrend zurück.</ta>
            <ta e="T36" id="Seg_653" s="T31">Wo ich fischte, blieben meine Seen zurück.</ta>
            <ta e="T40" id="Seg_654" s="T36">Jetzt sehe ich diese meinigen nicht.</ta>
            <ta e="T47" id="Seg_655" s="T40">Meine Zeltstangen sind morsch geworden, meine [aus Birkenrinde] genähten Zeltwände haben sich alle aufgerollt.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T6" id="Seg_656" s="T0">Wo ich schritt, die schwarzen Berge ja ⌈liegend⌉ blieben zurück.</ta>
            <ta e="T12" id="Seg_657" s="T6">Mein beschrittenes Land trug ⌈= wuchs⌉ goldenes Gras.</ta>
            <ta e="T20" id="Seg_658" s="T12">Die schwarzen Berge ja blieben ⌈liegend⌉ zurück6, die Weissen Berge (=das Sajanische Gebirge) ja blieben zurück.</ta>
            <ta e="T23" id="Seg_659" s="T20">Unsere Kräfte blieben zurück.</ta>
            <ta e="T28" id="Seg_660" s="T23">Von einem grossen ⌈= vielen⌉ Stamm blieb ich zurück. </ta>
            <ta e="T31" id="Seg_661" s="T28">Von meinen Angehörigen blieb ich irregehend zurück.</ta>
            <ta e="T36" id="Seg_662" s="T31">Wo ich fischte, meine Seen blieben zurück.</ta>
            <ta e="T40" id="Seg_663" s="T36">Jetzt sehe ich sie nicht.</ta>
            <ta e="T47" id="Seg_664" s="T40">⌈Liegend⌉ auch die Stützstangen des Zeltes wurden morsch, auch die genähten Rindenscheiben alle sich ringelten.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_665" s="T0">Там где я ходил, остались мои черные горы.</ta>
            <ta e="T12" id="Seg_666" s="T6">Земля, по которой я ходил, зеленой травой заросла.</ta>
            <ta e="T20" id="Seg_667" s="T12">Мои черные горы остались позади, мои Саянские горы остались позади.</ta>
            <ta e="T23" id="Seg_668" s="T20">Моя сила и мощь остались в прошлом. </ta>
            <ta e="T28" id="Seg_669" s="T23">От большого рода я один остался.</ta>
            <ta e="T31" id="Seg_670" s="T28">От близких я отбился.</ta>
            <ta e="T36" id="Seg_671" s="T31">Там, где я рыбу ловил, озера мои остались.</ta>
            <ta e="T40" id="Seg_672" s="T36">Не видать их мне больше.</ta>
            <ta e="T47" id="Seg_673" s="T40">Жерди моего чума сгнили, сшитые [из бересты] стены все сморщились.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T48" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
