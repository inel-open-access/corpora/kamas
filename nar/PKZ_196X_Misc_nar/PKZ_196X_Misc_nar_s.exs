<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4F621ECA-379B-8C52-31B0-501508A9C564">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Misc_nar.wav" />
         <referenced-file url="PKZ_196X_Misc_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_Misc_nar\PKZ_196X_Misc_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">59</ud-information>
            <ud-information attribute-name="# HIAT:w">40</ud-information>
            <ud-information attribute-name="# e">40</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.021" type="appl" />
         <tli id="T1" time="1.122" type="appl" />
         <tli id="T2" time="2.5932460828861026" />
         <tli id="T3" time="3.376" type="appl" />
         <tli id="T4" time="4.196" type="appl" />
         <tli id="T5" time="5.015" type="appl" />
         <tli id="T6" time="5.749" type="appl" />
         <tli id="T7" time="6.482" type="appl" />
         <tli id="T8" time="7.216" type="appl" />
         <tli id="T9" time="10.172991060370675" />
         <tli id="T10" time="10.869" type="appl" />
         <tli id="T11" time="11.646" type="appl" />
         <tli id="T12" time="12.424" type="appl" />
         <tli id="T13" time="13.201" type="appl" />
         <tli id="T14" time="13.979" type="appl" />
         <tli id="T15" time="14.471" type="appl" />
         <tli id="T16" time="14.926" type="appl" />
         <tli id="T17" time="15.382" type="appl" />
         <tli id="T18" time="15.837" type="appl" />
         <tli id="T19" time="16.525" type="appl" />
         <tli id="T20" time="17.212" type="appl" />
         <tli id="T21" time="17.9" type="appl" />
         <tli id="T22" time="18.588" type="appl" />
         <tli id="T23" time="19.275" type="appl" />
         <tli id="T24" time="19.963" type="appl" />
         <tli id="T25" time="20.651" type="appl" />
         <tli id="T26" time="21.338" type="appl" />
         <tli id="T27" time="22.525908776535072" />
         <tli id="T28" time="25.06582332044151" />
         <tli id="T29" time="25.758" type="appl" />
         <tli id="T30" time="26.524" type="appl" />
         <tli id="T31" time="27.29" type="appl" />
         <tli id="T32" time="28.057" type="appl" />
         <tli id="T33" time="28.972" type="appl" />
         <tli id="T34" time="29.886" type="appl" />
         <tli id="T35" time="32.2789139674409" />
         <tli id="T36" time="33.602" type="appl" />
         <tli id="T37" time="35.59213582655246" />
         <tli id="T38" time="36.741" type="appl" />
         <tli id="T39" time="37.547" type="appl" />
         <tli id="T40" time="38.352" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T40" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Šödördə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">markaʔi</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Măn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">šödörluʔpiem</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">markaʔi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">I</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">tăn</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">šödördə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">markaʔi</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Davaj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">tănziʔ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_45" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">ša-</ts>
                  <nts id="Seg_48" n="HIAT:ip">)</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_51" n="HIAT:w" s="T12">sanə</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">lejzittə</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_58" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">A</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">iʔgö</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">dĭn</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">sanəʔi</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_73" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">Iʔgö</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_78" n="HIAT:ip">(</nts>
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">a-</ts>
                  <nts id="Seg_81" n="HIAT:ip">)</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_84" n="HIAT:w" s="T20">amnaʔ</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_86" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">dĭ</ts>
                  <nts id="Seg_89" n="HIAT:ip">)</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">lejeʔ</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">a</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">măn</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">lejleʔbəm</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">sanə</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_109" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">Kabarləj</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_115" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_117" n="HIAT:w" s="T28">Măn</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">băldəbiom</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">măn</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_126" n="HIAT:w" s="T31">üjüzaŋdə</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_130" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_132" n="HIAT:w" s="T32">Măn</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_135" n="HIAT:w" s="T33">băldəbiom</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">udazaŋbə</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_142" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_144" n="HIAT:w" s="T35">Măn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_147" n="HIAT:w" s="T36">băldəlaʔbəm</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_151" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_153" n="HIAT:w" s="T37">Iʔ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_156" n="HIAT:w" s="T38">băldəlaʔ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_159" n="HIAT:w" s="T39">üjütsiʔ</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T40" id="Seg_162" n="sc" s="T0">
               <ts e="T1" id="Seg_164" n="e" s="T0">Šödördə </ts>
               <ts e="T2" id="Seg_166" n="e" s="T1">markaʔi. </ts>
               <ts e="T3" id="Seg_168" n="e" s="T2">Măn </ts>
               <ts e="T4" id="Seg_170" n="e" s="T3">šödörluʔpiem </ts>
               <ts e="T5" id="Seg_172" n="e" s="T4">markaʔi. </ts>
               <ts e="T6" id="Seg_174" n="e" s="T5">I </ts>
               <ts e="T7" id="Seg_176" n="e" s="T6">tăn </ts>
               <ts e="T8" id="Seg_178" n="e" s="T7">šödördə </ts>
               <ts e="T9" id="Seg_180" n="e" s="T8">markaʔi. </ts>
               <ts e="T10" id="Seg_182" n="e" s="T9">Davaj </ts>
               <ts e="T11" id="Seg_184" n="e" s="T10">tănziʔ </ts>
               <ts e="T12" id="Seg_186" n="e" s="T11">(ša-) </ts>
               <ts e="T13" id="Seg_188" n="e" s="T12">sanə </ts>
               <ts e="T14" id="Seg_190" n="e" s="T13">lejzittə. </ts>
               <ts e="T15" id="Seg_192" n="e" s="T14">A </ts>
               <ts e="T16" id="Seg_194" n="e" s="T15">iʔgö </ts>
               <ts e="T17" id="Seg_196" n="e" s="T16">dĭn </ts>
               <ts e="T18" id="Seg_198" n="e" s="T17">sanəʔi. </ts>
               <ts e="T19" id="Seg_200" n="e" s="T18">Iʔgö, </ts>
               <ts e="T20" id="Seg_202" n="e" s="T19">(a-) </ts>
               <ts e="T21" id="Seg_204" n="e" s="T20">amnaʔ </ts>
               <ts e="T22" id="Seg_206" n="e" s="T21">(dĭ) </ts>
               <ts e="T23" id="Seg_208" n="e" s="T22">lejeʔ, </ts>
               <ts e="T24" id="Seg_210" n="e" s="T23">a </ts>
               <ts e="T25" id="Seg_212" n="e" s="T24">măn </ts>
               <ts e="T26" id="Seg_214" n="e" s="T25">lejleʔbəm </ts>
               <ts e="T27" id="Seg_216" n="e" s="T26">sanə. </ts>
               <ts e="T28" id="Seg_218" n="e" s="T27">Kabarləj. </ts>
               <ts e="T29" id="Seg_220" n="e" s="T28">Măn </ts>
               <ts e="T30" id="Seg_222" n="e" s="T29">băldəbiom </ts>
               <ts e="T31" id="Seg_224" n="e" s="T30">măn </ts>
               <ts e="T32" id="Seg_226" n="e" s="T31">üjüzaŋdə. </ts>
               <ts e="T33" id="Seg_228" n="e" s="T32">Măn </ts>
               <ts e="T34" id="Seg_230" n="e" s="T33">băldəbiom </ts>
               <ts e="T35" id="Seg_232" n="e" s="T34">udazaŋbə. </ts>
               <ts e="T36" id="Seg_234" n="e" s="T35">Măn </ts>
               <ts e="T37" id="Seg_236" n="e" s="T36">băldəlaʔbəm. </ts>
               <ts e="T38" id="Seg_238" n="e" s="T37">Iʔ </ts>
               <ts e="T39" id="Seg_240" n="e" s="T38">băldəlaʔ </ts>
               <ts e="T40" id="Seg_242" n="e" s="T39">üjütsiʔ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_243" s="T0">PKZ_196X_Misc_nar.001 (002)</ta>
            <ta e="T5" id="Seg_244" s="T2">PKZ_196X_Misc_nar.002 (003)</ta>
            <ta e="T9" id="Seg_245" s="T5">PKZ_196X_Misc_nar.003 (004)</ta>
            <ta e="T14" id="Seg_246" s="T9">PKZ_196X_Misc_nar.004 (005)</ta>
            <ta e="T18" id="Seg_247" s="T14">PKZ_196X_Misc_nar.005 (006)</ta>
            <ta e="T27" id="Seg_248" s="T18">PKZ_196X_Misc_nar.006 (007)</ta>
            <ta e="T28" id="Seg_249" s="T27">PKZ_196X_Misc_nar.007 (008)</ta>
            <ta e="T32" id="Seg_250" s="T28">PKZ_196X_Misc_nar.008 (009)</ta>
            <ta e="T35" id="Seg_251" s="T32">PKZ_196X_Misc_nar.009 (010)</ta>
            <ta e="T37" id="Seg_252" s="T35">PKZ_196X_Misc_nar.010 (011)</ta>
            <ta e="T40" id="Seg_253" s="T37">PKZ_196X_Misc_nar.011 (012)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_254" s="T0">Šödördə markaʔi. </ta>
            <ta e="T5" id="Seg_255" s="T2">Măn šödörluʔpiem markaʔi. </ta>
            <ta e="T9" id="Seg_256" s="T5">I tăn šödördə markaʔi. </ta>
            <ta e="T14" id="Seg_257" s="T9">Davaj tănziʔ (ša-) sanə lejzittə. </ta>
            <ta e="T18" id="Seg_258" s="T14">A iʔgö dĭn sanəʔi. </ta>
            <ta e="T27" id="Seg_259" s="T18">Iʔgö, (a-) amnaʔ (dĭ) lejeʔ, a măn lejleʔbəm sanə. </ta>
            <ta e="T28" id="Seg_260" s="T27">Kabarləj. </ta>
            <ta e="T32" id="Seg_261" s="T28">Măn băldəbiom măn üjüzaŋdə. </ta>
            <ta e="T35" id="Seg_262" s="T32">Măn băldəbiom udazaŋbə. </ta>
            <ta e="T37" id="Seg_263" s="T35">Măn băldəlaʔbəm. </ta>
            <ta e="T40" id="Seg_264" s="T37">Iʔ băldəlaʔ üjütsiʔ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_265" s="T0">šödör-də</ta>
            <ta e="T2" id="Seg_266" s="T1">marka-ʔi</ta>
            <ta e="T3" id="Seg_267" s="T2">măn</ta>
            <ta e="T4" id="Seg_268" s="T3">šödör-luʔ-pie-m</ta>
            <ta e="T5" id="Seg_269" s="T4">marka-ʔi</ta>
            <ta e="T6" id="Seg_270" s="T5">i</ta>
            <ta e="T7" id="Seg_271" s="T6">tăn</ta>
            <ta e="T8" id="Seg_272" s="T7">šödör-də</ta>
            <ta e="T9" id="Seg_273" s="T8">marka-ʔi</ta>
            <ta e="T10" id="Seg_274" s="T9">davaj</ta>
            <ta e="T11" id="Seg_275" s="T10">tăn-ziʔ</ta>
            <ta e="T13" id="Seg_276" s="T12">sanə</ta>
            <ta e="T14" id="Seg_277" s="T13">lej-zittə</ta>
            <ta e="T15" id="Seg_278" s="T14">a</ta>
            <ta e="T16" id="Seg_279" s="T15">iʔgö</ta>
            <ta e="T17" id="Seg_280" s="T16">dĭn</ta>
            <ta e="T18" id="Seg_281" s="T17">sanə-ʔi</ta>
            <ta e="T19" id="Seg_282" s="T18">iʔgö</ta>
            <ta e="T21" id="Seg_283" s="T20">amna-ʔ</ta>
            <ta e="T22" id="Seg_284" s="T21">dĭ</ta>
            <ta e="T23" id="Seg_285" s="T22">lej-e-ʔ</ta>
            <ta e="T24" id="Seg_286" s="T23">a</ta>
            <ta e="T25" id="Seg_287" s="T24">măn</ta>
            <ta e="T26" id="Seg_288" s="T25">lej-leʔbə-m</ta>
            <ta e="T27" id="Seg_289" s="T26">sanə</ta>
            <ta e="T28" id="Seg_290" s="T27">kabarləj</ta>
            <ta e="T29" id="Seg_291" s="T28">măn</ta>
            <ta e="T30" id="Seg_292" s="T29">băldə-bio-m</ta>
            <ta e="T31" id="Seg_293" s="T30">măn</ta>
            <ta e="T32" id="Seg_294" s="T31">üjü-zaŋ-də</ta>
            <ta e="T33" id="Seg_295" s="T32">măn</ta>
            <ta e="T34" id="Seg_296" s="T33">băldə-bio-m</ta>
            <ta e="T35" id="Seg_297" s="T34">uda-zaŋ-bə</ta>
            <ta e="T36" id="Seg_298" s="T35">măn</ta>
            <ta e="T37" id="Seg_299" s="T36">băldə-laʔbə-m</ta>
            <ta e="T38" id="Seg_300" s="T37">i-ʔ</ta>
            <ta e="T39" id="Seg_301" s="T38">băldə-laʔ</ta>
            <ta e="T40" id="Seg_302" s="T39">üjü-t-siʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_303" s="T0">šödör-t</ta>
            <ta e="T2" id="Seg_304" s="T1">marka-jəʔ</ta>
            <ta e="T3" id="Seg_305" s="T2">măn</ta>
            <ta e="T4" id="Seg_306" s="T3">šödör-luʔbdə-bi-m</ta>
            <ta e="T5" id="Seg_307" s="T4">marka-jəʔ</ta>
            <ta e="T6" id="Seg_308" s="T5">i</ta>
            <ta e="T7" id="Seg_309" s="T6">tăn</ta>
            <ta e="T8" id="Seg_310" s="T7">šödör-t</ta>
            <ta e="T9" id="Seg_311" s="T8">marka-jəʔ</ta>
            <ta e="T10" id="Seg_312" s="T9">davaj</ta>
            <ta e="T11" id="Seg_313" s="T10">tăn-ziʔ</ta>
            <ta e="T13" id="Seg_314" s="T12">sanə</ta>
            <ta e="T14" id="Seg_315" s="T13">lej-zittə</ta>
            <ta e="T15" id="Seg_316" s="T14">a</ta>
            <ta e="T16" id="Seg_317" s="T15">iʔgö</ta>
            <ta e="T17" id="Seg_318" s="T16">dĭn</ta>
            <ta e="T18" id="Seg_319" s="T17">sanə-jəʔ</ta>
            <ta e="T19" id="Seg_320" s="T18">iʔgö</ta>
            <ta e="T21" id="Seg_321" s="T20">amnə-ʔ</ta>
            <ta e="T22" id="Seg_322" s="T21">dĭ</ta>
            <ta e="T23" id="Seg_323" s="T22">lej-ə-ʔ</ta>
            <ta e="T24" id="Seg_324" s="T23">a</ta>
            <ta e="T25" id="Seg_325" s="T24">măn</ta>
            <ta e="T26" id="Seg_326" s="T25">lej-laʔbə-m</ta>
            <ta e="T27" id="Seg_327" s="T26">sanə</ta>
            <ta e="T28" id="Seg_328" s="T27">kabarləj</ta>
            <ta e="T29" id="Seg_329" s="T28">măn</ta>
            <ta e="T30" id="Seg_330" s="T29">băldə-bi-m</ta>
            <ta e="T31" id="Seg_331" s="T30">măn</ta>
            <ta e="T32" id="Seg_332" s="T31">üjü-zAŋ-də</ta>
            <ta e="T33" id="Seg_333" s="T32">măn</ta>
            <ta e="T34" id="Seg_334" s="T33">băldə-bi-m</ta>
            <ta e="T35" id="Seg_335" s="T34">uda-zAŋ-m</ta>
            <ta e="T36" id="Seg_336" s="T35">măn</ta>
            <ta e="T37" id="Seg_337" s="T36">băldə-laʔbə-m</ta>
            <ta e="T38" id="Seg_338" s="T37">e-ʔ</ta>
            <ta e="T39" id="Seg_339" s="T38">băldə-lAʔ</ta>
            <ta e="T40" id="Seg_340" s="T39">üjü-t-ziʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_341" s="T0">sew-IMP.2SG.O</ta>
            <ta e="T2" id="Seg_342" s="T1">button-PL</ta>
            <ta e="T3" id="Seg_343" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_344" s="T3">sew-MOM-PST-1SG</ta>
            <ta e="T5" id="Seg_345" s="T4">button-PL</ta>
            <ta e="T6" id="Seg_346" s="T5">and</ta>
            <ta e="T7" id="Seg_347" s="T6">you.NOM</ta>
            <ta e="T8" id="Seg_348" s="T7">sew-IMP.2SG.O</ta>
            <ta e="T9" id="Seg_349" s="T8">button-PL</ta>
            <ta e="T10" id="Seg_350" s="T9">HORT</ta>
            <ta e="T11" id="Seg_351" s="T10">you.NOM-INS</ta>
            <ta e="T13" id="Seg_352" s="T12">pine.nut.[NOM.SG]</ta>
            <ta e="T14" id="Seg_353" s="T13">crack-INF.LAT</ta>
            <ta e="T15" id="Seg_354" s="T14">and</ta>
            <ta e="T16" id="Seg_355" s="T15">many</ta>
            <ta e="T17" id="Seg_356" s="T16">there</ta>
            <ta e="T18" id="Seg_357" s="T17">pine.nut-PL</ta>
            <ta e="T19" id="Seg_358" s="T18">many</ta>
            <ta e="T21" id="Seg_359" s="T20">sit.down-IMP.2SG</ta>
            <ta e="T22" id="Seg_360" s="T21">this.[NOM.SG]</ta>
            <ta e="T23" id="Seg_361" s="T22">crack-EP-IMP.2SG</ta>
            <ta e="T24" id="Seg_362" s="T23">and</ta>
            <ta e="T25" id="Seg_363" s="T24">I.NOM</ta>
            <ta e="T26" id="Seg_364" s="T25">crack-DUR-1SG</ta>
            <ta e="T27" id="Seg_365" s="T26">pine.nut.[NOM.SG]</ta>
            <ta e="T28" id="Seg_366" s="T27">enough</ta>
            <ta e="T29" id="Seg_367" s="T28">I.NOM</ta>
            <ta e="T30" id="Seg_368" s="T29">break-PST-1SG</ta>
            <ta e="T31" id="Seg_369" s="T30">I.NOM</ta>
            <ta e="T32" id="Seg_370" s="T31">foot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T33" id="Seg_371" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_372" s="T33">break-PST-1SG</ta>
            <ta e="T35" id="Seg_373" s="T34">hand-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T36" id="Seg_374" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_375" s="T36">break-DUR-1SG</ta>
            <ta e="T38" id="Seg_376" s="T37">NEG.AUX-IMP.2SG</ta>
            <ta e="T39" id="Seg_377" s="T38">break-2PL</ta>
            <ta e="T40" id="Seg_378" s="T39">foot-3SG-INS</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_379" s="T0">шить-IMP.2SG.O</ta>
            <ta e="T2" id="Seg_380" s="T1">пуговица-PL</ta>
            <ta e="T3" id="Seg_381" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_382" s="T3">шить-MOM-PST-1SG</ta>
            <ta e="T5" id="Seg_383" s="T4">пуговица-PL</ta>
            <ta e="T6" id="Seg_384" s="T5">и</ta>
            <ta e="T7" id="Seg_385" s="T6">ты.NOM</ta>
            <ta e="T8" id="Seg_386" s="T7">шить-IMP.2SG.O</ta>
            <ta e="T9" id="Seg_387" s="T8">пуговица-PL</ta>
            <ta e="T10" id="Seg_388" s="T9">HORT</ta>
            <ta e="T11" id="Seg_389" s="T10">ты.NOM-INS</ta>
            <ta e="T13" id="Seg_390" s="T12">кедровый.орех.[NOM.SG]</ta>
            <ta e="T14" id="Seg_391" s="T13">лущить-INF.LAT</ta>
            <ta e="T15" id="Seg_392" s="T14">а</ta>
            <ta e="T16" id="Seg_393" s="T15">много</ta>
            <ta e="T17" id="Seg_394" s="T16">там</ta>
            <ta e="T18" id="Seg_395" s="T17">кедровый.орех-PL</ta>
            <ta e="T19" id="Seg_396" s="T18">много</ta>
            <ta e="T21" id="Seg_397" s="T20">сесть-IMP.2SG</ta>
            <ta e="T22" id="Seg_398" s="T21">этот.[NOM.SG]</ta>
            <ta e="T23" id="Seg_399" s="T22">лущить-EP-IMP.2SG</ta>
            <ta e="T24" id="Seg_400" s="T23">а</ta>
            <ta e="T25" id="Seg_401" s="T24">я.NOM</ta>
            <ta e="T26" id="Seg_402" s="T25">лущить-DUR-1SG</ta>
            <ta e="T27" id="Seg_403" s="T26">кедровый.орех.[NOM.SG]</ta>
            <ta e="T28" id="Seg_404" s="T27">хватит</ta>
            <ta e="T29" id="Seg_405" s="T28">я.NOM</ta>
            <ta e="T30" id="Seg_406" s="T29">сломать-PST-1SG</ta>
            <ta e="T31" id="Seg_407" s="T30">я.NOM</ta>
            <ta e="T32" id="Seg_408" s="T31">нога-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T33" id="Seg_409" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_410" s="T33">сломать-PST-1SG</ta>
            <ta e="T35" id="Seg_411" s="T34">рука-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T36" id="Seg_412" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_413" s="T36">сломать-DUR-1SG</ta>
            <ta e="T38" id="Seg_414" s="T37">NEG.AUX-IMP.2SG</ta>
            <ta e="T39" id="Seg_415" s="T38">сломать-2PL</ta>
            <ta e="T40" id="Seg_416" s="T39">нога-3SG-INS</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_417" s="T0">v-v:mood.pn</ta>
            <ta e="T2" id="Seg_418" s="T1">n-n:num</ta>
            <ta e="T3" id="Seg_419" s="T2">pers</ta>
            <ta e="T4" id="Seg_420" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_421" s="T4">n-n:num</ta>
            <ta e="T6" id="Seg_422" s="T5">conj</ta>
            <ta e="T7" id="Seg_423" s="T6">pers</ta>
            <ta e="T8" id="Seg_424" s="T7">v-v:mood.pn</ta>
            <ta e="T9" id="Seg_425" s="T8">n-n:num</ta>
            <ta e="T10" id="Seg_426" s="T9">v</ta>
            <ta e="T11" id="Seg_427" s="T10">pers-n:case</ta>
            <ta e="T13" id="Seg_428" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_429" s="T13">v-v:n.fin</ta>
            <ta e="T15" id="Seg_430" s="T14">conj</ta>
            <ta e="T16" id="Seg_431" s="T15">quant</ta>
            <ta e="T17" id="Seg_432" s="T16">adv</ta>
            <ta e="T18" id="Seg_433" s="T17">n-n:num</ta>
            <ta e="T19" id="Seg_434" s="T18">quant</ta>
            <ta e="T21" id="Seg_435" s="T20">v-v:mood.pn</ta>
            <ta e="T22" id="Seg_436" s="T21">dempro-n:case</ta>
            <ta e="T23" id="Seg_437" s="T22">v-v:ins-v:mood.pn</ta>
            <ta e="T24" id="Seg_438" s="T23">conj</ta>
            <ta e="T25" id="Seg_439" s="T24">pers</ta>
            <ta e="T26" id="Seg_440" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_441" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_442" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_443" s="T28">pers</ta>
            <ta e="T30" id="Seg_444" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_445" s="T30">pers</ta>
            <ta e="T32" id="Seg_446" s="T31">n-n:num-n:case.poss</ta>
            <ta e="T33" id="Seg_447" s="T32">pers</ta>
            <ta e="T34" id="Seg_448" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_449" s="T34">n-n:num-n:case.poss</ta>
            <ta e="T36" id="Seg_450" s="T35">pers</ta>
            <ta e="T37" id="Seg_451" s="T36">v-v&gt;v-v:pn</ta>
            <ta e="T38" id="Seg_452" s="T37">aux-v:mood.pn</ta>
            <ta e="T39" id="Seg_453" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_454" s="T39">n-n:case.poss-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_455" s="T0">v</ta>
            <ta e="T2" id="Seg_456" s="T1">n</ta>
            <ta e="T3" id="Seg_457" s="T2">pers</ta>
            <ta e="T4" id="Seg_458" s="T3">v</ta>
            <ta e="T5" id="Seg_459" s="T4">n</ta>
            <ta e="T6" id="Seg_460" s="T5">conj</ta>
            <ta e="T7" id="Seg_461" s="T6">pers</ta>
            <ta e="T8" id="Seg_462" s="T7">v</ta>
            <ta e="T9" id="Seg_463" s="T8">n</ta>
            <ta e="T10" id="Seg_464" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_465" s="T10">pers</ta>
            <ta e="T13" id="Seg_466" s="T12">n</ta>
            <ta e="T14" id="Seg_467" s="T13">v</ta>
            <ta e="T15" id="Seg_468" s="T14">conj</ta>
            <ta e="T16" id="Seg_469" s="T15">quant</ta>
            <ta e="T17" id="Seg_470" s="T16">adv</ta>
            <ta e="T18" id="Seg_471" s="T17">n</ta>
            <ta e="T19" id="Seg_472" s="T18">quant</ta>
            <ta e="T21" id="Seg_473" s="T20">v</ta>
            <ta e="T22" id="Seg_474" s="T21">dempro</ta>
            <ta e="T23" id="Seg_475" s="T22">v</ta>
            <ta e="T24" id="Seg_476" s="T23">conj</ta>
            <ta e="T25" id="Seg_477" s="T24">pers</ta>
            <ta e="T26" id="Seg_478" s="T25">v</ta>
            <ta e="T27" id="Seg_479" s="T26">n</ta>
            <ta e="T28" id="Seg_480" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_481" s="T28">pers</ta>
            <ta e="T30" id="Seg_482" s="T29">v</ta>
            <ta e="T31" id="Seg_483" s="T30">pers</ta>
            <ta e="T32" id="Seg_484" s="T31">n</ta>
            <ta e="T33" id="Seg_485" s="T32">pers</ta>
            <ta e="T34" id="Seg_486" s="T33">v</ta>
            <ta e="T35" id="Seg_487" s="T34">n</ta>
            <ta e="T36" id="Seg_488" s="T35">pers</ta>
            <ta e="T37" id="Seg_489" s="T36">v</ta>
            <ta e="T38" id="Seg_490" s="T37">aux</ta>
            <ta e="T39" id="Seg_491" s="T38">v</ta>
            <ta e="T40" id="Seg_492" s="T39">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_493" s="T0">0.2.h:A</ta>
            <ta e="T2" id="Seg_494" s="T1">np:Th</ta>
            <ta e="T3" id="Seg_495" s="T2">pro.h:A</ta>
            <ta e="T5" id="Seg_496" s="T4">np:Th</ta>
            <ta e="T7" id="Seg_497" s="T6">pro.h:A</ta>
            <ta e="T9" id="Seg_498" s="T8">np:Th</ta>
            <ta e="T11" id="Seg_499" s="T10">pro.h:Com</ta>
            <ta e="T13" id="Seg_500" s="T12">np:P</ta>
            <ta e="T18" id="Seg_501" s="T17">np:Th</ta>
            <ta e="T21" id="Seg_502" s="T20">0.2.h:A</ta>
            <ta e="T22" id="Seg_503" s="T21">pro:P</ta>
            <ta e="T23" id="Seg_504" s="T22">0.2.h:A</ta>
            <ta e="T25" id="Seg_505" s="T24">pro.h:A</ta>
            <ta e="T27" id="Seg_506" s="T26">np:P</ta>
            <ta e="T29" id="Seg_507" s="T28">pro.h:E</ta>
            <ta e="T31" id="Seg_508" s="T30">pro.h:Poss</ta>
            <ta e="T32" id="Seg_509" s="T31">np:P</ta>
            <ta e="T33" id="Seg_510" s="T32">pro.h:E</ta>
            <ta e="T35" id="Seg_511" s="T34">np:P 0.1.h:Poss</ta>
            <ta e="T36" id="Seg_512" s="T35">pro.h:E</ta>
            <ta e="T38" id="Seg_513" s="T37">0.2.h:E</ta>
            <ta e="T40" id="Seg_514" s="T39">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_515" s="T0">v:pred 0.2.h:S</ta>
            <ta e="T2" id="Seg_516" s="T1">np:O</ta>
            <ta e="T3" id="Seg_517" s="T2">pro.h:S</ta>
            <ta e="T4" id="Seg_518" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_519" s="T4">np:O</ta>
            <ta e="T7" id="Seg_520" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_521" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_522" s="T8">np:O</ta>
            <ta e="T10" id="Seg_523" s="T9">ptcl:pred</ta>
            <ta e="T13" id="Seg_524" s="T12">np:O</ta>
            <ta e="T18" id="Seg_525" s="T17">np:S</ta>
            <ta e="T21" id="Seg_526" s="T20">v:pred 0.2.h:S</ta>
            <ta e="T22" id="Seg_527" s="T21">pro:O</ta>
            <ta e="T23" id="Seg_528" s="T22">v:pred 0.2.h:S</ta>
            <ta e="T25" id="Seg_529" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_530" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_531" s="T26">np:O</ta>
            <ta e="T29" id="Seg_532" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_533" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_534" s="T31">np:O</ta>
            <ta e="T33" id="Seg_535" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_536" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_537" s="T34">np:O</ta>
            <ta e="T36" id="Seg_538" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_539" s="T36">v:pred</ta>
            <ta e="T38" id="Seg_540" s="T37">v:pred 0.2.h:S</ta>
            <ta e="T40" id="Seg_541" s="T39">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_542" s="T5">RUS:gram</ta>
            <ta e="T10" id="Seg_543" s="T9">RUS:gram</ta>
            <ta e="T15" id="Seg_544" s="T14">RUS:gram</ta>
            <ta e="T24" id="Seg_545" s="T23">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_546" s="T0">Пришивай пуговицы.</ta>
            <ta e="T5" id="Seg_547" s="T2">Я шила пуговицы.</ta>
            <ta e="T9" id="Seg_548" s="T5">И ты пришивай пуговицы.</ta>
            <ta e="T14" id="Seg_549" s="T9">Давай с тобой лущить орехи.</ta>
            <ta e="T18" id="Seg_550" s="T14">Там много орехов.</ta>
            <ta e="T27" id="Seg_551" s="T18">Много, садись, лущи их, и я буду лущить орехи. [?]</ta>
            <ta e="T28" id="Seg_552" s="T27">Хватит.</ta>
            <ta e="T32" id="Seg_553" s="T28">Я сломал ноги.</ta>
            <ta e="T35" id="Seg_554" s="T32">Я сломал руки.</ta>
            <ta e="T37" id="Seg_555" s="T35">Я ломаю.</ta>
            <ta e="T40" id="Seg_556" s="T37">Не ломай ноги. [?]</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_557" s="T0">Sew buttons.</ta>
            <ta e="T5" id="Seg_558" s="T2">I was sewing buttons.</ta>
            <ta e="T9" id="Seg_559" s="T5">And you sew buttons!</ta>
            <ta e="T14" id="Seg_560" s="T9">Let's bite pine nuts with you.</ta>
            <ta e="T18" id="Seg_561" s="T14">There are many pine nuts.</ta>
            <ta e="T27" id="Seg_562" s="T18">Many, sit down, crack them, and I will crack pine nuts. [?]</ta>
            <ta e="T28" id="Seg_563" s="T27">Enough.</ta>
            <ta e="T32" id="Seg_564" s="T28">I broke my legs.</ta>
            <ta e="T35" id="Seg_565" s="T32">I broke my arms.</ta>
            <ta e="T37" id="Seg_566" s="T35">I am breaking.</ta>
            <ta e="T40" id="Seg_567" s="T37">Don't break your legs. [?]</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_568" s="T0">Nähe Knöpfe.</ta>
            <ta e="T5" id="Seg_569" s="T2">Ich nähte Knöpfe.</ta>
            <ta e="T9" id="Seg_570" s="T5">Und du näh Knöpfe!</ta>
            <ta e="T14" id="Seg_571" s="T9">Lass uns Pinienkerne aushülsen.</ta>
            <ta e="T18" id="Seg_572" s="T14">Es gibt dort viele Pinienkerne.</ta>
            <ta e="T27" id="Seg_573" s="T18">Viele, setze dich, hülse sie aus, und ich werde Pinienkerne aushülsen.</ta>
            <ta e="T28" id="Seg_574" s="T27">Genug.</ta>
            <ta e="T32" id="Seg_575" s="T28">Ich brach mir die Beine.</ta>
            <ta e="T35" id="Seg_576" s="T32">Ich brach mir die Arme.</ta>
            <ta e="T37" id="Seg_577" s="T35">Ich breche.</ta>
            <ta e="T40" id="Seg_578" s="T37">Brecht euch nicht die Beine. [?]</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T27" id="Seg_579" s="T18">[KlG] dĭ lejeʔ: should be de-ʔ lej-jəʔ ‘bring the cedarnuts!’; de-ʔ ‘bring-IMP.2SG’</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
