<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDCBFBA0D9-2392-32F6-4349-38ADACBE14E8">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BearLeek_nar.wav" />
         <referenced-file url="PKZ_196X_BearLeek_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_BearLeek_nar\PKZ_196X_BearLeek_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">50</ud-information>
            <ud-information attribute-name="# HIAT:w">32</ud-information>
            <ud-information attribute-name="# e">32</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2018" time="0.0" type="appl" />
         <tli id="T2019" time="0.801" type="appl" />
         <tli id="T2020" time="1.602" type="appl" />
         <tli id="T2021" time="2.404" type="appl" />
         <tli id="T2022" time="3.419554477955263" />
         <tli id="T2023" time="4.415" type="appl" />
         <tli id="T2024" time="5.289" type="appl" />
         <tli id="T2025" time="6.164" type="appl" />
         <tli id="T2026" time="6.793" type="appl" />
         <tli id="T2027" time="7.422" type="appl" />
         <tli id="T2028" time="8.051" type="appl" />
         <tli id="T2029" time="8.766" type="appl" />
         <tli id="T2030" time="9.481" type="appl" />
         <tli id="T2031" time="10.197" type="appl" />
         <tli id="T2032" time="10.912" type="appl" />
         <tli id="T2033" time="12.291731885671553" />
         <tli id="T2034" time="13.217" type="appl" />
         <tli id="T2035" time="14.025" type="appl" />
         <tli id="T2036" time="14.834" type="appl" />
         <tli id="T2037" time="15.642" type="appl" />
         <tli id="T2038" time="16.45" type="appl" />
         <tli id="T2039" time="17.043" type="appl" />
         <tli id="T2040" time="17.636" type="appl" />
         <tli id="T2041" time="18.229" type="appl" />
         <tli id="T2042" time="18.822" type="appl" />
         <tli id="T2043" time="19.415" type="appl" />
         <tli id="T2044" time="20.204034352207415" />
         <tli id="T2045" time="21.046" type="appl" />
         <tli id="T2046" time="21.819" type="appl" />
         <tli id="T2047" time="22.593" type="appl" />
         <tli id="T2048" time="23.366" type="appl" />
         <tli id="T2049" time="24.078" type="appl" />
         <tli id="T2050" time="24.79" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2050" id="Seg_0" n="sc" s="T2018">
               <ts e="T2022" id="Seg_2" n="HIAT:u" s="T2018">
                  <ts e="T2019" id="Seg_4" n="HIAT:w" s="T2018">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2020" id="Seg_7" n="HIAT:w" s="T2019">kambiam</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T2021" id="Seg_11" n="HIAT:w" s="T2020">kăl-</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2022" id="Seg_15" n="HIAT:w" s="T2021">kəlbajlaʔ</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2025" id="Seg_19" n="HIAT:u" s="T2022">
                  <ts e="T2023" id="Seg_21" n="HIAT:w" s="T2022">Onʼiʔ</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2024" id="Seg_24" n="HIAT:w" s="T2023">udatziʔ</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2025" id="Seg_27" n="HIAT:w" s="T2024">nĭŋgəbiem</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2028" id="Seg_31" n="HIAT:u" s="T2025">
                  <ts e="T2026" id="Seg_33" n="HIAT:w" s="T2025">Onʼiʔ</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2027" id="Seg_36" n="HIAT:w" s="T2026">udam</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2028" id="Seg_39" n="HIAT:w" s="T2027">ĭzemnie</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2033" id="Seg_43" n="HIAT:u" s="T2028">
                  <ts e="T2029" id="Seg_45" n="HIAT:w" s="T2028">Dĭn</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2030" id="Seg_48" n="HIAT:w" s="T2029">iʔgö</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2031" id="Seg_51" n="HIAT:w" s="T2030">moškaʔi</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2032" id="Seg_54" n="HIAT:w" s="T2031">măna</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2033" id="Seg_57" n="HIAT:w" s="T2032">ambiʔi</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2038" id="Seg_61" n="HIAT:u" s="T2033">
                  <ts e="T2034" id="Seg_63" n="HIAT:w" s="T2033">Măn</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2035" id="Seg_66" n="HIAT:w" s="T2034">nĭŋgəbiem</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2036" id="Seg_69" n="HIAT:w" s="T2035">sumna</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_71" n="HIAT:ip">(</nts>
                  <ts e="T2037" id="Seg_73" n="HIAT:w" s="T2036">u-</ts>
                  <nts id="Seg_74" n="HIAT:ip">)</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2038" id="Seg_77" n="HIAT:w" s="T2037">uda</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2044" id="Seg_81" n="HIAT:u" s="T2038">
                  <ts e="T2039" id="Seg_83" n="HIAT:w" s="T2038">I</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2040" id="Seg_86" n="HIAT:w" s="T2039">deʔpiem</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2041" id="Seg_89" n="HIAT:w" s="T2040">maːʔnʼi</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2042" id="Seg_93" n="HIAT:w" s="T2041">šobiam</ts>
                  <nts id="Seg_94" n="HIAT:ip">,</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2043" id="Seg_97" n="HIAT:w" s="T2042">nezeŋ</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2044" id="Seg_100" n="HIAT:w" s="T2043">šobiʔi</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2048" id="Seg_104" n="HIAT:u" s="T2044">
                  <ts e="T2045" id="Seg_106" n="HIAT:w" s="T2044">Ugaːndə</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_108" n="HIAT:ip">(</nts>
                  <ts e="T2046" id="Seg_110" n="HIAT:w" s="T2045">pu-</ts>
                  <nts id="Seg_111" n="HIAT:ip">)</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2047" id="Seg_114" n="HIAT:w" s="T2046">putʼəmnia</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2048" id="Seg_117" n="HIAT:w" s="T2047">kəlbatsiʔ</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2050" id="Seg_121" n="HIAT:u" s="T2048">
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <ts e="T2049" id="Seg_124" n="HIAT:w" s="T2048">Deʔ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2050" id="Seg_127" n="HIAT:w" s="T2049">miʔnʼibeʔ</ts>
                  <nts id="Seg_128" n="HIAT:ip">!</nts>
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2050" id="Seg_131" n="sc" s="T2018">
               <ts e="T2019" id="Seg_133" n="e" s="T2018">Măn </ts>
               <ts e="T2020" id="Seg_135" n="e" s="T2019">kambiam </ts>
               <ts e="T2021" id="Seg_137" n="e" s="T2020">(kăl-) </ts>
               <ts e="T2022" id="Seg_139" n="e" s="T2021">kəlbajlaʔ. </ts>
               <ts e="T2023" id="Seg_141" n="e" s="T2022">Onʼiʔ </ts>
               <ts e="T2024" id="Seg_143" n="e" s="T2023">udatziʔ </ts>
               <ts e="T2025" id="Seg_145" n="e" s="T2024">nĭŋgəbiem. </ts>
               <ts e="T2026" id="Seg_147" n="e" s="T2025">Onʼiʔ </ts>
               <ts e="T2027" id="Seg_149" n="e" s="T2026">udam </ts>
               <ts e="T2028" id="Seg_151" n="e" s="T2027">ĭzemnie. </ts>
               <ts e="T2029" id="Seg_153" n="e" s="T2028">Dĭn </ts>
               <ts e="T2030" id="Seg_155" n="e" s="T2029">iʔgö </ts>
               <ts e="T2031" id="Seg_157" n="e" s="T2030">moškaʔi </ts>
               <ts e="T2032" id="Seg_159" n="e" s="T2031">măna </ts>
               <ts e="T2033" id="Seg_161" n="e" s="T2032">ambiʔi. </ts>
               <ts e="T2034" id="Seg_163" n="e" s="T2033">Măn </ts>
               <ts e="T2035" id="Seg_165" n="e" s="T2034">nĭŋgəbiem </ts>
               <ts e="T2036" id="Seg_167" n="e" s="T2035">sumna </ts>
               <ts e="T2037" id="Seg_169" n="e" s="T2036">(u-) </ts>
               <ts e="T2038" id="Seg_171" n="e" s="T2037">uda. </ts>
               <ts e="T2039" id="Seg_173" n="e" s="T2038">I </ts>
               <ts e="T2040" id="Seg_175" n="e" s="T2039">deʔpiem </ts>
               <ts e="T2041" id="Seg_177" n="e" s="T2040">maːʔnʼi, </ts>
               <ts e="T2042" id="Seg_179" n="e" s="T2041">šobiam, </ts>
               <ts e="T2043" id="Seg_181" n="e" s="T2042">nezeŋ </ts>
               <ts e="T2044" id="Seg_183" n="e" s="T2043">šobiʔi. </ts>
               <ts e="T2045" id="Seg_185" n="e" s="T2044">Ugaːndə </ts>
               <ts e="T2046" id="Seg_187" n="e" s="T2045">(pu-) </ts>
               <ts e="T2047" id="Seg_189" n="e" s="T2046">putʼəmnia </ts>
               <ts e="T2048" id="Seg_191" n="e" s="T2047">kəlbatsiʔ. </ts>
               <ts e="T2049" id="Seg_193" n="e" s="T2048">"Deʔ </ts>
               <ts e="T2050" id="Seg_195" n="e" s="T2049">miʔnʼibeʔ!" </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2022" id="Seg_196" s="T2018">PKZ_196X_BearLeek_nar.001 (001)</ta>
            <ta e="T2025" id="Seg_197" s="T2022">PKZ_196X_BearLeek_nar.002 (002)</ta>
            <ta e="T2028" id="Seg_198" s="T2025">PKZ_196X_BearLeek_nar.003 (003)</ta>
            <ta e="T2033" id="Seg_199" s="T2028">PKZ_196X_BearLeek_nar.004 (004)</ta>
            <ta e="T2038" id="Seg_200" s="T2033">PKZ_196X_BearLeek_nar.005 (005)</ta>
            <ta e="T2044" id="Seg_201" s="T2038">PKZ_196X_BearLeek_nar.006 (006)</ta>
            <ta e="T2048" id="Seg_202" s="T2044">PKZ_196X_BearLeek_nar.007 (007)</ta>
            <ta e="T2050" id="Seg_203" s="T2048">PKZ_196X_BearLeek_nar.008 (008)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2022" id="Seg_204" s="T2018">Măn kambiam (kăl-) kəlbajlaʔ. </ta>
            <ta e="T2025" id="Seg_205" s="T2022">Onʼiʔ udatziʔ nĭŋgəbiem. </ta>
            <ta e="T2028" id="Seg_206" s="T2025">Onʼiʔ udam ĭzemnie. </ta>
            <ta e="T2033" id="Seg_207" s="T2028">Dĭn iʔgö moškaʔi măna ambiʔi. </ta>
            <ta e="T2038" id="Seg_208" s="T2033">Măn nĭŋgəbiem sumna (u-) uda. </ta>
            <ta e="T2044" id="Seg_209" s="T2038">I deʔpiem maːʔnʼi, šobiam, nezeŋ šobiʔi. </ta>
            <ta e="T2048" id="Seg_210" s="T2044">"Ugaːndə (pu-) putʼəmnia kəlbatsiʔ! </ta>
            <ta e="T2050" id="Seg_211" s="T2048">Deʔ miʔnʼibeʔ!" </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2019" id="Seg_212" s="T2018">măn</ta>
            <ta e="T2020" id="Seg_213" s="T2019">kam-bia-m</ta>
            <ta e="T2022" id="Seg_214" s="T2021">kəlba-j-laʔ</ta>
            <ta e="T2023" id="Seg_215" s="T2022">onʼiʔ</ta>
            <ta e="T2024" id="Seg_216" s="T2023">uda-t-ziʔ</ta>
            <ta e="T2025" id="Seg_217" s="T2024">nĭŋgə-bie-m</ta>
            <ta e="T2026" id="Seg_218" s="T2025">onʼiʔ</ta>
            <ta e="T2027" id="Seg_219" s="T2026">uda-m</ta>
            <ta e="T2028" id="Seg_220" s="T2027">ĭzem-nie</ta>
            <ta e="T2029" id="Seg_221" s="T2028">dĭn</ta>
            <ta e="T2030" id="Seg_222" s="T2029">iʔgö</ta>
            <ta e="T2031" id="Seg_223" s="T2030">moška-ʔi</ta>
            <ta e="T2032" id="Seg_224" s="T2031">măna</ta>
            <ta e="T2033" id="Seg_225" s="T2032">am-bi-ʔi</ta>
            <ta e="T2034" id="Seg_226" s="T2033">măn</ta>
            <ta e="T2035" id="Seg_227" s="T2034">nĭŋgə-bie-m</ta>
            <ta e="T2036" id="Seg_228" s="T2035">sumna</ta>
            <ta e="T2038" id="Seg_229" s="T2037">uda</ta>
            <ta e="T2039" id="Seg_230" s="T2038">i</ta>
            <ta e="T2040" id="Seg_231" s="T2039">deʔ-pie-m</ta>
            <ta e="T2041" id="Seg_232" s="T2040">maːʔ-nʼi</ta>
            <ta e="T2042" id="Seg_233" s="T2041">šo-bia-m</ta>
            <ta e="T2043" id="Seg_234" s="T2042">ne-zeŋ</ta>
            <ta e="T2044" id="Seg_235" s="T2043">šo-bi-ʔi</ta>
            <ta e="T2045" id="Seg_236" s="T2044">ugaːndə</ta>
            <ta e="T2047" id="Seg_237" s="T2046">putʼəm-nia</ta>
            <ta e="T2048" id="Seg_238" s="T2047">kəlba-t-siʔ</ta>
            <ta e="T2049" id="Seg_239" s="T2048">de-ʔ</ta>
            <ta e="T2050" id="Seg_240" s="T2049">miʔnʼibeʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2019" id="Seg_241" s="T2018">măn</ta>
            <ta e="T2020" id="Seg_242" s="T2019">kan-bi-m</ta>
            <ta e="T2022" id="Seg_243" s="T2021">kalba-j-lAʔ</ta>
            <ta e="T2023" id="Seg_244" s="T2022">onʼiʔ</ta>
            <ta e="T2024" id="Seg_245" s="T2023">uda-t-ziʔ</ta>
            <ta e="T2025" id="Seg_246" s="T2024">nĭŋgə-bi-m</ta>
            <ta e="T2026" id="Seg_247" s="T2025">onʼiʔ</ta>
            <ta e="T2027" id="Seg_248" s="T2026">uda-m</ta>
            <ta e="T2028" id="Seg_249" s="T2027">ĭzem-liA</ta>
            <ta e="T2029" id="Seg_250" s="T2028">dĭn</ta>
            <ta e="T2030" id="Seg_251" s="T2029">iʔgö</ta>
            <ta e="T2031" id="Seg_252" s="T2030">moška-jəʔ</ta>
            <ta e="T2032" id="Seg_253" s="T2031">măna</ta>
            <ta e="T2033" id="Seg_254" s="T2032">am-bi-jəʔ</ta>
            <ta e="T2034" id="Seg_255" s="T2033">măn</ta>
            <ta e="T2035" id="Seg_256" s="T2034">nĭŋgə-bi-m</ta>
            <ta e="T2036" id="Seg_257" s="T2035">sumna</ta>
            <ta e="T2038" id="Seg_258" s="T2037">uda</ta>
            <ta e="T2039" id="Seg_259" s="T2038">i</ta>
            <ta e="T2040" id="Seg_260" s="T2039">det-bi-m</ta>
            <ta e="T2041" id="Seg_261" s="T2040">maʔ-gənʼi</ta>
            <ta e="T2042" id="Seg_262" s="T2041">šo-bi-m</ta>
            <ta e="T2043" id="Seg_263" s="T2042">ne-zAŋ</ta>
            <ta e="T2044" id="Seg_264" s="T2043">šo-bi-jəʔ</ta>
            <ta e="T2045" id="Seg_265" s="T2044">ugaːndə</ta>
            <ta e="T2047" id="Seg_266" s="T2046">putʼtʼəm-liA</ta>
            <ta e="T2048" id="Seg_267" s="T2047">kalba-t-ziʔ</ta>
            <ta e="T2049" id="Seg_268" s="T2048">det-ʔ</ta>
            <ta e="T2050" id="Seg_269" s="T2049">miʔnʼibeʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2019" id="Seg_270" s="T2018">I.NOM</ta>
            <ta e="T2020" id="Seg_271" s="T2019">go-PST-1SG</ta>
            <ta e="T2022" id="Seg_272" s="T2021">bear.leek-VBLZ-CVB</ta>
            <ta e="T2023" id="Seg_273" s="T2022">single.[NOM.SG]</ta>
            <ta e="T2024" id="Seg_274" s="T2023">hand-3SG-INS</ta>
            <ta e="T2025" id="Seg_275" s="T2024">tear-PST-1SG</ta>
            <ta e="T2026" id="Seg_276" s="T2025">single.[NOM.SG]</ta>
            <ta e="T2027" id="Seg_277" s="T2026">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T2028" id="Seg_278" s="T2027">hurt-PRS.[3SG]</ta>
            <ta e="T2029" id="Seg_279" s="T2028">there</ta>
            <ta e="T2030" id="Seg_280" s="T2029">many</ta>
            <ta e="T2031" id="Seg_281" s="T2030">midge-PL</ta>
            <ta e="T2032" id="Seg_282" s="T2031">I.LAT</ta>
            <ta e="T2033" id="Seg_283" s="T2032">eat-PST-3PL</ta>
            <ta e="T2034" id="Seg_284" s="T2033">I.NOM</ta>
            <ta e="T2035" id="Seg_285" s="T2034">tear-PST-1SG</ta>
            <ta e="T2036" id="Seg_286" s="T2035">five.[NOM.SG]</ta>
            <ta e="T2038" id="Seg_287" s="T2037">hand.[NOM.SG]</ta>
            <ta e="T2039" id="Seg_288" s="T2038">and</ta>
            <ta e="T2040" id="Seg_289" s="T2039">bring-PST-1SG</ta>
            <ta e="T2041" id="Seg_290" s="T2040">tent-LAT/LOC.1SG</ta>
            <ta e="T2042" id="Seg_291" s="T2041">come-PST-1SG</ta>
            <ta e="T2043" id="Seg_292" s="T2042">woman-PL</ta>
            <ta e="T2044" id="Seg_293" s="T2043">come-PST-3PL</ta>
            <ta e="T2045" id="Seg_294" s="T2044">very</ta>
            <ta e="T2047" id="Seg_295" s="T2046">smell-PRS.[3SG]</ta>
            <ta e="T2048" id="Seg_296" s="T2047">bear.leek-3SG-INS</ta>
            <ta e="T2049" id="Seg_297" s="T2048">bring-IMP.2SG</ta>
            <ta e="T2050" id="Seg_298" s="T2049">we.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2019" id="Seg_299" s="T2018">я.NOM</ta>
            <ta e="T2020" id="Seg_300" s="T2019">пойти-PST-1SG</ta>
            <ta e="T2022" id="Seg_301" s="T2021">черемша-VBLZ-CVB</ta>
            <ta e="T2023" id="Seg_302" s="T2022">один.[NOM.SG]</ta>
            <ta e="T2024" id="Seg_303" s="T2023">рука-3SG-INS</ta>
            <ta e="T2025" id="Seg_304" s="T2024">рвать-PST-1SG</ta>
            <ta e="T2026" id="Seg_305" s="T2025">один.[NOM.SG]</ta>
            <ta e="T2027" id="Seg_306" s="T2026">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T2028" id="Seg_307" s="T2027">болеть-PRS.[3SG]</ta>
            <ta e="T2029" id="Seg_308" s="T2028">там</ta>
            <ta e="T2030" id="Seg_309" s="T2029">много</ta>
            <ta e="T2031" id="Seg_310" s="T2030">мошка-PL</ta>
            <ta e="T2032" id="Seg_311" s="T2031">я.LAT</ta>
            <ta e="T2033" id="Seg_312" s="T2032">съесть-PST-3PL</ta>
            <ta e="T2034" id="Seg_313" s="T2033">я.NOM</ta>
            <ta e="T2035" id="Seg_314" s="T2034">рвать-PST-1SG</ta>
            <ta e="T2036" id="Seg_315" s="T2035">пять.[NOM.SG]</ta>
            <ta e="T2038" id="Seg_316" s="T2037">рука.[NOM.SG]</ta>
            <ta e="T2039" id="Seg_317" s="T2038">и</ta>
            <ta e="T2040" id="Seg_318" s="T2039">принести-PST-1SG</ta>
            <ta e="T2041" id="Seg_319" s="T2040">чум-LAT/LOC.1SG</ta>
            <ta e="T2042" id="Seg_320" s="T2041">прийти-PST-1SG</ta>
            <ta e="T2043" id="Seg_321" s="T2042">женщина-PL</ta>
            <ta e="T2044" id="Seg_322" s="T2043">прийти-PST-3PL</ta>
            <ta e="T2045" id="Seg_323" s="T2044">очень</ta>
            <ta e="T2047" id="Seg_324" s="T2046">пахнуть-PRS.[3SG]</ta>
            <ta e="T2048" id="Seg_325" s="T2047">черемша-3SG-INS</ta>
            <ta e="T2049" id="Seg_326" s="T2048">принести-IMP.2SG</ta>
            <ta e="T2050" id="Seg_327" s="T2049">мы.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2019" id="Seg_328" s="T2018">pers</ta>
            <ta e="T2020" id="Seg_329" s="T2019">v-v:tense-v:pn</ta>
            <ta e="T2022" id="Seg_330" s="T2021">n-n&gt;v-v:n.fin</ta>
            <ta e="T2023" id="Seg_331" s="T2022">adj-n:case</ta>
            <ta e="T2024" id="Seg_332" s="T2023">n-n:case.poss-n:case</ta>
            <ta e="T2025" id="Seg_333" s="T2024">v-v:tense-v:pn</ta>
            <ta e="T2026" id="Seg_334" s="T2025">adj-n:case</ta>
            <ta e="T2027" id="Seg_335" s="T2026">n-n:case.poss</ta>
            <ta e="T2028" id="Seg_336" s="T2027">v-v:tense-v:pn</ta>
            <ta e="T2029" id="Seg_337" s="T2028">adv</ta>
            <ta e="T2030" id="Seg_338" s="T2029">quant</ta>
            <ta e="T2031" id="Seg_339" s="T2030">n-n:num</ta>
            <ta e="T2032" id="Seg_340" s="T2031">pers</ta>
            <ta e="T2033" id="Seg_341" s="T2032">v-v:tense-v:pn</ta>
            <ta e="T2034" id="Seg_342" s="T2033">pers</ta>
            <ta e="T2035" id="Seg_343" s="T2034">v-v:tense-v:pn</ta>
            <ta e="T2036" id="Seg_344" s="T2035">num-n:case</ta>
            <ta e="T2038" id="Seg_345" s="T2037">n-n:case</ta>
            <ta e="T2039" id="Seg_346" s="T2038">conj</ta>
            <ta e="T2040" id="Seg_347" s="T2039">v-v:tense-v:pn</ta>
            <ta e="T2041" id="Seg_348" s="T2040">n-n:case.poss</ta>
            <ta e="T2042" id="Seg_349" s="T2041">v-v:tense-v:pn</ta>
            <ta e="T2043" id="Seg_350" s="T2042">n-n:num</ta>
            <ta e="T2044" id="Seg_351" s="T2043">v-v:tense-v:pn</ta>
            <ta e="T2045" id="Seg_352" s="T2044">adv</ta>
            <ta e="T2047" id="Seg_353" s="T2046">v-v:tense-v:pn</ta>
            <ta e="T2048" id="Seg_354" s="T2047">n-n:case.poss-n:case</ta>
            <ta e="T2049" id="Seg_355" s="T2048">v-v:mood.pn</ta>
            <ta e="T2050" id="Seg_356" s="T2049">pers</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2019" id="Seg_357" s="T2018">pers</ta>
            <ta e="T2020" id="Seg_358" s="T2019">v</ta>
            <ta e="T2022" id="Seg_359" s="T2021">v</ta>
            <ta e="T2023" id="Seg_360" s="T2022">adj</ta>
            <ta e="T2024" id="Seg_361" s="T2023">n</ta>
            <ta e="T2025" id="Seg_362" s="T2024">v</ta>
            <ta e="T2026" id="Seg_363" s="T2025">adj</ta>
            <ta e="T2027" id="Seg_364" s="T2026">n</ta>
            <ta e="T2028" id="Seg_365" s="T2027">v</ta>
            <ta e="T2029" id="Seg_366" s="T2028">adv</ta>
            <ta e="T2030" id="Seg_367" s="T2029">quant</ta>
            <ta e="T2031" id="Seg_368" s="T2030">n</ta>
            <ta e="T2032" id="Seg_369" s="T2031">pers</ta>
            <ta e="T2033" id="Seg_370" s="T2032">v</ta>
            <ta e="T2034" id="Seg_371" s="T2033">pers</ta>
            <ta e="T2035" id="Seg_372" s="T2034">v</ta>
            <ta e="T2036" id="Seg_373" s="T2035">num</ta>
            <ta e="T2038" id="Seg_374" s="T2037">n</ta>
            <ta e="T2039" id="Seg_375" s="T2038">conj</ta>
            <ta e="T2040" id="Seg_376" s="T2039">v</ta>
            <ta e="T2041" id="Seg_377" s="T2040">n</ta>
            <ta e="T2042" id="Seg_378" s="T2041">v</ta>
            <ta e="T2043" id="Seg_379" s="T2042">n</ta>
            <ta e="T2044" id="Seg_380" s="T2043">v</ta>
            <ta e="T2045" id="Seg_381" s="T2044">adv</ta>
            <ta e="T2047" id="Seg_382" s="T2046">v</ta>
            <ta e="T2048" id="Seg_383" s="T2047">n</ta>
            <ta e="T2049" id="Seg_384" s="T2048">v</ta>
            <ta e="T2050" id="Seg_385" s="T2049">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2019" id="Seg_386" s="T2018">pro.h:A</ta>
            <ta e="T2024" id="Seg_387" s="T2023">np:Ins</ta>
            <ta e="T2025" id="Seg_388" s="T2024">0.1.h:A</ta>
            <ta e="T2027" id="Seg_389" s="T2026">np:Th 0.1.h:Poss</ta>
            <ta e="T2031" id="Seg_390" s="T2030">np:A</ta>
            <ta e="T2032" id="Seg_391" s="T2031">pro.h:P</ta>
            <ta e="T2034" id="Seg_392" s="T2033">pro.h:A</ta>
            <ta e="T2038" id="Seg_393" s="T2037">np:Th</ta>
            <ta e="T2040" id="Seg_394" s="T2039">0.1.h:A</ta>
            <ta e="T2041" id="Seg_395" s="T2040">np:G</ta>
            <ta e="T2042" id="Seg_396" s="T2041">0.1.h:A</ta>
            <ta e="T2043" id="Seg_397" s="T2042">np.h:A</ta>
            <ta e="T2047" id="Seg_398" s="T2046">0.3:Th</ta>
            <ta e="T2049" id="Seg_399" s="T2048">0.2.h:A</ta>
            <ta e="T2050" id="Seg_400" s="T2049">pro.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2019" id="Seg_401" s="T2018">pro.h:S</ta>
            <ta e="T2020" id="Seg_402" s="T2019">v:pred</ta>
            <ta e="T2022" id="Seg_403" s="T2021">s:purp</ta>
            <ta e="T2025" id="Seg_404" s="T2024">0.1.h:S v:pred</ta>
            <ta e="T2027" id="Seg_405" s="T2026">np:S</ta>
            <ta e="T2028" id="Seg_406" s="T2027">v:pred</ta>
            <ta e="T2031" id="Seg_407" s="T2030">np:S</ta>
            <ta e="T2032" id="Seg_408" s="T2031">pro.h:O</ta>
            <ta e="T2033" id="Seg_409" s="T2032">v:pred</ta>
            <ta e="T2034" id="Seg_410" s="T2033">pro.h:S</ta>
            <ta e="T2035" id="Seg_411" s="T2034">v:pred</ta>
            <ta e="T2038" id="Seg_412" s="T2037">np:O</ta>
            <ta e="T2040" id="Seg_413" s="T2039">0.1.h:S v:pred</ta>
            <ta e="T2042" id="Seg_414" s="T2041">0.1.h:S v:pred</ta>
            <ta e="T2043" id="Seg_415" s="T2042">np.h:S</ta>
            <ta e="T2044" id="Seg_416" s="T2043">v:pred</ta>
            <ta e="T2047" id="Seg_417" s="T2046">0.3.h:S v:pred</ta>
            <ta e="T2049" id="Seg_418" s="T2048">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2039" id="Seg_419" s="T2038">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2022" id="Seg_420" s="T2018">Я ходила за черемшой.</ta>
            <ta e="T2025" id="Seg_421" s="T2022">Одной рукой рвала.</ta>
            <ta e="T2028" id="Seg_422" s="T2025">Одна рука болит.</ta>
            <ta e="T2033" id="Seg_423" s="T2028">Много мошек меня там ело.</ta>
            <ta e="T2038" id="Seg_424" s="T2033">Я нарвала пять (пучков?).</ta>
            <ta e="T2044" id="Seg_425" s="T2038">И принесла домой, вернулась, пришли женщины.</ta>
            <ta e="T2048" id="Seg_426" s="T2044">«Очень черемшой пахнет!</ta>
            <ta e="T2050" id="Seg_427" s="T2048">Дай нам!»</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2022" id="Seg_428" s="T2018">I went to pick bear leek.</ta>
            <ta e="T2025" id="Seg_429" s="T2022">I picked with one hand.</ta>
            <ta e="T2028" id="Seg_430" s="T2025">One of my hands hurts.</ta>
            <ta e="T2033" id="Seg_431" s="T2028">Many blackflies ate me there.</ta>
            <ta e="T2038" id="Seg_432" s="T2033">I picked five handfuls. [?]</ta>
            <ta e="T2044" id="Seg_433" s="T2038">And I brought it home, I came, women came.</ta>
            <ta e="T2048" id="Seg_434" s="T2044">"It smells a lot of bear leek!</ta>
            <ta e="T2050" id="Seg_435" s="T2048">Give us [some]!"</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2022" id="Seg_436" s="T2018">Ich ging Bärlauch pflücken.</ta>
            <ta e="T2025" id="Seg_437" s="T2022">Ich pflückte mit einer Hand.</ta>
            <ta e="T2028" id="Seg_438" s="T2025">Eine meiner Händen tut mir weh.</ta>
            <ta e="T2033" id="Seg_439" s="T2028">Viele Kriebelmücken fraßen mich dort.</ta>
            <ta e="T2038" id="Seg_440" s="T2033">Ich pflückte fünf Handvoll.</ta>
            <ta e="T2044" id="Seg_441" s="T2038">Und ich brachte es nach Hause, Ich kam, Frauen kamen.</ta>
            <ta e="T2048" id="Seg_442" s="T2044">„Es riecht sehr nach Bärlauch!</ta>
            <ta e="T2050" id="Seg_443" s="T2048">Gib uns [etwas]!“</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2022" id="Seg_444" s="T2018">[KlT:] Jan. kalba ’medvehagyma'. [AAV]: Tape SU0192</ta>
            <ta e="T2025" id="Seg_445" s="T2022">[KlT:] Nĭŋgə - to tear, to pick.</ta>
            <ta e="T2033" id="Seg_446" s="T2028">[KlT:] Мошка 'blackfly' 'kihulane'.</ta>
            <ta e="T2048" id="Seg_447" s="T2044">Puʔtʼə ’to stink’ Ru. INS model.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2018" />
            <conversion-tli id="T2019" />
            <conversion-tli id="T2020" />
            <conversion-tli id="T2021" />
            <conversion-tli id="T2022" />
            <conversion-tli id="T2023" />
            <conversion-tli id="T2024" />
            <conversion-tli id="T2025" />
            <conversion-tli id="T2026" />
            <conversion-tli id="T2027" />
            <conversion-tli id="T2028" />
            <conversion-tli id="T2029" />
            <conversion-tli id="T2030" />
            <conversion-tli id="T2031" />
            <conversion-tli id="T2032" />
            <conversion-tli id="T2033" />
            <conversion-tli id="T2034" />
            <conversion-tli id="T2035" />
            <conversion-tli id="T2036" />
            <conversion-tli id="T2037" />
            <conversion-tli id="T2038" />
            <conversion-tli id="T2039" />
            <conversion-tli id="T2040" />
            <conversion-tli id="T2041" />
            <conversion-tli id="T2042" />
            <conversion-tli id="T2043" />
            <conversion-tli id="T2044" />
            <conversion-tli id="T2045" />
            <conversion-tli id="T2046" />
            <conversion-tli id="T2047" />
            <conversion-tli id="T2048" />
            <conversion-tli id="T2049" />
            <conversion-tli id="T2050" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
