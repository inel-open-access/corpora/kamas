<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF0C9352D-7C83-0393-CAE7-CC345C48B377">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Gate_nar.wav" />
         <referenced-file url="PKZ_196X_Gate_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_Gate_nar\PKZ_196X_Gate_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">65</ud-information>
            <ud-information attribute-name="# HIAT:w">45</ud-information>
            <ud-information attribute-name="# e">45</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1357" time="0.008" type="appl" />
         <tli id="T1358" time="0.607" type="appl" />
         <tli id="T1359" time="1.205" type="appl" />
         <tli id="T1360" time="1.804" type="appl" />
         <tli id="T1361" time="2.402" type="appl" />
         <tli id="T1362" time="3.001" type="appl" />
         <tli id="T1363" time="3.878" type="appl" />
         <tli id="T1364" time="4.636" type="appl" />
         <tli id="T1365" time="5.395" type="appl" />
         <tli id="T1366" time="6.154" type="appl" />
         <tli id="T1367" time="6.987" type="appl" />
         <tli id="T1368" time="7.79" type="appl" />
         <tli id="T1369" time="8.593" type="appl" />
         <tli id="T1370" time="9.397" type="appl" />
         <tli id="T1371" time="10.2" type="appl" />
         <tli id="T1372" time="11.003" type="appl" />
         <tli id="T1373" time="11.806" type="appl" />
         <tli id="T1374" time="12.33" type="appl" />
         <tli id="T1375" time="12.821" type="appl" />
         <tli id="T1376" time="13.313" type="appl" />
         <tli id="T1377" time="13.804" type="appl" />
         <tli id="T1378" time="14.296" type="appl" />
         <tli id="T1379" time="14.787" type="appl" />
         <tli id="T1380" time="15.279" type="appl" />
         <tli id="T1381" time="15.797" type="appl" />
         <tli id="T1382" time="16.314" type="appl" />
         <tli id="T1383" time="16.832" type="appl" />
         <tli id="T1384" time="17.771" type="appl" />
         <tli id="T1385" time="18.71" type="appl" />
         <tli id="T1386" time="19.649" type="appl" />
         <tli id="T1387" time="20.588" type="appl" />
         <tli id="T1388" time="21.527" type="appl" />
         <tli id="T1389" time="22.082" type="appl" />
         <tli id="T1390" time="22.613" type="appl" />
         <tli id="T1391" time="23.144" type="appl" />
         <tli id="T1392" time="23.859769874758374" />
         <tli id="T1393" time="24.482" type="appl" />
         <tli id="T1394" time="25.276" type="appl" />
         <tli id="T1395" time="26.326412750885954" />
         <tli id="T1396" time="27.371" type="appl" />
         <tli id="T1397" time="28.275" type="appl" />
         <tli id="T1398" time="29.18" type="appl" />
         <tli id="T1399" time="30.084" type="appl" />
         <tli id="T1400" time="30.988" type="appl" />
         <tli id="T1401" time="31.436" type="appl" />
         <tli id="T1402" time="31.885" type="appl" />
         <tli id="T1403" time="32.333" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1403" id="Seg_0" n="sc" s="T1357">
               <ts e="T1362" id="Seg_2" n="HIAT:u" s="T1357">
                  <ts e="T1358" id="Seg_4" n="HIAT:w" s="T1357">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1359" id="Seg_7" n="HIAT:w" s="T1358">teinen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_10" n="HIAT:w" s="T1359">kambiam</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_13" n="HIAT:w" s="T1360">onʼiʔ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1362" id="Seg_16" n="HIAT:w" s="T1361">nenə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1366" id="Seg_20" n="HIAT:u" s="T1362">
                  <ts e="T1363" id="Seg_22" n="HIAT:w" s="T1362">Dĭn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1364" id="Seg_25" n="HIAT:w" s="T1363">varotaʔi</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1365" id="Seg_28" n="HIAT:w" s="T1364">ej</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1366" id="Seg_31" n="HIAT:w" s="T1365">kajliaʔi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1373" id="Seg_35" n="HIAT:u" s="T1366">
                  <ts e="T1367" id="Seg_37" n="HIAT:w" s="T1366">Măn</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1368" id="Seg_40" n="HIAT:w" s="T1367">kubiom</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1369" id="Seg_44" n="HIAT:w" s="T1368">măndəm:</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1370" id="Seg_47" n="HIAT:w" s="T1369">nada</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1371" id="Seg_50" n="HIAT:w" s="T1370">baltuziʔ</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1372" id="Seg_53" n="HIAT:w" s="T1371">stolpdə</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1373" id="Seg_56" n="HIAT:w" s="T1372">jaʔsittə</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1380" id="Seg_60" n="HIAT:u" s="T1373">
                  <nts id="Seg_61" n="HIAT:ip">"</nts>
                  <ts e="T1374" id="Seg_63" n="HIAT:w" s="T1373">No</ts>
                  <nts id="Seg_64" n="HIAT:ip">,</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1375" id="Seg_67" n="HIAT:w" s="T1374">ej</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1376" id="Seg_71" n="HIAT:w" s="T1375">no</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1377" id="Seg_74" n="HIAT:w" s="T1376">jaʔdə</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1378" id="Seg_78" n="HIAT:w" s="T1377">ĭmbi</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1379" id="Seg_81" n="HIAT:w" s="T1378">tănan</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1380" id="Seg_84" n="HIAT:w" s="T1379">ajirial</ts>
                  <nts id="Seg_85" n="HIAT:ip">!</nts>
                  <nts id="Seg_86" n="HIAT:ip">"</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1383" id="Seg_89" n="HIAT:u" s="T1380">
                  <ts e="T1381" id="Seg_91" n="HIAT:w" s="T1380">Dĭ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1382" id="Seg_94" n="HIAT:w" s="T1381">jaʔpi</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1383" id="Seg_98" n="HIAT:w" s="T1382">jaʔpi</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1388" id="Seg_102" n="HIAT:u" s="T1383">
                  <ts e="T1384" id="Seg_104" n="HIAT:w" s="T1383">I</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1385" id="Seg_107" n="HIAT:w" s="T1384">kručokdə</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1386" id="Seg_110" n="HIAT:w" s="T1385">bar</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1387" id="Seg_113" n="HIAT:w" s="T1386">dibər</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_116" n="HIAT:w" s="T1387">saʔməluʔpi</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1392" id="Seg_120" n="HIAT:u" s="T1388">
                  <ts e="T1389" id="Seg_122" n="HIAT:w" s="T1388">Dĭ</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1390" id="Seg_125" n="HIAT:w" s="T1389">bar</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1391" id="Seg_128" n="HIAT:w" s="T1390">kakənarluʔpi</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1392" id="Seg_131" n="HIAT:w" s="T1391">bar</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1395" id="Seg_135" n="HIAT:u" s="T1392">
                  <ts e="T1393" id="Seg_137" n="HIAT:w" s="T1392">Vot</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1394" id="Seg_141" n="HIAT:w" s="T1393">jakšə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1395" id="Seg_144" n="HIAT:w" s="T1394">molaːmbi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1400" id="Seg_148" n="HIAT:u" s="T1395">
                  <nts id="Seg_149" n="HIAT:ip">(</nts>
                  <ts e="T1396" id="Seg_151" n="HIAT:w" s="T1395">Kar-</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1397" id="Seg_154" n="HIAT:w" s="T1396">ka-</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1398" id="Seg_157" n="HIAT:w" s="T1397">ka-</ts>
                  <nts id="Seg_158" n="HIAT:ip">)</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1399" id="Seg_161" n="HIAT:w" s="T1398">Kajbi</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_164" n="HIAT:w" s="T1399">varotaʔi</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1403" id="Seg_168" n="HIAT:u" s="T1400">
                  <ts e="T1401" id="Seg_170" n="HIAT:w" s="T1400">Tuj</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1403" id="Seg_173" n="HIAT:w" s="T1401">šoškaʔi</ts>
                  <nts id="Seg_174" n="HIAT:ip">…</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1403" id="Seg_176" n="sc" s="T1357">
               <ts e="T1358" id="Seg_178" n="e" s="T1357">Măn </ts>
               <ts e="T1359" id="Seg_180" n="e" s="T1358">teinen </ts>
               <ts e="T1360" id="Seg_182" n="e" s="T1359">kambiam </ts>
               <ts e="T1361" id="Seg_184" n="e" s="T1360">onʼiʔ </ts>
               <ts e="T1362" id="Seg_186" n="e" s="T1361">nenə. </ts>
               <ts e="T1363" id="Seg_188" n="e" s="T1362">Dĭn </ts>
               <ts e="T1364" id="Seg_190" n="e" s="T1363">varotaʔi </ts>
               <ts e="T1365" id="Seg_192" n="e" s="T1364">ej </ts>
               <ts e="T1366" id="Seg_194" n="e" s="T1365">kajliaʔi. </ts>
               <ts e="T1367" id="Seg_196" n="e" s="T1366">Măn </ts>
               <ts e="T1368" id="Seg_198" n="e" s="T1367">kubiom, </ts>
               <ts e="T1369" id="Seg_200" n="e" s="T1368">măndəm: </ts>
               <ts e="T1370" id="Seg_202" n="e" s="T1369">nada </ts>
               <ts e="T1371" id="Seg_204" n="e" s="T1370">baltuziʔ </ts>
               <ts e="T1372" id="Seg_206" n="e" s="T1371">stolpdə </ts>
               <ts e="T1373" id="Seg_208" n="e" s="T1372">jaʔsittə. </ts>
               <ts e="T1374" id="Seg_210" n="e" s="T1373">"No, </ts>
               <ts e="T1375" id="Seg_212" n="e" s="T1374">ej, </ts>
               <ts e="T1376" id="Seg_214" n="e" s="T1375">no </ts>
               <ts e="T1377" id="Seg_216" n="e" s="T1376">jaʔdə, </ts>
               <ts e="T1378" id="Seg_218" n="e" s="T1377">ĭmbi </ts>
               <ts e="T1379" id="Seg_220" n="e" s="T1378">tănan </ts>
               <ts e="T1380" id="Seg_222" n="e" s="T1379">ajirial!" </ts>
               <ts e="T1381" id="Seg_224" n="e" s="T1380">Dĭ </ts>
               <ts e="T1382" id="Seg_226" n="e" s="T1381">jaʔpi, </ts>
               <ts e="T1383" id="Seg_228" n="e" s="T1382">jaʔpi. </ts>
               <ts e="T1384" id="Seg_230" n="e" s="T1383">I </ts>
               <ts e="T1385" id="Seg_232" n="e" s="T1384">kručokdə </ts>
               <ts e="T1386" id="Seg_234" n="e" s="T1385">bar </ts>
               <ts e="T1387" id="Seg_236" n="e" s="T1386">dibər </ts>
               <ts e="T1388" id="Seg_238" n="e" s="T1387">saʔməluʔpi. </ts>
               <ts e="T1389" id="Seg_240" n="e" s="T1388">Dĭ </ts>
               <ts e="T1390" id="Seg_242" n="e" s="T1389">bar </ts>
               <ts e="T1391" id="Seg_244" n="e" s="T1390">kakənarluʔpi </ts>
               <ts e="T1392" id="Seg_246" n="e" s="T1391">bar. </ts>
               <ts e="T1393" id="Seg_248" n="e" s="T1392">Vot, </ts>
               <ts e="T1394" id="Seg_250" n="e" s="T1393">jakšə </ts>
               <ts e="T1395" id="Seg_252" n="e" s="T1394">molaːmbi. </ts>
               <ts e="T1396" id="Seg_254" n="e" s="T1395">(Kar- </ts>
               <ts e="T1397" id="Seg_256" n="e" s="T1396">ka- </ts>
               <ts e="T1398" id="Seg_258" n="e" s="T1397">ka-) </ts>
               <ts e="T1399" id="Seg_260" n="e" s="T1398">Kajbi </ts>
               <ts e="T1400" id="Seg_262" n="e" s="T1399">varotaʔi. </ts>
               <ts e="T1401" id="Seg_264" n="e" s="T1400">Tuj </ts>
               <ts e="T1403" id="Seg_266" n="e" s="T1401">šoškaʔi… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1362" id="Seg_267" s="T1357">PKZ_196X_Gate_nar.001 (001)</ta>
            <ta e="T1366" id="Seg_268" s="T1362">PKZ_196X_Gate_nar.002 (002)</ta>
            <ta e="T1373" id="Seg_269" s="T1366">PKZ_196X_Gate_nar.003 (003)</ta>
            <ta e="T1380" id="Seg_270" s="T1373">PKZ_196X_Gate_nar.004 (004)</ta>
            <ta e="T1383" id="Seg_271" s="T1380">PKZ_196X_Gate_nar.005 (005)</ta>
            <ta e="T1388" id="Seg_272" s="T1383">PKZ_196X_Gate_nar.006 (006)</ta>
            <ta e="T1392" id="Seg_273" s="T1388">PKZ_196X_Gate_nar.007 (007)</ta>
            <ta e="T1395" id="Seg_274" s="T1392">PKZ_196X_Gate_nar.008 (008)</ta>
            <ta e="T1400" id="Seg_275" s="T1395">PKZ_196X_Gate_nar.009 (009)</ta>
            <ta e="T1403" id="Seg_276" s="T1400">PKZ_196X_Gate_nar.010 (010)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1362" id="Seg_277" s="T1357">Măn teinen kambiam onʼiʔ nenə. </ta>
            <ta e="T1366" id="Seg_278" s="T1362">Dĭn varotaʔi ej kajliaʔi. </ta>
            <ta e="T1373" id="Seg_279" s="T1366">Măn kubiom, măndəm: nada baltuziʔ stolpdə jaʔsittə. </ta>
            <ta e="T1380" id="Seg_280" s="T1373">"No, ej, no jaʔdə, ĭmbi tănan ajirial!" </ta>
            <ta e="T1383" id="Seg_281" s="T1380">Dĭ jaʔpi, jaʔpi. </ta>
            <ta e="T1388" id="Seg_282" s="T1383">I kručokdə bar dibər saʔməluʔpi. </ta>
            <ta e="T1392" id="Seg_283" s="T1388">Dĭ bar kakənarluʔpi bar. </ta>
            <ta e="T1395" id="Seg_284" s="T1392">Vot, jakšə molaːmbi. </ta>
            <ta e="T1400" id="Seg_285" s="T1395">(Kar- ka- ka-) Kajbi varotaʔi. </ta>
            <ta e="T1403" id="Seg_286" s="T1400">Tuj šoškaʔi … </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1358" id="Seg_287" s="T1357">măn</ta>
            <ta e="T1359" id="Seg_288" s="T1358">teinen</ta>
            <ta e="T1360" id="Seg_289" s="T1359">kam-bia-m</ta>
            <ta e="T1361" id="Seg_290" s="T1360">onʼiʔ</ta>
            <ta e="T1362" id="Seg_291" s="T1361">ne-nə</ta>
            <ta e="T1363" id="Seg_292" s="T1362">dĭn</ta>
            <ta e="T1364" id="Seg_293" s="T1363">varota-ʔi</ta>
            <ta e="T1365" id="Seg_294" s="T1364">ej</ta>
            <ta e="T1366" id="Seg_295" s="T1365">kaj-lia-ʔi</ta>
            <ta e="T1367" id="Seg_296" s="T1366">măn</ta>
            <ta e="T1368" id="Seg_297" s="T1367">ku-bio-m</ta>
            <ta e="T1369" id="Seg_298" s="T1368">măn-də-m</ta>
            <ta e="T1370" id="Seg_299" s="T1369">nada</ta>
            <ta e="T1371" id="Seg_300" s="T1370">baltu-ziʔ</ta>
            <ta e="T1372" id="Seg_301" s="T1371">stolp-də</ta>
            <ta e="T1373" id="Seg_302" s="T1372">jaʔ-sittə</ta>
            <ta e="T1374" id="Seg_303" s="T1373">no</ta>
            <ta e="T1375" id="Seg_304" s="T1374">ej</ta>
            <ta e="T1376" id="Seg_305" s="T1375">no</ta>
            <ta e="T1377" id="Seg_306" s="T1376">jaʔ-də</ta>
            <ta e="T1378" id="Seg_307" s="T1377">ĭmbi</ta>
            <ta e="T1379" id="Seg_308" s="T1378">tănan</ta>
            <ta e="T1380" id="Seg_309" s="T1379">ajir-ia-l</ta>
            <ta e="T1381" id="Seg_310" s="T1380">dĭ</ta>
            <ta e="T1382" id="Seg_311" s="T1381">jaʔ-pi</ta>
            <ta e="T1383" id="Seg_312" s="T1382">jaʔ-pi</ta>
            <ta e="T1384" id="Seg_313" s="T1383">i</ta>
            <ta e="T1385" id="Seg_314" s="T1384">kručok-də</ta>
            <ta e="T1386" id="Seg_315" s="T1385">bar</ta>
            <ta e="T1387" id="Seg_316" s="T1386">dibər</ta>
            <ta e="T1388" id="Seg_317" s="T1387">saʔmə-luʔ-pi</ta>
            <ta e="T1389" id="Seg_318" s="T1388">dĭ</ta>
            <ta e="T1390" id="Seg_319" s="T1389">bar</ta>
            <ta e="T1391" id="Seg_320" s="T1390">kakənar-luʔ-pi</ta>
            <ta e="T1392" id="Seg_321" s="T1391">bar</ta>
            <ta e="T1393" id="Seg_322" s="T1392">vot</ta>
            <ta e="T1394" id="Seg_323" s="T1393">jakšə</ta>
            <ta e="T1395" id="Seg_324" s="T1394">mo-laːm-bi</ta>
            <ta e="T1399" id="Seg_325" s="T1398">kaj-bi</ta>
            <ta e="T1400" id="Seg_326" s="T1399">varota-ʔi</ta>
            <ta e="T1401" id="Seg_327" s="T1400">tuj</ta>
            <ta e="T1403" id="Seg_328" s="T1401">šoška-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1358" id="Seg_329" s="T1357">măn</ta>
            <ta e="T1359" id="Seg_330" s="T1358">teinen</ta>
            <ta e="T1360" id="Seg_331" s="T1359">kan-bi-m</ta>
            <ta e="T1361" id="Seg_332" s="T1360">onʼiʔ</ta>
            <ta e="T1362" id="Seg_333" s="T1361">ne-Tə</ta>
            <ta e="T1363" id="Seg_334" s="T1362">dĭn</ta>
            <ta e="T1364" id="Seg_335" s="T1363">varota-jəʔ</ta>
            <ta e="T1365" id="Seg_336" s="T1364">ej</ta>
            <ta e="T1366" id="Seg_337" s="T1365">kaj-liA-jəʔ</ta>
            <ta e="T1367" id="Seg_338" s="T1366">măn</ta>
            <ta e="T1368" id="Seg_339" s="T1367">ku-bi-m</ta>
            <ta e="T1369" id="Seg_340" s="T1368">măn-ntə-m</ta>
            <ta e="T1370" id="Seg_341" s="T1369">nadə</ta>
            <ta e="T1371" id="Seg_342" s="T1370">baltu-ziʔ</ta>
            <ta e="T1372" id="Seg_343" s="T1371">stolp-də</ta>
            <ta e="T1373" id="Seg_344" s="T1372">hʼaʔ-zittə</ta>
            <ta e="T1374" id="Seg_345" s="T1373">no</ta>
            <ta e="T1375" id="Seg_346" s="T1374">ej</ta>
            <ta e="T1376" id="Seg_347" s="T1375">no</ta>
            <ta e="T1377" id="Seg_348" s="T1376">hʼaʔ-t</ta>
            <ta e="T1378" id="Seg_349" s="T1377">ĭmbi</ta>
            <ta e="T1379" id="Seg_350" s="T1378">tănan</ta>
            <ta e="T1380" id="Seg_351" s="T1379">ajir-liA-l</ta>
            <ta e="T1381" id="Seg_352" s="T1380">dĭ</ta>
            <ta e="T1382" id="Seg_353" s="T1381">hʼaʔ-bi</ta>
            <ta e="T1383" id="Seg_354" s="T1382">hʼaʔ-bi</ta>
            <ta e="T1384" id="Seg_355" s="T1383">i</ta>
            <ta e="T1385" id="Seg_356" s="T1384">kručok-də</ta>
            <ta e="T1386" id="Seg_357" s="T1385">bar</ta>
            <ta e="T1387" id="Seg_358" s="T1386">dĭbər</ta>
            <ta e="T1388" id="Seg_359" s="T1387">saʔmə-luʔbdə-bi</ta>
            <ta e="T1389" id="Seg_360" s="T1388">dĭ</ta>
            <ta e="T1390" id="Seg_361" s="T1389">bar</ta>
            <ta e="T1391" id="Seg_362" s="T1390">kakənar-luʔbdə-bi</ta>
            <ta e="T1392" id="Seg_363" s="T1391">bar</ta>
            <ta e="T1393" id="Seg_364" s="T1392">vot</ta>
            <ta e="T1394" id="Seg_365" s="T1393">jakšə</ta>
            <ta e="T1395" id="Seg_366" s="T1394">mo-laːm-bi</ta>
            <ta e="T1399" id="Seg_367" s="T1398">kaj-bi</ta>
            <ta e="T1400" id="Seg_368" s="T1399">varota-jəʔ</ta>
            <ta e="T1401" id="Seg_369" s="T1400">tüj</ta>
            <ta e="T1403" id="Seg_370" s="T1401">šoška-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1358" id="Seg_371" s="T1357">I.NOM</ta>
            <ta e="T1359" id="Seg_372" s="T1358">today</ta>
            <ta e="T1360" id="Seg_373" s="T1359">go-PST-1SG</ta>
            <ta e="T1361" id="Seg_374" s="T1360">one.[NOM.SG]</ta>
            <ta e="T1362" id="Seg_375" s="T1361">woman-LAT</ta>
            <ta e="T1363" id="Seg_376" s="T1362">there</ta>
            <ta e="T1364" id="Seg_377" s="T1363">gate-PL</ta>
            <ta e="T1365" id="Seg_378" s="T1364">NEG</ta>
            <ta e="T1366" id="Seg_379" s="T1365">close-PRS-3PL</ta>
            <ta e="T1367" id="Seg_380" s="T1366">I.NOM</ta>
            <ta e="T1368" id="Seg_381" s="T1367">see-PST-1SG</ta>
            <ta e="T1369" id="Seg_382" s="T1368">say-IPFVZ-1SG</ta>
            <ta e="T1370" id="Seg_383" s="T1369">one.should</ta>
            <ta e="T1371" id="Seg_384" s="T1370">axe-INS</ta>
            <ta e="T1372" id="Seg_385" s="T1371">pillar-NOM/GEN/ACC.3SG</ta>
            <ta e="T1373" id="Seg_386" s="T1372">cut-INF.LAT</ta>
            <ta e="T1374" id="Seg_387" s="T1373">well</ta>
            <ta e="T1375" id="Seg_388" s="T1374">NEG</ta>
            <ta e="T1376" id="Seg_389" s="T1375">well</ta>
            <ta e="T1377" id="Seg_390" s="T1376">cut-IMP.2SG.O</ta>
            <ta e="T1378" id="Seg_391" s="T1377">what.[NOM.SG]</ta>
            <ta e="T1379" id="Seg_392" s="T1378">you.DAT</ta>
            <ta e="T1380" id="Seg_393" s="T1379">pity-PRS-2SG</ta>
            <ta e="T1381" id="Seg_394" s="T1380">this.[NOM.SG]</ta>
            <ta e="T1382" id="Seg_395" s="T1381">cut-PST.[3SG]</ta>
            <ta e="T1383" id="Seg_396" s="T1382">cut-PST.[3SG]</ta>
            <ta e="T1384" id="Seg_397" s="T1383">and</ta>
            <ta e="T1385" id="Seg_398" s="T1384">hook-NOM/GEN/ACC.3SG</ta>
            <ta e="T1386" id="Seg_399" s="T1385">PTCL</ta>
            <ta e="T1387" id="Seg_400" s="T1386">there</ta>
            <ta e="T1388" id="Seg_401" s="T1387">fall-MOM-PST.[3SG]</ta>
            <ta e="T1389" id="Seg_402" s="T1388">this.[NOM.SG]</ta>
            <ta e="T1390" id="Seg_403" s="T1389">PTCL</ta>
            <ta e="T1391" id="Seg_404" s="T1390">laugh-MOM-PST.[3SG]</ta>
            <ta e="T1392" id="Seg_405" s="T1391">PTCL</ta>
            <ta e="T1393" id="Seg_406" s="T1392">look</ta>
            <ta e="T1394" id="Seg_407" s="T1393">good</ta>
            <ta e="T1395" id="Seg_408" s="T1394">become-RES-PST.[3SG]</ta>
            <ta e="T1399" id="Seg_409" s="T1398">close-PST.[3SG]</ta>
            <ta e="T1400" id="Seg_410" s="T1399">gate-PL</ta>
            <ta e="T1401" id="Seg_411" s="T1400">now</ta>
            <ta e="T1403" id="Seg_412" s="T1401">pig-PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1358" id="Seg_413" s="T1357">я.NOM</ta>
            <ta e="T1359" id="Seg_414" s="T1358">сегодня</ta>
            <ta e="T1360" id="Seg_415" s="T1359">пойти-PST-1SG</ta>
            <ta e="T1361" id="Seg_416" s="T1360">один.[NOM.SG]</ta>
            <ta e="T1362" id="Seg_417" s="T1361">женщина-LAT</ta>
            <ta e="T1363" id="Seg_418" s="T1362">там</ta>
            <ta e="T1364" id="Seg_419" s="T1363">ворота-PL</ta>
            <ta e="T1365" id="Seg_420" s="T1364">NEG</ta>
            <ta e="T1366" id="Seg_421" s="T1365">закрыть-PRS-3PL</ta>
            <ta e="T1367" id="Seg_422" s="T1366">я.NOM</ta>
            <ta e="T1368" id="Seg_423" s="T1367">видеть-PST-1SG</ta>
            <ta e="T1369" id="Seg_424" s="T1368">сказать-IPFVZ-1SG</ta>
            <ta e="T1370" id="Seg_425" s="T1369">надо</ta>
            <ta e="T1371" id="Seg_426" s="T1370">топор-INS</ta>
            <ta e="T1372" id="Seg_427" s="T1371">столб-NOM/GEN/ACC.3SG</ta>
            <ta e="T1373" id="Seg_428" s="T1372">резать-INF.LAT</ta>
            <ta e="T1374" id="Seg_429" s="T1373">ну</ta>
            <ta e="T1375" id="Seg_430" s="T1374">NEG</ta>
            <ta e="T1376" id="Seg_431" s="T1375">ну</ta>
            <ta e="T1377" id="Seg_432" s="T1376">резать-IMP.2SG.O</ta>
            <ta e="T1378" id="Seg_433" s="T1377">что.[NOM.SG]</ta>
            <ta e="T1379" id="Seg_434" s="T1378">ты.DAT</ta>
            <ta e="T1380" id="Seg_435" s="T1379">жалеть-PRS-2SG</ta>
            <ta e="T1381" id="Seg_436" s="T1380">этот.[NOM.SG]</ta>
            <ta e="T1382" id="Seg_437" s="T1381">резать-PST.[3SG]</ta>
            <ta e="T1383" id="Seg_438" s="T1382">резать-PST.[3SG]</ta>
            <ta e="T1384" id="Seg_439" s="T1383">и</ta>
            <ta e="T1385" id="Seg_440" s="T1384">крючок-NOM/GEN/ACC.3SG</ta>
            <ta e="T1386" id="Seg_441" s="T1385">PTCL</ta>
            <ta e="T1387" id="Seg_442" s="T1386">там</ta>
            <ta e="T1388" id="Seg_443" s="T1387">упасть-MOM-PST.[3SG]</ta>
            <ta e="T1389" id="Seg_444" s="T1388">этот.[NOM.SG]</ta>
            <ta e="T1390" id="Seg_445" s="T1389">PTCL</ta>
            <ta e="T1391" id="Seg_446" s="T1390">смеяться-MOM-PST.[3SG]</ta>
            <ta e="T1392" id="Seg_447" s="T1391">PTCL</ta>
            <ta e="T1393" id="Seg_448" s="T1392">вот</ta>
            <ta e="T1394" id="Seg_449" s="T1393">хороший</ta>
            <ta e="T1395" id="Seg_450" s="T1394">стать-RES-PST.[3SG]</ta>
            <ta e="T1399" id="Seg_451" s="T1398">закрыть-PST.[3SG]</ta>
            <ta e="T1400" id="Seg_452" s="T1399">ворота-PL</ta>
            <ta e="T1401" id="Seg_453" s="T1400">сейчас</ta>
            <ta e="T1403" id="Seg_454" s="T1401">свинья-PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1358" id="Seg_455" s="T1357">pers</ta>
            <ta e="T1359" id="Seg_456" s="T1358">adv</ta>
            <ta e="T1360" id="Seg_457" s="T1359">v-v:tense-v:pn</ta>
            <ta e="T1361" id="Seg_458" s="T1360">num-n:case</ta>
            <ta e="T1362" id="Seg_459" s="T1361">n-n:case</ta>
            <ta e="T1363" id="Seg_460" s="T1362">adv</ta>
            <ta e="T1364" id="Seg_461" s="T1363">n-n:case</ta>
            <ta e="T1365" id="Seg_462" s="T1364">ptcl</ta>
            <ta e="T1366" id="Seg_463" s="T1365">v-v:tense-v:pn</ta>
            <ta e="T1367" id="Seg_464" s="T1366">pers</ta>
            <ta e="T1368" id="Seg_465" s="T1367">v-v:tense-v:pn</ta>
            <ta e="T1369" id="Seg_466" s="T1368">v-v&gt;v-v:pn</ta>
            <ta e="T1370" id="Seg_467" s="T1369">ptcl</ta>
            <ta e="T1371" id="Seg_468" s="T1370">n-n:case</ta>
            <ta e="T1372" id="Seg_469" s="T1371">n-n:case.poss</ta>
            <ta e="T1373" id="Seg_470" s="T1372">v-v:n.fin</ta>
            <ta e="T1374" id="Seg_471" s="T1373">ptcl</ta>
            <ta e="T1375" id="Seg_472" s="T1374">ptcl</ta>
            <ta e="T1376" id="Seg_473" s="T1375">ptcl</ta>
            <ta e="T1377" id="Seg_474" s="T1376">v-v:mood.pn</ta>
            <ta e="T1378" id="Seg_475" s="T1377">que-n:case</ta>
            <ta e="T1379" id="Seg_476" s="T1378">pers</ta>
            <ta e="T1380" id="Seg_477" s="T1379">v-v:tense-v:pn</ta>
            <ta e="T1381" id="Seg_478" s="T1380">dempro-n:case</ta>
            <ta e="T1382" id="Seg_479" s="T1381">v-v:tense-v:pn</ta>
            <ta e="T1383" id="Seg_480" s="T1382">v-v:tense-v:pn</ta>
            <ta e="T1384" id="Seg_481" s="T1383">conj</ta>
            <ta e="T1385" id="Seg_482" s="T1384">n-n:case.poss</ta>
            <ta e="T1386" id="Seg_483" s="T1385">ptcl</ta>
            <ta e="T1387" id="Seg_484" s="T1386">adv</ta>
            <ta e="T1388" id="Seg_485" s="T1387">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1389" id="Seg_486" s="T1388">dempro-n:case</ta>
            <ta e="T1390" id="Seg_487" s="T1389">ptcl</ta>
            <ta e="T1391" id="Seg_488" s="T1390">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1392" id="Seg_489" s="T1391">ptcl</ta>
            <ta e="T1393" id="Seg_490" s="T1392">ptcl</ta>
            <ta e="T1394" id="Seg_491" s="T1393">adj</ta>
            <ta e="T1395" id="Seg_492" s="T1394">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1399" id="Seg_493" s="T1398">v-v:tense-v:pn</ta>
            <ta e="T1400" id="Seg_494" s="T1399">n-n:num</ta>
            <ta e="T1401" id="Seg_495" s="T1400">adv</ta>
            <ta e="T1403" id="Seg_496" s="T1401">n-n:num</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1358" id="Seg_497" s="T1357">pers</ta>
            <ta e="T1359" id="Seg_498" s="T1358">adv</ta>
            <ta e="T1360" id="Seg_499" s="T1359">v</ta>
            <ta e="T1361" id="Seg_500" s="T1360">num</ta>
            <ta e="T1362" id="Seg_501" s="T1361">n</ta>
            <ta e="T1363" id="Seg_502" s="T1362">adv</ta>
            <ta e="T1364" id="Seg_503" s="T1363">n</ta>
            <ta e="T1365" id="Seg_504" s="T1364">ptcl</ta>
            <ta e="T1366" id="Seg_505" s="T1365">v</ta>
            <ta e="T1367" id="Seg_506" s="T1366">pers</ta>
            <ta e="T1368" id="Seg_507" s="T1367">v</ta>
            <ta e="T1369" id="Seg_508" s="T1368">v</ta>
            <ta e="T1370" id="Seg_509" s="T1369">ptcl</ta>
            <ta e="T1371" id="Seg_510" s="T1370">n</ta>
            <ta e="T1372" id="Seg_511" s="T1371">n</ta>
            <ta e="T1373" id="Seg_512" s="T1372">v</ta>
            <ta e="T1374" id="Seg_513" s="T1373">ptcl</ta>
            <ta e="T1375" id="Seg_514" s="T1374">ptcl</ta>
            <ta e="T1376" id="Seg_515" s="T1375">ptcl</ta>
            <ta e="T1377" id="Seg_516" s="T1376">v</ta>
            <ta e="T1378" id="Seg_517" s="T1377">que</ta>
            <ta e="T1379" id="Seg_518" s="T1378">pers</ta>
            <ta e="T1380" id="Seg_519" s="T1379">v</ta>
            <ta e="T1381" id="Seg_520" s="T1380">dempro</ta>
            <ta e="T1382" id="Seg_521" s="T1381">v</ta>
            <ta e="T1383" id="Seg_522" s="T1382">v</ta>
            <ta e="T1384" id="Seg_523" s="T1383">conj</ta>
            <ta e="T1385" id="Seg_524" s="T1384">n</ta>
            <ta e="T1386" id="Seg_525" s="T1385">ptcl</ta>
            <ta e="T1387" id="Seg_526" s="T1386">adv</ta>
            <ta e="T1388" id="Seg_527" s="T1387">v</ta>
            <ta e="T1389" id="Seg_528" s="T1388">dempro</ta>
            <ta e="T1390" id="Seg_529" s="T1389">ptcl</ta>
            <ta e="T1391" id="Seg_530" s="T1390">v</ta>
            <ta e="T1392" id="Seg_531" s="T1391">ptcl</ta>
            <ta e="T1393" id="Seg_532" s="T1392">ptcl</ta>
            <ta e="T1394" id="Seg_533" s="T1393">adj</ta>
            <ta e="T1395" id="Seg_534" s="T1394">v</ta>
            <ta e="T1399" id="Seg_535" s="T1398">v</ta>
            <ta e="T1400" id="Seg_536" s="T1399">n</ta>
            <ta e="T1401" id="Seg_537" s="T1400">adv</ta>
            <ta e="T1403" id="Seg_538" s="T1401">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1358" id="Seg_539" s="T1357">pro.h:S</ta>
            <ta e="T1360" id="Seg_540" s="T1359">v:pred</ta>
            <ta e="T1364" id="Seg_541" s="T1363">np:S</ta>
            <ta e="T1365" id="Seg_542" s="T1364">ptcl.neg</ta>
            <ta e="T1366" id="Seg_543" s="T1365">v:pred</ta>
            <ta e="T1367" id="Seg_544" s="T1366">pro.h:S</ta>
            <ta e="T1368" id="Seg_545" s="T1367">v:pred</ta>
            <ta e="T1369" id="Seg_546" s="T1368">0.1.h:S v:pred</ta>
            <ta e="T1370" id="Seg_547" s="T1369">ptcl:pred</ta>
            <ta e="T1372" id="Seg_548" s="T1371">np:O</ta>
            <ta e="T1375" id="Seg_549" s="T1374">ptcl.neg</ta>
            <ta e="T1377" id="Seg_550" s="T1376">0.2.h:S v:pred</ta>
            <ta e="T1380" id="Seg_551" s="T1379">0.2.h:S v:pred</ta>
            <ta e="T1381" id="Seg_552" s="T1380">pro.h:S</ta>
            <ta e="T1382" id="Seg_553" s="T1381">v:pred</ta>
            <ta e="T1383" id="Seg_554" s="T1382">0.3.h:S v:pred</ta>
            <ta e="T1385" id="Seg_555" s="T1384">np:S</ta>
            <ta e="T1388" id="Seg_556" s="T1387">v:pred</ta>
            <ta e="T1389" id="Seg_557" s="T1388">pro.h:S</ta>
            <ta e="T1391" id="Seg_558" s="T1390">v:pred</ta>
            <ta e="T1395" id="Seg_559" s="T1394">0.3:S v:pred</ta>
            <ta e="T1399" id="Seg_560" s="T1398"> v:pred</ta>
            <ta e="T1400" id="Seg_561" s="T1399">np:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1358" id="Seg_562" s="T1357">pro.h:A</ta>
            <ta e="T1359" id="Seg_563" s="T1358">adv:Time</ta>
            <ta e="T1362" id="Seg_564" s="T1361">np:G</ta>
            <ta e="T1363" id="Seg_565" s="T1362">adv:L</ta>
            <ta e="T1364" id="Seg_566" s="T1363">np:Th</ta>
            <ta e="T1367" id="Seg_567" s="T1366">pro.h:E</ta>
            <ta e="T1369" id="Seg_568" s="T1368">0.1.h:A</ta>
            <ta e="T1371" id="Seg_569" s="T1370">np:Ins</ta>
            <ta e="T1372" id="Seg_570" s="T1371">np:P</ta>
            <ta e="T1377" id="Seg_571" s="T1376">0.2.h:A</ta>
            <ta e="T1380" id="Seg_572" s="T1379">0.2.h:E</ta>
            <ta e="T1381" id="Seg_573" s="T1380">pro.h:A</ta>
            <ta e="T1383" id="Seg_574" s="T1382">0.3.h:A</ta>
            <ta e="T1385" id="Seg_575" s="T1384">np:Th</ta>
            <ta e="T1387" id="Seg_576" s="T1386">adv:L</ta>
            <ta e="T1389" id="Seg_577" s="T1388">pro.h:A</ta>
            <ta e="T1395" id="Seg_578" s="T1394">0.3:Th</ta>
            <ta e="T1400" id="Seg_579" s="T1399">np:Th</ta>
            <ta e="T1401" id="Seg_580" s="T1400">adv:Time</ta>
            <ta e="T1403" id="Seg_581" s="T1401">np:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1364" id="Seg_582" s="T1363">RUS:cult</ta>
            <ta e="T1370" id="Seg_583" s="T1369">RUS:mod</ta>
            <ta e="T1372" id="Seg_584" s="T1371">RUS:cult</ta>
            <ta e="T1374" id="Seg_585" s="T1373">RUS:disc</ta>
            <ta e="T1376" id="Seg_586" s="T1375">RUS:disc</ta>
            <ta e="T1384" id="Seg_587" s="T1383">RUS:gram</ta>
            <ta e="T1385" id="Seg_588" s="T1384">RUS:cult</ta>
            <ta e="T1386" id="Seg_589" s="T1385">TURK:disc</ta>
            <ta e="T1390" id="Seg_590" s="T1389">TURK:disc</ta>
            <ta e="T1392" id="Seg_591" s="T1391">TURK:disc</ta>
            <ta e="T1393" id="Seg_592" s="T1392">RUS:mod</ta>
            <ta e="T1394" id="Seg_593" s="T1393">TURK:core</ta>
            <ta e="T1400" id="Seg_594" s="T1399">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1362" id="Seg_595" s="T1357">Я сегодня ходила к одной женщине.</ta>
            <ta e="T1366" id="Seg_596" s="T1362">У неё ворота не закрываются.</ta>
            <ta e="T1373" id="Seg_597" s="T1366">Я увидела, говорю: надо топором по столбу ударить.</ta>
            <ta e="T1380" id="Seg_598" s="T1373">«Ну нет, ну ударь, тебе что, жалко». [?]</ta>
            <ta e="T1383" id="Seg_599" s="T1380">Она била, била.</ta>
            <ta e="T1388" id="Seg_600" s="T1383">И крючок вскочил туда [=на место].</ta>
            <ta e="T1392" id="Seg_601" s="T1388">Она засмеялась.</ta>
            <ta e="T1395" id="Seg_602" s="T1392">Вот, хорошо стало.</ta>
            <ta e="T1400" id="Seg_603" s="T1395">Ворота закрылись.</ta>
            <ta e="T1403" id="Seg_604" s="T1400">Теперь свиньи…</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1362" id="Seg_605" s="T1357">I went to a woman today.</ta>
            <ta e="T1366" id="Seg_606" s="T1362">Her gates do not close.</ta>
            <ta e="T1373" id="Seg_607" s="T1366">I saw, I say, one should whack its pole with an axe.</ta>
            <ta e="T1380" id="Seg_608" s="T1373">"Well, no, well, whack, what are you feeling sorry about." [?]</ta>
            <ta e="T1383" id="Seg_609" s="T1380">She whacked, whacked.</ta>
            <ta e="T1388" id="Seg_610" s="T1383">And the hook jumped in there.</ta>
            <ta e="T1392" id="Seg_611" s="T1388">She was laughing.</ta>
            <ta e="T1395" id="Seg_612" s="T1392">So, it became good.</ta>
            <ta e="T1400" id="Seg_613" s="T1395">The gates closed.</ta>
            <ta e="T1403" id="Seg_614" s="T1400">Now the pigs…</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1362" id="Seg_615" s="T1357">Ich ging heute zu einer Frau.</ta>
            <ta e="T1366" id="Seg_616" s="T1362">Ihre Pforten schließen nicht.</ta>
            <ta e="T1373" id="Seg_617" s="T1366">Ich sah, sage ich, es ist notwendig ihren Pfosten mit einer Axt zu schmettern.</ta>
            <ta e="T1380" id="Seg_618" s="T1373">„Also, nein, also, hack es, was bedauerst du.“ [?]</ta>
            <ta e="T1383" id="Seg_619" s="T1380">Sie schmetterte, schmetterte.</ta>
            <ta e="T1388" id="Seg_620" s="T1383">Und der Haken sprang dort.</ta>
            <ta e="T1392" id="Seg_621" s="T1388">Sie lachte.</ta>
            <ta e="T1395" id="Seg_622" s="T1392">So, wurde es gut.</ta>
            <ta e="T1400" id="Seg_623" s="T1395">Die Pforten schließen.</ta>
            <ta e="T1403" id="Seg_624" s="T1400">Nun die Schweine…</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1403" id="Seg_625" s="T1400">[GVY:] End of the recording. Probably "Now the pigs won't run away".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
