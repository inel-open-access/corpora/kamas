<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5774C2F4-3DCE-6AFD-B012-5951FA651934">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_TwistedArm_nar.wav" />
         <referenced-file url="PKZ_196X_TwistedArm_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_TwistedArm_nar\PKZ_196X_TwistedArm_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">41</ud-information>
            <ud-information attribute-name="# HIAT:w">29</ud-information>
            <ud-information attribute-name="# e">29</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.004" type="appl" />
         <tli id="T1" time="1.408" type="appl" />
         <tli id="T2" time="2.813" type="appl" />
         <tli id="T3" time="4.217" type="appl" />
         <tli id="T4" time="5.622" type="appl" />
         <tli id="T5" time="7.026" type="appl" />
         <tli id="T6" time="8.431" type="appl" />
         <tli id="T7" time="8.972" type="appl" />
         <tli id="T8" time="9.514" type="appl" />
         <tli id="T9" time="10.055" type="appl" />
         <tli id="T10" time="10.596" type="appl" />
         <tli id="T11" time="11.138" type="appl" />
         <tli id="T12" time="11.679" type="appl" />
         <tli id="T13" time="12.22" type="appl" />
         <tli id="T14" time="12.762" type="appl" />
         <tli id="T15" time="13.303" type="appl" />
         <tli id="T16" time="13.844" type="appl" />
         <tli id="T17" time="14.386" type="appl" />
         <tli id="T18" time="14.927" type="appl" />
         <tli id="T19" time="16.511" type="appl" />
         <tli id="T20" time="17.614" type="appl" />
         <tli id="T21" time="18.342" type="appl" />
         <tli id="T22" time="19.071" type="appl" />
         <tli id="T23" time="19.799" type="appl" />
         <tli id="T24" time="21.201" type="appl" />
         <tli id="T25" time="22.603" type="appl" />
         <tli id="T26" time="24.006" type="appl" />
         <tli id="T27" time="25.408" type="appl" />
         <tli id="T28" time="26.81" type="appl" />
         <tli id="T29" time="28.298" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T29" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nagur</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">maj</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">măn</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">büzəj</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">kundlaʔpiam</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">staikanə</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_25" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">Dĭ</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">măna</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">bar</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">saʔməluʔpi</ts>
                  <nts id="Seg_37" n="HIAT:ip">,</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">măn</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">dʼünə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">saʔməluʔpiam</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">i</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">udam</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">ugaːndə</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">tăŋ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">ĭzembi</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Pežərluʔpi</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_71" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">Šerzittə</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">ĭmbidə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">nʼelʼzʼa</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">bɨlə</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_86" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">Dĭgəttə</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">kundʼo</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_93" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">ĭzən-</ts>
                  <nts id="Seg_96" n="HIAT:ip">)</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">ĭzembi</ts>
                  <nts id="Seg_100" n="HIAT:ip">,</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">ĭzembi</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_107" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">Kabarləj</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T29" id="Seg_112" n="sc" s="T0">
               <ts e="T1" id="Seg_114" n="e" s="T0">Nagur </ts>
               <ts e="T2" id="Seg_116" n="e" s="T1">(maj) </ts>
               <ts e="T3" id="Seg_118" n="e" s="T2">măn </ts>
               <ts e="T4" id="Seg_120" n="e" s="T3">büzəj </ts>
               <ts e="T5" id="Seg_122" n="e" s="T4">kundlaʔpiam </ts>
               <ts e="T6" id="Seg_124" n="e" s="T5">staikanə. </ts>
               <ts e="T7" id="Seg_126" n="e" s="T6">Dĭ </ts>
               <ts e="T8" id="Seg_128" n="e" s="T7">măna </ts>
               <ts e="T9" id="Seg_130" n="e" s="T8">bar </ts>
               <ts e="T10" id="Seg_132" n="e" s="T9">saʔməluʔpi, </ts>
               <ts e="T11" id="Seg_134" n="e" s="T10">măn </ts>
               <ts e="T12" id="Seg_136" n="e" s="T11">dʼünə </ts>
               <ts e="T13" id="Seg_138" n="e" s="T12">saʔməluʔpiam </ts>
               <ts e="T14" id="Seg_140" n="e" s="T13">i </ts>
               <ts e="T15" id="Seg_142" n="e" s="T14">udam </ts>
               <ts e="T16" id="Seg_144" n="e" s="T15">ugaːndə </ts>
               <ts e="T17" id="Seg_146" n="e" s="T16">tăŋ </ts>
               <ts e="T18" id="Seg_148" n="e" s="T17">ĭzembi. </ts>
               <ts e="T19" id="Seg_150" n="e" s="T18">Pežərluʔpi. </ts>
               <ts e="T20" id="Seg_152" n="e" s="T19">Šerzittə </ts>
               <ts e="T21" id="Seg_154" n="e" s="T20">ĭmbidə </ts>
               <ts e="T22" id="Seg_156" n="e" s="T21">nʼelʼzʼa </ts>
               <ts e="T23" id="Seg_158" n="e" s="T22">bɨlə. </ts>
               <ts e="T24" id="Seg_160" n="e" s="T23">Dĭgəttə </ts>
               <ts e="T25" id="Seg_162" n="e" s="T24">kundʼo </ts>
               <ts e="T26" id="Seg_164" n="e" s="T25">(ĭzən-) </ts>
               <ts e="T27" id="Seg_166" n="e" s="T26">ĭzembi, </ts>
               <ts e="T28" id="Seg_168" n="e" s="T27">ĭzembi. </ts>
               <ts e="T29" id="Seg_170" n="e" s="T28">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_171" s="T0">PKZ_196X_TwistedArm_nar.001 (001)</ta>
            <ta e="T18" id="Seg_172" s="T6">PKZ_196X_TwistedArm_nar.002 (003)</ta>
            <ta e="T19" id="Seg_173" s="T18">PKZ_196X_TwistedArm_nar.003 (004)</ta>
            <ta e="T23" id="Seg_174" s="T19">PKZ_196X_TwistedArm_nar.004 (005)</ta>
            <ta e="T28" id="Seg_175" s="T23">PKZ_196X_TwistedArm_nar.005 (006)</ta>
            <ta e="T29" id="Seg_176" s="T28">PKZ_196X_TwistedArm_nar.006 (007)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_177" s="T0">Nagur (maj) măn büzəj kundlaʔpiam staikanə. </ta>
            <ta e="T18" id="Seg_178" s="T6">Dĭ măna bar saʔməluʔpi, măn dʼünə saʔməluʔpiam i udam ugaːndə tăŋ ĭzembi. </ta>
            <ta e="T19" id="Seg_179" s="T18">Pežərluʔpi. </ta>
            <ta e="T23" id="Seg_180" s="T19">Šerzittə ĭmbidə nʼelʼzʼa bɨlə. </ta>
            <ta e="T28" id="Seg_181" s="T23">Dĭgəttə kundʼo (ĭzən-) ĭzembi, ĭzembi. </ta>
            <ta e="T29" id="Seg_182" s="T28">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_183" s="T0">nagur</ta>
            <ta e="T2" id="Seg_184" s="T1">maj</ta>
            <ta e="T3" id="Seg_185" s="T2">măn</ta>
            <ta e="T4" id="Seg_186" s="T3">büzəj</ta>
            <ta e="T5" id="Seg_187" s="T4">kund-laʔ-pia-m</ta>
            <ta e="T6" id="Seg_188" s="T5">staika-nə</ta>
            <ta e="T7" id="Seg_189" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_190" s="T7">măna</ta>
            <ta e="T9" id="Seg_191" s="T8">bar</ta>
            <ta e="T10" id="Seg_192" s="T9">saʔmə-luʔ-pi</ta>
            <ta e="T11" id="Seg_193" s="T10">măn</ta>
            <ta e="T12" id="Seg_194" s="T11">dʼü-nə</ta>
            <ta e="T13" id="Seg_195" s="T12">saʔmə-luʔ-pia-m</ta>
            <ta e="T14" id="Seg_196" s="T13">i</ta>
            <ta e="T15" id="Seg_197" s="T14">uda-m</ta>
            <ta e="T16" id="Seg_198" s="T15">ugaːndə</ta>
            <ta e="T17" id="Seg_199" s="T16">tăŋ</ta>
            <ta e="T18" id="Seg_200" s="T17">ĭzem-bi</ta>
            <ta e="T19" id="Seg_201" s="T18">pežər-luʔ-pi</ta>
            <ta e="T20" id="Seg_202" s="T19">šer-zittə</ta>
            <ta e="T21" id="Seg_203" s="T20">ĭmbi=də</ta>
            <ta e="T24" id="Seg_204" s="T23">dĭgəttə</ta>
            <ta e="T25" id="Seg_205" s="T24">kundʼo</ta>
            <ta e="T27" id="Seg_206" s="T26">ĭzem-bi</ta>
            <ta e="T28" id="Seg_207" s="T27">ĭzem-bi</ta>
            <ta e="T29" id="Seg_208" s="T28">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_209" s="T0">nagur</ta>
            <ta e="T2" id="Seg_210" s="T1">maj</ta>
            <ta e="T3" id="Seg_211" s="T2">măn</ta>
            <ta e="T4" id="Seg_212" s="T3">büzəj</ta>
            <ta e="T5" id="Seg_213" s="T4">kun-laʔbə-bi-m</ta>
            <ta e="T6" id="Seg_214" s="T5">staika-Tə</ta>
            <ta e="T7" id="Seg_215" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_216" s="T7">măna</ta>
            <ta e="T9" id="Seg_217" s="T8">bar</ta>
            <ta e="T10" id="Seg_218" s="T9">saʔmə-luʔbdə-bi</ta>
            <ta e="T11" id="Seg_219" s="T10">măn</ta>
            <ta e="T12" id="Seg_220" s="T11">tʼo-Tə</ta>
            <ta e="T13" id="Seg_221" s="T12">saʔmə-luʔbdə-bi-m</ta>
            <ta e="T14" id="Seg_222" s="T13">i</ta>
            <ta e="T15" id="Seg_223" s="T14">uda-m</ta>
            <ta e="T16" id="Seg_224" s="T15">ugaːndə</ta>
            <ta e="T17" id="Seg_225" s="T16">tăŋ</ta>
            <ta e="T18" id="Seg_226" s="T17">ĭzem-bi</ta>
            <ta e="T19" id="Seg_227" s="T18">pežər-luʔbdə-bi</ta>
            <ta e="T20" id="Seg_228" s="T19">šer-zittə</ta>
            <ta e="T21" id="Seg_229" s="T20">ĭmbi=də</ta>
            <ta e="T24" id="Seg_230" s="T23">dĭgəttə</ta>
            <ta e="T25" id="Seg_231" s="T24">kondʼo</ta>
            <ta e="T27" id="Seg_232" s="T26">ĭzem-bi</ta>
            <ta e="T28" id="Seg_233" s="T27">ĭzem-bi</ta>
            <ta e="T29" id="Seg_234" s="T28">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_235" s="T0">three.[NOM.SG]</ta>
            <ta e="T2" id="Seg_236" s="T1">May.[NOM.SG]</ta>
            <ta e="T3" id="Seg_237" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_238" s="T3">calf.[NOM.SG]</ta>
            <ta e="T5" id="Seg_239" s="T4">bring-DUR-PST-1SG</ta>
            <ta e="T6" id="Seg_240" s="T5">shed-LAT</ta>
            <ta e="T7" id="Seg_241" s="T6">this.[NOM.SG]</ta>
            <ta e="T8" id="Seg_242" s="T7">I.LAT</ta>
            <ta e="T9" id="Seg_243" s="T8">PTCL</ta>
            <ta e="T10" id="Seg_244" s="T9">fall-MOM-PST.[3SG]</ta>
            <ta e="T11" id="Seg_245" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_246" s="T11">place-LAT</ta>
            <ta e="T13" id="Seg_247" s="T12">fall-MOM-PST-1SG</ta>
            <ta e="T14" id="Seg_248" s="T13">and</ta>
            <ta e="T15" id="Seg_249" s="T14">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T16" id="Seg_250" s="T15">very</ta>
            <ta e="T17" id="Seg_251" s="T16">strongly</ta>
            <ta e="T18" id="Seg_252" s="T17">hurt-PST.[3SG]</ta>
            <ta e="T19" id="Seg_253" s="T18">swell-MOM-PST.[3SG]</ta>
            <ta e="T20" id="Seg_254" s="T19">dress-INF.LAT</ta>
            <ta e="T21" id="Seg_255" s="T20">what.[NOM.SG]=INDEF</ta>
            <ta e="T24" id="Seg_256" s="T23">then</ta>
            <ta e="T25" id="Seg_257" s="T24">long.time</ta>
            <ta e="T27" id="Seg_258" s="T26">hurt-PST.[3SG]</ta>
            <ta e="T28" id="Seg_259" s="T27">hurt-PST.[3SG]</ta>
            <ta e="T29" id="Seg_260" s="T28">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_261" s="T0">три.[NOM.SG]</ta>
            <ta e="T2" id="Seg_262" s="T1">май.[NOM.SG]</ta>
            <ta e="T3" id="Seg_263" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_264" s="T3">теленок.[NOM.SG]</ta>
            <ta e="T5" id="Seg_265" s="T4">нести-DUR-PST-1SG</ta>
            <ta e="T6" id="Seg_266" s="T5">хлев-LAT</ta>
            <ta e="T7" id="Seg_267" s="T6">этот.[NOM.SG]</ta>
            <ta e="T8" id="Seg_268" s="T7">я.LAT</ta>
            <ta e="T9" id="Seg_269" s="T8">PTCL</ta>
            <ta e="T10" id="Seg_270" s="T9">упасть-MOM-PST.[3SG]</ta>
            <ta e="T11" id="Seg_271" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_272" s="T11">место-LAT</ta>
            <ta e="T13" id="Seg_273" s="T12">упасть-MOM-PST-1SG</ta>
            <ta e="T14" id="Seg_274" s="T13">и</ta>
            <ta e="T15" id="Seg_275" s="T14">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T16" id="Seg_276" s="T15">очень</ta>
            <ta e="T17" id="Seg_277" s="T16">сильно</ta>
            <ta e="T18" id="Seg_278" s="T17">болеть-PST.[3SG]</ta>
            <ta e="T19" id="Seg_279" s="T18">опухнуть-MOM-PST.[3SG]</ta>
            <ta e="T20" id="Seg_280" s="T19">надеть-INF.LAT</ta>
            <ta e="T21" id="Seg_281" s="T20">что.[NOM.SG]=INDEF</ta>
            <ta e="T24" id="Seg_282" s="T23">тогда</ta>
            <ta e="T25" id="Seg_283" s="T24">долго</ta>
            <ta e="T27" id="Seg_284" s="T26">болеть-PST.[3SG]</ta>
            <ta e="T28" id="Seg_285" s="T27">болеть-PST.[3SG]</ta>
            <ta e="T29" id="Seg_286" s="T28">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_287" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_288" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_289" s="T2">pers</ta>
            <ta e="T4" id="Seg_290" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_291" s="T4">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_292" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_293" s="T6">dempro-n:case</ta>
            <ta e="T8" id="Seg_294" s="T7">pers</ta>
            <ta e="T9" id="Seg_295" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_296" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_297" s="T10">pers</ta>
            <ta e="T12" id="Seg_298" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_299" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_300" s="T13">conj</ta>
            <ta e="T15" id="Seg_301" s="T14">n-n:case.poss</ta>
            <ta e="T16" id="Seg_302" s="T15">adv</ta>
            <ta e="T17" id="Seg_303" s="T16">adv</ta>
            <ta e="T18" id="Seg_304" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_305" s="T18">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_306" s="T19">v-v:n.fin</ta>
            <ta e="T21" id="Seg_307" s="T20">que-n:case=ptcl</ta>
            <ta e="T24" id="Seg_308" s="T23">adv</ta>
            <ta e="T25" id="Seg_309" s="T24">adv</ta>
            <ta e="T27" id="Seg_310" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_311" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_312" s="T28">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_313" s="T0">num</ta>
            <ta e="T2" id="Seg_314" s="T1">n</ta>
            <ta e="T3" id="Seg_315" s="T2">pers</ta>
            <ta e="T4" id="Seg_316" s="T3">n</ta>
            <ta e="T5" id="Seg_317" s="T4">v</ta>
            <ta e="T6" id="Seg_318" s="T5">n</ta>
            <ta e="T7" id="Seg_319" s="T6">dempro</ta>
            <ta e="T8" id="Seg_320" s="T7">pers</ta>
            <ta e="T9" id="Seg_321" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_322" s="T9">v</ta>
            <ta e="T11" id="Seg_323" s="T10">pers</ta>
            <ta e="T12" id="Seg_324" s="T11">n</ta>
            <ta e="T13" id="Seg_325" s="T12">v</ta>
            <ta e="T14" id="Seg_326" s="T13">conj</ta>
            <ta e="T15" id="Seg_327" s="T14">n</ta>
            <ta e="T16" id="Seg_328" s="T15">adv</ta>
            <ta e="T17" id="Seg_329" s="T16">adv</ta>
            <ta e="T18" id="Seg_330" s="T17">v</ta>
            <ta e="T19" id="Seg_331" s="T18">v</ta>
            <ta e="T20" id="Seg_332" s="T19">v</ta>
            <ta e="T21" id="Seg_333" s="T20">que</ta>
            <ta e="T24" id="Seg_334" s="T23">adv</ta>
            <ta e="T25" id="Seg_335" s="T24">adv</ta>
            <ta e="T27" id="Seg_336" s="T26">v</ta>
            <ta e="T28" id="Seg_337" s="T27">v</ta>
            <ta e="T29" id="Seg_338" s="T28">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_339" s="T1">n:Time</ta>
            <ta e="T3" id="Seg_340" s="T2">pro.h:A</ta>
            <ta e="T4" id="Seg_341" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_342" s="T5">np:G</ta>
            <ta e="T7" id="Seg_343" s="T6">pro:E</ta>
            <ta e="T8" id="Seg_344" s="T7">pro.h:G</ta>
            <ta e="T11" id="Seg_345" s="T10">pro.h:E</ta>
            <ta e="T12" id="Seg_346" s="T11">np:G</ta>
            <ta e="T15" id="Seg_347" s="T14">np:E</ta>
            <ta e="T19" id="Seg_348" s="T18">0.3:P</ta>
            <ta e="T21" id="Seg_349" s="T20">pro:Th</ta>
            <ta e="T24" id="Seg_350" s="T23">adv:Time</ta>
            <ta e="T27" id="Seg_351" s="T26">0.3:E</ta>
            <ta e="T28" id="Seg_352" s="T27">0.3:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_353" s="T2">pro.h:S</ta>
            <ta e="T4" id="Seg_354" s="T3">np:O</ta>
            <ta e="T5" id="Seg_355" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_356" s="T6">pro:S</ta>
            <ta e="T10" id="Seg_357" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_358" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_359" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_360" s="T14">np:S</ta>
            <ta e="T18" id="Seg_361" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_362" s="T18">v:pred 0.3:S</ta>
            <ta e="T20" id="Seg_363" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_364" s="T20">pro:O</ta>
            <ta e="T27" id="Seg_365" s="T26">v:pred 0.3:S</ta>
            <ta e="T28" id="Seg_366" s="T27">v:pred 0.3:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_367" s="T1">RUS:cult</ta>
            <ta e="T6" id="Seg_368" s="T5">RUS:cult</ta>
            <ta e="T9" id="Seg_369" s="T8">TURK:disc</ta>
            <ta e="T14" id="Seg_370" s="T13">RUS:gram</ta>
            <ta e="T21" id="Seg_371" s="T20">TURK:gram(INDEF)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T2" id="Seg_372" s="T1">dir:bare</ta>
            <ta e="T6" id="Seg_373" s="T5">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T23" id="Seg_374" s="T21">RUS:int.alt</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_375" s="T0">Третьего мая я вела теленка в хлев.</ta>
            <ta e="T18" id="Seg_376" s="T6">Он упал на меня, я упала на землю, и руку очень сильно повредила.</ta>
            <ta e="T19" id="Seg_377" s="T18">Она опухла.</ta>
            <ta e="T23" id="Seg_378" s="T19">Ничего нельзя было надеть.</ta>
            <ta e="T28" id="Seg_379" s="T23">Потом она долго болела, болела.</ta>
            <ta e="T29" id="Seg_380" s="T28">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_381" s="T0">On the 3rd of May I took the calf to the shed.</ta>
            <ta e="T18" id="Seg_382" s="T6">It fell on me, I fell on the ground, and my hand got hurt badly.</ta>
            <ta e="T19" id="Seg_383" s="T18">It swelled up.</ta>
            <ta e="T23" id="Seg_384" s="T19">It was impossible to put up anything.</ta>
            <ta e="T28" id="Seg_385" s="T23">Then for a long time it hurt, it hurt.</ta>
            <ta e="T29" id="Seg_386" s="T28">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_387" s="T0">Am dritten Mai brachte ich das Kalb in den Stall.</ta>
            <ta e="T18" id="Seg_388" s="T6">Es fiel auf mich, ich fiel auf den Boden und meine Hand wurde schwer verletzt.</ta>
            <ta e="T19" id="Seg_389" s="T18">Sie schwoll an.</ta>
            <ta e="T23" id="Seg_390" s="T19">Es war unmöglich etwas anzuziehen.</ta>
            <ta e="T28" id="Seg_391" s="T23">Dann schmerzte und schmerzte es für eine lange Zeit.</ta>
            <ta e="T29" id="Seg_392" s="T28">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_393" s="T0">[GVY:] Стайка is a dialectal word for the cattle shed.</ta>
            <ta e="T18" id="Seg_394" s="T6">[GVY:] The cow fell or stumbled?</ta>
            <ta e="T19" id="Seg_395" s="T18">[GVY:] pronounced [z]</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
