<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA1EA684F-D2FE-6F3B-CD4A-62DBE2A0636F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_TripToAginskoe2_nar.wav" />
         <referenced-file url="PKZ_196X_TripToAginskoe2_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_TripToAginskoe2_nar\PKZ_196X_TripToAginskoe2_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">280</ud-information>
            <ud-information attribute-name="# HIAT:w">189</ud-information>
            <ud-information attribute-name="# e">185</ud-information>
            <ud-information attribute-name="# HIAT:u">43</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.002" type="appl" />
         <tli id="T1" time="0.889" type="appl" />
         <tli id="T2" time="1.775" type="appl" />
         <tli id="T3" time="2.662" type="appl" />
         <tli id="T185" time="3.0164" type="intp" />
         <tli id="T4" time="3.548" type="appl" />
         <tli id="T5" time="4.369" type="appl" />
         <tli id="T6" time="5.185" type="appl" />
         <tli id="T7" time="6.0" type="appl" />
         <tli id="T8" time="6.816" type="appl" />
         <tli id="T9" time="7.631" type="appl" />
         <tli id="T10" time="8.215" type="appl" />
         <tli id="T11" time="8.798" type="appl" />
         <tli id="T12" time="9.381" type="appl" />
         <tli id="T13" time="9.965" type="appl" />
         <tli id="T14" time="10.548" type="appl" />
         <tli id="T15" time="11.132" type="appl" />
         <tli id="T16" time="12.829" type="appl" />
         <tli id="T17" time="14.526" type="appl" />
         <tli id="T18" time="16.224" type="appl" />
         <tli id="T19" time="17.921" type="appl" />
         <tli id="T20" time="19.618" type="appl" />
         <tli id="T21" time="20.471" type="appl" />
         <tli id="T22" time="21.323" type="appl" />
         <tli id="T23" time="22.176" type="appl" />
         <tli id="T24" time="23.144" type="appl" />
         <tli id="T25" time="24.113" type="appl" />
         <tli id="T26" time="25.081" type="appl" />
         <tli id="T27" time="26.05" type="appl" />
         <tli id="T28" time="27.018" type="appl" />
         <tli id="T29" time="27.986" type="appl" />
         <tli id="T30" time="28.955" type="appl" />
         <tli id="T31" time="30.232532303428034" />
         <tli id="T32" time="30.919" type="appl" />
         <tli id="T33" time="31.749" type="appl" />
         <tli id="T34" time="32.578" type="appl" />
         <tli id="T35" time="33.408" type="appl" />
         <tli id="T36" time="34.43242102474219" />
         <tli id="T37" time="37.225680348917784" />
         <tli id="T38" time="38.067" type="appl" />
         <tli id="T39" time="38.944" type="appl" />
         <tli id="T40" time="39.822" type="appl" />
         <tli id="T41" time="40.624" type="appl" />
         <tli id="T42" time="41.425" type="appl" />
         <tli id="T43" time="42.227" type="appl" />
         <tli id="T44" time="43.028" type="appl" />
         <tli id="T45" time="43.83" type="appl" />
         <tli id="T46" time="44.631" type="appl" />
         <tli id="T47" time="48.88537141808992" />
         <tli id="T48" time="49.855" type="appl" />
         <tli id="T49" time="50.918" type="appl" />
         <tli id="T186" time="51.37314285714285" type="intp" />
         <tli id="T50" time="51.98" type="appl" />
         <tli id="T51" time="53.22" type="appl" />
         <tli id="T52" time="54.46" type="appl" />
         <tli id="T53" time="55.571" type="appl" />
         <tli id="T54" time="56.683" type="appl" />
         <tli id="T55" time="57.794" type="appl" />
         <tli id="T56" time="58.525" type="appl" />
         <tli id="T57" time="59.256" type="appl" />
         <tli id="T58" time="59.988" type="appl" />
         <tli id="T59" time="60.719" type="appl" />
         <tli id="T60" time="61.45" type="appl" />
         <tli id="T61" time="62.282" type="appl" />
         <tli id="T62" time="63.114" type="appl" />
         <tli id="T63" time="64.53829001752742" />
         <tli id="T64" time="65.62" type="appl" />
         <tli id="T65" time="66.696" type="appl" />
         <tli id="T66" time="67.311" type="appl" />
         <tli id="T67" time="67.926" type="appl" />
         <tli id="T68" time="68.542" type="appl" />
         <tli id="T69" time="70.88478852973545" />
         <tli id="T70" time="71.519" type="appl" />
         <tli id="T71" time="72.102" type="appl" />
         <tli id="T72" time="72.686" type="appl" />
         <tli id="T73" time="73.292" type="appl" />
         <tli id="T74" time="73.897" type="appl" />
         <tli id="T75" time="75.26467248196306" />
         <tli id="T76" time="76.135" type="appl" />
         <tli id="T77" time="76.814" type="appl" />
         <tli id="T78" time="77.493" type="appl" />
         <tli id="T79" time="78.172" type="appl" />
         <tli id="T80" time="78.851" type="appl" />
         <tli id="T81" time="79.53" type="appl" />
         <tli id="T82" time="81.07785179146457" />
         <tli id="T83" time="82.641" type="appl" />
         <tli id="T84" time="84.161" type="appl" />
         <tli id="T85" time="85.681" type="appl" />
         <tli id="T86" time="86.564" type="appl" />
         <tli id="T87" time="87.443" type="appl" />
         <tli id="T88" time="88.569" type="appl" />
         <tli id="T89" time="89.696" type="appl" />
         <tli id="T90" time="90.822" type="appl" />
         <tli id="T91" time="92.31088749847144" />
         <tli id="T92" time="93.346" type="appl" />
         <tli id="T93" time="94.54" type="appl" />
         <tli id="T94" time="95.733" type="appl" />
         <tli id="T95" time="96.927" type="appl" />
         <tli id="T96" time="98.12" type="appl" />
         <tli id="T97" time="98.696" type="appl" />
         <tli id="T98" time="99.272" type="appl" />
         <tli id="T99" time="99.848" type="appl" />
         <tli id="T100" time="100.424" type="appl" />
         <tli id="T101" time="101.0" type="appl" />
         <tli id="T102" time="101.576" type="appl" />
         <tli id="T103" time="102.152" type="appl" />
         <tli id="T104" time="102.98393804263645" />
         <tli id="T105" time="103.763" type="appl" />
         <tli id="T106" time="104.799" type="appl" />
         <tli id="T107" time="106.6838400114132" />
         <tli id="T108" time="107.372" type="appl" />
         <tli id="T109" time="108.188" type="appl" />
         <tli id="T110" time="109.64376158643461" />
         <tli id="T111" time="110.165" type="appl" />
         <tli id="T112" time="110.752" type="appl" />
         <tli id="T113" time="111.34" type="appl" />
         <tli id="T114" time="111.927" type="appl" />
         <tli id="T115" time="113.26366567480537" />
         <tli id="T116" time="114.157" type="appl" />
         <tli id="T117" time="115.051" type="appl" />
         <tli id="T118" time="115.945" type="appl" />
         <tli id="T119" time="116.453" type="appl" />
         <tli id="T120" time="116.956" type="appl" />
         <tli id="T121" time="117.459" type="appl" />
         <tli id="T122" time="118.04" type="appl" />
         <tli id="T123" time="118.621" type="appl" />
         <tli id="T124" time="119.202" type="appl" />
         <tli id="T125" time="119.783" type="appl" />
         <tli id="T126" time="120.642" type="appl" />
         <tli id="T127" time="121.502" type="appl" />
         <tli id="T128" time="122.361" type="appl" />
         <tli id="T129" time="123.221" type="appl" />
         <tli id="T130" time="124.30337317083112" />
         <tli id="T131" time="124.867" type="appl" />
         <tli id="T132" time="125.574" type="appl" />
         <tli id="T133" time="126.282" type="appl" />
         <tli id="T134" time="126.989" type="appl" />
         <tli id="T135" time="127.696" type="appl" />
         <tli id="T136" time="128.403" type="appl" />
         <tli id="T137" time="129.056" type="appl" />
         <tli id="T138" time="129.71" type="appl" />
         <tli id="T139" time="130.363" type="appl" />
         <tli id="T140" time="131.016" type="appl" />
         <tli id="T141" time="132.068" type="appl" />
         <tli id="T142" time="133.121" type="appl" />
         <tli id="T143" time="133.819" type="appl" />
         <tli id="T144" time="134.517" type="appl" />
         <tli id="T145" time="135.216" type="appl" />
         <tli id="T146" time="136.1830584111197" />
         <tli id="T147" time="136.525" type="appl" />
         <tli id="T148" time="137.136" type="appl" />
         <tli id="T149" time="137.747" type="appl" />
         <tli id="T150" time="138.774" type="appl" />
         <tli id="T151" time="139.801" type="appl" />
         <tli id="T152" time="140.828" type="appl" />
         <tli id="T153" time="141.642" type="appl" />
         <tli id="T154" time="142.457" type="appl" />
         <tli id="T155" time="143.272" type="appl" />
         <tli id="T156" time="144.086" type="appl" />
         <tli id="T157" time="144.927" type="appl" />
         <tli id="T158" time="145.768" type="appl" />
         <tli id="T159" time="146.609" type="appl" />
         <tli id="T160" time="147.45" type="appl" />
         <tli id="T161" time="148.186" type="appl" />
         <tli id="T162" time="148.911" type="appl" />
         <tli id="T163" time="149.636" type="appl" />
         <tli id="T164" time="150.36" type="appl" />
         <tli id="T165" time="151.085" type="appl" />
         <tli id="T166" time="151.81" type="appl" />
         <tli id="T167" time="152.535" type="appl" />
         <tli id="T168" time="153.26" type="appl" />
         <tli id="T169" time="153.853" type="appl" />
         <tli id="T170" time="154.445" type="appl" />
         <tli id="T171" time="155.038" type="appl" />
         <tli id="T172" time="155.631" type="appl" />
         <tli id="T173" time="156.273" type="appl" />
         <tli id="T174" time="156.915" type="appl" />
         <tli id="T175" time="157.556" type="appl" />
         <tli id="T176" time="158.198" type="appl" />
         <tli id="T177" time="158.84" type="appl" />
         <tli id="T178" time="159.482" type="appl" />
         <tli id="T179" time="160.124" type="appl" />
         <tli id="T180" time="160.766" type="appl" />
         <tli id="T181" time="161.407" type="appl" />
         <tli id="T182" time="162.049" type="appl" />
         <tli id="T183" time="162.691" type="appl" />
         <tli id="T184" time="163.549" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T20" start="T19">
            <tli id="T19.tx.1" />
         </timeline-fork>
         <timeline-fork end="T52" start="T51">
            <tli id="T51.tx.1" />
         </timeline-fork>
         <timeline-fork end="T60" start="T59">
            <tli id="T59.tx.1" />
         </timeline-fork>
         <timeline-fork end="T65" start="T64">
            <tli id="T64.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T184" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Miʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">koʔbdom</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">šobi</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_15" n="HIAT:w" s="T3">Kazan</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_18" n="HIAT:w" s="T185">turagən</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_22" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_24" n="HIAT:w" s="T4">Măndə:</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_27" n="HIAT:w" s="T5">Urgaja</ts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_31" n="HIAT:w" s="T6">tăn</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">sazəngən</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">amnolaʔbəl</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">A</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">kăde</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">măr-</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_54" n="HIAT:w" s="T12">măn</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_57" n="HIAT:w" s="T13">nereʔluʔpiem</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">bar</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_64" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_66" n="HIAT:w" s="T15">Možet</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_70" n="HIAT:w" s="T16">tenöbiam</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">što</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_77" n="HIAT:w" s="T18">kudajdə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx.1" id="Seg_80" n="HIAT:w" s="T19">numan</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T19.tx.1">üzleʔbəm</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_87" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_89" n="HIAT:w" s="T20">Dĭgəttə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_92" n="HIAT:w" s="T21">sazəndə</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_95" n="HIAT:w" s="T22">amnolbiʔi</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_99" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_101" n="HIAT:w" s="T23">Dĭ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_104" n="HIAT:w" s="T24">măndə:</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_106" n="HIAT:ip">"</nts>
                  <ts e="T26" id="Seg_108" n="HIAT:w" s="T25">Dʼok</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_112" n="HIAT:w" s="T26">tăn</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_114" n="HIAT:ip">(</nts>
                  <ts e="T28" id="Seg_116" n="HIAT:w" s="T27">dʼăbaktərbiziʔ-</ts>
                  <nts id="Seg_117" n="HIAT:ip">)</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_120" n="HIAT:w" s="T28">dʼăbaktərlaʔbəl</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_123" n="HIAT:w" s="T29">bostə</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_126" n="HIAT:w" s="T30">šĭkətsiʔ</ts>
                  <nts id="Seg_127" n="HIAT:ip">"</nts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_131" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_133" n="HIAT:w" s="T31">Dĭgəttə</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_136" n="HIAT:w" s="T32">măn</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_139" n="HIAT:w" s="T33">tenöbiem</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_143" n="HIAT:w" s="T34">možet</ts>
                  <nts id="Seg_144" n="HIAT:ip">…</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_147" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_149" n="HIAT:w" s="T36">Kabarləj</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_153" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_155" n="HIAT:w" s="T37">Dĭgəttə</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_158" n="HIAT:w" s="T38">măn</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_161" n="HIAT:w" s="T39">tenöbiem</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_165" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_167" n="HIAT:w" s="T40">Dĭ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_170" n="HIAT:w" s="T41">Jelʼa</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">pʼaŋdəbi</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">măna</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_180" n="HIAT:w" s="T44">dĭbər</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_183" n="HIAT:w" s="T45">amnolbi</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_186" n="HIAT:w" s="T46">sazəndə</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_190" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_192" n="HIAT:w" s="T47">Măn</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_195" n="HIAT:w" s="T48">mĭmbiem</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_198" n="HIAT:w" s="T49">Kazan</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_201" n="HIAT:w" s="T186">turanə</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_205" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_207" n="HIAT:w" s="T50">Kudajdə</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51.tx.1" id="Seg_210" n="HIAT:w" s="T51">numan</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_213" n="HIAT:w" s="T51.tx.1">üzəsʼtə</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_217" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_219" n="HIAT:w" s="T52">Dĭgəttə</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_222" n="HIAT:w" s="T53">šobiam</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_225" n="HIAT:w" s="T54">dĭbər</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_229" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_231" n="HIAT:w" s="T55">Dĭn</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_234" n="HIAT:w" s="T56">ej</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_237" n="HIAT:w" s="T57">šobiʔi</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">kudajdə</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <ts e="T59.tx.1" id="Seg_244" n="HIAT:w" s="T59">numan</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_247" n="HIAT:w" s="T59.tx.1">üzəsʼtə</ts>
                  <nts id="Seg_248" n="HIAT:ip">)</nts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_252" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_254" n="HIAT:w" s="T60">Dĭgəttə</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_257" n="HIAT:w" s="T61">unnʼazaŋdə</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_260" n="HIAT:w" s="T62">bar</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_264" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_266" n="HIAT:w" s="T63">Kudajdə</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx.1" id="Seg_269" n="HIAT:w" s="T64">numan</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_272" n="HIAT:w" s="T64.tx.1">üzəbibeʔ</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_276" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_278" n="HIAT:w" s="T65">Dĭgəttə</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_281" n="HIAT:w" s="T66">măn</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_284" n="HIAT:w" s="T67">kambiam</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_287" n="HIAT:w" s="T68">maːʔnʼi</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_291" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_293" n="HIAT:w" s="T69">Dĭzeŋ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_296" n="HIAT:w" s="T70">ej</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_299" n="HIAT:w" s="T71">öʔlieʔi</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_303" n="HIAT:u" s="T72">
                  <nts id="Seg_304" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_306" n="HIAT:w" s="T72">Dʼok</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_310" n="HIAT:w" s="T73">măndəm</ts>
                  <nts id="Seg_311" n="HIAT:ip">,</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_314" n="HIAT:w" s="T74">kallam</ts>
                  <nts id="Seg_315" n="HIAT:ip">"</nts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_319" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_321" n="HIAT:w" s="T75">Kambiam</ts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_325" n="HIAT:w" s="T76">kambiam</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_329" n="HIAT:w" s="T77">surno</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_332" n="HIAT:w" s="T78">bar</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_335" n="HIAT:w" s="T79">šonəga</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_339" n="HIAT:w" s="T80">da</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_342" n="HIAT:w" s="T81">beržə</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_346" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_348" n="HIAT:w" s="T82">Dĭgəttə</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_351" n="HIAT:w" s="T83">šobiam</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_355" n="HIAT:w" s="T84">šobiam</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_359" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_361" n="HIAT:w" s="T85">Mašina</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_364" n="HIAT:w" s="T86">nuga</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_368" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_370" n="HIAT:w" s="T87">Măn</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_373" n="HIAT:w" s="T88">šobiam</ts>
                  <nts id="Seg_374" n="HIAT:ip">,</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_377" n="HIAT:w" s="T89">šobiam</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_380" n="HIAT:ip">(</nts>
                  <ts e="T91" id="Seg_382" n="HIAT:w" s="T90">aktʼigən</ts>
                  <nts id="Seg_383" n="HIAT:ip">)</nts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_387" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_389" n="HIAT:w" s="T91">Gijen</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_392" n="HIAT:w" s="T92">dĭ</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_395" n="HIAT:w" s="T93">turaʔi</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_399" n="HIAT:w" s="T94">šobiam</ts>
                  <nts id="Seg_400" n="HIAT:ip">,</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_403" n="HIAT:w" s="T95">Kuražanə</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_407" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_409" n="HIAT:w" s="T96">Dĭn</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_412" n="HIAT:w" s="T97">šaːbiam</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_415" n="HIAT:w" s="T98">i</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_417" n="HIAT:ip">(</nts>
                  <ts e="T100" id="Seg_419" n="HIAT:w" s="T99">uʔ-</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_421" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_423" n="HIAT:w" s="T100">ertən</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_426" n="HIAT:w" s="T101">e-</ts>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_430" n="HIAT:w" s="T102">erte</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_433" n="HIAT:w" s="T103">uʔbdəbiam</ts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_437" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_439" n="HIAT:w" s="T104">Dĭgəttə</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_442" n="HIAT:w" s="T105">šobiam</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_445" n="HIAT:w" s="T106">maʔnʼi</ts>
                  <nts id="Seg_446" n="HIAT:ip">.</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_449" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_451" n="HIAT:w" s="T107">Bʼeʔ</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_454" n="HIAT:w" s="T108">časɨ</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_457" n="HIAT:w" s="T109">ibiʔi</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_461" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_463" n="HIAT:w" s="T110">Šobiam</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_467" n="HIAT:w" s="T111">a</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_470" n="HIAT:w" s="T112">tüžöj</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_473" n="HIAT:w" s="T113">ej</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_476" n="HIAT:w" s="T114">surdona</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_480" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_482" n="HIAT:w" s="T115">Büzəj</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_485" n="HIAT:w" s="T116">bar</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_488" n="HIAT:w" s="T117">kirgarlaʔbə</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_492" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_494" n="HIAT:w" s="T118">Süt</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_497" n="HIAT:w" s="T119">ej</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_500" n="HIAT:w" s="T120">kabaria</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_504" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_506" n="HIAT:w" s="T121">Šaːmbi</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_509" n="HIAT:w" s="T122">dĭ</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_512" n="HIAT:w" s="T123">ne</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_515" n="HIAT:w" s="T124">măna</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_519" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_521" n="HIAT:w" s="T125">Dĭgəttə</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_524" n="HIAT:w" s="T126">măn</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_527" n="HIAT:w" s="T127">toltanoʔ</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_530" n="HIAT:w" s="T128">dĭʔnə</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_533" n="HIAT:w" s="T129">mĭbiem</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_537" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_539" n="HIAT:w" s="T130">I</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_542" n="HIAT:w" s="T131">karbiam</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_545" n="HIAT:w" s="T132">dĭ</ts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_548" n="HIAT:ip">(</nts>
                  <ts e="T134" id="Seg_550" n="HIAT:w" s="T133">ej</ts>
                  <nts id="Seg_551" n="HIAT:ip">)</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_554" n="HIAT:w" s="T134">iʔ</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_557" n="HIAT:w" s="T135">kirgarlaʔbə</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_561" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_563" n="HIAT:w" s="T136">Dĭgəttə</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_566" n="HIAT:w" s="T137">nüdʼin</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_569" n="HIAT:w" s="T138">tüžöj</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_572" n="HIAT:w" s="T139">šobi</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_576" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_578" n="HIAT:w" s="T140">Kambiam</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_581" n="HIAT:w" s="T141">surdosʼtə</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_585" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_587" n="HIAT:w" s="T142">Kuliom:</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_590" n="HIAT:w" s="T143">süt</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_593" n="HIAT:w" s="T144">ugaːndə</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_596" n="HIAT:w" s="T145">iʔgö</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_600" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_602" n="HIAT:w" s="T146">Ĭmbidə</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_605" n="HIAT:w" s="T147">ej</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_608" n="HIAT:w" s="T148">nörbəbiöm</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_612" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_614" n="HIAT:w" s="T149">Dĭgəttə</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_617" n="HIAT:w" s="T150">erten</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_620" n="HIAT:w" s="T151">surdobiam</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_624" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_626" n="HIAT:w" s="T152">Dĭgəttə</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_629" n="HIAT:w" s="T153">bazoʔ</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_632" n="HIAT:w" s="T154">nüdʼin</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_635" n="HIAT:w" s="T155">surdobiam</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_639" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_641" n="HIAT:w" s="T156">Kuliom</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_644" n="HIAT:w" s="T157">bar</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_648" n="HIAT:w" s="T158">süt</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_651" n="HIAT:w" s="T159">amga</ts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_655" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_657" n="HIAT:w" s="T160">Dĭgəttə</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_660" n="HIAT:w" s="T161">mămbiam:</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_662" n="HIAT:ip">"</nts>
                  <nts id="Seg_663" n="HIAT:ip">(</nts>
                  <ts e="T163" id="Seg_665" n="HIAT:w" s="T162">Dĭ-</ts>
                  <nts id="Seg_666" n="HIAT:ip">)</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_669" n="HIAT:w" s="T163">Šiʔ</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_672" n="HIAT:w" s="T164">taldʼen</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_675" n="HIAT:w" s="T165">tüžöj</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_678" n="HIAT:w" s="T166">ej</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_681" n="HIAT:w" s="T167">surdobilaʔ</ts>
                  <nts id="Seg_682" n="HIAT:ip">?</nts>
                  <nts id="Seg_683" n="HIAT:ip">"</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_686" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_688" n="HIAT:w" s="T168">A</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_691" n="HIAT:w" s="T169">dĭ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_694" n="HIAT:w" s="T170">koʔbdo</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_697" n="HIAT:w" s="T171">măndə:</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_699" n="HIAT:ip">"</nts>
                  <ts e="T173" id="Seg_701" n="HIAT:w" s="T172">I</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_704" n="HIAT:w" s="T173">miʔ</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_707" n="HIAT:w" s="T174">ej</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_710" n="HIAT:w" s="T175">surdona</ts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_714" n="HIAT:w" s="T176">i</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_717" n="HIAT:w" s="T177">tăn</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_720" n="HIAT:w" s="T178">ej</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_723" n="HIAT:w" s="T179">surdona</ts>
                  <nts id="Seg_724" n="HIAT:ip">,</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_726" n="HIAT:ip">(</nts>
                  <ts e="T181" id="Seg_728" n="HIAT:w" s="T180">dĭ-</ts>
                  <nts id="Seg_729" n="HIAT:ip">)</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_732" n="HIAT:w" s="T181">dăreʔ</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_735" n="HIAT:w" s="T182">öʔlubibeʔ</ts>
                  <nts id="Seg_736" n="HIAT:ip">"</nts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_740" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_742" n="HIAT:w" s="T183">Kabarləj</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T184" id="Seg_745" n="sc" s="T0">
               <ts e="T1" id="Seg_747" n="e" s="T0">Miʔ </ts>
               <ts e="T2" id="Seg_749" n="e" s="T1">(koʔbdom) </ts>
               <ts e="T3" id="Seg_751" n="e" s="T2">šobi </ts>
               <ts e="T185" id="Seg_753" n="e" s="T3">Kazan </ts>
               <ts e="T4" id="Seg_755" n="e" s="T185">turagən. </ts>
               <ts e="T5" id="Seg_757" n="e" s="T4">Măndə: </ts>
               <ts e="T6" id="Seg_759" n="e" s="T5">Urgaja, </ts>
               <ts e="T7" id="Seg_761" n="e" s="T6">tăn </ts>
               <ts e="T8" id="Seg_763" n="e" s="T7">sazəngən </ts>
               <ts e="T9" id="Seg_765" n="e" s="T8">amnolaʔbəl. </ts>
               <ts e="T10" id="Seg_767" n="e" s="T9">A </ts>
               <ts e="T11" id="Seg_769" n="e" s="T10">kăde </ts>
               <ts e="T12" id="Seg_771" n="e" s="T11">(măr-) </ts>
               <ts e="T13" id="Seg_773" n="e" s="T12">măn </ts>
               <ts e="T14" id="Seg_775" n="e" s="T13">nereʔluʔpiem </ts>
               <ts e="T15" id="Seg_777" n="e" s="T14">bar. </ts>
               <ts e="T16" id="Seg_779" n="e" s="T15">Možet, </ts>
               <ts e="T17" id="Seg_781" n="e" s="T16">tenöbiam, </ts>
               <ts e="T18" id="Seg_783" n="e" s="T17">što </ts>
               <ts e="T19" id="Seg_785" n="e" s="T18">kudajdə </ts>
               <ts e="T20" id="Seg_787" n="e" s="T19">numan üzleʔbəm. </ts>
               <ts e="T21" id="Seg_789" n="e" s="T20">Dĭgəttə </ts>
               <ts e="T22" id="Seg_791" n="e" s="T21">sazəndə </ts>
               <ts e="T23" id="Seg_793" n="e" s="T22">amnolbiʔi. </ts>
               <ts e="T24" id="Seg_795" n="e" s="T23">Dĭ </ts>
               <ts e="T25" id="Seg_797" n="e" s="T24">măndə: </ts>
               <ts e="T26" id="Seg_799" n="e" s="T25">"Dʼok, </ts>
               <ts e="T27" id="Seg_801" n="e" s="T26">tăn </ts>
               <ts e="T28" id="Seg_803" n="e" s="T27">(dʼăbaktərbiziʔ-) </ts>
               <ts e="T29" id="Seg_805" n="e" s="T28">dʼăbaktərlaʔbəl </ts>
               <ts e="T30" id="Seg_807" n="e" s="T29">bostə </ts>
               <ts e="T31" id="Seg_809" n="e" s="T30">šĭkətsiʔ". </ts>
               <ts e="T32" id="Seg_811" n="e" s="T31">Dĭgəttə </ts>
               <ts e="T33" id="Seg_813" n="e" s="T32">măn </ts>
               <ts e="T34" id="Seg_815" n="e" s="T33">tenöbiem, </ts>
               <ts e="T36" id="Seg_817" n="e" s="T34">možet… </ts>
               <ts e="T37" id="Seg_819" n="e" s="T36">Kabarləj. </ts>
               <ts e="T38" id="Seg_821" n="e" s="T37">Dĭgəttə </ts>
               <ts e="T39" id="Seg_823" n="e" s="T38">măn </ts>
               <ts e="T40" id="Seg_825" n="e" s="T39">tenöbiem. </ts>
               <ts e="T41" id="Seg_827" n="e" s="T40">Dĭ </ts>
               <ts e="T42" id="Seg_829" n="e" s="T41">Jelʼa </ts>
               <ts e="T43" id="Seg_831" n="e" s="T42">pʼaŋdəbi </ts>
               <ts e="T44" id="Seg_833" n="e" s="T43">măna, </ts>
               <ts e="T45" id="Seg_835" n="e" s="T44">dĭbər </ts>
               <ts e="T46" id="Seg_837" n="e" s="T45">amnolbi </ts>
               <ts e="T47" id="Seg_839" n="e" s="T46">sazəndə. </ts>
               <ts e="T48" id="Seg_841" n="e" s="T47">Măn </ts>
               <ts e="T49" id="Seg_843" n="e" s="T48">mĭmbiem </ts>
               <ts e="T186" id="Seg_845" n="e" s="T49">Kazan </ts>
               <ts e="T50" id="Seg_847" n="e" s="T186">turanə. </ts>
               <ts e="T51" id="Seg_849" n="e" s="T50">Kudajdə </ts>
               <ts e="T52" id="Seg_851" n="e" s="T51">numan üzəsʼtə. </ts>
               <ts e="T53" id="Seg_853" n="e" s="T52">Dĭgəttə </ts>
               <ts e="T54" id="Seg_855" n="e" s="T53">šobiam </ts>
               <ts e="T55" id="Seg_857" n="e" s="T54">dĭbər. </ts>
               <ts e="T56" id="Seg_859" n="e" s="T55">Dĭn </ts>
               <ts e="T57" id="Seg_861" n="e" s="T56">ej </ts>
               <ts e="T58" id="Seg_863" n="e" s="T57">šobiʔi </ts>
               <ts e="T59" id="Seg_865" n="e" s="T58">kudajdə </ts>
               <ts e="T60" id="Seg_867" n="e" s="T59">(numan üzəsʼtə). </ts>
               <ts e="T61" id="Seg_869" n="e" s="T60">Dĭgəttə </ts>
               <ts e="T62" id="Seg_871" n="e" s="T61">unnʼazaŋdə </ts>
               <ts e="T63" id="Seg_873" n="e" s="T62">bar. </ts>
               <ts e="T64" id="Seg_875" n="e" s="T63">Kudajdə </ts>
               <ts e="T65" id="Seg_877" n="e" s="T64">numan üzəbibeʔ. </ts>
               <ts e="T66" id="Seg_879" n="e" s="T65">Dĭgəttə </ts>
               <ts e="T67" id="Seg_881" n="e" s="T66">măn </ts>
               <ts e="T68" id="Seg_883" n="e" s="T67">kambiam </ts>
               <ts e="T69" id="Seg_885" n="e" s="T68">maːʔnʼi. </ts>
               <ts e="T70" id="Seg_887" n="e" s="T69">Dĭzeŋ </ts>
               <ts e="T71" id="Seg_889" n="e" s="T70">ej </ts>
               <ts e="T72" id="Seg_891" n="e" s="T71">öʔlieʔi. </ts>
               <ts e="T73" id="Seg_893" n="e" s="T72">"Dʼok, </ts>
               <ts e="T74" id="Seg_895" n="e" s="T73">măndəm, </ts>
               <ts e="T75" id="Seg_897" n="e" s="T74">kallam". </ts>
               <ts e="T76" id="Seg_899" n="e" s="T75">Kambiam, </ts>
               <ts e="T77" id="Seg_901" n="e" s="T76">kambiam, </ts>
               <ts e="T78" id="Seg_903" n="e" s="T77">surno </ts>
               <ts e="T79" id="Seg_905" n="e" s="T78">bar </ts>
               <ts e="T80" id="Seg_907" n="e" s="T79">šonəga, </ts>
               <ts e="T81" id="Seg_909" n="e" s="T80">da </ts>
               <ts e="T82" id="Seg_911" n="e" s="T81">beržə. </ts>
               <ts e="T83" id="Seg_913" n="e" s="T82">Dĭgəttə </ts>
               <ts e="T84" id="Seg_915" n="e" s="T83">šobiam, </ts>
               <ts e="T85" id="Seg_917" n="e" s="T84">šobiam. </ts>
               <ts e="T86" id="Seg_919" n="e" s="T85">Mašina </ts>
               <ts e="T87" id="Seg_921" n="e" s="T86">nuga. </ts>
               <ts e="T88" id="Seg_923" n="e" s="T87">Măn </ts>
               <ts e="T89" id="Seg_925" n="e" s="T88">šobiam, </ts>
               <ts e="T90" id="Seg_927" n="e" s="T89">šobiam, </ts>
               <ts e="T91" id="Seg_929" n="e" s="T90">(aktʼigən). </ts>
               <ts e="T92" id="Seg_931" n="e" s="T91">Gijen </ts>
               <ts e="T93" id="Seg_933" n="e" s="T92">dĭ </ts>
               <ts e="T94" id="Seg_935" n="e" s="T93">turaʔi, </ts>
               <ts e="T95" id="Seg_937" n="e" s="T94">šobiam, </ts>
               <ts e="T96" id="Seg_939" n="e" s="T95">Kuražanə. </ts>
               <ts e="T97" id="Seg_941" n="e" s="T96">Dĭn </ts>
               <ts e="T98" id="Seg_943" n="e" s="T97">šaːbiam </ts>
               <ts e="T99" id="Seg_945" n="e" s="T98">i </ts>
               <ts e="T100" id="Seg_947" n="e" s="T99">(uʔ- </ts>
               <ts e="T101" id="Seg_949" n="e" s="T100">(ertən </ts>
               <ts e="T102" id="Seg_951" n="e" s="T101">e-) </ts>
               <ts e="T103" id="Seg_953" n="e" s="T102">erte </ts>
               <ts e="T104" id="Seg_955" n="e" s="T103">uʔbdəbiam. </ts>
               <ts e="T105" id="Seg_957" n="e" s="T104">Dĭgəttə </ts>
               <ts e="T106" id="Seg_959" n="e" s="T105">šobiam </ts>
               <ts e="T107" id="Seg_961" n="e" s="T106">maʔnʼi. </ts>
               <ts e="T108" id="Seg_963" n="e" s="T107">Bʼeʔ </ts>
               <ts e="T109" id="Seg_965" n="e" s="T108">časɨ </ts>
               <ts e="T110" id="Seg_967" n="e" s="T109">ibiʔi. </ts>
               <ts e="T111" id="Seg_969" n="e" s="T110">Šobiam, </ts>
               <ts e="T112" id="Seg_971" n="e" s="T111">a </ts>
               <ts e="T113" id="Seg_973" n="e" s="T112">tüžöj </ts>
               <ts e="T114" id="Seg_975" n="e" s="T113">ej </ts>
               <ts e="T115" id="Seg_977" n="e" s="T114">surdona. </ts>
               <ts e="T116" id="Seg_979" n="e" s="T115">Büzəj </ts>
               <ts e="T117" id="Seg_981" n="e" s="T116">bar </ts>
               <ts e="T118" id="Seg_983" n="e" s="T117">kirgarlaʔbə. </ts>
               <ts e="T119" id="Seg_985" n="e" s="T118">Süt </ts>
               <ts e="T120" id="Seg_987" n="e" s="T119">ej </ts>
               <ts e="T121" id="Seg_989" n="e" s="T120">kabaria. </ts>
               <ts e="T122" id="Seg_991" n="e" s="T121">Šaːmbi </ts>
               <ts e="T123" id="Seg_993" n="e" s="T122">dĭ </ts>
               <ts e="T124" id="Seg_995" n="e" s="T123">ne </ts>
               <ts e="T125" id="Seg_997" n="e" s="T124">măna. </ts>
               <ts e="T126" id="Seg_999" n="e" s="T125">Dĭgəttə </ts>
               <ts e="T127" id="Seg_1001" n="e" s="T126">măn </ts>
               <ts e="T128" id="Seg_1003" n="e" s="T127">toltanoʔ </ts>
               <ts e="T129" id="Seg_1005" n="e" s="T128">dĭʔnə </ts>
               <ts e="T130" id="Seg_1007" n="e" s="T129">mĭbiem. </ts>
               <ts e="T131" id="Seg_1009" n="e" s="T130">I </ts>
               <ts e="T132" id="Seg_1011" n="e" s="T131">karbiam </ts>
               <ts e="T133" id="Seg_1013" n="e" s="T132">dĭ, </ts>
               <ts e="T134" id="Seg_1015" n="e" s="T133">(ej) </ts>
               <ts e="T135" id="Seg_1017" n="e" s="T134">iʔ </ts>
               <ts e="T136" id="Seg_1019" n="e" s="T135">kirgarlaʔbə. </ts>
               <ts e="T137" id="Seg_1021" n="e" s="T136">Dĭgəttə </ts>
               <ts e="T138" id="Seg_1023" n="e" s="T137">nüdʼin </ts>
               <ts e="T139" id="Seg_1025" n="e" s="T138">tüžöj </ts>
               <ts e="T140" id="Seg_1027" n="e" s="T139">šobi. </ts>
               <ts e="T141" id="Seg_1029" n="e" s="T140">Kambiam </ts>
               <ts e="T142" id="Seg_1031" n="e" s="T141">surdosʼtə. </ts>
               <ts e="T143" id="Seg_1033" n="e" s="T142">Kuliom: </ts>
               <ts e="T144" id="Seg_1035" n="e" s="T143">süt </ts>
               <ts e="T145" id="Seg_1037" n="e" s="T144">ugaːndə </ts>
               <ts e="T146" id="Seg_1039" n="e" s="T145">iʔgö. </ts>
               <ts e="T147" id="Seg_1041" n="e" s="T146">Ĭmbidə </ts>
               <ts e="T148" id="Seg_1043" n="e" s="T147">ej </ts>
               <ts e="T149" id="Seg_1045" n="e" s="T148">nörbəbiöm. </ts>
               <ts e="T150" id="Seg_1047" n="e" s="T149">Dĭgəttə </ts>
               <ts e="T151" id="Seg_1049" n="e" s="T150">erten </ts>
               <ts e="T152" id="Seg_1051" n="e" s="T151">surdobiam. </ts>
               <ts e="T153" id="Seg_1053" n="e" s="T152">Dĭgəttə </ts>
               <ts e="T154" id="Seg_1055" n="e" s="T153">bazoʔ </ts>
               <ts e="T155" id="Seg_1057" n="e" s="T154">nüdʼin </ts>
               <ts e="T156" id="Seg_1059" n="e" s="T155">surdobiam. </ts>
               <ts e="T157" id="Seg_1061" n="e" s="T156">Kuliom </ts>
               <ts e="T158" id="Seg_1063" n="e" s="T157">bar, </ts>
               <ts e="T159" id="Seg_1065" n="e" s="T158">süt </ts>
               <ts e="T160" id="Seg_1067" n="e" s="T159">amga. </ts>
               <ts e="T161" id="Seg_1069" n="e" s="T160">Dĭgəttə </ts>
               <ts e="T162" id="Seg_1071" n="e" s="T161">mămbiam: </ts>
               <ts e="T163" id="Seg_1073" n="e" s="T162">"(Dĭ-) </ts>
               <ts e="T164" id="Seg_1075" n="e" s="T163">Šiʔ </ts>
               <ts e="T165" id="Seg_1077" n="e" s="T164">taldʼen </ts>
               <ts e="T166" id="Seg_1079" n="e" s="T165">tüžöj </ts>
               <ts e="T167" id="Seg_1081" n="e" s="T166">ej </ts>
               <ts e="T168" id="Seg_1083" n="e" s="T167">surdobilaʔ?" </ts>
               <ts e="T169" id="Seg_1085" n="e" s="T168">A </ts>
               <ts e="T170" id="Seg_1087" n="e" s="T169">dĭ </ts>
               <ts e="T171" id="Seg_1089" n="e" s="T170">koʔbdo </ts>
               <ts e="T172" id="Seg_1091" n="e" s="T171">măndə: </ts>
               <ts e="T173" id="Seg_1093" n="e" s="T172">"I </ts>
               <ts e="T174" id="Seg_1095" n="e" s="T173">miʔ </ts>
               <ts e="T175" id="Seg_1097" n="e" s="T174">ej </ts>
               <ts e="T176" id="Seg_1099" n="e" s="T175">surdona, </ts>
               <ts e="T177" id="Seg_1101" n="e" s="T176">i </ts>
               <ts e="T178" id="Seg_1103" n="e" s="T177">tăn </ts>
               <ts e="T179" id="Seg_1105" n="e" s="T178">ej </ts>
               <ts e="T180" id="Seg_1107" n="e" s="T179">surdona, </ts>
               <ts e="T181" id="Seg_1109" n="e" s="T180">(dĭ-) </ts>
               <ts e="T182" id="Seg_1111" n="e" s="T181">dăreʔ </ts>
               <ts e="T183" id="Seg_1113" n="e" s="T182">öʔlubibeʔ". </ts>
               <ts e="T184" id="Seg_1115" n="e" s="T183">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1116" s="T0">PKZ_196X_TripToAginskoe2_nar.001 (002)</ta>
            <ta e="T9" id="Seg_1117" s="T4">PKZ_196X_TripToAginskoe2_nar.002 (003)</ta>
            <ta e="T15" id="Seg_1118" s="T9">PKZ_196X_TripToAginskoe2_nar.003 (004)</ta>
            <ta e="T20" id="Seg_1119" s="T15">PKZ_196X_TripToAginskoe2_nar.004 (005)</ta>
            <ta e="T23" id="Seg_1120" s="T20">PKZ_196X_TripToAginskoe2_nar.005 (006)</ta>
            <ta e="T31" id="Seg_1121" s="T23">PKZ_196X_TripToAginskoe2_nar.006 (007)</ta>
            <ta e="T36" id="Seg_1122" s="T31">PKZ_196X_TripToAginskoe2_nar.007 (008)</ta>
            <ta e="T37" id="Seg_1123" s="T36">PKZ_196X_TripToAginskoe2_nar.008 (009)</ta>
            <ta e="T40" id="Seg_1124" s="T37">PKZ_196X_TripToAginskoe2_nar.009 (010)</ta>
            <ta e="T47" id="Seg_1125" s="T40">PKZ_196X_TripToAginskoe2_nar.010 (011)</ta>
            <ta e="T50" id="Seg_1126" s="T47">PKZ_196X_TripToAginskoe2_nar.011 (012)</ta>
            <ta e="T52" id="Seg_1127" s="T50">PKZ_196X_TripToAginskoe2_nar.012 (013)</ta>
            <ta e="T55" id="Seg_1128" s="T52">PKZ_196X_TripToAginskoe2_nar.013 (014)</ta>
            <ta e="T60" id="Seg_1129" s="T55">PKZ_196X_TripToAginskoe2_nar.014 (015)</ta>
            <ta e="T63" id="Seg_1130" s="T60">PKZ_196X_TripToAginskoe2_nar.015 (016)</ta>
            <ta e="T65" id="Seg_1131" s="T63">PKZ_196X_TripToAginskoe2_nar.016 (017)</ta>
            <ta e="T69" id="Seg_1132" s="T65">PKZ_196X_TripToAginskoe2_nar.017 (018)</ta>
            <ta e="T72" id="Seg_1133" s="T69">PKZ_196X_TripToAginskoe2_nar.018 (019)</ta>
            <ta e="T75" id="Seg_1134" s="T72">PKZ_196X_TripToAginskoe2_nar.019 (020)</ta>
            <ta e="T82" id="Seg_1135" s="T75">PKZ_196X_TripToAginskoe2_nar.020 (021)</ta>
            <ta e="T85" id="Seg_1136" s="T82">PKZ_196X_TripToAginskoe2_nar.021 (022)</ta>
            <ta e="T87" id="Seg_1137" s="T85">PKZ_196X_TripToAginskoe2_nar.022 (023)</ta>
            <ta e="T91" id="Seg_1138" s="T87">PKZ_196X_TripToAginskoe2_nar.023 (024)</ta>
            <ta e="T96" id="Seg_1139" s="T91">PKZ_196X_TripToAginskoe2_nar.024 (025)</ta>
            <ta e="T104" id="Seg_1140" s="T96">PKZ_196X_TripToAginskoe2_nar.025 (026)</ta>
            <ta e="T107" id="Seg_1141" s="T104">PKZ_196X_TripToAginskoe2_nar.026 (027)</ta>
            <ta e="T110" id="Seg_1142" s="T107">PKZ_196X_TripToAginskoe2_nar.027 (028)</ta>
            <ta e="T115" id="Seg_1143" s="T110">PKZ_196X_TripToAginskoe2_nar.028 (029)</ta>
            <ta e="T118" id="Seg_1144" s="T115">PKZ_196X_TripToAginskoe2_nar.029 (030)</ta>
            <ta e="T121" id="Seg_1145" s="T118">PKZ_196X_TripToAginskoe2_nar.030 (031)</ta>
            <ta e="T125" id="Seg_1146" s="T121">PKZ_196X_TripToAginskoe2_nar.031 (032)</ta>
            <ta e="T130" id="Seg_1147" s="T125">PKZ_196X_TripToAginskoe2_nar.032 (033)</ta>
            <ta e="T136" id="Seg_1148" s="T130">PKZ_196X_TripToAginskoe2_nar.033 (034)</ta>
            <ta e="T140" id="Seg_1149" s="T136">PKZ_196X_TripToAginskoe2_nar.034 (035)</ta>
            <ta e="T142" id="Seg_1150" s="T140">PKZ_196X_TripToAginskoe2_nar.035 (036)</ta>
            <ta e="T146" id="Seg_1151" s="T142">PKZ_196X_TripToAginskoe2_nar.036 (037)</ta>
            <ta e="T149" id="Seg_1152" s="T146">PKZ_196X_TripToAginskoe2_nar.037 (038)</ta>
            <ta e="T152" id="Seg_1153" s="T149">PKZ_196X_TripToAginskoe2_nar.038 (039)</ta>
            <ta e="T156" id="Seg_1154" s="T152">PKZ_196X_TripToAginskoe2_nar.039 (040)</ta>
            <ta e="T160" id="Seg_1155" s="T156">PKZ_196X_TripToAginskoe2_nar.040 (041)</ta>
            <ta e="T168" id="Seg_1156" s="T160">PKZ_196X_TripToAginskoe2_nar.041 (042)</ta>
            <ta e="T183" id="Seg_1157" s="T168">PKZ_196X_TripToAginskoe2_nar.042 (043) </ta>
            <ta e="T184" id="Seg_1158" s="T183">PKZ_196X_TripToAginskoe2_nar.043 (045)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1159" s="T0">Miʔ (koʔbdom) šobi Kazan turagən. </ta>
            <ta e="T9" id="Seg_1160" s="T4">Măndə:"Urgaja, tăn sazəngən amnolaʔbəl. </ta>
            <ta e="T15" id="Seg_1161" s="T9">A kăde (măr-) măn nereʔluʔpiem bar. </ta>
            <ta e="T20" id="Seg_1162" s="T15">Možet, tenöbiam, što kudajdə numan üzleʔbəm. </ta>
            <ta e="T23" id="Seg_1163" s="T20">Dĭgəttə sazəndə amnolbiʔi. </ta>
            <ta e="T31" id="Seg_1164" s="T23">Dĭ măndə: "Dʼok, tăn (dʼăbaktərbiziʔ-) dʼăbaktərlaʔbəl bostə šĭkətsiʔ". </ta>
            <ta e="T36" id="Seg_1165" s="T31">Dĭgəttə măn tenöbiam, možet … </ta>
            <ta e="T37" id="Seg_1166" s="T36">Kabarləj. </ta>
            <ta e="T40" id="Seg_1167" s="T37">Dĭgəttə măn tenöbiam. </ta>
            <ta e="T47" id="Seg_1168" s="T40">Dĭ Jelʼa pʼaŋdəbi măna, dĭbər amnolbi sazəndə. </ta>
            <ta e="T50" id="Seg_1169" s="T47">Măn mĭmbiem Kazan turanə. </ta>
            <ta e="T52" id="Seg_1170" s="T50">Kudajdə numan üzəsʼtə. </ta>
            <ta e="T55" id="Seg_1171" s="T52">Dĭgəttə šobiam dĭbər. </ta>
            <ta e="T60" id="Seg_1172" s="T55">Dĭn ej šobiʔi kudajdə (numan üzəsʼtə). </ta>
            <ta e="T63" id="Seg_1173" s="T60">Dĭgəttə unnʼazaŋdə bar. </ta>
            <ta e="T65" id="Seg_1174" s="T63">Kudajdə numan üzəbibeʔ. </ta>
            <ta e="T69" id="Seg_1175" s="T65">Dĭgəttə măn kambiam maːʔnʼi. </ta>
            <ta e="T72" id="Seg_1176" s="T69">Dĭzeŋ ej öʔlieʔi. </ta>
            <ta e="T75" id="Seg_1177" s="T72">"Dʼok, măndəm, kallam". </ta>
            <ta e="T82" id="Seg_1178" s="T75">Kambiam, kambiam, surno bar šonəga, da beržə. </ta>
            <ta e="T85" id="Seg_1179" s="T82">Dĭgəttə šobiam, šobiam. </ta>
            <ta e="T87" id="Seg_1180" s="T85">Mašina nuga. </ta>
            <ta e="T91" id="Seg_1181" s="T87">Măn šobiam, šobiam, (aktʼigən). </ta>
            <ta e="T96" id="Seg_1182" s="T91">Gijen dĭ turaʔi, šobiam, Kuražanə. </ta>
            <ta e="T104" id="Seg_1183" s="T96">Dĭn šaːbiam i (uʔ- (ertən e-) erte uʔbdəbiam. </ta>
            <ta e="T107" id="Seg_1184" s="T104">Dĭgəttə šobiam maʔnʼi. </ta>
            <ta e="T110" id="Seg_1185" s="T107">Bʼeʔ časɨ ibiʔi. </ta>
            <ta e="T115" id="Seg_1186" s="T110">Šobiam, a tüžöj ej surdona. </ta>
            <ta e="T118" id="Seg_1187" s="T115">Büzəj bar kirgarlaʔbə. </ta>
            <ta e="T121" id="Seg_1188" s="T118">Süt ej kabaria. </ta>
            <ta e="T125" id="Seg_1189" s="T121">Šaːmbi dĭ ne măna. </ta>
            <ta e="T130" id="Seg_1190" s="T125">Dĭgəttə măn toltanoʔ dĭʔnə mĭbiem. </ta>
            <ta e="T136" id="Seg_1191" s="T130">I karbiam dĭ, (ej) iʔ kirgarlaʔbə. </ta>
            <ta e="T140" id="Seg_1192" s="T136">Dĭgəttə nüdʼin tüžöj šobi. </ta>
            <ta e="T142" id="Seg_1193" s="T140">Kambiam surdosʼtə. </ta>
            <ta e="T146" id="Seg_1194" s="T142">Kuliom: süt ugaːndə iʔgö. </ta>
            <ta e="T149" id="Seg_1195" s="T146">Ĭmbidə ej nörbəbiöm. </ta>
            <ta e="T152" id="Seg_1196" s="T149">Dĭgəttə erten surdobiam. </ta>
            <ta e="T156" id="Seg_1197" s="T152">Dĭgəttə bazoʔ nüdʼin surdobiam. </ta>
            <ta e="T160" id="Seg_1198" s="T156">Kuliom bar, süt amgaʔ. </ta>
            <ta e="T168" id="Seg_1199" s="T160">Dĭgəttə mămbiam: "(Dĭ-) Šiʔ taldʼen tüžöj ej surdobilaʔ?" </ta>
            <ta e="T183" id="Seg_1200" s="T168">A dĭ koʔbdo măndə: "I miʔ ej surdona, i tăn ej surdona, (dĭ-) dăreʔ öʔlubibeʔ". </ta>
            <ta e="T184" id="Seg_1201" s="T183">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1202" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_1203" s="T1">koʔbdo-m</ta>
            <ta e="T3" id="Seg_1204" s="T2">šo-bi</ta>
            <ta e="T185" id="Seg_1205" s="T3">Kazan</ta>
            <ta e="T4" id="Seg_1206" s="T185">tura-gən</ta>
            <ta e="T5" id="Seg_1207" s="T4">măn-də</ta>
            <ta e="T6" id="Seg_1208" s="T5">urgaja</ta>
            <ta e="T7" id="Seg_1209" s="T6">tăn</ta>
            <ta e="T8" id="Seg_1210" s="T7">sazən-gən</ta>
            <ta e="T9" id="Seg_1211" s="T8">amno-laʔbə-l</ta>
            <ta e="T10" id="Seg_1212" s="T9">a</ta>
            <ta e="T11" id="Seg_1213" s="T10">kăde</ta>
            <ta e="T13" id="Seg_1214" s="T12">măn</ta>
            <ta e="T14" id="Seg_1215" s="T13">nereʔ-luʔ-pie-m</ta>
            <ta e="T15" id="Seg_1216" s="T14">bar</ta>
            <ta e="T16" id="Seg_1217" s="T15">možet</ta>
            <ta e="T17" id="Seg_1218" s="T16">tenö-bia-m</ta>
            <ta e="T18" id="Seg_1219" s="T17">što</ta>
            <ta e="T19" id="Seg_1220" s="T18">kudaj-də</ta>
            <ta e="T20" id="Seg_1221" s="T19">numan üz-leʔbə-m</ta>
            <ta e="T21" id="Seg_1222" s="T20">dĭgəttə</ta>
            <ta e="T22" id="Seg_1223" s="T21">sazən-də</ta>
            <ta e="T23" id="Seg_1224" s="T22">amnol-bi-ʔi</ta>
            <ta e="T24" id="Seg_1225" s="T23">dĭ</ta>
            <ta e="T25" id="Seg_1226" s="T24">măn-də</ta>
            <ta e="T26" id="Seg_1227" s="T25">dʼok</ta>
            <ta e="T27" id="Seg_1228" s="T26">tăn</ta>
            <ta e="T29" id="Seg_1229" s="T28">dʼăbaktər-laʔbə-l</ta>
            <ta e="T30" id="Seg_1230" s="T29">bos-tə</ta>
            <ta e="T31" id="Seg_1231" s="T30">šĭkə-t-siʔ</ta>
            <ta e="T32" id="Seg_1232" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_1233" s="T32">măn</ta>
            <ta e="T34" id="Seg_1234" s="T33">tenö-bie-m</ta>
            <ta e="T36" id="Seg_1235" s="T34">možet</ta>
            <ta e="T37" id="Seg_1236" s="T36">kabarləj</ta>
            <ta e="T38" id="Seg_1237" s="T37">dĭgəttə</ta>
            <ta e="T39" id="Seg_1238" s="T38">măn</ta>
            <ta e="T40" id="Seg_1239" s="T39">tenö-bie-m</ta>
            <ta e="T41" id="Seg_1240" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_1241" s="T41">Jelʼa</ta>
            <ta e="T43" id="Seg_1242" s="T42">pʼaŋdə-bi</ta>
            <ta e="T44" id="Seg_1243" s="T43">măna</ta>
            <ta e="T45" id="Seg_1244" s="T44">dĭbər</ta>
            <ta e="T46" id="Seg_1245" s="T45">amnol-bi</ta>
            <ta e="T47" id="Seg_1246" s="T46">sazən-də</ta>
            <ta e="T48" id="Seg_1247" s="T47">măn</ta>
            <ta e="T49" id="Seg_1248" s="T48">mĭm-bie-m</ta>
            <ta e="T186" id="Seg_1249" s="T49">Kazan</ta>
            <ta e="T50" id="Seg_1250" s="T186">tura-nə</ta>
            <ta e="T51" id="Seg_1251" s="T50">kudaj-də</ta>
            <ta e="T52" id="Seg_1252" s="T51">numan üzə-sʼtə</ta>
            <ta e="T53" id="Seg_1253" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_1254" s="T53">šo-bia-m</ta>
            <ta e="T55" id="Seg_1255" s="T54">dĭbər</ta>
            <ta e="T56" id="Seg_1256" s="T55">dĭn</ta>
            <ta e="T57" id="Seg_1257" s="T56">ej</ta>
            <ta e="T58" id="Seg_1258" s="T57">šo-bi-ʔi</ta>
            <ta e="T59" id="Seg_1259" s="T58">kudaj-də</ta>
            <ta e="T60" id="Seg_1260" s="T59">numan üzə-sʼtə</ta>
            <ta e="T61" id="Seg_1261" s="T60">dĭgəttə</ta>
            <ta e="T62" id="Seg_1262" s="T61">unnʼa-zaŋ-də</ta>
            <ta e="T63" id="Seg_1263" s="T62">bar</ta>
            <ta e="T64" id="Seg_1264" s="T63">kudaj-də</ta>
            <ta e="T65" id="Seg_1265" s="T64">numan üzə-bi-beʔ</ta>
            <ta e="T66" id="Seg_1266" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_1267" s="T66">măn</ta>
            <ta e="T68" id="Seg_1268" s="T67">kam-bia-m</ta>
            <ta e="T69" id="Seg_1269" s="T68">maːʔ-nʼi</ta>
            <ta e="T70" id="Seg_1270" s="T69">dĭ-zeŋ</ta>
            <ta e="T71" id="Seg_1271" s="T70">ej</ta>
            <ta e="T72" id="Seg_1272" s="T71">öʔ-lie-ʔi</ta>
            <ta e="T73" id="Seg_1273" s="T72">dʼok</ta>
            <ta e="T74" id="Seg_1274" s="T73">măn-də-m</ta>
            <ta e="T75" id="Seg_1275" s="T74">kal-la-m</ta>
            <ta e="T76" id="Seg_1276" s="T75">kam-bia-m</ta>
            <ta e="T77" id="Seg_1277" s="T76">kam-bia-m</ta>
            <ta e="T78" id="Seg_1278" s="T77">surno</ta>
            <ta e="T79" id="Seg_1279" s="T78">bar</ta>
            <ta e="T80" id="Seg_1280" s="T79">šonə-ga</ta>
            <ta e="T81" id="Seg_1281" s="T80">da</ta>
            <ta e="T82" id="Seg_1282" s="T81">beržə</ta>
            <ta e="T83" id="Seg_1283" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1284" s="T83">šo-bia-m</ta>
            <ta e="T85" id="Seg_1285" s="T84">šo-bia-m</ta>
            <ta e="T86" id="Seg_1286" s="T85">mašina</ta>
            <ta e="T87" id="Seg_1287" s="T86">nu-ga</ta>
            <ta e="T88" id="Seg_1288" s="T87">măn</ta>
            <ta e="T89" id="Seg_1289" s="T88">šo-bia-m</ta>
            <ta e="T90" id="Seg_1290" s="T89">šo-bia-m</ta>
            <ta e="T91" id="Seg_1291" s="T90">aktʼi-gən</ta>
            <ta e="T92" id="Seg_1292" s="T91">gijen</ta>
            <ta e="T93" id="Seg_1293" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_1294" s="T93">tura-ʔi</ta>
            <ta e="T95" id="Seg_1295" s="T94">šo-bia-m</ta>
            <ta e="T96" id="Seg_1296" s="T95">Kuraža-nə</ta>
            <ta e="T97" id="Seg_1297" s="T96">dĭn</ta>
            <ta e="T98" id="Seg_1298" s="T97">šaː-bia-m</ta>
            <ta e="T99" id="Seg_1299" s="T98">i</ta>
            <ta e="T101" id="Seg_1300" s="T100">ertə-n</ta>
            <ta e="T103" id="Seg_1301" s="T102">erte</ta>
            <ta e="T104" id="Seg_1302" s="T103">uʔbdə-bia-m</ta>
            <ta e="T105" id="Seg_1303" s="T104">dĭgəttə</ta>
            <ta e="T106" id="Seg_1304" s="T105">šo-bia-m</ta>
            <ta e="T107" id="Seg_1305" s="T106">maʔ-nʼi</ta>
            <ta e="T108" id="Seg_1306" s="T107">bʼeʔ</ta>
            <ta e="T109" id="Seg_1307" s="T108">časɨ</ta>
            <ta e="T110" id="Seg_1308" s="T109">i-bi-ʔi</ta>
            <ta e="T111" id="Seg_1309" s="T110">šo-bia-m</ta>
            <ta e="T112" id="Seg_1310" s="T111">a</ta>
            <ta e="T113" id="Seg_1311" s="T112">tüžöj</ta>
            <ta e="T114" id="Seg_1312" s="T113">ej</ta>
            <ta e="T115" id="Seg_1313" s="T114">surdo-na</ta>
            <ta e="T116" id="Seg_1314" s="T115">büzəj</ta>
            <ta e="T117" id="Seg_1315" s="T116">bar</ta>
            <ta e="T118" id="Seg_1316" s="T117">kirgar-laʔbə</ta>
            <ta e="T119" id="Seg_1317" s="T118">süt</ta>
            <ta e="T120" id="Seg_1318" s="T119">ej</ta>
            <ta e="T121" id="Seg_1319" s="T120">kabar-ia</ta>
            <ta e="T122" id="Seg_1320" s="T121">šaːm-bi</ta>
            <ta e="T123" id="Seg_1321" s="T122">dĭ</ta>
            <ta e="T124" id="Seg_1322" s="T123">ne</ta>
            <ta e="T125" id="Seg_1323" s="T124">măna</ta>
            <ta e="T126" id="Seg_1324" s="T125">dĭgəttə</ta>
            <ta e="T127" id="Seg_1325" s="T126">măn</ta>
            <ta e="T128" id="Seg_1326" s="T127">toltanoʔ</ta>
            <ta e="T129" id="Seg_1327" s="T128">dĭʔ-nə</ta>
            <ta e="T130" id="Seg_1328" s="T129">mĭ-bie-m</ta>
            <ta e="T131" id="Seg_1329" s="T130">i</ta>
            <ta e="T132" id="Seg_1330" s="T131">kar-bia-m</ta>
            <ta e="T133" id="Seg_1331" s="T132">dĭ</ta>
            <ta e="T134" id="Seg_1332" s="T133">ej</ta>
            <ta e="T135" id="Seg_1333" s="T134">i-ʔ</ta>
            <ta e="T136" id="Seg_1334" s="T135">kirgar-laʔbə</ta>
            <ta e="T137" id="Seg_1335" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_1336" s="T137">nüdʼi-n</ta>
            <ta e="T139" id="Seg_1337" s="T138">tüžöj</ta>
            <ta e="T140" id="Seg_1338" s="T139">šo-bi</ta>
            <ta e="T141" id="Seg_1339" s="T140">kam-bia-m</ta>
            <ta e="T142" id="Seg_1340" s="T141">surdo-sʼtə</ta>
            <ta e="T143" id="Seg_1341" s="T142">ku-lio-m</ta>
            <ta e="T144" id="Seg_1342" s="T143">süt</ta>
            <ta e="T145" id="Seg_1343" s="T144">ugaːndə</ta>
            <ta e="T146" id="Seg_1344" s="T145">iʔgö</ta>
            <ta e="T147" id="Seg_1345" s="T146">ĭmbi=də</ta>
            <ta e="T148" id="Seg_1346" s="T147">ej</ta>
            <ta e="T149" id="Seg_1347" s="T148">nörbə-biö-m</ta>
            <ta e="T150" id="Seg_1348" s="T149">dĭgəttə</ta>
            <ta e="T151" id="Seg_1349" s="T150">erte-n</ta>
            <ta e="T152" id="Seg_1350" s="T151">surdo-bia-m</ta>
            <ta e="T153" id="Seg_1351" s="T152">dĭgəttə</ta>
            <ta e="T154" id="Seg_1352" s="T153">bazoʔ</ta>
            <ta e="T155" id="Seg_1353" s="T154">nüdʼi-n</ta>
            <ta e="T156" id="Seg_1354" s="T155">surdo-bia-m</ta>
            <ta e="T157" id="Seg_1355" s="T156">ku-lio-m</ta>
            <ta e="T158" id="Seg_1356" s="T157">bar</ta>
            <ta e="T159" id="Seg_1357" s="T158">süt</ta>
            <ta e="T160" id="Seg_1358" s="T159">amga</ta>
            <ta e="T161" id="Seg_1359" s="T160">dĭgəttə</ta>
            <ta e="T162" id="Seg_1360" s="T161">măm-bia-m</ta>
            <ta e="T164" id="Seg_1361" s="T163">šiʔ</ta>
            <ta e="T165" id="Seg_1362" s="T164">taldʼen</ta>
            <ta e="T166" id="Seg_1363" s="T165">tüžöj</ta>
            <ta e="T167" id="Seg_1364" s="T166">ej</ta>
            <ta e="T168" id="Seg_1365" s="T167">surdo-bi-laʔ</ta>
            <ta e="T169" id="Seg_1366" s="T168">a</ta>
            <ta e="T170" id="Seg_1367" s="T169">dĭ</ta>
            <ta e="T171" id="Seg_1368" s="T170">koʔbdo</ta>
            <ta e="T172" id="Seg_1369" s="T171">măn-də</ta>
            <ta e="T173" id="Seg_1370" s="T172">i</ta>
            <ta e="T174" id="Seg_1371" s="T173">miʔ</ta>
            <ta e="T175" id="Seg_1372" s="T174">ej</ta>
            <ta e="T176" id="Seg_1373" s="T175">surdo-na</ta>
            <ta e="T177" id="Seg_1374" s="T176">i</ta>
            <ta e="T178" id="Seg_1375" s="T177">tăn</ta>
            <ta e="T179" id="Seg_1376" s="T178">ej</ta>
            <ta e="T180" id="Seg_1377" s="T179">surdo-na</ta>
            <ta e="T182" id="Seg_1378" s="T181">dăreʔ</ta>
            <ta e="T183" id="Seg_1379" s="T182">öʔlu-bi-beʔ</ta>
            <ta e="T184" id="Seg_1380" s="T183">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1381" s="T0">miʔ</ta>
            <ta e="T2" id="Seg_1382" s="T1">koʔbdo-m</ta>
            <ta e="T3" id="Seg_1383" s="T2">šo-bi</ta>
            <ta e="T185" id="Seg_1384" s="T3">Kazan</ta>
            <ta e="T4" id="Seg_1385" s="T185">tura-Kən</ta>
            <ta e="T5" id="Seg_1386" s="T4">măn-ntə</ta>
            <ta e="T6" id="Seg_1387" s="T5">urgaja</ta>
            <ta e="T7" id="Seg_1388" s="T6">tăn</ta>
            <ta e="T8" id="Seg_1389" s="T7">sazən-Kən</ta>
            <ta e="T9" id="Seg_1390" s="T8">amno-laʔbə-l</ta>
            <ta e="T10" id="Seg_1391" s="T9">a</ta>
            <ta e="T11" id="Seg_1392" s="T10">kădaʔ</ta>
            <ta e="T13" id="Seg_1393" s="T12">măn</ta>
            <ta e="T14" id="Seg_1394" s="T13">nereʔ-luʔbdə-bi-m</ta>
            <ta e="T15" id="Seg_1395" s="T14">bar</ta>
            <ta e="T16" id="Seg_1396" s="T15">možet</ta>
            <ta e="T17" id="Seg_1397" s="T16">tenö-bi-m</ta>
            <ta e="T18" id="Seg_1398" s="T17">što</ta>
            <ta e="T19" id="Seg_1399" s="T18">kudaj-Tə</ta>
            <ta e="T20" id="Seg_1400" s="T19">numan üzə-laʔbə-m</ta>
            <ta e="T21" id="Seg_1401" s="T20">dĭgəttə</ta>
            <ta e="T22" id="Seg_1402" s="T21">sazən-Tə</ta>
            <ta e="T23" id="Seg_1403" s="T22">amnol-bi-jəʔ</ta>
            <ta e="T24" id="Seg_1404" s="T23">dĭ</ta>
            <ta e="T25" id="Seg_1405" s="T24">măn-ntə</ta>
            <ta e="T26" id="Seg_1406" s="T25">dʼok</ta>
            <ta e="T27" id="Seg_1407" s="T26">tăn</ta>
            <ta e="T29" id="Seg_1408" s="T28">tʼăbaktər-laʔbə-l</ta>
            <ta e="T30" id="Seg_1409" s="T29">bos-də</ta>
            <ta e="T31" id="Seg_1410" s="T30">šĭkə-t-ziʔ</ta>
            <ta e="T32" id="Seg_1411" s="T31">dĭgəttə</ta>
            <ta e="T33" id="Seg_1412" s="T32">măn</ta>
            <ta e="T34" id="Seg_1413" s="T33">tenö-bi-m</ta>
            <ta e="T36" id="Seg_1414" s="T34">možet</ta>
            <ta e="T37" id="Seg_1415" s="T36">kabarləj</ta>
            <ta e="T38" id="Seg_1416" s="T37">dĭgəttə</ta>
            <ta e="T39" id="Seg_1417" s="T38">măn</ta>
            <ta e="T40" id="Seg_1418" s="T39">tenö-bi-m</ta>
            <ta e="T41" id="Seg_1419" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_1420" s="T41">Jelʼa</ta>
            <ta e="T43" id="Seg_1421" s="T42">pʼaŋdə-bi</ta>
            <ta e="T44" id="Seg_1422" s="T43">măna</ta>
            <ta e="T45" id="Seg_1423" s="T44">dĭbər</ta>
            <ta e="T46" id="Seg_1424" s="T45">amnol-bi</ta>
            <ta e="T47" id="Seg_1425" s="T46">sazən-Tə</ta>
            <ta e="T48" id="Seg_1426" s="T47">măn</ta>
            <ta e="T49" id="Seg_1427" s="T48">mĭn-bi-m</ta>
            <ta e="T186" id="Seg_1428" s="T49">Kazan</ta>
            <ta e="T50" id="Seg_1429" s="T186">tura-Tə</ta>
            <ta e="T51" id="Seg_1430" s="T50">kudaj-Tə</ta>
            <ta e="T52" id="Seg_1431" s="T51">numan üzə-zittə</ta>
            <ta e="T53" id="Seg_1432" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_1433" s="T53">šo-bi-m</ta>
            <ta e="T55" id="Seg_1434" s="T54">dĭbər</ta>
            <ta e="T56" id="Seg_1435" s="T55">dĭn</ta>
            <ta e="T57" id="Seg_1436" s="T56">ej</ta>
            <ta e="T58" id="Seg_1437" s="T57">šo-bi-jəʔ</ta>
            <ta e="T59" id="Seg_1438" s="T58">kudaj-Tə</ta>
            <ta e="T60" id="Seg_1439" s="T59">numan üzə-zittə</ta>
            <ta e="T61" id="Seg_1440" s="T60">dĭgəttə</ta>
            <ta e="T62" id="Seg_1441" s="T61">unʼə-zAŋ-də</ta>
            <ta e="T63" id="Seg_1442" s="T62">bar</ta>
            <ta e="T64" id="Seg_1443" s="T63">kudaj-Tə</ta>
            <ta e="T65" id="Seg_1444" s="T64">numan üzə-bi-bAʔ</ta>
            <ta e="T66" id="Seg_1445" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_1446" s="T66">măn</ta>
            <ta e="T68" id="Seg_1447" s="T67">kan-bi-m</ta>
            <ta e="T69" id="Seg_1448" s="T68">maʔ-gənʼi</ta>
            <ta e="T70" id="Seg_1449" s="T69">dĭ-zAŋ</ta>
            <ta e="T71" id="Seg_1450" s="T70">ej</ta>
            <ta e="T72" id="Seg_1451" s="T71">öʔ-liA-jəʔ</ta>
            <ta e="T73" id="Seg_1452" s="T72">dʼok</ta>
            <ta e="T74" id="Seg_1453" s="T73">măn-ntə-m</ta>
            <ta e="T75" id="Seg_1454" s="T74">kan-lV-m</ta>
            <ta e="T76" id="Seg_1455" s="T75">kan-bi-m</ta>
            <ta e="T77" id="Seg_1456" s="T76">kan-bi-m</ta>
            <ta e="T78" id="Seg_1457" s="T77">surno</ta>
            <ta e="T79" id="Seg_1458" s="T78">bar</ta>
            <ta e="T80" id="Seg_1459" s="T79">šonə-gA</ta>
            <ta e="T81" id="Seg_1460" s="T80">da</ta>
            <ta e="T82" id="Seg_1461" s="T81">beržə</ta>
            <ta e="T83" id="Seg_1462" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1463" s="T83">šo-bi-m</ta>
            <ta e="T85" id="Seg_1464" s="T84">šo-bi-m</ta>
            <ta e="T86" id="Seg_1465" s="T85">mašina</ta>
            <ta e="T87" id="Seg_1466" s="T86">nu-gA</ta>
            <ta e="T88" id="Seg_1467" s="T87">măn</ta>
            <ta e="T89" id="Seg_1468" s="T88">šo-bi-m</ta>
            <ta e="T90" id="Seg_1469" s="T89">šo-bi-m</ta>
            <ta e="T91" id="Seg_1470" s="T90">aʔtʼi-Kən</ta>
            <ta e="T92" id="Seg_1471" s="T91">gijen</ta>
            <ta e="T93" id="Seg_1472" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_1473" s="T93">tura-jəʔ</ta>
            <ta e="T95" id="Seg_1474" s="T94">šo-bi-m</ta>
            <ta e="T96" id="Seg_1475" s="T95">Kuraža-Tə</ta>
            <ta e="T97" id="Seg_1476" s="T96">dĭn</ta>
            <ta e="T98" id="Seg_1477" s="T97">šaː-bi-m</ta>
            <ta e="T99" id="Seg_1478" s="T98">i</ta>
            <ta e="T101" id="Seg_1479" s="T100">ertə-n</ta>
            <ta e="T103" id="Seg_1480" s="T102">ertə</ta>
            <ta e="T104" id="Seg_1481" s="T103">uʔbdə-bi-m</ta>
            <ta e="T105" id="Seg_1482" s="T104">dĭgəttə</ta>
            <ta e="T106" id="Seg_1483" s="T105">šo-bi-m</ta>
            <ta e="T107" id="Seg_1484" s="T106">maʔ-gənʼi</ta>
            <ta e="T108" id="Seg_1485" s="T107">biəʔ</ta>
            <ta e="T109" id="Seg_1486" s="T108">časɨ</ta>
            <ta e="T110" id="Seg_1487" s="T109">i-bi-jəʔ</ta>
            <ta e="T111" id="Seg_1488" s="T110">šo-bi-m</ta>
            <ta e="T112" id="Seg_1489" s="T111">a</ta>
            <ta e="T113" id="Seg_1490" s="T112">tüžöj</ta>
            <ta e="T114" id="Seg_1491" s="T113">ej</ta>
            <ta e="T115" id="Seg_1492" s="T114">surdo-NTA</ta>
            <ta e="T116" id="Seg_1493" s="T115">büzəj</ta>
            <ta e="T117" id="Seg_1494" s="T116">bar</ta>
            <ta e="T118" id="Seg_1495" s="T117">kirgaːr-laʔbə</ta>
            <ta e="T119" id="Seg_1496" s="T118">süt</ta>
            <ta e="T120" id="Seg_1497" s="T119">ej</ta>
            <ta e="T121" id="Seg_1498" s="T120">kabar-liA</ta>
            <ta e="T122" id="Seg_1499" s="T121">šʼaːm-bi</ta>
            <ta e="T123" id="Seg_1500" s="T122">dĭ</ta>
            <ta e="T124" id="Seg_1501" s="T123">ne</ta>
            <ta e="T125" id="Seg_1502" s="T124">măna</ta>
            <ta e="T126" id="Seg_1503" s="T125">dĭgəttə</ta>
            <ta e="T127" id="Seg_1504" s="T126">măn</ta>
            <ta e="T128" id="Seg_1505" s="T127">toltano</ta>
            <ta e="T129" id="Seg_1506" s="T128">dĭ-Tə</ta>
            <ta e="T130" id="Seg_1507" s="T129">mĭ-bi-m</ta>
            <ta e="T131" id="Seg_1508" s="T130">i</ta>
            <ta e="T132" id="Seg_1509" s="T131">kar-bi-m</ta>
            <ta e="T133" id="Seg_1510" s="T132">dĭ</ta>
            <ta e="T134" id="Seg_1511" s="T133">ej</ta>
            <ta e="T135" id="Seg_1512" s="T134">e-ʔ</ta>
            <ta e="T136" id="Seg_1513" s="T135">kirgaːr-laʔbə</ta>
            <ta e="T137" id="Seg_1514" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_1515" s="T137">nüdʼi-n</ta>
            <ta e="T139" id="Seg_1516" s="T138">tüžöj</ta>
            <ta e="T140" id="Seg_1517" s="T139">šo-bi</ta>
            <ta e="T141" id="Seg_1518" s="T140">kan-bi-m</ta>
            <ta e="T142" id="Seg_1519" s="T141">surdo-zittə</ta>
            <ta e="T143" id="Seg_1520" s="T142">ku-liA-m</ta>
            <ta e="T144" id="Seg_1521" s="T143">süt</ta>
            <ta e="T145" id="Seg_1522" s="T144">ugaːndə</ta>
            <ta e="T146" id="Seg_1523" s="T145">iʔgö</ta>
            <ta e="T147" id="Seg_1524" s="T146">ĭmbi=də</ta>
            <ta e="T148" id="Seg_1525" s="T147">ej</ta>
            <ta e="T149" id="Seg_1526" s="T148">nörbə-bi-m</ta>
            <ta e="T150" id="Seg_1527" s="T149">dĭgəttə</ta>
            <ta e="T151" id="Seg_1528" s="T150">ertə-n</ta>
            <ta e="T152" id="Seg_1529" s="T151">surdo-bi-m</ta>
            <ta e="T153" id="Seg_1530" s="T152">dĭgəttə</ta>
            <ta e="T154" id="Seg_1531" s="T153">bazoʔ</ta>
            <ta e="T155" id="Seg_1532" s="T154">nüdʼi-n</ta>
            <ta e="T156" id="Seg_1533" s="T155">surdo-bi-m</ta>
            <ta e="T157" id="Seg_1534" s="T156">ku-liA-m</ta>
            <ta e="T158" id="Seg_1535" s="T157">bar</ta>
            <ta e="T159" id="Seg_1536" s="T158">süt</ta>
            <ta e="T160" id="Seg_1537" s="T159">amka</ta>
            <ta e="T161" id="Seg_1538" s="T160">dĭgəttə</ta>
            <ta e="T162" id="Seg_1539" s="T161">măn-bi-m</ta>
            <ta e="T164" id="Seg_1540" s="T163">šiʔ</ta>
            <ta e="T165" id="Seg_1541" s="T164">taldʼen</ta>
            <ta e="T166" id="Seg_1542" s="T165">tüžöj</ta>
            <ta e="T167" id="Seg_1543" s="T166">ej</ta>
            <ta e="T168" id="Seg_1544" s="T167">surdo-bi-lAʔ</ta>
            <ta e="T169" id="Seg_1545" s="T168">a</ta>
            <ta e="T170" id="Seg_1546" s="T169">dĭ</ta>
            <ta e="T171" id="Seg_1547" s="T170">koʔbdo</ta>
            <ta e="T172" id="Seg_1548" s="T171">măn-ntə</ta>
            <ta e="T173" id="Seg_1549" s="T172">i</ta>
            <ta e="T174" id="Seg_1550" s="T173">miʔ</ta>
            <ta e="T175" id="Seg_1551" s="T174">ej</ta>
            <ta e="T176" id="Seg_1552" s="T175">surdo-NTA</ta>
            <ta e="T177" id="Seg_1553" s="T176">i</ta>
            <ta e="T178" id="Seg_1554" s="T177">tăn</ta>
            <ta e="T179" id="Seg_1555" s="T178">ej</ta>
            <ta e="T180" id="Seg_1556" s="T179">surdo-NTA</ta>
            <ta e="T182" id="Seg_1557" s="T181">dărəʔ</ta>
            <ta e="T183" id="Seg_1558" s="T182">öʔlu-bi-bAʔ</ta>
            <ta e="T184" id="Seg_1559" s="T183">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1560" s="T0">we.NOM</ta>
            <ta e="T2" id="Seg_1561" s="T1">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T3" id="Seg_1562" s="T2">come-PST.[3SG]</ta>
            <ta e="T185" id="Seg_1563" s="T3">Aginskoe</ta>
            <ta e="T4" id="Seg_1564" s="T185">settlement-LOC</ta>
            <ta e="T5" id="Seg_1565" s="T4">say-IPFVZ.[3SG]</ta>
            <ta e="T6" id="Seg_1566" s="T5">grandmother.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1567" s="T6">you.NOM</ta>
            <ta e="T8" id="Seg_1568" s="T7">paper-LOC</ta>
            <ta e="T9" id="Seg_1569" s="T8">sit-DUR-2SG</ta>
            <ta e="T10" id="Seg_1570" s="T9">and</ta>
            <ta e="T11" id="Seg_1571" s="T10">how</ta>
            <ta e="T13" id="Seg_1572" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_1573" s="T13">frighten-MOM-PST-1SG</ta>
            <ta e="T15" id="Seg_1574" s="T14">PTCL</ta>
            <ta e="T16" id="Seg_1575" s="T15">maybe</ta>
            <ta e="T17" id="Seg_1576" s="T16">think-PST-1SG</ta>
            <ta e="T18" id="Seg_1577" s="T17">that</ta>
            <ta e="T19" id="Seg_1578" s="T18">God-LAT</ta>
            <ta e="T20" id="Seg_1579" s="T19">pray-DUR-1SG</ta>
            <ta e="T21" id="Seg_1580" s="T20">then</ta>
            <ta e="T22" id="Seg_1581" s="T21">paper-LAT</ta>
            <ta e="T23" id="Seg_1582" s="T22">seat-PST-3PL</ta>
            <ta e="T24" id="Seg_1583" s="T23">this.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1584" s="T24">say-IPFVZ.[3SG]</ta>
            <ta e="T26" id="Seg_1585" s="T25">no</ta>
            <ta e="T27" id="Seg_1586" s="T26">you.NOM</ta>
            <ta e="T29" id="Seg_1587" s="T28">speak-DUR-2SG</ta>
            <ta e="T30" id="Seg_1588" s="T29">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T31" id="Seg_1589" s="T30">language-3SG-INS</ta>
            <ta e="T32" id="Seg_1590" s="T31">then</ta>
            <ta e="T33" id="Seg_1591" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_1592" s="T33">think-PST-1SG</ta>
            <ta e="T36" id="Seg_1593" s="T34">maybe</ta>
            <ta e="T37" id="Seg_1594" s="T36">enough</ta>
            <ta e="T38" id="Seg_1595" s="T37">then</ta>
            <ta e="T39" id="Seg_1596" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_1597" s="T39">think-PST-1SG</ta>
            <ta e="T41" id="Seg_1598" s="T40">this.[NOM.SG]</ta>
            <ta e="T42" id="Seg_1599" s="T41">Elya.[NOM.SG]</ta>
            <ta e="T43" id="Seg_1600" s="T42">write-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1601" s="T43">I.ACC</ta>
            <ta e="T45" id="Seg_1602" s="T44">there</ta>
            <ta e="T46" id="Seg_1603" s="T45">seat-PST.[3SG]</ta>
            <ta e="T47" id="Seg_1604" s="T46">paper-LAT</ta>
            <ta e="T48" id="Seg_1605" s="T47">I.NOM</ta>
            <ta e="T49" id="Seg_1606" s="T48">go-PST-1SG</ta>
            <ta e="T186" id="Seg_1607" s="T49">Aginskoe</ta>
            <ta e="T50" id="Seg_1608" s="T186">settlement-LAT</ta>
            <ta e="T51" id="Seg_1609" s="T50">God-LAT</ta>
            <ta e="T52" id="Seg_1610" s="T51">pray-INF.LAT</ta>
            <ta e="T53" id="Seg_1611" s="T52">then</ta>
            <ta e="T54" id="Seg_1612" s="T53">come-PST-1SG</ta>
            <ta e="T55" id="Seg_1613" s="T54">there</ta>
            <ta e="T56" id="Seg_1614" s="T55">there</ta>
            <ta e="T57" id="Seg_1615" s="T56">NEG</ta>
            <ta e="T58" id="Seg_1616" s="T57">come-PST-3PL</ta>
            <ta e="T59" id="Seg_1617" s="T58">God-LAT</ta>
            <ta e="T60" id="Seg_1618" s="T59">pray-INF.LAT</ta>
            <ta e="T61" id="Seg_1619" s="T60">then</ta>
            <ta e="T62" id="Seg_1620" s="T61">alone-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T63" id="Seg_1621" s="T62">PTCL</ta>
            <ta e="T64" id="Seg_1622" s="T63">God-LAT</ta>
            <ta e="T65" id="Seg_1623" s="T64">pray-PST-1PL</ta>
            <ta e="T66" id="Seg_1624" s="T65">then</ta>
            <ta e="T67" id="Seg_1625" s="T66">I.NOM</ta>
            <ta e="T68" id="Seg_1626" s="T67">go-PST-1SG</ta>
            <ta e="T69" id="Seg_1627" s="T68">tent-LAT/LOC.1SG</ta>
            <ta e="T70" id="Seg_1628" s="T69">this-PL</ta>
            <ta e="T71" id="Seg_1629" s="T70">NEG</ta>
            <ta e="T72" id="Seg_1630" s="T71">let-PRS-3PL</ta>
            <ta e="T73" id="Seg_1631" s="T72">no</ta>
            <ta e="T74" id="Seg_1632" s="T73">say-IPFVZ-1SG</ta>
            <ta e="T75" id="Seg_1633" s="T74">go-FUT-1SG</ta>
            <ta e="T76" id="Seg_1634" s="T75">go-PST-1SG</ta>
            <ta e="T77" id="Seg_1635" s="T76">go-PST-1SG</ta>
            <ta e="T78" id="Seg_1636" s="T77">rain.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1637" s="T78">PTCL</ta>
            <ta e="T80" id="Seg_1638" s="T79">come-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_1639" s="T80">and</ta>
            <ta e="T82" id="Seg_1640" s="T81">wind.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1641" s="T82">then</ta>
            <ta e="T84" id="Seg_1642" s="T83">come-PST-1SG</ta>
            <ta e="T85" id="Seg_1643" s="T84">come-PST-1SG</ta>
            <ta e="T86" id="Seg_1644" s="T85">machine.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1645" s="T86">stand-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_1646" s="T87">I.NOM</ta>
            <ta e="T89" id="Seg_1647" s="T88">come-PST-1SG</ta>
            <ta e="T90" id="Seg_1648" s="T89">come-PST-1SG</ta>
            <ta e="T91" id="Seg_1649" s="T90">road-LOC</ta>
            <ta e="T92" id="Seg_1650" s="T91">where</ta>
            <ta e="T93" id="Seg_1651" s="T92">this.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1652" s="T93">house-PL</ta>
            <ta e="T95" id="Seg_1653" s="T94">come-PST-1SG</ta>
            <ta e="T96" id="Seg_1654" s="T95">Kurazha-LAT</ta>
            <ta e="T97" id="Seg_1655" s="T96">there</ta>
            <ta e="T98" id="Seg_1656" s="T97">spend.night-PST-1SG</ta>
            <ta e="T99" id="Seg_1657" s="T98">and</ta>
            <ta e="T101" id="Seg_1658" s="T100">morning-GEN</ta>
            <ta e="T103" id="Seg_1659" s="T102">morning</ta>
            <ta e="T104" id="Seg_1660" s="T103">get.up-PST-1SG</ta>
            <ta e="T105" id="Seg_1661" s="T104">then</ta>
            <ta e="T106" id="Seg_1662" s="T105">come-PST-1SG</ta>
            <ta e="T107" id="Seg_1663" s="T106">tent-LAT/LOC.1SG</ta>
            <ta e="T108" id="Seg_1664" s="T107">ten.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1665" s="T108">hour.PL</ta>
            <ta e="T110" id="Seg_1666" s="T109">be-PST-3PL</ta>
            <ta e="T111" id="Seg_1667" s="T110">come-PST-1SG</ta>
            <ta e="T112" id="Seg_1668" s="T111">and</ta>
            <ta e="T113" id="Seg_1669" s="T112">cow.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1670" s="T113">NEG</ta>
            <ta e="T115" id="Seg_1671" s="T114">milk-PTCP.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1672" s="T115">calf.[NOM.SG]</ta>
            <ta e="T117" id="Seg_1673" s="T116">PTCL</ta>
            <ta e="T118" id="Seg_1674" s="T117">shout-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_1675" s="T118">milk.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1676" s="T119">NEG</ta>
            <ta e="T121" id="Seg_1677" s="T120">grab-PRS.[3SG]</ta>
            <ta e="T122" id="Seg_1678" s="T121">lie-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1679" s="T122">this.[NOM.SG]</ta>
            <ta e="T124" id="Seg_1680" s="T123">woman.[NOM.SG]</ta>
            <ta e="T125" id="Seg_1681" s="T124">I.LAT</ta>
            <ta e="T126" id="Seg_1682" s="T125">then</ta>
            <ta e="T127" id="Seg_1683" s="T126">I.NOM</ta>
            <ta e="T128" id="Seg_1684" s="T127">potato.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1685" s="T128">this-LAT</ta>
            <ta e="T130" id="Seg_1686" s="T129">give-PST-1SG</ta>
            <ta e="T131" id="Seg_1687" s="T130">and</ta>
            <ta e="T132" id="Seg_1688" s="T131">open-PST-1SG</ta>
            <ta e="T133" id="Seg_1689" s="T132">this.[NOM.SG]</ta>
            <ta e="T134" id="Seg_1690" s="T133">NEG</ta>
            <ta e="T135" id="Seg_1691" s="T134">NEG.AUX-IMP.2SG</ta>
            <ta e="T136" id="Seg_1692" s="T135">shout-DUR.[3SG]</ta>
            <ta e="T137" id="Seg_1693" s="T136">then</ta>
            <ta e="T138" id="Seg_1694" s="T137">evening-LOC.ADV</ta>
            <ta e="T139" id="Seg_1695" s="T138">cow.[NOM.SG]</ta>
            <ta e="T140" id="Seg_1696" s="T139">come-PST.[3SG]</ta>
            <ta e="T141" id="Seg_1697" s="T140">go-PST-1SG</ta>
            <ta e="T142" id="Seg_1698" s="T141">milk-INF.LAT</ta>
            <ta e="T143" id="Seg_1699" s="T142">see-PRS-1SG</ta>
            <ta e="T144" id="Seg_1700" s="T143">milk.[NOM.SG]</ta>
            <ta e="T145" id="Seg_1701" s="T144">very</ta>
            <ta e="T146" id="Seg_1702" s="T145">many</ta>
            <ta e="T147" id="Seg_1703" s="T146">what.[NOM.SG]=INDEF</ta>
            <ta e="T148" id="Seg_1704" s="T147">NEG</ta>
            <ta e="T149" id="Seg_1705" s="T148">tell-PST-1SG</ta>
            <ta e="T150" id="Seg_1706" s="T149">then</ta>
            <ta e="T151" id="Seg_1707" s="T150">morning-GEN</ta>
            <ta e="T152" id="Seg_1708" s="T151">milk-PST-1SG</ta>
            <ta e="T153" id="Seg_1709" s="T152">then</ta>
            <ta e="T154" id="Seg_1710" s="T153">again</ta>
            <ta e="T155" id="Seg_1711" s="T154">evening-LOC.ADV</ta>
            <ta e="T156" id="Seg_1712" s="T155">milk-PST-1SG</ta>
            <ta e="T157" id="Seg_1713" s="T156">see-PRS-1SG</ta>
            <ta e="T158" id="Seg_1714" s="T157">PTCL</ta>
            <ta e="T159" id="Seg_1715" s="T158">milk.[NOM.SG]</ta>
            <ta e="T160" id="Seg_1716" s="T159">few</ta>
            <ta e="T161" id="Seg_1717" s="T160">then</ta>
            <ta e="T162" id="Seg_1718" s="T161">say-PST-1SG</ta>
            <ta e="T164" id="Seg_1719" s="T163">you.PL.NOM</ta>
            <ta e="T165" id="Seg_1720" s="T164">yesterday</ta>
            <ta e="T166" id="Seg_1721" s="T165">cow.[NOM.SG]</ta>
            <ta e="T167" id="Seg_1722" s="T166">NEG</ta>
            <ta e="T168" id="Seg_1723" s="T167">milk-PST-2PL</ta>
            <ta e="T169" id="Seg_1724" s="T168">and</ta>
            <ta e="T170" id="Seg_1725" s="T169">this.[NOM.SG]</ta>
            <ta e="T171" id="Seg_1726" s="T170">girl.[NOM.SG]</ta>
            <ta e="T172" id="Seg_1727" s="T171">say-IPFVZ.[3SG]</ta>
            <ta e="T173" id="Seg_1728" s="T172">and</ta>
            <ta e="T174" id="Seg_1729" s="T173">we.NOM</ta>
            <ta e="T175" id="Seg_1730" s="T174">NEG</ta>
            <ta e="T176" id="Seg_1731" s="T175">milk-PTCP.[NOM.SG]</ta>
            <ta e="T177" id="Seg_1732" s="T176">and</ta>
            <ta e="T178" id="Seg_1733" s="T177">you.NOM</ta>
            <ta e="T179" id="Seg_1734" s="T178">NEG</ta>
            <ta e="T180" id="Seg_1735" s="T179">milk-PTCP.[NOM.SG]</ta>
            <ta e="T182" id="Seg_1736" s="T181">so</ta>
            <ta e="T183" id="Seg_1737" s="T182">send-PST-1PL</ta>
            <ta e="T184" id="Seg_1738" s="T183">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1739" s="T0">мы.NOM</ta>
            <ta e="T2" id="Seg_1740" s="T1">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T3" id="Seg_1741" s="T2">прийти-PST.[3SG]</ta>
            <ta e="T185" id="Seg_1742" s="T3">Агинское</ta>
            <ta e="T4" id="Seg_1743" s="T185">поселение-LOC</ta>
            <ta e="T5" id="Seg_1744" s="T4">сказать-IPFVZ.[3SG]</ta>
            <ta e="T6" id="Seg_1745" s="T5">бабушка.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1746" s="T6">ты.NOM</ta>
            <ta e="T8" id="Seg_1747" s="T7">бумага-LOC</ta>
            <ta e="T9" id="Seg_1748" s="T8">сидеть-DUR-2SG</ta>
            <ta e="T10" id="Seg_1749" s="T9">а</ta>
            <ta e="T11" id="Seg_1750" s="T10">как</ta>
            <ta e="T13" id="Seg_1751" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_1752" s="T13">пугать-MOM-PST-1SG</ta>
            <ta e="T15" id="Seg_1753" s="T14">PTCL</ta>
            <ta e="T16" id="Seg_1754" s="T15">может.быть</ta>
            <ta e="T17" id="Seg_1755" s="T16">думать-PST-1SG</ta>
            <ta e="T18" id="Seg_1756" s="T17">что</ta>
            <ta e="T19" id="Seg_1757" s="T18">Бог-LAT</ta>
            <ta e="T20" id="Seg_1758" s="T19">молиться-DUR-1SG</ta>
            <ta e="T21" id="Seg_1759" s="T20">тогда</ta>
            <ta e="T22" id="Seg_1760" s="T21">бумага-LAT</ta>
            <ta e="T23" id="Seg_1761" s="T22">сажать-PST-3PL</ta>
            <ta e="T24" id="Seg_1762" s="T23">этот.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1763" s="T24">сказать-IPFVZ.[3SG]</ta>
            <ta e="T26" id="Seg_1764" s="T25">нет</ta>
            <ta e="T27" id="Seg_1765" s="T26">ты.NOM</ta>
            <ta e="T29" id="Seg_1766" s="T28">говорить-DUR-2SG</ta>
            <ta e="T30" id="Seg_1767" s="T29">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T31" id="Seg_1768" s="T30">язык-3SG-INS</ta>
            <ta e="T32" id="Seg_1769" s="T31">тогда</ta>
            <ta e="T33" id="Seg_1770" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_1771" s="T33">думать-PST-1SG</ta>
            <ta e="T36" id="Seg_1772" s="T34">может.быть</ta>
            <ta e="T37" id="Seg_1773" s="T36">хватит</ta>
            <ta e="T38" id="Seg_1774" s="T37">тогда</ta>
            <ta e="T39" id="Seg_1775" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_1776" s="T39">думать-PST-1SG</ta>
            <ta e="T41" id="Seg_1777" s="T40">этот.[NOM.SG]</ta>
            <ta e="T42" id="Seg_1778" s="T41">Эля.[NOM.SG]</ta>
            <ta e="T43" id="Seg_1779" s="T42">писать-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1780" s="T43">я.ACC</ta>
            <ta e="T45" id="Seg_1781" s="T44">там</ta>
            <ta e="T46" id="Seg_1782" s="T45">сажать-PST.[3SG]</ta>
            <ta e="T47" id="Seg_1783" s="T46">бумага-LAT</ta>
            <ta e="T48" id="Seg_1784" s="T47">я.NOM</ta>
            <ta e="T49" id="Seg_1785" s="T48">идти-PST-1SG</ta>
            <ta e="T186" id="Seg_1786" s="T49">Агинское</ta>
            <ta e="T50" id="Seg_1787" s="T186">поселение-LAT</ta>
            <ta e="T51" id="Seg_1788" s="T50">Бог-LAT</ta>
            <ta e="T52" id="Seg_1789" s="T51">молиться-INF.LAT</ta>
            <ta e="T53" id="Seg_1790" s="T52">тогда</ta>
            <ta e="T54" id="Seg_1791" s="T53">прийти-PST-1SG</ta>
            <ta e="T55" id="Seg_1792" s="T54">там</ta>
            <ta e="T56" id="Seg_1793" s="T55">там</ta>
            <ta e="T57" id="Seg_1794" s="T56">NEG</ta>
            <ta e="T58" id="Seg_1795" s="T57">прийти-PST-3PL</ta>
            <ta e="T59" id="Seg_1796" s="T58">Бог-LAT</ta>
            <ta e="T60" id="Seg_1797" s="T59">молиться-INF.LAT</ta>
            <ta e="T61" id="Seg_1798" s="T60">тогда</ta>
            <ta e="T62" id="Seg_1799" s="T61">один-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T63" id="Seg_1800" s="T62">PTCL</ta>
            <ta e="T64" id="Seg_1801" s="T63">Бог-LAT</ta>
            <ta e="T65" id="Seg_1802" s="T64">молиться-PST-1PL</ta>
            <ta e="T66" id="Seg_1803" s="T65">тогда</ta>
            <ta e="T67" id="Seg_1804" s="T66">я.NOM</ta>
            <ta e="T68" id="Seg_1805" s="T67">пойти-PST-1SG</ta>
            <ta e="T69" id="Seg_1806" s="T68">чум-LAT/LOC.1SG</ta>
            <ta e="T70" id="Seg_1807" s="T69">этот-PL</ta>
            <ta e="T71" id="Seg_1808" s="T70">NEG</ta>
            <ta e="T72" id="Seg_1809" s="T71">пускать-PRS-3PL</ta>
            <ta e="T73" id="Seg_1810" s="T72">нет</ta>
            <ta e="T74" id="Seg_1811" s="T73">сказать-IPFVZ-1SG</ta>
            <ta e="T75" id="Seg_1812" s="T74">пойти-FUT-1SG</ta>
            <ta e="T76" id="Seg_1813" s="T75">пойти-PST-1SG</ta>
            <ta e="T77" id="Seg_1814" s="T76">пойти-PST-1SG</ta>
            <ta e="T78" id="Seg_1815" s="T77">дождь.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1816" s="T78">PTCL</ta>
            <ta e="T80" id="Seg_1817" s="T79">прийти-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_1818" s="T80">и</ta>
            <ta e="T82" id="Seg_1819" s="T81">ветер.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1820" s="T82">тогда</ta>
            <ta e="T84" id="Seg_1821" s="T83">прийти-PST-1SG</ta>
            <ta e="T85" id="Seg_1822" s="T84">прийти-PST-1SG</ta>
            <ta e="T86" id="Seg_1823" s="T85">машина.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1824" s="T86">стоять-PRS.[3SG]</ta>
            <ta e="T88" id="Seg_1825" s="T87">я.NOM</ta>
            <ta e="T89" id="Seg_1826" s="T88">прийти-PST-1SG</ta>
            <ta e="T90" id="Seg_1827" s="T89">прийти-PST-1SG</ta>
            <ta e="T91" id="Seg_1828" s="T90">дорога-LOC</ta>
            <ta e="T92" id="Seg_1829" s="T91">где</ta>
            <ta e="T93" id="Seg_1830" s="T92">этот.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1831" s="T93">дом-PL</ta>
            <ta e="T95" id="Seg_1832" s="T94">прийти-PST-1SG</ta>
            <ta e="T96" id="Seg_1833" s="T95">Куража-LAT</ta>
            <ta e="T97" id="Seg_1834" s="T96">там</ta>
            <ta e="T98" id="Seg_1835" s="T97">ночевать-PST-1SG</ta>
            <ta e="T99" id="Seg_1836" s="T98">и</ta>
            <ta e="T101" id="Seg_1837" s="T100">утро-GEN</ta>
            <ta e="T103" id="Seg_1838" s="T102">утро</ta>
            <ta e="T104" id="Seg_1839" s="T103">встать-PST-1SG</ta>
            <ta e="T105" id="Seg_1840" s="T104">тогда</ta>
            <ta e="T106" id="Seg_1841" s="T105">прийти-PST-1SG</ta>
            <ta e="T107" id="Seg_1842" s="T106">чум-LAT/LOC.1SG</ta>
            <ta e="T108" id="Seg_1843" s="T107">десять.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1844" s="T108">час.PL</ta>
            <ta e="T110" id="Seg_1845" s="T109">быть-PST-3PL</ta>
            <ta e="T111" id="Seg_1846" s="T110">прийти-PST-1SG</ta>
            <ta e="T112" id="Seg_1847" s="T111">а</ta>
            <ta e="T113" id="Seg_1848" s="T112">корова.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1849" s="T113">NEG</ta>
            <ta e="T115" id="Seg_1850" s="T114">доить-PTCP.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1851" s="T115">теленок.[NOM.SG]</ta>
            <ta e="T117" id="Seg_1852" s="T116">PTCL</ta>
            <ta e="T118" id="Seg_1853" s="T117">кричать-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_1854" s="T118">молоко.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1855" s="T119">NEG</ta>
            <ta e="T121" id="Seg_1856" s="T120">хватать-PRS.[3SG]</ta>
            <ta e="T122" id="Seg_1857" s="T121">лгать-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1858" s="T122">этот.[NOM.SG]</ta>
            <ta e="T124" id="Seg_1859" s="T123">женщина.[NOM.SG]</ta>
            <ta e="T125" id="Seg_1860" s="T124">я.LAT</ta>
            <ta e="T126" id="Seg_1861" s="T125">тогда</ta>
            <ta e="T127" id="Seg_1862" s="T126">я.NOM</ta>
            <ta e="T128" id="Seg_1863" s="T127">картофель.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1864" s="T128">этот-LAT</ta>
            <ta e="T130" id="Seg_1865" s="T129">дать-PST-1SG</ta>
            <ta e="T131" id="Seg_1866" s="T130">и</ta>
            <ta e="T132" id="Seg_1867" s="T131">открыть-PST-1SG</ta>
            <ta e="T133" id="Seg_1868" s="T132">этот.[NOM.SG]</ta>
            <ta e="T134" id="Seg_1869" s="T133">NEG</ta>
            <ta e="T135" id="Seg_1870" s="T134">NEG.AUX-IMP.2SG</ta>
            <ta e="T136" id="Seg_1871" s="T135">кричать-DUR.[3SG]</ta>
            <ta e="T137" id="Seg_1872" s="T136">тогда</ta>
            <ta e="T138" id="Seg_1873" s="T137">вечер-LOC.ADV</ta>
            <ta e="T139" id="Seg_1874" s="T138">корова.[NOM.SG]</ta>
            <ta e="T140" id="Seg_1875" s="T139">прийти-PST.[3SG]</ta>
            <ta e="T141" id="Seg_1876" s="T140">пойти-PST-1SG</ta>
            <ta e="T142" id="Seg_1877" s="T141">доить-INF.LAT</ta>
            <ta e="T143" id="Seg_1878" s="T142">видеть-PRS-1SG</ta>
            <ta e="T144" id="Seg_1879" s="T143">молоко.[NOM.SG]</ta>
            <ta e="T145" id="Seg_1880" s="T144">очень</ta>
            <ta e="T146" id="Seg_1881" s="T145">много</ta>
            <ta e="T147" id="Seg_1882" s="T146">что.[NOM.SG]=INDEF</ta>
            <ta e="T148" id="Seg_1883" s="T147">NEG</ta>
            <ta e="T149" id="Seg_1884" s="T148">сказать-PST-1SG</ta>
            <ta e="T150" id="Seg_1885" s="T149">тогда</ta>
            <ta e="T151" id="Seg_1886" s="T150">утро-GEN</ta>
            <ta e="T152" id="Seg_1887" s="T151">доить-PST-1SG</ta>
            <ta e="T153" id="Seg_1888" s="T152">тогда</ta>
            <ta e="T154" id="Seg_1889" s="T153">опять</ta>
            <ta e="T155" id="Seg_1890" s="T154">вечер-LOC.ADV</ta>
            <ta e="T156" id="Seg_1891" s="T155">доить-PST-1SG</ta>
            <ta e="T157" id="Seg_1892" s="T156">видеть-PRS-1SG</ta>
            <ta e="T158" id="Seg_1893" s="T157">PTCL</ta>
            <ta e="T159" id="Seg_1894" s="T158">молоко.[NOM.SG]</ta>
            <ta e="T160" id="Seg_1895" s="T159">мало</ta>
            <ta e="T161" id="Seg_1896" s="T160">тогда</ta>
            <ta e="T162" id="Seg_1897" s="T161">сказать-PST-1SG</ta>
            <ta e="T164" id="Seg_1898" s="T163">вы.NOM</ta>
            <ta e="T165" id="Seg_1899" s="T164">вчера</ta>
            <ta e="T166" id="Seg_1900" s="T165">корова.[NOM.SG]</ta>
            <ta e="T167" id="Seg_1901" s="T166">NEG</ta>
            <ta e="T168" id="Seg_1902" s="T167">доить-PST-2PL</ta>
            <ta e="T169" id="Seg_1903" s="T168">а</ta>
            <ta e="T170" id="Seg_1904" s="T169">этот.[NOM.SG]</ta>
            <ta e="T171" id="Seg_1905" s="T170">девушка.[NOM.SG]</ta>
            <ta e="T172" id="Seg_1906" s="T171">сказать-IPFVZ.[3SG]</ta>
            <ta e="T173" id="Seg_1907" s="T172">и</ta>
            <ta e="T174" id="Seg_1908" s="T173">мы.NOM</ta>
            <ta e="T175" id="Seg_1909" s="T174">NEG</ta>
            <ta e="T176" id="Seg_1910" s="T175">доить-PTCP.[NOM.SG]</ta>
            <ta e="T177" id="Seg_1911" s="T176">и</ta>
            <ta e="T178" id="Seg_1912" s="T177">ты.NOM</ta>
            <ta e="T179" id="Seg_1913" s="T178">NEG</ta>
            <ta e="T180" id="Seg_1914" s="T179">доить-PTCP.[NOM.SG]</ta>
            <ta e="T182" id="Seg_1915" s="T181">так</ta>
            <ta e="T183" id="Seg_1916" s="T182">посылать-PST-1PL</ta>
            <ta e="T184" id="Seg_1917" s="T183">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1918" s="T0">pers</ta>
            <ta e="T2" id="Seg_1919" s="T1">n-n:case.poss</ta>
            <ta e="T3" id="Seg_1920" s="T2">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_1921" s="T3">propr</ta>
            <ta e="T4" id="Seg_1922" s="T185">n-n:case</ta>
            <ta e="T5" id="Seg_1923" s="T4">v-v&gt;v-v:pn</ta>
            <ta e="T6" id="Seg_1924" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1925" s="T6">pers</ta>
            <ta e="T8" id="Seg_1926" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1927" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_1928" s="T9">conj</ta>
            <ta e="T11" id="Seg_1929" s="T10">que</ta>
            <ta e="T13" id="Seg_1930" s="T12">pers</ta>
            <ta e="T14" id="Seg_1931" s="T13">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1932" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_1933" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_1934" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1935" s="T17">conj</ta>
            <ta e="T19" id="Seg_1936" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1937" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_1938" s="T20">adv</ta>
            <ta e="T22" id="Seg_1939" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1940" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1941" s="T23">dempro-n:case</ta>
            <ta e="T25" id="Seg_1942" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_1943" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_1944" s="T26">pers</ta>
            <ta e="T29" id="Seg_1945" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_1946" s="T29">refl-n:case.poss</ta>
            <ta e="T31" id="Seg_1947" s="T30">n-n:case.poss-n:case</ta>
            <ta e="T32" id="Seg_1948" s="T31">adv</ta>
            <ta e="T33" id="Seg_1949" s="T32">pers</ta>
            <ta e="T34" id="Seg_1950" s="T33">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1951" s="T34">ptcl</ta>
            <ta e="T37" id="Seg_1952" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_1953" s="T37">adv</ta>
            <ta e="T39" id="Seg_1954" s="T38">pers</ta>
            <ta e="T40" id="Seg_1955" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1956" s="T40">dempro-n:case</ta>
            <ta e="T42" id="Seg_1957" s="T41">propr-n:case</ta>
            <ta e="T43" id="Seg_1958" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1959" s="T43">pers</ta>
            <ta e="T45" id="Seg_1960" s="T44">adv</ta>
            <ta e="T46" id="Seg_1961" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1962" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_1963" s="T47">pers</ta>
            <ta e="T49" id="Seg_1964" s="T48">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_1965" s="T49">propr</ta>
            <ta e="T50" id="Seg_1966" s="T186">n-n:case</ta>
            <ta e="T51" id="Seg_1967" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_1968" s="T51">v-v:n.fin</ta>
            <ta e="T53" id="Seg_1969" s="T52">adv</ta>
            <ta e="T54" id="Seg_1970" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1971" s="T54">adv</ta>
            <ta e="T56" id="Seg_1972" s="T55">adv</ta>
            <ta e="T57" id="Seg_1973" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_1974" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1975" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_1976" s="T59">v-v:n.fin</ta>
            <ta e="T61" id="Seg_1977" s="T60">adv</ta>
            <ta e="T62" id="Seg_1978" s="T61">adv-n:num-n:case.poss</ta>
            <ta e="T63" id="Seg_1979" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_1980" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_1981" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1982" s="T65">adv</ta>
            <ta e="T67" id="Seg_1983" s="T66">pers</ta>
            <ta e="T68" id="Seg_1984" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1985" s="T68">n-n:case.poss</ta>
            <ta e="T70" id="Seg_1986" s="T69">dempro-n:num</ta>
            <ta e="T71" id="Seg_1987" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_1988" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_1989" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_1990" s="T73">v-v&gt;v-v:pn</ta>
            <ta e="T75" id="Seg_1991" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1992" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1993" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1994" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1995" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1996" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1997" s="T80">conj</ta>
            <ta e="T82" id="Seg_1998" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1999" s="T82">adv</ta>
            <ta e="T84" id="Seg_2000" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_2001" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_2002" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_2003" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_2004" s="T87">pers</ta>
            <ta e="T89" id="Seg_2005" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_2006" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2007" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2008" s="T91">que</ta>
            <ta e="T93" id="Seg_2009" s="T92">dempro-n:case</ta>
            <ta e="T94" id="Seg_2010" s="T93">n-n:num</ta>
            <ta e="T95" id="Seg_2011" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_2012" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2013" s="T96">adv</ta>
            <ta e="T98" id="Seg_2014" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2015" s="T98">conj</ta>
            <ta e="T101" id="Seg_2016" s="T100">n-n:case</ta>
            <ta e="T103" id="Seg_2017" s="T102">n</ta>
            <ta e="T104" id="Seg_2018" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_2019" s="T104">adv</ta>
            <ta e="T106" id="Seg_2020" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_2021" s="T106">n-n:case.poss</ta>
            <ta e="T108" id="Seg_2022" s="T107">num-n:case</ta>
            <ta e="T109" id="Seg_2023" s="T108">n</ta>
            <ta e="T110" id="Seg_2024" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_2025" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_2026" s="T111">conj</ta>
            <ta e="T113" id="Seg_2027" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2028" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_2029" s="T114">v-v:n.fin-n:case</ta>
            <ta e="T116" id="Seg_2030" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_2031" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_2032" s="T117">v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_2033" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2034" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2035" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_2036" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2037" s="T122">dempro-n:case</ta>
            <ta e="T124" id="Seg_2038" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2039" s="T124">pers</ta>
            <ta e="T126" id="Seg_2040" s="T125">adv</ta>
            <ta e="T127" id="Seg_2041" s="T126">pers</ta>
            <ta e="T128" id="Seg_2042" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_2043" s="T128">dempro-n:case</ta>
            <ta e="T130" id="Seg_2044" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_2045" s="T130">conj</ta>
            <ta e="T132" id="Seg_2046" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_2047" s="T132">dempro-n:case</ta>
            <ta e="T134" id="Seg_2048" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2049" s="T134">aux-v:mood.pn</ta>
            <ta e="T136" id="Seg_2050" s="T135">v-v&gt;v-v:pn</ta>
            <ta e="T137" id="Seg_2051" s="T136">adv</ta>
            <ta e="T138" id="Seg_2052" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2053" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_2054" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_2055" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2056" s="T141">v-v:n.fin</ta>
            <ta e="T143" id="Seg_2057" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2058" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_2059" s="T144">adv</ta>
            <ta e="T146" id="Seg_2060" s="T145">quant</ta>
            <ta e="T147" id="Seg_2061" s="T146">que-n:case=ptcl</ta>
            <ta e="T148" id="Seg_2062" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_2063" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_2064" s="T149">adv</ta>
            <ta e="T151" id="Seg_2065" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2066" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_2067" s="T152">adv</ta>
            <ta e="T154" id="Seg_2068" s="T153">adv</ta>
            <ta e="T155" id="Seg_2069" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_2070" s="T155">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_2071" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_2072" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_2073" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_2074" s="T159">adv</ta>
            <ta e="T161" id="Seg_2075" s="T160">adv</ta>
            <ta e="T162" id="Seg_2076" s="T161">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2077" s="T163">pers</ta>
            <ta e="T165" id="Seg_2078" s="T164">adv</ta>
            <ta e="T166" id="Seg_2079" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_2080" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2081" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_2082" s="T168">conj</ta>
            <ta e="T170" id="Seg_2083" s="T169">dempro-n:case</ta>
            <ta e="T171" id="Seg_2084" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_2085" s="T171">v-v&gt;v-v:pn</ta>
            <ta e="T173" id="Seg_2086" s="T172">conj</ta>
            <ta e="T174" id="Seg_2087" s="T173">pers</ta>
            <ta e="T175" id="Seg_2088" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2089" s="T175">v-v:n.fin-n:case</ta>
            <ta e="T177" id="Seg_2090" s="T176">conj</ta>
            <ta e="T178" id="Seg_2091" s="T177">pers</ta>
            <ta e="T179" id="Seg_2092" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_2093" s="T179">v-v:n.fin-n:case</ta>
            <ta e="T182" id="Seg_2094" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_2095" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_2096" s="T183">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2097" s="T0">pers</ta>
            <ta e="T2" id="Seg_2098" s="T1">n</ta>
            <ta e="T3" id="Seg_2099" s="T2">v</ta>
            <ta e="T185" id="Seg_2100" s="T3">propr</ta>
            <ta e="T4" id="Seg_2101" s="T185">n</ta>
            <ta e="T5" id="Seg_2102" s="T4">v</ta>
            <ta e="T6" id="Seg_2103" s="T5">n</ta>
            <ta e="T7" id="Seg_2104" s="T6">pers</ta>
            <ta e="T8" id="Seg_2105" s="T7">n</ta>
            <ta e="T9" id="Seg_2106" s="T8">v</ta>
            <ta e="T10" id="Seg_2107" s="T9">conj</ta>
            <ta e="T11" id="Seg_2108" s="T10">que</ta>
            <ta e="T13" id="Seg_2109" s="T12">pers</ta>
            <ta e="T14" id="Seg_2110" s="T13">v</ta>
            <ta e="T15" id="Seg_2111" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_2112" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2113" s="T16">v</ta>
            <ta e="T18" id="Seg_2114" s="T17">conj</ta>
            <ta e="T19" id="Seg_2115" s="T18">n</ta>
            <ta e="T20" id="Seg_2116" s="T19">v</ta>
            <ta e="T21" id="Seg_2117" s="T20">adv</ta>
            <ta e="T22" id="Seg_2118" s="T21">n</ta>
            <ta e="T23" id="Seg_2119" s="T22">v</ta>
            <ta e="T24" id="Seg_2120" s="T23">dempro</ta>
            <ta e="T25" id="Seg_2121" s="T24">v</ta>
            <ta e="T26" id="Seg_2122" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_2123" s="T26">pers</ta>
            <ta e="T29" id="Seg_2124" s="T28">v</ta>
            <ta e="T30" id="Seg_2125" s="T29">refl</ta>
            <ta e="T31" id="Seg_2126" s="T30">n</ta>
            <ta e="T32" id="Seg_2127" s="T31">adv</ta>
            <ta e="T33" id="Seg_2128" s="T32">pers</ta>
            <ta e="T34" id="Seg_2129" s="T33">v</ta>
            <ta e="T36" id="Seg_2130" s="T34">ptcl</ta>
            <ta e="T37" id="Seg_2131" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_2132" s="T37">adv</ta>
            <ta e="T39" id="Seg_2133" s="T38">pers</ta>
            <ta e="T40" id="Seg_2134" s="T39">v</ta>
            <ta e="T41" id="Seg_2135" s="T40">dempro</ta>
            <ta e="T42" id="Seg_2136" s="T41">propr</ta>
            <ta e="T43" id="Seg_2137" s="T42">v</ta>
            <ta e="T44" id="Seg_2138" s="T43">pers</ta>
            <ta e="T45" id="Seg_2139" s="T44">adv</ta>
            <ta e="T46" id="Seg_2140" s="T45">v</ta>
            <ta e="T47" id="Seg_2141" s="T46">n</ta>
            <ta e="T48" id="Seg_2142" s="T47">pers</ta>
            <ta e="T49" id="Seg_2143" s="T48">v</ta>
            <ta e="T186" id="Seg_2144" s="T49">propr</ta>
            <ta e="T50" id="Seg_2145" s="T186">n</ta>
            <ta e="T51" id="Seg_2146" s="T50">n</ta>
            <ta e="T52" id="Seg_2147" s="T51">v</ta>
            <ta e="T53" id="Seg_2148" s="T52">adv</ta>
            <ta e="T54" id="Seg_2149" s="T53">v</ta>
            <ta e="T55" id="Seg_2150" s="T54">adv</ta>
            <ta e="T56" id="Seg_2151" s="T55">adv</ta>
            <ta e="T57" id="Seg_2152" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2153" s="T57">v</ta>
            <ta e="T59" id="Seg_2154" s="T58">n</ta>
            <ta e="T60" id="Seg_2155" s="T59">v</ta>
            <ta e="T61" id="Seg_2156" s="T60">adv</ta>
            <ta e="T62" id="Seg_2157" s="T61">adv</ta>
            <ta e="T63" id="Seg_2158" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_2159" s="T63">n</ta>
            <ta e="T65" id="Seg_2160" s="T64">v</ta>
            <ta e="T66" id="Seg_2161" s="T65">adv</ta>
            <ta e="T67" id="Seg_2162" s="T66">pers</ta>
            <ta e="T68" id="Seg_2163" s="T67">v</ta>
            <ta e="T69" id="Seg_2164" s="T68">n</ta>
            <ta e="T70" id="Seg_2165" s="T69">dempro</ta>
            <ta e="T71" id="Seg_2166" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_2167" s="T71">v</ta>
            <ta e="T73" id="Seg_2168" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_2169" s="T73">v</ta>
            <ta e="T75" id="Seg_2170" s="T74">v</ta>
            <ta e="T76" id="Seg_2171" s="T75">v</ta>
            <ta e="T77" id="Seg_2172" s="T76">v</ta>
            <ta e="T78" id="Seg_2173" s="T77">n</ta>
            <ta e="T79" id="Seg_2174" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_2175" s="T79">v</ta>
            <ta e="T81" id="Seg_2176" s="T80">conj</ta>
            <ta e="T82" id="Seg_2177" s="T81">n</ta>
            <ta e="T83" id="Seg_2178" s="T82">adv</ta>
            <ta e="T84" id="Seg_2179" s="T83">v</ta>
            <ta e="T85" id="Seg_2180" s="T84">v</ta>
            <ta e="T86" id="Seg_2181" s="T85">n</ta>
            <ta e="T87" id="Seg_2182" s="T86">v</ta>
            <ta e="T88" id="Seg_2183" s="T87">pers</ta>
            <ta e="T89" id="Seg_2184" s="T88">v</ta>
            <ta e="T90" id="Seg_2185" s="T89">v</ta>
            <ta e="T91" id="Seg_2186" s="T90">n</ta>
            <ta e="T92" id="Seg_2187" s="T91">que</ta>
            <ta e="T93" id="Seg_2188" s="T92">dempro</ta>
            <ta e="T94" id="Seg_2189" s="T93">n</ta>
            <ta e="T95" id="Seg_2190" s="T94">v</ta>
            <ta e="T96" id="Seg_2191" s="T95">n</ta>
            <ta e="T97" id="Seg_2192" s="T96">adv</ta>
            <ta e="T98" id="Seg_2193" s="T97">v</ta>
            <ta e="T99" id="Seg_2194" s="T98">conj</ta>
            <ta e="T101" id="Seg_2195" s="T100">n</ta>
            <ta e="T103" id="Seg_2196" s="T102">n</ta>
            <ta e="T104" id="Seg_2197" s="T103">v</ta>
            <ta e="T105" id="Seg_2198" s="T104">adv</ta>
            <ta e="T106" id="Seg_2199" s="T105">v</ta>
            <ta e="T107" id="Seg_2200" s="T106">n</ta>
            <ta e="T108" id="Seg_2201" s="T107">num</ta>
            <ta e="T109" id="Seg_2202" s="T108">n</ta>
            <ta e="T110" id="Seg_2203" s="T109">v</ta>
            <ta e="T111" id="Seg_2204" s="T110">v</ta>
            <ta e="T112" id="Seg_2205" s="T111">conj</ta>
            <ta e="T113" id="Seg_2206" s="T112">n</ta>
            <ta e="T114" id="Seg_2207" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_2208" s="T114">adj</ta>
            <ta e="T116" id="Seg_2209" s="T115">n</ta>
            <ta e="T117" id="Seg_2210" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_2211" s="T117">v</ta>
            <ta e="T119" id="Seg_2212" s="T118">n</ta>
            <ta e="T120" id="Seg_2213" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2214" s="T120">v</ta>
            <ta e="T122" id="Seg_2215" s="T121">v</ta>
            <ta e="T123" id="Seg_2216" s="T122">dempro</ta>
            <ta e="T124" id="Seg_2217" s="T123">n</ta>
            <ta e="T125" id="Seg_2218" s="T124">pers</ta>
            <ta e="T126" id="Seg_2219" s="T125">adv</ta>
            <ta e="T127" id="Seg_2220" s="T126">pers</ta>
            <ta e="T128" id="Seg_2221" s="T127">n</ta>
            <ta e="T129" id="Seg_2222" s="T128">dempro</ta>
            <ta e="T130" id="Seg_2223" s="T129">v</ta>
            <ta e="T131" id="Seg_2224" s="T130">conj</ta>
            <ta e="T132" id="Seg_2225" s="T131">v</ta>
            <ta e="T133" id="Seg_2226" s="T132">dempro</ta>
            <ta e="T134" id="Seg_2227" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2228" s="T134">aux</ta>
            <ta e="T136" id="Seg_2229" s="T135">v</ta>
            <ta e="T137" id="Seg_2230" s="T136">adv</ta>
            <ta e="T138" id="Seg_2231" s="T137">n</ta>
            <ta e="T139" id="Seg_2232" s="T138">n</ta>
            <ta e="T140" id="Seg_2233" s="T139">v</ta>
            <ta e="T141" id="Seg_2234" s="T140">v</ta>
            <ta e="T142" id="Seg_2235" s="T141">v</ta>
            <ta e="T143" id="Seg_2236" s="T142">v</ta>
            <ta e="T144" id="Seg_2237" s="T143">n</ta>
            <ta e="T145" id="Seg_2238" s="T144">adv</ta>
            <ta e="T146" id="Seg_2239" s="T145">quant</ta>
            <ta e="T147" id="Seg_2240" s="T146">que</ta>
            <ta e="T148" id="Seg_2241" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_2242" s="T148">v</ta>
            <ta e="T150" id="Seg_2243" s="T149">adv</ta>
            <ta e="T151" id="Seg_2244" s="T150">n</ta>
            <ta e="T152" id="Seg_2245" s="T151">v</ta>
            <ta e="T153" id="Seg_2246" s="T152">adv</ta>
            <ta e="T154" id="Seg_2247" s="T153">adv</ta>
            <ta e="T155" id="Seg_2248" s="T154">n</ta>
            <ta e="T156" id="Seg_2249" s="T155">v</ta>
            <ta e="T157" id="Seg_2250" s="T156">v</ta>
            <ta e="T158" id="Seg_2251" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_2252" s="T158">n</ta>
            <ta e="T160" id="Seg_2253" s="T159">adv</ta>
            <ta e="T161" id="Seg_2254" s="T160">adv</ta>
            <ta e="T162" id="Seg_2255" s="T161">v</ta>
            <ta e="T164" id="Seg_2256" s="T163">pers</ta>
            <ta e="T165" id="Seg_2257" s="T164">adv</ta>
            <ta e="T166" id="Seg_2258" s="T165">n</ta>
            <ta e="T167" id="Seg_2259" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2260" s="T167">v</ta>
            <ta e="T169" id="Seg_2261" s="T168">conj</ta>
            <ta e="T170" id="Seg_2262" s="T169">dempro</ta>
            <ta e="T171" id="Seg_2263" s="T170">n</ta>
            <ta e="T172" id="Seg_2264" s="T171">v</ta>
            <ta e="T173" id="Seg_2265" s="T172">conj</ta>
            <ta e="T174" id="Seg_2266" s="T173">pers</ta>
            <ta e="T175" id="Seg_2267" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2268" s="T175">adj</ta>
            <ta e="T177" id="Seg_2269" s="T176">conj</ta>
            <ta e="T178" id="Seg_2270" s="T177">pers</ta>
            <ta e="T179" id="Seg_2271" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_2272" s="T179">adj</ta>
            <ta e="T182" id="Seg_2273" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_2274" s="T182">v</ta>
            <ta e="T184" id="Seg_2275" s="T183">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2276" s="T0">pro.h:Poss</ta>
            <ta e="T2" id="Seg_2277" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_2278" s="T3">np:So</ta>
            <ta e="T5" id="Seg_2279" s="T4">0.3.h:A</ta>
            <ta e="T7" id="Seg_2280" s="T6">pro.h:Th</ta>
            <ta e="T8" id="Seg_2281" s="T7">np:L</ta>
            <ta e="T13" id="Seg_2282" s="T12">pro.h:E</ta>
            <ta e="T17" id="Seg_2283" s="T16">0.1.h:E</ta>
            <ta e="T19" id="Seg_2284" s="T18">np.h:R</ta>
            <ta e="T20" id="Seg_2285" s="T19">0.1.h:A</ta>
            <ta e="T21" id="Seg_2286" s="T20">adv:Time</ta>
            <ta e="T22" id="Seg_2287" s="T21">np:G</ta>
            <ta e="T23" id="Seg_2288" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_2289" s="T23">pro.h:A</ta>
            <ta e="T27" id="Seg_2290" s="T26">pro.h:A</ta>
            <ta e="T30" id="Seg_2291" s="T29">pro.h:Poss</ta>
            <ta e="T31" id="Seg_2292" s="T30">np:Ins</ta>
            <ta e="T32" id="Seg_2293" s="T31">adv:Time</ta>
            <ta e="T33" id="Seg_2294" s="T32">pro.h:E</ta>
            <ta e="T38" id="Seg_2295" s="T37">adv:Time</ta>
            <ta e="T39" id="Seg_2296" s="T38">pro.h:E</ta>
            <ta e="T42" id="Seg_2297" s="T41">np.h:A</ta>
            <ta e="T44" id="Seg_2298" s="T43">pro.h:Th</ta>
            <ta e="T45" id="Seg_2299" s="T44">adv:L</ta>
            <ta e="T46" id="Seg_2300" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_2301" s="T46">np:G</ta>
            <ta e="T48" id="Seg_2302" s="T47">pro.h:A</ta>
            <ta e="T50" id="Seg_2303" s="T49">np:G</ta>
            <ta e="T51" id="Seg_2304" s="T50">np.h:R</ta>
            <ta e="T53" id="Seg_2305" s="T52">adv:Time</ta>
            <ta e="T54" id="Seg_2306" s="T53">0.1.h:A</ta>
            <ta e="T55" id="Seg_2307" s="T54">adv:L</ta>
            <ta e="T56" id="Seg_2308" s="T55">adv:L</ta>
            <ta e="T58" id="Seg_2309" s="T57">0.3.h:A</ta>
            <ta e="T59" id="Seg_2310" s="T58">np.h:R</ta>
            <ta e="T61" id="Seg_2311" s="T60">adv:Time</ta>
            <ta e="T64" id="Seg_2312" s="T63">np.h:R</ta>
            <ta e="T65" id="Seg_2313" s="T64">0.1.h:A</ta>
            <ta e="T66" id="Seg_2314" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_2315" s="T66">pro.h:A</ta>
            <ta e="T69" id="Seg_2316" s="T68">np:G</ta>
            <ta e="T70" id="Seg_2317" s="T69">pro.h:A</ta>
            <ta e="T74" id="Seg_2318" s="T73">0.1.h:A</ta>
            <ta e="T75" id="Seg_2319" s="T74">0.1.h:A</ta>
            <ta e="T76" id="Seg_2320" s="T75">0.1.h:A</ta>
            <ta e="T77" id="Seg_2321" s="T76">0.1.h:A</ta>
            <ta e="T78" id="Seg_2322" s="T77">np:Th</ta>
            <ta e="T82" id="Seg_2323" s="T81">np:Th</ta>
            <ta e="T83" id="Seg_2324" s="T82">adv:Time</ta>
            <ta e="T84" id="Seg_2325" s="T83">0.1.h:A</ta>
            <ta e="T85" id="Seg_2326" s="T84">0.1.h:A</ta>
            <ta e="T86" id="Seg_2327" s="T85">np:Th</ta>
            <ta e="T88" id="Seg_2328" s="T87">pro.h:A</ta>
            <ta e="T90" id="Seg_2329" s="T89">0.1.h:A</ta>
            <ta e="T91" id="Seg_2330" s="T90">np:Pth</ta>
            <ta e="T94" id="Seg_2331" s="T93">np:L</ta>
            <ta e="T95" id="Seg_2332" s="T94">0.1.h:A</ta>
            <ta e="T96" id="Seg_2333" s="T95">np:G</ta>
            <ta e="T97" id="Seg_2334" s="T96">adv:L</ta>
            <ta e="T98" id="Seg_2335" s="T97">0.1.h:A</ta>
            <ta e="T103" id="Seg_2336" s="T102">n:Time</ta>
            <ta e="T104" id="Seg_2337" s="T103">0.1.h:A</ta>
            <ta e="T105" id="Seg_2338" s="T104">adv:Time</ta>
            <ta e="T106" id="Seg_2339" s="T105">0.1.h:A</ta>
            <ta e="T107" id="Seg_2340" s="T106">np:G</ta>
            <ta e="T109" id="Seg_2341" s="T108">n:Time</ta>
            <ta e="T111" id="Seg_2342" s="T110">0.1.h:A</ta>
            <ta e="T113" id="Seg_2343" s="T112">np:Th</ta>
            <ta e="T116" id="Seg_2344" s="T115">np:A</ta>
            <ta e="T119" id="Seg_2345" s="T118">np:Th</ta>
            <ta e="T121" id="Seg_2346" s="T120">0.3:A</ta>
            <ta e="T124" id="Seg_2347" s="T123">np.h:A</ta>
            <ta e="T125" id="Seg_2348" s="T124">pro.h:R</ta>
            <ta e="T126" id="Seg_2349" s="T125">adv:Time</ta>
            <ta e="T127" id="Seg_2350" s="T126">pro.h:A</ta>
            <ta e="T128" id="Seg_2351" s="T127">np:Th</ta>
            <ta e="T129" id="Seg_2352" s="T128">pro.h:R</ta>
            <ta e="T132" id="Seg_2353" s="T131">0.1.h:A</ta>
            <ta e="T133" id="Seg_2354" s="T132">pro.h:A</ta>
            <ta e="T135" id="Seg_2355" s="T134">0.2.h:A</ta>
            <ta e="T137" id="Seg_2356" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_2357" s="T137">n:Time</ta>
            <ta e="T139" id="Seg_2358" s="T138">np:A</ta>
            <ta e="T141" id="Seg_2359" s="T140">0.1.h:A</ta>
            <ta e="T143" id="Seg_2360" s="T142">0.1.h:E</ta>
            <ta e="T144" id="Seg_2361" s="T143">np:Th</ta>
            <ta e="T147" id="Seg_2362" s="T146">pro:Th</ta>
            <ta e="T149" id="Seg_2363" s="T148">0.1.h:A</ta>
            <ta e="T150" id="Seg_2364" s="T149">adv:Time</ta>
            <ta e="T151" id="Seg_2365" s="T150">n:Time</ta>
            <ta e="T152" id="Seg_2366" s="T151">0.1.h:A</ta>
            <ta e="T153" id="Seg_2367" s="T152">adv:Time</ta>
            <ta e="T155" id="Seg_2368" s="T154">n:Time</ta>
            <ta e="T156" id="Seg_2369" s="T155">0.1.h:A</ta>
            <ta e="T157" id="Seg_2370" s="T156">0.1.h:E</ta>
            <ta e="T161" id="Seg_2371" s="T160">adv:Time</ta>
            <ta e="T162" id="Seg_2372" s="T161">0.1.h:A</ta>
            <ta e="T164" id="Seg_2373" s="T163">pro.h:A</ta>
            <ta e="T165" id="Seg_2374" s="T164">adv:Time</ta>
            <ta e="T166" id="Seg_2375" s="T165">np:Th</ta>
            <ta e="T171" id="Seg_2376" s="T170">np.h:A</ta>
            <ta e="T174" id="Seg_2377" s="T173">pro.h:Poss</ta>
            <ta e="T178" id="Seg_2378" s="T177">pro.h:Poss</ta>
            <ta e="T183" id="Seg_2379" s="T182">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2380" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_2381" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_2382" s="T4">v:pred 0.3.h:S</ta>
            <ta e="T7" id="Seg_2383" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_2384" s="T8">v:pred</ta>
            <ta e="T13" id="Seg_2385" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_2386" s="T13">v:pred</ta>
            <ta e="T17" id="Seg_2387" s="T16">v:pred 0.1.h:S</ta>
            <ta e="T20" id="Seg_2388" s="T19">v:pred 0.1.h:S</ta>
            <ta e="T23" id="Seg_2389" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T24" id="Seg_2390" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_2391" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_2392" s="T26">pro.h:S</ta>
            <ta e="T29" id="Seg_2393" s="T28">v:pred</ta>
            <ta e="T33" id="Seg_2394" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_2395" s="T33">v:pred</ta>
            <ta e="T39" id="Seg_2396" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_2397" s="T39">v:pred</ta>
            <ta e="T42" id="Seg_2398" s="T41">np.h:S</ta>
            <ta e="T43" id="Seg_2399" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_2400" s="T43">pro.h:O</ta>
            <ta e="T46" id="Seg_2401" s="T45">v:pred 0.3.h:S</ta>
            <ta e="T48" id="Seg_2402" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_2403" s="T48">v:pred</ta>
            <ta e="T52" id="Seg_2404" s="T51">s:purp</ta>
            <ta e="T54" id="Seg_2405" s="T53">v:pred 0.1.h:S</ta>
            <ta e="T57" id="Seg_2406" s="T56">ptcl.neg</ta>
            <ta e="T58" id="Seg_2407" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T60" id="Seg_2408" s="T59">s:purp</ta>
            <ta e="T65" id="Seg_2409" s="T64">v:pred 0.1.h:S</ta>
            <ta e="T67" id="Seg_2410" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_2411" s="T67">v:pred</ta>
            <ta e="T70" id="Seg_2412" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_2413" s="T70">ptcl.neg</ta>
            <ta e="T72" id="Seg_2414" s="T71">v:pred</ta>
            <ta e="T73" id="Seg_2415" s="T72">ptcl.neg</ta>
            <ta e="T74" id="Seg_2416" s="T73">v:pred 0.1.h:S</ta>
            <ta e="T75" id="Seg_2417" s="T74">v:pred 0.1.h:S</ta>
            <ta e="T76" id="Seg_2418" s="T75">v:pred 0.1.h:S</ta>
            <ta e="T77" id="Seg_2419" s="T76">v:pred 0.1.h:S</ta>
            <ta e="T78" id="Seg_2420" s="T77">np:S</ta>
            <ta e="T80" id="Seg_2421" s="T79">v:pred</ta>
            <ta e="T82" id="Seg_2422" s="T81">np:S</ta>
            <ta e="T84" id="Seg_2423" s="T83">v:pred 0.1.h:S</ta>
            <ta e="T85" id="Seg_2424" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T86" id="Seg_2425" s="T85">np:S</ta>
            <ta e="T87" id="Seg_2426" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_2427" s="T87">pro.h:S</ta>
            <ta e="T89" id="Seg_2428" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_2429" s="T89">v:pred 0.1.h:S</ta>
            <ta e="T95" id="Seg_2430" s="T94">v:pred 0.1.h:S</ta>
            <ta e="T98" id="Seg_2431" s="T97">v:pred 0.1.h:S</ta>
            <ta e="T104" id="Seg_2432" s="T103">v:pred 0.1.h:S</ta>
            <ta e="T106" id="Seg_2433" s="T105">v:pred 0.1.h:S</ta>
            <ta e="T109" id="Seg_2434" s="T108">n:pred</ta>
            <ta e="T110" id="Seg_2435" s="T109">cop 0.3:S</ta>
            <ta e="T111" id="Seg_2436" s="T110">v:pred 0.1.h:S</ta>
            <ta e="T113" id="Seg_2437" s="T112">np:S</ta>
            <ta e="T114" id="Seg_2438" s="T113">ptcl.neg</ta>
            <ta e="T115" id="Seg_2439" s="T114">adj:pred</ta>
            <ta e="T116" id="Seg_2440" s="T115">np:S</ta>
            <ta e="T118" id="Seg_2441" s="T117">v:pred</ta>
            <ta e="T119" id="Seg_2442" s="T118">np:O</ta>
            <ta e="T120" id="Seg_2443" s="T119">ptcl:pred</ta>
            <ta e="T121" id="Seg_2444" s="T120">v:pred 0.3:S</ta>
            <ta e="T122" id="Seg_2445" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_2446" s="T123">np.h:S</ta>
            <ta e="T127" id="Seg_2447" s="T126">pro.h:S</ta>
            <ta e="T128" id="Seg_2448" s="T127">np:O</ta>
            <ta e="T130" id="Seg_2449" s="T129">v:pred</ta>
            <ta e="T132" id="Seg_2450" s="T131">v:pred 0.1.h:S</ta>
            <ta e="T133" id="Seg_2451" s="T132">pro.h:S</ta>
            <ta e="T134" id="Seg_2452" s="T133">ptcl.neg</ta>
            <ta e="T135" id="Seg_2453" s="T134">v:pred 0.2.h:S</ta>
            <ta e="T139" id="Seg_2454" s="T138">np:S</ta>
            <ta e="T140" id="Seg_2455" s="T139">v:pred</ta>
            <ta e="T141" id="Seg_2456" s="T140">v:pred 0.1.h:S</ta>
            <ta e="T142" id="Seg_2457" s="T141">s:purp</ta>
            <ta e="T143" id="Seg_2458" s="T142">v:pred 0.1.h:S</ta>
            <ta e="T144" id="Seg_2459" s="T143">np:S</ta>
            <ta e="T146" id="Seg_2460" s="T145">adj:pred</ta>
            <ta e="T147" id="Seg_2461" s="T146">pro:O</ta>
            <ta e="T148" id="Seg_2462" s="T147">ptcl.neg</ta>
            <ta e="T149" id="Seg_2463" s="T148">v:pred 0.1.h:S</ta>
            <ta e="T152" id="Seg_2464" s="T151">v:pred 0.1.h:S</ta>
            <ta e="T156" id="Seg_2465" s="T155">v:pred 0.1.h:S</ta>
            <ta e="T157" id="Seg_2466" s="T156">v:pred 0.1.h:S</ta>
            <ta e="T162" id="Seg_2467" s="T161">v:pred 0.1.h:S</ta>
            <ta e="T164" id="Seg_2468" s="T163">pro.h:S</ta>
            <ta e="T166" id="Seg_2469" s="T165">np:O</ta>
            <ta e="T167" id="Seg_2470" s="T166">ptcl.neg</ta>
            <ta e="T168" id="Seg_2471" s="T167">v:pred</ta>
            <ta e="T171" id="Seg_2472" s="T170">np.h:S</ta>
            <ta e="T172" id="Seg_2473" s="T171">v:pred</ta>
            <ta e="T175" id="Seg_2474" s="T174">ptcl.neg</ta>
            <ta e="T176" id="Seg_2475" s="T175">adj:pred</ta>
            <ta e="T179" id="Seg_2476" s="T178">ptcl.neg</ta>
            <ta e="T180" id="Seg_2477" s="T179">adj:pred</ta>
            <ta e="T183" id="Seg_2478" s="T182">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_2479" s="T185">TAT:cult</ta>
            <ta e="T10" id="Seg_2480" s="T9">RUS:gram</ta>
            <ta e="T15" id="Seg_2481" s="T14">TURK:disc</ta>
            <ta e="T16" id="Seg_2482" s="T15">RUS:mod</ta>
            <ta e="T18" id="Seg_2483" s="T17">RUS:gram</ta>
            <ta e="T19" id="Seg_2484" s="T18">TURK:cult</ta>
            <ta e="T26" id="Seg_2485" s="T25">TURK:disc</ta>
            <ta e="T29" id="Seg_2486" s="T28">TURK:cor</ta>
            <ta e="T36" id="Seg_2487" s="T34">RUS:mod</ta>
            <ta e="T42" id="Seg_2488" s="T41">RUS:cult</ta>
            <ta e="T50" id="Seg_2489" s="T186">TAT:cult</ta>
            <ta e="T51" id="Seg_2490" s="T50">TURK:cult</ta>
            <ta e="T59" id="Seg_2491" s="T58">TURK:cult</ta>
            <ta e="T63" id="Seg_2492" s="T62">TURK:disc</ta>
            <ta e="T64" id="Seg_2493" s="T63">TURK:cult</ta>
            <ta e="T73" id="Seg_2494" s="T72">TURK:disc</ta>
            <ta e="T79" id="Seg_2495" s="T78">TURK:disc</ta>
            <ta e="T81" id="Seg_2496" s="T80">RUS:gram</ta>
            <ta e="T86" id="Seg_2497" s="T85">RUS:cult</ta>
            <ta e="T94" id="Seg_2498" s="T93">TAT:cult</ta>
            <ta e="T99" id="Seg_2499" s="T98">RUS:gram</ta>
            <ta e="T109" id="Seg_2500" s="T108">RUS:cult</ta>
            <ta e="T112" id="Seg_2501" s="T111">RUS:gram</ta>
            <ta e="T113" id="Seg_2502" s="T112">TURK:cult</ta>
            <ta e="T117" id="Seg_2503" s="T116">TURK:disc</ta>
            <ta e="T119" id="Seg_2504" s="T118">TURK:cult</ta>
            <ta e="T131" id="Seg_2505" s="T130">RUS:gram</ta>
            <ta e="T139" id="Seg_2506" s="T138">TURK:cult</ta>
            <ta e="T144" id="Seg_2507" s="T143">TURK:cult</ta>
            <ta e="T147" id="Seg_2508" s="T146">TURK:gram(INDEF)</ta>
            <ta e="T158" id="Seg_2509" s="T157">TURK:disc</ta>
            <ta e="T159" id="Seg_2510" s="T158">TURK:cult</ta>
            <ta e="T166" id="Seg_2511" s="T165">TURK:cult</ta>
            <ta e="T169" id="Seg_2512" s="T168">RUS:gram</ta>
            <ta e="T173" id="Seg_2513" s="T172">RUS:gram</ta>
            <ta e="T177" id="Seg_2514" s="T176">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T86" id="Seg_2515" s="T85">dir:bare</ta>
            <ta e="T109" id="Seg_2516" s="T108">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2517" s="T0">Наша дочь приехала (из?) Агинского.</ta>
            <ta e="T9" id="Seg_2518" s="T4">Она говорит: "Бабушка, ты на бумаге". [?]</ta>
            <ta e="T15" id="Seg_2519" s="T9">Как я испугалась!</ta>
            <ta e="T20" id="Seg_2520" s="T15">Я подумала, что, может быть, я молилась Богу.</ta>
            <ta e="T23" id="Seg_2521" s="T20">А меня записали.</ta>
            <ta e="T31" id="Seg_2522" s="T23">Она говорит: "Нет, ты говоришь на своем языке".</ta>
            <ta e="T36" id="Seg_2523" s="T31">Тогда я подумала, может…</ta>
            <ta e="T37" id="Seg_2524" s="T36">Хватит.</ta>
            <ta e="T40" id="Seg_2525" s="T37">Тогда я подумала.</ta>
            <ta e="T47" id="Seg_2526" s="T40">Эля записала меня, а там перенесла [это] на бумагу. [?]</ta>
            <ta e="T50" id="Seg_2527" s="T47">Я поехала в Агинское.</ta>
            <ta e="T52" id="Seg_2528" s="T50">Помолиться Богу.</ta>
            <ta e="T55" id="Seg_2529" s="T52">Потом пришла туда.</ta>
            <ta e="T60" id="Seg_2530" s="T55">Там они не пришли молиться.</ta>
            <ta e="T63" id="Seg_2531" s="T60">Тогда одни…</ta>
            <ta e="T65" id="Seg_2532" s="T63">мы помолились.</ta>
            <ta e="T69" id="Seg_2533" s="T65">Потом я пошла домой.</ta>
            <ta e="T72" id="Seg_2534" s="T69">Они меня не пускают.</ta>
            <ta e="T75" id="Seg_2535" s="T72">"Нет, говорю, я пойду".</ta>
            <ta e="T82" id="Seg_2536" s="T75">Я шла-шла, дождь идет, и ветер.</ta>
            <ta e="T85" id="Seg_2537" s="T82">Я шла, шла.</ta>
            <ta e="T87" id="Seg_2538" s="T85">Машина стоит.</ta>
            <ta e="T91" id="Seg_2539" s="T87">Я шла, шла по дороге.</ta>
            <ta e="T96" id="Seg_2540" s="T91">Туде, где эти дома, я пришла, в (Куражу?).</ta>
            <ta e="T104" id="Seg_2541" s="T96">Я там переночевала и утром встала.</ta>
            <ta e="T107" id="Seg_2542" s="T104">Потом пошла домой.</ta>
            <ta e="T110" id="Seg_2543" s="T107">Было десять часов.</ta>
            <ta e="T115" id="Seg_2544" s="T110">Я пришла, корова не доена.</ta>
            <ta e="T118" id="Seg_2545" s="T115">Теленок мычит.</ta>
            <ta e="T121" id="Seg_2546" s="T118">Молока не хватает.</ta>
            <ta e="T125" id="Seg_2547" s="T121">Эта женщина обманула меня.</ta>
            <ta e="T130" id="Seg_2548" s="T125">Тогда я дала ему картошки.</ta>
            <ta e="T136" id="Seg_2549" s="T130">И открыла [= выпустила?] его, не кричи!</ta>
            <ta e="T140" id="Seg_2550" s="T136">Потом вечером пришла корова.</ta>
            <ta e="T142" id="Seg_2551" s="T140">Я пошла [ее] доить.</ta>
            <ta e="T146" id="Seg_2552" s="T142">Смотрю: молока очень много.</ta>
            <ta e="T149" id="Seg_2553" s="T146">Я ничего не сказала.</ta>
            <ta e="T152" id="Seg_2554" s="T149">Утром подоила.</ta>
            <ta e="T156" id="Seg_2555" s="T152">Потом вечером опять подоила.</ta>
            <ta e="T160" id="Seg_2556" s="T156">Смотрю: молока мало.</ta>
            <ta e="T168" id="Seg_2557" s="T160">Тогда я сказала: "Вы вчера корову не доили?"</ta>
            <ta e="T172" id="Seg_2558" s="T168">А та девушка говорит:</ta>
            <ta e="T183" id="Seg_2559" s="T172">"И наша не доенная, и твоя не доенная, мы так [их] отпустили".</ta>
            <ta e="T184" id="Seg_2560" s="T183">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2561" s="T0">Our daughter came (from?) Aginskoye.</ta>
            <ta e="T9" id="Seg_2562" s="T4">She says: "Grandmother, you are on the paper." [?]</ta>
            <ta e="T15" id="Seg_2563" s="T9">How I was afraid.</ta>
            <ta e="T20" id="Seg_2564" s="T15">I thought that maybe I was praying to God.</ta>
            <ta e="T23" id="Seg_2565" s="T20">Then they put it on the paper [= wrote this down, recorded it].</ta>
            <ta e="T31" id="Seg_2566" s="T23">She says: no, you are speaking in your own language.</ta>
            <ta e="T36" id="Seg_2567" s="T31">Then I thought, maybe…</ta>
            <ta e="T37" id="Seg_2568" s="T36">Enough.</ta>
            <ta e="T40" id="Seg_2569" s="T37">Then I thought.</ta>
            <ta e="T47" id="Seg_2570" s="T40">Elya had recorded me, there she put [it] on the paper. [?]</ta>
            <ta e="T50" id="Seg_2571" s="T47">I went to Aginskoye.</ta>
            <ta e="T52" id="Seg_2572" s="T50">To pray to God.</ta>
            <ta e="T55" id="Seg_2573" s="T52">Then I came there.</ta>
            <ta e="T60" id="Seg_2574" s="T55">There they did not come to pray to God.</ta>
            <ta e="T63" id="Seg_2575" s="T60">Then alone…</ta>
            <ta e="T65" id="Seg_2576" s="T63">we prayed to God.</ta>
            <ta e="T69" id="Seg_2577" s="T65">Then I went home.</ta>
            <ta e="T72" id="Seg_2578" s="T69">They do not [want to] let me go.</ta>
            <ta e="T75" id="Seg_2579" s="T72">"No, I say, I will go."</ta>
            <ta e="T82" id="Seg_2580" s="T75">I went, I went, rain comes, and wind.</ta>
            <ta e="T85" id="Seg_2581" s="T82">Then I came, I came.</ta>
            <ta e="T87" id="Seg_2582" s="T85">A car stands.</ta>
            <ta e="T91" id="Seg_2583" s="T87">I came, I came on the road.</ta>
            <ta e="T96" id="Seg_2584" s="T91">Where these houses are, I came to (Kurazha?).</ta>
            <ta e="T104" id="Seg_2585" s="T96">I spent the night there and in the morning I got up.</ta>
            <ta e="T107" id="Seg_2586" s="T104">Then I came home.</ta>
            <ta e="T110" id="Seg_2587" s="T107">It was ten o’clock.</ta>
            <ta e="T115" id="Seg_2588" s="T110">I came, and the cow was not milked.</ta>
            <ta e="T118" id="Seg_2589" s="T115">The calf is yelling.</ta>
            <ta e="T121" id="Seg_2590" s="T118">There is not enough milk.</ta>
            <ta e="T125" id="Seg_2591" s="T121">This woman lied to me.</ta>
            <ta e="T130" id="Seg_2592" s="T125">Then I gave it potatoes.</ta>
            <ta e="T136" id="Seg_2593" s="T130">And opened it [= let it out?], do not yell!</ta>
            <ta e="T140" id="Seg_2594" s="T136">Then in the evening the cow came.</ta>
            <ta e="T142" id="Seg_2595" s="T140">I went to milk [it].</ta>
            <ta e="T146" id="Seg_2596" s="T142">I see: there is very much milk.</ta>
            <ta e="T149" id="Seg_2597" s="T146">I did not say anything.</ta>
            <ta e="T152" id="Seg_2598" s="T149">Then in the morning I milked.</ta>
            <ta e="T156" id="Seg_2599" s="T152">Then again in the evening I milked.</ta>
            <ta e="T160" id="Seg_2600" s="T156">I see, there is little milk.</ta>
            <ta e="T168" id="Seg_2601" s="T160">Then I said: "You did not milk the cow yesterday?"</ta>
            <ta e="T172" id="Seg_2602" s="T168">And the girl says:</ta>
            <ta e="T183" id="Seg_2603" s="T172">We did not milk our [cow], and yours, too, we let [them] go so.</ta>
            <ta e="T184" id="Seg_2604" s="T183">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2605" s="T0">Unsere Tochter kam (aus?) Aginskoje.</ta>
            <ta e="T9" id="Seg_2606" s="T4">Sie sagt: "Oma, du bist auf dem Papier." [?]</ta>
            <ta e="T15" id="Seg_2607" s="T9">Was für eine Angst ich hatte.</ta>
            <ta e="T20" id="Seg_2608" s="T15">Ich dachte, dass ich vielleicht zu Gott beten würde.</ta>
            <ta e="T23" id="Seg_2609" s="T20">Dann brachte ich es zu Papier [= schrieb es nieder].</ta>
            <ta e="T31" id="Seg_2610" s="T23">Sie sagt: "Nein, du sprichst auf deiner eigenen Sprache."</ta>
            <ta e="T36" id="Seg_2611" s="T31">Dann dachte ich, vielleicht…</ta>
            <ta e="T37" id="Seg_2612" s="T36">Genug.</ta>
            <ta e="T40" id="Seg_2613" s="T37">Dann dachte ich.</ta>
            <ta e="T47" id="Seg_2614" s="T40">Elja hatte mich aufgenommen, und dort [es] zu Papier gebracht. [?]</ta>
            <ta e="T50" id="Seg_2615" s="T47">Ich ging nach Aginskoje.</ta>
            <ta e="T52" id="Seg_2616" s="T50">Um zu Gott zu beten.</ta>
            <ta e="T55" id="Seg_2617" s="T52">Dann kam ich dorthin.</ta>
            <ta e="T60" id="Seg_2618" s="T55">Dorthin kamen sie nicht, um zu Gott zu beten.</ta>
            <ta e="T63" id="Seg_2619" s="T60">Dann alleine…</ta>
            <ta e="T65" id="Seg_2620" s="T63">beteten wir zu Gott.</ta>
            <ta e="T69" id="Seg_2621" s="T65">Dann ging ich nach Hause.</ta>
            <ta e="T72" id="Seg_2622" s="T69">Sie [wollten] mich nicht gehen lassen.</ta>
            <ta e="T75" id="Seg_2623" s="T72">"Nein", sage ich, "ich werden gehen."</ta>
            <ta e="T82" id="Seg_2624" s="T75">Ich ging und ging, Regen kommt und Wind.</ta>
            <ta e="T85" id="Seg_2625" s="T82">Dann kam ich, ich kam.</ta>
            <ta e="T87" id="Seg_2626" s="T85">Ein Auto steht da.</ta>
            <ta e="T91" id="Seg_2627" s="T87">Ich kam, ich kam auf der Straße.</ta>
            <ta e="T96" id="Seg_2628" s="T91">Wo diese Häuser sind, kam ich zu (Kurazha?).</ta>
            <ta e="T104" id="Seg_2629" s="T96">Ich verbrachte die Nacht dort und stand am Morgen auf.</ta>
            <ta e="T107" id="Seg_2630" s="T104">Nach kam ich nach Hause.</ta>
            <ta e="T110" id="Seg_2631" s="T107">Es war zehn Uhr.</ta>
            <ta e="T115" id="Seg_2632" s="T110">Ich kam und die Kuh war nicht gemolken.</ta>
            <ta e="T118" id="Seg_2633" s="T115">Das Kalb schreit.</ta>
            <ta e="T121" id="Seg_2634" s="T118">Es gibt nicht genug Milch.</ta>
            <ta e="T125" id="Seg_2635" s="T121">Diese Frau hat mich angelogen.</ta>
            <ta e="T130" id="Seg_2636" s="T125">Dann gab ich ihm Kartoffeln.</ta>
            <ta e="T136" id="Seg_2637" s="T130">Und öffnete es [= ließ es heraus?], schrei nicht!</ta>
            <ta e="T140" id="Seg_2638" s="T136">Dann am Abend kam die Kuh.</ta>
            <ta e="T142" id="Seg_2639" s="T140">Ich ging, um [sie] zu melken.</ta>
            <ta e="T146" id="Seg_2640" s="T142">Ich sehe: dort ist viel Milch.</ta>
            <ta e="T149" id="Seg_2641" s="T146">Ich sagte nichts.</ta>
            <ta e="T152" id="Seg_2642" s="T149">Dann am Morgen molk ich.</ta>
            <ta e="T156" id="Seg_2643" s="T152">Dann molk ich wieder am Abend.</ta>
            <ta e="T160" id="Seg_2644" s="T156">Ich sehe, dort ist wenig Milch.</ta>
            <ta e="T168" id="Seg_2645" s="T160">Dann sagte ich: "Hast du die Kuh gestern nicht gemolken?"</ta>
            <ta e="T172" id="Seg_2646" s="T168">Und das Mädchen sagt:</ta>
            <ta e="T183" id="Seg_2647" s="T172">"Wir haben die unsere nicht gemolken, und die deine auch nicht, wir haben [sie] so gehen lassen."</ta>
            <ta e="T184" id="Seg_2648" s="T183">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T9" id="Seg_2649" s="T4">[GVY:] on the paper = in a newspaper?</ta>
            <ta e="T110" id="Seg_2650" s="T107">[GVY:] časɨ is the Russian NOM.PL</ta>
            <ta e="T125" id="Seg_2651" s="T121">[GVY:] Some woman had promised to milk the cow and did not do it?</ta>
            <ta e="T136" id="Seg_2652" s="T130">[GVY:] iʔ kirgarlaʔbə = iʔ kirgaraʔ?</ta>
            <ta e="T142" id="Seg_2653" s="T140">[GVY:] pronounced [š]</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T185" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T186" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
