<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID37087D20-6E90-49D4-C564-EB9D9DF7D51D">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BearMeat_nar.wav" />
         <referenced-file url="PKZ_196X_BearMeat_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_BearMeat_nar\PKZ_196X_BearMeat_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">28</ud-information>
            <ud-information attribute-name="# HIAT:w">21</ud-information>
            <ud-information attribute-name="# e">21</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.685" type="appl" />
         <tli id="T2" time="1.37" type="appl" />
         <tli id="T3" time="2.055" type="appl" />
         <tli id="T4" time="2.74" type="appl" />
         <tli id="T5" time="3.425" type="appl" />
         <tli id="T6" time="3.953" type="appl" />
         <tli id="T7" time="4.482" type="appl" />
         <tli id="T8" time="5.01" type="appl" />
         <tli id="T9" time="5.934" type="appl" />
         <tli id="T10" time="7.191304122121711" />
         <tli id="T11" time="8.286" type="appl" />
         <tli id="T12" time="9.272" type="appl" />
         <tli id="T13" time="10.259" type="appl" />
         <tli id="T14" time="11.245" type="appl" />
         <tli id="T15" time="12.232" type="appl" />
         <tli id="T16" time="12.866" type="appl" />
         <tli id="T17" time="13.5" type="appl" />
         <tli id="T18" time="14.135" type="appl" />
         <tli id="T19" time="14.769" type="appl" />
         <tli id="T20" time="15.403" type="appl" />
         <tli id="T21" time="16.462" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T21" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Teinen</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">măna</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">urgaːban</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">uja</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">deʔpiʔi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Măn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ej</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ambiam</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Koʔbsaŋdə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">mĭluʔpiem</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">I</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">šide</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">pürbiʔi</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">pirogəʔi</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">deʔpiʔi</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Măn</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">onʼiʔ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">ambiam</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">onʼiʔ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">iʔbolaʔbə</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">Kabarləj</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T21" id="Seg_83" n="sc" s="T0">
               <ts e="T1" id="Seg_85" n="e" s="T0">Teinen </ts>
               <ts e="T2" id="Seg_87" n="e" s="T1">măna </ts>
               <ts e="T3" id="Seg_89" n="e" s="T2">urgaːban </ts>
               <ts e="T4" id="Seg_91" n="e" s="T3">uja </ts>
               <ts e="T5" id="Seg_93" n="e" s="T4">deʔpiʔi. </ts>
               <ts e="T6" id="Seg_95" n="e" s="T5">Măn </ts>
               <ts e="T7" id="Seg_97" n="e" s="T6">ej </ts>
               <ts e="T8" id="Seg_99" n="e" s="T7">ambiam. </ts>
               <ts e="T9" id="Seg_101" n="e" s="T8">Koʔbsaŋdə </ts>
               <ts e="T10" id="Seg_103" n="e" s="T9">mĭluʔpiem. </ts>
               <ts e="T11" id="Seg_105" n="e" s="T10">I </ts>
               <ts e="T12" id="Seg_107" n="e" s="T11">šide </ts>
               <ts e="T13" id="Seg_109" n="e" s="T12">pürbiʔi </ts>
               <ts e="T14" id="Seg_111" n="e" s="T13">pirogəʔi </ts>
               <ts e="T15" id="Seg_113" n="e" s="T14">deʔpiʔi. </ts>
               <ts e="T16" id="Seg_115" n="e" s="T15">Măn </ts>
               <ts e="T17" id="Seg_117" n="e" s="T16">onʼiʔ </ts>
               <ts e="T18" id="Seg_119" n="e" s="T17">ambiam, </ts>
               <ts e="T19" id="Seg_121" n="e" s="T18">onʼiʔ </ts>
               <ts e="T20" id="Seg_123" n="e" s="T19">iʔbolaʔbə. </ts>
               <ts e="T21" id="Seg_125" n="e" s="T20">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_126" s="T0">PKZ_196X_BearMeat_nar.001 (001)</ta>
            <ta e="T8" id="Seg_127" s="T5">PKZ_196X_BearMeat_nar.002 (003)</ta>
            <ta e="T10" id="Seg_128" s="T8">PKZ_196X_BearMeat_nar.003 (004)</ta>
            <ta e="T15" id="Seg_129" s="T10">PKZ_196X_BearMeat_nar.004 (005)</ta>
            <ta e="T20" id="Seg_130" s="T15">PKZ_196X_BearMeat_nar.005 (006)</ta>
            <ta e="T21" id="Seg_131" s="T20">PKZ_196X_BearMeat_nar.006 (007)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_132" s="T0">Teinen măna urgaːban uja deʔpiʔi. </ta>
            <ta e="T8" id="Seg_133" s="T5">Măn ej ambiam. </ta>
            <ta e="T10" id="Seg_134" s="T8">Koʔbsaŋdə mĭluʔpiem. </ta>
            <ta e="T15" id="Seg_135" s="T10">I šide pürbiʔi pirogəʔi deʔpiʔi. </ta>
            <ta e="T20" id="Seg_136" s="T15">Măn onʼiʔ ambiam, onʼiʔ iʔbolaʔbə. </ta>
            <ta e="T21" id="Seg_137" s="T20">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_138" s="T0">teinen</ta>
            <ta e="T2" id="Seg_139" s="T1">măna</ta>
            <ta e="T3" id="Seg_140" s="T2">urgaːba-n</ta>
            <ta e="T4" id="Seg_141" s="T3">uja</ta>
            <ta e="T5" id="Seg_142" s="T4">deʔ-pi-ʔi</ta>
            <ta e="T6" id="Seg_143" s="T5">măn</ta>
            <ta e="T7" id="Seg_144" s="T6">ej</ta>
            <ta e="T8" id="Seg_145" s="T7">am-bia-m</ta>
            <ta e="T9" id="Seg_146" s="T8">koʔb-saŋ-də</ta>
            <ta e="T10" id="Seg_147" s="T9">mĭ-luʔ-pie-m</ta>
            <ta e="T11" id="Seg_148" s="T10">i</ta>
            <ta e="T12" id="Seg_149" s="T11">šide</ta>
            <ta e="T13" id="Seg_150" s="T12">pür-bi-ʔi</ta>
            <ta e="T14" id="Seg_151" s="T13">pirog-əʔi</ta>
            <ta e="T15" id="Seg_152" s="T14">deʔ-pi-ʔi</ta>
            <ta e="T16" id="Seg_153" s="T15">măn</ta>
            <ta e="T17" id="Seg_154" s="T16">onʼiʔ</ta>
            <ta e="T18" id="Seg_155" s="T17">am-bia-m</ta>
            <ta e="T19" id="Seg_156" s="T18">onʼiʔ</ta>
            <ta e="T20" id="Seg_157" s="T19">iʔbo-laʔbə</ta>
            <ta e="T21" id="Seg_158" s="T20">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_159" s="T0">teinen</ta>
            <ta e="T2" id="Seg_160" s="T1">măna</ta>
            <ta e="T3" id="Seg_161" s="T2">urgaːba-n</ta>
            <ta e="T4" id="Seg_162" s="T3">uja</ta>
            <ta e="T5" id="Seg_163" s="T4">det-bi-jəʔ</ta>
            <ta e="T6" id="Seg_164" s="T5">măn</ta>
            <ta e="T7" id="Seg_165" s="T6">ej</ta>
            <ta e="T8" id="Seg_166" s="T7">am-bi-m</ta>
            <ta e="T9" id="Seg_167" s="T8">koʔbdo-zAŋ-Tə</ta>
            <ta e="T10" id="Seg_168" s="T9">mĭ-luʔbdə-bi-m</ta>
            <ta e="T11" id="Seg_169" s="T10">i</ta>
            <ta e="T12" id="Seg_170" s="T11">šide</ta>
            <ta e="T13" id="Seg_171" s="T12">pür-bi-jəʔ</ta>
            <ta e="T14" id="Seg_172" s="T13">pirog-jəʔ</ta>
            <ta e="T15" id="Seg_173" s="T14">det-bi-jəʔ</ta>
            <ta e="T16" id="Seg_174" s="T15">măn</ta>
            <ta e="T17" id="Seg_175" s="T16">onʼiʔ</ta>
            <ta e="T18" id="Seg_176" s="T17">am-bi-m</ta>
            <ta e="T19" id="Seg_177" s="T18">onʼiʔ</ta>
            <ta e="T20" id="Seg_178" s="T19">iʔbö-laʔbə</ta>
            <ta e="T21" id="Seg_179" s="T20">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_180" s="T0">today</ta>
            <ta e="T2" id="Seg_181" s="T1">I.LAT</ta>
            <ta e="T3" id="Seg_182" s="T2">bear-GEN</ta>
            <ta e="T4" id="Seg_183" s="T3">meat.[NOM.SG]</ta>
            <ta e="T5" id="Seg_184" s="T4">bring-PST-3PL</ta>
            <ta e="T6" id="Seg_185" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_186" s="T6">NEG</ta>
            <ta e="T8" id="Seg_187" s="T7">eat-PST-1SG</ta>
            <ta e="T9" id="Seg_188" s="T8">daughter-PL-LAT</ta>
            <ta e="T10" id="Seg_189" s="T9">give-MOM-PST-1SG</ta>
            <ta e="T11" id="Seg_190" s="T10">and</ta>
            <ta e="T12" id="Seg_191" s="T11">two.[NOM.SG]</ta>
            <ta e="T13" id="Seg_192" s="T12">bake-PST-3PL</ta>
            <ta e="T14" id="Seg_193" s="T13">pie-PL</ta>
            <ta e="T15" id="Seg_194" s="T14">bring-PST-3PL</ta>
            <ta e="T16" id="Seg_195" s="T15">I.NOM</ta>
            <ta e="T17" id="Seg_196" s="T16">one.[NOM.SG]</ta>
            <ta e="T18" id="Seg_197" s="T17">eat-PST-1SG</ta>
            <ta e="T19" id="Seg_198" s="T18">one.[NOM.SG]</ta>
            <ta e="T20" id="Seg_199" s="T19">lie-DUR.[3SG]</ta>
            <ta e="T21" id="Seg_200" s="T20">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_201" s="T0">сегодня</ta>
            <ta e="T2" id="Seg_202" s="T1">я.LAT</ta>
            <ta e="T3" id="Seg_203" s="T2">медведь-GEN</ta>
            <ta e="T4" id="Seg_204" s="T3">мясо.[NOM.SG]</ta>
            <ta e="T5" id="Seg_205" s="T4">принести-PST-3PL</ta>
            <ta e="T6" id="Seg_206" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_207" s="T6">NEG</ta>
            <ta e="T8" id="Seg_208" s="T7">съесть-PST-1SG</ta>
            <ta e="T9" id="Seg_209" s="T8">дочь-PL-LAT</ta>
            <ta e="T10" id="Seg_210" s="T9">дать-MOM-PST-1SG</ta>
            <ta e="T11" id="Seg_211" s="T10">и</ta>
            <ta e="T12" id="Seg_212" s="T11">два.[NOM.SG]</ta>
            <ta e="T13" id="Seg_213" s="T12">печь-PST-3PL</ta>
            <ta e="T14" id="Seg_214" s="T13">пирог-PL</ta>
            <ta e="T15" id="Seg_215" s="T14">принести-PST-3PL</ta>
            <ta e="T16" id="Seg_216" s="T15">я.NOM</ta>
            <ta e="T17" id="Seg_217" s="T16">один.[NOM.SG]</ta>
            <ta e="T18" id="Seg_218" s="T17">съесть-PST-1SG</ta>
            <ta e="T19" id="Seg_219" s="T18">один.[NOM.SG]</ta>
            <ta e="T20" id="Seg_220" s="T19">лежать-DUR.[3SG]</ta>
            <ta e="T21" id="Seg_221" s="T20">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_222" s="T0">adv</ta>
            <ta e="T2" id="Seg_223" s="T1">pers</ta>
            <ta e="T3" id="Seg_224" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_225" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_226" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_227" s="T5">pers</ta>
            <ta e="T7" id="Seg_228" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_229" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_230" s="T8">n-n:num-n:case</ta>
            <ta e="T10" id="Seg_231" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_232" s="T10">conj</ta>
            <ta e="T12" id="Seg_233" s="T11">num-n:case</ta>
            <ta e="T13" id="Seg_234" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_235" s="T13">n-n:num</ta>
            <ta e="T15" id="Seg_236" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_237" s="T15">pers</ta>
            <ta e="T17" id="Seg_238" s="T16">num-n:case</ta>
            <ta e="T18" id="Seg_239" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_240" s="T18">num-n:case</ta>
            <ta e="T20" id="Seg_241" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_242" s="T20">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_243" s="T0">adv</ta>
            <ta e="T2" id="Seg_244" s="T1">pers</ta>
            <ta e="T3" id="Seg_245" s="T2">n</ta>
            <ta e="T4" id="Seg_246" s="T3">n</ta>
            <ta e="T5" id="Seg_247" s="T4">v</ta>
            <ta e="T6" id="Seg_248" s="T5">pers</ta>
            <ta e="T7" id="Seg_249" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_250" s="T7">v</ta>
            <ta e="T9" id="Seg_251" s="T8">n</ta>
            <ta e="T10" id="Seg_252" s="T9">v</ta>
            <ta e="T11" id="Seg_253" s="T10">conj</ta>
            <ta e="T12" id="Seg_254" s="T11">num</ta>
            <ta e="T13" id="Seg_255" s="T12">v</ta>
            <ta e="T14" id="Seg_256" s="T13">n</ta>
            <ta e="T15" id="Seg_257" s="T14">v</ta>
            <ta e="T16" id="Seg_258" s="T15">pers</ta>
            <ta e="T17" id="Seg_259" s="T16">num</ta>
            <ta e="T18" id="Seg_260" s="T17">v</ta>
            <ta e="T19" id="Seg_261" s="T18">num</ta>
            <ta e="T20" id="Seg_262" s="T19">v</ta>
            <ta e="T21" id="Seg_263" s="T20">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_264" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_265" s="T1">0.1.h:P</ta>
            <ta e="T3" id="Seg_266" s="T2">np:Poss</ta>
            <ta e="T4" id="Seg_267" s="T3">np:Th</ta>
            <ta e="T5" id="Seg_268" s="T4">0.3.h:A</ta>
            <ta e="T6" id="Seg_269" s="T5">pro.h:A</ta>
            <ta e="T9" id="Seg_270" s="T8">np.h:R</ta>
            <ta e="T10" id="Seg_271" s="T9">0.1.h:A</ta>
            <ta e="T14" id="Seg_272" s="T13">np:Th</ta>
            <ta e="T15" id="Seg_273" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_274" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_275" s="T16">np:P</ta>
            <ta e="T19" id="Seg_276" s="T18">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_277" s="T3">np:O</ta>
            <ta e="T5" id="Seg_278" s="T4">v:pred 0.3.h:S</ta>
            <ta e="T6" id="Seg_279" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_280" s="T6">ptcl.neg</ta>
            <ta e="T8" id="Seg_281" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_282" s="T9">v:pred 0.1.h:S</ta>
            <ta e="T14" id="Seg_283" s="T13">np:O</ta>
            <ta e="T15" id="Seg_284" s="T14">v:pred 0.3.h:S</ta>
            <ta e="T16" id="Seg_285" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_286" s="T16">np:O</ta>
            <ta e="T18" id="Seg_287" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_288" s="T18">np:S</ta>
            <ta e="T20" id="Seg_289" s="T19">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_290" s="T10">RUS:gram</ta>
            <ta e="T14" id="Seg_291" s="T13">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_292" s="T0">Мне сегодня принесли медвежатины.</ta>
            <ta e="T8" id="Seg_293" s="T5">Я не ела.</ta>
            <ta e="T10" id="Seg_294" s="T8">Я отдала [ее] девочкам.</ta>
            <ta e="T15" id="Seg_295" s="T10">И они испекли и принесли мне два пирога.</ta>
            <ta e="T20" id="Seg_296" s="T15">Я один съела, один лежит.</ta>
            <ta e="T21" id="Seg_297" s="T20">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_298" s="T0">Today they brought me bear meat.</ta>
            <ta e="T8" id="Seg_299" s="T5">I did not eat.</ta>
            <ta e="T10" id="Seg_300" s="T8">I gave [it] to the girls.</ta>
            <ta e="T15" id="Seg_301" s="T10">And they brought me two baked pies.</ta>
            <ta e="T20" id="Seg_302" s="T15">I ate one, one is lying.</ta>
            <ta e="T21" id="Seg_303" s="T20">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_304" s="T0">Heute wurde mir Bärenfleisch gebracht.</ta>
            <ta e="T8" id="Seg_305" s="T5">Ich aß nicht.</ta>
            <ta e="T10" id="Seg_306" s="T8">Ich gab [es] den Mädchen.</ta>
            <ta e="T15" id="Seg_307" s="T10">Und sie brachten mir zwei gebackene Piroggen.</ta>
            <ta e="T20" id="Seg_308" s="T15">Ich aß eine, eine liegt dort.</ta>
            <ta e="T21" id="Seg_309" s="T20">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
