<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID0BBE5B7F-75CF-6D48-291B-934979EC6357">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_AfterChase_nar.wav" />
         <referenced-file url="PKZ_196X_AfterChase_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\nar\PKZ_196X_AfterChase_nar\PKZ_196X_AfterChase_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">26</ud-information>
            <ud-information attribute-name="# HIAT:w">18</ud-information>
            <ud-information attribute-name="# e">18</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.056" type="appl" />
         <tli id="T1" time="0.813" type="appl" />
         <tli id="T2" time="1.57" type="appl" />
         <tli id="T3" time="2.326" type="appl" />
         <tli id="T4" time="3.083" type="appl" />
         <tli id="T5" time="3.809" type="appl" />
         <tli id="T6" time="4.535" type="appl" />
         <tli id="T7" time="5.772899887626382" />
         <tli id="T8" time="6.444" type="appl" />
         <tli id="T9" time="7.171" type="appl" />
         <tli id="T10" time="7.897" type="appl" />
         <tli id="T11" time="8.836" type="appl" />
         <tli id="T12" time="9.774" type="appl" />
         <tli id="T13" time="10.713" type="appl" />
         <tli id="T14" time="13.019022494843334" />
         <tli id="T15" time="14.233" type="appl" />
         <tli id="T16" time="15.309" type="appl" />
         <tli id="T17" time="16.384" type="appl" />
         <tli id="T18" time="17.472" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T18" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Šide</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kambiʔi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">saməjzittə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭgəttə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">šobiʔi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">maːʔndə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Ibiʔi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">ara</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">bĭʔpiʔi</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_42" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">Dĭgəttə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">davaj</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">dʼabərosʼtə</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">kudonzittə</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_58" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">Toʔnarbiʔi</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">odʼin</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">onʼiʔtə</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_70" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">Kabarləj</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T18" id="Seg_75" n="sc" s="T0">
               <ts e="T1" id="Seg_77" n="e" s="T0">Šide </ts>
               <ts e="T2" id="Seg_79" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_81" n="e" s="T2">kambiʔi </ts>
               <ts e="T4" id="Seg_83" n="e" s="T3">saməjzittə. </ts>
               <ts e="T5" id="Seg_85" n="e" s="T4">Dĭgəttə </ts>
               <ts e="T6" id="Seg_87" n="e" s="T5">šobiʔi </ts>
               <ts e="T7" id="Seg_89" n="e" s="T6">maːʔndə. </ts>
               <ts e="T8" id="Seg_91" n="e" s="T7">Ibiʔi </ts>
               <ts e="T9" id="Seg_93" n="e" s="T8">ara, </ts>
               <ts e="T10" id="Seg_95" n="e" s="T9">bĭʔpiʔi. </ts>
               <ts e="T11" id="Seg_97" n="e" s="T10">Dĭgəttə </ts>
               <ts e="T12" id="Seg_99" n="e" s="T11">davaj </ts>
               <ts e="T13" id="Seg_101" n="e" s="T12">dʼabərosʼtə, </ts>
               <ts e="T14" id="Seg_103" n="e" s="T13">kudonzittə. </ts>
               <ts e="T15" id="Seg_105" n="e" s="T14">Toʔnarbiʔi </ts>
               <ts e="T16" id="Seg_107" n="e" s="T15">odʼin </ts>
               <ts e="T17" id="Seg_109" n="e" s="T16">onʼiʔtə. </ts>
               <ts e="T18" id="Seg_111" n="e" s="T17">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_112" s="T0">PKZ_196X_AfterChase_nar.001 (002)</ta>
            <ta e="T7" id="Seg_113" s="T4">PKZ_196X_AfterChase_nar.002 (003)</ta>
            <ta e="T10" id="Seg_114" s="T7">PKZ_196X_AfterChase_nar.003 (004)</ta>
            <ta e="T14" id="Seg_115" s="T10">PKZ_196X_AfterChase_nar.004 (005)</ta>
            <ta e="T17" id="Seg_116" s="T14">PKZ_196X_AfterChase_nar.005 (006)</ta>
            <ta e="T18" id="Seg_117" s="T17">PKZ_196X_AfterChase_nar.006 (007)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_118" s="T0">Šide kuza kambiʔi saməjzittə. </ta>
            <ta e="T7" id="Seg_119" s="T4">Dĭgəttə šobiʔi maːʔndə. </ta>
            <ta e="T10" id="Seg_120" s="T7">Ibiʔi ara, bĭʔpiʔi. </ta>
            <ta e="T14" id="Seg_121" s="T10">Dĭgəttə davaj dʼabərosʼtə, kudonzittə. </ta>
            <ta e="T17" id="Seg_122" s="T14">Toʔnarbiʔi odʼin onʼiʔtə. </ta>
            <ta e="T18" id="Seg_123" s="T17">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_124" s="T0">šide</ta>
            <ta e="T2" id="Seg_125" s="T1">kuza</ta>
            <ta e="T3" id="Seg_126" s="T2">kam-bi-ʔi</ta>
            <ta e="T4" id="Seg_127" s="T3">saməj-zittə</ta>
            <ta e="T5" id="Seg_128" s="T4">dĭgəttə</ta>
            <ta e="T6" id="Seg_129" s="T5">šo-bi-ʔi</ta>
            <ta e="T7" id="Seg_130" s="T6">maːʔ-ndə</ta>
            <ta e="T8" id="Seg_131" s="T7">i-bi-ʔi</ta>
            <ta e="T9" id="Seg_132" s="T8">ara</ta>
            <ta e="T10" id="Seg_133" s="T9">bĭʔ-pi-ʔi</ta>
            <ta e="T11" id="Seg_134" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_135" s="T11">davaj</ta>
            <ta e="T13" id="Seg_136" s="T12">dʼabəro-sʼtə</ta>
            <ta e="T14" id="Seg_137" s="T13">kudon-zittə</ta>
            <ta e="T15" id="Seg_138" s="T14">toʔ-nar-bi-ʔi</ta>
            <ta e="T16" id="Seg_139" s="T15">odʼin</ta>
            <ta e="T17" id="Seg_140" s="T16">onʼiʔ-tə</ta>
            <ta e="T18" id="Seg_141" s="T17">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_142" s="T0">šide</ta>
            <ta e="T2" id="Seg_143" s="T1">kuza</ta>
            <ta e="T3" id="Seg_144" s="T2">kan-bi-jəʔ</ta>
            <ta e="T4" id="Seg_145" s="T3">saməj-zittə</ta>
            <ta e="T5" id="Seg_146" s="T4">dĭgəttə</ta>
            <ta e="T6" id="Seg_147" s="T5">šo-bi-jəʔ</ta>
            <ta e="T7" id="Seg_148" s="T6">maʔ-gəndə</ta>
            <ta e="T8" id="Seg_149" s="T7">i-bi-jəʔ</ta>
            <ta e="T9" id="Seg_150" s="T8">ara</ta>
            <ta e="T10" id="Seg_151" s="T9">bĭs-bi-jəʔ</ta>
            <ta e="T11" id="Seg_152" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_153" s="T11">davaj</ta>
            <ta e="T13" id="Seg_154" s="T12">tʼabəro-zittə</ta>
            <ta e="T14" id="Seg_155" s="T13">kudonz-zittə</ta>
            <ta e="T15" id="Seg_156" s="T14">toʔbdə-nar-bi-jəʔ</ta>
            <ta e="T16" id="Seg_157" s="T15">odʼin</ta>
            <ta e="T17" id="Seg_158" s="T16">onʼiʔ-Tə</ta>
            <ta e="T18" id="Seg_159" s="T17">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_160" s="T0">two.[NOM.SG]</ta>
            <ta e="T2" id="Seg_161" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_162" s="T2">go-PST-3PL</ta>
            <ta e="T4" id="Seg_163" s="T3">hunt-INF.LAT</ta>
            <ta e="T5" id="Seg_164" s="T4">then</ta>
            <ta e="T6" id="Seg_165" s="T5">come-PST-3PL</ta>
            <ta e="T7" id="Seg_166" s="T6">tent-LAT/LOC.3SG</ta>
            <ta e="T8" id="Seg_167" s="T7">take-PST-3PL</ta>
            <ta e="T9" id="Seg_168" s="T8">vodka.[NOM.SG]</ta>
            <ta e="T10" id="Seg_169" s="T9">drink-PST-3PL</ta>
            <ta e="T11" id="Seg_170" s="T10">then</ta>
            <ta e="T12" id="Seg_171" s="T11">INCH</ta>
            <ta e="T13" id="Seg_172" s="T12">fight-INF.LAT</ta>
            <ta e="T14" id="Seg_173" s="T13">scold-INF.LAT</ta>
            <ta e="T15" id="Seg_174" s="T14">hit-MULT-PST-3PL</ta>
            <ta e="T16" id="Seg_175" s="T15">one</ta>
            <ta e="T17" id="Seg_176" s="T16">one-LAT</ta>
            <ta e="T18" id="Seg_177" s="T17">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_178" s="T0">два.[NOM.SG]</ta>
            <ta e="T2" id="Seg_179" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_180" s="T2">пойти-PST-3PL</ta>
            <ta e="T4" id="Seg_181" s="T3">охотиться-INF.LAT</ta>
            <ta e="T5" id="Seg_182" s="T4">тогда</ta>
            <ta e="T6" id="Seg_183" s="T5">прийти-PST-3PL</ta>
            <ta e="T7" id="Seg_184" s="T6">чум-LAT/LOC.3SG</ta>
            <ta e="T8" id="Seg_185" s="T7">взять-PST-3PL</ta>
            <ta e="T9" id="Seg_186" s="T8">водка.[NOM.SG]</ta>
            <ta e="T10" id="Seg_187" s="T9">пить-PST-3PL</ta>
            <ta e="T11" id="Seg_188" s="T10">тогда</ta>
            <ta e="T12" id="Seg_189" s="T11">INCH</ta>
            <ta e="T13" id="Seg_190" s="T12">бороться-INF.LAT</ta>
            <ta e="T14" id="Seg_191" s="T13">ругать-INF.LAT</ta>
            <ta e="T15" id="Seg_192" s="T14">ударить-MULT-PST-3PL</ta>
            <ta e="T16" id="Seg_193" s="T15">один</ta>
            <ta e="T17" id="Seg_194" s="T16">один-LAT</ta>
            <ta e="T18" id="Seg_195" s="T17">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_196" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_197" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_198" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_199" s="T3">v-v:n.fin</ta>
            <ta e="T5" id="Seg_200" s="T4">adv</ta>
            <ta e="T6" id="Seg_201" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_202" s="T6">n-n:case.poss</ta>
            <ta e="T8" id="Seg_203" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_204" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_205" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_206" s="T10">adv</ta>
            <ta e="T12" id="Seg_207" s="T11">v</ta>
            <ta e="T13" id="Seg_208" s="T12">v-v:n.fin</ta>
            <ta e="T14" id="Seg_209" s="T13">v-v:n.fin</ta>
            <ta e="T15" id="Seg_210" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_211" s="T15">num</ta>
            <ta e="T17" id="Seg_212" s="T16">num-n:case</ta>
            <ta e="T18" id="Seg_213" s="T17">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_214" s="T0">num</ta>
            <ta e="T2" id="Seg_215" s="T1">n</ta>
            <ta e="T3" id="Seg_216" s="T2">v</ta>
            <ta e="T4" id="Seg_217" s="T3">v</ta>
            <ta e="T5" id="Seg_218" s="T4">adv</ta>
            <ta e="T6" id="Seg_219" s="T5">v</ta>
            <ta e="T7" id="Seg_220" s="T6">n</ta>
            <ta e="T8" id="Seg_221" s="T7">v</ta>
            <ta e="T9" id="Seg_222" s="T8">n</ta>
            <ta e="T10" id="Seg_223" s="T9">v</ta>
            <ta e="T11" id="Seg_224" s="T10">adv</ta>
            <ta e="T12" id="Seg_225" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_226" s="T12">v</ta>
            <ta e="T14" id="Seg_227" s="T13">v</ta>
            <ta e="T15" id="Seg_228" s="T14">v</ta>
            <ta e="T16" id="Seg_229" s="T15">num</ta>
            <ta e="T17" id="Seg_230" s="T16">num</ta>
            <ta e="T18" id="Seg_231" s="T17">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_232" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_233" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_234" s="T3">s:purp</ta>
            <ta e="T6" id="Seg_235" s="T5"> 0.3.h:S v:pred</ta>
            <ta e="T8" id="Seg_236" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_237" s="T8">np:O</ta>
            <ta e="T10" id="Seg_238" s="T9">0.3.h:S v:pred</ta>
            <ta e="T13" id="Seg_239" s="T12">s:compl</ta>
            <ta e="T14" id="Seg_240" s="T13">s:compl</ta>
            <ta e="T15" id="Seg_241" s="T14">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_242" s="T1">np.h:A</ta>
            <ta e="T6" id="Seg_243" s="T5">0.3.h:A</ta>
            <ta e="T8" id="Seg_244" s="T7">0.3.h:A</ta>
            <ta e="T9" id="Seg_245" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_246" s="T9">0.3.h:A</ta>
            <ta e="T15" id="Seg_247" s="T14">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_248" s="T8">TURK:cult</ta>
            <ta e="T12" id="Seg_249" s="T11">RUS:gram</ta>
            <ta e="T16" id="Seg_250" s="T15">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_251" s="T0">Двое мужчин пошли охотиться.</ta>
            <ta e="T7" id="Seg_252" s="T4">Потом пришли домой.</ta>
            <ta e="T10" id="Seg_253" s="T7">Взяли водки, выпили.</ta>
            <ta e="T14" id="Seg_254" s="T10">Потом стали драться, ругаться.</ta>
            <ta e="T17" id="Seg_255" s="T14">Били друг друга.</ta>
            <ta e="T18" id="Seg_256" s="T17">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_257" s="T0">Two men went to hunt.</ta>
            <ta e="T7" id="Seg_258" s="T4">Then they came home.</ta>
            <ta e="T10" id="Seg_259" s="T7">They took vodka and drank it.</ta>
            <ta e="T14" id="Seg_260" s="T10">Then they started to fight, to swear.</ta>
            <ta e="T17" id="Seg_261" s="T14">They hit each other.</ta>
            <ta e="T18" id="Seg_262" s="T17">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_263" s="T0">Zwei Männer gingen jagen.</ta>
            <ta e="T7" id="Seg_264" s="T4">Dann kamen sie nach Hause.</ta>
            <ta e="T10" id="Seg_265" s="T7">Sie nahmen und tranken Wodka.</ta>
            <ta e="T14" id="Seg_266" s="T10">Dann fingen sie an zu kämpfen, zu fluchen.</ta>
            <ta e="T17" id="Seg_267" s="T14">Sie schlugen einander.</ta>
            <ta e="T18" id="Seg_268" s="T17">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_269" s="T0">[GVY:] šaməjzittə.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
